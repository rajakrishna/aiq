String label = "aiq-webapp-${UUID.randomUUID().toString()}" as String

properties([gitLabConnection('gitlab')])

podTemplate(label: label, yaml: """
apiVersion: v1
kind: Pod
spec:
  containers:
  - name: node
    image: node:12
    command: ['cat']
    tty: true
  - name: docker
    image: docker:19
    command: ['cat']
    tty: true
    volumeMounts:
    - name: dockersock
      mountPath: /var/run/docker.sock
    - name: docker-credentials
      mountPath: /root/.docker
  - name: kubectl
    image: bitnami/kubectl:1.20
    command: ['cat']
    tty: true
    securityContext:
      runAsUser: 1000
    volumeMounts:
    - name: kubeconfig
      mountPath: /.kube
  volumes:
  - name: dockersock
    hostPath:
      path: /var/run/docker.sock
  - name: docker-credentials
    secret:
      secretName: gcr-aiops-docker-credentials
      items:
      - key: .dockerconfigjson
        path: config.json
  - name: kubeconfig
    secret:
      secretName: kube-config-new
""") {
  String buildType = 'feature'
  String gitCommit = ''
  try {
    gitlabBuilds(builds: ['checkout', 'yarn build', 'docker build', 'deploy dev', 'deploy stage']) {
      node(label) {
        stage('checkout') {
          gitlabCommitStatus('checkout') {
            def scmVars = checkout scm
            echo "scm vars: ${scmVars}"
            gitCommit = scmVars["GIT_COMMIT"]
            echo "gitCommit: ${gitCommit}"
            sh "ls -la"
            def sourceBranch
            try {
              sourceBranch = gitlabSourceBranch
            } catch (ignored) {
              sourceBranch = BRANCH_NAME
            }
            echo "sourceBranch is ${sourceBranch}"
            try {
              if (sourceBranch =~ /release/) {
                buildType = 'release'
              } else if (sourceBranch == 'develop') {
                buildType = 'snapshot'
              }
            } catch (ignored) {
              buildType = 'feature'
            }
            echo "Building ${buildType} from branch ${sourceBranch}"
          }
        }
        stage('yarn build') {
          gitlabCommitStatus('yarn build') {
            container('node') {
              String buildTag = 'dev'
              if (buildType == 'snapshot') {
                buildTag = 'stage'
              } else if (buildType == 'release') {
                buildTag = 'prod'
              }
              sh """
              node -v
              npm -v
              yarn install
              CI=false yarn build:${buildTag}
              """
              dir('build') {
                sh "tar -cvzf aiq-webapp-${gitCommit}.tar.gz *"
              }
              archiveArtifacts artifacts: 'build/*.tar.gz', onlyIfSuccessful: true
            }
          }
        }
        stage('docker build') {
          gitlabCommitStatus('docker build') {
            container('docker') {
              String tag = 'latest'
              if (buildType == 'snapshot') {
                tag = 'snapshot'
              } else if (buildType == 'release') {
                def packageVersion = sh returnStdout: true, script: 'awk -F\'"\' \'/"version": ".+"/{ print $4; exit; }\' package.json'
                tag = packageVersion.trim()
              }
              sh """
              docker build -t gcr.io/aiops-224805/aiq-webapp:${tag} -f Dockerfile.single .
              docker push gcr.io/aiops-224805/aiq-webapp:${tag}
              """
            }
          }
        }
        stage('deploy dev') {
          gitlabCommitStatus('deploy dev') {
            if (buildType == 'feature') {
              container('kubectl') {
                sh """
                kubectl rollout restart deployment aiq-webapp --context=pt-sandbox
                kubectl rollout status deployment aiq-webapp --context=pt-sandbox
                """
              }
            }
          }
        }
        stage('deploy stage') {
          gitlabCommitStatus('deploy stage') {
            if (buildType == 'snapshot') {
              container('kubectl') {
                sh """
                kubectl rollout restart deployment aiq-webapp --context=kai-stage-gcp
                kubectl rollout status deployment aiq-webapp --context=kai-stage-gcp
                """
              }
            }
          }
        }
      }
    }
    currentBuild.result = 'SUCCESS'
  } catch (e) {
    currentBuild.result = 'FAILURE'
    echo "${e}"
  }
}
