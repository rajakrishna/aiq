# Changelog

All notable changes to this project will be documented in this file. This project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- Added H2O Java and Python servers to deployment form
- Added CPU and Memory usages to deployment
- Added CPU and Memory charts to deployments, notebooks and workflows
- Added Metrics to deployments
- Added Sidebar Navigation

### Changed

### Removed

### Fixed
- Fixed deployments page reload issue
- Fixed duplicate API calls in home page
- Fixed projects page scrolling issue and projects dropdown styling

## [0.8.1] - 2023-03-14

### Fixed
- Parameters getting deleted when workflow is updated

## [0.8.0] - 2023-01-25

### Added
- List unused volumes while creating/updating Notebooks
- Support for webhook notifications in Workflows
- Capability to update CPU, GPU and Memory of existing Notebooks
- Validations on Secret name and keys
- Helm chart for deployments

### Changed
- Updates to schedule form in Workflows
    - Remove periodic options from schedule form
    - Use cron expression for scheduling the workflows
    - Provide suspend option to enable/disable scheduling
    - Disable Run button when scheduling is enabled for a workflow
- Removed the project link from the model versions table in the model details page.

### Fixed
- Fixed navigating issue with Add Secrets links (git secrets, image pull secrets and docker push secrets) in workflow form.
- Fixed secrets list updation issue on adding new secret to the project.
- Fixed 'Generic' type of secrets keys hard coded issue.

## [0.7.2] - 2022-12-13

### Fixed

- Prompt for parameters when submitting a Job


## [0.7.1] - 2022-12-12

### Fixed

- Fix for blank pages when the user has no projects
- Disable create options when no project is selected


## [0.7.0] - 2022-12-08

### Added

- US14: Global Project Selection
- US34: Global Navigation
- US110: Resource Limitations on Notebooks based on Subscription
- US14: Global Project Selection
- US18: Navigation Enhancements - Project Details Page
- US142: Navigation Enhancements - Project Details Page Continued
- US132: Navigation Enhancements - Datasets Page
- US134: Navigation Enhancements - Applications
- US250: Show dependencies in Registered Model Version page
- US274: Add Confirmation pop for delete user in project details page
- US273: Added Confirmation message for delete secret in project details page
- US127: Limits on Notebook storage
- US92: Page for the user to upload a dataset (implementation)
- US52: List batch scoring runs
- US53: Download scoring results
- US147: Search and Filters in batch scoring runs
- US220: Show session expiry message before redirecting to Sign In page
- US111: Configuration changes for Notebook sizes (align with GCP VM sizes)
- US269: Option to restart/stop/start a deployment
- US182: Sign-In page should ask for tenant
- US336: Add overview tab in project details page
- Added search to Global Project Selection
- Show allocated GPUs in the Notebooks list table
- Added Workflow Parameters

### Changed

- US20: Navigation Enhancements - Experiment Details page
- US13: Navigation Enhancements - Notebooks List
- US81: Navigation Enhancements - Notebooks Form
- US21: Navigation Enhancements - Model Catalog Landing Page
- US22: Navigation Enhancements - Model Details Page
- US23: Navigation Enhancements - Model Version Details Page
- US25: Navigation Enhancements - Deployments Landing Page
- US24: Navigation Enhancements - Model Import Page
- US31: Navigation Enhancements - Run History
- US26: Navigation Enhancements - Deployment Creation Page
- US114: Project creation modal enhancements
- US167: Readable Alert for users with proper reason for re-registering with same email
- US57 Update Secrets form to include predefined keys based on type
- US133: Navigation Enhancements - Changes to Monitoring
- US195: Disable Project team management options if the User is not the owner of the Project
- US272: Navigation Enhancements - Workflows
- US168: Post Register Page with sign-in steps
- US172: Email validation error message should not show up immediately as the user starts typing
- US171:Delete option on Project card should NOT be available if the User is not the Project Owner
- US221: Redirect the User to original url after Relogin
- Auto add TenantAdmins to a Project when it is created

### Fixed

- US33: Library icons on Project Card should not wrap to next line
- US115: User Avatars on Project Card should not wrap
- US113: Name on Project Card should not wrap
- US122: Remove duplicate API calls on app launch
- DE4: Navigation menu items are not set to active when viewing Model Version Details page
- DE9: Fix app crashes when there are no projects for the user
- DE8: S3 Key Validattion
- DE29: Secrets API is calling continuously
- D21: Register page not available after register user
- D22: Remove Add filters button in model catalog
- DE19:Secrets list are not updating in project details page After create
- D20: Clear input fields after creating secrets
- DE28: Unable to view/edit newly created workflows
- US170: Reverting Button Text to Original Text
- DE6: Application run issue fixed
- D26 Fix app crashes when there are no projects for the user
- Fixed Navigation issues in model details page
- Fixed tabs issues in model details page
- US170: Reverting Button Text to Original Text
- DE6: Application run issue fixed
- US276: Minor enhancements on Notebooks page
- DE15: Do not show email validation error while it is being typed
- US337: Fix Project hyperlinks in all pages
- Fixed the 'react-joyride' version to 'v2.5.2' as further versions not supporting.
- Fixed bug secrets not added to deployments.
- Fixed incorrect display of duration of run in the run history page.
- Fix a bug where extraResources was being sent as `0`
- Fixed bugs found in workflow parameterization.

## [0.6.1] - 2022-11-05

### Fixed

- Git URL validation in workflows to allow URLs not ending in .git

## [0.6.0] - 2021-11-12

### Added

- Notebooks in Project details
- Workflows in Project details
- Publish option in Experiment details
- Deployment form
- Batch Scoring form
- Notebook Auth Token in Notebooks landing page
- Notebook delete confirmation

### Changed

- Default images list in create notebook form
- Update empty state messages to remove word "Sorry"
- Default page from home to projects
- Changed REST API endpoints for deployment


### Removed

- Removed top bar navigation
- Deploy option from Experiment details

### Fixed

- Deleting a deployment is routing to global deployments page instead of project deployment page
- Refresh button does not do anything in deploymnet list page
- Version column is empty (no value is shown) in deploymnet list page
- Available Replicas is empty (no value is shown) in deploymnet list page
- Available replicas is empty (should be a number - 0 or more) in deploymnet details page
- Model link is incorrect (should show N/A if modelId is not available) in deploymnet details page
- Run is empty (should show N/A if mlworkflowrun is not available) in deploymnet details page
- Added user tier validation in notebook creation page


## [0.5.0] - 2021-04-24

### Added

- Deployment Visualization
- Streaming Logs
- Export Workflow as JSON and YAML
- Validation to S3 source in Job Form
- Option to edit table view in experiments page
- Tooltip on the action button in Experiment List Page
- SPARK sub type in Job Form
- New input fields in Source Section of Job Form
- Errors dropdown on Experiment Details page
- Progress Icon in for download
- Cancel method for ongoing downloads
- Log Loss in Experiment Details
- Experiment Comparison
- Error message for workflow run with no step
- Table view for Requirements, Source Code and Runtime in experiment detail page.
- Section to update Requirements in experiment details page.
- Functionality to fetch dependencies and runtime from experiment in Import Model Form

### Changed

- Unit for CPUs from Milis to Cores in Job Form
- Workflow breadcrumb
- Dataset API calling methods

### Fixed

- Source object not getting cleared after changing the Source Type
- Requirements and Runtime are not getting populated after selecting Model as source type
- Import form not populating S3 data
- US173:Fixed Welcome tour issue

## [0.4.0] - 2020-05-19

### Added

- Job Form
- Job Listing
- Job Detail Page
- Navigation
- Model Catalog Page
- Import Model Form
- Deployment page
- Deployment Scaling
- Guided Walkthrough for new users
