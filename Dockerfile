FROM node:lts as build

WORKDIR /webapp
ADD ["package.json", "yarn.lock", "./"]

RUN yarn install

ADD [".", "."]
RUN NODE_ENV='production' yarn build:prod


FROM nginx:stable-alpine

COPY --from=build ./webapp/build /usr/share/nginx/html
