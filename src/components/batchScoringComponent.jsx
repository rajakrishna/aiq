import React, { Component } from "react";
import { connect } from "react-redux";
import BatchScoringFormComponent from "./batchScoringFormComponent";
import BatchScoringRunListComponent from "./batchScoringRunListComponent";

class BatchScoringComponent extends Component {
  render() {
    return (
      <div>
        <div className="m-info-table pl-4">
          <BatchScoringFormComponent deployment={this.props.deployment} />
          <BatchScoringRunListComponent deployment={this.props.deployment} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
  };
};

export default connect(mapStateToProps)(BatchScoringComponent);
