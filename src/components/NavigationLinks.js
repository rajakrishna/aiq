import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import logo from "../images/predera-logo.png";
import User from "./shared/User";
import sample_company_logo from "../images/company.svg";
import { toast } from "react-toastify";
import { listenNotifications, listenEvents } from "../api";
import notification_icon from "../images/notification-icon.svg";
import moment from "moment";
import { logout } from "../ducks/auth";
import {
  notificationsList,
  notificationUnreadCounts,
  markAsRead,
} from "../ducks/notifications";
import { getUserDetail } from "../ducks/users";
import { getProjectsSummary } from "../ducks/projects";
import { eventPosters } from "../ducks/common";
import Mixpanel from "mixpanel-browser";
import { l } from "../utils/helper";
import { Badge } from "@material-ui/core";
import SecretModalFullScreen from "./modals/SecretModalFullScreen";
import ExpiryModalFullScreen from "./modals/ExpiryModalFullScreen";
import { globalProjectSelection } from "../ducks/projects";
import Button from "@material-ui/core/Button";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import ProjectSearchDropdownNew from "./projectSearchDropdownNew";
import ReactTooltip from "react-tooltip";
import ProjectModalFullScreen from "./modals/projectModalFullScreen";

const theme = createMuiTheme({
  overrides: {
    MuiSelect: {
      select: {
        paddingLeft: "20px !important",
        maxWidth: "100px",
      },
      icon: {
        color: "#3D69F9",
      },
    },
    MuiMenuItem: {
      root: {
        "&$selected": {
          backgroundColor: "#e6eefc !important",
          color: "blue !important",
        },
      },
    },
  },
  typography: {
    useNextVariants: true,
    fontFamily: "inherit",
    letterSpacing: "inherit",
    button: { textTransform: "none", fontSize: [16, "!important"] },
  },
});

class NavigationLinks extends Component {
  state = {
    builtMenu: null,
    notifications_count: 0,
    notifications: [],
    open: false,
    openDeploy: false,
    openBuild: false,
    openApplication: false,
    openAutomate: false,
    project: {},
    projectParams: {
      page: 0,
      nameContains: "",
    },
    projectId: 0,
  };

  paths = [
    // "/home",
    // "/projects",
    // "/experiments",
    // "/models",
    // "/deployments",
    // "/monitoring",
    // "/workflows",
    // "/run-history",
    // "/workflows/new",
    // "/notifications"
  ];

  promptToast = (msg) => {
    if (msg) {
      toast(<ul className="notification-list">{msg}</ul>, {
        position: toast.POSITION.TOP_RIGHT,
        autoClose: 60000,
        type: toast.TYPE.DEFAULT,
        closeButton: false,
        closeOnClick: true,
      });
    } else {
      toast.info("You have a new notification", {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: 60000,
        closeButton: false,
      });
    }
  };

  loadNotifications = async (notificationData) => {
    this.props.notificationsList({
      sort: "created_date,desc",
      size: 20,
    });
    this.props.notificationUnreadCounts();
  };

  checkIfRecent = (date, sec) => {
    const date1 = new Date(date);
    const date2 = new Date();
    if ((date2.getTime() - date1.getTime()) / 100 < sec) {
      return true;
    }
    return false;
  };

  componentDidUpdate(prevProps) {

    if (this.props.user && this.props.user.userName && !this.enabledListener) {
      this.enabledListener = 1;
      listenNotifications(this.props.user.userName, this.loadNotifications);
      listenEvents(this.props.user.userName, this.props.eventPosters);
    }
    if (
      this.props.notifications &&
      prevProps.notifications &&
      this.props.notifications[0] &&
      prevProps.notifications[0] &&
      prevProps.notifications[0].id !== this.props.notifications[0].id
    ) {
      if (this.checkIfRecent(this.props.notifications[0].created_date, 60)) {
        this.promptToast(this.renderNotification(this.props.notifications[0]));
      }
    }
  }

  componentDidMount() {
    this.props.getUserDetail();
    this.loadNotifications();
    if (!this.props.selectedProject.name) {
      this.props.getProjectsSummary(this.state.projectParams);
    }
  }

  markAsRead = (data) => {
    let notificationId = typeof data === "object" ? data : [data];
    if (notificationId.length > 1) {
      const tempData = data.map((e) => {
        if (!e.read) return e.id;
      });
      notificationId = tempData.filter((e) => e);
    }
    if (!notificationId.length) {
      return;
    }
    this.props.markAsRead({ ids: notificationId });
    this.loadNotifications();
  };

  getClass = (pathname, currentPath) => {
    if (pathname !== currentPath && pathname.includes(currentPath + "/")) {
      return "nav-action nav-menu-item active";
    } else if (
      pathname === currentPath ||
      ((pathname === "" || pathname === "/") && currentPath === "/home")
    ) {
      return "nav-action nav-menu-item active";
    } else {
      return "nav-action nav-menu-item";
    }
  };

  getLink = (id, type) => {
    switch (type) {
      case "MLWorkflow":
        return "/workflows/" + id;
      case "MLWorkflowRun":
        return "/run-history/" + id;
      case "RegisteredModelVersion":
        return "/registered-model-versions/" + id;
      case "Project":
        return "/projects/" + id;
      case "Insight":
      default:
        return "/monitoring/" + id;
    }
  };


  handleToggleHome = () => {
    this.props.history.push("/projects");
  };

  handleToggleDeploy = () => {
    this.setState((state) => ({ openDeploy: !state.openDeploy }));
  };
  handleCloseDeploy = (event) => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }
    this.setState({ openDeploy: false });
  };
  handleCloseDeployDeployments = (event) => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }
    this.setState({ openDeploy: false });
    this.props.history.push("/deployments");
  };
  handleCloseDeployModelCatalog = (event) => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }
    this.setState({ openDeploy: false });
    this.props.history.push("/models");
  };
  handleCloseDeployMonitoring = (event) => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }
    this.setState({ openDeploy: false });
    this.props.history.push("/monitoring");
  };

  handleToggleBuild = () => {
    this.setState((state) => ({ openBuild: !state.openBuild }));
  };
  handleCloseBuild = (event) => {
    if (this.builtMenu.contains(event.target)) {
      return;
    }
    this.setState({ openBuild: false });
  };
  handleCloseBuildNotebooks = (event) => {
    if (this.builtMenu.contains(event.target)) {
      return;
    }
    this.setState({ openBuild: false });
    this.props.history.push("/notebooks");
  };
  handleCloseBuildExperiments = (event) => {
    if (this.builtMenu.contains(event.target)) {
      return;
    }
    this.setState({ openBuild: false });
    this.props.history.push("/experiments");
  };
  handleCloseBuildDatasets = (event) => {
    if (this.builtMenu.contains(event.target)) {
      return;
    }
    this.setState({ openBuild: false });
    // not avialble in app.js
    this.props.history.push("/datasets");
  };

  handleToggleApplications = () => {
    this.setState((state) => ({ openApplication: !state.openApplication }));
    this.props.history.push("/applications");
  };

  handleToggleAutomate = () => {
    this.setState((state) => ({ openAutomate: !state.openAutomate }));
  };
  handleCloseAutomate = (event) => {
    if (this.automateMenu.contains(event.target)) {
      return;
    }
    this.setState({ openAutomate: false });
  };
  handleCloseAutomateWorkflows = (event) => {
    if (this.automateMenu.contains(event.target)) {
      return;
    }
    this.setState({ openAutomate: false });
    this.props.history.push("/workflows");
  };
  handleCloseAutomateRunHistory = (event) => {
    if (this.automateMenu.contains(event.target)) {
      return;
    }
    this.setState({ openAutomate: false });
    this.props.history.push("/run-history");
  };

  renderNotification = (notification, key = new Date().getTime()) => {
    return (
      <li className="notification-item" key={key}>
        <span className="notification-icon">
          <img
            src={notification_icon}
            data-tip=""
            data-for={`mark-as-read-${key}`}
            alt="notification icon"
          />
        </span>
        <span className="notification-msg">
          <Link
            to={this.getLink(
              notification.entity_id,
              notification.reference_entity
            )}
            onClick={() => this.markAsRead(notification.id)}
          >
            <h6 style={notification.read ? { fontWeight: "normal" } : {}}>
              {notification.message}
            </h6>
          </Link>
          <time>{moment(notification.created_date).format("LLL")}</time>
        </span>
      </li>
    );
  };

  render() {
    const { openApplication } = this.state;

    const { pathname } = this.props.location;
    const {
      user,
      notifications,
      notifications_count,
      workflowCounts,
      projectCounts,
      modelCounts,
      experimentCounts,
    } = this.props;
    const counter = {
      "/workflows": workflowCounts,
      "/projects": projectCounts,
      "/models": modelCounts,
      "/experiments": experimentCounts,
    };
    return (
      <div className="navbar navbar-expand-md navbar-default main-nav">
        <div className="container-fluid">
          <div className="collapse navbar-collapse justify-content-between" id="navbarCollapse">
            <Link to="/projects" className="navbar-brand">
              <img src={logo} alt="" />
            </Link>
            <div className="menu-bar d-flex">
              <MuiThemeProvider theme={theme}>
                <div className="d-inline">
                  <ProjectSearchDropdownNew />
                </div>
                <Button
                  style={{
                    border: "none",
                    outline: "none",
                    backgroundColor: this.props.location.pathname.includes(
                      "applications"
                    )
                      ? "#e6eefc"
                      : null,
                  }}
                  aria-owns={openApplication ? "menu-list-grow" : undefined}
                  aria-haspopup="true"
                  color={
                    this.props.location.pathname.includes("applications")
                      ? "primary"
                      : null
                  }
                  onClick={this.handleToggleApplications}
                  className="mr-2"
                >
                  Applications
                </Button>
              </MuiThemeProvider>
            </div>
            {this.paths.length > 0 && 
              <ul className="nav navbar-nav" style={{ paddingTop: 7 }}>
                {this.paths.map((e, i) => {
                  if (i < 5)
                    return (
                      <li key={"nav" + i} className={this.getClass(pathname, e)}>
                        <Link
                          to={e}
                          onClick={() => {
                            Mixpanel.track(l[pathname]);
                          }}
                        >
                          {l[e]}
                          {counter[e] ? (
                            <span className="badge badge-pill badge-secondary ml v-middle">
                              {counter[e]}
                            </span>
                          ) : (
                            ""
                          )}
                        </Link>
                      </li>
                    );
                })}
                {this.paths.indexOf(pathname) >= 5 ? (
                  <li
                    key={"hidden-path" + pathname}
                    className="nav-action nav-menu-item active"
                  >
                    <Link
                      to={pathname}
                      onClick={() => {
                        Mixpanel.track(l[pathname]);
                      }}
                    >
                      {l[pathname]}
                    </Link>
                  </li>
                ) : (
                  ""
                )}
              </ul>
            }
            <ul className="nav navbar-nav">
              {process.env.REACT_APP_COMPONENT_NOTEBOOKS != "DISABLED" && (
                <li className="nav-action">
                  <Link to="/notebooks">
                    <i className="nav-icon notebooks-icon" />
                  </Link>
                </li>
              )}
              <li
                className="nav-action dropdown mt-sm"
                onClick={() => {
                  toast.dismiss();
                  this.markAsRead(notifications);
                }}
              >
                <a data-toggle="dropdown">
                  <Badge color="secondary" badgeContent={notifications_count}>
                    <i className="fa fa-bell" style={{ fontSize: "1.2em" }}></i>
                  </Badge>
                </a>
                <div
                  className="dropdown-menu dropdown-arrow dropdown-menu-right notification-dropdown"
                  aria-labelledby="dropdownMenuButton"
                >
                  <div className="notification-wraper">
                    <ul className="notification-list">
                      {notifications.length !== 0 ? (
                        notifications.map((notification, key) => {
                          return this.renderNotification(notification, key);
                        })
                      ) : (
                        <li className="notification-item">
                          <div className="pd-tb text-gray content-center f-12">
                            No notifications!
                          </div>
                        </li>
                      )}
                    </ul>
                    <hr className="m-0" />
                    {notifications.length ? (
                      <div className="content-center pd-tb">
                        <Link to="/notifications">
                          See all notifications ({notifications_count})
                        </Link>
                      </div>
                    ) : (
                      ""
                    )}
                  </div>
                </div>
              </li>
              <li className="nav-action dropdown mt-sm pt-xs">
                <a data-toggle="dropdown">
                  <i className="fa fa-plus" style={{ fontSize: "1.2em" }} />
                </a>
                <div
                  className="dropdown-menu dropdown-arrow dropdown-menu-right no-select"
                  aria-labelledby="dropdownMenuButton"
                >
                  <span>
                    <ProjectModalFullScreen dropdown={"dropdown"} />
                  </span>
                  <Link to="/workflows/new" 
                  className={`dropdown-item clickable ${!this.props.selectedProject ? "disabled":""}`}
                  data-tip data-for="no_projects_workflow"
                  onClick={e =>!this.props.selectedProject && e.preventDefault()}
                  >
                    New Workflow
                  </Link>
                  { !this.props.selectedProject &&
                    <ReactTooltip 
                      id="no_projects_workflow"
                        aria-haspopup="true"
                        className="custom-tooltip"
                      >
                        <h6>You have no projects</h6>
                          
                    </ReactTooltip>
                  }
                  <SecretModalFullScreen selectedProject={this.props.selectedProject} secretModelParent="headerNavigation"/>
                  { !this.props.selectedProject &&
                          <ReactTooltip 
                            id="no_projects_secret"
                              aria-haspopup="true"
                              className="custom-tooltip"
                           >
                              <h6>You have no projects</h6>
                               
                          </ReactTooltip>
                  }
                  <Link to="/models/new"
                   className={`dropdown-item clickable ${!this.props.selectedProject ? "disabled":""}`}
                   data-tip data-for="no_projects_model"
                   onClick={e =>!this.props.selectedProject && e.preventDefault()}
                   >
                    Import Model
                  </Link>
                  { !this.props.selectedProject &&
                          <ReactTooltip 
                            id="no_projects_model"
                              aria-haspopup="true"
                              className="custom-tooltip"
                           >
                              <h6>You have no projects</h6>
                               
                          </ReactTooltip>
                  }
                </div>
              </li>
              <li className="nav-divider" />
              <li className="dropdown nav-dropdown profile-links-dropdown">
                <a
                  href=""
                  className="nav-link"
                  id="dropdownMenuButton"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <User user={user.userName} isMenu={1} />
                </a>
                <div
                  className="dropdown-menu dropdown-arrow dropdown-menu-right"
                  aria-labelledby="dropdownMenuButton"
                >
                  <Link className="dropdown-item" to="/profile">
                    <i className="fa fa-user" /> Profile
                  </Link>
                  <Link to="/help" className="dropdown-item">
                    <i className="fa fa-question-circle" /> Help
                  </Link>
                  <Link
                    to=""
                    className="dropdown-item"
                    onClick={this.props.logout}
                  >
                    <i className="fa fa-sign-out" /> Logout
                  </Link>
                </div>
              </li>
              {user.role === "TenantAdmin" && <li className="nav-divider" />}
              {user.role === "TenantAdmin" && (
                <li className="company-logo">
                  <Link to="/company">
                    <img src={sample_company_logo} alt="" />
                  </Link>
                </li>
              )}
            </ul>
          </div>
        </div>
        <ExpiryModalFullScreen />
        <input style={{ opacity: 0, position: "absolute", zIndex: -1 }} />
        <input
          type="password"
          style={{ opacity: 0, position: "absolute", zIndex: -1 }}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    user: state.users.user,
    notifications: state.notification.notifications,
    notifications_count: state.notification.notifications_unread_count,
    workflowCounts: state.jobs.counter,
    modelCounts: state.models.registered_model_counter,
    experimentCounts: state.experiments.counter,
    projectCounts: state.projects.counter,
    projects: state.projects.summary,
    selectedProject: state.projects.globalProject,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators(
      {
        logout,
        getUserDetail,
        notificationsList,
        notificationUnreadCounts,
        markAsRead,
        eventPosters,
        globalProjectSelection,
        getProjectsSummary,
      },
      dispatch
    ),
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(NavigationLinks)
);
