import React, { Component, createRef } from "react";
import { Tooltip } from "@material-ui/core";
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';

export class DeploymentGraph extends Component {
    state = {
        arrows: [],
        graph: [],
        zoomX: 1,
        drawer: false,
        drawerDetails: {}
    }
    step = [];
    depth = -1;
    arrowArr = [];
    timelineContainer = createRef();

    componentDidMount() {
        const { deployment } = this.props;
        const graph = deployment.spec.predictors.map(e => {
            return e.graph
        });
        this.setState({
            graph: this.drawGraph(graph)
        }, this.drawArrows())
        /* Event Listener to enable Zoom In */
        // this.timelineContainer.current.addEventListener('wheel', this.zoomEvent, true);
    }

    componentWillUnmount() {
        /* Event Listener to enable Zoom In */
        // this.timelineContainer.current.removeEventListener('wheel', this.zoomEvent, true);
    }

    zoomEvent = (e) => {
        let zX = this.state.zoomX;
        var dir;
        dir = (e.deltaY > 0) ? 0.1 : -0.1;
        zX += dir;
        zX = zX >= 0.3 ? zX : 0.3;
        zX = zX <= 1 ? zX : 1;
        this.setState({
            zoomX: zX
        })
        e.preventDefault();
        return;
    }

    drawGraph = (data, depth = 0, width = 0) => {
        this.depth = depth;
        if(!data || (data && !data.length)) return;
        width = width > 0 ? width+1 : width;
        for(let i=0; i<data.length; i++) {
            const e = data[i];
            this.arrowArr.push({
                id: "a"+(i+"-"+(width > 0 ? width - 1 : width))+"-dep-"+depth,
                children: e.children && e.children.map((e,j)=>{
                    return("a"+(j+"-"+i)+"-dep-"+(depth+1));
                })
            })
            this.step.push(
                <div 
                    key={(i+"-"+(width > 0 ? width - 1 : width))+"-dep-"+depth} 
                    id={"a"+(i+"-"+(width > 0 ? width - 1 : width))+"-dep-"+depth} 
                    className="step-container clickable deploy-step" 
                    style={{top: depth * 160, left: (width+i)*300}}
                    onClick={()=>{
                        this.setState({
                            drawer: true,
                            drawerDetails: e
                        })
                    }}
                >
                    
                    <table className="details-table">
                        <tbody>
                            <tr>
                                <td className="pd-lr-sm">
                                    <Tooltip title={e.name}>
                                        <span>{e.name}</span>
                                    </Tooltip>
                                </td>
                            </tr>
                            <tr>
                                <td className="pd-lr-sm f-12">({e.type || e.implementation})</td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            );
            if(e.children && e.children.length) {
                this.drawGraph(e.children, (depth+1), i);
            }
        }
        return this.step;
    }

    drawArrows = () => {
        setTimeout(()=>{
        const arrows = this.arrowArr.map(e => {
            if(!e.children) return;
            const div1 = document.querySelector("#"+e.id);
            return e.children.map((m)=>{
                const div2 = document.querySelector("#"+m);
                const div1x = div1.offsetLeft + (div1.offsetWidth/2);
                const div1y = div1.offsetTop + (div1.offsetHeight/2);
                const div2x = div2.offsetLeft + (div2.offsetWidth/2);
                const div2y = div2.offsetTop + (div2.offsetHeight/2);

                /* Equation for curved Lines */
                // const mpx = (div2x + div1x) * 0.5;
    			// const mpy = (div2y + div1y) * 0.5;
    
    			// angle of perpendicular to line:
    			// const theta = Math.atan2(div2y - div1y, div2x - div1x) - Math.PI / 2;
    
    			// distance of control point from mid-point of line:
    			// const offset = 30;
    
    			// location of control point:
    			// const c1x = mpx + offset * Math.cos(theta);
                // const c1y = mpy + offset * Math.sin(theta);
                
                // const d = "M" + div1x + " " + div1y + " Q " + c1x + " " + c1y + " " + div2x + " " + div2y;

                return(
                    <line
                        key={"a"+e.id+m}
                        className="line-svg blue-line" 
                        x1={div1x}
                        y1={div1y}
                        x2={div2x} 
                        y2={div2y}
                    />
                    /* Use this for curved line */
                    // <path 
                    //     key={"a"+e.id+m} 
                    //     d={d}
                    //     stroke="#3aa6d1" 
                    //     stroke-width="3" 
                    //     stroke-linecap="round" 
                    //     fill="transparent"
                    // />
                )
            })
        });
        this.setState({
            arrows: arrows.flat()
        })
        },10)
    }

    render() {
        const { arrows, graph, zoomX, drawer, drawerDetails } = this.state;
        return(
            <div ref={this.timelineContainer}>
                <div 
                    className="timeline-section" 
                    style={{ 
                            minHeight: 452,
                            height: (100+160) * (this.depth < 1 ? 0.6 : this.depth), 
                            transform: "scale("+zoomX+")"
                        }}
                >
                    <svg className="svg">
                        {arrows}
                    </svg>
                    <div className="relative text-center mb-lg">
                        { graph }
                    </div>
                </div>
                <SwipeableDrawer 
                    anchor={"right"}
                    open={drawer}
                    onClose={()=>{this.setState({drawer: false})}}
                    onOpen={()=>{this.setState({drawer: true})}}
                >
                    <div className="pd-lg">
                        <h4>Details</h4>
                        <table className="no-border monospace">
                            <tbody>
                                <tr>
                                    <td className="pd-lr">
                                        Name:
                                    </td>
                                    <td className="pd-lr">
                                        {drawerDetails.name}
                                    </td>
                                </tr>
                                <tr>
                                    <td className="pd-lr">
                                        Type:
                                    </td>
                                    <td className="pd-lr">
                                        {drawerDetails.type || drawerDetails.implementation}
                                    </td>
                                </tr>
                                {drawerDetails.endpoint &&
                                <tr>
                                    <td className="pd-lr">
                                        Endpoint:
                                    </td>
                                    <td className="pd-lr">
                                        {drawerDetails.endpoint.type}
                                    </td>
                                </tr>}
                            </tbody>
                        </table>
                        {drawerDetails.parameters &&
                        <div>
                            <hr />
                            <h5>Parameters</h5>
                            {drawerDetails.parameters.map((e,i)=>{
                                return(
                                    <div className="pd-sm" key={"parameterTable"+i}>
                                        <table className="no-border monospace">
                                            <tbody >
                                                <tr>
                                                    <td className="pd-lr">
                                                        Name:
                                                    </td>
                                                    <td className="pd-lr">
                                                        {e.name}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td className="pd-lr">
                                                        Type:
                                                    </td>
                                                    <td className="pd-lr">
                                                        {e.type}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td className="pd-lr">
                                                        Value:
                                                    </td>
                                                    <td className="pd-lr">
                                                        {e.value}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <hr />  
                                    </div>
                                )
                            })}
                        </div>}
                    </div>
                </SwipeableDrawer>
            </div>
        )
    }
}
