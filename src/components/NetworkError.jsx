import React, { Component } from 'react';
import NavigationLinks from './NavigationLinks';

class NetworkError extends Component {
  render() {
    return(
      <div className="main-container">
      <NavigationLinks path={this.props.match.path}/>
        <div className="main-body">
          <div>
            <h1>Network Error :-( </h1>
          </div>
        </div>
      </div>
    )
  }
}

export default NetworkError;
