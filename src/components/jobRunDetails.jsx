import React, { Component } from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getJobRun } from "../api";
import { alertMsg } from "../ducks/alertsReducer";
import { capitalizeVars } from "../utils/helper";

class JobRunDetails extends Component  {

    steps = [
        "pre_train_steps",
        "train",
        "post_train_steps",
        "pre_validate_steps",
        "validate",
        "post_validate_steps",
        "pre_deploy_steps",
        "deploy",
        "post_deploy_steps"
    ]

    state = {
        runDetails: null,
        arrows: [],
        activeStep: ''
    }

    componentDidMount() {
        this.getJobRun()
    }

    setActiveStep = (activeStep) => {
        this.setStat({
            activeStep
        })
    }

    getJobRun = () => {
        const steps = [...this.steps];
        this.steps = [];
        getJobRun(this.props.token, this.props.match.params.id)
        .then(res => {
            if(res.status === 200) {
                const runDetails = res.data;
                this.steps = steps.filter(e => {
                    if(runDetails.spec[e]) return e;
                })
                this.setState({
                    runDetails,
                    activeStep: this.steps[0]
                },()=>{setTimeout(()=>{this.renderSteps()})})
            }
        })
        .catch(err => {
            this.props.alertMsg("Network Error, please try again!","error")
        })
    }

    renderSteps() {
        const { runDetails } = this.state;

        const arrows = runDetails && this.steps.map((e,i)=>{

            return [];
        })
    }

    render() {
        const { runDetails, arrows, activeStep } = this.state;
        return(
            <div className="main-container">
                <div className="main-body" style={{overflow: "hidden", paddingTop: 72}}>   
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-sm-12">
                                <div style={{height: 'calc(100vh - 126px)'}}>
                                    <div className="timeline-section">
                                        <svg className="svg">
                                            {arrows}
                                        </svg>
                                        {
                                            runDetails && this.steps.map((elem, index)=>{
                                                if((/pre|post/gi).test(elem)) {
                                                    return(
                                                        <div key={elem} className={"relative text-center mb-lg"} style={{minWidth: 400}}>
                                                            <div id={elem} className="relative step-container">
                                                                <div className="form-step" style={{background: "#f1f2f1"}}>
                                                                </div>
                                                                <span className="f-12">{capitalizeVars(elem.replace(/\d/g,""))}</span>
                                                            </div>
                                                        </div>
                                                    )
                                                } else {
                                                    return(
                                                        <div key={elem+index} className={"relative text-center mb-lg"} style={{minWidth: 400}}>
                                                            <div id={elem} className="relative">
                                                            <div 
                                                                    className={"clickable form-step"} 
                                                                    aria-selected={activeStep === elem} 
                                                                    onClick={(event)=>{
                                                                        this.setActiveStep(elem)
                                                                    }}
                                                                >
                                                                    {/* <JobForm key={elem+i} jobtype={elem} /> */}
                                                                    <div className="content-center">
                                                                        {/* <ShowDetails name={elem} /> */}
                                                                        {elem}
                                                                    </div>
                                                                </div>
                                                                <span className="f-12">{capitalizeVars(elem)}</span>
                                                            </div>
                                                        </div>
                                                    )
                                                }
                                            })
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

const mapStateToProps = state => {
    return {
        token: state.auth.token,
        user: state.users.user
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            alertMsg
        },
        dispatch
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(JobRunDetails);