import React from 'react';
import { Link } from "react-router-dom";
import { codes } from "./exampleCode";
import { CopyPre } from "./CopyPre";

export default function LogMonitor(props) {
    return(
      <div className="pd">
        <h4>Monitor Deployed Models</h4>
        <p>
          Import and initialize Predera Agent in your code to automatically log predictions
          and monitor your Model.
        </p>
        <CopyPre code={codes.monitorModel.importCode}>
          <pre>
            {codes.monitorModel.importCode}
          </pre>
        </CopyPre>
        <p>
          <b>Note</b> that AiqAgent must be imported and initialized before any importing
          any ML library such as scikit, tensorflow, etc.
        </p>
        <p>
          <b>model_id</b> is the unique identifier of the deployed Model and can be found from
          the Model details page.
        </p>
        <p>
          Once the agent is imported, any inferences made in your code will be automatically
          sent to the server and used to determine model health, performance, data drifts,
          and bias in real-time.
        </p>
        <h5>Authentication</h5>
        <p>You will also have to set your credentials so the agent can successfully
          authenticate to the server. There are two ways of doing this.</p>
        <ol>
          <li>Set environment variables `AIQ_USERNAME` and `AIQ_PASSWORD`. For example, on mac/linux
              <CopyPre code={codes.logAnExp.envCode}>
                <pre>
                  {codes.logAnExp.envCode}
                </pre>
              </CopyPre>
          </li>
          <li>Set credentials in code
            <CopyPre code={codes.monitorModel.crendtialCode}>
              <pre>
                {codes.monitorModel.crendtialCode}
              </pre>
            </CopyPre>
          </li>
        </ol>
        <h5>Example</h5>
        <p>
          Below is a complete example<br/>
        </p>
        <CopyPre code={codes.monitorModel.exampleCode}>
          <pre className="mt-lg">
            {codes.monitorModel.exampleCode}
          </pre>
        </CopyPre>
        <p>
          As AiqAgent is imported and initialized in this example, all predictions are
          automatically sent to the server and can be seen in the Runtime section of the
          deployed model. Learn more about <Link to="#">Monitoring ML Models</Link>.
        </p>
      </div>
    )
}