export const codes = {
    installAgent: {
        downCmd: `$ pip install aiq-agent-<version>.tar.gz`
    },
    logAnExp: {
        exampleCode: 
`# Import and initialize the agent
from aiq_agent import AiqAgent
agent = AiqAgent(project_name='examples', model_name='digits-classifier')

# Import datasets, classifiers and performance metrics
from sklearn import datasets, svm, metrics
from sklearn.externals import joblib

# The digits dataset
digits = datasets.load_digits()

# To apply a classifier on this data, we need to flatten the image, to
# turn the data in a (samples, feature) matrix:
n_samples = len(digits.images)
data = digits.images.reshape((n_samples, -1))

# Create a classifier: a support vector classifier
classifier = svm.SVC(gamma=0.001, probability=True, kernel='linear')

# We learn the digits on the first half of the digits
classifier.fit(data[:n_samples // 2], digits.target[:n_samples // 2])

# Now predict the value of the digit on the second half:
expected = digits.target[n_samples // 2:]
with agent.test(test_target=expected):
    predicted = classifier.predict(data[n_samples // 2:])

print("Classification report for classifier %s:\\n%s\\n"
      % (classifier, metrics.classification_report(expected, predicted)))
print("Confusion matrix:\\n%s" % metrics.confusion_matrix(expected, predicted))`,
        envCode: 
`$ export AIQ_USERNAME=username
$ export AIQ_PASSWORD=password`,
        crendtialCode:
`from aiq_agent import AiqAgent
from aiq_agent.config import CONFIG
CONFIG.username = 'username'
CONFIG.password = 'password'
agent = AiqAgent(project_name='my-project', model_name='my-model')`,
        importCode:
`from aiq_agent import AiqAgent
agent = AiqAgent(project_name='my-project', model_name='my-model')`
    },
    monitorModel: {
        importCode:
`from aiq_agent import AiqAgent
agent = AiqAgent(model_id='8d56308af78e490ea978a0f4529e5b37')`,
        crendtialCode: 
`from aiq_agent import AiqAgent
from aiq_agent.config import CONFIG
CONFIG.username = 'username'
CONFIG.password = 'password'
agent = AiqAgent(model_id='<insert model id here>')`,
        exampleCode:
`# Import and initialize the Agent
from aiq_agent import AiqAgent
agent = AiqAgent(model_id='<insert model id here>')

# sklearn imports
from sklearn import datasets
from sklearn.externals import joblib
# The digits dataset
digits = datasets.load_digits()

# prep data
n_samples = len(digits.images)
data = digits.images.reshape((n_samples, -1))

# load the model
classifier = joblib.load('/mnt/repository/artifacts/digits-classifier-v0.1.pkl')

# make predictions
predicted = classifier.predict(data[n_samples // 2:])`
    }
}