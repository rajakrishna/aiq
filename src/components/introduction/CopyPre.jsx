import React, { Component, createRef } from "react";

export class CopyPre extends Component {
    state = {
        display: ""
    }
    timeout = "";
    mainContainer = createRef()

    showTool = () => {
        this.setState({
            display: 1
        }, () =>{
            clearTimeout(this.timeout);
            this.timeout = setTimeout(()=>{
                this.setState({
                    display: ""
                })
            }, 500);
        })
    }

    render() {
        return (
            <div className="relative" id="copier" ref={this.mainContainer}>
                <div className="copy-code">
                    {this.props.children}
                </div>
                <span className="clickable copy-btn mr" onClick={()=>{
                    const text = this.props.code;
                    const inp = document.createElement("textarea");
                    inp.value = text;
                    this.mainContainer.current.appendChild(inp);
                    inp.select();
                    document.execCommand("copy");
                    this.mainContainer.current.removeChild(inp);
                    this.showTool();
                }}>
                    <i className="fa fa-copy"></i>
                </span>
                {
                    this.state.display &&
                    <span className="tool">Copied!</span>
                }
            </div>
        )
    }
}