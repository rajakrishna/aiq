import React from "react";
import { Link } from "react-router-dom";
import { codes } from "./exampleCode";
import { CopyPre } from "./CopyPre";

export default function LogExperiment(props) {
  return (
    <div>
      <h4>Log an Experiment</h4>
      <p className="mt-lg">
        Import and initialize Predera Agent in your training code to
        automatically log the trained model and all its details as an
        Experiment.
      </p>
      <CopyPre code={codes.logAnExp.importCode}>
        <pre className="mt-lg">{codes.logAnExp.importCode}</pre>
      </CopyPre>
      <h5>Note:</h5>
      <ol>
        <li>
          {" "}
          <b>AiqAgent</b> must be imported and initialized before any ML
          libraries such as scikit, tensorflow are imported.
        </li>
        <li>
          {" "}
          <b>project_name</b> is mandatory. A new project with the given name
          will be created if it does not exist already. Learn more about{" "}
          <Link to="#">Collaborating on Projects</Link>.
        </li>
        <li>
          {" "}
          <b>model_name</b> is mandatory. A new experiment will be created with
          this name. If an experiment with the same name exists then a new
          version of it will be created.
        </li>
      </ol>
      <h5>Authentication</h5>
      <p>
        You will also have to set your credentials so the agent can successfully
        authenticate to the server. There are two ways of doing this.
      </p>
      <ol>
        <li>
          {" "}
          Set environment variables `AIQ_USERNAME` and `AIQ_PASSWORD`. For
          example, on mac/linux
          <CopyPre code={codes.logAnExp.envCode}>
            <pre>{codes.logAnExp.envCode}</pre>
          </CopyPre>
        </li>
        <li>
          Set credentials in code
          <CopyPre code={codes.logAnExp.crendtialCode}>
            <pre>{codes.logAnExp.crendtialCode}</pre>
          </CopyPre>
        </li>
      </ol>
      <h5>What information is logged?</h5>
      <p>
        Below details will be automatically extracted and logged as an
        Experiment.
      </p>
      <ul>
        <li>ML Library and Algorithm</li>
        <li>Hyperparameters</li>
        <li>Features</li>
        <li>Performance Metrics (based on the Algorithm)</li>
        <li>Datasets (training, test, validation)</li>
        <li>Metadata (training duration, created by, created date, etc)</li>
      </ul>
      <h5>Example</h5>
      <p>
        Below is a complete example that will train and log a classification
        model to identify hand written digits.
      </p>
      <CopyPre code={codes.logAnExp.exampleCode}>
        <pre className="mt-lg">{codes.logAnExp.exampleCode}</pre>
      </CopyPre>
      <div className="mt">
        <h5>Next Steps</h5>
        <div>
          <Link to="/Getting-Started/Deploy-model">Deploy a Model</Link>
        </div>
        <div>
          <Link to="/Getting-Started/Log-prediction">
            Monitor deployed Models
          </Link>
        </div>
      </div>
    </div>
  );
}
