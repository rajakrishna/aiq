import React from "react";
import { Link } from "react-router-dom";

export default function DeployModel(props) {
  return (
    <div>
      <h4>Deploy a Model</h4>
      <p className="mt-lg">
        Deploying a Model will give you a REST/gRPC API that can be used for
        inferencing.
      </p>
      <p>Follow below steps to deploy an existing Experiment.</p>
      <ul>
        <li>
          Select an <b>Experiment</b> that you want to deploy from{" "}
          <Link to="/experiments">Experiments page</Link>.
        </li>
        <li>
          Click on <b>Deploy</b> button. This will take you to{" "}
          <Link to="/workflows/new">Add workflow</Link> page where you can customize the
          deployment if required - for example, runtime dependencies,
          environment variables, compute resources such as CPU/GPU and Memory,
          etc.
        </li>
        <li>
          Click on <b>Save &amp; Run</b>. This will create the deployment workflow
          and run it once immediately.
        </li>
        <li>
          Once the workflow is completed successfully, the newly deployed model will
          show up in the <Link to="/models">Deployed Models</Link> section.
        </li>
        <li>
          You can view the deployment details, predictions endpoints and
          error/debug logs from the <b>Deployment</b> section on the{" "}
          <Link to="/models">Model details</Link> page.
        </li>
      </ul>
      <div className="mt">
        <h5>Next Steps</h5>
        <div>
          <Link to="/Getting-Started/Log-prediction">
            Monitor deployed Models
          </Link>
        </div>
      </div>
    </div>
  );
}
