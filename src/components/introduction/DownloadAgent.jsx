import React from "react";
import { Link } from "react-router-dom";
import { codes } from "./exampleCode";
import { CopyPre } from "./CopyPre";

export default function DownloadAgent(props) {
  return (
    <div>
      <h4>Install Agent</h4>
      <p className="mt-lg">
        Predera Agent automatically instruments into your code and logs
        experiments and predictions to help manage and monitor your ML models.
      </p>
      <p>
        Click on the Download button below to save the agent installation
        package on your computer.
      </p>
      <p>
        <button
          onClick={() => {
            props.download();
          }}
          className="btn btn-primary btn-sm btn-rounded"
        >
          Download
        </button>
        <span className="f-12">
          <Link to="/help"> Advanced install options &amp; other platforms</Link>
        </span>
      </p>
      <p>After downloading the agent, use pip to install it.</p>
      <CopyPre code={codes.installAgent.downCmd}>
        <pre className="mt-lg">{codes.installAgent.downCmd}</pre>
      </CopyPre>

      <div className="mt">
        <h5>Next Steps</h5>
        <div>
          <Link to="/Getting-Started/Log-experiment">
            Log and track Experiments
          </Link>
        </div>
        <div>
          <Link to="/Getting-Started/Deploy-model">
            Monitor deployed Models
          </Link>
        </div>
      </div>
    </div>
  );
}
