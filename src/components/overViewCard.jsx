import React, { Component } from 'react';
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

const styles = {
    card: {
      maxWidth: 250,
      "&:hover": {
        backgroundColor: "#f9fafb",
      },
  
    },
    title: {
      fontSize: 14
    },
    pos: {
      marginBottom: 12
    }
  };

class OverViewCard extends Component {
    constructor(props) {
        super(props);
    }
    state = {  }
    render() {
        const { classes } = this.props;

        return ( 
          <Card className={classes.card}>
            <CardContent>
             <Typography variant="h5" component="h2">
               {this.props.count}
             </Typography>
            <Typography className={classes.pos} color="textSecondary">
              {this.props.name}
            </Typography>
          </CardContent>
       </Card>
         );
    }
}

OverViewCard.propTypes = {
    classes: PropTypes.object.isRequired
  };
 
export default withStyles(styles)(OverViewCard);