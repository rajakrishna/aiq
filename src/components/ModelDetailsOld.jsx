import React, { Component } from 'react';
import NavigationLinks from './NavigationLinks';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getModelDetails } from '../ducks/modelDetails';
// import ModelFeatureSignificance from './chart/ModelFeatureSignificance';
// import ModelROCCurve from './chart/ModelROCCurve';
// import ModelLiftCurve from './chart/ModelLiftCurve';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import moment from 'moment';
import User from './shared/User';
// import ModelConfusionMatrix from './chart/ModelConfusionMatrix';
import Library from './shared/Library';
import hyper_parameters from '../images/hyper-parameters.png'
import model_performance_metrics from '../images/model-progress-bars.png'
import confusion_metrics from '../images/metrics.png'
// import significance from '../images/significance.png'
import roc_curve from '../images/curve.png'

class ModelDetails extends Component {
  state = {
  }

  componentDidMount() {
    this.props.getModelDetails(this.props.match.params.id);
  }

  percentFormat(value) {
    return `${(value *100).toFixed(1)}%`
  }

  render() {
    return(
      <div className="main-container bg-white">
        <NavigationLinks path={this.props.match.path}/>
        {/* <Sidebar path={this.props.match.path}/> */}
        <div className="main-body content-with-tab-nav">
          <div className="model-details-header clearfix">
            <div className="page-breadcrumb">
              <ul>
                <li><a href="/models">Models</a></li>
                <li className="active">{this.props.models.name}</li>
              </ul>
            </div>
            <div className="model-header-actions">
              <button type="button" className="btn btn-link btn-sm"><i className="fa fa-filter"></i> Filter</button>
              <button type="button" className="btn btn-link btn-link-danger btn-sm"><i className="fa fa-trash"></i> Delete</button>
            </div>
          </div>
          <div className="model-details-content">
            <Tabs>
              <div className="tab-navigation">
                <TabList className="tab-list">
                  <Tab>Details</Tab>
                  <Tab>Performance</Tab>
                  <Tab>Runtime</Tab>
                  <Tab>Data Drift</Tab>
                  <Tab>Settings</Tab>
                </TabList>
              </div>

              <TabPanel className="content-wraper model-details-wraper">
                <Tabs>
                  <TabList className="tab-list-inline">
                    <Tab>Model Details</Tab>
                    <Tab>Training Dataset</Tab>
                  </TabList>
                  <TabPanel>
                    <div className="tab-body">
                      <div className="models-details-body">
                        <ul className="model-data-list">
                          { this.props.models.name &&
                            <li>
                              <label className="data-label">Name</label>
                              <p className="data-text">{this.props.models.name}</p>
                            </li>
                          }
                          { this.props.models.version &&
                            <li>
                              <label className="data-label">Version</label>
                              <p className="data-text">{this.props.models.version}</p>
                            </li>
                          }
                          { this.props.models.project &&
                            <li>
                              <label className="data-label">Project</label>
                              <p className="data-text">{this.props.models.project}</p>
                            </li>
                          }
                          { this.props.models.replication_factor &&
                            <li>
                              <label className="data-label">Replication Factor</label>
                              <p className="data-text">{this.props.models.replication_factor}</p>
                            </li>
                          }
                          { this.props.models.ml_library &&
                            <li>
                              <label className="data-label">Library</label>
                              <ul className="project-members-list">
                                <Library library={this.props.models.ml_library}/>
                              </ul>
                            </li>
                          }
                          { this.props.models.type &&
                            <li>
                              <label className="data-label">Type</label>
                              <p className="data-text">{this.props.models.type}</p>
                            </li>
                          }
                          { this.props.models.ml_algorithm &&
                            <li>
                              <label className="data-label">Algorithm</label>
                              <p className="data-text">{this.props.models.ml_algorithm}</p>
                            </li>
                          }
                          { this.props.models.status &&
                            <li>
                              <label className="data-label">Status</label>
                              <p className="data-text">{this.props.models.status}</p>
                            </li>
                          }
                          { this.props.models.id &&
                            <li>
                              <label className="data-label">REST API</label>
                              <p className="data-text">{this.props.models.id}</p>
                            </li>
                          }
                          { this.props.models.owner &&
                            <li>
                              <label className="data-label">Owner</label>
                              <ul className="project-members-list">
                                <User user={this.props.models.owner}/>
                              </ul>
                            </li>
                          }
                          { this.props.models.created_date &&
                            <li>
                              <label className="data-label">Created Date</label>
                              <p className="data-text">{ moment(this.props.models.created_date).format('llll')}</p>
                            </li>
                          }
                          { this.props.models.deployed_date &&
                            <li>
                              <label className="data-label">Deployed Date</label>
                              <p className="data-text">{ moment(this.props.models.deployed_date).format('llll')}</p>
                            </li>
                          }
                        </ul>
                      </div>
                    </div>
                  </TabPanel>

                  <TabPanel className="tab-body">
                    <div className="tab-body">
                      <table className="table table-bordered">
                        <thead className="table-primary-head">
                          <tr>
                            <th>Feature</th>
                            <th>Mean Train</th>
                            <th>Stddev Train</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>408</td>
                            <td>0.25115420129270544</td>
                            <td>0.4338773914397639</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </TabPanel>
                </Tabs>
              </TabPanel>

              <TabPanel className="content-wraper model-details-wraper">
                <div className="performance-tab">
                  <Tabs>
                    <TabList className="tab-list-inline">
                      <Tab>Hyper Parameters</Tab>
                      <Tab>Model Performance</Tab>
                      <Tab>Confusion Metrics</Tab>
                      <Tab>ROC & LIFT Curve</Tab>
                    </TabList>
                    <TabPanel>
                      <div className="tab-body">
                        <img src={hyper_parameters} alt="Hyper parameters" className="img-responsive" />
                      </div>
                    </TabPanel>

                    <TabPanel>
                      <div className="tab-body">
                        <img src={model_performance_metrics} alt="Model performance" className="img-responsive" />
                      </div>
                    </TabPanel>

                    <TabPanel>
                      <div className="tab-body">
                        <img src={confusion_metrics} alt="confusion metrics" className="img-responsive" />
                      </div>
                    </TabPanel>

                    <TabPanel>
                      <div className="tab-body">
                        <img src={roc_curve} alt="curves" className="img-responsive" />
                      </div>
                    </TabPanel>
                  </Tabs>
                </div>
              </TabPanel>

              <TabPanel className="content-wraper content-wraper-sm model-details-wraper">
                <div className="panel">
                  <div className="panel-header">
                    <h4 className="panel-title">Runtime</h4>
                  </div>
                  <div className="panel-body">

                  </div>
                </div>
              </TabPanel>
              <TabPanel className="content-wraper content-wraper-sm model-details-wraper">
                <div className="panel">
                  <div className="panel-header">
                    <h4 className="panel-title">Data Drift</h4>
                  </div>
                  <div className="panel-body">

                  </div>
                </div>
              </TabPanel>

              <TabPanel className="content-wraper content-wraper-sm model-details-wraper">
                <div className="panel">
                  <div className="panel-header">
                    <h4 className="panel-title">Settings</h4>
                  </div>
                  <div className="panel-body">
                    <h4>Settings</h4>
                  </div>
                </div>
              </TabPanel>
            </Tabs>
          </div>

        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    models: state.modelDetails.modelDetails,
  }
};

const mapDispatchToProps = dispatch => {
  return (
    bindActionCreators({
      getModelDetails
    }, dispatch)
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(ModelDetails);
