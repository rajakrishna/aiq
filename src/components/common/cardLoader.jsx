import React from "react";
import ContentLoader from "react-content-loader";

const CardLoader = props => (
  <ContentLoader
    height={610}
    width={1320}
    speed={2}
    primaryColor="#ffffff"
    secondaryColor="#fefefe"
    {...props}
  >
    <rect x="35.84" y="22" rx="5" ry="5" width="272" height="272" />
    <rect x="357.82" y="22" rx="5" ry="5" width="272" height="272" />
    <rect x="680.82" y="22" rx="5" ry="5" width="272" height="272" />
    <rect x="1001.82" y="20" rx="5" ry="5" width="272" height="272" />
    <rect x="29.84" y="310" rx="5" ry="5" width="272" height="272" />
    <rect x="352.84" y="313" rx="5" ry="5" width="272" height="272" />
    <rect x="677.84" y="315" rx="5" ry="5" width="272" height="272" />
    <rect x="999.84" y="315" rx="5" ry="5" width="272" height="272" />
  </ContentLoader>
);
export default CardLoader;
