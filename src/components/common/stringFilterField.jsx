import React, { Component } from "react";
import{startCase} from "lodash";
class StringFilterField extends Component{
  state={
    name: this.props.name,
    key: this.props.filterKey || "equals",
    value: this.props.filterValue || '',
  }

  UNSAFE_componentWillReceiveProps(props){
    if (this.props !== props) {
      this.setState(
        {
          name: props.name,
          key: props.filterKey || "equals",
          value: props.filterValue || ""
        }
      )
    }
  }

  passToCaller = (obj) => {
    let state = this.state
    this.setState({...state,...obj})
    this.props.OnValueChange({...state,...obj})
  }

  selectionKeyChanged = (e) => {
    this.passToCaller({key: e.target.value})
  }

  filterValue1Changed = (e) => {
    this.passToCaller({value: e.target.value})
  }

  render = ()=>{

    let {name,key,value} = this.state
    return(
        <div className="form-group row">
          <label className="col-md-3">{startCase(name).replace("Ml","ML")}</label>
          <div className="col-md-4">
            <select
              name="probability_range"
              id=""
              className="form-control select-field"
              onChange={this.selectionKeyChanged}
              value={key}
            >
              <option value="equals">is</option>
              <option value="in">is one of</option>
              <option value="contains">contains</option>
            </select>
          </div>
          <div className="col-md-4">
            <input
              type="text"
              name="probability_value"
              className="form-control input-sm"
              placeholder={`${name}`}
              onChange={this.filterValue1Changed}
              value={value}
            />
          </div>
        </div>
    );
  }

}

export default StringFilterField;