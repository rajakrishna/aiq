import React from "react";

const Pagination = props => {
  const {
    itemsCount,
    pageSize,
    onPageChange,
    currentPage,
    currentPageSize
  } = props;
  const pagesCount = Math.ceil(itemsCount / pageSize) - 1;

  var startIndex = currentPage * pageSize + 1;
  var endIndex =
    currentPage === pagesCount ? itemsCount : startIndex + currentPageSize - 1;

  return (
    <div>
      <React.Fragment>
        <span className="pagination-counters">
          {startIndex === endIndex ? <span>{`${endIndex} `}</span> : <span>{`${startIndex}`} to {`${endIndex} `}</span>}
          of {itemsCount}
        </span>
        <button
          className="btn btn-pagination"
          onClick={() => onPageChange("previousPage")}
          disabled={currentPage === 0 ? "disabled" : ""}
        >
          <i className="fa fa-chevron-left" />
        </button>
        <button
          className="btn btn-pagination"
          onClick={() => onPageChange("nextPage")}
          disabled={pagesCount === currentPage ? "disabled" : ""}
        >
          <i className="fa fa-chevron-right" />
        </button>
      </React.Fragment>
    </div>
  );
};

export default Pagination;
