import React, { Component } from "react";
import{startCase} from "lodash";
class NumericFilterField extends Component{
  state={
    name: this.props.name,
    key: this.props.filterKey || "equals",
    value1: this.props.value1 || "",
    value2: this.props.value2 || "",
  }

  UNSAFE_componentWillReceiveProps(props){
    if (this.props !== props) {
      this.setState(
        {
          name: props.name,
          key: props.filterKey || "equals",
          value1: props.value1 || "",
          value2: props.value2 || "",
        }
      )
    }
  }

  passToCaller = (obj) => {
    let state = this.state
    this.setState({...state,...obj})
    this.props.OnValueChange({...state,...obj})
  }

  selectionKeyChanged = (e) => {
    this.passToCaller({key: e.target.value})
  }

  filterValue1Changed = (e) => {
    this.passToCaller({value1: e.target.value})
  }
  filterValue2Changed = (e) => {
    this.passToCaller({value2: e.target.value})
  }

  render = ()=>{

    let {name,key,value1,value2} = this.state
    return(
      <div className="form-group row">
      <label className="col-md-3">{startCase(name)}</label>
      <div className="col-md-4">
        <select
          name="probability_range"
          id=""
          className="form-control select-field"
          onChange={this.selectionKeyChanged}
          value={key}
        >
          <option value="equals">is</option>
          <option value="in">is one of</option>
          <option value="lessThan">is less than</option>
          <option value="lessOrEqualThan">is less or equal than</option>
          <option value="greaterThan">is greater than</option>
          <option value="greaterOrEqualThan">is greater or equal than</option>
          <option value="isBetween">is between</option>
        </select>
      </div>
      {key && 
        key !== 'output.in' &&
        key !== 'isBetween' &&
        <div className="col-md-4">
          <input
            type="number"
            name="value1"
            className="form-control input-sm"
            placeholder="Ex: 0.77"
            onChange={this.filterValue1Changed}
            value={value1}
          />
        </div>
      }
      {key === 'output.in' &&
        <div className="col-md-4">
          <input
            type="number"
            name="value1"
            className="form-control input-sm"
            placeholder="Ex: 0.11,0.44,0.99"
            onChange={this.filterValue1Changed}
            value={value1}
          />
        </div>
      }
      {key === 'isBetween' &&
        <React.Fragment>
          <div className="col-md-2">
            <input
              type="number"
              name="value1"
              className="form-control input-sm"
              placeholder="Ex: 0.11"
              onChange={this.filterValue1Changed}
              value={value1}
            />
          </div>
          <div className="col-md-2">
            <input
              type="number"
              name="value2"
              className="form-control input-sm"
              placeholder="Ex: 0.77"
              onChange={this.filterValue2Changed}
              value={value2}
            />
          </div>
        </React.Fragment>
      }
    </div>
    );
  }

}

export default NumericFilterField;