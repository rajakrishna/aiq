import React, { Component } from 'react';
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import Button from '@material-ui/core/Button';
import MicroModal from "micromodal";
import { AllAvailableApplications, EnabledApplications } from '../easyAiRedux/applicationsData';



const styles = (theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center",
    overflow: "hidden",
    padding: 2,
  },
  card: {
    display: "flex",
    width: 426,
    height: 175,
    marginBottom: 30,
    marginRight: 30,
  },
  details: {
    display: "flex",
    flexDirection: "column",
    width: 265
  },
  content: {
    // flex: "1 0 auto",
    paddingTop: '1.5rem',
    paddingLeft: '1.5rem',
    paddingRight: '1.5rem',
    paddingBottom: '0.8rem',
  },
  cover: {
    width: 200,
  },
  controls: {
    display: "flex",
    alignItems: "center",
    paddingLeft: '1.5rem',
    paddingBottom: '1.5rem'
  },
  title: {
    fontWeight: 600,
    fontSize: 16,
    marginBottom: 10
  },
  subtitle: {
    fontSize: 16,
    fontWeight: 550,
    lineHeight: 1
  },
  body: {
    width: 230,
    maxWidth: 230,
    minHeight: 80,
    overflow: 'hidden',
    marginTop: 8,
    lineHeight: 1
  },
  body2: {
    fontSize: 14,
    width: 240,
    maxWidth: 240,
    maxHeight: 34,
    overflow: 'hidden',
    marginTop: 5,
    paddingRight: '20px'
  },
  button: {
    fontWeight: 550,
    fontSize: '15px',
    textTransform: 'none'
  }

});


class Applications extends Component {
    constructor(props) {
        super(props);
        this.state = {
          solutionType: 'all',
          applicationSearchValue: '',
        }
    }
    // render() { 
    //     return ( 
    //         <div className="main-container">
    //         <div className="main-body">
    //           <div className="content-wraper-sm">
    //             <div className="card">
    //                 <div className="card-header mt">
    //                   <h4>Applications</h4>
    //                   </div>
    //                   <div className="card-body h-min-370">
    //                     <div className="empty-items-box">
    //                         <h4>Build and deploy AI Apps using any code and framework</h4>
    //                         <h4>Coming soon!</h4>
    //                     </div>
    //                   </div>
    //             </div>
    //           </div>
    //         </div>
    //       </div>
    //      );
    // }

    handleSearch = () => {
      console.log("Handle Search Application Input Value")
    }

    handleRefresh = () => {
      console.log("Handle Refresh Applicatoins List")
    }

    renderSearchContainer = () => {
      return (
        <div className="project-list-head clearfix">
          <div className='mt-3 w-100 d-flex flex-row align-items-center'>
            <h4 className="mr-3 m-title-info">Applications</h4>
            <div className="projects-search">
              <input
                type="text"
                // value={this.state.projectParams.nameContains}
                className="form-control"
                placeholder="Search Applications"
                onChange={(event) => this.setState({applicationSearchValue: event.target.value})}
                value={this.state.applicationSearchValue}
              />
              <button
                className="btn btn-primary"
                onClick={this.handleSearch}
              >
                <i className="fa fa-search" />
              </button>
            </div>
            <div className="project-tools">
              <select
                className='p-2 border border-1 border-secondary rounded'
                value={this.state.solutionType}
                onChange={(event) => this.setState({solutionType: event.target.value})}
                type="select"
              >
                <option value="all">All</option>
                <option value="retail">Retail</option>
                <option value="sales">Sales</option>
                <option value="healthcare">Healthcare</option>
              </select>
              <button
                style={{ margin: 10 }}
                className="btn btn-default"
                onClick={this.handleRefresh}
              >
                <i className="fa fa-sync-alt" />
              </button>
            </div>
          </div>
        </div>
      )
    }

    renderCard = (eachItem) => {
      const {classes} = this.props
        return (
        <Card key={eachItem.app_id} className={classes.card}>
          <CardMedia
            className={classes.cover}
            image={eachItem.app_image_url}
            title={`${eachItem.type} Image`}
          />
          <div className={classes.details}>
            <CardContent className={classes.content}>
              <Typography variant="title" className={classes.title}>
                {eachItem.name}
              </Typography>
              <Typography variant="caption" className={classes.subtitle}>
                {`AI APPLICATION`}
              </Typography>
              <Typography variant="caption" className={classes.body2}>
                {eachItem.description}
              </Typography>
            </CardContent>
            <div className={classes.controls}>
              <Button variant="outlined" size='small' className={classes.button}>
                {eachItem.status === 'Available' ? 
                    `Add`
                :
                  <a href={eachItem.app_url} target="_blank">
                    {`View`}
                  </a>
                }
              </Button>
            </div>
          </div>
        </Card>
      )
    }

    render() {
      const { classes, theme } = this.props;
      const newApplicationSearchValue = this.state.applicationSearchValue;
      const newEnabledApplicatinos = EnabledApplications.filter((eachItem) => eachItem.name.toLocaleLowerCase().includes(this.state.applicationSearchValue.toLocaleLowerCase()))
      const newAvailableApplications = AllAvailableApplications.filter((eachItem) => eachItem.name.toLocaleLowerCase().includes(this.state.applicationSearchValue.toLocaleLowerCase()))
      return (
        <div className="main-container">
          <div className="main-body">
            <div className="content-wraper-sm">
              <div className="projects-wraper mr-5 ml-5">
                    <h4 className="mr-3 mt-4 m-title-info">Applications</h4>
                    <div className="empty-items-box">
                      <h4>Build and deploy AI Apps using any code and framework</h4>
                      <h4>Coming soon!</h4>
                  </div>
                {/* <div className="card-body h-min-370" style={{ padding: 'inherit' }}>
                  {this.renderSearchContainer()}
                  <h4 className='mt-2 mb-2' style={{ fontWeight: '550px' }}>Enabled</h4>
                  <div className={classes.root}>
                    { (newApplicationSearchValue.length === 0 ? EnabledApplications : newEnabledApplicatinos).map((eachItem) => (this.state.solutionType === 'all' ? this.renderCard(eachItem) : eachItem.category === this.state.solutionType && this.renderCard(eachItem)))
                    }
                  </div>
                  <h4 className='mt-2 mb-2'>Available</h4>
                  <div className={classes.root}>
                    { (newApplicationSearchValue.length === 0 ? AllAvailableApplications : newAvailableApplications).map((eachItem) => (this.state.solutionType === 'all' ? this.renderCard(eachItem) : eachItem.category === this.state.solutionType && this.renderCard(eachItem))) }
                  </div>
                </div> */}
              </div>
            </div>
          </div>
        </div>
      );
    }
}
 
Applications.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired
};

export default withStyles(styles, { withTheme: true })(Applications);