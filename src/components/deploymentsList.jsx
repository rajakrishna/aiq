import _, { startCase } from "lodash";
import MicroModal from "micromodal";
import Mixpanel from "mixpanel-browser";
import moment from "moment";
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { deleteDeployment, getDeployments } from "../ducks/deployments";
import { getDeployementType, parseDepList } from "../utils/helper";
import Pagination from "./common/pagination";
import { confirm } from "./Confirm";
import FilterDeployments from "./deployments/FilterDeployments";
import CardLoader from "./models/cardLoader";
import { globalProjectSelection } from "../ducks/projects";

class DeploymentsList extends Component {
  _isMounted = false;
  state = {
    deploymentsList: [],
    depFilters: {
      nameContains: "",
      projectID: this.props.selectedProject && this.props.selectedProject.id,
    },
    currentDepPage: 0,
    totalDeployments: 0,
    reload: false,
  };

  componentDidMount() {
    Mixpanel.track("Deployments Page");
    this._isMounted = true;

    MicroModal.init({
      disableScroll: true,
      disableFocus: false,
      awaitCloseAnimation: false,
      debugMode: true,
    });
    this.getDeployments();
  }
  getDeployments() {
    if (this.props.selectedProject && this.props.selectedProject.id) {
      this.props.getDeployments({ projectID: this.props.selectedProject.id });
    }
  }
  componentDidUpdate(prevProps) {
    if (
      this.props.selectedProject &&
      prevProps.selectedProject.id !== this.props.selectedProject.id
    ) {
      this.props.getDeployments({ projectID: this.props.selectedProject.id });
    }

    if (
      this.state.deploymentsList.length != this.props.deployments.length &&
      !this.state.keyword &&
      prevProps.selectedProject.id !== this.props.selectedProject.id
    ) {
      this.setState(
        {
          deploymentsList: this.props.deployments,
          totalDeployments: this.props.deployments.length,
        },
        () => {}
      );
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (
      nextProps.deployments &&
      nextProps.deployments.length &&
      !prevState.keyword
    ) {
      return {
        allDeployments: nextProps.deployments,
        totalDeployments: nextProps.deployments.length,
        deploymentsList: parseDepList(
          nextProps.deployments,
          nextProps.deployments.length,
          prevState.currentDepPage
        ),
        card_loader: nextProps.loader,
      };
    }
    return {
      card_loader: nextProps.loader,
    };
  }

  getColor(type) {
    const status = {
      Creating: "info",
      Failed: "failed",
      Available: "success",
    };
    const color = status[type] ? status[type] : "primary";
    return color;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  handleInputSearchChange = (e) => {
    let filters = { ...this.state.depFilters };
    filters[e.target.name] = e.target.value;
    this.applyDepFilters(filters);
  };

  applyDepFilters = (depFilters) => {
    this.setState({ depFilters, currentPage: 1 });
    this.props.getDeployments(_.pickBy(depFilters, _.identity));
  };

  handleRemoveFilter = (filter) => {
    let updatedFilters = { ...this.state.filters };
    updatedFilters[filter] = null;
    this.setState({ filters: updatedFilters });
    this.applyFilters(updatedFilters);
  };

  handleDepRemoveFilter = (filter) => {
    let updatedFilters = { ...this.state.depFilters };
    updatedFilters[filter] = null;
    updatedFilters["projectID"] = this.props.selectedProject.id;
    this.setState({ depFilters: updatedFilters });
    this.applyDepFilters(updatedFilters);
  };

  handleDepPageChange = (pageAction) => {
    let pageNumber = this.state.currentDepPage;
    const allDeployments = this.state.allDeployments;
    pageAction === "nextPage" ? (pageNumber += 1) : (pageNumber -= 1);
    this.setState({
      currentDepPage: pageNumber,
      deploymentsList: parseDepList(
        allDeployments,
        allDeployments.length,
        pageNumber
      ),
    });
  };
  getAvailableReplicas = (deployment) => {
    if (deployment.status.deploymentStatus != null) {
      var keys = Object.keys(deployment.status.deploymentStatus),
        item = deployment.status.deploymentStatus[keys[0]];
      if (item.availableReplicas) {
        return item.availableReplicas;
      } else {
        return 0;
      }
    }
  };
  deleteDeployments = (name) => {
    this.props.deleteDeployment(name, () => {
      this.props.getDeployments(_.pickBy(this.state.depFilters, _.identity));
      this.props.getDeployments({ projectID: this.props.selectedProject.id });
    });
  };

  getFilterTab = (data) => {
    switch (typeof data) {
      case "object":
        if (data.value) {
          return `${data.key}-${data.value}`;
        }
        if (data.key == "isBetween") {
          return `${data.key}-${data.value1}&${data.value2}`;
        }
        if (data.value1) {
          return `${data.key}-${data.value1}`;
        }
        break;
      default:
        return data;
    }
  };

  handleSearch = (keyword) => {
    const { allDeployments } = this.state;
    if (!keyword) {
      this.setState({
        deploymentsList: parseDepList(allDeployments, allDeployments.length, 0),
        currentDepPage: 0,
        totalDeployments: allDeployments.length,
        keyword: "",
      });
      return;
    }
    keyword = keyword.toLowerCase();
    const parsedList = allDeployments.filter((e) => {
      return e.metadata.name.includes(keyword);
    });
    const deploymentsList = parseDepList(parsedList, parsedList.length, 0);
    this.setState({
      keyword: keyword,
      deploymentsList: deploymentsList,
      currentDepPage: 0,
      totalDeployments: parsedList.length,
    });
  };

  refresh() {
    this.props.getDeployments({ projectID: this.props.selectedProject.id });
  }
  handleCancelForm = () => {
    this.setState({ showForm: false });
  };

  render() {
    const {
      depFilters,
      deploymentsList,
      totalDeployments,
      currentDepPage,
    } = this.state;
    let appliedDepFilters = _.pickBy(depFilters, _.identity);
    return (
      <div className="main-container">
        <div className="main-body">
          <div className="content-wraper-sm">
            <div>
              {/* {countersVisibility !== false ? <Counters /> : ""} */}
              {
                <div className="card">
                  <div>
                    <div className="card-header header-with-search pl-4 mr-5 pt-3">
                      <h4 className="bold-heading">Deployments</h4>
                      <div className="card-search inline-block">
                        <input
                          type="text"
                          className="form-control"
                          placeholder="SEARCH"
                          onChange={(event) => {
                            this.handleSearch(event.target.value);
                          }}
                        />
                      </div>
                      <div className="card-tools">
                        <div className="model-pagination">
                          {totalDeployments > 20 && (
                            <Pagination
                              itemsCount={totalDeployments}
                              currentPage={currentDepPage}
                              currentPageSize={20}
                              pageSize={20}
                              onPageChange={this.handleDepPageChange}
                            />
                          )}
                        </div>
                        <div className="model-pagination inline-flex">
                          <button
                            className={`btn btn-sm btn-primary ml clickable ${
                              !this.props.selectedProject ? "disabled" : ""
                            }`}
                            type="button"
                            onClick={() => {
                              this.props.history.push("/deployments/new", {
                                prevUrl: this.props.history.location.pathname,
                              });
                            }}
                            disabled={!this.props.selectedProject}
                          >
                            Create Deployment
                          </button>
                          <button
                            className="btn btn-sm btn-default ml"
                            type="button"
                          >
                            <i
                              className={
                                !this.state.reload
                                  ? "fa fa-sync"
                                  : "fa fa-sync rotate"
                              }
                              onClick={() => {
                                this.refresh();
                              }}
                            />
                          </button>
                        </div>
                      </div>
                    </div>
                    <div className="model-filter-header pl-4 mr-5" style={{marginTop: '0.8rem'}}>
                      <div className="filter-btn ml-sm ml-0">
                        <button
                          className="btn btn-primary btn-sm btn-rounded ml-0"
                          onClick={() => MicroModal.show("modal-deploy")}
                        >
                          Add Filter +
                        </button>
                      </div>
                      <div className="applied-filters">
                        <ul className="filter-items-list">
                          {!_.isEmpty(appliedDepFilters) &&
                            Object.keys(appliedDepFilters)
                              .filter((f) => f !== "projectID")
                              .map((key) => (
                                <li key={key} className="filter-tag-item">
                                  {startCase(key)}-
                                  {this.getFilterTab(appliedDepFilters[key])}
                                  <i
                                    className="fa fa-times"
                                    style={{ marginTop: 3 }}
                                    onClick={() =>
                                      this.handleDepRemoveFilter(key)
                                    }
                                  ></i>
                                </li>
                              ))}
                        </ul>
                      </div>
                    </div>
                    {(deploymentsList && deploymentsList.length) ||
                    this.state.card_loader ? (
                      <div className="card-body h-min-370 ml-0 mr-2 pl-4">
                        {this.state.card_loader ? (
                          <CardLoader />
                        ) : (
                          <table className="table data-table no-border">
                            <thead>
                              <tr>
                                <td className="pl-0">Name</td>
                                <td className="text-center">Status</td>
                                <td>Created Date</td>
                                <td>Type</td>
                                <td>Replicas</td>
                                <td>Available Replicas</td>
                                <td>Age</td>
                                <td></td>
                              </tr>
                            </thead>
                            <tbody>
                              {deploymentsList &&
                                deploymentsList.map((deployment, index) => {
                                  return (
                                    <tr key={"deployment" + index}>
                                      <td className="pl-0">
                                        <span
                                          className="text-link"
                                          onClick={() => {
                                            this.props.history.push(
                                              `/deployments/${deployment.metadata.name}`,
                                              {
                                                prevUrl: this.props.history
                                                  .location.pathname,
                                              }
                                            );
                                          }}
                                        >
                                          {deployment.metadata.name}
                                        </span>
                                      </td>
                                      <td className="text-center">
                                        {deployment.status && (
                                          <label
                                            className={`model-item-status status-${this.getColor(
                                              deployment.status.state
                                            )}`}
                                          >
                                            {deployment.status.state}
                                          </label>
                                        )}
                                      </td>
                                      <td>
                                        {deployment.metadata &&
                                          moment(
                                            deployment.metadata
                                              .creationTimestamp
                                          ).format("LL")}
                                      </td>
                                      <td>{getDeployementType(deployment)}</td>
                                      <td>
                                        {deployment.spec &&
                                          deployment.spec.predictors &&
                                          deployment.spec.predictors[0]
                                            .replicas}
                                      </td>
                                      {/* {deployment.status &&
                                          deployment.status.deploymentStatus &&
                                          deployment.status.deploymentStatus[0]
                                            .availableReplicas ? (
                                            <td>
                                            {deployment.status.deploymentStatus[0].availableReplicas}
                                          </td>
                                      ) : (
                                        <td>0</td>
                                      )}   */}
                                      <td>
                                        {this.getAvailableReplicas(deployment)}
                                      </td>
                                      <td>
                                        {deployment.metadata &&
                                          moment(
                                            deployment.metadata
                                              .creationTimestamp
                                          ).toNow(true)}
                                      </td>
                                      <td>
                                        <button
                                          type="button"
                                          className="btn btn-danger btn-sm mr-sm"
                                          data-toggle="tooltip"
                                          title="Delete"
                                          onClick={() => {
                                            confirm(
                                              "Are you sure?",
                                              "Delete"
                                            ).then(() => {
                                              this.deleteDeployments(
                                                deployment.metadata.name
                                              );
                                            });
                                          }}
                                        >
                                          <i className="fa fa-trash"></i>
                                        </button>
                                      </td>
                                    </tr>
                                  );
                                })}
                            </tbody>
                          </table>
                        )}
                      </div>
                    ) : (
                      <div className="empty-items-box">
                        <h4>You don't have any Deployments!</h4>
                      </div>
                    )}
                    <FilterDeployments
                      filterType={"deployments"}
                      applyFilters={this.applyDepFilters}
                      filters={this.state.depFilters}
                      project={/project/.test(this.props.match.path)}
                    />
                  </div>
                </div>
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    deployments: state.deployments.deployments,
    loader: state.deployments.loader,
    selectedProject: state.projects.globalProject,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      getDeployments,
      deleteDeployment,
      globalProjectSelection,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(DeploymentsList);
