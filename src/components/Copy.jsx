import React, { Component } from "react";

export class Copy extends Component {
    state = {
        display: ""
    }
    timeout = "";

    showTool = () => {
        this.setState({
            display: 1
        }, () =>{
            clearTimeout(this.timeout);
            this.timeout = setTimeout(()=>{
                this.setState({
                    display: ""
                })
            }, 500);
        })
    }

    render() {
        const {mw} = this.props;
        return (
            <div className="relative">
                <span className="copy" style={mw ? {maxWidth:mw} : {}}>
                    {this.props.children}
                </span>
                <button type="button" className="btn btn-sm btn-default" onClick={()=>{
                    const text = this.props.children;
                    const inp = document.createElement("input");
                    inp.value = this.props.cpyText || text;
                    document.body.appendChild(inp);
                    inp.select();
                    document.execCommand("copy");
                    document.body.removeChild(inp);
                    this.showTool();
                }}>
                    <i className="fa fa-copy"></i>
                </button>
                {
                    this.state.display &&
                    <span className="tool">Copied!</span>
                }
            </div>
        )
    }
}