import React from "react";

export const FullLoader = (props) => {
    return(
        <div className="backdrop">
            {props.display ?
            <div className="bgBlue">
                <div className="loader">
                    <span><i className="fa fa-gear"></i></span>
                </div>
            </div> :
            "" }
        </div>
    )
}