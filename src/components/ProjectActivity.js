import React, { Component } from "react";
import User from "./shared/User";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { globalProjectSelection } from "../ducks/projects";
import { alertMsg } from "../ducks/alertsReducer";
import Mixpanel from "mixpanel-browser";
import { getErrorMessage,getResponseError } from "../utils/helper";
import { getProject,deleteProject } from "../api";
import MicroModal from "micromodal";
import moment from "moment";
import Secrets from "./Secrets";
import ManageTeam from "./ManageTeam";
import ProjectModifyFullScreen from "./modals/ProjectModifyFullScreen";
import CardLoader from "./models/cardLoader";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import ProjectDetailsCount from "./projectDetailsCount";
class ProjectActivity extends Component {
  project = "";
  projectOwner = "";
  allUsers = "";
  constructor(props) {
    super(props);
    this.state = {
      tab: 0,
      projectUsers: [],
      allUsers: [],
      selectionUsers: [],
      project: "",
      projectOwner: "",
      open: false,
      card_loader: true,
    };
  }

  async componentDidMount() {
    if (this.props.selectedProject && this.props.selectedProject.id) {
      this.projectDetails();
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.selectedProject && prevProps.selectedProject.id !== this.props.selectedProject.id) {
      this.projectDetails();
    }
  }
  projectDetails = async () => {
    this.setState({
      card_loader: true,
    });
    MicroModal.init();
    await getProject(this.props.token, this.props.selectedProject.id)
      .then((response) => {
        if (response.status) {
          this.project = response.data;
          this.projectOwner = response.data.owner;
          this.props.globalProjectSelection(this.project);
          this.setState({
            card_loader: false,
          });
        }
      })
      .catch((error) => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while fetching Project details, please try again.";
        Mixpanel.track("Get Project Failed", {
          project_id: this.props.match.params.id,
          error: errorMessage,
        });
        this.props.alertMsg(errorMessage, "error");
      });
  };

  delProject = (id) => {
    deleteProject(this.props.token, id)
      .then((res) => {
        if (res.status === 200) {
          this.props.alertMsg("Project deleted successfully", "success");
          this.props.history.push("/projects");
          this.setState({ open: false });
        } else {
          const errorMessage =
            getResponseError(res) ||
            "An error occurred while deleting the Project, please try again.";
          this.props.alertMsg(errorMessage, "error");
          this.setState({ open: false });
        }
      })
      .catch((err) => {
        const errorMessage =
          getErrorMessage(err) ||
          "An error occurred while deleting the Project, please try again.";
        this.props.alertMsg(errorMessage, "error");
        this.setState({ open: false });
      });
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };


  render() {
    return (
      <div className="main-body" style={{minHeight: '100vh', minWidth: '86vw'}}>
        {this.state.card_loader ? (
          <CardLoader />
        ) : (        
        <div className="content-wraper-sm">
          <div className="m-details-card pt-4">
            <div className="m-details-card-head pl-4 mr-5">
              <div className="m-title-info">
                <h4>{this.props.selectedProject.name}</h4>
                <small>Projects</small>
              </div>
              <div className="m-tech-stack">
                <button
                 className="btn btn-default btn-sm mr"
                 onClick={() => {
                  this.projectDetails();
                }}                 
                 >
                  <i className="fa fa-sync-alt"></i>
                </button>

                <div className="d-inline">
                  <ProjectModifyFullScreen project={this.props.selectedProject} />
                </div>
                <button 
                type="button" 
                className="btn btn-sm btn-danger"
                color="secondary"
                onClick={this.handleClickOpen}
                style={{
                  minWidth: "30px",
                  backgroundColor: "#ff0000",
                  color: "#ffffff",
                  height: "30px",
                }}                
                >
                  <i className="fa fa-trash"></i>
                </button>
                <Dialog
                    open={this.state.open}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                  >
                    <DialogTitle id="alert-dialog-title">
                      {`Are you sure you want to delete project ${this.props.selectedProject.name} ?`}
                    </DialogTitle>
                    <DialogContent>
                      <DialogContentText id="alert-dialog-description">
                        This operation cannot be Undone
                      </DialogContentText>
                      <DialogContentText style={{ border: "grey" }}>
                      All Project resources such as Notebooks, Experiments, Models, Deployments and Workflows will be permanently deleted.
                      </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                      <Button
                        onClick={this.handleClose}
                        color="secondary"
                        border={0}
                      >
                        Cancel
                      </Button>
                      <Button
                        onClick={() => {
                          this.delProject(this.props.selectedProject.id);
                        }}
                        color="primary"
                        border={0}
                      >
                        Delete
                      </Button>
                    </DialogActions>
                  </Dialog>                
              </div>
            </div>

            <div className="m-info-table model-details">
                <table className="table data-table no-border modeldetails-table">
                  <thead>
                    <tr>
                      <td>Description</td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td> {this.props.selectedProject.description}</td>
                    </tr>
                  </tbody>
                </table>
                <hr />
                <table className="table data-table no-border">
                  <thead>
                    <tr>
                      <td>Created By</td>
                      <td>Created On</td>
                      <td>Updated By</td>
                      <td>Updated On</td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><span className="mr-1"> <User user={this.props.selectedProject.owner} /></span>{String(this.props.selectedProject.owner)}</td>
                      <td>  {moment(
                              this.props.selectedProject.created_date
                            ).format("LL")}</td>
                      <td><span className="mr-1"><User user={this.props.selectedProject.owner} /></span>{String(this.props.selectedProject.owner)}</td>
                      <td>  {moment(
                              this.props.selectedProject.last_modified_date
                            ).format("LL")}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            <div className="m-details-body model-tabs pd-tb">
              <div>
                <ul className="inline-tabs tabs-blue pad-50">
                <li
                    className={`react-tabs__tab${this.state.tab === 0 &&
                      "--selected"}`}
                    onClick={() => {
                      this.setState({ tab: 0 });
                    }}
                  >
                    Overview
                  </li>                  
                  <li
                    className={`react-tabs__tab${this.state.tab === 1 &&
                      "--selected"}`}
                    onClick={() => {
                      this.setState({ tab: 1 });
                    }}
                  >
                    Activity
                  </li>
                  <li
                    className={`react-tabs__tab${this.state.tab === 2 &&
                      "--selected"}`}
                    onClick={() => {
                      this.setState({ tab: 2 });
                    }}
                  >
                    Secrets
                  </li>
                  <li
                    className={`react-tabs__tab${this.state.tab === 3 &&
                      "--selected"}`}
                    onClick={() => {
                      this.setState({ tab: 3 });
                    }}
                  >
                    Manage Team
                  </li>
                </ul>
                <hr />
                <div>
                  {this.state.tab === 3 && (
                    <div>
                      <div className="m-info-table m-0">
                        <ManageTeam project={this.props.selectedProject} />
                      </div>
                    </div>
                  )}
                  {this.state.tab === 2 && (
                    <div>
                      <div className="m-info-table mlr-30">
                        <Secrets project={this.props.selectedProject} />
                      </div>
                    </div>
                  )}
                  {this.state.tab === 1 && (
                    <div>
                      <div className="m-info-table mlr-30">
                        <div className="empty-items-box">
                          <h4>See all project activity at one place.</h4>
                          <h4>Coming soon!</h4>
                        </div>
                      </div>
                    </div>
                  )}
                  {this.state.tab === 0 && (
                    <div>
                      <div className="m-info-table mr-30">
                        <ProjectDetailsCount />
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    projectDetails: state.projectDetails.projectDetails,
    globalProject: state.projects.globalProject,
    selectedProject: state.projects.globalProject,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      globalProjectSelection,
      alertMsg,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(ProjectActivity);
