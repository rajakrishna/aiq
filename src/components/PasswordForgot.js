import React, { Component } from "react";
import logo from "../images/predera-logo.png";
import { requestPasswordReset } from "../api";
import Mixpanel from "mixpanel-browser";
import { getErrorMessage } from "../utils/helper";

class PasswordForgot extends Component {
  state = {
    email: "",
    error: ""
  };

  componentDidMount() {
    Mixpanel.track("Forgot Password Page");
  }

  onSubmit = event => {
    event.preventDefault();
    if (this.state.email.length !== 0) {
      let data = {
        userName: this.state.email.trim()
      };
      requestPasswordReset(data)
        .then(response => {
          if (response.status) {
            this.props.history.push(
              "/forgot-password-verification",
              data.userName
            );
          }
        })
        .catch(error => {
          const errorMessage =
            getErrorMessage(error) ||
            "An error occurred while sending Password Reset Request, please try again.";
          Mixpanel.track("Request Password Reset Failed", {
            error: errorMessage
          });
          this.setState({
            error: errorMessage
          });
        });
    }
  };

  validateEmail = email => {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (email.length === 0) {
      this.setState({ error: "Email can not be blank" });
    } else if (!re.test(email)) {
      this.setState({ error: "Invalid Email" });
    } else {
      this.setState({ error: "" });
    }
    this.setState({ email: email });
  };

  render() {
    return (
      <div className="auth-wraper">
        <form onSubmit={this.onSubmit}>
          <div className="auth-box">
            <div className="logo-head">
              <img src={logo} alt="" />
            </div>
            <h2 className="auth-heading">Forgot Password</h2>
            {this.state.error && (
              <div className="error-placement  alert alert-danger">
                <i className="fa fa-exclamation-triangle" />
                <span>{this.state.error}</span>
              </div>
            )}

            <p className="mt-lg">
              We will send you instructions for your password recovery to the
              email provided
            </p>

            <div className="form-group">
              <label>Email</label>
              <div className="input-group">
                <span className="input-group-addon">
                  <i className="fa fa-envelope"></i>
                </span>
                <input
                  value={this.state.email}
                  onChange={e => this.validateEmail(e.target.value)}
                  type="email"
                  className="form-control"
                  placeholder="Enter email "
                  required
                />
              </div>
            </div>
            <div className="form-group mt-xl">
              <button type="submit" className="btn btn-primary form-btn">
                Send
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default PasswordForgot;
