import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { enabled,available } from "../easyAiRedux/DataSourceData"; 


const styles = theme => ({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
      width:600
    },
    margin: {
      margin: '8px 0px',
    },
    bootstrapRoot: {
      'label + &': {
        marginTop: theme.spacing.unit * 3,
      },
    },
    bootstrapInput: {
      borderRadius: 4,
      position: 'relative',
      backgroundColor: theme.palette.common.white,
      border: '1px solid #ced4da',
      fontSize: 16,
      width: 600,
      padding: '10px 12px',
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      '&:focus': {
        borderRadius: 4,
        borderColor: '#80bdff',
        boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
      },
    },
    bootstrapFormLabel: {
      fontSize: 18,
    },
    paramsform:{
        backgroundColor:'#EAEAEA',
        width:'100%',
        marginTop:16
    },
    paramsmargin:{
        width:550,
        marginLeft:25,
        marginBottom:16
    },
    checkbox:{
      marginLeft:25,
      marginBottom:16
    },
    paramstitle:{
        marginLeft:25,
        marginTop:10,
        fontWeight:550
    },
    backbutton:{
        '&:hover': {
            cursor: "pointer",
            color: "blue"
          }
    },
    actionbuttons:{
        display:'flex',
        justifyContent:'end',
        marginTop:16,
        width:'100%'
    },
    button:{
        marginLeft:10,
        textTransform: 'none',
        fontWeight:550
    },
    formtitle:{
        fontWeight:550
    }
  });

class DataSourceForm extends Component {
    constructor(props) {
        super(props);
    }


    state = { 
        datasource:{
          ref_id: '',
          name: '',
          type: '',
          desc: '',
          params: []
        },

     }

     componentDidMount =()=>{
      if (!this.props.location.state) {
        this.props.history.push("/datasets")
        return
      }
       if(this.props.location.state.ref_id){
         const enabledDatasource = enabled.find((item)=> item.ref_id === this.props.location.state.ref_id)
         this.setState({datasource:enabledDatasource})
        }else{
          const available_dataSource = available.find((item)=>item.type === this.props.location.state.type)
          this.setState({datasource:{
            ref_id: '',
            name: '',
            type: available_dataSource.type,
            desc: '',
            params: available_dataSource.params
          }})
        }
     }
 
    handleBack =()=>{
        this.props.history.goBack()
    }

    handleCancel = () =>{
      this.props.history.push("/datasource_details")
    }

    handleCreate =()=>{
      this.props.history.push("/datasets")
    }
    handleUpdate =()=>{
      this.props.history.push("/datasource_details")
    }


    handleName =(value)=>{
      const newName = {...this.state.datasource,name:value}
      this.setState({datasource:newName})
    }

    handleDesc =(value)=>{
      const newDesc = {...this.state.datasource,desc:value}
      this.setState({datasource:newDesc})
    }

    handleChange =(value,id) =>{
      let params = this.state.datasource.params
      params.forEach((item)=>{
        if(item.ref_id === id){
          item.value = value
        }
      })
      this.setState({...this.state.datasource,params:params})
    }


    render() { 

        const { classes } = this.props;
        return ( 
            <div className='main-container bg-grey'>
                <div className="main-body pt-4 pl-4">
                    <div className="content-wraper-sm">
                        <div className="projects-wraper ml-5 mr-5">
                            <div className="model-header">
                              <h6
                              className={classes.backbutton}
                                onClick={this.handleBack}
                               >
                                {`< Back`}
                                </h6>
                            </div>
                            <div className="card-body h-min-370 p-0">
                              {(this.props.location.state && this.props.location.state.ref_id) ? 
                                  <h4 className={classes.formtitle}>Edit Data Source</h4>
                                :  <h4 className={classes.formtitle}>Create Data Source</h4>
                               }
                                <div className={classes.root}>
                                    <FormControl className={classes.margin}>
                                        <InputLabel shrink htmlFor="name-input" className={classes.bootstrapFormLabel}>
                                          Name *
                                        </InputLabel>
                                        <InputBase
                                        id="name-input"
                                        value = {this.state.datasource.name}
                                        onChange={(event)=> this.handleName(event.target.value) }
                                        classes={{
                                            root: classes.bootstrapRoot,
                                            input: classes.bootstrapInput,
                                        }}
                                        />
                                    </FormControl>
                                    <FormControl className={classes.margin}>
                                        <InputLabel shrink htmlFor="description-input" className={classes.bootstrapFormLabel}>
                                           Description
                                        </InputLabel>
                                        <InputBase
                                        id="description-input"
                                        value={this.state.datasource.desc}
                                        onChange={(event) => this.handleDesc(event.target.value)}
                                        classes={{
                                            root: classes.bootstrapRoot,
                                            input: classes.bootstrapInput,
                                        }}
                                        />
                                    </FormControl>
                                    <div className={classes.paramsform}>
                                        <h5 className={classes.paramstitle}>{this.state.datasource.type}</h5>
                                        {
                                          this.state.datasource.params.map((eachItem)=>{
                                            if(eachItem.name === "sandbox"){
                                              return (
                                                <FormGroup className={classes.checkbox} key={eachItem.ref_id}>
                                                   <FormControlLabel
                                                      control={
                                                        <Checkbox
                                                          checked={JSON.parse((eachItem.value).toString().toLowerCase())} 
                                                          onChange={()=> this.handleChange((!JSON.parse((eachItem.value).toString().toLowerCase())),eachItem.ref_id)}
                                                          color="primary"
                                                        />
                                                      }
                                                      label="Sandbox"
                                                    />
                                                </FormGroup>
                                              )
                                            }else{
                                              return(
                                                <FormControl className={classes.paramsmargin} key={eachItem.ref_id}>
                                                <InputLabel shrink htmlFor="username-input" className={classes.bootstrapFormLabel}>
                                                  {eachItem.name} *
                                                </InputLabel>
                                                <InputBase
                                                id="username-input"
                                                value={eachItem.value}
                                                type={(eachItem.name === 'password') ? 'password':'text' }
                                                onChange ={(event) => this.handleChange(event.target.value,eachItem.ref_id)}
                                                classes={{
                                                    root: classes.bootstrapRoot,
                                                    input: classes.bootstrapInput,
                                                }}
                                                />
                                            </FormControl>
                                              )
                                            }
                                          })
                                        }
                                    </div>
                                    <div className={classes.actionbuttons}>
                                      <Button 
                                      variant="outlined"
                                      onClick={this.handleCancel} 
                                      className={classes.button}
                                      >
                                         Cancel
                                      </Button>
                                      {(this.props.location.state && this.props.location.state.ref_id) ?
                                        <Button 
                                          variant="outlined"
                                          onClick={this.handleUpdate} 
                                          className={classes.button}
                                        >
                                          Update
                                        </Button> :
                                        <Button 
                                          variant="outlined"
                                          onClick={this.handleCreate} 
                                          className={classes.button}
                                        >
                                          Save
                                        </Button>
                                    }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
         );
    }
}

DataSourceForm.propTypes = {
    classes: PropTypes.object.isRequired,
  };
 
export default withStyles(styles)(DataSourceForm);