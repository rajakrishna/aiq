import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { getAllModels } from "../../api";
import User from "../shared/User";
import Library from "../shared/Library";
import moment from "moment";
import CardLoader from "../models/cardLoader";

class ExperimentsOverview extends Component {
  state = {
    showLoader: true
  };

  componentDidMount() {
    getAllModels(this.props.token, {
      statusEquals: "EXPERIMENT",
      sort: "created_date,desc",
      size: 5
    })
      .then(response => {
        if (response.status) {
          this.setState({
            modelList: response.data,
            showLoader: false
          });
        }
      })
      .catch(error => {});
  }

  render() {
    const { modelList, showLoader } = this.state;
    return (
      <div className="card">
        <div className="card-header">
          <h4>RECENT EXPERIMENTS</h4>
        </div>
        <div className="card-body">
          {showLoader ? ( <CardLoader /> ) : (
          modelList && Object.keys(modelList).length ?
          <table className="table data-table no-border">
            <thead>
              <tr>
                <td>Name</td>
                <td>Version</td>
                <td>Type</td>
                <td>Algorithm</td>
                <td>Stack</td>
                <td>Owner</td>
                <td>Created Date</td>
                <td>Features</td>
                <td>Training Duration</td>
                <td>Testing Duration</td>
              </tr>
            </thead>
            <tbody>
                {Object.keys(modelList).map((model, index) => {
                  return (
                    <tr key={index}>
                      <td>
                        <Link to={`/monitoring/${modelList[model].id}`}>
                          {modelList[model].name}
                        </Link>
                        <p className="data-text"><small>{modelList[model].project_name}</small></p>
                      </td>
                      <td>{modelList[model].version}</td>
                      <td>{modelList[model].type}</td>
                      <td>{modelList[model].ml_algorithm}</td>
                      <td>
                        <ul className="stack-list">
                          <Library library={modelList[model].ml_library} />
                        </ul>
                      </td>
                      <td>
                        <div className="project-members-info">
                          <ul className="project-members-list">
                            <User user={modelList[model].owner} />
                          </ul>
                        </div>
                      </td>
                      <td>{modelList[model].created_date && moment(modelList[model].created_date).format("LL")}</td>
                      <td>{modelList[model].feature_significance && modelList[model].feature_significance.length}</td>
                      <td>{modelList[model].training_duration}</td>
                      <td>{modelList[model].testing_duration}</td>
                    </tr>
                  );
                })}
            </tbody>
          </table> :
          <div className="h180 content-center">No data available!</div>)}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token
  };
};

export default connect(mapStateToProps)(ExperimentsOverview);
