import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Link } from "react-router-dom";
import { getRegisteredModels } from '../../ducks/models';
import User from "../shared/User";
import Library from "../shared/Library";
import moment from "moment";
import CardLoader from "../models/cardLoader";

class ModelsOverview extends Component {

  componentDidMount() {
    this.props.getRegisteredModels(
        {size: 5}
    );
  }

  render() {
    const { registeredModels, loader } = this.props;
    return (
      <div className="card">
        <div className="card-header">
          <h4>RECENT MODELS</h4>
        </div>
        <div className="card-body">
          {loader ? ( <CardLoader /> ) : (
          registeredModels && Object.keys(registeredModels).length ?
          <table className="table data-table no-border">
            <thead>
                <tr>
                    <td>Name</td>
                    <td>Description</td>
                    <td>Project Name</td>
                    <td>Created By</td>
                    <td>Created Date</td>
                    <td>Last Modified By</td>
                    <td>Last Modified Date</td>
                </tr>
            </thead>
            <tbody>
                {registeredModels && registeredModels.map((registeredModel, index) => {
                    return (
                        <tr key={"registered" + index}>
                            <td>
                                <Link to={`/models/${registeredModel.id}`}>
                                    {registeredModel.name}
                                </Link>
                            </td>
                            <td style={{ minWidth: 200 }}>
                                {registeredModel.description}
                            </td>
                            <td>
                                {registeredModel.project_name}
                            </td>
                            <td>
                                {registeredModel.created_by && <User user={registeredModel.created_by} />}
                            </td>
                            <td>
                                {registeredModel.created_date && moment(registeredModel.created_date).format("LL")}
                            </td>
                            <td>
                                {registeredModel.last_modified_by && <User user={registeredModel.last_modified_by} />}
                            </td>
                            <td>
                                {registeredModel.last_modified_date && moment(registeredModel.last_modified_date).format("LL")}
                            </td>
                        </tr>
                    );
                })}
            </tbody>
          </table> :
          <div className="h180 content-center">No data available!</div>)}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token,
    registeredModels: state.models.registeredModels,
    loader: state.models.loader
  };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            getRegisteredModels,
        },
        dispatch
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(ModelsOverview);
