import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { getAllInsights } from "../../api";
import moment from "moment";

class RecentInsights extends Component {
  _isMounted = false;
  state = {
    modelsList: [],
    activeFilter: 1
  };

  getInsights = (days, filter_index) => {
    this.setState({ activeFilter: filter_index });
    getAllInsights(this.props.token, {
      sort: "timestamp,desc",
      size: 10,
      timestampGreaterOrEqualThan: `now-${days}`
    })
      .then(response => {
        if (response.status && this._isMounted) {
          this.setState({ modelsList: response.data });
        }
      })
      .catch(error => {});
  };

  componentDidMount() {
    this._isMounted = true;
    getAllInsights(this.props.token, {
      sort: "timestamp,desc",
      size: 10,
      timestampGreaterOrEqualThan: `now-1d`
    })
      .then(response => {
        if (response.status && this._isMounted) {
          this.setState({ modelsList: response.data });
        }
      })
      .catch(error => {});
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    const { modelsList, activeFilter } = this.state;
    return (
      <div className="card">
        <div className="card-header">
          <h4>RECENT INSIGHTS</h4>
          <div className="card-tools">
            <ul className="filter-tabs">
              <li
                className={activeFilter === 1 ? "active" : ""}
                onClick={() => this.getInsights("1d", 1)}
              >
                LAST 1 DAY
              </li>
              <li
                className={activeFilter === 2 ? "active" : ""}
                onClick={() => this.getInsights("7d", 2)}
              >
                LAST 7 DAYS
              </li>
              <li
                className={activeFilter === 3 ? "active" : ""}
                onClick={() => this.getInsights("30d", 3)}
              >
                LAST MONTH
              </li>
            </ul>
          </div>
        </div>
        {(modelsList && Object.keys(modelsList).length !== 0) ?
        (<div className="card-body">
          <div className="fixed-content-box">
            <table className="table data-table no-border">
              <thead>
                <tr>
                  <td>Model</td>
                  <td width="26%">Insight</td>
                  <td>Version</td>
                  <td>Created Date</td>
                </tr>
              </thead>
              <tbody>
                {Object.keys(modelsList).length !== 0 &&
                  Object.keys(modelsList).map((model, index) => {
                    return (
                      <tr key={index}>
                        <td className="model-name-text">
                          <Link to={`/models/${modelsList[model].model_id}`}>
                            {modelsList[model].model_name}
                          </Link>
                          <br />
                          <small className="text-gray">
                            {modelsList[model].project_name}
                          </small>
                        </td>
                        <td>{modelsList[model].reason}</td>
                        <td>{modelsList[model].version} </td>
                        <td>{moment(modelsList[model].timestamp).format("LL")}</td>
                      </tr>
                    );
                  })}
              </tbody>
            </table>
          </div>
        </div>
        ) : (
          <div className="card-body">
              <div className="h180 content-center">No data available</div>
          </div>
        ) }
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token
  };
};

export default connect(mapStateToProps)(RecentInsights);
