import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { getAllModels } from "../../api";
import User from "../shared/User";
import Library from "../shared/Library";
import { Tooltip } from "@material-ui/core";

class RecentModelDeployment extends Component {
  _isMounted = false;
  state = {
    currentSlide: 1
  };

  componentDidMount() {
    this._isMounted = true;
    getAllModels(this.props.token, {
      statusEquals: "DEPLOYED",
      sort: "created_date,desc",
      size: 5
    })
      .then(response => {
        if (response.status && this._isMounted) {
          this.setState({ modelList: response.data });
        }
      })
      .catch(error => {});
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  addScoreTextClassHealth = score => {
    if (score <= 50) {
      return "text-danger";
    } else if (score > 50 && score < 75) {
      return "text-warning";
    } else {
      return "text-success";
    }
  };

  addScoreTextClassDrift = score => {
    if (score <= 50) {
      return "text-success";
    } else if (score > 50 && score < 75) {
      return "text-warning";
    } else {
      return "text-danger";
    }
  };

  render() {
    var settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };

    const { modelList } = this.state;
    return (
      <div className="card">
        <div className="card-header">
          <h4>RECENT MODEL DEPLOYMENTS ({modelList && modelList.length ? this.state.currentSlide : 0} of {modelList ? modelList.length : 0})</h4>
        </div>
        <div className="card-body model-list-slider">
        {modelList && Object.keys(modelList).length ?
          <Slider
            {...settings}
            afterChange={currentSlide => {
              this.setState({ currentSlide: currentSlide + 1 });
            }}
          >
              {Object.keys(modelList).map((model, index) => {
                return (
                  <div key={index}>
                    <table className="table data-table no-border">
                      <thead>
                        <tr>
                          <td>Name</td>
                          <td>Version</td>
                          <td>Algorithm</td>
                          <td>Stack</td>
                          <td>Owner</td>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td className="model-name-text clip-text">
                            <Tooltip title={modelList[model].name}>
                              <Link to={`/models/${modelList[model].id}`}>
                                {modelList[model].name}
                              </Link>
                            </Tooltip>
                            <br />
                            <small className="text-gray">
                              {modelList[model].project_name}
                            </small>
                          </td>
                          <td>{modelList[model].version}</td>
                          <td>{modelList[model].ml_algorithm}</td>
                          <td>
                            <ul className="stack-list">
                              <Library library={modelList[model].ml_library} />
                            </ul>
                          </td>
                          <td>
                            <div className="project-members-info">
                              <ul className="project-members-list">
                                <User user={modelList[model].owner} />
                              </ul>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                    <div className="model-score-row recent-model-counters">
                      <div className="score-card">
                        <span className="score-card-label">
                          HEALTH
                          <br />
                          (SCORE)
                        </span>
                        <span className={`score-card-value ${this.addScoreTextClassHealth(parseInt(modelList[model].health_score,10))}`}>
                          {parseInt(modelList[model].health_score,10)}
                        </span>
                      </div>
                      <div className="score-card">
                        <span className="score-card-label">DATA DRIFT</span>
                        <span className={`score-card-value ${this.addScoreTextClassDrift(parseInt(modelList[model].data_drift,10))}`}>
                          {parseInt(modelList[model].data_drift,10)}
                        </span>
                      </div>
                      <div className="score-card">
                        <span className="score-card-label" >PREDICTION COUNT</span>
                        <span className={`score-card-value ${parseInt(modelList[model].predictions_count,10)}`}>
                          {parseInt(modelList[model].predictions_count,10)}
                        </span>
                      </div>
                    </div>
                  </div>
                )}
              )}
          </Slider>
          :
          <div className="h180 content-center">No data available!</div>
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token
  };
};

export default connect(mapStateToProps)(RecentModelDeployment);
