import React from "react";
import ContentLoader from "react-content-loader";

const Loader1 = () => (
  <ContentLoader
    height={210}
    width={639}
    speed={2}
    primaryColor="#fbfbfb"
    secondaryColor="#eae2e2"
  >
    <rect x="201.36" y="59.27" rx="0" ry="0" width="0" height="0" />
    <rect x="233.55" y="51.27" rx="0" ry="0" width="0" height="0" />
    <rect x="245.55" y="53.27" rx="0" ry="0" width="0" height="0" />
    <rect x="31.02" y="21.27" rx="0" ry="0" width="578.85" height="48" />
    <rect x="31" y="85.27" rx="0" ry="0" width="578.9017" height="48" />
    <rect x="77" y="254.27" rx="0" ry="0" width="567.5" height="48" />
    <rect x="32" y="154.27" rx="0" ry="0" width="578.9017" height="48" />
  </ContentLoader>
);

const Loader2 = () => (
  <ContentLoader
    height={80}
    width={629}
    speed={2}
    primaryColor="#fbfbfb"
    secondaryColor="#eae2e2"
  >
    <rect x="201.36" y="59.27" rx="0" ry="0" width="0" height="0" />
    <rect x="237.55" y="44.27" rx="0" ry="0" width="0" height="0" />
    <rect x="249.55" y="46.27" rx="0" ry="0" width="0" height="0" />
    <rect x="35.02" y="14.27" rx="0" ry="0" width="162.08" height="48" />
    <rect x="230.02" y="14.27" rx="0" ry="0" width="162.08" height="48" />
    <rect x="426.02" y="14.27" rx="0" ry="0" width="162.08" height="48" />
  </ContentLoader>
);

const Loader3 = () => (
  <ContentLoader
    height={260}
    width={629}
    speed={2}
    primaryColor="#fbfbfb"
    secondaryColor="#eae2e2"
  >
    <rect x="201.36" y="59.27" rx="0" ry="0" width="0" height="0" />
    <rect x="237.55" y="44.27" rx="0" ry="0" width="0" height="0" />
    <rect x="249.55" y="46.27" rx="0" ry="0" width="0" height="0" />
    <rect
      x="23.5"
      y="21.27"
      rx="0"
      ry="0"
      width="581"
      height="202.16000000000003"
    />
    <rect x="313.5" y="92.27" rx="0" ry="0" width="0" height="0" />
  </ContentLoader>
);

export { Loader1, Loader2, Loader3 };
