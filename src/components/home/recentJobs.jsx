import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { getAllJobs } from "../../api";
import User from "../shared/User";
import moment from "moment";
import CardLoader from "../models/cardLoader";

class RecentJobs extends Component {
  _isMounted = false;
  state = {
    showLoader: true
  };

  componentDidMount() {
    this._isMounted = true;
    getAllJobs(this.props.token, {
      sort: "created_date,desc",
      size: 5
    })
      .then(response => {
        if (response.status && this._isMounted) {
          this.setState({
            jobsList: response.data,
            showLoader: false
          });
        }
      })
      .catch(error => {
        console.error("Error fetching recent workflows: ", error);
      });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    const { jobsList, showLoader } = this.state;
    return (
      <div className="card">
        <div className="card-header">
          <h4>RECENT WORKFLOWS</h4>
        </div>
        <div className="card-body">
          {showLoader ? (<CardLoader />) : (
            jobsList && jobsList.length ?
            <table className="table data-table no-border">
              <thead>
                <tr>
                  <td>Name</td>
                  <td>Created By</td>
                  <td>Created Date</td>
                  <td>Last Run</td>
                  <td>Number of Runs</td>
                  <td>Scheduled</td>
                </tr>
              </thead>
              <tbody>
                {jobsList.map((job, index) => {
                    return (
                      <tr key={index}>
                        <td>
                          <Link to={`/workflows/${job.id}`}>
                            {job.name}
                          </Link>
                          <p className="data-text"><small>{job.spec.project_name}</small></p>
                        </td>
                        <td>
                          <div className="project-members-info">
                            <ul className="project-members-list">
                              <User user={job.created_by} />
                            </ul>
                          </div>
                        </td>
                        <td>{job.created_date && moment(job.created_date).format("LL")}</td>
                        <td>{job.last_modified_date && moment(job.last_modified_date).format("LL")}</td>
                        <td>{job.number_of_runs}</td>
                        <td>
                          {job.job_schedule && job.job_schedule.type !== 'JOB_SCHEDULE_TYPE_NOT_SET' ? 'Yes' : 'No'}
                        </td>
                      </tr>
                    );
                  })}
              </tbody>
            </table>
          :
          <div className="h180 content-center">No data available!</div>
          )
        }
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token
  };
};

export default connect(mapStateToProps)(RecentJobs);
