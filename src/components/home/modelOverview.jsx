import React, { Component } from "react";
import { connect } from "react-redux";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { getAllModels } from "../../api";
import ModelOverviewDetails from "./modelOverviewDetails";
import { Link } from "react-router-dom";
import { Loader1 } from "./modelOverviewLoader";
import Library from "../shared/Library";
import {maxBy,slice} from 'lodash';
import ReactTooltip from "react-tooltip";


class ModelOverview extends Component {
  state = {
    failed: [],
    topModel: [],
    overviewLoader: true
  };

  async componentDidMount() {
    let failed = { ...this.state.failed };
    let topModel = { ...this.state.topModel };

    const failedData = await getAllModels(this.props.token, {
      statusEquals: "DEPLOYED",
      sort: "created_date,desc",
      size: 5,
      failingEquals: true
    });
    const topModelData = await getAllModels(this.props.token, {
      statusEquals: "DEPLOYED",
      sort: "created_date,desc",
      size: 5
    });

    failed = failedData.data;
    topModel = topModelData.data;
    this.setState({
      failed,
      topModel,
      selectedFailedModel: failed[0],
      selectedTopModel: topModel[0],
      overviewLoader: false
    });
  }

  onClickSetFailedModel = model => {
    this.setState({
      selectedFailedModel: model
    });
  };

  onClickSetTopModel = model => {
    this.setState({
      selectedTopModel: model
    });
  };

  getInsightsToolTipTable= (insights,tableHeading) => {
    return(
      <table className="table table-striped">
        <thead>
          <tr><td>{tableHeading}</td></tr>
        </thead>
        <tbody>
        <tbody>
          {insights &&
            insights.map((item, itemIndex) => {
              return (
                <tr key={itemIndex}>
                  {item &&
                    item.reason}
                </tr>
              );
            })}
        </tbody>
        </tbody>
      </table>
    )
  }

  recentInsight = (insights) => {
    if (insights && insights.length > 0){
      var insight = maxBy(insights,"timestamp")
      insights = (insights.length > 5) ? slice(insights,0,5) : insights
      return(
          <div
            className="prediction-more-info-link"
            data-tip="light"
          >
            <p data-tip='' data-for={insight.id}>
            {insight.reason}<i className="fa fa-info-circle" />
            </p>
            <ReactTooltip place="bottom" type="light" id={insight.id} >
              {this.getInsightsToolTipTable(insights,"Insights")}
            </ReactTooltip>
          </div>
      )
    }else{
      return (<div>NA</div>);
    }
  }


  topReasons = (insights) => {
    if (insights && insights.length > 0){
      var insight = maxBy(insights,"points_lost")
      insights = (insights.length > 5) ? slice(insights,0,5) : insights
      return(
          <div
            className="prediction-more-info-link"
            data-tip="light"
          >
            <p data-tip='' data-for={insight.id}>
            {insight.reason}<i className="fa fa-info-circle" />
            </p>
            <ReactTooltip place="bottom" type="light" id={insight.id} >
              {this.getInsightsToolTipTable(insights,"Reasons")}
            </ReactTooltip>
          </div>
      )
    }else{
      return (<div>NA</div>);
    }
  }

  render() {
    const { failed, topModel, overviewLoader } = this.state;
    return (
      <div className="card model-overview-card">
        <div className="card-header">
          <h4>Models In Production</h4>
        </div>
        <div className="card-body">
          <Tabs>
            <TabList className="tabs-rounded">
              <Tab className="failed-models-tab">
                FAILING
                <span className="tab-counter">{failed.length}</span>
              </Tab>
              <Tab>
                LIVE MODELS
                <span className="tab-counter">{topModel.length}</span>
              </Tab>
            </TabList>
            <TabPanel>
              <div className="row">
                <div className="col-md-6 model-overview-tabs">
                  {overviewLoader ? (
                    <Loader1 />
                  ) : (
                    <table className="table data-table no-border">
                      <thead>
                        <tr>
                          <td>Name</td>
                          <td>Top Reason</td>
                          <td>Version</td>
                          <td>Algorithm</td>
                          <td>Type</td>
                          <td>Stack</td>
                        </tr>
                      </thead>
                      <tbody className="table-tabs">
                        {failed.map((model, index) => {
                          return (
                            <tr
                              key={index}
                              className={
                                this.state.selectedFailedModel.id === model.id
                                  ? "active-table-tab"
                                  : "table-tab"
                              }
                              onClick={() => this.onClickSetFailedModel(model)}
                            >
                              <td>
                                <Link to={`/models/${model.id}`}>
                                  {model.name}
                                </Link>
                                <br />
                                <small className="text-gray">
                                  {model.project_name}
                                </small>
                              </td>
                              <td>
                                {
                                  this.topReasons(model.insights)
                                }
                              </td>
                              <td>{model.version}</td>
                              <td>{model.ml_algorithm}</td>
                              <td>{model.type}</td>
                              <td>
                                <ul className="stack-list">
                                  <Library library={model.ml_library} />
                                </ul>
                              </td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                  )}
                </div>

                <React.Fragment>
                  <ModelOverviewDetails
                    selectedModel={this.state.selectedFailedModel || {}}
                  />
                </React.Fragment>
              </div>
            </TabPanel>
            <TabPanel>
              <div className="row">
                <div className="col-md-6 fixed-height-table">
                  <table className="table data-table no-border">
                    <thead>
                      <tr>
                        <td>Name</td>
                        <td>Recent Insights</td>
                        <td>Version</td>
                        <td>Algorithm</td>
                        <td>Type</td>
                        <td>Stack</td>
                      </tr>
                    </thead>
                    <tbody className="table-tabs">
                      {topModel.map((model, index) => {
                        return (
                          <tr
                            key={index}
                            className={
                              this.state.selectedTopModel.id === model.id
                                ? "active-table-tab"
                                : "table-tab"
                            }
                            onClick={() => this.onClickSetTopModel(model)}
                          >
                            <td>
                              <Link to={`/models/${model.id}`}>
                                {model.name}
                              </Link>
                              <br />
                              <small className="text-gray">
                                {model.project_name}
                              </small>
                            </td>
                            <td>
                                {this.recentInsight(model.insights)}
                              </td>
                            <td>{model.version}</td>
                            <td>{model.ml_algorithm}</td>
                            <td>{model.type}</td>
                            <td>
                              <ul className="stack-list">
                                <Library library={model.ml_library} />
                              </ul>
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </table>
                </div>
                <ModelOverviewDetails
                  selectedModel={this.state.selectedTopModel || {}}
                />
              </div>
            </TabPanel>
          </Tabs>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token
  };
};

export default connect(mapStateToProps)(ModelOverview);
