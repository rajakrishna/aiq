import React, { Component } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { getAllModelHealthDetails } from "../../ducks/modelDetails";
import ModelHealthAreaChart from "../chart/ModelHealthAreaChart";
import {
  getAllModelHealth,
  getDataDriftsTrend,
  getPredictionCount
} from "../../api";
import { DataDriftChart } from "../chart/DataDrift";
import { PredictionLineChart } from "../chart/PredictionLineChart";

class ModelOverviewDetails extends Component {
  state = {
    selectedModel: {},
    modelHealthDetails: [],
    dataDrift: [],
    predictionData: [],
    duration: "now-1d"
  };

  componentDidMount() {
    this.props.getAllModelHealthDetails({
      modelIdEquals: this.props.selectedModel.id,
      timestampGreaterOrEqualThan: this.state.duration,
      interval: "day",
      sort: "timestamp,desc"
    });

    this.setState({ selectedModel: this.props.selectedModel });
  }

  UNSAFE_componentWillReceiveProps(nextProps) {

    if (nextProps.selectedModel.id !== this.state.selectedModel.id) {
      this.setState({ selectedModel: nextProps.selectedModel },() => this.callAllApi());
    }
    if (this.props.modelHealthDetails) {
      this.setState({ modelHealthDetails: this.props.modelHealthDetails });
    }
  }

  callAllApi = async() => {
    await this.modelHealthData(this.state.duration);
    await this.getChartData({ greaterOrEqualThan: this.state.duration})
    await this.getPredictionData({"prediction_date":this.state.duration,"interval":"hour"})
  }

  modelHealthData = (duration, interval="day") => {
    getAllModelHealth(this.props.token, {
      modelIdEquals: this.state.selectedModel.id,
      interval: interval,
      timestampGreaterOrEqualThan: duration,
      sort: "timestamp,desc"
    }).then(response => {
      if (response.status) {
        this.setState({ modelHealthDetails: response.data,duration: duration });
      }
    });
  };

  getChartData = data => {
    return getDataDriftsTrend(this.props.token, {
      ...data,
      id: this.state.selectedModel.id
    })
      .then(response => {
        if (response.status) {
          this.setState({ dataDrift: response.data, duration: data.greaterOrEqualThan });
        }
      })
      .catch(error => console.log(error));
  };

  getPredictionData = data => {
    return getPredictionCount(this.props.token, {
      interval: data.interval,
      greaterOrEqualThan: data.prediction_date,
      field: "prediction_date",
      modelIdEquals: this.state.selectedModel.id
    })
      .then(response => {
        if (response.status) {
          this.setState({ predictionData: response.data,duration: data.prediction_date });
        }
      })
      .catch(error => console.log(error));
  };

  healthScoreColor = score =>{
    if (score <= 50) {
      return "#dc3545";
    } else if (score > 50 && score < 75) {
      return "#ffc107";
    } else {
      return "#28a745";
    }
  }

  addScoreTextClass = score => {
    if (score <= 50) {
      return "text-danger";
    } else if (score > 50 && score < 75) {
      return "text-warning";
    } else {
      return "text-success";
    }
  };

  render() {
    const { selectedModel, dataDrift, predictionData } = this.state;
    return (
      <div className="col-md-6">
        {Object.keys(selectedModel).length !== 0 && (
          <Tabs className="model-card-tabs">
            <TabList className="counter-card-row inline-tabs">
              <Tab
                className="data-counter-card"
                selectedClassName="active-counter-card"
                onClick={() => this.modelHealthData("now-1d", "hour")}
              >
                <span className="counter-card-label">
                  HEALTH <br />
                  (SCORE)
                </span>
                <span
                  className={"counter-card-value " + this.addScoreTextClass(selectedModel.health_score)}
                >
                  {selectedModel.health_score.toFixed(2)}{" "}
                </span>
              </Tab>
              <Tab
                className="data-counter-card"
                selectedClassName="active-counter-card"
                onClick={() =>
                  this.getChartData({
                    greaterOrEqualThan: "now-1d"
                  })
                }
              >
                <span className="counter-card-label">DATA DRIFT</span>
                <span className="counter-card-value">
                  {selectedModel.data_drift}
                </span>
              </Tab>
              <Tab
                className="data-counter-card"
                selectedClassName="active-counter-card"
                onClick={() => this.getPredictionData({"prediction_date":"now-1d","interval":"hour"})}
              >
                <span className="counter-card-label">PREDICTIONS COUNT</span>
                <span className="counter-card-value">
                  {selectedModel.predictions_count}
                </span>
              </Tab>
            </TabList>
            <TabPanel>
              <Tabs>
                <TabList className="inline-tabs tabs-gray">
                  <Tab onClick={() => this.modelHealthData("now-1d", "hour")}>
                    LAST 1 DAY
                  </Tab>
                  <Tab onClick={() => this.modelHealthData("now-7d")}>
                    LAST 7 DAYS
                  </Tab>
                  <Tab onClick={() => this.modelHealthData("now-30d")}>
                    LAST MONTH
                  </Tab>
                </TabList>
                <TabPanel>
                  <ModelHealthAreaChart
                    modelHealthDetails={this.state.modelHealthDetails}
                    chartColor={this.healthScoreColor(selectedModel.health_score)}
                    format="LT"
                    dayOne={true}
                  />
                </TabPanel>
                <TabPanel>
                  <ModelHealthAreaChart
                    chartColor={this.healthScoreColor(selectedModel.health_score)}
                    format="ll"
                    modelHealthDetails={this.state.modelHealthDetails}
                  />
                </TabPanel>
                <TabPanel>
                  <ModelHealthAreaChart
                    chartColor={this.healthScoreColor(selectedModel.health_score)}
                    format="ll"
                    modelHealthDetails={this.state.modelHealthDetails}
                  />
                </TabPanel>
              </Tabs>
            </TabPanel>
            <TabPanel>
              <Tabs>
                <TabList className="inline-tabs tabs-gray">
                  <Tab
                    onClick={() =>
                      this.getChartData({
                        greaterOrEqualThan: "now-1d"
                      })
                    }
                  >
                    LAST 1 DAY
                  </Tab>
                  <Tab
                    onClick={() =>
                      this.getChartData({
                        greaterOrEqualThan: "now-7d"
                      })
                    }
                  >
                    LAST 7 DAYS
                  </Tab>
                  <Tab
                    onClick={() =>
                      this.getChartData({
                        greaterOrEqualThan: "now-30d"
                      })
                    }
                  >
                    LAST MONTH
                  </Tab>
                </TabList>
                <TabPanel>
                  <DataDriftChart data={dataDrift} dayOne={true} />
                </TabPanel>
                <TabPanel>
                  <DataDriftChart data={dataDrift} />
                </TabPanel>
                <TabPanel>
                  <DataDriftChart data={dataDrift} />
                </TabPanel>
              </Tabs>
            </TabPanel>
            <TabPanel>
              <Tabs>
                <TabList className="inline-tabs tabs-gray">
                  <Tab onClick={() => this.getPredictionData({"prediction_date":"now-1d","interval":"hour"})}>
                    LAST 1 DAY
                  </Tab>
                  <Tab onClick={() => this.getPredictionData({"prediction_date":"now-7d","interval":"day"})}>
                    LAST 7 DAYS
                  </Tab>
                  <Tab onClick={() => this.getPredictionData({"prediction_date":"now-30d","interval":"day"})}>
                    LAST MONTH
                  </Tab>
                </TabList>
                <TabPanel>
                  <PredictionLineChart data={predictionData} dayOne={true} />
                </TabPanel>
                <TabPanel>
                  <PredictionLineChart data={predictionData} />
                </TabPanel>
                <TabPanel>
                  <PredictionLineChart data={predictionData} />
                </TabPanel>
              </Tabs>
            </TabPanel>
          </Tabs>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token,
    modelHealthDetails: state.modelDetails.modelHealthDetails
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getAllModelHealthDetails
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModelOverviewDetails);
