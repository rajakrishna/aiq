import Filter from "../modelDetails/filter";
import React, { Component } from "react";
import MicroModal from "micromodal";
import moment from 'moment';
import {getAllFilteredPredictions} from "../../api";
import { connect } from "react-redux";
import getDefaultFilters from './default_filters';

export function buildFilters(predictionInputFilters,model_id){
  let filter = `model_id.equals=${model_id}&sort=prediction_date,desc`
  if(predictionInputFilters.output.output_value.length !== 0){
    if(predictionInputFilters.output.output_range === "output.in"){
      let array = predictionInputFilters.output.output_value.split(',');
      array.forEach(element => {
        filter = filter + `&${predictionInputFilters.output.output_range}=${Number(element)}`
      });
    } else {
      filter = filter + `&${predictionInputFilters.output.output_range}=${predictionInputFilters.output.output_value}`
    }
  }
  if(predictionInputFilters.probability.probability_value.length !== 0){
    if (predictionInputFilters.probability.probability_range === "probability.in") {
      let array = predictionInputFilters.probability.probability_value.split(',');
      array.forEach(element => {
        filter = filter + `&${predictionInputFilters.probability.probability_range}=${Number(element)}`
      });
    } else if(predictionInputFilters.probability.probability_range === "isBetween") {
      filter = filter + `&probability.greaterOrEqualThan=${predictionInputFilters.probability.probability_value}`
      filter = filter + `&probability.lessOrEqualThan=${predictionInputFilters.probability.probability_value2}`
    }else if(predictionInputFilters.probability.probability_range !== "isBetween" && predictionInputFilters.probability.probability_range !== "probability.in"){
      filter = filter + `&${predictionInputFilters.probability.probability_range}=${predictionInputFilters.probability.probability_value}`
    }
  }

  for (const i in predictionInputFilters.inputs) {
    if (predictionInputFilters.inputs.hasOwnProperty(i)) {
      if(predictionInputFilters.inputs[i].field.length !== 0 && predictionInputFilters.inputs[i].value.length !== 0){
        if(predictionInputFilters.inputs[i].range === "in"){
          let array = predictionInputFilters.inputs[i].value.split(',');
          array.forEach(element => {
            filter = filter + `&${predictionInputFilters.inputs[i].field}.${predictionInputFilters.inputs[i].range}=${Number(element)}`;
          });
        } else if (predictionInputFilters.inputs[i].range === "isBetween" && predictionInputFilters.inputs[i].value2.length !== 0){
          filter = filter + `&${predictionInputFilters.inputs[i].field}.greaterOrEqualThan=${predictionInputFilters.inputs[i].value}`;
          filter = filter + `&${predictionInputFilters.inputs[i].field}.lessOrEqualThan=${predictionInputFilters.inputs[i].value2}`
        }else {
          filter = filter + `&${predictionInputFilters.inputs[i].field}.${predictionInputFilters.inputs[i].range}=${predictionInputFilters.inputs[i].value}`
        }
      }
    }
  }

  for (const i in predictionInputFilters.reasons) {
    if (predictionInputFilters.reasons.hasOwnProperty(i)) {
      if(predictionInputFilters.reasons[i].field.length !== 0 && predictionInputFilters.reasons[i].value.length !== 0){
        if(predictionInputFilters.reasons[i].range === "in"){
          let array = predictionInputFilters.reasons[i].value.split(',');
          array.forEach(element => {
            filter = filter + `&${predictionInputFilters.reasons[i].field}.${predictionInputFilters.reasons[i].range}=${Number(element)}`;
          });
        } else if (predictionInputFilters.reasons[i].range === "isBetween" && predictionInputFilters.reasons[i].value2.length !== 0){
          filter = filter + `&${predictionInputFilters.reasons[i].field}.greaterOrEqualThan=${predictionInputFilters.reasons[i].value}`;
          filter = filter + `&${predictionInputFilters.reasons[i].field}.lessOrEqualThan=${predictionInputFilters.reasons[i].value2}`
        }else {
          filter = filter + `&${predictionInputFilters.reasons[i].field}.${predictionInputFilters.reasons[i].range}=${predictionInputFilters.reasons[i].value}`
        }
      }
    }
  }

  for (const i in predictionInputFilters.properties) {
    if (predictionInputFilters.properties.hasOwnProperty(i)) {
      if(predictionInputFilters.properties[i].field.length !== 0 && predictionInputFilters.properties[i].value.length !== 0){
        if(predictionInputFilters.properties[i].range === "in"){
          let array = predictionInputFilters.properties[i].value.split(',');
          array.forEach(element => {
            filter = filter + `&${predictionInputFilters.properties[i].field}.${predictionInputFilters.properties[i].range}=${element}`;
          });
        } else {
          filter = filter + `&${predictionInputFilters.properties[i].field}.${predictionInputFilters.properties[i].range}=${predictionInputFilters.properties[i].value}`
        }
      }
    }
  }

  if(predictionInputFilters.dateRange.startDate !== ''){
    filter = filter + `&prediction_date.greaterOrEqualThan=${moment(predictionInputFilters.dateRange.startDate).toISOString()}&prediction_date.lessOrEqualThan=${moment(predictionInputFilters.dateRange.endDate).toISOString()}`
  }else if(predictionInputFilters.timestamp){
    filter = filter + `&prediction_date.greaterOrEqualThan=${predictionInputFilters.timestamp}`
  }
  return filter
}

class PredictionInputFilter extends Component{
  state={
    predictionData: [],
    predictionInputFilters: this.props.defaultFilters || getDefaultFilters(),
  }

  async componentDidMount() {
    MicroModal.init({
      disableScroll: true,
      disableFocus: false,
      awaitCloseAnimation: false,
      debugMode: true
    });
  }

  UNSAFE_UNSAFE_componentWillReceiveProps(nextProps,previousProps){
    var result = JSON.stringify(previousProps.defaultFilters)  === JSON.stringify(nextProps.defaultFilters)
    if (!result){
      this.setState({predictionInputFilters:nextProps.defaultFilters})
    }
  }

  predictionOutputChange = e => {
    let predictionInputFilters = { ...this.state.predictionInputFilters };
    predictionInputFilters.output[e.target.name] = e.target.value;
    this.setState({ predictionInputFilters });
  };


  addPredictionInput = () => {
    let predictionInputFilters = this.state.predictionInputFilters;
    predictionInputFilters.inputs.push({
      id: predictionInputFilters.inputs.length,
      field: '',
      range:'equals',
      value: ''
    })

    this.setState({predictionInputFilters})
  }
  predictionProbablityChange = e => {
    let predictionInputFilters = { ...this.state.predictionInputFilters };
    predictionInputFilters.probability[e.target.name] = e.target.value;
    this.setState({ predictionInputFilters });
  };

  predictionInputChange = (e,index) => {
    let predictionInputFilters = { ...this.state.predictionInputFilters };
    predictionInputFilters.inputs[index][e.target.name] = e.target.value;
    this.setState({ predictionInputFilters });
  };


  deletePredictionInput = (id) => {
    let predictionInputFilters = this.state.predictionInputFilters;
    const inputs = predictionInputFilters.inputs.filter((item) => item.id !== id);
    predictionInputFilters.inputs = inputs;
    this.setState({predictionInputFilters})
  }

  addPredictionReason = () => {
    let predictionInputFilters = this.state.predictionInputFilters;
    predictionInputFilters.reasons.push({
      id: predictionInputFilters.reasons.length,
      field: '',
      range:'equals',
      value: ''
    })

    this.setState({predictionInputFilters})
  }

  deletePredictionReason = (id) => {
    let predictionInputFilters = this.state.predictionInputFilters;
    const reasons = predictionInputFilters.reasons.filter((item) => item.id !== id);
    predictionInputFilters.reasons = reasons;
    this.setState({predictionInputFilters})
  }

  addPredictionProperty = () => {
    let predictionInputFilters = this.state.predictionInputFilters;
    predictionInputFilters.properties.push({
      id: predictionInputFilters.properties.length,
      field: '',
      range:'',
      value: ''
    })

    this.setState({predictionInputFilters})
  }

  deletePredictionProperty = (id) => {
    let predictionInputFilters = this.state.predictionInputFilters;
    const properties = predictionInputFilters.properties.filter((item) => item.id !== id);
    predictionInputFilters.properties = properties;
    this.setState({predictionInputFilters})
  }

  handleApplyFilters = () => {
    // this.setState({ appliedFilters: this.state.predictionInputFilters });
    let { predictionInputFilters } = this.state;
    let filter = buildFilters(predictionInputFilters,this.props.modelID)
    getAllFilteredPredictions(this.props.token, filter)
    .then(response => {
      if (response.status) {
        this.setState({modelPrediction: response.data})
        this.props.handleFilter(response.data,predictionInputFilters)
      }
    })
    .catch(error => console.log(error))

  };

  handleResetFilters = () => {
    let filter = `model_id.equals=${this.props.modelID}&sort=prediction_date,desc`
    getAllFilteredPredictions(this.props.token, filter)
    .then(response => {
      if (response.status) {
        this.setState({modelPrediction: response.data,predictionInputFilters: getDefaultFilters()})
        this.props.handleFilter(response.data,getDefaultFilters())
      }
    })
    .catch(error => console.log(error))
  };

  render = ()=>{
    const {predictionInputFilters} = this.state
    return(
      <Filter
        filters={predictionInputFilters}
        applyFilters={() => this.handleApplyFilters()}
        resetFilters={this.handleResetFilters}
      >
        <div className="form-group row">
          <label className="col-md-3">Output</label>
          <div className="col-md-4">
            <select
              name="output_range"
              id=""
              className="form-control select-field"
              onChange={this.predictionOutputChange}
              defaultValue={predictionInputFilters.output.output_range}
            >
              <option value='output.equals'>equals</option>
              <option value='output.contains'>contains</option>
              <option value='output.in'>is on off</option>
            </select>
          </div>
          { predictionInputFilters && predictionInputFilters.output.output_range !== 'output.in' &&

            <input
              type="string"
              name="output_value"
              className="form-control input-sm col-md-3"
              placeholder="Ex: 0.11"
              onChange={this.predictionOutputChange}
              value={predictionInputFilters.output.output_value}
            />
          }
          { predictionInputFilters && predictionInputFilters.output.output_range === 'output.in' &&
              <input
                type="string"
                name="output_value"
                className="form-control input-sm col-md-3"
                placeholder="Ex: 0.11,0.2,0.33"
                onChange={this.predictionOutputChange}
                value={predictionInputFilters.output.output_value}
              />
          }
        </div>
        <div className="form-group row">
          <label className="col-md-3">probability</label>
          <div className="col-md-4">
            <select
              name="probability_range"
              id=""
              className="form-control select-field"
              onChange={this.predictionProbablityChange}
              defaultValue={predictionInputFilters.probability.probability_range}
            >
              <option value="probability.equals">is</option>
              <option value="probability.in">is one of</option>
              <option value="probability.lessThan">is less than</option>
              <option value="probability.lessOrEqualThan">is less or equal than</option>
              <option value="probability.greaterThan">is greater than</option>
              <option value="probability.greaterOrEqualThan">is greater or equal than</option>
              <option value="isBetween">is between</option>
            </select>
          </div>
          {predictionInputFilters &&
            predictionInputFilters.probability.probability_range !== 'output.in' &&
            predictionInputFilters.probability.probability_range !== 'isBetween' &&
          <input
            type="number"
            name="probability_value"
            className="form-control input-sm col-md-3"
            placeholder="Ex: 0.77"
            onChange={this.predictionProbablityChange}
            value={predictionInputFilters.probability.probability_value}
          />}
          {predictionInputFilters && predictionInputFilters.probability.probability_range === 'output.in' &&
          <input
            type="string"
            name="probability_value"
            className="form-control input-sm col-md-3"
            placeholder="Ex: 0.11,0.44,0.99"
            onChange={this.predictionProbablityChange}
            value={predictionInputFilters.probability.probability_value}
          />}
          {predictionInputFilters && predictionInputFilters.probability.probability_range === 'isBetween' &&
            <React.Fragment>
              <input
                type="string"
                name="probability_value"
                className="form-control input-sm col-md-2"
                placeholder="Ex: 0.11"
                onChange={this.predictionProbablityChange}
                value={predictionInputFilters.probability.probability_value}
              />
              <input
                type="string"
                name="probability_value2"
                className="form-control input-sm col-md-2"
                placeholder="Ex: 0.77"
                onChange={this.predictionProbablityChange}
                value={predictionInputFilters.probability.probability_value2}
              />
            </React.Fragment>
          }
        </div>
        <hr/>
        <div className="action-row">
          <h4>Prediction Input</h4>
          <div className="action-right-col">
            <button
              type="button"
              className="pull-right btn btn-primary btn-sm btn-rounded"
              onClick={() => this.addPredictionInput()}
            >
              +
            </button>
          </div>
        </div>
        <div>
          { predictionInputFilters && predictionInputFilters.inputs.map((input,index) => {
            return(
              <div className="form-group row" key={index}>
                <div className="col-md-3">
                  <input
                    type="string"
                    name="field"
                    className="form-control input-sm"
                    placeholder="field"
                    onChange={(e) =>this.predictionInputChange(e,index)}
                    value={input.field}
                  />
                </div>
                <div className="col-md-4">
                  <select
                    name="range"
                    id=""
                    className="form-control select-field"
                    onChange={(e) =>this.predictionInputChange(e,index)}
                    defaultValue={input.range}
                  >
                    <option value="equals">is</option>
                    <option value="in">is one of</option>
                    <option value="lessThan">is less than</option>
                    <option value="lessOrEqualThan">is less or equal than</option>
                    <option value="greaterThan">is greater than</option>
                    <option value="greaterOrEqualThan">is greater or equal than</option>
                    <option value="isBetween">is between</option>
                  </select>
                </div>
                { input.range !== "isBetween" &&
                  <input
                    type="string"
                    name="value"
                    className="form-control input-sm col-md-3"
                    placeholder={input.range === "in"? "Ex: val1,val2":"Ex: value" }
                    onChange={(e) => this.predictionInputChange(e,index)}
                    value={input.value}
                  />}
                { input.range === "isBetween" &&
                  <React.Fragment>
                    <input
                      type="string"
                      name="value"
                      className="form-control input-sm col-md-2"
                      placeholder="val1"
                      onChange={(e) => this.predictionInputChange(e,index)}
                      value={input.value}
                    />
                    <input
                      type="string"
                      name="value2"
                      className="form-control input-sm col-md-2"
                      placeholder="val2"
                      onChange={(e) => this.predictionInputChange(e,index)}
                      value={input.value2}
                    />
                  </React.Fragment>
                }
                <button
                  className="modal__close"
                  onClick={() => this.deletePredictionInput(input.id)}
                >X</button>
              </div>
            )})
          }
        </div>
      </Filter>
    );
  }

}

const mapStateToProps = state => {
  return {
    token: state.auth.token
  };
};

export default connect(mapStateToProps)(PredictionInputFilter);