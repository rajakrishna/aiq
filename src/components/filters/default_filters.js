export const getDefaultFilters = ()=> {
  return(  
    {
      output:{
        output_range: "output.equals",
        output_value: ""
      },
      probability: {
        probability_range: "probability.equals",
        probability_value: '',
        probability_value2: '',
      },
      inputs: [{
        id: 0,
        field: '',
        range:'equals',
        value: ''
      }],
      reasons: [{
        id: 0,
        field: '',
        range:'equals',
        value: ''
      }],
      properties: [{
        id: 0,
        field: '',
        range:'equals',
        value: ''
      }],
      dateRange: {
        startDate: '',
        endDate: ''
      }
    }
  )
}

export default getDefaultFilters;