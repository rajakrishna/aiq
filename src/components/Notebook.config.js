export const Config = {
	"spawnerFormDefaults": {
        "namespace": {
            "value": "kai"
        },
		"image": {
			"value": "gcr.io/aiops-224805/scipy-notebook:latest",
			"options": [
				"gcr.io/aiops-224805/base-notebook:latest",
				"gcr.io/aiops-224805/minimal-notebook:latest",
				"gcr.io/aiops-224805/r-notebook:latest",
				"gcr.io/aiops-224805/scipy-notebook:latest",
				"gcr.io/aiops-224805/tensorflow-notebook:latest",
				"gcr.io/aiops-224805/datascience-notebook:latest",
				"gcr.io/aiops-224805/pyspark-notebook:latest",
				"gcr.io/aiops-224805/all-spark-notebook:latest"
			],
			"readOnly": false
		},
		"cpu": {
			"value": "0.5"
		},
		"memory": {
			"value": "1.0Gi"
		},
		"workspaceVolume": {
			"value": {
				"type": {
					"value": "New"
				},
				"name": {
					"value": "notebook-workspace"
				},
				"size": {
					"value": "10"
				},
				"mountPath": {
					"readOnly": true,
					"value": "/home/jovyan"
				},
				"accessModes": {
					"value": "ReadWriteOnce"
				}
			}
		},
		"dataVolumes": {
			"value": []
		},
		"extraResources": {
			"value": "{}"
		},
		"maxVolumeSize":100
	},
	"packages": [
		{
		  packageDesc:"Includes popular package from the scientific Python ecosystem",
		  package: "gcr.io/aiops-224805/scipy-notebook:latest",
		  imagePath: "sklearnlogo_logo",
		  name: "scipy-notebook"
		},
		{
		  packageDesc: "Includes popular Python deep learning libraries",
		  package: "gcr.io/aiops-224805/tensorflow-notebook:latest",
		  imagePath: "tensorflowlogo_logo",
		  name: "tensorflow-notebook"
		},
		{
		  packageDesc: "Includes popular popular packages from the R ecosystem",
		  package: "gcr.io/aiops-224805/r-notebook:latest",
		  imagePath: "Rlogo_logo",
		  name: "r-notebook"
		},
		{
		  packageDesc:"Includes libraries for data analysis from the julia, Python and R communities ",
		  package: "gcr.io/aiops-224805/datascience-notebook:latest",
		  imagePath: "julialogo_logo",
		  name: "datascience-notebook"
		},
		{
		  packageDesc: "Includes Python, R and Scala support for Apache Spark",
		  package: "gcr.io/aiops-224805/all-spark-notebook:latest",
		  imagePath: "spark_logo_logo",
		  name: "all-spark-notebook"
		},
	  ],
}

export const sparkImages = [
	"gcr.io/aiops-224805/spark-py:2.4.5",
	"gcr.io/aiops-224805/spark-py:2.4.3",
	"gcr.io/aiops-224805/spark-py:2.4.2",
	"gcr.io/aiops-224805/spark-py:2.4.1"
] 