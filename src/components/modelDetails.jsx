import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import TrainingMetrics from "./modelDetails/trainingMetrics";
import Dataset from "./modelDetails/dataset";
import Runtime from "./modelDetails/runtime";
import Datadrift from "./modelDetails/datadrift";
import Bias from "./modelDetails/bias";
import LeftArrow from "../images/left-chevron.png";
import Rules from './Rules';
import { alertMsg } from "../ducks/alertsReducer";
import { confirm } from "./Confirm";
import { getModelDetails, downloadModelFile, saveNotes, saveRequirements } from "../ducks/modelDetails";
import { undeployModel, deleteModel, getAllInsights, downloadCancel } from "../api";
import Deployment from "./modelDetails/Deployment";
import Library from "./shared/Library";
import moment from "moment";
import User from "./shared/User";
import Mixpanel from "mixpanel-browser";
import { getErrorMessage, getResponseError, getDataSize } from "../utils/helper";
import Tooltip from "@material-ui/core/Tooltip";
import Joyride from 'react-joyride';
import {jobProto} from './Jobs/jobProto';
import Notes from './Notes';
import RequirementEditor from './modelDetails/RequirementEditor';
import {CircularProgress, LinearProgress, Dialog, DialogActions, DialogTitle, DialogContent} from '@material-ui/core';
import {customNotificationsList} from "../ducks/notifications";
import Axios from 'axios';
import { Copy } from "./Copy";
import { Link } from "react-router-dom";

const pathname = [
  '',
  'Runtime',
  'Data-drift',
  'Bias',
  'Rules',
  'Training-metrics',
  'Dataset',
  'Deployment'
]

class ModelDetails extends Component {
  state = {
    expjobs: null,
    expandable: false,
    editable: false,
    insights: [],
    currentUrl: "",
    currentIndex: 0,
    run: false,
    reloadFlag: true,
    stepIndex: 0,
    steps: [
      {
        target: '.model-details',
        content: (
          <div className="fl">
            <p>See the ML library and algorithm used to train the model.</p>
            <p>
              Track number of features used in each version and the time it
              took to train and validate the Model.
            </p>
          </div>
        ),
        placement: 'bottom',
        hideBackButton: true,
        hideCloseButton: true,
        disableBeacon: true,
        disableOverlayClose: true,
      },
      {
        target: '.insight-label',
        content: <div className="fl">Continuous insights to improve the quality of your ML model.</div>,
        hideBackButton: true,
        hideCloseButton: true,
        disableBeacon: true,
        disableOverlayClose: true,
      },
      {
        target: '.model-tabs',
        content: (
          <div className="fl">
            <h4>Training Metrics</h4>
            <p>
              Hyperparameters, Feature Significance, Performance Metrics, and
              Visualizations specific to the algorithm are automatically logged
              and displayed as part of Experiment details.
            </p>
          </div>
        ),
        hideBackButton: true,
        hideCloseButton: true,
        disableBeacon: true,
        disableOverlayClose: true,
      },
      {
        target: '#download_btn',
        content: (
          <div className="fl">
            Model Artifact is stored and available for download.
          </div>
        ),
        hideBackButton: true,
        hideCloseButton: true,
        disableBeacon: true,
        disableOverlayClose: true,
      },
      {
        target: '#deploy_btn',
        content: (
          <div className="fl">
            <p>Deploy the trained Model easily with a click of a button and get</p>
            <ul>
              <li>REST or gRPC API endpoint</li>
              <li>Monitor the deployed Model for performance and bias</li>
              <li>Autoscaling of the deployment</li>
              <li>A/B Test easily</li>
              <li>Combine with other models for complex use cases</li>
            </ul>
          </div>
        ),
        locale: { last: "Complete"},
        hideBackButton: true,
        hideCloseButton: true,
        disableBeacon: true,
        disableOverlayClose: true
      }
    ],
    loadPercent: 100,
    dataLoaded: 0,
    dataTotal: 0,
    reqExpandable: false,
  }
  downloadCancelToken = "";

  componentDidMount() {
    Mixpanel.track('Model Details Page', { 'model_id': this.props.match.params.id });
    this.props.getModelDetails(this.props.match.params.id);
    if (this.props.history.location.state) {
      let prevUrl = this.props.history.location.state.prevUrl;
      this.setState({ prevUrl })
    }

    getAllInsights(this.props.token, {
      modelIdEquals: this.props.match.params.id,
      sort: "timestamp,desc",
      pointsCategoryEquals: "model_train"
    }).then(res => {
      if (res.status === 200) {
        this.setState({
          insights: res.data
        })
      }
    });

    this.props.customNotificationsList(
      {entityId: this.props.match.params.id, type: "ERROR"}
    )
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const url = nextProps.location.pathname;
    let index = prevState.currentIndex;
    if (url !== prevState.currentUrl) {
      index = pathname.findIndex((e, i) => {
        if (url.includes(e))
          return i;
        return null
      })
      if (index < 1) {
        index = 0;
      } else {
        index--;
      }
    }
    if(nextProps.modelDetails.name) {
      const jobs = {...jobProto,
        name: nextProps.modelDetails.name + "-deploy",
        viewable: false,
        render_job: true,
        spec:{
          project_id: nextProps.modelDetails.project_id,
          project_name: nextProps.modelDetails.project_name,
          model_name: nextProps.modelDetails.name,
          deploy: {
            type: "DEPLOY",
            sub_type: "DEPLOY_SIMPLE",
            source: {
              type: "EXPERIMENT",
              experiment: {
                id: nextProps.modelDetails.id
              }
            }
          }
        }
      }
      return ({
        expjobs: jobs,
        currentUrl: url,
        currentIndex: index,
        run: nextProps.history.location.state ? nextProps.history.location.state.tutorial : false
      })
    }
    return ({
      currentUrl: url,
      currentIndex: index,
      run: nextProps.history.location.state ? nextProps.history.location.state.tutorial : false
    });
  }

  undeploy(id, name = "") {
    this.props.alertMsg("Undeploying the Model...", "info");
    undeployModel(this.props.token, id)
      .then(res => {
        if (res.status === 200) {
          Mixpanel.track('Undeploy Model Successful', { 'model_id': id });
          this.props.alertMsg(`Model ${name} undeployed!`, "success");
          this.props.history.go(".");
        } else {
          const errorMessage = getResponseError(res) || "An error occurred while undeploying the Model, please try again.";
          Mixpanel.track('Undeploy Model Failed', { 'model_id': id, error: errorMessage });
          this.props.alertMsg(errorMessage, "error");
        }
      })
      .catch(err => {
        const errorMessage = getErrorMessage(err) || "An error occurred while undeploying the Model, please try again.";
        Mixpanel.track('Undeploy Model Failed', { 'model_id': id, error: errorMessage });
        this.props.alertMsg(errorMessage, "error");
      })
  }

  handleJoyrideCallback = (data) => {
    const defaultCondition = data.action === "next" && data.lifecycle === "complete";
    if (defaultCondition && data.index === 4) {
      data.status = "finished";
      this.setState({
        stepIndex: ++data.index,
        reloadFlag: false
      })
      return 0;
    }
    if (defaultCondition) {
      this.setState({
        stepIndex: ++data.index
      }
      // Code Required to handle walkthrough
      // , () => {
        // if (this.state.stepIndex === 2) {
        //   document.querySelector(".hyper-parameters").click()
        // } else if (this.state.stepIndex === 3) {
        //   document.querySelector(".feature-significance").click();
        // } else if (this.state.stepIndex === 4) {
        //   document.querySelector(".performance-metrics").click();
        // } else if (this.state.stepIndex === 5) {
        //   document.querySelector(".confusion-matrix").click();
        // } else if (this.state.stepIndex === 6) {
        //   document.querySelector(".roc-curve").click();
        // } else if (this.state.stepIndex === 7) {
        //   document.querySelector(".lift-curve").click();
        // } else if (this.state.stepIndex === 8) {
        //   document.querySelector("#dataset-tab").click();
        // }
      // }
      )
    }
  }

  delExpAction = (id) => {
    this.props.alertMsg("Deleting the Model...", "info");
    deleteModel(this.props.token, id)
      .then(res => {
        if (res.status === 200) {
          Mixpanel.track('Delete Model Successful', { 'model_id': id });
          this.props.alertMsg("Model deleted successfully", "success");
          this.props.history.push("/experiments");
        } else {
          const errorMessage = getResponseError(res) || "An error occurred while deleting the Model, please try again.";
          Mixpanel.track('Delete Model Failed', { 'model_id': id, error: errorMessage });
          this.props.alertMsg(errorMessage, "error");
        }
      })
      .catch(err => {
        const errorMessage = getErrorMessage(err) || "An error occurred while deleting the Model, please try again.";
        Mixpanel.track('Delete Model Failed', { 'model_id': id, error: errorMessage });
        this.props.alertMsg(errorMessage, "error");
      })
  }

  handleNotesExpand = (editable=false) => {
    if(this.props.user.userName !== this.props.modelDetails.owner) {
      editable = false
    }
    const condition = this.state.expandable;
    this.setState({
      expandable: !condition,
      editable: editable
    })
  }

  handleReqExpand = (editable=false, exp=false) => {
    if(this.props.user.userName !== this.props.modelDetails.owner) {
      editable = false
    }
    const condition = this.state.reqExpandable;
    this.setState({
      reqExpandable: exp || !condition,
      reqEditable: editable
    })
  }

  previewNote = (notes) => {
    if(!notes) {
      return "Click to add notes...";
    }
    let notesObj = {},
        finaltext = "";

    try {
      notesObj = JSON.parse(notes);
      const text = notesObj.ops[0].insert;
      finaltext = text.slice(0,20);
      finaltext = finaltext.trim() ? finaltext.trim()+"..." : "Click to add notes...";
    } catch(err) {
      finaltext = "Click to add notes..."
    }

    return finaltext;
  }

  handleNoteSubmit = (data) => {
    this.setState({
      noteLoader: true
    })
    this.props.saveNotes(this.props.modelDetails.id, data,
      () => {
        this.setState({
          noteLoader: false,
          editable: false
        })
      },
      () => {
        this.setState({
          noteLoader: false,
          editable: false
        })
      }
    );
  }

  handleDownloadButtonClick = () => {
    const CancelToken = Axios.CancelToken;
    this.downloadCancelToken = CancelToken.source();
    this.props.downloadModelFile(this.props.match.params.id, this.monitorProgress, this.downloadCancelToken)
  }

  monitorProgress = (data) => {
    if(typeof data === 'object')
      this.setState({
        dataLoaded: data.loaded,
        dataTotal: data.total,
        loadPercent: Math.floor(data.loaded/data.total * 100),
      });
    else
      this.setState({
        dataLoaded: 0,
        dataTotal: 0,
        loadPercent: 100
      },
        this.props.alertMsg("Download Failed - "+data, "error")
      )
  }

  handleCancel = () =>{
    this.downloadCancelToken.cancel("Operation cancelled by user");
    this.setState({ 
      loadPercent: 100
    })
  }

  downloadDialog = () => {
    return(
      <Dialog
        open={this.state.loadPercent < 100}
        fullWidth={true}
        maxWidth={'sm'}
      >
        <DialogTitle id="run-dialog">
            Downloading...
        </DialogTitle>
        <DialogContent>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-sm-11 mt-sm">
                      {!this.state.dataLoaded && 
                        <div>
                          <CircularProgress size={16} />
                        </div>
                      }
                      <div className="inline-block" style={{width: "95%"}}>
                        <LinearProgress  variant="determinate" value={this.state.loadPercent} />
                      </div>
                      <span className="inline-block ml-sm clickable v-sub" onClick={()=>{this.handleCancel()}}>
                        <i className="fa fa-times" />
                      </span>
                    </div>
                    <div className="col-sm-12">
                      <span className="f-12 text-gray">
                        {getDataSize(this.state.dataLoaded) || "--"}/{getDataSize(this.state.dataTotal) || "--"}
                      </span>
                    </div>
                </div>
            </div>
        </DialogContent>
    </Dialog>
    )
  }

  render() {
    const { modelDetails, customNotification } = this.props;
    const model_file_id = modelDetails && modelDetails.model_file_id;
    const { insights,
            currentIndex,
            steps,
            stepIndex,
            run,
            reloadFlag,
            expandable,
            reqExpandable,
            editable,
            reqEditable,
            noteLoader,
            loadPercent } = this.state;
    const { url } = this.props.match;
    if (!Object.keys(modelDetails).length) {
      return (<div></div>);
    }
    return (
      <div className="main-container">
        <Joyride
          continuous={true}
          run={!!(run && reloadFlag)}
          stepIndex={stepIndex}
          spotlightPadding={0}
          scrollOffset={300}
          scrollToFirstStep={true}
          showProgress={false}
          showSkipButton={false}
          steps={steps}
          styles={{
            options: {
              width: 500
            }
          }}
          callback={this.handleJoyrideCallback}
        />
        <div className="main-body">
          <div className="content-wraper-sm">
            <div className="m-details-card">
              <div className="m-details-card-head pl-4 mr-5">
                {/* <div
                  className="back-action clickable"
                  onClick={() => { this.state.prevUrl ? this.props.history.push(this.state.prevUrl) : this.props.history.goBack() }}
                >
                  <img src={LeftArrow} alt="left arrow" />
                </div> */}
                <div className="m-title-info">
                  <h4>{modelDetails.name}</h4>
                  <small>{modelDetails.project_name}</small>
                </div>
                <div className="m-status-row">
                  {modelDetails.failing === false && (
                    <label className="model-item-status status-success">
                      {modelDetails.status}
                    </label>
                  )}
                  {modelDetails.failing === true && (
                    <label className="model-item-status status-failed">
                      {modelDetails.status}
                    </label>
                  )}
                </div>
                <div className="m-tech-stack">
                  <div className="inline-block">
                    <span className={!customNotification.length ? "insight-label dropdown-toggle content-none clickable mr" : "insight-label dropdown-toggle content-none clickable"} data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i className="fa fa-lightbulb"></i> {(insights && insights.length) || 0} Insight{insights && insights.length !== 1 ? "s" : ""}
                    </span>
                    <div className="dropdown-menu br-0 pd dropdown-menu-right mt-sm">
                      <div className="notification-wraper" style={{ maxWidth: 450 }}>
                        <ul className="notification-list">
                          {insights && insights.length ? insights.map((e, key) => {
                            return (
                              <li className="notification-item" key={key}>
                                <span className="notification-icon text-center">
                                  <i className="fa fa-lightbulb insight-icon mt-sm"></i>
                                </span>
                                <span className="notification-msg">
                                  <div>{e.reason}</div>
                                  <time>
                                    {moment(e.timestamp).format(
                                      "LLL"
                                    )}
                                  </time>
                                </span>
                              </li>
                            );
                          }) : <li>No Insights found!</li>}
                        </ul>
                      </div>
                    </div>
                  </div>
                  {customNotification.length ?
                  <div className="inline-block">
                    <span className="insight-label mr dropdown-toggle content-none clickable" id="errorDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i className="fa fa-exclamation-circle text-danger"></i>&nbsp;
                      {customNotification && !customNotification.length ? "0 Error" : customNotification.length+" Error"}
                    </span>
                    <div className="dropdown-menu br-0 pd dropdown-menu-right mt-sm" aria-labelledby="errorDropdown">
                      <div className="notification-wraper" style={{ maxWidth: 450 }}>
                        <ul className="notification-list">
                          {customNotification && customNotification.length ? customNotification.map((e, key) => {
                            return (
                              <li className="notification-item" key={"error"+key}>
                                <span className="notification-icon text-center">
                                  <i className="fa fa-exclamation-circle insight-icon mt-sm text-danger"></i>
                                </span>
                                <span className="notification-msg">
                                  <div>{e.message}</div>
                                  <time>
                                    {moment(e.timestamp).format(
                                      "LLL"
                                    )}
                                  </time>
                                </span>
                              </li>
                            );
                          }) : <li>No Error found!</li>}
                        </ul>
                      </div>
                    </div>
                  </div> : ""}
                  {modelDetails.status !== "DEPLOYED" ?
                    <Tooltip title={model_file_id ? "Publish this experiment!" : "Model Artifact not found!"}>
                      <button
                        type="button"
                        id="deploy_btn"
                        className={"btn btn-sm btn-default mr-sm " + (!model_file_id && "disabled")}
                        onClick={() => {
                          if (model_file_id) {
                            Mixpanel.track('Model Publish Button', { 'model_id': this.props.match.params.id });
                            // TODO: More detailed spec should be sent
                            const spec = {
                              name: modelDetails.name,
                              project_id: modelDetails.project_id,
                              project_name: modelDetails.project_name,
                              importFileType: "EXPERIMENT",
                            };
                            this.props.history.push("/import-model", { spec });
                          }
                        }}>Publish</button>
                    </Tooltip> :
                    <button type="button" className="btn btn-sm btn-default mr-sm">Published</button>
                  }
                  <Tooltip title={model_file_id ? "Download Model Artifact" : "Model Artifact not found!"}>
                    <button
                      type="button"
                      id="download_btn"
                      className={"btn btn-sm btn-default mr-sm " + (!model_file_id && "disabled")}
                      onClick={() => {
                          if (model_file_id && loadPercent === 100) {
                            Mixpanel.track('Model Download Button', { 'model_id': this.props.match.params.id });
                            this.setState({
                              loadPercent: 0
                            },
                            this.handleDownloadButtonClick()
                          )
                        }
                      }}
                    >
                      {loadPercent === 100 ? 
                        <i className="fa fa-download"></i> : 
                        <CircularProgress size={16} />
                      }
                      
                    </button>
                  </Tooltip>
                  {modelDetails.status !== "DEPLOYED" &&
                    <button type="button" className="btn btn-sm btn-danger" onClick={() => {
                      Mixpanel.track('Model Delete Button', { 'model_id': this.props.match.params.id });
                      confirm("Are you sure?").then(
                        () => {
                          this.delExpAction(this.props.match.params.id)
                        }
                      )
                    }}><i className="fa fa-trash"></i></button>
                  }
                </div>
              </div>

              <div className="m-info-table model-details mr-5">
                <table className="table data-table no-border modeldetails-table">
                  <thead>
                    <tr>
                      <td>Version</td>
                      <td>Type</td>
                      <td>Algorithm</td>
                      {modelDetails.source_code && modelDetails.source_code.runtime &&
                      <td>Runtime</td>}
                      <td>Stack</td>
                      <td>Created By</td>
                      <td>Created Date</td>
                      {modelDetails.status === "DEPLOYED" && <td>Deployed Date</td>}
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>{modelDetails.version}</td>
                      <td>{modelDetails.type}</td>
                      <td>{modelDetails.ml_algorithm}</td>
                      {modelDetails.source_code && modelDetails.source_code.runtime &&
                      <td className="model-library">
                        <div className="project-members-info">
                          <ul className="project-members-list">
                            <Library library={modelDetails.source_code.runtime} />
                          </ul>
                        </div>
                      </td>}
                      <td className="model-library">
                        <div className="project-members-info">
                          <ul className="project-members-list">
                            <Library library={modelDetails.ml_library} />
                          </ul>
                        </div>
                      </td>
                      <td>
                        <div className="project-members-info">
                          <ul className="project-members-list">
                            <User user={modelDetails.owner} />
                          </ul>
                        </div>
                      </td>
                      <td>
                        {modelDetails.created_date &&
                          moment(modelDetails.created_date).format("LL")}
                      </td>
                      {modelDetails.status === "DEPLOYED" && <td>
                        {modelDetails.last_modified_date &&
                          moment(modelDetails.last_modified_date).format("LL")}
                      </td>}
                    </tr>
                  </tbody>
                </table>
                <hr />
                <table className="table data-table no-border">
                  <thead>
                    <tr>
                      <td>Features</td>
                      <td>Training Duration</td>
                      <td>Testing Duration</td>
                      <td>
                      {modelDetails.source_code ?
                        "Source" : ""}</td>
                      <td>
                        {modelDetails.source_code ?
                          "Command" : ""}
                      </td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        {modelDetails.feature_significance
                          ? modelDetails.feature_significance.length
                          : 0}
                      </td>
                      <td className="training-duration">{modelDetails.training_duration || "-"}</td>
                      <td className="testing-duration">{modelDetails.testing_duration || "-"}</td>
                      <td>
                        {modelDetails.source_code ? modelDetails.source_code.git ?
                        <div>
                        <Copy mw={"auto"} cpyText={modelDetails.source_code.git.revision}>
                          <Tooltip title={modelDetails.source_code.git.repository}>
                            <a href={(modelDetails.source_code.git.repository ? modelDetails.source_code.git.repository.replace(".git","/-/tree" + modelDetails.source_code.git.revision) : "")}>
                              {modelDetails.source_code.git.revision}
                            </a>
                          </Tooltip>
                        </Copy>
                        </div> : "-" : ""}
                      </td>
                      <td>
                        {modelDetails.source_code ? modelDetails.source_code.command ?
                        <Copy mw={"auto"}>
                            {modelDetails.source_code.command}
                        </Copy> : "-" : ""}
                      </td>
                    </tr>
                  </tbody>
                </table>
                <hr />
                {modelDetails.source_code && 
                  <div>
                    <div>
                      <div 
                        className="mb clickable"
                        onClick={()=>{this.handleReqExpand()}}
                      >
                          Dependencies
                          <span
                              className={"f-12 pd-lr-sm"}
                          >
                              <i className={reqExpandable ? "fa fa-caret-down" : "fa fa-caret-right"}></i>
                          </span>
                      </div>
                    </div>
                    <RequirementEditor
                      saveRequirements={this.props.saveRequirements}
                      id={modelDetails.id}
                      req={modelDetails.requirements || modelDetails.source_code.requirements}
                      editable={reqEditable}
                      userName={this.props.user.userName}
                      owner={modelDetails.owner}
                      handleReqExpand={this.handleReqExpand}
                      expanded={reqExpandable}
                    />
                  </div>
                }
                <div className="pl-2">
                  <div 
                    className="mb clickable mt"
                    onClick={()=>{this.handleNotesExpand()}}
                  >
                      Notes
                      <span
                          className={"f-12 pd-lr-sm"}
                      >
                          <i className={expandable ? "fa fa-caret-down" : "fa fa-caret-right"}></i>
                      </span>
                  </div>
                </div>
                {expandable ?
                  <Notes
                    key={(new Date()).getTime()}
                    notes={modelDetails.notes}
                    id={modelDetails.id}
                    editable={editable}
                    saveNotes={this.handleNoteSubmit}
                    loader={noteLoader}
                    userName={this.props.user.userName}
                    owner={modelDetails.owner}
                  /> : 
                  <div 
                    className="text-dark-gray f-13 pd ml-2" 
                    style={{border: "1px solid #ccc"}}
                    onClick={()=>{this.handleNotesExpand(true)}}
                  >
                    {this.previewNote(modelDetails.notes)}
                  </div>
                  }
              </div>
              <div className="m-details-body model-tabs pd-tb mr-5">
                <div>
                  <ul className="inline-tabs tabs-blue pad-50">
                    {modelDetails.status === "DEPLOYED" && (
                      <React.Fragment>
                        <li className={`react-tabs__tab${currentIndex === 0 && "--selected"}`} onClick={() => {
                          this.setState({currentIndex:0})
                          Mixpanel.track('Model Details Page - Runtime Tab', { 'model_id': this.props.match.params.id });
                        }}>RUNTIME</li>
                        <li className={`react-tabs__tab${currentIndex === 1 && "--selected"}`} onClick={() => {
                            this.setState({currentIndex:1})
                          Mixpanel.track('Model Details Page - Data Drift Tab', { 'model_id': this.props.match.params.id });
                        }}>DATA DRIFT</li>
                        <li className={`react-tabs__tab${currentIndex === 2 && "--selected"}`} onClick={() => {
                            this.setState({currentIndex:2})
                          Mixpanel.track('Model Details Page - Bias Tab', { 'model_id': this.props.match.params.id });
                        }}>BIAS</li>
                        {process.env.REACT_APP_COMPONENT_RULES !== "DISABLED" && <li className={`react-tabs__tab${currentIndex === 3 && "--selected"}`} onClick={() => {
                              this.setState({currentIndex:3})
                            Mixpanel.track('Model Details Page - Rules Tab', { 'model_id': this.props.match.params.id });
                          }}>RULES</li>}
                      </React.Fragment>
                    )}
                    <li className={`react-tabs__tab${(currentIndex === 4 || (modelDetails.status !== "DEPLOYED" && currentIndex === 0)) && "--selected"}`}
                      onClick={() => {
                        this.setState({currentIndex:4})
                        Mixpanel.track('Model Details Page - Training Metrics Tab', { 'model_id': this.props.match.params.id });
                      }}>TRAINING METRICS</li>
                    <li id="dataset-tab" className={`react-tabs__tab${currentIndex === 5 && "--selected"}`} onClick={() => {
                      this.setState({currentIndex:5})
                      Mixpanel.track('Model Details Page - Dataset Tab', { 'model_id': this.props.match.params.id });
                    }}>DATASET</li>
                    {modelDetails.status === "DEPLOYED" && (
                      <li className={`react-tabs__tab${currentIndex === 6 && "--selected"}`} onClick={() => {
                        this.setState({currentIndex:6})
                        Mixpanel.track('Model Details Page - Deployment Tab', { 'model_id': this.props.match.params.id });
                      
                      }}>Deployment</li>
                    )}
                  </ul>
                  <hr className="ml-4 mr-5"/>
                  {modelDetails.status === "DEPLOYED" && (
                    <React.Fragment>
                      {currentIndex === 0 &&
                        <Runtime modelDetails={modelDetails} />
                      }
                      {currentIndex === 1 &&
                        <Datadrift modelDetails={modelDetails} />
                      }
                      {currentIndex === 2 &&
                        <Bias modelDetails={modelDetails} />
                      }
                      {currentIndex === 3 &&
                        <Rules modelDetails={modelDetails} />
                      }
                    </React.Fragment>
                  )}
                  {(currentIndex === 4 || (modelDetails.status !== "DEPLOYED" && currentIndex === 0)) &&
                    <TrainingMetrics {...this.props} />
                  }
                  {currentIndex === 5 &&
                    <div className="dataset-content" style={run ? { height: "200vh" } : {}}>
                      <Dataset modelDetails={modelDetails} />
                    </div>
                  }
                  {modelDetails.status === "DEPLOYED" && currentIndex === 6 &&
                    <Deployment modelDetails={modelDetails} />
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
        {this.downloadDialog()}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    modelDetails: state.modelDetails.modelDetails,
    modelFile: state.modelDetails.modelFile,
    customNotification: state.notification.customNotification,
    user: state.users.user,
    token: state.auth.token
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      customNotificationsList,
      downloadModelFile,
      getModelDetails,
      saveRequirements,
      saveNotes,
      alertMsg
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModelDetails);
