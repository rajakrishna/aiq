
export const SlackNotifications = {
    webhook_url : '',
}

export const EmailNotifications = {
    email_addresses : '',
}

export const MLWorkflowStepType = [
    'ML_WORKFLOW_STEP_TYPE_NOT_SET',
    'PRE_TRAIN',
    'TRAIN',
    'POST_TRAIN',
    'PRE_VALIDATE',
    'VALIDATE',
    'POST_VALIDATE',
    'VERIFY',
    'PRE_DEPLOY',
    'DEPLOY',
    'POST_DEPLOY'
]

export const MLWorkflowStepSubType = [
    'ML_WORKFLOW_STEP_SUB_TYPE_NOT_SET',
    'SIMPLE',
    'TRAIN_SIMPLE',
    'TRAIN_DISTRIBUTED',
    'DEPLOY_SIMPLE',
    'DEPLOY_TF_SERVING',
    'DEPLOY_AB_TEST',
    'DEPLOY_TRANSFORMER',
    'DEPLOY_GRAPH'
]

export const Runtime = [
    'RUNTIME_NOT_SET',
    'PYTHON2',
    'PYTHON3'
]


export const SourceType = [
    'REPO_TYPE_NOT_SET',
    'GIT_REPO'
]

export const TfVersion = [
    'TF_VERSION_NOT_SET',
    'TF_VERSION_1_10',
    'TF_VERSION_1_11',
    'TF_VERSION_1_12'
]

export const NotificationsType = [
    'NOTIFICATIONS_TYPE_NOT_SET',
    'SLACK',
    'EMAIL'
]

export const MLWorkflowPhase = [
    'PHASE_TYPE_NOT_SET',
    'CREATED',
    'STARTED',
    'RUNNING',
    'FAILED',
    'ERRORED',
    'SUCCEEDED',
    'TERMINATED'
]

export const ObjectMeta = {
    name : '',
    generateName : '',
    creationTimestamp : {},
    labels : '',
    annotations : '',
}

export const MLWorkflowStepOutput = {
    store : '',
    path : '',
}

export const DockerImage = {  //req || Check slack
    repository : '',
    name : '',
    tag : '',
}

export const ResourceRequirements = {
    
    cpu : 0,
    gpu : 0,
    tpu : 0,
    tf_version : '',
    memory : '',
    ps : 0,
    workers : 0,
}

export const KubernetesSecretReference = {
    name : '',
}

export const EnvVar = {
    name : '',
    value : '',
}

export const Notifications = {
    notifications_type : '',//NotificationsType,
    slack_notifications : SlackNotifications,
    email_notifications : EmailNotifications,
}

export const DeployMetrics = {
    name : '',
    goal : '',
}

export const GitRepoSource = {
    repository : '',
    revision : '',
    directory : '',
    secret : KubernetesSecretReference,
}

export const Source = {
    sourceType : '', //Dropdown
    gitRepoSource : GitRepoSource,  //req
    command : '',  //req
    image : '',   //req Docker image || serverimage
    runtime : '',  //req
    model_artifact_path : '',  //req only for TF
}

export const MLWorkflowDeployCondition = {
    compare_with_previous : false,
    deploy_metrics : DeployMetrics,
}

export const MLWorkflowStep = {
    name : '', //req
    type : MLWorkflowStepType,//MLWorkflowStepType, //req 
    sub_type : MLWorkflowStepType,//MLWorkflowStepSubType, //auto-assign Deploy || Train  //req
    deploy_condition : MLWorkflowDeployCondition,
    replicas : 0,  //req
    source : Source,  //req
    output : MLWorkflowStepOutput, 
    docker : DockerImage,  //req only in Deployment, Contains defaults
    resources : ResourceRequirements,  //req || optional
    env : EnvVar,  //req || optional
}

export const MLWorkflowSpec = {
    project_name : '', //req fetch from API // Dropdown
    model_name : '', //req
    // pre_train_steps : MLWorkflowStep,
    train : MLWorkflowStep, //req
    // post_train_steps : MLWorkflowStep,
    // pre_test_steps : MLWorkflowStep,
    // test : MLWorkflowStep,
    // post_test_steps : MLWorkflowStep,
    // pre_validate_steps : MLWorkflowStep,
    // validate : MLWorkflowStep,
    // post_validate_steps : MLWorkflowStep,
    // pre_deploy_steps : MLWorkflowStep,
    // deploy : {}, //req
    // post_deploy_steps : MLWorkflowStep,
    // image_pull_secrets : KubernetesSecretReference,
    // docker_push_secrets : KubernetesSecretReference,
    git_secrets : KubernetesSecretReference,
    global_env_variables : EnvVar,
    notifications : Notifications, //req || optional though
}


export const MLWorkflowStepStatus = {
    phase : '',
    type : '',
    started_at : {},
    finished_at : {},
    message : '',
    argo_workflow_name : '',
}

export const MLWorkflowStatus = {
    phase : '',
    started_at : {},
    finished_at : {},
    message : '',
    steps : MLWorkflowStepStatus,
}

export const MLWorkflow = {
   // apiVersion : '',
    //kind : '',
   //metadata : ObjectMeta,
    spec : MLWorkflowSpec,
    //status : MLWorkflowStatus,
}



export const MLWorkflowDeployABTestStep = {
    name : '', //req
    type : MLWorkflowStepType,//MLWorkflowStepType, //req
    sub_type : MLWorkflowStepType,//MLWorkflowStepSubType, //req Deploy
    deploy_condition : MLWorkflowDeployCondition,
    replicas : 0,  //req
    ratio : 0,  //req
    source_a : Source,  //req
    docker_a : DockerImage,  //req || default tag: a|b, model_a|_b
    resources : ResourceRequirements,  //req || optional
    env_a : EnvVar,  //req || op
    source_b : Source,  //req || op
    docker_b : DockerImage,  //req || default
    env_b : EnvVar,  //req || op
}

export const MLWorkflowDeployTransformerStep = { //same as AB
    name : '',
    type : MLWorkflowStepType,//MLWorkflowStepType,
    sub_type : MLWorkflowStepType,//MLWorkflowStepSubType,
    deploy_condition : MLWorkflowDeployCondition,
    replicas : 0,
    model_source : Source,
    model_docker : DockerImage,
    resources : ResourceRequirements,
    model_env : EnvVar,
    transformer_source : Source,
    transformer_docker : DockerImage,
    transformer_env : EnvVar,
}
