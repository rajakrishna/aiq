import React, { Component } from 'react';
import { connect } from "react-redux";

class Redirection extends Component {
    componentDidMount() {
        this.props.history.push(`/workflows/${this.props.match.params.id}`,{prevUrl: "/workflows/new"});
    }

    render() {
        return(
            <div className="main-container">
                <div className="main-body" style={{overflow: "hidden", paddingTop: 72}}>   
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-sm-12 pd-0 m-0">
                                Redirecting...
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token,
        user: state.users.user,
    };
};

export default connect(mapStateToProps)(Redirection);