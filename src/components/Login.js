import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Redirect, Link } from "react-router-dom";
import Mixpanel from "mixpanel-browser";
import { LoginUser, authReset } from "../ducks/auth";
import logo from "../images/predera-logo.png";

class Login extends Component {
  state = {
    email: "",
    accountName: "",
    password: "",
    emailValidationError: "",
    displayLoginError: true,
    loader: false,
  };

  componentDidMount() {
    Mixpanel.track("Login Page");
  }

  onSubmit = (e) => {
    this.props.authReset();
    e.preventDefault();
    const isUserWithAccountName = this.state.accountName.length !== 0;
    const loginDetails =  isUserWithAccountName ? { 
      userName: this.state.email.trim(),
      password: this.state.password,
      accountName: this.state.accountName

    } : {
      userName: this.state.email.trim(),
      password: this.state.password,
    };
    this.setState({ loader: true, displayLoginError: true });
    this.props.LoginUser(loginDetails);
  };

  static getDerivedStateFromProps(nextProps) {
    if (nextProps.auth.message || nextProps.auth.regMessage) {
      return {
        loader: false,
      };
    }
    return null;
  }

  validataAccountName = actName => {
    // if (actName.length === 0) {
    //   this.setState({error: "Account Name Is Blank"})
    // } else {
    //   this.setState({accountName: actName})
    // }
    this.setState({accountName: actName})
  }

  validateEmail = () => {
    const {email} = this.state
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (email.length === 0) {
      this.setState({ emailValidationError: "Email is required" });
    } else if (!re.test(email)) {
      this.setState({ emailValidationError: "Invalid Email" });
    } else {
      this.setState({ emailValidationError: "" });
    }
  };

  renderEmailValidationError = () => {
    return <div>
        {this.state.emailValidationError && (
      <div className="error-placement alert alert-danger">
        <i className="fa fa-exclamation-triangle" />
        <span>{this.state.emailValidationError}</span>
      </div>
    )}
    </div>
  }

  renderLoginError = () => {
    return (
    <div>
        {this.props.auth.message && (
          <div className="error-placement alert alert-danger">
          <i className="fa fa-exclamation-triangle" />
          <span>{this.props.auth.message}</span>
        </div>)}
    </div>)
  }

  render() {
    const token = this.props.auth && this.props.auth.token;
    const newPasswordRequired =
      this.props.auth && this.props.auth.newPasswordRequired;
    const { loader } = this.state;
    if (newPasswordRequired) {
      return (
        <Redirect to={`/initial_password_change/${this.state.email.trim()}`} />
      );
    }
    return token ? (
      <Redirect to={this.props.location.state.from.pathname} />
    ) : (
      <div className="auth-wraper">
        <form onSubmit={this.onSubmit}>
          <div className="auth-box">
            <div className="logo-head">
              <img src={logo} alt="" />
            </div>
            <h2 className="auth-heading">Sign In</h2>
            {/* error messages */}
            {this.renderEmailValidationError()}
            {this.state.displayLoginError && this.renderLoginError()}

            <div className="form-group form-material">
              <label>Username</label>
              <input
                value={this.state.email}
                onChange={(e) => this.setState({email: e.target.value})}
                onFocus={() => this.setState({emailValidationError: "", displayLoginError: false})}
                onBlur={this.validateEmail}
                type="text"
                className="form-control"
                placeholder="Enter email "
                required
              />
            </div>

            <div className="form-group form-material">
              <label>Password</label>
              <input
                value={this.state.password}
                onChange={e => this.setState({ password: e.target.value })}
                onFocus={() => this.setState({displayLoginError: false})}
                type="password"
                className="form-control"
                placeholder="Enter Password"
                required
              />
            </div>

            <div className="form-group form-material mb-4">
                <label>Account Name (optional)</label>
                <input
                  value={this.state.accountName}
                  onChange={e => this.validataAccountName(e.target.value)}
                  type="text"
                  className="form-control"
                  placeholder="Enter Account Name "
                />
            </div>

            <div className="form-group">
              <button
                type="submit"
                className="btn btn-primary form-btn"
                disabled={loader && "disabled"}
              >
                {loader ? "Signing in..." : "Sign In"}
              </button>
            </div>
            <div className="forgot-link">
              <Link to="/sign_up">Register</Link>
              &nbsp; &nbsp;
              <Link to="/forgot-password">Forgot Password?</Link>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    newPasswordRequired: state.newPasswordRequired,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators(
      {
        LoginUser,
        authReset,
      },
      dispatch
    ),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
