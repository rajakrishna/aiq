import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import moment from "moment";
import Pagination from "./common/pagination";
import User from "./shared/User";

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

import MicroModal from "micromodal";
import { Tooltip, Typography } from "@material-ui/core";
import {
  CircularProgress,
  LinearProgress,
  Dialog,
  DialogActions,
  DialogTitle,
  DialogContent,
  Grid,
  Button
} from "@material-ui/core";

import DateRangePicker from "react-bootstrap-daterangepicker";
import { startCase } from "lodash";
import Axios from "axios";

import CardLoader from "./models/cardLoader";
import { getDataSize } from "./../utils/helper";
import { getBatchScoreRuns, getBatchScoreRunsPerPage, downloadPrediction, deleteBatchScoreRunItem } from "./../ducks/deployments";
import { alertMsg } from "./../ducks/alertsReducer";
import { stringify } from "yamljs";
import { getBatchScoringRuns } from "../api";

const styles = theme => ({
  fab: {
    margin: theme.spacing.unit,
  },
  extendedIcon: {
    marginRight: theme.spacing.unit,
  },
  root: {
    paddingLeft: '0px',
    paddingRight: '31px',
    marginTop: '20px',
    marginBotton: '20px'
  },
});



class BatchScoringRunListComponent extends Component {
  state = {
    appliedFilters: {},
    filters: {},
    batchRunsPerPage: [],
    loadPercent: 100,
    dataLoaded: "",
    dataTotal: "",
    currentPage: 0,
    pageSize: 5
  };

  componentDidMount() {
    this.loadBatchScoreRuns();
    this.loadBatchScoreRunsPerPage();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.currentPage !== prevState.currentPage || prevProps.batchscoreruns.length !== this.props.batchscoreruns.length) {
      this.loadBatchScoreRunsPerPage();
    }
  }

  getBatchScoringRuns = (filters = { ...this.state.filters }) => {
    let appliedFilters = { ...filters };
    delete appliedFilters["name"];
    this.setState({
      appliedFilters: appliedFilters,
    });
    this.props.getBatchScoreRuns(filters);
  };

  getFilterTab = (data) => {
    if (data) {
      switch (typeof data) {
        case "object":
          if (data.value) {
            return `${data.key}-${data.value}`;
          } else if (data.key == "isBetween") {
            return `${data.key}-${data.value1}&${data.value2}`;
          } else if (data.value1) {
            return `${data.key}-${data.value1}`;
          } else {
            return moment(data).format("LL");
          }
        default:
          return data;
      }
    }
  };

  sortFilter = (e) => {
    const { deployment } = this.props;
    let filters = { ...this.state.filters };
    filters[e.target.name] = e.target.value;
    filters["name"] = deployment.metadata.name;
    this.applyFilters(filters);
  };

  handleResetFilters = () => {
    this.setState({ appliedFilters: {}, filters: {} });
    const { deployment } = this.props;
    let filters = { name: deployment.metadata.name };
    filters["name"] = deployment.metadata.name;
    this.getBatchScoringRuns(filters);
  };

  statusFilter = (e) => {
    const { deployment } = this.props;
    let filters = { ...this.state.filters };
    filters["name"] = deployment.metadata.name;
    filters[e.target.name] = e.target.value;
    this.setState({ filters });
  };

  handleApplyFilters = () => {
    this.getBatchScoringRuns(this.state.filters);
  };

  applyFilters = (filters) => {
    this.setState({ filters });
    this.getBatchScoringRuns(filters);
  };

  handleRemoveFilter = (filter) => {
    let updatedFilters = { ...this.state.filters };
    delete updatedFilters[filter];
    this.setState({ filters: updatedFilters });
    this.applyFilters(updatedFilters);
  };

  loadBatchScoreRuns = () => {
    const { deployment } = this.props;
    var obj = {
      startDate: null,
      endDate: null,
      name: deployment.metadata.name,
      sort: 'requested_date,desc'
    };
    this.props.getBatchScoreRuns(obj);
  }

  loadBatchScoreRunsPerPage = () => {
    const { deployment } = this.props;
    var obj = {
      startDate: null,
      endDate: null,
      name: deployment.metadata.name,
      pageNumber: this.state.currentPage,
      pageSize: this.state.pageSize,
      sort: 'requested_date,desc'
    };
    this.props.getBatchScoreRunsPerPage(obj);
  }

  handlePageChange = pageAction => {
    if (pageAction === 'previousPage') {
      this.setState(prevState => ({currentPage : prevState.currentPage - 1}));
    } else if (pageAction === 'nextPage') {
      this.setState(prevState => ({currentPage : prevState.currentPage + 1}));
    }
  };

  getColor = (type) => {
    console.log("status== " + type);
    const status = {
      CREATED: "info",
      FAILED: "failed",
      SUCCEEDED: "success",
    };
    const color = status[type] ? status[type] : "primary";
    return color;
  };

  getModifiedTimeString(date_1, date_2) {
    const duration = (date_2.getTime() - date_1.getTime()) / 1000;
      let outputDuration;
      if (duration < 60 ) {
        const outputCount = Math.floor(duration).toString();
        const outputCountTag = outputCount > 1 ? ' seconds' : ' second';
        outputDuration = outputCount + outputCountTag;
      } else if (duration >= 60 && duration < 3600) {
        const outputCount = (Math.floor(duration / 60)).toString();
        const outputCountTag = outputCount > 1 ? ' minutes' : ' minute';
        outputDuration = outputCount + outputCountTag;
      } else if (duration >= 3600 && duration < (3600 * 24)) {
        const outputCount = (Math.floor(duration / 3600)).toString();
        const outputCountTag = outputCount > 1 ? ' hours' : ' hour'
        outputDuration = outputCount + outputCountTag;
      } else if (duration >= (3600 * 24) && (duration < (3600 * 24 * 7))) {
        const outputCount = (Math.floor(duration / (3600 * 24))).toString();
        const outputCountTag = outputCount > 1 ? ' days' : ' day'
        outputDuration = outputCount + outputCountTag;
      } else if (duration >= (3600 * 24 * 7) && (duration < (3600 * 24 * 30))) {
        const outputCount = (Math.floor(duration / (3600 * 24 * 7))).toString();
        const outputCountTag = outputCount > 1 ? ' weeks' : ' week'
        outputDuration = outputCount + outputCountTag;
      } else if (duration >= (3600 * 24 * 30) && (duration < (3600 * 24 * 360))) {
        const outputCount = (Math.floor(duration / (3600 * 24 * 30))).toString();
        const outputCountTag = outputCount > 1 ? ' months' : ' month'
        outputDuration = outputCount + outputCountTag;
      } else if (duration >= 3600 * 24 * 360) {
        const outputCount = (Math.floor(duration / (3600 * 24 * 360))).toString();
        const outputCountTag = outputCount > 1 ? ' years' : ' year'
        outputDuration = outputCount + outputCountTag;
      }
      return outputDuration;
  }

  getDuration(e) {
    if (e.started_date && e.ended_date) {
      var date1 = new Date(e.started_date);
      var date2 = new Date(e.ended_date);
      const dateString = this.getModifiedTimeString(date1, date2)
      return dateString
    } else {
      var date3 = new Date(e.requested_date);
      var date4 = new Date();
      const dateString = this.getModifiedTimeString(date3, date4)
      return dateString+" ago"
    }
  }

  getFIleSize(file_size) {
    let outputFileSize
    if (file_size < 1024) {
      outputFileSize = file_size.toString() + ' bytes';
    } else if (file_size >= 1024 && file_size < 1048576) {
      outputFileSize = (Math.floor(file_size / 1024)).toString() + ' kb';
    } else if (file_size >= 1048576) {
      outputFileSize = (Math.floor(file_size / 1048576)).toString() + ' mb';
    }
    return outputFileSize;
  }

  getEndDate(end_date) {
    let displayEndDate
    if (end_date) {
      displayEndDate = moment(end_date).format('Do MMM YYYY, ddd, h:mm a');
    } else {
      displayEndDate = 'Not Completed';
    }
    return displayEndDate;
  }

  monitorProgress = (data) => {
    if (typeof data === "object")
      this.setState({
        dataLoaded: data.loaded,
        dataTotal: data.total,
        loadPercent: Math.floor((data.loaded / data.total) * 100),
      });
    else
      this.setState(
        {
          dataLoaded: 0,
          dataTotal: 0,
          loadPercent: 100,
        },
        this.props.alertMsg("Download Failed - " + data, "error")
      );
  };

  handleDownloadButtonClick = (id) => {
    const { deployment } = this.props;
    const CancelToken = Axios.CancelToken;
    this.downloadCancelToken = CancelToken.source();
    this.props.downloadPrediction(
      id,
      deployment.metadata.name,
      this.monitorProgress,
      this.downloadCancelToken
    );
  };

  handleDeleteBatchScoreItem = (name, id) => {
    const deleteLoadingMessage = "Deleting Batch Scoring Run. Please wait...";
    this.props.alertMsg(deleteLoadingMessage);
    this.props.deleteBatchScoreRunItem(name, id, function() {
      this.props.alertMsg(this.props.deleteMessage);
      this.loadBatchScoreRuns();
      this.loadBatchScoreRunsPerPage();
    }.bind(this));
  };

  handleCancel = () => {
    this.downloadCancelToken.cancel("Operation cancelled by user");
    this.setState({
      loadPercent: 100,
    });
  };

  handleEvent = (event, picker) => {
    const { deployment } = this.props;
    let filters = { ...this.state.filters };
    filters["startDate"] = picker.startDate._d;
    filters["endDate"] = picker.endDate._d;
    filters["name"] = deployment.metadata.name;
    //this.setState({ filters });
    this.applyFilters(filters);
    // getBatchScoringRuns(this.props.token, filters)
    //   .then((res) => {
    //     if (res.status === 200) {
    //       this.setState({
    //         batchRuns: res.data,
    //       });
    //     }
    //   })
    //   .catch((err) => {});
  };

  downloadDialog = () => {
    return (
      <Dialog
        open={this.state.loadPercent < 100}
        fullWidth={true}
        maxWidth={"sm"}
      >
        <DialogTitle id="run-dialog">Downloading...</DialogTitle>
        <DialogContent>
          <div className="container-fluid">
            <div className="row">
              <div className="col-sm-11 mt-sm">
                {!this.state.dataLoaded && (
                  <div>
                    <CircularProgress size={16} />
                  </div>
                )}
                <div className="inline-block" style={{ width: "95%" }}>
                  <LinearProgress
                    variant="determinate"
                    value={this.state.loadPercent}
                  />
                </div>
                <span
                  className="inline-block ml-sm clickable v-sub"
                  onClick={() => {
                    this.handleCancel();
                  }}
                >
                  <i className="fa fa-times" />
                </span>
              </div>
              <div className="col-sm-12">
                <span className="f-12 text-gray">
                  {getDataSize(this.state.dataLoaded) || "--"}/
                  {getDataSize(this.state.dataTotal) || "--"}
                </span>
              </div>
            </div>
          </div>
        </DialogContent>
      </Dialog>
    );
  };

  render() {
    const { appliedFilters, filters} = this.state;
    const { classes } = this.props;

    return (
      <div>
        <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
        >
            <Typography variant="h5">
              <div className={classes.root}>
                Batch Prediction History
              </div>
            </Typography>
            <Typography>
              <div className={classes.root}>
                <span>Batch Predictions Count : </span>
                <span className="h5">{this.props.batchscoreruns.length}</span>
              </div>
            </Typography>
        </Grid>
        <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
        >
          <div className={classes.root}>
            <button
              className="btn btn-primary btn-sm btn-rounded"
              onClick={() => MicroModal.show("modal-predictions-filter")}
            >
              Add Filter +
            </button>
          </div>
          <div className={classes.root}>
            <div className="d-flex flex-row align-items-center">
              <div className="d-flex flex-row align-items-center">
                <label style={{ minWidth: 50 }} htmlFor="batchscoringRunSortBy">
                  <small>Sort By</small>
                </label>
                <select
                  name="sort"
                  onChange={this.sortFilter}
                  id="batchscoringRunSortBy"
                  className="form-control select-field"
                  value={filters.sort || ""}
                  >
                    <option value="">Select</option>
                    <option value="requested_date,desc">Created</option>
                    <option value="file_name,asc">File Name</option>
                    <option value="file_size,asc">File Size</option>
                    <option value="workflow_status,asc">Status</option>
                    <option value="ended_date,desc">Completed</option>
                    <option value="ended_date,desc">Duration</option>
                </select>
              </div>
              <div className="col">
                <DateRangePicker
                  startDate={new Date()}
                  endDate={new Date()}
                  onApply={this.handleEvent}
                >
                  <button className="btn btn-primary btn-sm btn-rounded">
                    Select Date Range
                  </button>
                </DateRangePicker>
              </div>
              <div>
                <button onClick={() => {this.loadBatchScoreRuns(); this.loadBatchScoreRunsPerPage()}} type="button" class="btn btn-sm btn-default"><i class="fa fa-sync"></i></button>         
            </div>     
            </div>
          </div>
        </Grid>
        {/* <div class="data-sec-head mt-48 ml-15">
          <h3>Batch Prediction History</h3>
          <button type="button" class="btn btn-sm btn-default"><i class="fa fa-sync"></i></button>
        </div> */}
        
        {(this.props.batchscoreruns && this.props.batchscoreruns.length) ||
        this.state.card_loader ? (
          <div className="card-body h-min-370 p-0">
            {this.state.card_loader ? (
              <CardLoader />
            ) : (
              <table className="table data-table no-border mt-30">
                <thead>
                  <tr className="text-center">
                    <td>Created</td>
                    <td>File Name</td>
                    <td>File Size</td>
                    <td>Status</td>
                    <td>Completed</td>
                    <td>Duration</td>
                    <td>Requested By</td>
                  </tr>
                </thead>
                <tbody>
                  {this.props.batchscoreRunsPerPage.map((e, i) => {
                    return (
                      <tr className="text-center">
                        <td>{moment(e.requested_date).format('YYYY-MM-DD HH:MM:SS')}</td>
                        <td>{e.file_name}</td>
                        <td>
                          {this.getFIleSize(e.file_size)}
                          </td>
                        <td className="text-center">
                          <label
                            className={`model-item-status status-${this.getColor(
                              e.workflow_status
                            )}`}
                          >
                            {e.workflow_status}
                          </label>
                        </td>
                        <td>
                          {this.getEndDate(e.ended_date)}
                        </td>
                        <td>{this.getDuration(e)}</td>
                        <td>
                          <div className="project-members-info">
                            <ul className="project-members-list">
                              <User user={e.requested_by} />
                            </ul>
                          </div>
                        </td>
                        <td>
                          <Tooltip title="Download Prediction File">
                            <button
                              type="button"
                              id="download_btn"
                              className="btn btn-sm btn-default mr-sm "
                              onClick={() => {
                                this.setState(
                                  {
                                    loadPercent: 0,
                                  },
                                  this.handleDownloadButtonClick(e.workflow_id)
                                );
                              }}
                            >
                              <i className="fa fa-download"></i>
                            </button>
                          </Tooltip>
                            <button
                            onClick={() => {this.handleDeleteBatchScoreItem(e.model_name, e.workflow_id)}}
                            type="button" 
                            class="btn btn-sm btn-danger mr"
                            >
                              <i class="fa fa-trash"></i>
                            </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            )}
            {this.props.batchscoreruns.length > 10 && <div className={classes.root}>
                <Grid 
                  container
                  direction="row"
                  justify="flex-end"
                >
                  <Pagination 
                  itemsCount={this.props.batchscoreruns.length}
                  currentPage={this.state.currentPage}
                  currentPageSize={this.state.pageSize}
                  pageSize={10}
                  onPageChange={this.handlePageChange}
                  />
                </Grid>
            </div>
            }
          </div>
        ) : (
          <div className="empty-items-box">
            <h4>No Batch Prediction Runs found!</h4>
          </div>
        )}

        {this.downloadDialog()}
        <div
          className="modal micromodal-slide"
          id="modal-predictions-filter"
          aria-hidden="true"
        >
          <div className="modal__overlay" tabIndex="-1" data-micromodal-close>
            <div
              className="modal__container"
              role="dialog"
              aria-modal="true"
              aria-labelledby="modal-1-title"
            >
              <header className="modal__header">
                <h2 className="modal__title" id="modal-1-title">
                  Filter Models
                </h2>
                <button
                  className="modal__close"
                  aria-label="Close modal"
                  data-micromodal-close
                />
              </header>
              <main className="modal__content" id="modal-1-content">
                <div className="modal-filter-form">
                  <div className="form-group row">
                    <label htmlFor="" className="col-md-3">
                      Status
                    </label>
                    <select
                      name="status"
                      onChange={this.statusFilter}
                      id=""
                      className="col-md-8"
                      value={this.state.filters.status || ""}
                    >
                      <option value="">Select</option>
                      <option value="CREATED">Created</option>
                      <option value="COMPLETED">Completed</option>
                      <option value="FAILED">Failed</option>
                      <option value="RUNNING">Running</option>
                      <option value="SUCCEEDED">Succeeded</option>
                    </select>
                  </div>
                </div>
              </main>
              <footer className="modal__footer filter-form-actions mt-xxl">
                <button
                  className="btn btn-primary btn-rounded"
                  data-micromodal-close
                  onClick={this.handleApplyFilters}
                >
                  APPLY FILTERS
                </button>
                <button
                  className="btn btn-default btn-rounded"
                  data-micromodal-close
                  aria-label="Close this dialog window"
                  onClick={this.handleResetFilters}
                >
                  RESET FILTERS
                </button>
              </footer>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    batchscoreruns: state.deployments.batchscoreruns,
    batchscoreRunsPerPage: state.deployments.batchscorerunsPerPage,
    deleteMessage: state.deployments.message
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      getBatchScoreRuns,
      downloadPrediction,
      alertMsg,
      deleteBatchScoreRunItem,
      getBatchScoreRunsPerPage,
    },
    dispatch
  );
};

BatchScoringRunListComponent.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)((withStyles(styles)(BatchScoringRunListComponent)));
