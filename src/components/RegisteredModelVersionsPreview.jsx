import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { getRegisteredModelsVersion } from '../ducks/models';
import Library from "./shared/Library";
import moment from "moment";
import User from "./shared/User";
import { withRouter } from "react-router-dom";

class RegisteredModelVersionsPreview extends Component {

    state = {
        projectID: "",
        modelsList: [],
        appliedFilters: {},
        filters: {
            projectName: ""
        },
        currentPage: 0,
        totalModels: 0,
    }

    componentDidMount() {
        this.getModelVersions();
    }

    getModelVersions = (filters={...this.state.filters}) => {
        const { registeredModelId } = this.props;
        filters.registered_model_id = registeredModelId;
        filters.size = 4;
        filters.noLoader = true;
        this.props.getRegisteredModelsVersion(
            filters
        );
    }
    
    render() {
        const { registeredModelVersion } = this.props;
        return(
            <div style={{maxHeight: 270, overflow: "auto"}}>
            <table className="table data-table no-border">
                <thead>
                    <tr>
                        {/* <td>Name</td> */}
                        <td>Version</td>
                        <td>Type</td>
                        <td>Runtime</td>
                        <td>Algorithm</td>
                        <td>Stack</td>
                        <td>Owner</td>
                        <td>Created Date</td>
                        <td>Last Modified</td>
                        {/* <td></td> */}
                    </tr>
                </thead>
                <tbody>
                    {registeredModelVersion && registeredModelVersion.map((registeredModel, index) => {
                        return (
                            <tr key={"registered" + index} className="selectable-list" onClick={()=>{
                                this.props.history.push(`/registered-model-versions/${registeredModel.id}`);
                            }}>
                                {/* <td style={{ minWidth: 150 }}>
                                    <Link to={`/registered-model-versions/${registeredModel.id}`}>
                                        {registeredModel.registered_model_name}
                                    </Link>
                                    <p
                                        className="data-text blank-link clickable"
                                        onClick={() => { this.props.history.push('/projects/' + registeredModel.project_id) }}
                                    >
                                        <small>{registeredModel.project_name}</small>
                                    </p>
                                </td> */}
                                <td>
                                    {registeredModel.version}
                                </td>
                                <td>
                                    {registeredModel.type}
                                </td>
                                <td>
                                    {registeredModel.runtime ? registeredModel.runtime : "-"}
                                </td>
                                <td>
                                    {registeredModel.ml_algorithm  ? registeredModel.ml_algorithm : "-"}
                                </td>
                                <td>
                                    {registeredModel.ml_library && !(/NOT_SET/ig).test(registeredModel.ml_library) ?
                                    <div className="project-members-info">
                                        <ul className="project-members-list">
                                            <Library library={registeredModel.ml_library} />
                                        </ul>
                                    </div> : "-"}
                                </td>
                                <td>
                                    {registeredModel.created_by && <User user={registeredModel.created_by} />}
                                </td>
                                <td>
                                    {registeredModel.created_date && moment(registeredModel.created_date).format("LL")}
                                </td>
                                <td>
                                    {registeredModel.last_modified_date && moment(registeredModel.last_modified_date).format("LL")}
                                </td>
                                {/* <td className="p-0">
                                    <button
                                        style={{margin: 6}}
                                        className="btn btn-sm btn-danger"
                                        onClick={()=>{
                                            this.handleDelete(registeredModel.id);
                                        }}
                                    >
                                        <i className="fa fa-trash" />
                                    </button>
                                </td> */}
                            </tr>
                        );
                    })}
                </tbody>
            </table>    
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.users.user,
        token: state.auth.token,
        registeredModelVersion: state.models.registeredModelVersion,
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            getRegisteredModelsVersion
        },
        dispatch
    );
};


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(RegisteredModelVersionsPreview));
