import React, { Component } from "react";
import {Tooltip} from '@material-ui/core';
import moment from "moment";
import { capitalizeVars } from "../../utils/helper";
import { Link } from "react-router-dom";

export default class CompareExperiments extends Component {

    state = {
        allList: [
            'id',
            'name',
            'version',
            'type',
            'ml_algorithm',
            'created_date',
            'feature_significance',
            'training_duration',
            'testing_duration'
        ],
        compareList: [],
        currentField: "",
        search: null,
        des: 0
    }

    componentDidMount() {
        this.setState({ 
            compareList: this.props.compareList
        },this.prepareList(this.props.compareList));
    }

    sortList = (elem) => {
        const { compareList } = this.props;
        const { currentField, des } = this.state;
        const tempList = [...compareList]
        tempList.sort((obj1,obj2)=>{
            const field = elem.replace(/^HP\-|^PM\-/,"");
            let f1 = "",
                f2 = "";
            if((/^HP\-/).test(elem)) {
                f1 = parseFloat(obj1.hyperparameters[field]) || obj1.hyperparameters[field] || "";
                f2 = parseFloat(obj2.hyperparameters[field]) || obj2.hyperparameters[field] || "";
            } else if((/^PM\-/).test(elem)) {
                f1 = parseFloat(this.props.expandPerfMetrics(obj1.performance_metrics, field, obj1)) || 0;
                f2 = parseFloat(this.props.expandPerfMetrics(obj2.performance_metrics, field, obj2)) || 0;
            } else if(field === "created_date") {
                f1 = obj1[field] ? new Date(obj1[field]).getTime() : 0;
                f2 = obj2[field] ? new Date(obj2[field]).getTime() : 0;
            } else if(field === "feature_significance") {
                f1 = obj1[field] ? obj1[field].length : 0;
                f2 = obj2[field] ? obj2[field].length : 0;
            } else if((/duration/gi).test(field)) {
                f1 = obj1[field] ? parseFloat(obj1[field].replace(/\D/g,"")) : 0;
                f2 = obj2[field] ? parseFloat(obj2[field].replace(/\D/g,"")) : 0;
            } else {
                f1 = obj1[field] ? obj1[field] : 0;
                f2 = obj2[field] ? obj2[field] : 0;
            }
            
            if(typeof f1 === "string" && typeof f2 === "string") {
                if(des) {
                    return(f1.localeCompare(f2))
                } 
                return(f2.localeCompare(f1))
            } else {
                if(des) {
                    return f1 - f2;
                }
                return f2 - f1;
            }
        })
        this.setState({
            compareList: tempList,
            currentField: elem,
            des: !this.state.des
        })
    }

    prepareList = (modelsList) => {
        if(!modelsList) return;
        const allList = [...this.state.allList];
        let hyperKeys = [];
        let perfKeys = [];
        for(let i in modelsList) {
            const spec = modelsList[i];
            if(spec.hyperparameters) {
                const hKeys = Object.keys(spec.hyperparameters).map( e => {
                    return "HP-"+e
                })
                hyperKeys.push(hKeys);
                hyperKeys = hyperKeys.flat();
            }
            if(spec.performance_metrics && spec.performance_metrics.length) {
                const pKeys = spec.performance_metrics[0].metrics.map( e => {
                    return "PM-"+e.name
                })
                perfKeys.push(pKeys);
                perfKeys = perfKeys.flat();
            }
        }
        allList.push([...new Set(hyperKeys)]);
        allList.push([...new Set(perfKeys)]);
        const listing = allList.flat();
        this.setState({
            allList: listing,
        })
    }

    render() {
        const {allList} = this.state;
        const {compareList, des, currentField} = this.state;
        return(
            <div style={{display: 'flex', overflowX: 'hidden'}}>
                <div className="inline-block vertical-columns vertical-header pd">
                    {allList.map((e,i)=>{
                        const hyperFlag = (!allList[i-1] || (allList[i-1] && !allList[i-1].includes("HP-"))) && e.includes("HP-");
                        const perfFlag = (!allList[i-1] || (allList[i-1] && !allList[i-1].includes("PM-"))) && e.includes("PM-");
                        const field = e.replace(/^HP\-|^PM\-/,"");
                        return(<div className="clickable" key={e} onClick={()=>{this.sortList(e)}}>
                            {hyperFlag &&
                            <div className="text-dark-gray text-bold mt p-0">
                                Metrics:
                            </div>}
                            {perfFlag &&
                            <div className="text-dark-gray text-bold mt p-0">
                                Parameters:
                            </div>}
                            {
                                (/^HP\-|^PM\-/).test(e) ? 

                                <span className="pd-lr-sm">
                                    {capitalizeVars(field)}&nbsp;
                                    {currentField === e &&
                                        <i className={"fa " + (!des ? "fa-long-arrow-alt-up" : "fa-long-arrow-alt-down")}></i>
                                    }
                                </span>

                                : 
                                <span>
                                    {capitalizeVars(field)}&nbsp;
                                    {currentField === e &&
                                        <i className={"fa " + (!des ? "fa-long-arrow-alt-up" : "fa-long-arrow-alt-down")}></i>
                                    }
                                </span>
                            }
                        </div>)}
                    )}
                </div>
                <div class="of-auto" style={{display: "flex"}}>
                {compareList.map(e => {
                    return(
                        <div key={e.id} className="inline-block vertical-columns vertical-body pd">
                            {allList.map((elem,i) => {
                                const field = elem.replace(/^HP\-|^PM\-/,"");
                                const hyperFlag = (!allList[i-1] || (allList[i-1] && !allList[i-1].includes("HP-"))) && elem.includes("HP-");
                                const perfFlag = (!allList[i-1] || (allList[i-1] && !allList[i-1].includes("PM-"))) && elem.includes("PM-");
                                if((/^HP\-/).test(elem)) {
                                    return(
                                        <div key={e.id+elem}>
                                            {hyperFlag &&
                                                <div className="mt p-0 transparent-text">
                                                    Metrics:
                                                </div>}
                                                {perfFlag &&
                                                <div className="mt p-0 transparent-text">
                                                    Parameters:
                                                </div>
                                            }   
                                            {e.hyperparameters[field] || "-"}
                                        </div>
                                    )
                                }
                                if((/^PM\-/).test(elem)) {
                                    return(
                                        <div key={e.id+elem}> 
                                            {hyperFlag &&
                                                <div className="mt p-0 transparent-text">
                                                    Metrics:
                                                </div>}
                                                {perfFlag &&
                                                <div className="mt p-0 transparent-text">
                                                    Parameters:
                                                </div>
                                            }     
                                            {this.props.expandPerfMetrics(e.performance_metrics, field, e)}
                                        </div>
                                    )
                                }
                                if(field === "id") {
                                    return(
                                    <div key={e.id+elem}>
                                        {hyperFlag &&
                                            <div className="mt p-0 transparent-text">
                                                Metrics:
                                            </div>}
                                            {perfFlag &&
                                            <div className="mt p-0 transparent-text">
                                                Parameters:
                                            </div>
                                        }   
                                        <Link 
                                            to={`/monitoring/${e.id}`}
                                            target="_blank"
                                        >
                                            {e.id}
                                        </Link>
                                    </div>);
                                }
                                if(field === "created_date") {
                                    return(
                                        <div key={e.id+elem}>
                                            {hyperFlag &&
                                                <div className="mt p-0 transparent-text">
                                                    Metrics:
                                                </div>}
                                                {perfFlag &&
                                                <div className="mt p-0 transparent-text">
                                                    Parameters:
                                                </div>
                                            }   
                                            {e[field] && moment(e[field]).format("LL")}
                                        </div>
                                    )
                                }
                                if(field === "feature_significance") {
                                    return(
                                        <div key={e.id+elem}>
                                            {hyperFlag &&
                                                <div className="mt p-0 transparent-text">
                                                    Metrics:
                                                </div>}
                                                {perfFlag &&
                                                <div className="mt p-0 transparent-text">
                                                    Parameters:
                                                </div>
                                            }   
                                            {e[field] ? e[field].length : '-'}
                                        </div>
                                    )
                                }
                                return(
                                    <div key={e.id+elem}>
                                        {hyperFlag &&
                                            <div className="mt p-0 transparent-text">
                                                Metrics:
                                            </div>}
                                            {perfFlag &&
                                            <div className="mt p-0 transparent-text">
                                                Parameters:
                                            </div>
                                        }   
                                        {e[field] || "-"}
                                    </div>
                                )
                            })}
                        </div>
                    )
                })}
                </div>
            </div>
        );
    }
}
