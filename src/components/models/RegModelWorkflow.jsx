import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Dialog, DialogActions, DialogContent } from "@material-ui/core";
import { getjobRunDetails, clearJobRunDetails } from "../../ducks/jobRunDetails";
import { CircularProgress } from "@material-ui/core";
import { JobStatus } from "../Jobs/job-run/JobStatus";
import { JobLogs } from "../Jobs/job-run/JobLogs";
import { confirm } from "../Confirm";

class RegModelWorkflow extends Component {
    _isMounted = false;
    timeInterval = {};

    state = {
        showDialog: false,
        keyUpdate: 1
    }

    componentDidMount() {
        this._isMounted = true;
        this.props.registeredModelDetail.last_mlworkflow_run && this.fetchDetails();
    }

    fetchDetails() {
        this.timeInterval = setInterval(()=>{
            this.props.getjobRunDetails(this.props.registeredModelDetail.last_mlworkflow_run,
                (data)=>{
                    if(data && data.status && 
                        ['FAILED',
                        'ERRORED',
                        'SUCCEEDED',
                        'TERMINATED'].includes(data.status.phase)) {
                        clearInterval(this.timeInterval);
                    }
                },
                ()=>{
                    console.warn("Workflow Run log!")
                }
            )
            
        }, 6000)
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return({
            keyUpdate: prevState.keyUpdate + 1
        })
    }

    componentWillUnmount() {
        clearInterval(this.timeInterval);
        this.props.clearJobRunDetails();
        this._isMounted = false;
    }

    getColor(type) {
        const status = {
          'CREATED':"info",
          'STARTED':"info",
          'RUNNING':"info",
          'FAILED':"failed",
          'ERRORED':"failed",
          'SUCCEEDED':"success",
          'TERMINATED':"primary"
        }
        const color = status[type] ? status[type] : 'primary';
        return color;
    }

    longDate(timestamp) {
        if(!timestamp)
          return '';
    
        let dt = new Date(timestamp);
        return dt.toLocaleTimeString('en-us',{year: 'numeric', month: 'long', day: 'numeric' });
    }

    statusDialog(keyUpdate) {
        const {
            jobRunDetails,
            registeredModelDetail,
        } = this.props;

        return (
            <Dialog 
                open={this.state.showDialog}
                fullWidth={true}
                maxWidth={"md"}
            >
                <DialogContent>
                    <div className="deployment-dialog">
                        <h5>Deploy Logs</h5>
                        <JobStatus key={keyUpdate+"status"} type="deploy" jobStore={jobRunDetails} spec={jobRunDetails.spec.deploy}/>
                        <JobLogs key={keyUpdate+"logs"} token={this.props.token} expanded={false} name={registeredModelDetail.last_mlworkflow_run} type="deploy" jobStore={jobRunDetails} spec={jobRunDetails.spec.deploy}/> 
                    </div>
                </DialogContent>
                <DialogActions>
                    <button
                        type="button"
                        className="btn btn-primary btn-sm"
                        onClick={() => {
                            this.setState({
                                showDialog: false
                            })
                        }}
                    >
                        Close
                    </button>
                </DialogActions>
            </Dialog>
        )
    }

    render() {
        const {
            jobRunDetails,
            registeredModelDetail,
            deploying
        } = this.props;
        const {
            keyUpdate
        } = this.state;
        if(!registeredModelDetail.last_mlworkflow_run) {
            return ""
        } else if(deploying || (registeredModelDetail.last_mlworkflow_run && jobRunDetails && !jobRunDetails.status)) { 
            return (
                <button
                    className="btn btn-sm btn-default btn-disabled mr-sm"
                    type="button"
                >
                    <CircularProgress size={14} color={"inherit"} className="v-middle" /> Deploying
                </button>
            )
        } else if(registeredModelDetail.last_mlworkflow_run && jobRunDetails && jobRunDetails.status) {
            switch(this.getColor(jobRunDetails.status.phase)) {
                case "info":
                    return (
                        <div style={{display: "inline-block"}}>
                            <button
                                className="btn btn-sm btn-default btn-disabled mr-sm dropdown-toggle content-none"
                                data-toggle="dropdown" 
                                aria-haspopup="true" 
                                aria-expanded="false"
                                type="button"
                            >
                                <CircularProgress size={14} color={"inherit"} className="v-middle" /> Deploying
                            </button>
                            <div className="dropdown-menu dropdown-arrow">
                                <span className="dropdown-item clickable"
                                    onClick={()=>{
                                        this.setState({
                                            showDialog: true
                                        })
                                    }}
                                >
                                    Show Logs
                                </span>
                            </div>
                            {this.statusDialog(keyUpdate)}
                        </div>
                    )
                case "failed":
                case "primary":
                    return (
                        <div style={{display: "inline-block"}}>
                            <button
                                className="btn btn-sm btn-default mr-sm dropdown-toggle content-none"
                                data-toggle="dropdown" 
                                aria-haspopup="true" 
                                aria-expanded="false"
                                type="button"
                            >
                                <i className="fa fa-exclamation-triangle text-danger"></i> Deployment Failed
                            </button>
                            <div className="dropdown-menu dropdown-arrow">
                                <span className="dropdown-item clickable"
                                    onClick={
                                        this.props.handleDeploy
                                    }
                                >
                                    Retry Deploy
                                </span>
                                <span className="dropdown-item clickable"
                                    onClick={()=>{
                                        this.setState({
                                            showDialog: true
                                        })
                                    }}
                                >
                                    Show Logs
                                </span>
                            </div>
                            {this.statusDialog(keyUpdate)}
                        </div>
                    )
                case "success":
                    return (
                        <button
                            className="btn btn-sm btn-default mr-sm"
                            type="button"
                            onClick={()=>
                                confirm("Previous deployment was successful. Do you want to redeploy?", "Deploy").then(
                                    () => {
                                        this.props.handleDeploy()
                                    }
                                )
                            }
                        >
                            <i className="fa fa-check" style={{color: "#82c91e"}}></i> Deploy
                        </button>
                    )
            } 
        }
        return (
            ""
        );
    }
}
const mapStateToProps = state => {
    return {
        token: state.auth.token,
        jobRunDetails: state.jobRunDetails.jobRunDetails,
        loader: state.jobRunDetails.loader
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            getjobRunDetails,
            clearJobRunDetails,
        },
        dispatch
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(RegModelWorkflow);
