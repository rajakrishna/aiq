import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { TextField, MenuItem, Chip, CircularProgress } from '@material-ui/core';
import LeftArrow from "../../images/left-chevron.png";
import Dropzone from 'react-dropzone';
import { getExperimentList, getMlFlowList, fetchMlFlowRunList } from "../../ducks/experiments";
import { getProjectsList } from "../../ducks/projects";
import { fetchSecrets } from "../../ducks/jobStore"
import { submitModelCatalog } from "../../ducks/models"
import { alertMsg } from "../../ducks/alertsReducer"
import { uploadFile } from '../../api';
import MicroModal from "micromodal";
import { runtime as runtimeArr, mlLibraries as mlLibrariesArr, randomStrings, l, modelTypes } from '../../utils/helper';
import moment from "moment";
import CardLoader from "../models/cardLoader";
import { globalProjectSelection } from "../../ducks/projects"

class ImportModel extends Component {
    
    state = {
        name: "",
        model_type: "",
        description: "",
        importFileType: "FILE",
        runtime: "",
        ml_library: "",
        algorithm: "",
        experiment: "",
        ml_flow: "",
        project_id:this.props.selectedProject && this.props.selectedProject.id,
        filename: "",
        path: null,
        key: "",
        bucket: "",
        aws_secret: "",
        file: "",
        dependencies: "",
        file_id: "",
        readOnly: false,
        validatedImport: false,
        error: {}
    }

    componentDidMount() {
        this.props.getProjectsList({ size: 500 });
    }

    componentDidUpdate = (prevProps) => {
        if (this.props.selectedProject && prevProps.selectedProject.id !== this.props.selectedProject.id) {
            this.setState({ project_id: this.props.selectedProject.id}, () => this.handleChange({
                target: {
                    name: "importFileType",
                    value: this.state.importFileType
                }
            }));
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if(nextProps.history.location.state && !prevState.readOnly) {
            const spec = nextProps.history.location.state.spec;
            // TODO: Spec sent from Experiment page should work
            return { ...spec };
        }
        return null;
    }


    handleChange = (e) => {
        const target = e.target;
        const name = target.name;
        const value = (/NOT_SET/gi).test(target.value) ? "" : target.value;
        switch(name)  {
            case "importFileType":
                this.setState({
                    [name]: value,
                    file: "",
                    experiment: "",
                    s3: "",
                    ml_flow: "",
                    hostname: "",
                    run_id: ""
                }, ()=>{
                    if(!this.state.project_id) return;
                    this.props.getExperimentList({
                        statusEquals: 'EXPERIMENT',
                        projectIDEquals: this.state.project_id,
                        size: 5000
                    })
                    this.validateImport();
                    this.props.fetchSecrets({
                        project_id: this.state.project_id
                    });
                })
            return;
            case "project_id":
                this.setState({
                    [name]: value,
                    experiment: "",
                    aws_secret: "",
                }, () => {
                    this.props.getExperimentList({
                        statusEquals: 'EXPERIMENT',
                        projectIDEquals: value,
                        size: 5000
                    });
                    this.validateImport();
                    this.props.fetchSecrets({
                        project_id: value
                    });
                })
            return;
            case "experiment":
                const expDetails = this.getExperimentDetail(value);
                this.setState({
                    model_type: expDetails.type,
                    ml_library: expDetails.ml_library,
                    algorithm: expDetails.ml_algorithm,
                    dependencies: expDetails.requirements || (expDetails.source_code && expDetails.source_code.requirements),
                    runtime: expDetails.source_code ? expDetails.source_code.runtime : "",
                })
            break;
        }
        this.setState({
            [name]: value
        },()=>{
            if(["experiment", "bucket", "key", "hostname", "ml_flow"].includes(name)) {
                this.validateImport();
            }
        })
    }

    handleBlur = (e) => {
        const target = e.target;
        const name = target.name;
        const value = (/NOT_SET/gi).test(target.value) ? "" : target.value;
        const error = this.state.error;

        switch(name) {
            case "name":
            case "project_id":
            case "experiment":
            case "runtime":
            case "hostname":
                this.setState({
                    error: {...error, [name]: value ? null : true}
                })
            break;
        }
    }

    
    validateImport = () => {
        const {
            importFileType,
            experiment,
            key,
            bucket,
            file_id,
            filename,
            hostname,
            ml_flow
        } = this.state;
        if(
            (experiment && importFileType === "EXPERIMENT") || 
            (key && bucket && importFileType === "S3") || 
            ((file_id || filename) && importFileType === "FILE") ||
            (hostname && ml_flow && importFileType === "MLFLOW")) {
            this.setState({
                validatedImport: true
            })
        } else {
            this.setState({
                validatedImport: false
            })
        }
    }

    upload = (file) => {
        const name = file.name,
            {project_id, 
            ml_library, 
            algorithm, 
            model_type} = this.state;
        let extension = "";
        if (name.includes(".")) {
            extension = name.split(".");
            extension = "." + extension[extension.length - 1];
        }
        const uniqueStr = btoa((new Date).getTime()).replace(/[^\w]/g, "");
        const file_name = randomStrings(25, uniqueStr) + extension;
        const data = {
            file,
            model_file: JSON.stringify({
                "model_type": model_type,
                "ml_library": ml_library,
                "model_name": this.state.name,
                "ml_algorithm": algorithm,
                "project_name": this.getProjectName(project_id),
                "project_id": project_id,
                "filename": file_name
            })
        };
        this.setState({
            filename: name,
            uploading: true
        }, () => {
            uploadFile(this.props.token, data)
                .then((response) => {
                    const data = response.data;
                    this.setState({
                        uploading: false,
                        file_id: data.id
                    },()=>{
                        this.validateImport();
                        this.ImportModel();
                    })
                })
                .catch((error) => {
                    this.setState({
                        uploading: false,
                        filename: ""
                    })
                })
        });
    }

    getProjectName = (id) => {
        let name = "";
        this.props.projects.map((e) => {
            if (e.id === id) {
                name = e.name;
            }
        });
        return name;
    }

    getExperimentDetail = (id) => {
        let details = "";
        this.props.experimentList.map((e) => {
            if (e.id === id) {
                details = e;
            }
        });
        return details;
    }

    getColor = (type) => {
        const status = {
          'CREATED':"info",
          'STARTED':"info",
          'RUNNING':"info",
          'FAILED':"failed",
          'ERRORED':"failed",
          'SUCCEEDED':"success",
          'FINISHED':"success",
          'TERMINATED':"primary"
        }
        const color = status[type] ? status[type] : 'primary';
        return color;
    }

    handleSubmit = () => {
        if(this.props.loader || this.state.uploading || !this.validateInp()) return;
        const { importFileType, file } = this.state;
        if(importFileType === "FILE") {
            this.upload(file);
            return;
        }
        this.ImportModel();
    }

    ImportModel = () => {
        const { 
            id,
            name,
            model_type,
            description,
            project_id,
            importFileType,
            experiment,
            ml_library,
            key,
            bucket,
            aws_secret,
            algorithm,
            runtime,
            file_id,
            run_id,
            hostname,
            ml_flow,
            dependencies } = this.state;

        const data = {
            id: id || null,
            name,
            description,
            type: importFileType,
            runtime,
            dependencies: dependencies && dependencies.split("\n").join(","),
            ml_library,
            ml_algorithm: algorithm,
            model_type,
            project_id,
            project_name: this.getProjectName(project_id)
        }

        switch(importFileType) {
            case "EXPERIMENT":
                data.experiment_id = experiment;
                data.s3 = null;
                data.model_file_id = null;
                data.mlflow_experiment = null;
            break;
            case "MLFLOW":
                data.mlflow_experiment = {
                    "experiment_id": ml_flow,
                    "host": hostname,
                    "run_id": run_id
                };
                data.experiment_id = null;
                data.s3 = null;
                data.model_file_id = null;
            break;
            case "S3":
                data.s3 = {
                    path: null,
                    bucket,
                    key,
                    aws_secret
                };
                data.model_file_id = null;
                data.experiment_id = null;
                data.mlflow_experiment = null;
            break;
            case "FILE": 
            default:
                data.model_file_id = file_id
                data.s3 = null;
                data.experiment_id = null;
                data.mlflow_experiment = null;
            break;
        }
        this.delKeys(data);
        this.props.submitModelCatalog({
            ...data
        }, 
        () => {
            this.props.alertMsg("Model Imported successfully", "success"); this.props.history.push("/models");
        },
        () => {
            this.props.alertMsg("Error occurred while importing model, please check your input and try again!", "error");
        });
    }

    isEmpty(obj) {
        for(var key in obj) 
            return false;

        return true
    }
      
    delKeys(app){
        for(var key in app){
            if(app[key] !== null && typeof(app[key]) === 'object' && app[key] !== ""){
                this.delKeys(app[key])
        
                if(this.isEmpty(app[key])) {
                delete app[key]
                }
            } 
            if(app[key] === null || !app[key]) {
                delete app[key]
            }
        }
    }

    validateInp() {
        const { name,
                importFileType,
                runtime,
                ml_library,
                aws_secret,
                project_id,
                run_id
            } = this.state;

        if(importFileType === "MLFLOW" && !run_id) {
            return false
        }

        if(name && !!(importFileType === "MLFLOW" ^ !!ml_library) && !!(importFileType === "MLFLOW" ^ !!runtime) && !(importFileType === "S3" ^ !!aws_secret) && project_id) {
            return true;
        }
        return false;
    }

    render() {
        const { name,
            model_type,
            description,
            importFileType,
            experiment,
            dependencies,
            runtime,
            ml_library,
            key,
            bucket,
            aws_secret,
            ml_flow,
            algorithm,
            project_id,
            filename,
            uploading,
            validatedImport,
            readOnly,
            hostname,
            run_list,
            run_id,
            mlflowrunLoader,
            error } = this.state;
        const { experimentList, projects, expLoader, mlFlowLoader, mlFlowList, secretLoader, secretsList, loader } = this.props;
        return (
            <div className="main-container">
                <div className="main-body" style={{ overflow: "hidden" }}>
                    <div className="content-wraper-sm wd-half">
                        <div className="m-details-card pb-0">
                            <div className="m-details-card-head">
                                <div>
                                    <h4 className="inline-block datasets_title">CREATE MODEL</h4>
                                </div>
                                <div>
                                    <button className="btn btn-secondary ml-4"
                                            onClick={() => { this.state.prevUrl ? this.props.history.push(this.state.prevUrl) : this.props.history.goBack() }}
                                    >
                                        Cancel
                                    </button>
                                </div>
                            </div>
                            <div className="m-details-body">
                                <div className="container-fluid pl-4">
                                    <div className="row">
                                        <div className="col-sm-12">
                                            <div>
                                                <TextField
                                                    id={'name'}
                                                    label={"Name"}
                                                    name={'name'}
                                                    type={"text"}
                                                    placeholder={"Enter name"}
                                                    value={name}
                                                    onChange={this.handleChange}
                                                    onBlur={this.handleBlur}
                                                    error={!!error.name}
                                                    helperText={error.name && "This field is required!"}
                                                    margin="normal"
                                                    fullWidth
                                                    disabled={readOnly}
                                                    InputLabelProps={{
                                                        shrink: true,
                                                    }}
                                                />
                                                <TextField
                                                    id={'description'}
                                                    label={"Description"}
                                                    name={'description'}
                                                    type={"text"}
                                                    multiline={false}
                                                    rows={4}
                                                    rowsMax={4}
                                                    placeholder={"Enter catalog description"}
                                                    value={description}
                                                    onChange={this.handleChange}
                                                    margin="normal"
                                                    fullWidth
                                                    disabled={readOnly}
                                                    InputLabelProps={{
                                                        shrink: true,
                                                    }}
                                                />                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {project_id &&
                        <div className="m-details-card mt p-0">
                            <div className="container-fluid pl-4">
                                <div className="row">
                                    <div className="col-sm-12">
                                        <div className="m-details-body">
                                            <div>
                                                <div className="text-dark-gray f-12 mt-lg">
                                                    Import From
                                                </div>
                                                <ul className="inline-tabs tabs-blue mt mb p-0 inline-block">
                                                    <li 
                                                        className={`react-tabs__tab${importFileType === "FILE" && "--selected"}`}
                                                        onClick={()=>{
                                                            this.handleChange({
                                                                target: {
                                                                    name: "importFileType",
                                                                    value: "FILE"
                                                                }
                                                            })
                                                        }}
                                                    >
                                                        Desktop
                                                    </li>
                                                    <li 
                                                        className={`react-tabs__tab${importFileType === "EXPERIMENT" && "--selected"}`}
                                                        onClick={()=>{
                                                            this.handleChange({
                                                                target: {
                                                                    name: "importFileType",
                                                                    value: "EXPERIMENT"
                                                                }
                                                            })
                                                        }}
                                                    >
                                                        Experiment
                                                    </li>
                                                    <li 
                                                        className={`react-tabs__tab${importFileType === "S3" && "--selected"}`}
                                                        onClick={()=>{
                                                            this.handleChange({
                                                                target: {
                                                                    name: "importFileType",
                                                                    value: "S3"
                                                                }
                                                            })
                                                        }}
                                                    >
                                                        Cloud
                                                    </li>
                                                    <li 
                                                        className={`react-tabs__tab${importFileType === "MLFLOW" && "--selected"}`}
                                                        onClick={()=>{
                                                            this.handleChange({
                                                                target: {
                                                                    name: "importFileType",
                                                                    value: "MLFLOW"
                                                                }
                                                            })
                                                        }}
                                                    >
                                                        ML Flow
                                                    </li>
                                                </ul>
                                            </div>
                                            {importFileType === "FILE" && !filename &&
                                                <div className="mt">
                                                    <Dropzone onDrop={acceptedFiles => {
                                                        if (acceptedFiles) {
                                                            this.setState({
                                                                filename: acceptedFiles[0].name,
                                                                file: acceptedFiles[0]
                                                            })
                                                            this.validateImport();
                                                        }
                                                    }}>
                                                        {({ getRootProps, getInputProps }) => (
                                                            <div {...getRootProps()} className="content-center upload-space">
                                                                <input {...getInputProps()} />
                                                                <p>
                                                                    <i
                                                                        className="fa fa-file-upload"
                                                                        style={{ fontSize: 60 }}
                                                                    />
                                                                </p>
                                                                <p>Drag 'n' drop file here, or click to select file</p>
                                                            </div>
                                                        )}
                                                    </Dropzone>
                                                </div>    
                                                }
                                                {importFileType === "FILE" && filename &&
                                                <div style={{height: 263}}>
                                                    <Chip
                                                        label={filename}
                                                        onDelete={() => {
                                                            this.setState({
                                                                filename: "",
                                                                file_id: ""
                                                            })
                                                        }}
                                                    />
                                                </div>
                                                }
                                                {importFileType === "EXPERIMENT" && !expLoader &&
                                                    <div>
                                                        <div className="scrollable-table">
                                                            <div className="scrollable-table-header text-gray">
                                                                <div>
                                                                    <span>Name</span>
                                                                    <span>Version</span>
                                                                </div>
                                                            </div>
                                                            <div className="scrollable-table-body">
                                                            {experimentList.length ? experimentList.map((e, i) => {
                                                                return (
                                                                    <div 
                                                                        key={"experiment-column"+i}
                                                                        className={`clickable selectable-list ${experiment === e.id && "selected-item"}`}
                                                                        onClick={
                                                                            ()=>{
                                                                                this.handleChange({
                                                                                    target: {
                                                                                        name: "experiment",
                                                                                        value: e.id
                                                                                    }
                                                                                })
                                                                            }
                                                                        }
                                                                    >
                                                                        <span>
                                                                            {e.name}
                                                                        </span>
                                                                        <span>
                                                                            {e.version}
                                                                        </span>
                                                                    </div>
                                                                )
                                                            }) :
                                                                <div className="pd-lg text-center text-gray">
                                                                    No Experiments found in this project!
                                                                </div>
                                                            }
                                                            </div>
                                                        </div>
                                                    </div>
                                                }
                                                {importFileType === "MLFLOW" &&
                                                    <div style={{minHeight: 263}}>
                                                        <div className="container-fluid">
                                                            <div className="row">
                                                                <div className="col-sm-10">
                                                                    <TextField
                                                                        id={'hostname'}
                                                                        label={"Hostname"}
                                                                        name={'hostname'}
                                                                        type={"text"}
                                                                        placeholder={"Enter hostname"}
                                                                        value={hostname}
                                                                        onChange={this.handleChange}
                                                                        onBlur={this.handleBlur}
                                                                        error={!!error.hostname}
                                                                        helperText={error.hostname && "This field is required!"}
                                                                        margin="normal"
                                                                        fullWidth
                                                                        InputLabelProps={{
                                                                            shrink: true,
                                                                        }}
                                                                    />
                                                                </div>
                                                                <div className="col-sm-2">
                                                                    <button 
                                                                        className="btn btn-default"
                                                                        style={{marginTop: 25}}
                                                                        type="button"
                                                                        onClick={()=>{
                                                                            if(hostname)
                                                                            this.props.getMlFlowList(
                                                                                {hostname: hostname},
                                                                                ()=>{
                                                                                    return;
                                                                                },
                                                                                (err)=>{
                                                                                    this.props.alertMsg(err,"error");
                                                                                })
                                                                            else this.props.alertMsg("Hostname is required!","error");
                                                                        }}
                                                                    >
                                                                        Go {mlFlowLoader && <CircularProgress size={14} />}
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        {hostname && !mlFlowLoader && mlFlowList && mlFlowList.length ?
                                                        <div className="scrollable-table mlflow">
                                                            <div className="scrollable-table-header text-gray">
                                                                <div>
                                                                    <span>Artifact</span>
                                                                    <span>Status</span>
                                                                </div>
                                                            </div>
                                                            <div className="scrollable-table-body">
                                                            {mlFlowList.length ? mlFlowList.map((e, i) => {
                                                                return (
                                                                    <div 
                                                                        key={"ml_flow"+i}
                                                                        className={`clickable selectable-list ${ml_flow === e.experiment_id && "selected-item"}`}
                                                                        onClick={
                                                                            ()=>{
                                                                                this.handleChange({
                                                                                    target: {
                                                                                        name: "ml_flow",
                                                                                        value: e.experiment_id
                                                                                    }
                                                                                });
                                                                                this.setState({
                                                                                    mlflowrunLoader: true
                                                                                })
                                                                                this.props.fetchMlFlowRunList({
                                                                                    id: e.experiment_id,
                                                                                    hostname: hostname
                                                                                }, (list)=>{
                                                                                    this.setState({
                                                                                        run_list: list.runs,
                                                                                        mlflowrunLoader: false
                                                                                    })
                                                                                })
                                                                            }
                                                                        }
                                                                    >
                                                                        <span>
                                                                            {e.name}
                                                                        </span>
                                                                        <span>
                                                                            {e.artifact_location}
                                                                        </span>
                                                                    </div>
                                                                )
                                                            }) :
                                                                ""
                                                            }
                                                            </div>
                                                        </div> : 
                                                        <div className="pd-lg text-center text-gray">
                                                            No ML Flow Experiments found!
                                                        </div>}
                                                    </div>
                                                }
                                                {importFileType === "S3" &&
                                                    <div>
                                                        <span className="text-gray">S3</span>
                                                        <TextField
                                                            label="Bucket"
                                                            fullWidth
                                                            value={bucket}
                                                            name={"bucket"}
                                                            onChange={this.handleChange}
                                                            margin="normal"
                                                            InputLabelProps={{
                                                            shrink: true,
                                                            }}
                                                        />
                                                        <TextField
                                                            label="Key"
                                                            fullWidth
                                                            value={key}
                                                            name={"key"}
                                                            onChange={this.handleChange}
                                                            margin="normal"
                                                            InputLabelProps={{
                                                            shrink: true,
                                                            }}
                                                            error={key.charAt(0)=== "/"}
                                                            helperText={key.charAt(0)=== "/" ? 'Forward slash (/) Not allowed at beginning' : ' '}
                                                            
                                                        />
                                                        <TextField
                                                            label="AWS Secret"
                                                            fullWidth
                                                            value={aws_secret || "SECRET_NOT_SET"}
                                                            select
                                                            name={"aws_secret"}
                                                            onChange={this.handleChange}
                                                            disabled={secretLoader !== "loaded" || !secretLoader}
                                                            margin="normal"
                                                            InputLabelProps={{
                                                                shrink: true,
                                                            }}
                                                        >
                                                            <MenuItem value={"SECRET_NOT_SET"}>Select a secret</MenuItem>
                                                            {secretsList.map(secrets => (
                                                                <MenuItem key={secrets.name} value={secrets.name}>
                                                                    {secrets.name}
                                                                </MenuItem>
                                                            ))}
                                                        </TextField>
                                                        {project_id &&
                                                            <span
                                                                className="text-link"
                                                                onClick={()=>MicroModal.show("secret-modal")}
                                                            >
                                                                Add Secrets
                                                            </span>}
                                                    </div>
                                                }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        }
                        {project_id && importFileType && validatedImport &&
                        <div className="m-details-card mt p-0">
                            <div className="container-fluid">
                                <div className="row">
                                    <div className="col-sm-12">
                                        <div className="m-details-body">
                                            {importFileType !== "MLFLOW" ?
                                            <div>
                                                <TextField
                                                    id={'model_type'}
                                                    label={"Model Type"}
                                                    name={'model_type'}
                                                    select
                                                    value={model_type || "MODEL_TYPE_NOT_SET"}
                                                    onChange={this.handleChange}
                                                    onBlur={this.handleBlur}
                                                    error={!!error.model_type}
                                                    helperText={error.model_type && "This field is required!"}
                                                    margin="normal"
                                                    disabled={importFileType === "EXPERIMENT"}
                                                    fullWidth
                                                    InputLabelProps={{
                                                        shrink: true,
                                                    }}
                                                >
                                                    <MenuItem value={"MODEL_TYPE_NOT_SET"}>Select Model Type</MenuItem>
                                                    {
                                                        modelTypes.map((e, i) => {
                                                            return (
                                                                <MenuItem key={"modelTypes" + i} value={e}>{e}</MenuItem>
                                                                )
                                                            })
                                                    }
                                                </TextField>
                                                <TextField
                                                    id={'ml_library'}
                                                    label={"ML Library"}
                                                    name={'ml_library'}
                                                    select
                                                    value={ml_library || "ML_LIBRARY_NOT_SET"}
                                                    onChange={this.handleChange}
                                                    margin="normal"
                                                    disabled={importFileType === "EXPERIMENT"}
                                                    fullWidth
                                                    InputLabelProps={{
                                                        shrink: true,
                                                    }}
                                                >
                                                    <MenuItem value={"ML_LIBRARY_NOT_SET"}>Select ML Library</MenuItem>
                                                    {
                                                        mlLibrariesArr.map((e, i) => {
                                                            return (
                                                                <MenuItem value={e} key={"mllibrary" + i}>{l[e]}</MenuItem>
                                                            )
                                                        })
                                                    }
                                                </TextField>
                                                <TextField
                                                    id={'algorithm'}
                                                    label={"Algorithm"}
                                                    name={'algorithm'}
                                                    value={algorithm}
                                                    onChange={this.handleChange}
                                                    margin="normal"
                                                    disabled={importFileType === "EXPERIMENT"}
                                                    fullWidth
                                                    InputLabelProps={{
                                                        shrink: true,
                                                    }}
                                                />
                                                <TextField
                                                    id={'dependencies'}
                                                    label={"Dependencies"}
                                                    name={'dependencies'}
                                                    multiline
                                                    rows={3}
                                                    placeholder={"Dependencies seperated by new lines"}
                                                    value={dependencies ? dependencies.split(",").join("\n") : ""}
                                                    onChange={this.handleChange}
                                                    margin="normal"
                                                    fullWidth
                                                    InputLabelProps={{
                                                        shrink: true,
                                                    }}
                                                />
                                                <TextField
                                                id={'runtime'}
                                                label={"Runtime"}
                                                name={'runtime'}
                                                select
                                                value={runtime || "RUNTIME_NOT_SET"}
                                                onChange={this.handleChange}
                                                onBlur={this.handleBlur}
                                                margin="normal"
                                                error={!!error.runtime}
                                                helperText={error.runtime && "This field is required!"}
                                                fullWidth
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                            >
                                                <MenuItem value={"RUNTIME_NOT_SET"}>Select Runtime</MenuItem>
                                                {
                                                    runtimeArr.map((e, i) => {
                                                        return (
                                                            <MenuItem value={e} key={"runtime" + i}>{l[e]}</MenuItem>
                                                        )
                                                    })
                                                }
                                            </TextField>
                                            </div>:
                                            <div>
                                                {run_list && run_list.length && !mlflowrunLoader ? 

                                                <div className="scrollable-table mlflow-run">
                                                    <div className="scrollable-table-header text-gray">
                                                        <div>
                                                            <span>Name</span>
                                                            <span>Created On</span>
                                                            <span>Status</span>
                                                        </div>
                                                    </div>
                                                    <div className="scrollable-table-body">
                                                    {run_list.length ? run_list.map((e, i) => {
                                                        return (
                                                            <div
                                                                key={"ml_run"+i} 
                                                                className={`clickable selectable-list ${run_id === e.info.run_id && "selected-item"}`}
                                                                onClick={
                                                                    ()=>{
                                                                        this.handleChange({
                                                                            target: {
                                                                                name: "run_id",
                                                                                value: e.info.run_id
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                            >
                                                                <span>
                                                                    {e.info.name}
                                                                </span>
                                                                <span>
                                                                    {moment(Number(e.info.start_time)).format("LLLL")}
                                                                </span>
                                                                <span>
                                                                    <label
                                                                        className={`model-item-status status-${this.getColor(e.info.status)}`}
                                                                    >
                                                                        {e.info.status}
                                                                    </label>
                                                                </span>
                                                            </div>
                                                        )
                                                    }) :
                                                        ""
                                                    }
                                                    </div>
                                                </div>

                                                : !mlflowrunLoader ? 
                                                <div className="pd-lg text-center text-gray">
                                                    No ML Flow Runs found!
                                                </div> : <CardLoader />}
                                            </div> 
                                            }
                                            <div className="mt-lg mb">
                                                <button
                                                    className="btn btn-primary mr"
                                                    type="button"
                                                    onClick={this.handleSubmit}
                                                >
                                                    Submit {loader || uploading ? <CircularProgress size={14} color={"#fff"} /> : ""}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        }                        
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token,
        experimentList: state.experiments.experiments,
        mlFlowList: state.experiments.mlFlowList,
        expLoader: state.experiments.loader,
        mlFlowLoader: state.experiments.mlLoader,
        projects: state.projects.projects,
        secretsList: state.jobStore.secretsList,
        secretLoader: state.jobStore.status,
        loader: state.models.loader,
        selectedProject: state.projects.globalProject
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            submitModelCatalog,
            getExperimentList,
            getProjectsList,
            fetchSecrets,
            fetchMlFlowRunList,
            getMlFlowList,
            alertMsg,
            globalProjectSelection
        },
        dispatch
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(ImportModel);
