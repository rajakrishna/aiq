import React from "react";

import ContentLoader from "react-content-loader";

const CardLoader = () =>
  [0, 1, 2, 3, 4].map((key, value) => {
    return (
      <ContentLoader
        key={key}
        height={80}
        width={1300}
        speed={2}
        count={4}
        primaryColor="#f3f3f3"
        secondaryColor="#ecebeb"
      >
        <rect x="12" y="16" rx="4" ry="4" width="117" height="6.4" />
        <rect x="16" y="35" rx="3" ry="3" width="85" height="6.4" />
        <circle cx="51" cy="22.27" r="1" />
        <circle cx="194" cy="117.27" r="1" />
        <rect
          x="204.48"
          y="15.79"
          rx="0"
          ry="0"
          width="129.05"
          height="27.59"
        />
        <rect x="323" y="52.27" rx="0" ry="0" width="0" height="0" />
        <rect
          x="592.48"
          y="14.79"
          rx="0"
          ry="0"
          width="129.05"
          height="27.59"
        />
        <rect
          x="828.48"
          y="13.79"
          rx="0"
          ry="0"
          width="129.05"
          height="27.59"
        />
        <rect
          x="1046.46"
          y="11.79"
          rx="0"
          ry="0"
          width="129.05"
          height="27.59"
        />
      </ContentLoader>
    );
  });

export default CardLoader;
