import React, { Component } from "react";
import {FormGroup, FormControlLabel, Checkbox, Grid, Chip, TextField} from '@material-ui/core';
import { capitalizeVars } from "../../utils/helper";

export default class EditColumns extends Component {

    state = {
        allList: [
            'type',
            'ml_algorithm',
            'created_date',
            'feature_significance',
            'training_duration',
            'testing_duration'
        ],
        defaultList: [
            'type',
            'ml_algorithm',
            'created_date',
            'feature_significance',
            'training_duration',
            'testing_duration'
        ],
        active: this.props.listing,
        selectList: [],
        activeList: [],
        search: null
    }

    componentDidMount() {
        this.prepareList(this.props.modelsList);
    }

    prepareList = (modelsList) => {
        if(!modelsList) return;
        const active = this.state.active ? [...this.state.active] : [];
        const allList = [...this.state.allList];
        let hyperKeys = [];
        let perfKeys = [];
        for(let i in modelsList) {
            const spec = modelsList[i];
            if(spec.hyperparameters) {
                const hKeys = Object.keys(spec.hyperparameters).map( e => {
                    return "hyper."+e
                })
                hyperKeys.push(hKeys);
                hyperKeys = hyperKeys.flat();
            }
            if(spec.performance_metrics && spec.performance_metrics.length) {
                const pKeys = spec.performance_metrics[0].metrics.map( e => {
                    return "performance."+e.name
                })
                perfKeys.push(pKeys);
                perfKeys = perfKeys.flat();
            }
        }
        allList.push([...new Set(hyperKeys)]);
        allList.push([...new Set(perfKeys)]);
        const listing = allList.flat();
        const selectList = listing.filter((e)=>!active.includes(e));
        const activeList = listing.filter((e)=>active.includes(e));
        this.setState({
            allList: listing,
            selectList,
            activeList
        })
    }

    handleChange = (e) => {
        const name = e;
        const active = [...this.state.active];
        const {allList} = this.state;
        const i = active.indexOf(name);
        i >=0 ? active.splice(i,1) : active.push(name);
        active.sort();
        const selectList = allList.filter((elem)=>!active.includes(elem));
        const activeList = allList.filter((elem)=>active.includes(elem));
        this.setState({
            active,
            selectList,
            activeList
        })
    }

    handleReset = (e) => {
        const {allList, active} = this.state;
        const selectList = allList.filter((e)=>!active.includes(e));
        const activeList = allList.filter((e)=>active.includes(e));
        this.setState({
            active: [...this.state.defaultList],
            selectList: [...allList],
            activeList: [...this.state.defaultList]
        },()=>{this.props.changeListing(this.state.active)})
    }

    render() {
        const {allList, active, search, selectList, activeList} = this.state;
        return(
            <div className="modal micromodal-slide" id="modal-edit-columns" aria-hidden="true">
                <div className="modal__overlay" tabIndex="-1" data-micromodal-close>
                <div
                    className="modal__container p-3"
                    role="dialog"
                    aria-modal="true"
                    aria-labelledby="modal-edit-columns-title"
                    style={{marginTop: '9rem'}}
                >
                    <header className="modal__header mt-2">
                    <h2 className="modal__title pl-2" id="modal-edit-columns-title">
                        Edit View
                    </h2>
                    <button
                        className="modal__close"
                        aria-label="Close modal"
                        data-micromodal-close
                    />
                    </header>
                    <main className="modal__content mt-2" id="modal-edit-columns-content">
                        
                        <Grid container spacing={8}>
                            <Grid item xs={6}>
                            <div className="h-500 of-auto pd" style={{width: 300, height: 450}}>
                                <h4>Available Fields</h4>
                                <TextField
                                    id={"search"}
                                    label={"Search"}
                                    name={search}
                                    type={"text"}
                                    placeholder={"Enter keywords to search"}
                                    value={search || ""}
                                    onChange={(e)=>{
                                        this.setState({
                                            search: e.target.value
                                        })
                                    }}
                                    margin="normal"
                                    fullWidth
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                                {
                                selectList.map((e,i) => {
                                    if(!e || active.includes(e)) return;
                                    if(search && !e.includes(search)) return;
                                    const hyperFlag = (!selectList[i-1] || (selectList[i-1] && !selectList[i-1].includes("hyper"))) && e.includes("hyper");
                                    const perfFlag = (!selectList[i-1] || (selectList[i-1] && !selectList[i-1].includes("performance"))) && e.includes("performance");
                                    return(
                                        <div key={"selectedList"+i}>
                                            {hyperFlag ? 
                                                <div className="text-gray mt">
                                                    Parameters:
                                                </div>
                                            : ""}
                                            {perfFlag ? 
                                                <div className="text-gray mt">
                                                    Metrics:
                                                </div>
                                            : ""}
                                            <div className={e.includes("hyper") || e.includes("performance") ? "mt-sm mb-sm field-selection" : "field-selection"}>
                                                <div
                                                    className="pd-lr-sm relative"
                                                    onClick={()=>this.handleChange(e)}
                                                >
                                                    <span className="inline-block" style={{width: "calc(100% - 20px)"}}>
                                                        {e.replace(/hyper\./gi,"Parameter.").replace(/performance\./gi,"Metrics.")}
                                                    </span>
                                                    <span className="display-none action-icon">
                                                        <i className="fa fa-plus" />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        )
                                    })
                                }
                            </div>
                            </Grid>
                            <Grid item xs={6}>
                                <div className="pd h-500 of-auto" style={{width: 300, height: 450}}>
                                    <h4>Active Fields</h4>
                                    {
                                        activeList.map((e,i)=>{
                                            if(!e || !active.includes(e)) return;
                                            const hyperFlag = activeList[i-1] && !activeList[i-1].includes("hyper") && e.includes("hyper");
                                            const perfFlag = activeList[i-1] && !activeList[i-1].includes("performance") && e.includes("performance");
                                            return(
                                                <div key={"activeList"+i}>
                                                    {hyperFlag ? 
                                                        <div className="text-gray mt">
                                                            Parameters:
                                                        </div>
                                                    : ""}
                                                    {perfFlag ? 
                                                        <div className="text-gray mt">
                                                            Metrics:
                                                        </div>
                                                    : ""}
                                                    <div className={e.includes("hyper") || e.includes("performance") ? "mt-sm mb-sm field-selection" : "field-selection"}>
                                                        <div
                                                            className="pd-lr-sm relative"
                                                            onClick={()=>this.handleChange(e)}
                                                        >
                                                            <span className="inline-block" style={{width: "calc(100% - 20px)"}}>
                                                                {e.replace(/hyper\./gi,"Parameter.").replace(/performance\./gi,"Metrics.")}
                                                            </span>
                                                            <span className="display-none action-icon">
                                                                <i className="fa fa-times" />
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                        })
                                    }
                                </div>
                            </Grid>
                        </Grid>
                    </main>
                    <footer className="modal__footer filter-form-actions">
                    <button
                        className="btn btn-primary btn-rounded"
                        data-micromodal-close
                        onClick={()=>{this.props.changeListing(this.state.active)}}
                    >
                        APPLY
                    </button>
                    <button
                        className="btn btn-default btn-rounded"
                        onClick={this.handleReset}
                        data-micromodal-close
                        aria-label="Close this dialog window"
                    >
                        RESET
                    </button>
                    </footer>
                </div>
                </div>
            </div>
        );
    }
}
