import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getDeployments } from "../../ducks/deployments";
import { Link } from "react-router-dom";
import moment from "moment";
import { getDeployementType } from "../../utils/helper";

class RegModelDepList extends Component {
    _isMounted = false;

    componentDidMount() {
        this._isMounted = true;
        this.props.getDeployments();
    }

    getColor(type) {
        const status = {
            'Creating': "info",
            'Failed': "failed",
            'Available': "success"
        }
        const color = status[type] ? status[type] : 'primary';
        return color;
    }

    componentWillUnmount() {
        this._isMounted = false
    }

    render() {
        const {
            deployments,
            mlDeployments,
        } = this.props;
        return (
            <div>
                <table className="table data-table no-border">
                    <thead>
                        <tr>
                            <td>Name</td>
                            <td className="text-center">Status</td>
                            <td>Version</td>
                            <td>Created Date</td>
                            <td>Type</td>
                            <td>Replicas</td>
                            <td>Available Replicas</td>
                            <td>Age</td>
                        </tr>
                    </thead>
                    <tbody>
                        {deployments && deployments.map((deployment, index) => {
                            if(!mlDeployments.includes(deployment.metadata.name)) {
                                return;
                            }
                            return (
                                <tr key={"deployment" + index}>
                                    <td>
                                        <Link to={`/deployments/${deployment.metadata.name}`}>
                                            {deployment.metadata.name}
                                        </Link>
                                    </td>
                                    <td className="text-center">
                                        {deployment.status &&
                                            <label
                                                className={`model-item-status status-${this.getColor(deployment.status.state)}`}
                                            >
                                                {deployment.status.state}
                                            </label>
                                        }
                                    </td>
                                    <td>
                                        {deployment.spec && deployment.spec.annotations && deployment.spec.annotations.deployment_version}
                                    </td>
                                    <td>
                                        {deployment.metadata && moment(deployment.metadata.creationTimestamp).format("LL")}
                                    </td>
                                    <td>
                                        {getDeployementType(deployment)}
                                    </td>
                                    <td>
                                        {deployment.spec && deployment.spec.predictors && deployment.spec.predictors[0].replicas}
                                    </td>
                                    <td>
                                        {deployment.status && deployment.status.predictorStatus && deployment.status.predictorStatus[0].replicasAvailable}
                                    </td>
                                    <td>
                                        {deployment.metadata && moment(deployment.metadata.creationTimestamp).toNow(true)}
                                    </td>
                                </tr>
                            );
                        })
                        }
                    </tbody>
                </table>
            </div>
        );
    }
}
const mapStateToProps = state => {
    return {
        token: state.auth.token,
        deployments: state.deployments.deployments,
        loader: state.deployments.loader
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            getDeployments
        },
        dispatch
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(RegModelDepList);
