import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Library from "../shared/Library";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { getAllModelHealth } from "../../api";
import ModelHealthAreaChart from "../chart/ModelHealthAreaChart";
import { getAllModelHealthDetails } from "../../ducks/modelDetails";
import User from "../shared/User";
import moment from "moment";
import {maxBy,slice} from 'lodash';
import ReactTooltip from "react-tooltip";

class ModelListItem extends Component {
  state = {
    selectedModelId: null,
    modelHealthDetails: [],
    modelHealthDayChartData: [],
    modelHealthWeekChartData: [],
    modelHealthMonthChartData: [],
  };

  componentDidMount() {
    this.props.getAllModelHealthDetails({
      modelIdEquals: this.props.selectedModelId || this.props.modelsList[0].id,
      timestampGreaterOrEqualThan: "now-1d",
      interval: "hour",
      sort: "timestamp,desc"
    });
    this.setState({ selectedModel: this.props.selectedModel });
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.selectedModelId !== prevState.selectedModelId) {
      this.props.getAllModelHealthDetails({
        modelIdEquals:
          this.state.selectedModelId || this.props.modelsList[0].id,
        timestampGreaterOrEqualThan: "now-1d",
        interval: "hour",
        sort: "timestamp,desc"
      });
      this.setState({ selectedModel: this.props.selectedModel });
      this.modelHealthData("now-1d",'hour')
      this.modelHealthData("now-7d",'day')
      this.modelHealthData("now-30d",'day')
    }
  }

  graphClasses ={
    train_test_ratio:"progress-bar progress-bar-blue",
    recall:"progress-bar progress-bar-green",
    precision:"progress-bar progress-bar-yellow",
    f_score:"progress-bar progress-bar-sky-blue",
    accuracy:"progress-bar progress-bar-rose",
  };

  getGraphClass = (name) => {
    return this.graphClasses[name] ? this.graphClasses[name] : "progress-bar progress-bar-blue"
  }

  getInsightsToolTipTable= (insights,tableHeading) => {
    return(
      <table className="table table-striped">
        <thead>
          <tr><td>{tableHeading}</td></tr>
        </thead>
        <tbody>
        <tbody>
          {insights &&
            insights.map((item, itemIndex) => {
              return (
                <tr key={itemIndex}>
                  {item &&
                    item.reason}
                </tr>
              );
            })}
        </tbody>
        </tbody>
      </table>
    )
  }

  recentInsight = (insights) => {
    if (insights && insights.length > 0){
      var insight = maxBy(insights,"timestamp")
      insights = (insights.length > 5) ? slice(insights,0,5) : insights
      return(
          <div
            className="prediction-more-info-link"
            data-tip="light"
          >
            <p data-tip='' data-for={insight.id}>
            {insight.reason}<i className="fa fa-info-circle" />
            </p>
            <ReactTooltip place="bottom" type="light" id={insight.id} >
              {this.getInsightsToolTipTable(insights,"Insights")}
            </ReactTooltip>
          </div>
      )
    }else{
      return (<div>NA</div>);
    }
  }

  getPerformanceMatric = (data)=>{
    if (data == null)
      return
    return(
      <React.Fragment>
        {data.map(i=>{
          var outputClass = i.output_class
          var metrics = i.metrics
          return(
            <div key={outputClass}
                  className="performance-matrix-content"
                >
                <span className="p-m-item-value">Class: {outputClass}</span>
                <div className="row">
                {
                  metrics.map((metric,index) => {
                    return(
                        <div className="col-md-3" key={outputClass+""+index}>
                          <div className="p-m-item">
                            <label>{metric.name}</label>
                            <span className="p-m-item-value">
                              {metric.value}
                            </span>
                            <div className="progress">
                              <div
                                className={this.getGraphClass(metric.name)}
                                style={{
                                  width: `${metric.value * 100}%`
                                }}
                              />
                            </div>
                          </div>
                        </div>
                    )
                  })
                }
              </div>
            </div>
            )
          })
        }
      </React.Fragment>
    )
  }

  modelHealthData = (duration, interval) => {
    getAllModelHealth(this.props.token, {
      modelIdEquals: this.state.selectedModelId,
      timestampGreaterOrEqualThan: duration,
      interval: interval,
      sort: "timestamp,desc"
    }).then(response => {
      if (response.status) {
        switch(duration){
          case "now-1d":
            this.setState({ modelHealthDayChartData: response.data });
          break;
          case "now-7d":
            this.setState({ modelHealthWeekChartData: response.data });
          break;
          case "now-30d":
            this.setState({ modelHealthMonthChartData: response.data });
          break;
          default:
            console.error("Invalid duration : ",duration);
          break;
        }
      }
    });
  };

  healthScoreColor = score =>{
    if (score <= 50) {
      return "#dc3545";
    } else if (score > 50 && score < 75) {
      return "#ffc107";
    } else {
      return "#28a745";
    }
  }
  addScoreTextClass = score => {
    if (score < 50) {
      return "text-danger";
    } else if (score > 50 && score < 75) {
      return "text-warning";
    } else {
      return "text-success";
    }
  };

  addScoreTextClassDrift = score => {
    if (score < 50) {
      return "text-success";
    } else if (score > 50 && score < 75) {
      return "text-warning";
    } else {
      return "text-danger";
    }
  };

  render() {
    const { modelsList } = this.props;
    const { selectedModelId } = this.state;
    return (
      <div className="models-list">
        {modelsList.map((model, index) => {
          return (
            <div
              className={
                selectedModelId === model.id ? "model-details-card" : ""
              }
              key={index}
              onClick={() => this.setState({ selectedModelId: model.id })}>
              <div className="model-list-item">
                <div className="model-item-left">
                  <div className="row">
                    <div className="model-item-title col-md-5 pl-0">
                      <h6>
                        <span className="text-link" onClick={()=>{this.props.history.push(`/monitoring/${model.id}`,{prevUrl:this.props.history.location.pathname})}}>
                          {model.name}
                        </span>
                      </h6>
                      <small>{model.project_name}</small>
                    </div>
                    <div className="col-md-3">
                      {model.failing === false && (
                        <label className="model-item-status status-success">
                          SUCCESSFUL
                        </label>
                      )}
                      {model.failing === true && (
                        <label className="model-item-status status-failed">
                          FAILED
                        </label>
                      )}
                    </div>
                    <div className="col-md-3">
                        <span>
                          version {model.version}
                        </span>
                    </div>
                    <div className="col-md-1">
                      <ul className="stack-list">
                        <Library library={model.ml_library} />
                      </ul>
                    </div>
                  </div>
                </div>
                <div className="model-item-right">
                  {this.props.scores === false && (
                    <div className="model-score-row">
                      {model.version} <span> {' '} </span>
                      {model.ml_algorithm} <span> {' '} </span>
                      {model.type} <span> {' '} </span>
                      <ul className="stack-list">
                        <Library library={model.ml_library} />
                      </ul>
                      <div className="project-members-info">
                        <ul className="project-members-list">
                          <User user={model.owner} />
                        </ul>
                      </div>
                    </div>
                  )}
                  {this.props.scores === true && (
                    <div className="model-score-row">
                      <div className="score-card">
                        <span className="score-card-label">HEALTH (SCORE)</span>
                        <span
                          className={
                            "score-card-value " +
                            this.addScoreTextClass(model.health_score)
                          }
                        >
                          {Math.round(model.health_score)}
                        </span>
                      </div>
                      <div className="score-card">
                        <span className="score-card-label">DATA DRIFT</span>
                        <span className={`score-card-value ${this.addScoreTextClassDrift(model.data_drift)}`}>
                          {model.data_drift}
                        </span>
                      </div>
                      <div className="score-card">
                        <span className="score-card-label">
                          USAGE (PREDICTIONS)
                        </span>
                        <span className="score-card-value">
                          {model.predictions_count}
                        </span>
                      </div>
                    </div>
                  )}
                </div>
              </div>
              <div
                className={
                  selectedModelId === model.id
                    ? "model-details-card-body"
                    : "model-details-card-body  display-none"
                }
              >
                <div className="row pd-tb">
                  <div className="col-md-6">
                    <div>
                      <div className="performance-matrix-content pl-4">
                        <h6>Performance Metrics</h6>
                      </div>
                      <div className="notification-wraper pd-tb">
                      {this.getPerformanceMatric(model.performance_metrics)}
                      </div>
                    </div>
                  </div>
                  {this.props.scores === true && (
                    <div className="col-md-6">
                      <Tabs >
                        <TabList className="inline-tabs tabs-gray">
                          <Tab onClick={() => this.modelHealthData("now-1d",'hour')}>
                            LAST 1 DAY
                          </Tab>
                          <Tab onClick={() => this.modelHealthData("now-7d",'day')}>
                            LAST 7 DAYS
                          </Tab>
                          <Tab onClick={() => this.modelHealthData("now-30d",'day')}>
                            LAST MONTH
                          </Tab>
                        </TabList>
                        <TabPanel>
                          <ModelHealthAreaChart
                            modelHealthDetails={this.state.modelHealthDayChartData}
                            chartColor={this.healthScoreColor(model.health_score)}
                            format="LT"
                            dayOne={true}
                          />
                        </TabPanel>
                        <TabPanel>
                          <ModelHealthAreaChart
                            modelHealthDetails={this.state.modelHealthWeekChartData}
                            chartColor={this.healthScoreColor(model.health_score)}
                            format="ll"
                          />
                        </TabPanel>
                        <TabPanel>
                          <ModelHealthAreaChart
                            modelHealthDetails={this.state.modelHealthMonthChartData}
                            chartColor={this.healthScoreColor(model.health_score)}
                            format="ll"
                          />
                        </TabPanel>
                      </Tabs>
                      {/* <div className="performance-graph">
                      <ul className="inline-tabs tabs-gray">
                        <li className="react-tabs__tab--selected">LAST 1 DAY</li>
                        <li>LAST 7 DAYS</li>
                        <li>LAST MONTH</li>
                      </ul>
                      <img src={ModelGraph} alt="model graph" />
                    </div> */}
                    </div>
                  )}
                </div>
              </div>
            </div>
          );
        })}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token,
    modelHealthDetails: state.modelDetails.modelHealthDetails
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getAllModelHealthDetails
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModelListItem);
