import React, { Component } from "react";
// import InputRange from "react-input-range";
import "react-input-range/lib/css/index.css";
import { getAllProjects } from "../../api";
import { connect } from "react-redux";
import StringFilterField from '../common/stringFilterField'
import NumericFilterField from '../common/numericFilterField'
import Select from 'react-select';
import 'react-select/dist/react-select.css';
class FilterModal extends Component {
  constructor(props) {
    super(props);
    this._isMounted = false;
    this.state = { 
      filters: props.filters,
      filterType: props.filterType,
      projectNameList: [],
      libraries:[
        { 
          label: "SKLEARN",
          value: "SKLEARN"
        },
        { 
          label: "TENSORFLOW",
          value: "TENSORFLOW"
        },
        { 
          label: "KERAS",
          value: "KERAS"
        },
        { 
          label: "PYTORCH",
          value: "PYTORCH"
        },
        { 
          label: "H2O",
          value: "H2O"
        },
        { 
          label: "SPARKML",
          value: "SPARKML"
        },
        { 
          label: "SAGEMAKER",
          value: "SAGEMAKER"
        },
        { 
          label: "TPOT",
          value: "TPOT"
        },
        { 
          label: "AUTO_SKLEARN",
          value: "AUTO_SKLEARN"
        },
        { 
          label: "H2O_AUTOML",
          value: "H2O_AUTOML"
        },
      ],
    };
  }
  UNSAFE_componentWillReceiveProps({filters}) {
    this.setState({ filters });
  }

  componentDidMount() {
    this._isMounted = true;
    getAllProjects(this.props.token, { sort: "createdDate,desc" })
      .then(response => {
        let data = response.data;
        if (response.status) {
          let projectNameList = [];
          data.map(project => {
            return projectNameList.push({
              label: project.name,
              value: project.name
            });
          });
          if(this._isMounted)
            this.setState({ projectNameList });
        }
      })
      .catch(error => console.log(error));
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  handleStringInputChange = (obj)=> {
    let filters = { ...this.state.filters };
    switch(obj.name){
      case 'ml_algorithm':
        filters['ml_algorithm'] = obj
        break;
      case 'type':
        filters['type'] = obj
        break;
      default:
        return
    }
    this.setState({filters: filters})
  }
  handleNumericInputChange = (obj) => {
    let filters = { ...this.state.filters };
    switch(obj.name){
      case 'health_score':
        filters['health_score'] = obj
        break;
      case 'data_drift':
        filters['data_drift'] = obj
        break;
      default:
        return
    }

    this.setState({filters: filters})
  }
  handleSelectChange = e => {
    let filters = { ...this.state.filters };
    filters['mlLibraryContains'] = e;
    this.setState({filters: filters })
  }
  handleProjectChange = e => {
    let filters = { ...this.state.filters };
    filters['projectNameContains'] = e;
    this.setState({filters: filters })
  } 
  handleInputChange = e => {
    let filters = { ...this.state.filters };
    filters[e.target.name] = e.target.value;
    this.setState({ filters });
  };
  handleApplyFilters = () => {
    this.props.applyFilters(this.state.filters);
  };
  handleResetFilters = () => {
    this.setState({ filters: {} });
    this.props.applyFilters({});
  };

  render() {
    const { projectNameList,filterType } = this.state;
    let {
      projectNameContains,
      mlLibraryContains,
      ml_algorithm,
      type,
      failing,
      data_drift,
      health_score,
    } = this.state.filters;
    return (
      <div className="modal micromodal-slide" id="modal-1" aria-hidden="true">
        <div className="modal__overlay" tabIndex="-1" data-micromodal-close>
          <div
            className="modal__container"
            role="dialog"
            aria-modal="true"
            aria-labelledby="modal-1-title"
          >
            <header className="modal__header">
              <h2 className="modal__title" id="modal-1-title">
                Filter Models
              </h2>
              <button
                className="modal__close"
                aria-label="Close modal"
                data-micromodal-close
              />
            </header>
            <main className="modal__content" id="modal-1-content">
              <div className="modal-filter-form">
              {/* {!this.props.project &&
                <div className="form-group row">  
                  <label htmlFor="" className="col-md-3">Projects</label>
                  <Select
                    closeOnSelect={true}
                    className= "select-field col-md-8"
                    onChange={this.handleProjectChange}
                    options={projectNameList}
                    placeholder="Select Projects"
                    removeSelected={true}
                    simpleValue
                    value={projectNameContains}
                  />
                </div>
                } */}
                {(filterType === "models") && 
                  <React.Fragment>
                    <div className="form-group row">
                      <label htmlFor="" className="col-md-3">Failing</label>
                      <div className="col-md-4">
                        <select
                          name="failing"
                          value={failing || ""}
                          id="projectdrop"
                          className="form-control select-field"
                          onChange={this.handleInputChange}
                        >
                          <option value=''>Please select</option>
                          <option value={true}> Yes </option>
                          <option value={false}> No </option>
                        </select>
                      </div>
                    </div>
                    <NumericFilterField
                    name="health_score"
                    value1={health_score && health_score.value1 }
                    value2={health_score && health_score.value2 }
                    filterKey={health_score && health_score.key }
                    OnValueChange={(obj) => this.handleNumericInputChange(obj)}
                    />
                    <NumericFilterField
                    name="data_drift"
                    value1={data_drift && data_drift.value1 }
                    value2={data_drift && data_drift.value2 }
                    filterKey={data_drift && data_drift.key }
                    OnValueChange={(obj) => this.handleNumericInputChange(obj)}
                    />
                  </React.Fragment>
                }

                <div className="form-group row">
                  <label htmlFor="" className="col-md-3">Library</label>
                  
                    <Select
                      closeOnSelect={true}
                      className="select-field col-md-8"
                      multi
                      onChange={this.handleSelectChange}
                      options={this.state.libraries}
                      placeholder="Select Library"
                      removeSelected={true}
                      simpleValue
                      value={mlLibraryContains}
                    />

                </div>
                <StringFilterField
                  name="ml_algorithm"
                  filterValue={ml_algorithm && ml_algorithm.value}
                  filterKey={ml_algorithm && ml_algorithm.key}
                  OnValueChange={(obj) => this.handleStringInputChange(obj)}
                />
                <StringFilterField
                  name="type"
                  filterValue={type && type.value}
                  filterKey={type && type.key }
                  OnValueChange={(obj) => this.handleStringInputChange(obj)}
                />
              </div>
            </main>
            <footer className="modal__footer filter-form-actions">
              <button
                className="btn btn-primary btn-rounded"
                data-micromodal-close
                onClick={this.handleApplyFilters}
              >
                APPLY FILTERS
              </button>
              <button
                className="btn btn-default btn-rounded"
                data-micromodal-close
                aria-label="Close this dialog window"
                onClick={this.handleResetFilters}
              >
                RESET FILTERS
              </button>
            </footer>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token
  };
};

export default connect(mapStateToProps)(FilterModal);
