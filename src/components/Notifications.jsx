import React, { Component } from "react";
import { Link } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import Pagination from "./common/pagination";
import LeftArrow from "../images/left-chevron.png";
import { markAsRead, notificationsList, notificationCounts } from "../ducks/notifications";
import notification_icon from "../images/notification-icon.svg";
import moment from "moment";
import { Loader1 } from './home/modelOverviewLoader';
import { Switch, FormControlLabel } from '@material-ui/core';
import _, { startCase } from "lodash";
import Mixpanel from "mixpanel-browser";

class Notifications extends Component {

    state = {
        currentPageSize: 1,
        currentPage: 0,
        notifications: [],
        readNotify: false
    }

    componentDidMount() {
        Mixpanel.track("Notifications Page");
        this.props.notificationsList({pageNumber: 0});
        this.props.notificationCounts();
    }

    componentWillUnmount() {
        this.props.notificationsList({pageNumber: 0});
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if(nextProps.page !== prevState.currentPage || (nextProps.page === 0 && prevState.currentPage === 0)) {
            return ({
                currentPage: nextProps.page
            })
        }
        return null
    }
    
    handlePageChange = pageAction => {
        let pageNumber = this.state.currentPage;
        const readNotify = this.state.readNotify;
        pageAction === "nextPage" ? (pageNumber += 1) : (pageNumber -= 1);
        const params = { pageNumber: pageNumber };
        if(readNotify) {
            params.read = false;
        }
        this.props.notificationsList(params);
    }

    handleChanges = (e) => {
        if(this.props.loader) return;
        const target = e.target;
        const name = target.name;
        const value = target.name === "readNotify" ? target.checked : target.value;
        this.setState({
            [name]: value
        }, ()=>{
            this.props.notificationsList(this.getParams());
            this.props.notificationCounts({
                read: this.state.readNotify ? false : null,
            });
        });
    }

    getParams = () => {
        const { filters, timestamp, readNotify, currentPage } = this.state;
        return ({
            createdDateGreaterOrEqualThan: timestamp,
            read: readNotify ? false : null,
            pageNumber: currentPage
        })
    }

    componentDidUpdate() {
        const raw_ids = this.props.notifications.map(e => {
            if(!e.read)
                return e.id
        })
        const ids = raw_ids.filter(e => e);
        if(ids && ids.length) {
            this.props.markAsRead({ids: ids});
            this.props.notificationCounts({
                read: this.state.readNotify ? false : null,
            });
        }
    }

    getLink = (id, type) => {
        switch(type) {
            case "MLWorkflow":
                return("/workflows/"+id);
            case "MLWorkflowRun":
                return("/run-history/"+id);
            case "RegisteredModelVersion":
                return("/registered-model-versions/"+id);
            case "Project":
                return("/projects/"+id);
            case "Insight":
            default:
                return("/monitoring/"+id);
        }
    }
    
    render() {
        const { notifications_count, loader, notifications } = this.props;
        const { currentPage, readNotify, appliedFilters, timestamp } = this.state;
        return (
            <div className="main-container">
                <div className="main-body">
                    <div className="content-wraper-sm">
                        <div className="m-details-card">
                            <div className="m-details-card-head pl-4 mr-5">
                                {/* <div
                                    className="back-action clickable"
                                    onClick={() => { this.state.prevUrl ? this.props.history.push(this.state.prevUrl) : this.props.history.goBack() }}
                                >
                                    <img src={LeftArrow} alt="left arrow" />
                                </div> */}
                                <div className="m-title-info pd-sm">
                                    <h4>Notifications</h4>
                                </div>
                                <div className="m-tech-stack">
                                    <div style={{ width: 226 }}>
                                        {notifications_count > 20 && (
                                            <Pagination
                                                itemsCount={notifications_count}
                                                currentPage={currentPage}
                                                currentPageSize={notifications.length}
                                                pageSize={20}
                                                onPageChange={this.handlePageChange}
                                            />
                                        )}
                                    </div>
                                </div>
                            </div>
                            <div className="model-filter-header mlr-lg mr-5">
                                <div className="filter-btn ml">
                                <button
                                    className="btn btn-primary btn-sm btn-rounded ml"
                                    data-micromodal-trigger="modal-1"
                                >
                                    Add Filter +
                                </button>
                                </div>
                                <div className="applied-filters">
                                <ul className="filter-items-list">
                                    {!_.isEmpty(appliedFilters) &&
                                    Object.keys(appliedFilters).map(key => (
                                        <li key={key} className="filter-tag-item">
                                            {startCase(key)}-{this.getFilterTab(appliedFilters[key])}
                                        <i className="fa fa-times" style={{marginTop: 3}} onClick={() => this.handleRemoveFilter(key)}>
                                        </i>
                                        </li>
                                    ))}
                                </ul>
                                </div>
                                <div className="filter-right-actions">
                                    <div className="row align-items-center justify-content-between m-0">
                                        <div className="col p-0">
                                            <div className="float-right">
                                            <select
                                                style={{width: 140}}
                                                className="form-control select-field"
                                                name="timestamp"
                                                className="form-control select-field"
                                                onChange={this.handleChanges}
                                                defaultValue={timestamp}>
                                                <option value="">Select Timestamp</option>
                                                <option value="now-1d">Last 1 Day</option>
                                                <option value="now-7d">Last 7 Days</option>
                                                <option value="now-30d">Last Month</option>
                                            </select>
                                            </div>
                                        </div>
                                        <div className="col p-0">
                                            <FormControlLabel
                                                labelPlacement="start"
                                                control={
                                                    <Switch
                                                        checked={readNotify}
                                                        onChange={this.handleChanges}
                                                        name="readNotify"
                                                        value="readNotify"
                                                        color="primary"
                                                    />
                                                }
                                                label="Show unread"
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="pd-tb">
                                {loader ? <Loader1 /> :
                                <ul className="notification-list">
                                    {notifications && notifications.length ? notifications.map((notification, key)=>{
                                        return(
                                            <li className="notification-item" key={key}>
                                                <div className="mlr-lg pd-lr-lg">
                                                    <span className="notification-icon">
                                                    <img
                                                        src={notification_icon}
                                                        data-tip=""
                                                        data-for={`mark-as-read-${key}`}
                                                        alt="notification icon"
                                                    />
                                                    </span>
                                                    <span className="notification-msg">
                                                    <Link
                                                        to={this.getLink(notification.entity_id, notification.reference_entity)}
                                                    >
                                                        <h6 style={notification.read ? {fontWeight: "normal"} : {}}>{notification.message}</h6>
                                                    </Link>
                                                    <time>
                                                        {moment(notification.created_date).format(
                                                            "LLL"
                                                        )}
                                                    </time>
                                                    </span>
                                                </div>
                                            </li>
                                        )
                                    }) :
                                        <div className="pd-tb text-gray content-center">
                                            No notifications!
                                        </div>
                                    }
                                </ul>}
                            </div>
                            <div className="m-details-card-head pl-4 mr-5">
                                <div className="m-tech-stack">
                                    <div style={{ width: 226 }}>
                                        {notifications_count > 20 && (
                                            <Pagination
                                                itemsCount={notifications_count}
                                                currentPage={currentPage}
                                                currentPageSize={notifications.length}
                                                pageSize={20}
                                                onPageChange={this.handlePageChange}
                                            />
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        notifications: state.notification.notifications,
        notifications_count: state.notification.notifications_count,
        loader: state.notification.loader,
        page: state.notification.page,
        user: state.users.user,
        token: state.auth.token
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            notificationsList,
            notificationCounts,
            markAsRead
        },
        dispatch
    );
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Notifications);
