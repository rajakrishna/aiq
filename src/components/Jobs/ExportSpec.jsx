import React, { Component } from "react";
import { connect } from "react-redux";
import { Button, Menu, MenuItem, Fade } from '@material-ui/core';
import YAML from 'yamljs';
import { CopyPre } from "../introduction/CopyPre";

class ExportSpec extends Component {

    state = {
        showCode: 1,
    }

    componentDidMount() {
        this.handleExportRaw();
    }

    handleExportRaw = (type) => {
        const spec = {...this.props.jobStore}
        delete spec.viewable;
        delete spec.render_job;
        delete spec.running;
        this.delKeys(spec);
        if(type === "JSON") {
            const jsonString = JSON.stringify(spec, null, 4);
            this.setState({
                showCode: 2,
                code: jsonString,
            })
        } else {
            const yamlString = YAML.stringify(spec,100);
            this.setState({
                showCode: 1,
                code: yamlString,
            })
        }
    }

    isEmpty(obj) {
        for(var key in obj) 
            return false;

        return true
    }
      
    delKeys(app){
        for(var key in app){
            if(app[key] !== null && typeof(app[key]) === 'object' && app[key] !== ""){
                this.delKeys(app[key])
        
                if(this.isEmpty(app[key])) {
                delete app[key]
                }
            } 
            if(app[key] === null || !app[key]) {
                delete app[key]
            }
        }
    }

    render() {
        const { code, showCode } = this.state;
        return(
            <div>
                <ul className="inline-tabs tabs-blue p-0 mr-0 inline-block">
                    <li className={`react-tabs__tab${showCode === 1 && "--selected"}`} onClick={()=>{this.handleExportRaw("YAML")}}>YAML</li>
                    <li className={`react-tabs__tab${showCode === 2 && "--selected"}`} onClick={()=>{this.handleExportRaw("JSON")}}>JSON</li>
                </ul>
                {showCode &&
                <div className="mt-lg">
                    <CopyPre code={code}>
                        <pre style={{maxHeight: 300}}>{code}</pre>
                    </CopyPre>
                </div>}
            </div>            
        )
    }
}

const mapStateToProps = state => {
    return {
        jobStore: state.jobStore.jobStore,
        token: state.auth.token
    };
};

export default connect(mapStateToProps) (ExportSpec);