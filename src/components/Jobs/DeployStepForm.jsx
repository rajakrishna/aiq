import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { validate } from "./form_elements/ValidateInput";
import { changeJobStore, errorStore, setStepType } from "../../ducks/jobStore";

import StepForm from "./StepForm";

const mapSpecToState = (data) => {
    switch(data.sub_type){
        case "DEPLOY_SIMPLE":
        default:
            return ({
                name: data.name || null,
                type: "DEPLOY" || null,
                sub_type: data.sub_type || null,
                replicas: data.replicas || null,
                source: data.source || {},
                docker: data.docker || {},
                data_source: data.data_source || [],
                outputs: data.outputs || [],
                deploy_condition: data.deploy_condition || {},
                env: data.env || [],
                env_source: data.env_source || [],
                resources: data.resources || {},       
            })
        case "DEPLOY_ARTIFACT":
            return ({
                name: data.name || null,
                type: "DEPLOY" || null,
                sub_type: data.sub_type || null,
                replicas: data.replicas || null,
                source: data.source || {},
                docker: data.docker || {},
                deploy_condition: data.deploy_condition || {},
                env: data.env || [],
                env_source: data.env_source || [],
                resources: data.resources || {},       
            })
        case "DEPLOY_TF_SERVING": 
            return ({
                name: data.name || null,
                type: "DEPLOY" || null,
                sub_type: data.sub_type || null,
                replicas: data.replicas || null,
                source: data.source || {},
                docker: data.docker || {},
                data_source: data.data_source || [],
                outputs: data.outputs || [],
                deploy_condition: data.deploy_condition || {},
                env: data.env || [],
                env_source: data.env_source || [],
                resources: data.resources || {},       
            })
        case "DEPLOY_AB_TEST": 
            return ({
                name: data.name || null,
                type: "DEPLOY" || null,
                sub_type: data.sub_type || null,
                replicas: data.replicas || null,
                source_a: data.source_a || {},
                source_b: data.source_b || {},
                docker_a: data.docker_a || {},
                docker_b: data.docker_b || {},
                data_source: data.data_source || [],
                outputs: data.outputs || [],
                deploy_condition: data.deploy_condition || {},
                env_a: data.env_a || [],
                env_b: data.env_b || [],
                env_source_a: data.env_source_a || [],
                env_source_b: data.env_source_b || [],
                resources: data.resources || {},       
            })
        case "DEPLOY_TRANSFORMER": 
            return ({
                name: data.name || null,
                type: "DEPLOY" || null,
                sub_type: data.sub_type || null,
                replicas: data.replicas || null,
                model_source: data.model_source || {},
                transformer_source: data.transformer_source || {},
                model_docker: data.model_docker || {},
                transformer_docker: data.transformer_docker || {},
                data_source: data.data_source || [],
                outputs: data.outputs || [],
                deploy_condition: data.deploy_condition || {},
                model_env: data.model_env || [],
                transformer_env: data.transformer_env || [],
                model_env_source: data.model_env_source || [],
                transformer_env_source: data.transformer_env_source || [],
                resources: data.resources || {},       
            })
        case "DEPLOY_GRAPH": 
            return ({
                name: data.name || null,
                type: "DEPLOY" || null,
                sub_type: data.sub_type || null,
                replicas: data.replicas || null,
                yaml: data.yaml || "",
                source: data.source || {},
                docker: data.docker || {},
                data_source: data.data_source || [],
                outputs: data.outputs || [],
                deploy_condition: data.deploy_condition || {},
                env: data.env || [],
                env_source: data.env_source || [],
                resources: data.resources || {}
            })
    }
}

class DeployStepForm extends Component {
    deployTypes = [ 'DEPLOY_SIMPLE',
                    'DEPLOY_ARTIFACT',
                    'DEPLOY_TF_SERVING',
                    'DEPLOY_AB_TEST',
                    'DEPLOY_TRANSFORMER',
                    'DEPLOY_GRAPH' ];

    state = {
        spec: mapSpecToState({...this.props.spec, sub_type: this.props.stepTypes["deploy"] || this.props.spec.sub_type}),
        sub_type: this.props.stepTypes["deploy"] || this.props.spec.sub_type,
        error: this.props.jobError && this.props.jobError.spec && {...this.props.jobError.spec.deploy}
    }

    static getDerivedStateFromProps(nextProps) {
        return ({
            error: nextProps.jobError && nextProps.jobError.spec && {...nextProps.jobError.spec.deploy}
        })
    }

    saveData = (data = {}) => {
        const spec = {...data};
        // if(validate(spec, "deploy", this.props.jobStore, this.props.errorStore)) {
            this.mapStateToSpec(spec,()=>{
                this.setState({
                    spec
                });
            });
        // }
    }

    changeType = (sub_type) => {
        let stepTypes = this.props.stepTypes;
        stepTypes["deploy"] = sub_type;
        this.props.setStepType(stepTypes);
        this.setState({
            sub_type,
            spec: mapSpecToState({...this.state.spec, sub_type: sub_type}),
        })
    }

    mapStateToSpec = (spec = this.state.spec, callback) => {
        if(spec.sub_type==="DEPLOY_SIMPLE") {
            this.deploy = {
                "deploy_condition": spec.deploy_condition,
                "docker": spec.docker,
                "env": spec.env,
                "env_source": spec.env_source,
                "name": spec.name,
                "outputs": spec.outputs,
                "data_source": spec.data_source,
                "replicas": spec.replicas,
                "resources": spec.resources,
                "source": spec.source,
                "sub_type": spec.sub_type,
                "type": "DEPLOY"
            }
        } else if(spec.sub_type==="DEPLOY_ARTIFACT") {
            this.deploy = {
                "deploy_condition": spec.deploy_condition,
                "docker": spec.docker,
                "env": spec.env,
                "env_source": spec.env_source,
                "name": spec.name,
                "replicas": spec.replicas,
                "resources": spec.resources,
                "source": spec.source,
                "sub_type": spec.sub_type,
                "type": "DEPLOY"
            }
        } else if(spec.sub_type==="DEPLOY_TF_SERVING") {
            this.deploy = {
                "deploy_condition": spec.deploy_condition,
                "docker": spec.docker,
                "env": spec.env,
                "env_source": spec.env_source,
                "name": spec.name,
                "outputs": spec.outputs,
                "data_source": spec.data_source,
                "replicas": spec.replicas,
                "resources": spec.resources,
                "source": spec.source,
                "sub_type": spec.sub_type,
                "type": "DEPLOY"
            }
        } else if(spec.sub_type==="DEPLOY_AB_TEST") {
            this.deploy = {
                "deploy_condition": spec.deploy_condition,
                "docker_a": spec.docker_a,
                "docker_b": spec.docker_b,
                "env_a": spec.env_a,
                "env_b": spec.env_b,
                "env_source_a": spec.env_source_a,
                "env_source_b": spec.env_source_b,
                "name": spec.name,
                "outputs": spec.outputs,
                "data_source": spec.data_source,
                "replicas": spec.replicas,
                "resources": spec.resources,
                "source_a": spec.source_a,
                "source_b": spec.source_b,
                "sub_type": spec.sub_type,
                "type": "DEPLOY"
            }
        } else if(spec.sub_type==="DEPLOY_TRANSFORMER") {
            this.deploy = {
                "deploy_condition": spec.deploy_condition,
                "transformer_docker": spec.transformer_docker,
                "model_docker": spec.model_docker,
                "model_env": spec.model_env,
                "transformer_env": spec.transformer_env,
                "transformer_env_source": spec.transformer_env_source,
                "model_env_source": spec.model_env_source,
                "name": spec.name,
                "outputs": spec.outputs,
                "data_source": spec.data_source,
                "replicas": spec.replicas,
                "resources": spec.resources,
                "model_source": spec.model_source,
                "transformer_source": spec.transformer_source,
                "sub_type": spec.sub_type,
                "type": "DEPLOY"
            }
        }else if(spec.sub_type==="DEPLOY_GRAPH") {
            this.deploy = {
                "deploy_condition": spec.deploy_condition,
                "docker": spec.docker,
                "env": spec.env,
                "env_source": spec.env_source,
                "name": spec.name,
                "outputs": spec.outputs,
                "data_source": spec.data_source,
                "replicas": spec.replicas,
                "resources": spec.resources,
                "source": spec.source,
                "sub_type": spec.sub_type,
                "type": "DEPLOY",
                "yaml": spec.yaml
                } 
        } else {
            this.deploy = {
                "deploy_condition": spec.deploy_condition,
                "docker": spec.docker,
                "env": spec.env,
                "env_source": spec.env_source,
                "name": spec.name,
                "outputs": spec.outputs,
                "data_source": spec.data_source,
                "replicas": spec.replicas,
                "resources": spec.resources,
                "source": spec.source,
                "sub_type": spec.sub_type,
                "type": "DEPLOY"
            }
        }
        let jobStore = {...this.props.jobStore}
        jobStore.spec.deploy = this.deploy;
        this.props.changeJobStore (
            jobStore
        )
        setTimeout(()=>{callback()},100);
    }

    render() {
        const {spec, sub_type, error} = this.state;
        return(
            <div>
                {(sub_type === "DEPLOY_SIMPLE" || sub_type === "DEPLOY_ARTIFACT") &&
                <StepForm 
                    changeType={this.changeType}
                    errorStore={error}
                    key={"deploy-form"} 
                    jobTypes={this.deployTypes} 
                    spec={spec} 
                    saveData={this.saveData} 
                />}
                {sub_type === "DEPLOY_TF_SERVING" &&
                <StepForm 
                    changeType={this.changeType}
                    errorStore={error}
                    key={"deploy-form"} 
                    jobTypes={this.deployTypes} 
                    spec={spec} 
                    saveData={this.saveData} 
                />}
                {sub_type === "DEPLOY_AB_TEST" &&
                <StepForm 
                    changeType={this.changeType}
                    errorStore={error}
                    key={"deploy-form"} 
                    jobTypes={this.deployTypes} 
                    spec={spec} 
                    saveData={this.saveData} 
                />}
                {sub_type === "DEPLOY_TRANSFORMER" &&
                <StepForm 
                    changeType={this.changeType}
                    errorStore={error}
                    key={"deploy-form"} 
                    jobTypes={this.deployTypes} 
                    spec={spec} 
                    saveData={this.saveData} 
                />}
                {sub_type === "DEPLOY_GRAPH" &&
                <StepForm 
                    changeType={this.changeType}
                    errorStore={error}
                    key={"deploy-form"} 
                    jobTypes={this.deployTypes} 
                    spec={spec} 
                    saveData={this.saveData} 
                />}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        jobStore: state.jobStore.jobStore,
        stepTypes: state.jobStore.stepTypes,    
        jobError: state.jobStore.jobError
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            changeJobStore,
            errorStore,
            setStepType
        },
        dispatch
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(DeployStepForm);