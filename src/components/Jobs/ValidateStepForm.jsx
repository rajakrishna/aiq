import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { changeJobStore, setStepType } from "../../ducks/jobStore";

import StepForm from "./StepForm";

const mapSpecToState = (data) => {
    return ({
        name: data.name || null,
        type: "TRAIN",
        sub_type: data.sub_type || null,
        source: data.source || {},
        data_source: data.data_source || [],
        outputs: data.outputs || [],
        env: data.env || [],
        env_source: data.env_source || [],
        resources: data.resources || {},       
    })
}

class ValidateStepForm extends Component {
    validateTypes = [
        "SIMPLE",
    ]
    state = {
        sub_type: this.props.stepTypes["validate"] || (this.props.spec && this.props.spec.sub_type),
        spec: mapSpecToState({...this.props.spec, sub_type: this.props.stepTypes["validate"] || (this.props.spec && this.props.spec.sub_type)})
    }

    // componentDidMount() {
    //     // this.saveData(mapSpecToState(this.props.spec));
    //     console.log(this.props.spec);
    // }

    saveData = (data = {}) => {
        const spec = {...data};
        this.mapStateToSpec(spec,()=>{
        this.setState({
            spec
        })});
    }

    changeType = (sub_type) => {
        let stepTypes = this.props.stepTypes;
        stepTypes["validate"] = sub_type;
        this.props.setStepType(stepTypes);
        this.setState({
            sub_type,
            spec: mapSpecToState({...this.state.spec, sub_type: sub_type}),
        })
    }

    mapStateToSpec = (spec = this.state.spec, callback) => {
        this.validate = {
            // "deploy_condition": spec.deploy_condition,
            // "docker": spec.docker,
            "env": spec.env,
            "env_source": spec.env_source,
            "name": spec.name,
            "outputs": spec.outputs,
            "data_source": spec.data_source,
            // "replicas": spec.replicas,
            "resources": spec.resources,
            "source": spec.source,
            "sub_type": spec.sub_type,
            "type": "TRAIN"
        }

        let jobStore = {...this.props.jobStore}
        jobStore.spec.validate = this.validate;
        this.props.changeJobStore (
            jobStore
        )
        setTimeout(()=>{callback()},100);
    }

    render() {
        const {spec} = this.state;
        return(
            <div>
                <StepForm changeType={this.changeType} key={"deploy-form"} spec={spec} saveData={this.saveData} jobTypes={this.validateTypes}/>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        jobStore: state.jobStore.jobStore,
        stepTypes: state.jobStore.stepTypes
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            changeJobStore,
            setStepType
        },
        dispatch
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(ValidateStepForm);