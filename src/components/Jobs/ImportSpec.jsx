import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Button, Dialog, DialogActions, DialogTitle, DialogContent, Chip, TextField } from '@material-ui/core';
import YAML from 'yamljs';
import { changeJobStore } from "../../ducks/jobStore";
import Dropzone from 'react-dropzone';

class ImportSpec extends Component {

    state = {
        showInput: false,
        showUpload: true,
        filename: null,
        file: null,
        importText: ""
    }

    handleImportFile = () => {
        const { file, filename, showInput, importText } = this.state;
        if(!showInput) {
            const reader = new FileReader();
            reader.onload = (event) => {
                let spec = event.target.result;
                if((/\.json/gi).test(filename)) {
                    spec = JSON.parse(spec);
                } else {
                    spec = YAML.parse(spec);
                }
                this.props.changeJobStore(spec);
                this.props.closeImport();
                setTimeout(()=>{
                    this.props.renderEverything();
                },100)
            };
            reader.readAsText(file);
        } else {
            this.props.changeJobStore(YAML.parse(importText));
            this.props.closeImport();
            setTimeout(()=>{
                this.props.renderEverything();
            },100)
        }
    }

    render() {
        const { showUpload, showInput, filename, importText } = this.state;
        const { importDialog } = this.props;
        return(
            <Dialog
                open={importDialog}
                fullWidth={true}
                maxWidth={'sm'}
            >
                <DialogTitle id="run-dialog">
                    Import As
                </DialogTitle>
                <DialogContent>
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-sm-12 mt-sm">
                                <div>
                                    <ul className="inline-tabs tabs-blue p-0 mr-0 inline-block">
                                        <li className={`react-tabs__tab${showUpload && "--selected"}`} onClick={()=>{this.setState({showUpload: true, showInput: false})}}>File</li>
                                        <li className={`react-tabs__tab${showInput && "--selected"}`} onClick={()=>{this.setState({showUpload: false, showInput: true})}}>Raw</li>
                                    </ul>
                                    {showUpload && 
                                        <div className="mt">
                                            {!filename ?
                                            <Dropzone onDrop={acceptedFiles => {
                                                if (acceptedFiles && (/\.json|\.yml/gi).test(acceptedFiles[0].name)) {
                                                    this.setState({
                                                        filename: acceptedFiles[0].name,
                                                        file: acceptedFiles[0]
                                                    })
                                                }
                                            }}>
                                                {({ getRootProps, getInputProps }) => (
                                                    <div {...getRootProps()} className="content-center upload-space">
                                                        <input {...getInputProps()} />
                                                        <p>
                                                            <i
                                                                className="fa fa-file-upload"
                                                                style={{ fontSize: 60 }}
                                                            />
                                                        </p>
                                                        <p>Drag 'n' drop file here, or click to select file</p>
                                                    </div>
                                                )}
                                            </Dropzone> :
                                            <div style={{height: 263}}>
                                                <Chip
                                                    label={filename}
                                                    onDelete={() => {
                                                        this.setState({
                                                            filename: ""
                                                        })
                                                    }}
                                                />
                                            </div>
                                            }
                                        </div>
                                    }

                                    {showInput && 
                                        <div className="mt">
                                            <TextField
                                                id={"importText"}
                                                label={"Input Raw Text"}
                                                name={"importText"}
                                                value={importText}
                                                multiline={true}
                                                rows={11}
                                                rowsMax={11}
                                                onChange={(e)=>{
                                                    this.setState({
                                                        importText: e.target.value
                                                    })
                                                }}
                                                margin="normal"
                                                fullWidth
                                                InputLabelProps={{
                                                    shrink: true,
                                                }}
                                            />
                                        </div>
                                    }
                                </div> 
                            </div>
                        </div>
                    </div>
                </DialogContent>
                <DialogActions>
                    <button
                        className="btn btn-danger btn-sm"
                        onClick={() => {
                            this.props.closeImport()
                        }} color="primary"
                    >
                        Close
                    </button>
                    <button
                        type="button"
                        className="btn btn-primary btn-sm"
                        onClick={this.handleImportFile}
                    >
                        Import
                    </button>
                </DialogActions>
            </Dialog>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            changeJobStore
        },
        dispatch
    );
};

const mapStateToProps = state => {
    return {
        jobStore: state.jobStore.jobStore,
        token: state.auth.token
    };
};

export default connect(mapStateToProps, mapDispatchToProps) (ImportSpec);