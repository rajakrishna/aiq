import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { changeJobStore, setStepType } from "../../ducks/jobStore";

import StepForm from "./StepForm";

const mapSpecToState = (data) => {
    return ({
        name: data.name || null,
        type: "DEPLOY",
        sub_type: data.sub_type,
        source: data.source || {},
        data_source: data.data_source || [],
        outputs: data.outputs || [],
        env: data.env || [],
        env_source: data.env_source || [],
        resources: data.resources || {},       
    })
}

class PostDeployStepForm extends Component {
    postDeployTypes = [
        "SIMPLE",
        "SPARK",
        // "DASK",
        // "PANDAS",
    ]
    state = {
        sub_type: this.props.stepTypes["post_deploy_steps"][this.props.index] || this.props.spec[this.props.index].sub_type,
        spec: mapSpecToState({...this.props.spec[this.props.index], sub_type: this.props.stepTypes["post_deploy_steps"][this.props.index] || this.props.spec[this.props.index].sub_type}),
        error: this.props.jobError && this.props.jobError.spec && {...this.props.jobError.spec.post_deploy_steps[this.props.index]}
    }

    static getDerivedStateFromProps(nextProps) {
        return ({
            error: nextProps.jobError && nextProps.jobError.spec && nextProps.jobError.spec.post_deploy_steps && {...nextProps.jobError.spec.post_deploy_steps[nextProps.index]}
        })
    }


    // componentDidMount() {
    //     // this.saveData(mapSpecToState(this.props.spec));
    //     console.log(this.props.spec);
    // }

    saveData = (data = {}) => {
        const spec = {...data};
        this.mapStateToSpec(spec,()=>{
        this.setState({
            spec
        })});
    }

    changeType = (sub_type) => {
        let stepTypes = this.props.stepTypes;
        stepTypes["post_deploy_steps"][this.props.index] = sub_type;
        this.props.setStepType(stepTypes);
        this.setState({
            sub_type,
            spec: mapSpecToState({...this.state.spec, sub_type: sub_type}),
        })
    }

    mapStateToSpec = (spec = this.state.spec, callback) => {
        this.post_deploy = {
            "deploy_condition": spec.deploy_condition,
            // "docker": spec.docker,
            "env": spec.env,
            "env_source": spec.env_source,
            "name": spec.name,
            "outputs": spec.outputs,
            "data_source": spec.data_source,
            "replicas": spec.replicas,
            "resources": spec.resources,
            "source": spec.source,
            "sub_type": spec.sub_type,
            "type": "DEPLOY"
        }

        let jobStore = {...this.props.jobStore}
        if(!jobStore.spec.post_deploy_steps) {
            jobStore.spec.post_deploy_steps = []
        }
        jobStore.spec.post_deploy_steps[this.props.index] = this.post_deploy;
        this.props.changeJobStore (
            jobStore
        )
        setTimeout(()=>{callback()},100);
    }

    render() {
        const {spec, error} = this.state;
        return(
            <div>
                <StepForm formType={"POST"} errorStore={error} changeType={this.changeType} key={"train-form"} spec={spec} saveData={this.saveData} jobTypes={this.postDeployTypes} />
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        jobStore: state.jobStore.jobStore,
        stepTypes: state.jobStore.stepTypes
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            changeJobStore,
            setStepType
        },
        dispatch
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(PostDeployStepForm);