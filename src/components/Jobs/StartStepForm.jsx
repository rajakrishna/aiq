import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { changeJobStore, errorStore } from "../../ducks/jobStore";
import { validate } from "./form_elements/ValidateInput";

import StepForm from "./StepForm";
import { callbackify } from "util";

const mapSpecToState = (data) => {
    return ({
        "id": data.id || null,
        "name": data.name,
        "model_name": data.spec.model_name,
        "project_id": data.spec.project_id,
        "project_name": data.spec.project_name,
        "git_secrets": data.spec.git_secrets,
        "image_pull_secrets": data.spec.image_pull_secrets,
        "docker_push_secrets": data.spec.docker_push_secrets, 
        "global_env_source_variables": data.spec.global_env_source_variables,
        "global_env_variables": data.spec.global_env_variables,
        "notifications": data.spec.notifications,
        "run": false,
        "schedule": data.schedule,
        "spec": {
            "deploy": data.spec.deploy,
            "post_deploy_steps": data.spec.post_deploy_steps,
            "post_test_steps": data.spec.post_test_steps,
            "post_train_steps": data.spec.post_train_steps,
            "post_validate_steps": data.spec.post_validate_steps,
            "pre_deploy_steps": data.spec.pre_deploy_steps,
            "pre_test_steps": data.spec.pre_test_steps,
            "pre_train_steps": data.spec.pre_train_steps,
            "pre_validate_steps": data.spec.pre_validate_steps,
            "test": data.spec.test,
            "train": data.spec.train,
            "validate": data.spec.validate
            }
    })
}
const mapSpecToResetState = (data) => {
    return ({
        "id": data.id || null,
        "name": "",
        "model_name": "",
        "project_id": data.spec.project_id,
        "project_name": data.spec.project_name,
        "git_secrets": [],
        "image_pull_secrets": [],
        "docker_push_secrets": [], 
        "global_env_source_variables": [],
        "global_env_variables": [],
        "notifications": {},
        "run": false,
        "schedule": {},
        "spec": {
            "deploy": {},
            "post_deploy_steps": [],
            "post_test_steps": [],
            "post_train_steps": [],
            "post_validate_steps": [],
            "pre_deploy_steps": [],
            "pre_test_steps": [],
            "pre_train_steps": [],
            "pre_validate_steps": [],
            "test": {},
            "train": {},
            "validate": {}
            }
    })
}
class StartStepForm extends Component {

    // constructor(props) {
    //     super(props);
    //     console.log(props.jobStore);
    // }

    state = {
        spec: mapSpecToState({ ...this.props.jobStore }),
        error: {...this.props.jobError, ...this.props.jobError.spec}
    }

    static getDerivedStateFromProps(nextProps) {
        return ({
            error: {...nextProps.jobError, ...nextProps.jobError.spec}
        })
    }

    // componentDidMount() {
    //     // this.saveData(mapSpecToState(this.props.spec));
    //     console.log(this.props.spec);
    // }

    saveData = (data = {}) => {
        const spec = this.modSpec({ ...data });
        const stateSpec = mapSpecToState({...spec});
        this.mapStateToSpec(spec, ()=> {
            this.setState({
                spec: stateSpec
            })
        });
        // }
    }
    resetData = (data = {}) => {
        console.log("reset data",data);
        const spec = this.modSpec({ ...data });
        console.log("reset sepc",spec)
        const stateSpec = mapSpecToResetState({...spec});
        this.mapStateToSpec(spec, ()=> {
            this.setState({
                spec: stateSpec
            })
        });
        // }
    }
    getProjectName = (id) => {
        let name = "";
        this.props.projects.map((e) => {
            if(e.id === id) {
                name = e.name;
            }
        });
        return name;
    }

    modSpec = (data) => {
        return({
            "id": this.state.spec.id || null,
            "name": data.name,
            "run": false,
            "schedule": data.schedule,
            "spec": {
                "project_id": !(/NOT_SET/g).test(data.project_id) && data.project_id,
                "project_name": this.getProjectName(data.project_id),
                "docker_push_secrets": data.docker_push_secrets,
                "git_secrets": data.git_secrets,
                "global_env_source_variables": data.global_env_source_variables,
                "global_env_variables": data.global_env_variables,
                "image_pull_secrets": data.image_pull_secrets,
                "model_name": data.model_name,
                "notifications": data.notifications,
                "deploy": data.spec.deploy,
                "post_deploy_steps": data.spec.post_deploy_steps,
                "post_test_steps": data.spec.post_test_steps,
                "post_train_steps": data.spec.post_train_steps,
                "post_validate_steps": data.spec.post_validate_steps,
                "pre_deploy_steps": data.spec.pre_deploy_steps,
                "pre_test_steps": data.spec.pre_test_steps,
                "pre_train_steps": data.spec.pre_train_steps,
                "pre_validate_steps": data.spec.pre_validate_steps,
                "test": data.spec.test,
                "train": data.spec.train,
                "validate": data.spec.validate              
            }
        })
    }

    mapStateToSpec = (spec, callback) => {
        this.props.changeJobStore(
            {...spec}
        )
        setTimeout(()=>{callback()},100)
    }

    render() {
        const { spec, error } = this.state;
        return (
            <div>
                <StepForm errorStore={error} key={"start-form"} spec={spec} saveData={this.saveData} resetData={this.resetData}/>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        jobStore: state.jobStore.jobStore,
        jobError: state.jobStore.jobError,
        projects: state.projects.projects
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            changeJobStore,
            errorStore
        },
        dispatch
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(StartStepForm);