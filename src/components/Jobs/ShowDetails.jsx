import React, { Component } from "react";

export default class ShowDetails extends Component {
    render() {
        const { name } = this.props;
        return(
            <div className="text-left pd">
                <table className="details-table">
                    <tbody>
                        <tr>
                            <td className="pd-lr-sm">{name}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        )
    }

}