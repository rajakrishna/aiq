export const jobProto = {
    "name": "",
    "number_of_runs": 0,
    "run": false,
    "schedule": {},
    "spec": {
        "deploy": {},
        "docker_push_secrets": [],
        "git_secrets": [],
        "global_env_source_variables": [],
        "global_env_variables": [],
        "image_pull_secrets": [],
        "model_name": "",
        "notifications": {
            "email": {
            "email_addresses": ""
            },
            "slack": {
            "webhook_url": ""
            },
            "type": ""
        },
        "post_deploy_steps": [{}],
        "post_test_steps": [{}],
        "post_train_steps": [{}],
        "post_validate_steps": [{}],
        "pre_deploy_steps": [{}],
        "pre_test_steps": [{}],
        "pre_train_steps": [{}],
        "pre_validate_steps": [{}],
        "project_id": "",
        "project_name": "",
        "test": {},
        "train": {},
        "validate": {}
      
    }
  }