import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { changeJobStore } from "../../ducks/jobStore";

import StepForm from "./StepForm";

const mapSpecToState = (data) => {
    return ({
        name: data.name || null,
        type: data.type || null,
        replicas: data.replicas || null,

        //src
        // srcType: data.source && data.source.type,
        // command: data.source && data.source.command || "",
        // image: data.source && data.source.image || "",
        // model_artifact_path: data.source && data.source.model_artifact_path || "",
        // runtime: data.source && data.source.runtime || "",
        source: data.source || {},

        //Git
        // gitDirectory: data.source && data.source.git && data.source.git.directory,
        // gitRepository: data.source && data.source.git && data.source.git.repository,
        // gitRevision: data.source && data.source.git && data.source.git.revision,
        // gitSecret: data.source && data.source.git && data.source.git.secret,
        // //Exp
        // experimentId: data.source && data.source.experiment && data.source.experiment.id,
        // experimentRequirements: data.source && data.source.experiment && data.source.experiment.requirements


        // dockerName: (data.docker && data.docker.name) || "",
        // dockerRepository: (data.docker && data.docker.repository) || "",
        // dockerTag: (data.docker && data.docker.tag) || "",

        docker: data.docker || {},


        // compare_with_previous: (data.deploy_condition && data.deploy_condition.compare_with_previous) || false,
        // deploy_metrics: (data.deploy_condition && data.deploy_condition.deploy_metrics) || [],
        deploy_condition: data.deploy_condition || {},

        env: data.env || [],

        env_source: data.env_source || [],
        
        output: data.output || {},

        // cpu: (data.resources.cpu && data.cpu) || 0,
        // gpu: (data.resources.gpu && data.gpu) || 0,
        // memory: (data.resources.memory && data.memory) || "",
        // ps: (data.resources.ps && data.ps) || 0,
        // tf_version: (data.resources.tf_version && data.tf_version) || "",
        // tpu: (data.resources.tpu && data.tpu) || 0,
        // workers: (data.resources.workers && data.workers) || 0,

        //Resources
        resources: data.resources || {},       
    })
}

class TestStepForm extends Component {
    state = {
        spec: mapSpecToState({...this.props.spec})
    }

    // componentDidMount() {
    //     // this.saveData(mapSpecToState(this.props.spec));
    //     console.log(this.props.spec);
    // }

    saveData = (data = {}) => {
        const spec = {...data};
        this.mapStateToSpec(spec,()=>{
        this.setState({
            spec
        })});
    }

    mapStateToSpec = (spec = this.state.spec, callback) => {
        this.test = {
            "deploy_condition": spec.deploy_condition,
            "docker": spec.docker,
            "env": spec.env,
            "env_source": spec.env_source,
            "name": spec.name,
            "output": spec.output,
            "replicas": spec.replicas,
            "resources": spec.resources,
            "source": spec.source,
            "type": spec.type
        }

        let jobStore = {...this.props.jobStore}
        jobStore.spec.test = this.test;
        this.props.changeJobStore (
            jobStore
        )
        setTimeout(()=>{callback()},100);
    }

    render() {
        const {spec} = this.state;
        return(
            <div>
                <StepForm key={"deploy-form"} spec={spec} saveData={this.saveData} />
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        jobStore: state.jobStore.jobStore
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            changeJobStore
        },
        dispatch
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(TestStepForm);