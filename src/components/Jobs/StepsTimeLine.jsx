import React, { Component } from "react";
import { capitalizeVars, 
        sortByArray, 
        totalArrayLength, 
        reorderElems,
        findStatus,
        getStatusColorIcon } from "../../utils/helper";
import { confirm } from "../Confirm";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { setStep, toggleSteps, changeJobStore, setStepType } from "../../ducks/jobStore";
import JobForm from "./JobForm";
import ShowDetails from "./ShowDetails";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { Tooltip } from '@material-ui/core';

class StepsTimeLine extends Component {

    arrLength = 0;
    state = {
        selectedSteps: [],
        activeStep: "",
        arrows: [],
        index: "",
        currentIndex: 0,
    }

    static getDerivedStateFromProps(nextProps) {
        if(nextProps.selectedSteps && nextProps.steps) {
            return ({
                selectedSteps: sortByArray([...nextProps.selectedSteps], [...nextProps.steps])
            })
        }
        return null
    }

    componentDidUpdate = (nextProps, prevState) => {
        this.renderArrows();
    }

    componentDidMount() {
        this.setActiveStep({}, !this.props.jobStore.running ? "ML_Workflow" : this.getFirstStep());
        this.setCurrentIndex({
            currentIndex: this.props.jobStore.running ? 3 : 0
        })
    }

    sleep = (ms) => {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    renderArrows = async() => {
        if(this.arrLength === totalArrayLength(this.state.selectedSteps)) {
            return
        }
        this.arrLength = totalArrayLength([...this.state.selectedSteps]);
        
        await this.sleep(100);
        
        const {selectedSteps} = this.state,
        arrows = selectedSteps.map((e,i)=>{
        
            if(!selectedSteps[i+1]) return;

            const div1 = document.querySelector("#"+selectedSteps[i]),
            div2 = document.querySelector("#"+selectedSteps[i+1]);

            if(!(div1 && div2)) {
                return "";
            }
            return (
                // <line className="line-svg" x1={div1.offsetParent.offsetLeft + (div1.offsetWidth/2)} y1={div1.offsetTop + (div1.offsetHeight/2) - 12} x2={div2.offsetParent.offsetLeft + (div2.offsetWidth/2)} y2={div2.offsetTop + (div2.offsetHeight/2)-12} />
                <line key={selectedSteps[i]+selectedSteps[i+1]} className="line-svg" x1={div1.offsetParent.offsetLeft + (div1.offsetWidth/2)} y1={40} x2={div2.offsetParent.offsetLeft + (div2.offsetWidth/2)} y2={40} />
            )
        })

        const div1 = document.querySelector("#ML_Workflow"),
        div2 = document.querySelector("#"+selectedSteps[0]);
        if(div1) {
            if(!div2) {
                this.setState({arrows: []});
                return;
            }
            arrows.push(
                <line key={"ML_Workflow"+selectedSteps[0]} className="line-svg" x1={div1.offsetParent.offsetLeft + (div1.offsetWidth/2)} y1={40} x2={div2.offsetParent.offsetLeft + (div2.offsetWidth/2)} y2={40} />
            );
        }   

        this.setState({arrows});
    }

    getFirstStep = () => {
        const step = this.state.selectedSteps[0];
        if(step) {
            if(typeof step === 'object')
                return step[0];
            return step;
        }
        return ""
    }

    setActiveStep = (event, step=null, index="") => {
        index = !index && step ? step.replace(/[a-zA-Z_]/g,"") : index;
        const target = event.target;
        const jobStore = this.props.jobStore;
        if(target) {
            if(target.getAttribute("aria-label") === "action-btn") return;
        }
        const { activeStep } = this.state;
        if(activeStep === step || !step) {
            const firstStep = this.getFirstStep();
            index = !jobStore.running ? index : firstStep.replace(/[a-zA-Z_]/g,"");
            this.setState({
                activeStep: !jobStore.running ? "ML_Workflow" : firstStep,
                index
            })
        } else {
            this.setState({
                activeStep: step,
                index
            })
        }
    }

    setCurrentIndex = (currentIndex) => {
        this.setState({
            currentIndex: currentIndex.currentIndex
        })
    }

    duplicateStep = (step, selectedSteps, index) => {
        if(this.props.jobStore.viewable) return;
        let jobStore = {...this.props.jobStore};
        let spec = jobStore.spec;
        let stepTypes = this.props.stepTypes;
        if(typeof index === "object") {
            let arr = [...selectedSteps[index[0]]];
            let stepSub = stepTypes[step.replace(/\d+/,"")];

            let stepData = spec[step.replace(/\d/,"")];
            stepData.push(stepData[index[1]]);
            spec[step.replace(/\d+/,"")] = stepData;
            jobStore.spec = spec;

            stepSub.push(stepSub[index[1]]);
            stepTypes[step.replace(/\d+/,"")] = stepSub;

            arr.push(arr[index[1]].replace(/\d+/,arr.length));
            selectedSteps[index[0]] = [...arr];
        }
        this.props.changeJobStore({...jobStore});
        this.props.toggleSteps(selectedSteps);
        this.props.setStepType(stepTypes);
        // this.setActiveStep(!this.props.jobStore.running ? "ML_Workflow" : this.getFirstStep());
    }

    deleteStep = (step, selectedSteps, index) => {
        if(this.props.jobStore.viewable) return;
        let jobStore = {...this.props.jobStore};
        let stepTypes = this.props.stepTypes;
        if(typeof index === "object") {
            console.log(selectedSteps, index, [...selectedSteps[index[0]]]);
            let arr = [...selectedSteps[index[0]]];
            let stepSub = stepTypes[step.replace(/\d+/,"")];
            arr.splice(index[1], 1);
            stepSub.splice(index[1], 1);
            stepTypes[step.replace(/\d+/,"")] = stepSub;
            const cpyArr = arr.map((elem,i) => {
                return elem.replace(/\d+/, i);
            })
            selectedSteps[index[0]] = [...cpyArr];
            let currKey = jobStore.spec[step.replace(/\d+/,"")];
            currKey && currKey.splice(index[1],1);
            jobStore.spec[step.replace(/\d+/,"")] = currKey;
        } else {
            selectedSteps.splice(index, 1);
            jobStore.spec[step] = {};
            stepTypes[step] = "";
        }
        this.props.changeJobStore({...jobStore});
        this.props.toggleSteps(selectedSteps);
        this.props.setStepType(stepTypes);
        this.setActiveStep(!this.props.jobStore.running ? "ML_Workflow" : this.getFirstStep());
    }

    onDragEnd = (result, i, name) => {
        if(this.props.jobStore.viewable) return;
        // dropped outside the list
        if (!result.destination) {
          return;
        }
        const jobStore = {...this.props.jobStore};
        const step = jobStore.spec[name];
        
        let selectedSteps = [...this.state.selectedSteps];
        const arr = selectedSteps[i];

        let stepTypes = this.props.stepTypes;
        let types = stepTypes[name];

        jobStore.spec[name] = reorderElems(
            step,
            result.source.index,
            result.destination.index
        )

        selectedSteps[i] = reorderElems(
          [...arr],
          result.source.index,
          result.destination.index
        );

        stepTypes[name] = reorderElems(
            [...types],
            result.source.index,
            result.destination.index
        );
            
        this.props.changeJobStore({...jobStore});
        this.props.toggleSteps(selectedSteps);
        this.props.setStepType(stepTypes);
    }

    hasError = (step, index) => {
        const { errorStore } = this.props;
        if(index >= 0 && step) {
            if(errorStore.spec && errorStore.spec[step.replace(/\d+/g,"")] && errorStore.spec[step.replace(/\d+/g,"")][index]) return true;
        } else {
            if(errorStore.name || (errorStore.spec && (errorStore.spec.project_id || errorStore.spec.model_name || errorStore.spec.notifications || errorStore.schedule))) return true;
        }
        return false;
    }

    render() {
        const { selectedSteps, activeStep, arrows, currentIndex } = this.state;
        const { jobStore, errorStore } = this.props;
        return (
            <div style={{height: 'calc(100vh - 136px)'}}>
                
                <div className="timeline-section bg-light overlay">
                    <svg className="svg">
                        {arrows}
                    </svg>
                    {!jobStore.running &&
                    <div className={"relative text-center mb-lg"} style={{minWidth: 200}}>
                        <div id="ML_Workflow">
                            <div className={this.hasError() ? "clickable form-step ml-flow border-danger" : "clickable form-step ml-flow"} aria-selected={activeStep === "ML_Workflow"} onClick={()=>{
                                this.setActiveStep({}, 'ML_Workflow');
                            }}>
                            </div>
                            <span className="f-12">Start</span>
                            {/* <div className="display-none">{setTimeout(()=>this.renderArrows()),10}</div> */}
                        </div>
                    </div>
                    }
                    {
                        selectedSteps.map((e,i)=>{
                            if(typeof e === "string") {
                                const runStatus = jobStore.running ? findStatus(
                                    {
                                        type: e, 
                                        index: -1, 
                                        spec: jobStore.spec[e], 
                                        jobStore
                                    }
                                ) : ""
                                const statusIcon = jobStore.running && getStatusColorIcon(runStatus)
                                return (
                                    <div key={e+i} className={"relative text-center mb-lg pt-xs"} style={{minWidth: 400}}>
                                        <div id={e} className="relative">
                                            <div 
                                                className={errorStore && errorStore.spec && errorStore.spec[e] ? "clickable form-step border-danger" : "clickable form-step"} 
                                                aria-selected={activeStep === e} 
                                                onClick={(event)=>{
                                                    this.setActiveStep(event,e)
                                                }}
                                            >
                                                {jobStore.running && 
                                                <span className="status-icon">
                                                    <Tooltip title={runStatus}>
                                                        <span className="status-icon-bg">
                                                            <i className={statusIcon[1]} style={{color: statusIcon[0]}} />
                                                        </span>
                                                    </Tooltip>
                                                </span>
                                                }
                                                {!jobStore.viewable && 
                                                <div className="floating-btns" aria-label="action-btn">
                                                    <Tooltip title="Delete">
                                                        <span className="inline-block step-close" aria-label="action-btn">
                                                            <i className="fa fa-times" aria-label="action-btn" style={{verticalAlign: "top"}} onClick={()=>{
                                                            confirm("Are you sure? This will clear all the data saved in this step!", "Delete").then(
                                                                () => {
                                                                    this.deleteStep(e, selectedSteps, i)
                                                                }
                                                            )
                                                            }} />
                                                        </span>
                                                    </Tooltip>
                                                </div>}
                                                {/* <JobForm key={e+i} jobtype={e} /> */}
                                                <div className="content-center">
                                                    <ShowDetails name={jobStore.spec[e] && jobStore.spec[e].name} />
                                                </div>
                                            </div>
                                            <span className="f-12">{capitalizeVars(e)}</span>
                                            {/* <div className="display-none">{setTimeout(()=>this.renderArrows()),10}</div> */}
                                        </div>
                                    </div>
                                )
                            } else {
                                return (
                                    <div key={e[0]} className={"relative text-center mb-lg pt-xs"} style={{minWidth: 400}}>
                                        <div id={e[0]} className="relative step-container">
                                            <div className="form-step" style={{background: "#f1f2f1"}} onClick={()=>{
                                                // this.setActiveStep(e[0])
                                            }}>
                                                <DragDropContext onDragEnd={(action)=>{this.onDragEnd(action, i, e[0].replace(/\d/g,""))}}>
                                                    <Droppable droppableId={"droppable"+i}>
                                                    {(provided, snapshot) => (
                                                        <div
                                                        {...provided.droppableProps}
                                                        ref={provided.innerRef}
                                                        style={{
                                                            padding: 8,
                                                            width: 'auto',
                                                            maxHeight: 'inherit'
                                                        }}
                                                        >
                                                        {e.map((item, index) => {
                                                            const name = jobStore.spec[item.replace(/\d/,"")] && jobStore.spec[item.replace(/\d/,"")][index] && jobStore.spec[item.replace(/\d/,"")][index].name;
                                                            const runStatus = jobStore.running ? findStatus(
                                                                {
                                                                    type: item.replace(/\d+/,""), 
                                                                    index, 
                                                                    spec: jobStore.spec[item.replace(/\d+/,"")], 
                                                                    jobStore
                                                                }
                                                            ) : ""
                                                            const statusIcon = jobStore.running && getStatusColorIcon(runStatus)
                                                            return (
                                                            <Draggable key={item} draggableId={item} index={index}>
                                                            {(provided, snapshot) => (
                                                                <div
                                                                ref={provided.innerRef}
                                                                {...provided.draggableProps}
                                                                {...provided.dragHandleProps}
                                                                className={this.hasError(item, index) ? "mb-sm form-step step-container border-danger" : "mb-sm form-step step-container"}
                                                                style={{
                                                                    ...provided.draggableProps.style,
                                                                    userSelect: "none"
                                                                }}
                                                                aria-selected={activeStep === item}
                                                                onClick={(event)=>{
                                                                    if(!snapshot.isDragging) this.setActiveStep(event, item, index)
                                                                }}
                                                                >
                                                                {jobStore.running && 
                                                                <span className="status-icon">
                                                                    <Tooltip title={runStatus || "NOT STARTED"}>
                                                                        <span className="status-icon-bg">
                                                                            <i className={statusIcon[1]} style={{color: statusIcon[0]}} />
                                                                        </span>
                                                                    </Tooltip>
                                                                </span>
                                                                }
                                                                {!jobStore.viewable && 
                                                                <div className="floating-btns" aria-label="action-btn">
                                                                    <Tooltip title="Clone">
                                                                        <span className="inline-block step-close duplicate" aria-label="action-btn">
                                                                            <i className="fa fa-clone" style={{verticalAlign: "top"}} aria-label="action-btn" onClick={()=>{
                                                                                if(jobStore.viewable) return;
                                                                                this.duplicateStep(item, selectedSteps, [i, index]);
                                                                            }}/>
                                                                        </span>
                                                                    </Tooltip>
                                                                    <Tooltip title="Delete">
                                                                        <span className="inline-block step-close" aria-label="action-btn">
                                                                            <i className="fa fa-times" style={{verticalAlign: "top"}} aria-label="action-btn" onClick={()=>{
                                                                                if(jobStore.viewable) return;
                                                                                confirm("Are you sure? This will clear all the data saved in this step!", "Delete").then(
                                                                                    () => { 
                                                                                        this.deleteStep(item, selectedSteps, [i, index])
                                                                                    }
                                                                                )
                                                                            }}/>
                                                                        </span>
                                                                    </Tooltip>
                                                                </div>}
                                                                <ShowDetails name={name} />
                                                                </div>
                                                            )}
                                                            </Draggable>
                                                        )})}
                                                        {provided.placeholder}
                                                        </div>
                                                    )}
                                                    </Droppable>
                                                </DragDropContext>
                                            </div>
                                            <span className="f-12">{capitalizeVars(e[0].replace(/\d/g,""))}</span>
                                            {/* <div className="display-none">{setTimeout(()=>this.renderArrows()),10}</div> */}
                                        </div>
                                    </div>
                                )
                            }
                        })
                    }
                </div>
                {   activeStep &&
                    <JobForm 
                        setActiveStep={this.setActiveStep} 
                        key={activeStep}
                        jobIndex={this.state.index}
                        jobtype={activeStep.replace(/\d/g,"")}
                        currentIndex={currentIndex}
                        setCurrentIndex={this.setCurrentIndex}
                    />
                }
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        selectedSteps: state.jobStore.selectedSteps,
        jobStore: state.jobStore.jobStore,
        errorStore: state.jobStore.jobError,
        stepTypes: state.jobStore.stepTypes
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            toggleSteps,
            changeJobStore,
            setStep,
            setStepType
        },
        dispatch
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(StepsTimeLine);