import React, { Component } from "react";
// import InputRange from "react-input-range";
import "react-input-range/lib/css/index.css";
import { getAllProjects } from "../../api";
import { connect } from "react-redux";
import Select from 'react-select';
import 'react-select/dist/react-select.css';

const jobTypes = [
  'TRAIN_SIMPLE',
  'TRAIN_DISTRIBUTED',
  'DEPLOY_SIMPLE',
  'DEPLOY_TF_SERVING',
  'DEPLOY_AB_TEST',
  'DEPLOY_TRANSFORMER',
  'DEPLOY_GRAPH'
]

class FilterModal extends Component {
  constructor(props) {
    super(props);
    this._isMounted = false;
    this.state = { 
      filters: props.filters,
      filterType: props.filterType,
      projectNameList: [],
      jobTypeList: [],
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if(nextProps.filters.delete) {
        prevState.filters = {};
    }
    const filters = {...nextProps.filters, ...prevState.filters};
    const filterType = nextProps.filterType;
    return {
        filters,
        filterType
    }
  }

  componentDidMount() {
    this._isMounted = true;

    const jobTypeList = jobTypes.map((e,i)=>{
      return {
        label: this.parseString(e),
        value: e,
      }
    })

    this.setState({
      jobTypeList
    })

    getAllProjects(this.props.token, { sort: "createdDate,desc" })
      .then(response => {
        let data = response.data;
        if (response.status) {
          let projectNameList = [];
          data.map(project => {
            return projectNameList.push({
              label: project.name,
              value: project.name
            });
          });
          if(this._isMounted)
            this.setState({ projectNameList });
        }
      })
      .catch(error => console.log(error));
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  parseString = (str) => {
    str = str.toUpperCase();
    return str.replace(/_/g," ").replace(/(\w)(\w{1,})/g,(str,grp1,grp2) => {
        return(grp1+grp2.toLowerCase());
    });
  }

  handleProjectChange = e => {
    let filters = { ...this.state.filters };
    filters['projectName'] = e;
    this.setState({filters: filters })
  } 
  handleJobTypeChange = (e,f) => {
    let filters = { ...this.state.filters };
    filters["typein"] = e;
    this.setState({ filters });
  };
  handleApplyFilters = () => {
    this.props.applyFilters(this.state.filters);
  };
  handleResetFilters = () => {
    this.setState({ filters: {} });
    this.props.applyFilters({});
  };

  render() {
    const { jobTypeList, projectNameList } = this.state;
    const { projectName, typein } = this.state.filters;
    return (
      <div className="modal micromodal-slide" id="modal-1" aria-hidden="true">
        <div className="modal__overlay" tabIndex="-1" data-micromodal-close>
          <div
            className="modal__container"
            role="dialog"
            aria-modal="true"
            aria-labelledby="modal-1-title"
            style={{minHeight: 400}}
          >
            <header className="modal__header">
              <h2 className="modal__title" id="modal-1-title">
                Filter Workflow
              </h2>
              <button
                className="modal__close"
                aria-label="Close modal"
                data-micromodal-close
              />
            </header>
            <main className="modal__content mb-lg" id="modal-1-content">
              <div className="modal-filter-form">
                {/* <div className="form-group row">
                    <label htmlFor="" className="col-md-3">Projects</label>
                    <Select
                        closeOnSelect={true}
                        className= "select-field col-md-8"
                        name={"projectName"}
                        multi
                        onChange={this.handleProjectChange}
                        options={projectNameList}
                        placeholder="Select Projects"
                        removeSelected={true}
                        simpleValue
                        value={projectName}
                    />
                </div> */}
                <div className="form-group row">
                  <label htmlFor="" className="col-md-3">Type</label>
                  
                    <Select
                      closeOnSelect={true}
                      className="select-field col-md-8"
                      name={"typein"}
                      multi
                      onChange={this.handleJobTypeChange}
                      options={jobTypeList}
                      placeholder="Select Workflow type"
                      removeSelected={true}
                      simpleValue
                      value={typein}
                    />

                </div>
              </div>
            </main>
            <footer className="modal__footer filter-form-actions">
              <button
                className="btn btn-primary btn-rounded"
                data-micromodal-close
                onClick={this.handleApplyFilters}
              >
                APPLY FILTERS
              </button>
              <button
                className="btn btn-default btn-rounded"
                data-micromodal-close
                aria-label="Close this dialog window"
                onClick={this.handleResetFilters}
              >
                RESET FILTERS
              </button>
            </footer>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token
  };
};

export default connect(mapStateToProps)(FilterModal);
