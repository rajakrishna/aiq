import React, { Component } from "react";
import {
    capitalizeVars,
    actLikeClick,
    arrHasElem,
    getLength
} from "../../utils/helper";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Link } from "react-router-dom";
import ReactTooltip from "react-tooltip";
import { runJobAction } from "../../ducks/jobs";
import { toggleSteps, setStepType, setStep, changeJobStore } from "../../ducks/jobStore";
import { TextField, Tooltip, Dialog, DialogTitle, DialogActions, DialogContent, AppBar, Toolbar, IconButton, Typography } from "@material-ui/core";
import CloseIcon from '@material-ui/icons/Close';
import { alertMsg } from "../../ducks/alertsReducer";
import moment from "moment";
import { confirm } from "../Confirm";
import ExportSpec from "./ExportSpec";
import ImportSpec from "./ImportSpec";
import FileDownload from "js-file-download";
import YAML from 'yamljs';
import MicroModal from "micromodal";
import Button from '@material-ui/core/Button';
import { globalProjectSelection } from "./../../ducks/projects";
import JobrunPopupWithParameters from "./job-run/JobrunPopupWithParameters.jsx";


class StepsPicker extends Component {

    flag = 1;
    renderingFlag = 0;
    intervalSet = {};

    state = {
        duration: "",
        toolbarMenuShow: {},
        toolbarMenuShow_run: {},
        runWithParams: false,
        runHistory: false,
        params: [],
        exportDialog: false,
        importDialog: false,
        setPrevProject:false
    }

    componentDidMount() {
        const elems = document.querySelectorAll(".expansion");
        for (let i in elems) {
            if (typeof elems[i] === "object" && this.flag) {
                elems[i].addEventListener("click", () => {
                    elems[i].style.height = elems[i].style.height ? "" : "auto";
                })
            }
        }
        this.flag = 0;
        clearInterval(this.intervalSet);
        if(this.props.jobStore.status && this.props.jobStore.status.finished_at) {
            this.findDuration(this.props.jobStore.status.started_at, this.props.jobStore.status.finished_at);
        } else {
            this.intervalSet = setInterval(()=>{
            if(this.props.jobStore.status && this.props.jobStore.status.started_at)
                this.findDuration(this.props.jobStore.status.started_at, new Date());
            }, 1000);
        }
        if(window.sessionStorage.run) {
            this.setState({
                runHistory: true
            }, ()=>{window.sessionStorage.removeItem("run")})
        }
    }
    componentDidUpdate(prevProps) {
        const {setPrevProject}  = this.state 
        if (this.props.selectedProject && prevProps.selectedProject.id !==  this.props.selectedProject.id  && this.getWorkflowType() === "Edit") {
            console.log("componentDidUpdate",prevProps.selectedProject.id)
            MicroModal.show("confirm-modal");
        }
        if(setPrevProject){
            console.log("setPrevProject",prevProps.selectedProject.id)
            this.setState({setPrevProject:false})
            this.setPreProjectAsSelected(prevProps.selectedProject.id)
        }
      
    }

    setPreProjectAsSelected(id){
        const selectedProject = this.props.projects.filter(
            (item) => item.id == id
          );
          console.log("setPreProjectAsSelected",selectedProject[0].id)
          this.props.globalProjectSelection(selectedProject[0]);
          console.log("setPreProjectAsSelected2",this.props.selectedProject.id)
    }

    sleep = (ms) => {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    downloadSpec = (type) => {
        const spec = {...this.props.jobStore}
        const filename = spec.id;
        delete spec.viewable;
        delete spec.render_job;
        delete spec.running;
        if(type === "JSON") {
            const SpecToFile = new Blob([JSON.stringify(spec)], {type : 'application/text'});
            FileDownload(SpecToFile, filename+".json");
        } else {
            const SpecToFile = new Blob([YAML.stringify(spec,100)], {type : 'application/text'});
            FileDownload(SpecToFile, filename+".yml");
        }
    }

    renderEverything = async() => {
        if (this.renderingFlag) return;
        this.renderingFlag = 1;
        const steps = this.props.steps;
        const jobStore = this.props.jobStore;
        jobStore.spec && Object.keys(jobStore.spec).map(async(e, i) => {
            if (steps.includes(e)) {
                await this.sleep(100);
                if ((/pre|post/g).test(e)) {
                    jobStore.spec[e] && jobStore.spec[e].map((elem, num) => {
                        this.addSteps(e, elem.sub_type)
                    })
                } else {
                    jobStore.spec[e] && this.addSteps(e, jobStore.spec[e].sub_type)
                }
            }
        })
        this.props.changeJobStore({ ...jobStore, render_job: null })
    }

    componentWillUnmount() {
        const elems = document.querySelectorAll(".expansion");
        for (let i in elems) {
            if (typeof elems[i] === "object") {
                elems[i].removeEventListener("click", () => {
                    elems[i].style.height = elems[i].style.height ? "" : "auto";
                })
            }
        }
        clearInterval(this.intervalSet);
    }

    runJob() {
        this.props.alertMsg("Submitted!", "success");
        let finalData = this.props.jobStore.id;
        this.props.runJobAction(this.props.token, finalData);
        this.setState({
            params: [],
            runWithParams: false,
            toolbarMenuShow_run: {}
        })
        setTimeout(()=>{
            this.props.history.push("/run-history",{jobId: finalData});
            // this.setState({
            //     runHistory: true
            // })
        },3000);
    }

    addSteps = (step, subType = "") => {
        const selectedSteps = [...this.props.selectedSteps];
        let stepTypes = this.props.stepTypes;
        if ((/pre|post/g).test(step)) {
            let index = selectedSteps.length;
            let reg = new RegExp(step.replace(/\d/g, ""), "g");
            selectedSteps.forEach((elem, i) => {
                if (typeof elem === 'object' && elem.some(val => (reg).test(val))) {
                    index = i
                }
            });
            if (typeof selectedSteps[index] === "object") {
                selectedSteps[index].push(step + (parseInt(selectedSteps[index][selectedSteps[index].length - 1].replace(/[a-zA-Z_]/g, ""), 10) + 1));
            } else {
                selectedSteps[index] = [step + 0];
            }
            if (subType)
                if (stepTypes[step.replace(/\d/g, "")]) {
                    stepTypes[step.replace(/\d/g, "")].push(subType);
                } else {
                    stepTypes[step.replace(/\d/g, "")] = [subType];
                }

        } else if (!selectedSteps.includes(step)) {
            selectedSteps.push(step);
            if (subType)
                stepTypes[step] = subType
        }
        this.props.toggleSteps(selectedSteps);
        if (subType) this.props.setStepType(stepTypes);
        this.setState({
            toolbarMenuShow: {}
        })
    }

    hasElement = (selectedSteps, step) => {
        let flag = false;
        if (arrHasElem(selectedSteps, step) > -1) {
            flag = true;
        }
        return flag;
    }

    handleCancelModel = () =>{
     this.setState({setPrevProject:true})
     MicroModal.close('confirm-modal')
    }

    getClickStyle = (name, flag = 0) => {
        const btn = document.querySelector(`button[aria-label='${name}']`);
        return ({
            display: "block",
            transform: `translate3d(${btn.offsetLeft + (flag ? (-btn.offsetWidth * 2) : 0)}px, ${btn.offsetHeight + btn.offsetTop}px, 0px)`,
            top: 0,
            left: 0,
            willChange: "transform"
        })
    }

    getHoverStyle = (name) => {
        const btn = document.querySelector(`div[aria-label='${name}']`);
        return ({
            display: "block",
            transform: `translate3d(${btn.offsetLeft + btn.offsetWidth}px, ${btn.offsetTop}px, 0px)`,
            top: 0,
            left: 0,
            willChange: "transform"
        })
    }

    handleParams = (event, index) => {
        const target = event.target;
        const name = (target.name).replace(/\d/g, "");
        const value = target.value;
        let params = [...this.state.params];
        params[index][name] = value;
        this.setState({ params });
    }

    addParams = () => {
        const params = [...this.state.params];
        params.push({ name: "", value: "" });
        this.setState({
            params
        })
    }

    delParams = (i) => {
        let params = [...this.state.params];
        params.splice(i, 1);
        this.setState({ params });
    }

    getColor(type) {
        const status = {
            'CREATED': "info",
            'STARTED': "info",
            'RUNNING': "info",
            'FAILED': "failed",
            'ERRORED': "failed",
            'SUCCEEDED': "success",
            'TERMINATED': "primary"
        }
        const color = status[type] ? status[type] : 'primary';
        return color;
    }


    findDuration(startTime, endTime=null) {
        if (!startTime)
            return;

        let duration = "";

        let st = new Date(startTime),
            et = new Date(endTime),
            diff = ((et.getTime() - st.getTime()) / 1000),
            mins = 0,
            hour = 0,
            days = 0;

        while (diff >= 60) {
            diff -= 60;
            ++mins;
        }
        while (mins >= 60) {
            mins -= 60;
            ++hour;
        }
        while (hour >= 24) {
            hour -= 24;
            ++days;
        }

        hour = hour < 10 ? '0' + Math.round(hour) : Math.round(hour);
        mins = mins < 10 ? '0' + Math.round(mins) : Math.round(mins);
        diff = diff < 10 ? '0' + Math.round(diff) : Math.round(diff);
        if (days) {
            duration = `${days} Day`
        } else if (parseInt(hour, 10)) {
            duration = `${hour}:${mins} Hr`
        } else if (parseInt(mins, 10)) {
            duration = `${mins}:${diff} Min`
        } else if (parseInt(diff, 10)) {
            duration = `${diff} Sec`
        }
        this.setState({
            duration
        })
    }

    getWorkflowType = () => {
        const {viewable, render_job, running, id} = this.props.jobStore;
        if((!id || render_job) && !running) {
            return "New";
        }
        if(id && viewable && !running) {
            return "View";
        }
        if(id && !viewable && !running) {
            return "Edit";
        }
        if(running) {
            return "Run";
        }
    }

    renderRunButton = () => {
        const {jobStore} = this.props
        return (
            <div className="inline-block">
                {jobStore.spec.parameters && jobStore.spec.parameters.length > 0 ?
                    <JobrunPopupWithParameters />
                    : 
                    <Tooltip title="Delete">
                        <button
                            type="button"
                            className="toolbar-btn-primary clickable ml mt-sm mr"
                            onClick={() => {
                                this.runJob();
                                // this.setState({
                                //     toolbarMenuShow_run: Object.keys(toolbarMenuShow_run).length ? {} : this.getClickStyle("toolbarMenuShow_run", 1)
                                // })
                            }}
                        >
                                Run
                        </button>  
                    </Tooltip>
                }
            </div>
        )
    }

    render() {
        const { toolbarMenuShow, toolbarMenuShow_run, runWithParams, params, duration, runHistory, exportDialog, importDialog } = this.state;
        const { selectedSteps, jobStore } = this.props;
        (this.props.jobStore.viewable || this.props.jobStore.render_job) && this.renderEverything();

        return (
            <div className="job-toolbar m-0">
                <div className="inline-block">
                    <Tooltip title="Navigate to previous page">
                        <button
                            type="button"
                            className="toolbar-btn-default clickable ml mt-sm"
                            aria-label="toolbarMenuShow"
                            onClick={() => {
                                if(this.getWorkflowType() === "Edit" || this.getWorkflowType() === "New") {
                                    confirm("You have unsaved changes that will be lost if you decide to continue.", 
                                    "Are you sure you want to leave this page?",
                                    "Leave this Page",
                                    "Stay on this Page").then(
                                        () => {
                                            this.props.history.goBack()
                                        }
                                    )
                                } else if(this.getWorkflowType() === "Run") {
                                    window.sessionStorage.setItem("run", true);
                                    this.props.history.goBack();
                                } else 
                                    this.props.history.goBack();
                            }}
                        >
                            <i className="fa fa-chevron-left" /> Back
                        </button>
                    </Tooltip>
                </div>
                <div className="inline-block" style={jobStore.viewable ? { display: 'none' } : {}}>
                    <button
                        type="button"
                        className="toolbar-btn-primary clickable ml mt-sm"
                        aria-label="toolbarMenuShow"
                        onClick={() => {
                            this.setState({
                                toolbarMenuShow: Object.keys(toolbarMenuShow).length ? {} : this.getClickStyle("toolbarMenuShow")
                            })
                        }}
                    >
                        Add Step <i className="fa fa-caret-down" />
                    </button>
                    <div 
                        className="backdrop" onClick={(e)=>{
                            if(e.target.getAttribute("data-name") === "backdrop") {
                                this.setState({
                                    toolbarMenuShow: {}
                                })
                            }
                        }} 
                        data-name="backdrop" 
                        style={Object.keys(toolbarMenuShow).length ? {} : {display: "none"}}
                    >
                        <div className="toolbar-menu notification-wraper" data-name="toolbar" style={toolbarMenuShow}>
                            <div
                                tabIndex={1}
                                className={"toolbar-menu-item step-select expansion"}
                                aria-selected={this.hasElement(selectedSteps, "pre_train_steps")}
                            >
                                <span style={{ display: "block", height: 26 }}>
                                    {capitalizeVars("pre_train_steps")} {arrHasElem(selectedSteps, "pre_train_steps") > -1 ? <span className="count-icon">{getLength(selectedSteps, "pre_train_steps")}</span> : ""}
                                </span>
                                <div
                                    className={"toolbar-menu-item step-select clickable"}
                                    onKeyPress={(e) => {
                                        actLikeClick(e, this.addSteps, "pre_train_steps");
                                    }}
                                    onClick={() => {
                                        this.addSteps("pre_train_steps", "SIMPLE")
                                    }}
                                >
                                    Custom Code
                                </div>
                                <div
                                    className={"toolbar-menu-item step-select clickable"}
                                    onKeyPress={(e) => {
                                        actLikeClick(e, this.addSteps, "pre_train_steps");
                                    }}
                                    onClick={() => {
                                        this.addSteps("pre_train_steps", "SPARK")
                                    }}
                                >
                                    Spark
                                </div>
                            </div>
                            <div
                                tabIndex={1}
                                className={"toolbar-menu-item step-select expansion"}
                                aria-selected={this.hasElement(selectedSteps, "train")}
                            >
                                <span style={{ display: "block", height: 26 }}>
                                    {capitalizeVars("train")}
                                </span>
                                <div
                                    className={"toolbar-menu-item step-select clickable"}
                                    onKeyPress={(e) => {
                                        actLikeClick(e, this.addSteps, "train");
                                    }}
                                    onClick={() => {
                                        this.addSteps("train", "TRAIN_SIMPLE")
                                    }}
                                >Custom Code</div>
                                <div
                                    className={"toolbar-menu-item step-select clickable"}
                                    onKeyPress={(e) => {
                                        actLikeClick(e, this.addSteps, "train");
                                    }}
                                    onClick={() => {
                                        this.addSteps("train", "TRAIN_DISTRIBUTED")
                                    }}
                                >Tensorflow Distributed Training</div>
                            </div>
                            <div
                                tabIndex={1}
                                className={"toolbar-menu-item step-select expansion"}
                                aria-selected={this.hasElement(selectedSteps, "post_train_steps")}
                            >
                                <span style={{ display: "block", height: 26 }}>
                                    {capitalizeVars("post_train_steps")} {arrHasElem(selectedSteps, "post_train_steps") > -1 ? <span className="count-icon">{getLength(selectedSteps, "post_train_steps")}</span> : ""}
                                </span>
                                <div
                                    className={"toolbar-menu-item step-select clickable"}
                                    onKeyPress={(e) => {
                                        actLikeClick(e, this.addSteps, "post_train_steps");
                                    }}
                                    onClick={() => {
                                        this.addSteps("post_train_steps", "SIMPLE")
                                    }}
                                >
                                    Custom Code
                                </div>
                                
                                <div
                                    className={"toolbar-menu-item step-select clickable"}
                                    onKeyPress={(e) => {
                                        actLikeClick(e, this.addSteps, "post_train_steps");
                                    }}
                                    onClick={() => {
                                        this.addSteps("post_train_steps", "SPARK")
                                    }}
                                >
                                    Spark
                                </div>
                            </div>
                            <div
                                tabIndex={1}
                                className={"toolbar-menu-item step-select expansion"}
                                aria-selected={this.hasElement(selectedSteps, "pre_validate_steps")}
                            >
                                <span style={{ display: "block", height: 26 }}>
                                    {capitalizeVars("pre_validate_steps")} {arrHasElem(selectedSteps, "pre_validate_steps") > -1 ? <span className="count-icon">{getLength(selectedSteps, "pre_validate_steps")}</span> : ""}
                                </span>
                                <div
                                    className={"toolbar-menu-item step-select clickable"}
                                    onKeyPress={(e) => {
                                        actLikeClick(e, this.addSteps, "pre_validate_steps");
                                    }}
                                    onClick={() => {
                                        this.addSteps("pre_validate_steps", "SIMPLE")
                                    }}
                                >
                                    Custom Code
                                </div>
                                <div
                                    className={"toolbar-menu-item step-select clickable"}
                                    onKeyPress={(e) => {
                                        actLikeClick(e, this.addSteps, "pre_validate_steps");
                                    }}
                                    onClick={() => {
                                        this.addSteps("pre_validate_steps", "SPARK")
                                    }}
                                >
                                    Spark
                                </div>
                            </div>
                            <div
                                tabIndex={1}
                                className={"toolbar-menu-item step-select expansion"}
                                // onKeyPress={(e) => {
                                //     actLikeClick(e, this.addSteps, "validate");
                                // }}
                                // onClick={() => {
                                //     this.addSteps("validate")
                                // }}
                                aria-selected={this.hasElement(selectedSteps, "validate")}
                            >
                                <span style={{ display: "block", height: 26 }}>
                                    {capitalizeVars("validate")}
                                </span>
                                <div
                                    className={"toolbar-menu-item step-select clickable"}
                                    onKeyPress={(e) => {
                                        actLikeClick(e, this.addSteps, "validate");
                                    }}
                                    onClick={() => {
                                        this.addSteps("validate", "SIMPLE")
                                    }}
                                >
                                    Custom Code
                                </div>
                                
                                <div
                                    className={"toolbar-menu-item step-select clickable"}
                                    onKeyPress={(e) => {
                                        actLikeClick(e, this.addSteps, "post_validate_steps");
                                    }}
                                    onClick={() => {
                                        this.addSteps("post_validate_steps", "SPARK")
                                    }}
                                >
                                    Spark
                                </div>
                            </div>
                            <div
                                tabIndex={1}
                                className={"toolbar-menu-item step-select expansion"}
                                aria-selected={this.hasElement(selectedSteps, "post_validate_steps")}
                            >
                                <span style={{ display: "block", height: 26 }}>
                                    {capitalizeVars("post_validate_steps")} {arrHasElem(selectedSteps, "post_validate_steps") > -1 ? <span className="count-icon">{getLength(selectedSteps, "post_validate_steps")}</span> : ""}
                                </span>
                                <div
                                    className={"toolbar-menu-item step-select clickable"}
                                    onKeyPress={(e) => {
                                        actLikeClick(e, this.addSteps, "post_validate_steps");
                                    }}
                                    onClick={() => {
                                        this.addSteps("post_validate_steps", "SIMPLE")
                                    }}
                                >
                                    Custom Code
                                </div>
                                <div
                                    className={"toolbar-menu-item step-select clickable"}
                                    onKeyPress={(e) => {
                                        actLikeClick(e, this.addSteps, "pre_deploy_steps");
                                    }}
                                    onClick={() => {
                                        this.addSteps("pre_deploy_steps", "SPARK")
                                    }}
                                >
                                    Spark
                                </div>
                            </div>
                            <div
                                tabIndex={1}
                                className={"toolbar-menu-item step-select expansion"}
                                aria-selected={this.hasElement(selectedSteps, "pre_deploy_steps")}
                            >
                                <span style={{ display: "block", height: 26 }}>
                                    {capitalizeVars("pre_deploy_steps")} {arrHasElem(selectedSteps, "pre_deploy_steps") > -1 ? <span className="count-icon">{getLength(selectedSteps, "pre_deploy_steps")}</span> : ""}
                                </span>
                                <div
                                    className={"toolbar-menu-item step-select clickable"}
                                    onKeyPress={(e) => {
                                        actLikeClick(e, this.addSteps, "pre_deploy_steps");
                                    }}
                                    onClick={() => {
                                        this.addSteps("pre_deploy_steps", "SIMPLE")
                                    }}
                                >
                                    Custom Code
                                </div>
                            </div>
                            <div
                                tabIndex={1}
                                className={"toolbar-menu-item step-select expansion"}
                                aria-selected={this.hasElement(selectedSteps, "deploy")}
                            >
                                <span style={{ display: "block", height: 26 }}>
                                    {capitalizeVars("deploy")}
                                </span>
                                <div
                                    className={"toolbar-menu-item step-select clickable"}
                                    onKeyPress={(e) => {
                                        actLikeClick(e, this.addSteps, "deploy");
                                    }}
                                    onClick={() => {
                                        this.addSteps("deploy", "DEPLOY_SIMPLE")
                                    }}
                                >Custom Code</div>
                                <div
                                    className={"toolbar-menu-item step-select clickable"}
                                    onKeyPress={(e) => {
                                        actLikeClick(e, this.addSteps, "deploy");
                                    }}
                                    onClick={() => {
                                        this.addSteps("deploy", "DEPLOY_TF_SERVING")
                                    }}
                                >Tensorflow Model</div>
                                <div
                                    className={"toolbar-menu-item step-select clickable"}
                                    onKeyPress={(e) => {
                                        actLikeClick(e, this.addSteps, "deploy");
                                    }}
                                    onClick={() => {
                                        this.addSteps("deploy", "DEPLOY_ARTIFACT")
                                    }}
                                >Deploy Artifact</div>
                                <div
                                    className={"toolbar-menu-item step-select clickable"}
                                    onKeyPress={(e) => {
                                        actLikeClick(e, this.addSteps, "deploy");
                                    }}
                                    onClick={() => {
                                        this.addSteps("deploy", "DEPLOY_AB_TEST")
                                    }}
                                >A/B Test</div>
                                <div
                                    className={"toolbar-menu-item step-select clickable"}
                                    onKeyPress={(e) => {
                                        actLikeClick(e, this.addSteps, "deploy");
                                    }}
                                    onClick={() => {
                                        this.addSteps("deploy", "DEPLOY_TRANSFORMER")
                                    }}
                                >Model with Transformer</div>
                                <div
                                    className={"toolbar-menu-item step-select clickable"}
                                    onKeyPress={(e) => {
                                        actLikeClick(e, this.addSteps, "deploy");
                                    }}
                                    onClick={() => {
                                        this.addSteps("deploy", "DEPLOY_GRAPH")
                                    }}
                                >Graph</div>
                            </div>
                            <div
                                tabIndex={1}
                                className={"toolbar-menu-item step-select expansion"}
                                aria-selected={this.hasElement(selectedSteps, "post_deploy_steps")}
                            >
                                <span style={{ display: "block", height: 26 }}>
                                    {capitalizeVars("post_deploy_steps")} {arrHasElem(selectedSteps, "post_deploy_steps") > -1 ? <span className="count-icon">{getLength(selectedSteps, "post_deploy_steps")}</span> : ""}
                                </span>
                                <div
                                    className={"toolbar-menu-item step-select clickable"}
                                    onKeyPress={(e) => {
                                        actLikeClick(e, this.addSteps, "post_deploy_steps");
                                    }}
                                    onClick={() => {
                                        this.addSteps("post_deploy_steps", "SIMPLE")
                                    }}
                                >
                                    Custom Code
                                </div>
                                
                                <div
                                    className={"toolbar-menu-item step-select clickable"}
                                    onKeyPress={(e) => {
                                        actLikeClick(e, this.addSteps, "post_deploy_steps");
                                    }}
                                    onClick={() => {
                                        this.addSteps("post_deploy_steps", "SPARK")
                                    }}
                                >
                                    Spark
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="inline-block ml">
                    <span className="detail-text">
                        <b>
                            Workflows&nbsp;
                            {jobStore.metadata && jobStore.metadata.labels.jobId &&
                            <Link
                                to={`/workflows/${jobStore.metadata.labels.jobId}`}
                            >
                                {jobStore.metadata && jobStore.metadata.labels.jobName}
                            </Link>}
                            &nbsp;&gt;&nbsp;
                            {this.getWorkflowType()}&nbsp;
                        </b>
                        {jobStore.name || (jobStore.metadata && jobStore.metadata.name)}
                    </span>
                </div>
                {jobStore.running &&
                    <div className="inline-block ml">
                        <label
                            className={`model-item-status status-${this.getColor(jobStore.status && jobStore.status.phase)}`}
                        >
                            {jobStore.status && jobStore.status.phase}
                        </label>
                    </div>}
                {jobStore.running &&
                    <div className="inline-flex float-right">
                        {jobStore.running && 
                        <Tooltip title={"Refresh"}>
                            <button id="syncBtn" className="mr-sm mt-sm btn btn-sm btn-default" onClick={()=>{this.props.fetchMainDetails()}}>
                                <i id="syncBtnIco" className="fa fa-sync" />
                            </button>
                        </Tooltip>}
                        <button
                            type="button"
                            className="toolbar-btn-info clickable dropdown-toggle content-none"
                            // data-toggle="dropdown"
                            // aria-haspopup="true"
                            // aria-expanded="false"
                            data-toggle="modal"
                            data-target="#view-inputs"
                            style={{marginTop: 2}}
                        >
                            <i className={(jobStore.status && !jobStore.status.steps && jobStore.status.phase === "FAILED" ? "fa fa-exclamation-triangle text-danger" : "fa fa-info-circle")}></i>
                        </button>
                        {/* <div className="dropdown-menu br-0 pd dropdown-menu-right mt-sm">
                            <div style={{ minWidth: 200, maxWidth: 450 }}>
                                <ul className="notification-list monospace">
                                    {jobStore.metadata && jobStore.metadata.labels && jobStore.metadata.labels.jobName &&
                                    <li className="notification-item">
                                        Workflow: <Link to={`/workflows/${jobStore.metadata.labels.jobId}`}>{jobStore.metadata.labels.jobName}</Link>
                                    </li>}
                                    {jobStore.status && jobStore.status.started_at &&
                                    <li className="notification-item">
                                        Stated At: {moment(jobStore.status.started_at).format("lll")}
                                    </li>}
                                    {jobStore.status && jobStore.status.finished_at &&
                                    <li className="notification-item">
                                        Finished At: { moment(jobStore.status.finished_at).format("lll")}
                                    </li>}
                                    {duration &&
                                    <li className="notification-item">
                                        Duration: {duration}
                                    </li>}
                                    {jobStore.status && jobStore.status.phase === "FAILED" && !jobStore.status.steps &&
                                    <li className="notification-item">
                                        Error: <span className="text-danger">{jobStore.status.message}</span>
                                    </li>}
                                </ul>
                            </div>
                        </div> */}
                    </div>}                    
                {jobStore.viewable && !jobStore.running &&
                    <div className="inline-block float-right">
                        <button
                            type="button"
                            className="toolbar-btn-primary clickable ml mt-sm mr"
                            onClick={() => {
                                this.props.history.push("/run-history", {jobId: jobStore.id});
                                // this.setState({
                                //     runHistory: true
                                // })
                            }}
                        >
                            Run History
                        </button>

                        {jobStore.schedule === null ? this.renderRunButton(): <Tooltip title="Job is Sheduled">
                            <button
                        type="button"
                        aria-label="toolbarMenuShow_run"
                        className={!jobStore.schedule.suspend ? "toolbar-btn-default clickable ml mt-sm mr" : "toolbar-btn-primary clickable ml mt-sm mr"}
                        disabled={!jobStore.schedule.suspend}
                        onClick={() => {
                            this.runJob();
                            // this.setState({
                            //     toolbarMenuShow_run: Object.keys(toolbarMenuShow_run).length ? {} : this.getClickStyle("toolbarMenuShow_run", 1)
                            // })
                        }}
                    >
                            Run
                    </button>
                            </Tooltip>
                    }

                        {/* <div className="toolbar-menu notification-wraper" style={toolbarMenuShow_run}>
                            <div
                                tabIndex={1}
                                className={"toolbar-menu-item step-select clickable"}
                                onClick={() => {
                                    this.runJob()
                                }}
                            >
                                <span style={{ display: "block", height: 26 }}>
                                    Run
                            </span>
                            </div>
                            <div
                                tabIndex={1}
                                className={"toolbar-menu-item step-select clickable"}
                                onClick={() => {
                                    this.setState({
                                        runWithParams: true
                                    })
                                }}
                            >
                                <span style={{ display: "block", height: 26 }}>
                                    Run with params
                                </span>
                            </div>
                        </div> */}
                    </div>}
                {!jobStore.viewable &&
                    <div className="inline-block float-right">
                        <button
                            type="button"
                            className="toolbar-btn-primary clickable ml mt-sm mr"
                            onClick={() => this.props.submitJob()}
                        >
                            Save
                    </button>
                    </div>}
                {jobStore.viewable && !jobStore.running &&
                    <div className="inline-block float-right">
                        <button
                            type="button"
                            className="toolbar-btn-primary clickable ml mt-sm mr dropdown-toggle"
                            data-toggle="dropdown"
                        >
                            Export
                        </button>
                        <div className="dropdown-menu">
                            <span className="dropdown-item clickable f-14" onClick={()=>{
                                this.downloadSpec("YAML")
                            }}>YAML</span>
                            <span className="dropdown-item clickable f-14" onClick={()=>{
                                this.downloadSpec("JSON")
                            }}>JSON</span>
                        </div>
                    </div>}
                {jobStore.viewable && !jobStore.running &&
                    <div className="inline-block float-right">
                        <button
                            type="button"
                            className="toolbar-btn-primary clickable ml mt-sm mr"
                            onClick={()=>{this.setState({
                                exportDialog: true
                            })}}
                        >
                            View Spec
                        </button>
                    </div>}
                {jobStore.viewable && !jobStore.running &&
                    <div className="inline-block float-right">
                        <button
                            type="button"
                            className="toolbar-btn-primary clickable ml mt-sm mr"
                            onClick={() => this.props.changeJobStore({ ...jobStore, viewable: false })}
                        >
                            <i className="fa fa-edit"></i>
                        </button>
                    </div>}
                <Dialog open={runWithParams}>
                    <DialogTitle id="run-dialog">
                        Run with parameters
                    </DialogTitle>
                    <DialogContent>
                        <div className="container-fluid">
                            <div className="row mb">
                                <div className="col-sm-10">
                                    <span className="text-gray">Click the + icon to add param</span>
                                </div>
                                <div className="col-sm-2">
                                    <button className="btn btn-sm btn-primary" onClick={() => this.addParams()}>
                                        <i className="fa fa-plus-circle" />
                                    </button>
                                </div>
                            </div>
                            {params.map((e, i) => {
                                return (
                                    <div className="row">
                                        <div className="col-sm-5">
                                            <TextField
                                                label="Name"
                                                name={"name" + i}
                                                value={e.name}
                                                margin="normal"
                                                fullWidth
                                                onChange={(event) => { this.handleParams(event, i) }}
                                            >
                                            </TextField>
                                        </div>
                                        <div className="col-sm-5">
                                            <TextField
                                                label="Value"
                                                name={"value" + i}
                                                value={e.value}
                                                margin="normal"
                                                fullWidth
                                                onChange={(event) => this.handleParams(event, i)}
                                            >
                                            </TextField>
                                        </div>
                                        <div className="col-sm-2" style={{ marginTop: 34 }}>
                                            <button className="btn btn-sm btn-default" onClick={() => { this.delParams(i) }}>
                                                <i className="text-danger fa fa-trash"></i>
                                            </button>
                                        </div>
                                    </div>
                                )
                            })}
                        </div>
                    </DialogContent>
                    <DialogActions>
                        <button
                            className="btn btn-danger btn-sm"
                            onClick={() => {
                                this.setState({
                                    runWithParams: false
                                })
                            }} color="primary"
                        >
                            Cancel
                        </button>
                        <button
                            type="button"
                            className="btn btn-primary btn-sm"
                            onClick={() => this.runJob()}
                        >
                            Run
                        </button>
                    </DialogActions>
                </Dialog>
                <Dialog 
                    open={exportDialog}
                    fullWidth={true}
                    maxWidth={'sm'}
                >
                    <DialogTitle id="run-dialog">
                        Export As
                    </DialogTitle>
                    <DialogContent>
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-sm-12 mt-sm">
                                    <ExportSpec />
                                </div>
                            </div>
                        </div>
                    </DialogContent>
                    <DialogActions>
                        <button
                            className="btn btn-danger btn-sm"
                            onClick={() => {
                                this.setState({
                                    exportDialog: false
                                })
                            }} color="primary"
                        >
                            Close
                        </button>
                    </DialogActions>
                </Dialog>
                <ImportSpec 
                    importDialog={importDialog} 
                    closeImport={()=>this.setState({
                        importDialog: false
                    })}
                    renderEverything={this.renderEverything}
                />
                {/* <Dialog fullScreen open={runHistory}>
                    <AppBar className="relative" color="inherit">
                        <Toolbar>
                            <Typography variant="h6" color="inherit" style={{flex: 1}}>
                                Run History
                            </Typography>
                            <IconButton edge="start" color="inherit" onClick={()=>{this.setState({runHistory: false})}} aria-label="close">
                                <CloseIcon />
                            </IconButton>
                        </Toolbar>
                    </AppBar>
                    <DialogContent dividers>
                        <div className="container-fluid">
                            <div className="row mb">
                                <div className="col-md-12">
                                    <JobsRunList id={jobStore.id} />
                                </div>
                            </div>
                        </div>
                    </DialogContent> */}
                    {/* <DialogActions>
                        <button
                            className="btn btn-danger btn-sm"
                            onClick={() => {
                                this.setState({
                                    runHistory: false
                                })
                            }} color="primary"
                        >
                            Close
                        </button>
                    </DialogActions> */}
                {/* </Dialog> */}
                <div className="inline-block">
     
     <div
 className="modal micromodal-slide"
 id="confirm-modal"
 aria-hidden="true"
 >
 <div className="modal__overlay" tabIndex="-1" data-micromodal-close>
 <div
 className="modal__container"
 role="dialog"
 aria-modal="true"
 aria-labelledby="secret-modal-title"
 >
 <header className="modal__header">
 <h2 className="modal__title">Are you sure you want to change the project?</h2>
 </header>
 <main className="mt-3">
 <div className="modal-filter-form">
 <div>
 <p>You have unsaved changes that will be lost if you decide to continue.</p>
 </div>
 <div className="f-r">
 <Button color="secondary" onClick={()=>{this.handleCancelModel()}}>
 Cancel 
 </Button>
 <Button color="primary" onClick={()=>{this.props.history.goBack(),MicroModal.close('confirm-modal')}}>
 Continue 
 </Button>

 </div>

 </div>
 </main>
 </div>
 </div>
 </div>
 </div>
 <div className="modal" tabIndex="-1" role="dialog" id="view-inputs">
          <div className="modal-dialog modal-md" role="document" style={{maxWidth:"95vw",width:"fit-content"}}>
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Workflows Run Details</h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="notification-wraper" style={{ minWidth: 500}}>
                <ul className="notification-list monospace">
                   {jobStore.metadata && jobStore.metadata.name && 
                    <li className="notification-item">
                        RunName: {jobStore.metadata.name}
                    </li>}
                    {jobStore.status && jobStore.status.phase &&
                    <li className="notification-item">
                        Status: {jobStore.status.phase}
                    </li>}
                    {jobStore.metadata && jobStore.metadata.labels && jobStore.metadata.labels.jobName &&
                    <li className="notification-item">
                        Workflow: <Link to={`/workflows/${jobStore.metadata.labels.jobId}`}>{jobStore.metadata.labels.jobName}</Link>
                    </li>}
                    {jobStore.status && jobStore.status.started_at &&
                    <li className="notification-item">
                        Stated At: {moment(jobStore.status.started_at).format("lll")}
                    </li>}
                    {jobStore.status && jobStore.status.finished_at &&
                    <li className="notification-item">
                        Finished At: { moment(jobStore.status.finished_at).format("lll")}
                    </li>}
                    {duration &&
                    <li className="notification-item">
                        Duration: {duration}
                    </li>}
                    {jobStore.status && jobStore.status.phase === "FAILED" && !jobStore.status.steps &&
                    <li className="notification-item">
                        Error: <span className="text-danger">{jobStore.status.message}</span>
                    </li>}
                    {  jobStore.spec && jobStore.spec.parameters &&
                        <li className="notification-item">
                            Parameters:
                            <div className="notification-list monospace">
                                <div class="d-flex flex-column">
                                    { jobStore.spec.parameters.map((item)=> {
                                        return(
                                 <div class="d-flex flex-row">
                                    <div class="p-2 text-truncate"
                                    data-tip
                                    data-for={item.name}
                                    >
                                    {item.name}
                                    </div>
                                    <ReactTooltip
                                        id={item.name}
                                        aria-haspopup="true"
                                    >
                                        <p  style={{marginBottom:"inherit"}}>{item.name}</p>
                                    </ReactTooltip>
                                    <div class="p-2">:</div>
                                    <div class="p-2 text-truncate"
                                    data-tip
                                    data-for={item.value}
                                    >
                                    {item.value}
                                    </div>
                                    <ReactTooltip
                                        id={item.value}
                                        aria-haspopup="true"
                                    >
                                        <p  style={{marginBottom:"inherit"}}>{item.value}</p>
                                    </ReactTooltip>
                                 </div>
                                        )
                                    })
                                    }
                                </div>
                            </div>
                        </li>
                    }
                </ul>
             </div>
            <div className="modal-footer">
                <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
                >
                Close
                </button>
            </div>
           </div>
          </div>
        </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        selectedSteps: state.jobStore.selectedSteps,
        currentStep: state.jobStore.currentStep,
        stepTypes: state.jobStore.stepTypes,
        jobStore: state.jobStore.jobStore,
        token: state.auth.token,
        selectedProject: state.projects.globalProject,
        projects: state.projects.summary,
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            alertMsg,
            runJobAction,
            toggleSteps,
            setStep,
            setStepType,
            changeJobStore,
            globalProjectSelection
        },
        dispatch
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(StepsPicker);