import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { changeJobStore, setStepType } from "../../ducks/jobStore";

import StepForm from "./StepForm";

const mapSpecToState = (data) => {
    switch(data.sub_type) {
        case "TRAIN_DISTRIBUTED":
            return ({
                name: data.name || null,
                type: "TRAIN",
                sub_type: data.sub_type || null,
                source: data.source || {},
                docker: data.docker || {},
                data_source: data.data_source || [],
                outputs: data.outputs || [],
                env: data.env || [],
                env_source: data.env_source || [],
                resources: data.resources || {},       
            })
        default:
            return ({
                name: data.name || null,
                type: "TRAIN",
                sub_type: data.sub_type || null,
                source: data.source || {},
                data_source: data.data_source || [],
                outputs: data.outputs || [],
                env: data.env || [],
                env_source: data.env_source || [],
                resources: data.resources || {},       
            })
    }
}

class TrainStepForm extends Component {
    trainTypes = [
        "TRAIN_SIMPLE",
        "TRAIN_DISTRIBUTED"
    ]
    state = {
        sub_type: this.props.stepTypes["train"] || this.props.spec.sub_type,
        spec: mapSpecToState({...this.props.spec, sub_type: this.props.stepTypes["train"] || this.props.spec.sub_type}),
        error: this.props.jobError && this.props.jobError.spec && {...this.props.jobError.spec.train}
    }

    static getDerivedStateFromProps(nextProps) {
        return ({
            error: nextProps.jobError && nextProps.jobError.spec && {...nextProps.jobError.spec.train}
        })
    }

    changeType = (sub_type) => {
        let stepTypes = this.props.stepTypes;
        stepTypes["train"] = sub_type;
        this.props.setStepType(stepTypes);
        this.setState({
            sub_type,
            spec: mapSpecToState({...this.state.spec, sub_type: sub_type}),
        })
    }

    saveData = (data = {}) => {
        const spec = {...data};
        this.mapStateToSpec(spec,()=>{
        this.setState({
            spec
        })});
    }

    mapStateToSpec = (spec = this.state.spec, callback) => {
        if(spec.sub_type==="TRAIN_DISTRIBUTED") {
            this.train = {
                "docker": spec.docker,
                "env": spec.env,
                "env_source": spec.env_source,
                "name": spec.name,
                "outputs": spec.outputs,
                "data_source": spec.data_source,
                "resources": spec.resources,
                "source": spec.source,
                "sub_type": spec.sub_type,
                "type": "TRAIN",
            }
        } else {
            this.train = {
                "env": spec.env,
                "env_source": spec.env_source,
                "name": spec.name,
                "outputs": spec.outputs,
                "data_source": spec.data_source,
                "resources": spec.resources,
                "source": spec.source,
                "sub_type": spec.sub_type,
                "type": "TRAIN",
            }
        }

        let jobStore = {...this.props.jobStore}
        jobStore.spec.train = this.train;
        this.props.changeJobStore (
            jobStore
        )
        setTimeout(()=>{callback()},100);
    }

    render() {
        const { spec, error } = this.state;
        return(
            <div>
                <StepForm 
                    changeType={this.changeType} 
                    key={"deploy-form"} 
                    errorStore={error}
                    spec={spec} 
                    saveData={this.saveData} 
                    jobTypes={this.trainTypes}
                />
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        jobStore: state.jobStore.jobStore,
        jobError: state.jobStore.jobError,
        stepTypes: state.jobStore.stepTypes
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            changeJobStore,
            setStepType
        },
        dispatch
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(TrainStepForm);