import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import JobrunParameters from './JobrunParameters';
import { alertMsg } from '../../../ducks/alertsReducer';
import { runJobAction } from '../../../ducks/jobs';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';

function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const styles = theme => ({
  paper: {
    position: 'absolute',
    width: theme.spacing.unit * 75,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    outline: 'none',
  },
});


class JobrunPopupWithParameters extends React.Component {
  state = {
    open: false,
  };
  
  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  runJob = () => {
    this.props.alertMsg("Submitted!", "success");
    let finalData = this.props.jobId ? this.props.jobId : this.props.jobStore.id;
    const parameters = this.props.parameters ? this.props.parameters : this.props.jobStore.spec.parameters;
    this.props.runJobAction(this.props.token, finalData, parameters);
    setTimeout(()=>{
        this.props.history.push("/run-history",{jobId: finalData});
    },3000);
  }

  render() {
    const { classes } = this.props;

    return (
      <div className='d-inline'>
        {this.props.parameters && this.props.parameters.length > 0 ? 
        <button
            type="button"
            aria-label="toolbarMenuShow_run"
            className="toolbar-btn-primary clickable ml mt-sm mr m-0 mr-1 border-0 rounded pt-1 pb-1 pl-2 pr-2"
            onClick={() => {this.handleOpen()}}
        >
          <i className="fa fa-play"></i>
        </button>
        : 
        <button
            type="button"
            aria-label="toolbarMenuShow_run"
            className="toolbar-btn-primary clickable ml mt-sm mr"
            onClick={() => {this.handleOpen()}}
        >
          Run
        </button>
        }
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
        >
          <div style={getModalStyle()} className={classes.paper}>
            <JobrunParameters parameters={this.props.parameters}/>
            <div className='d-flex flex-row align-items-center justify-content-end mt-3'>
              <button
                  type="button"
                  className="btn btn-sm btn-secondary  fw-bolder mr-2"
                  onClick={this.handleClose}
              >
                  Cancel
              </button>
              <button
                  type="button"
                  aria-label="toolbarMenuShow_run"
                  className="workflow-run-button"
                  onClick={() => {
                      this.runJob();
                  }}
              >
                  Run
              </button>
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}

JobrunPopupWithParameters.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => {
  return {
    jobStore: state.jobStore.jobStore,
    token: state.auth.token,
    selectedProject: state.projects.globalProject
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
      {
          alertMsg,
          runJobAction,

      },
      dispatch
  );
};

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)((withStyles(styles)(JobrunPopupWithParameters))));