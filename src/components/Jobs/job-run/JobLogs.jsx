import React, { Component, createRef } from 'react';
import { getLogs } from '../../../api';
import {Loader1} from '../../home/modelOverviewLoader';

export class JobLogs extends Component {
    state = {
        logs: "",
        loading: false
    }
    _isMounted = true;
    logRef = createRef();
    xhr;

    componentDidMount() {
        this._isMounted = true;
        this.fetchLog();
    }

    componentWillUnmount() {
        this.closeXHR();
        this._isMounted = false
    }

    closeXHR = () => {
        if(this.xhr) {
            try {
                this.xhr.abort();
                this.xhr = null;
            } catch(e) {
                console.error(e);
            }
        }
    }

    dstreamHandler = async (url, callback) => {
        this.closeXHR();
        this.xhr = new XMLHttpRequest();
        this.xhr.open('GET', url, true);
        this.xhr.setRequestHeader("Authorization", `Bearer ${this.props.token}`);
        let seenChars = 0;
        const p = new Promise((resolve, reject) => {
            this.xhr.onreadystatechange = () => {
                if (this.xhr.readyState === this.xhr.LOADING && this.xhr.status === 200) {
                    callback(this.state.logs + this.xhr.response.substr(seenChars));
                    seenChars = this.xhr.responseText.length;
                } else if (this.xhr.readyState === this.xhr.DONE) {
                    if (this.xhr.status === 200) {
                        resolve(this.xhr.response);
                    } else {
                        reject(`XHR Failed with status ${this.xhr.status}: ${this.xhr.statusText}`);
                    }
                }
            };
        });
        this.xhr.send();
        return p;
    }

    fetchLog = () => {
        if(!this._isMounted || this.state.loading) return;
        this.setState({
            logs: '',
            loading: true
        });
        const {type, index, spec}  = this.props;
        let displayName = index >= 0 ? type+'-'+index+'-'+spec[index].name : type+'-'+spec.name;
        displayName = displayName.replace(/_/g, "-")
        this.dstreamHandler(`${process.env.REACT_APP_API_URL}/api/mlworkflow-runs/${this.props.name}/step/${displayName}/streamlogs`, 
        (data)=>{
            this.setState({
                logs: data
            },()=>this.logRef.current.scrollTo(0, this.logRef.current.scrollHeight));
        })
        .then((data)=>{
            this.setState({
                logs: data,
                loading: false
            },()=>this.logRef.current.scrollTo(0, this.logRef.current.scrollHeight));
        })
        .catch((err)=>{
            const { logs } = this.state;
            this.setState({
                logs: logs ? logs : "No logs found",
                loading: false,
                error: err
            });
            console.error(err);
        })
    }
    
    render() {
        const { expanded } = this.props;
        return(
            <div className="mlr-30 relative">
                <span className="clickable copy-btn" onClick={()=>{this.fetchLog()}}>
                    <i className={!this.state.loading ? "fa fa-play mt mr" : "fa fa-sync mt mr rotate"}></i>
                </span>
                <pre 
                    style={expanded ? {maxHeight: "70vh", minHeight: "70vh"} : {maxHeight: "34vh", minHeight: "34vh"} }
                    ref={this.logRef}
                >
                    {this.state.logs ? this.state.logs : <Loader1 />}
                </pre>
            </div>
        )
    }
}