import React, { Component } from 'react';
import moment from 'moment';
import { connect } from "react-redux";
import {getJobCpuData, getJobMemoryData} from "../../../api/jobs";
import CpuChart from "../../chart/CpuChart";
import MemoryChart from "../../chart/MemoryChart";

 class JobResource extends Component {
    state = { 
      cpuData:[],
      memoryData:[]
     }

    componentDidMount=()=>{
      this.getCpuData();
      this.getMemoryData();
      if(!this.props.jobStore.status.finished_at){  
        setInterval(()=>{
          this.getCpuData();
          this.getMemoryData();
        }, 30000);
        
      }
    }


    getCpuData=()=>{
      let currentDate = new Date()
      let step = '';
      const startTime = this.props.jobStore.status.started_at;
      let endTime = this.props.jobStore.status.finished_at;
      if(!endTime){
        endTime = currentDate.toISOString()
      }
      const minutes = (new Date(endTime) - new Date(startTime)) / 1000 / 60;
      if(minutes <= 15){
        step = '30s'
      }else if(minutes > 15 && minutes <= 60){
        step = '1m'
      }else{
        step = '2m'
      }
       getJobCpuData(this.props.token,this.props.jobStore.metadata.name,startTime,endTime,step).then(
        (res)=> {
         const data = res.data.data.result[0].values;
        this.setState({cpuData:data})
        }  
      ).catch((err)=>console.log("err::",err))
    }

    getMemoryData=()=>{
      let currentDate = new Date()
      let step = '';
      const startTime = this.props.jobStore.status.started_at;
      let endTime = this.props.jobStore.status.finished_at;
      if(!endTime){
        endTime = currentDate.toISOString()
      }
      const minutes = (new Date(endTime) - new Date(startTime)) / 1000 / 60;
      if(minutes <= 15){
        step = '30s'
      }else if(minutes > 15 && minutes <= 60){
        step = '1m'
      }else{
        step = '2m'
      }
      getJobMemoryData(this.props.token,this.props.jobStore.metadata.name,startTime,endTime,step).then(
        (res)=> {
         const data = res.data.data.result[0].values;
        this.setState({memoryData:data})
        }  
      ).catch((err)=>console.log("err::",err))
    }



    render() { 
        return ( 
            <div>
            <div className="m-info-table mlr-30" style={{ minHeight: 452 }}>
              <div className="mt-lg pd">
                <div className="inline-block" style={{ width: "49%" }}>
                  <h4>CPU Usage</h4>
                  <CpuChart cpuData={this.state.cpuData} />
                </div>
                <div className="inline-block" style={{ width: "49%" }}>
                  <h4>Memory Usage</h4>
                  <MemoryChart memoryData={this.state.memoryData} />
                </div>
              </div>
            </div>
            </div>
         );
    }
}

const mapStateToProps = state => {
      return {
        token: state.auth.token,
      }
}

 
export default connect(mapStateToProps)(JobResource);
