import React, { Component } from 'react';
import moment from "moment";

export class JobStatus extends Component {
  intervalSet = {};
  state = {
    status: null,
    duration: ""
  }

  componentDidMount() {
    this.parseStatus();
  }

  componentWillUnmount() {
    clearInterval(this.intervalSet);
  }

  findDuration(startTime, endTime=null) {
    if (!startTime)
        return;

    let duration = "";

    let st = new Date(startTime),
        et = new Date(endTime),
        diff = ((et.getTime() - st.getTime()) / 1000),
        mins = 0,
        hour = 0,
        days = 0;

    while (diff >= 60) {
        diff -= 60;
        ++mins;
    }
    while (mins >= 60) {
        mins -= 60;
        ++hour;
    }
    while (hour >= 24) {
        hour -= 24;
        ++days;
    }

    hour = hour < 10 ? '0' + Math.round(hour) : Math.round(hour);
    mins = mins < 10 ? '0' + Math.round(mins) : Math.round(mins);
    diff = diff < 10 ? '0' + Math.round(diff) : Math.round(diff);
    if (days) {
        duration = `${days} Day`
    } else if (parseInt(hour, 10)) {
        duration = `${hour}:${mins} Hr`
    } else if (parseInt(mins, 10)) {
        duration = `${mins}:${diff} Min`
    } else if (parseInt(diff, 10)) {
        duration = `${diff} Sec`
    }
    this.setState({
        duration
    })
  }

  parseStatus = () => {
    const { type, index, spec, jobStore } = this.props;
    const steps = (jobStore.status && jobStore.status.steps) || [];
    const displayName = index >= 0 ? type + '-' + index + '-' + spec[index].name : type + '-' + spec.name;
    const filteredSteps = steps && Object.keys(steps).filter((e, i) => {
      return steps[e].displayName === displayName.replace(/_/g, "-");
    })
    const status = steps && steps[filteredSteps[0]];
    this.setState({
      status
    })
    clearInterval(this.intervalSet);
    if(status && status.finished_at) {
        this.findDuration(status.started_at, status.finished_at);
    } else if(status && !['FAILED','ERRORED','SUCCEEDED','TERMINATED'].includes(status.phase)) {
        this.intervalSet = setInterval(()=>{
        if(status && status.started_at)
            this.findDuration(status.started_at, new Date());
        }, 1000);
    } else if(status && status.started_at && jobStore.status && jobStore.status.finished_at){
      this.findDuration(status.started_at, jobStore.status.finished_at);
    }
  }

  getColor(type) {
    const status = {
      'CREATED': "info",
      'STARTED': "info",
      'RUNNING': "info",
      'FAILED': "failed",
      'ERRORED': "failed",
      'SUCCEEDED': "success",
      'TERMINATED': "primary"
    }
    const color = status[type] ? status[type] : 'primary';
    return color;
  }

  getMsg = (id) => {
    const { jobStore } = this.props;
    const steps = (jobStore.status && jobStore.status.steps) || [];
    const msg = steps && steps[id].message;
    if((/child .*? failed/gim).test(msg)) {
      id = msg.replace(/child \'(.*?)\' failed/g,(a,b)=>{return b});
      return this.getMsg(id);
    } else if(!msg && steps && steps[id] && steps[id].children) {
      return this.getMsg(steps[id].children[0]);
    }
    if(steps[id].phase === "SUCCEEDED" || steps[id].phase === "RUNNING") {
      return "";
    }
    return msg;
  }

  render() {
    const { status, duration } = this.state;
    const { type, index, spec } = this.props;
    return (
      <div className="m-info-table model-details">
        {status &&
          <table className="table data-table no-border modeldetails-table">
            <thead>
              <tr>
                <td>Name</td>
                <td>Type</td>
                <td>Status</td>
                <td>Started At</td>
                <td>Finished At</td>
                <td>Duration</td>
                {status.phase !== "SUCCEEDED" &&
                  <td>Message</td>}
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{index === 0 || index >= 1 ? spec[index].name : spec.name}</td>
                <td>{type}</td>
                <td>
                  <label
                    className={`model-item-status status-${this.getColor(status.phase)}`}
                  >
                    {status.phase}
                  </label>
                </td>
                <td>{status.started_at && moment(status.started_at).format("lll")}</td>
                <td>
                  {status.finished_at && moment(status.finished_at).format("lll")}
                </td>
                <td>
                  {duration}
                </td>
                {status.phase !== "SUCCEEDED" &&
                  <td>{this.getMsg(status.id)}</td>}
              </tr>
            </tbody>
          </table>}
      </div>
    )
  }
}