import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { changeJobStore, setStepType } from "../../ducks/jobStore";

import StepForm from "./StepForm";

const mapSpecToState = (data) => {
    return ({
        name: data.name || null,
        type: "TRAIN" || null,
        sub_type: data.sub_type || null,
        source: data.source || {},
        data_source: data.data_source || [],
        outputs: data.outputs || [],
        env: data.env || [],
        env_source: data.env_source || [],
        resources: data.resources || {},       
    })
}

class PreTrainStepForm extends Component {

    preTrainTypes = [
        "SIMPLE",
        // "ONE_HOT_ENCODING",
        // "IMAGE_TO_TFRECORD",
        "SPARK",
        // "DASK",
        // "PANDAS",
    ]

    state = {
        sub_type: this.props.stepTypes["pre_train_steps"][this.props.index] || this.props.spec[this.props.index].sub_type,
        spec: mapSpecToState({...this.props.spec[this.props.index], sub_type: this.props.stepTypes["pre_train_steps"][this.props.index] || this.props.spec[this.props.index].sub_type}),
        error: this.props.jobError && this.props.jobError.spec && this.props.jobError.spec.pre_train_steps && {...this.props.jobError.spec.pre_train_steps[this.props.index]}
    }

    static getDerivedStateFromProps(nextProps) {
        return ({
            error: nextProps.jobError && nextProps.jobError.spec && nextProps.jobError.spec.pre_train_steps && {...nextProps.jobError.spec.pre_train_steps[nextProps.index]}
        })
    }

    saveData = (data = {}) => {
        const spec = {...data};
        this.mapStateToSpec(spec,()=>{
        this.setState({
            spec
        })});
    }

    changeType = (sub_type) => {
        let stepTypes = this.props.stepTypes;
        stepTypes["pre_train_steps"][this.props.index] = sub_type;
        this.props.setStepType(stepTypes);
        this.setState({
            sub_type,
            spec: mapSpecToState({...this.state.spec, sub_type: sub_type}),
        })
    }

    mapStateToSpec = (spec = this.state.spec, callback) => {
        this.pre_train = {
            // "deploy_condition": spec.deploy_condition,
            // "docker": spec.docker,
            "env": spec.env,
            "env_source": spec.env_source,
            "name": spec.name,
            // "output": spec.output,
            // "replicas": spec.replicas,
            "outputs": spec.outputs,
            "data_source": spec.data_source,
            "resources": spec.resources,
            "source": spec.source,
            "sub_type": spec.sub_type,
            "type": "TRAIN"
        }

        let jobStore = {...this.props.jobStore}
        if(!jobStore.spec.pre_train_steps) {
            jobStore.spec.pre_train_steps = []
        }
        jobStore.spec.pre_train_steps[this.props.index] = this.pre_train;
        this.props.changeJobStore (
            jobStore
        )
        setTimeout(()=>{callback()},100);
    }

    render() {
        const {spec, error} = this.state;
        return(
            <div>
                <StepForm formType={"PRE"} errorStore={error} changeType={this.changeType} key={"train-form"} spec={spec} saveData={this.saveData} jobTypes={this.preTrainTypes} />
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        jobStore: state.jobStore.jobStore,
        jobError: state.jobStore.jobError,
        stepTypes: state.jobStore.stepTypes
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            changeJobStore,
            setStepType
        },
        dispatch
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(PreTrainStepForm);