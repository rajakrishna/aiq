import React, { Component } from "react";
import { TextField } from "@material-ui/core";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import { capitalizeVars, runtime as runtimeArr, l, sparkRuntime, sparkVersion } from "../../../utils/helper";
import { Config, sparkImages } from '../../Notebook.config';
import Git from './Git';
import Experiment from './Experiment';
import RegisteredModel from './RegisteredModel';
import ModelArtifact from './ModelArtifact';
import ControlledExpansionPanels from '../../Collapse';

export class Source extends Component {
    state = {
        type: this.props.source.type || "",
        command: this.props.source.command || "",
        requirements: this.props.source.requirements || "",
        image: this.props.source.image || "gcr.io/aiops-224805/tensorflow-1.10.1-notebook-cpu:latest",
        image_type: Config.spawnerFormDefaults.image.options.includes(this.props.source.image || "gcr.io/aiops-224805/tensorflow-1.10.1-notebook-cpu:latest") ? 'true' : 'false',
        model_artifact_path: this.props.source.model_artifact_path || "",
        runtime: this.props.source.runtime || "",
        git: this.props.source.git,
        experiment: this.props.source.experiment,
        registered_model: this.props.source.registered_model,
        model_id: this.props.model_id,
        model_artifact: this.props.source.model_artifact,
        spark_runtime: this.props.source.spark_runtime,
        spark_version: this.props.source.spark_version,
    }
    
    handleSource = (event, index) => {
        if(this.props.readOnly) return;
        const target = event.target;
        const name = target.name;
        let value = (/NOT_SET/g).test(target.value) ? "" : target.value;
        if(name === "requirements") {
            value = value.split("\n").join(",");
        }
        if(name === "type") {
            this.setState({
                git: "",
                experiment: "",
                registered_model: "",
                type: value
            })
        } else {
            this.setState({
                [name]: value
            });
        }
    }

    handleChildren = (name, value) => {
        if(this.props.readOnly) return;
        this.setState({
            [name]: value
        },()=>{this.handleBlur()})
    }

    handleBlur = () => {
        if(this.props.readOnly) return;
        const state = {...this.state};
        delete state.image_type;
        if(this.props.steptype === 'SPARK') {
            state.spark_runtime = "SPARK_PYTHON3";
            state.spark_version = "SPARK_2_4";
        }
        this.props.addToSpec(this.props.fieldName, state);
    }

    render() {
        const { type, command, image, model_artifact_path,requirements, runtime, git, experiment, registered_model, model_artifact, image_type, spark_runtime, spark_version } = this.state;
        const { fieldName, errorStore, formType, steptype, model_id, readOnly } = this.props;
        let collapse = Object.keys(errorStore).length > 0;
        const selectable_images = steptype === 'SPARK' ? sparkImages : Config.spawnerFormDefaults.image.options;
        return (
            <ControlledExpansionPanels active={false} errorExpand={collapse} heading={capitalizeVars(fieldName)}>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-12">
                            <TextField
                                id={"type"}
                                label={"Type"}
                                select
                                disabled={readOnly}
                                name={"type"}
                                value={type || "SOURCE_TYPE_NOT_SET"}
                                onChange={this.handleSource}
                                onBlur={this.handleBlur}
                                error={!!errorStore.type}
                                helperText={errorStore.type || ""}
                                margin="normal"
                                fullWidth
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            >
                                <MenuItem value="SOURCE_TYPE_NOT_SET">Select An Option</MenuItem>
                                <MenuItem value="GIT">Git</MenuItem>
                                {(/DEPLOY|ARTIFACT/gi).test(steptype) && 
                                <MenuItem value="EXPERIMENT">Experiment</MenuItem>}
                                {(/DEPLOY|ARTIFACT/gi).test(steptype) && 
                                <MenuItem value="MODEL_ARTIFACT">Model Artifact</MenuItem>}
                                {(/DEPLOY|ARTIFACT/gi).test(steptype) && 
                                <MenuItem value="REGISTERED_MODEL">Model</MenuItem>}
                            </TextField>
                            {
                                type === 'GIT' && <Git git={git} errorStore={errorStore.git || {}} readOnly={this.props.readOnly} handleChildren={this.handleChildren} />
                            }
                            {
                                type === 'EXPERIMENT' &&  <Experiment requirements={requirements} errorStore={errorStore.experiment || {}} readOnly={this.props.readOnly} experiment={experiment} handleChildren={this.handleChildren} />
                            }
                            {
                                type === 'REGISTERED_MODEL' &&  <RegisteredModel requirements={requirements} model_id={model_id} errorStore={errorStore.registered_model || {}} readOnly={this.props.readOnly} registered_model={registered_model} handleChildren={this.handleChildren} />
                            }
                            {
                                type === 'MODEL_ARTIFACT' &&  <ModelArtifact requirements={requirements} errorStore={errorStore.model_artifact || {}} readOnly={this.props.readOnly} model_artifact={model_artifact || {}} handleChildren={this.handleChildren} />
                            }
                            {!(/DEPLOY|ARTIFACT/gi).test(steptype) && type === "GIT" &&
                            <TextField
                                id={"command"}
                                label={"Command"}
                                name={"command"}
                                value={command}
                                disabled={readOnly}
                                error={!!errorStore.command}
                                helperText={errorStore.command || ""}
                                onChange={this.handleSource}
                                onBlur={this.handleBlur}
                                margin="normal"
                                fullWidth
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />}
                            {!(/DEPLOY|ARTIFACT|TRAIN_DISTRIBUTED/gi).test(steptype) &&
                            <div className="mt-lg">
                                <FormControl component="fieldset">
                                    <RadioGroup 
                                        aria-label={"image_type"} 
                                        name={"image_type"} 
                                        value={image_type} 
                                        onChange={this.handleSource}
                                        row
                                    >
                                        <FormControlLabel
                                            value={"true"}
                                            control={<Radio color="primary" />}
                                            label="Standard Image"
                                            labelPlacement="start"
                                        />
                                        <FormControlLabel
                                            value={"false"}
                                            control={<Radio color="primary" />}
                                            label="Custom Image"
                                            labelPlacement="start"
                                        />
                                    </RadioGroup>
                                </FormControl>
                            </div>}
                            {!(/DEPLOY|ARTIFACT|TRAIN_DISTRIBUTED/gi).test(steptype) &&
                            <TextField
                                id={"image"}
                                label={"Image"}
                                select={image_type === 'true'}
                                name={"image"}
                                disabled={readOnly}
                                value={image || (image_type === 'true' ? "IMAGE_TYPE_NOT_SET" : "")}
                                onChange={this.handleSource}
                                onBlur={this.handleBlur}
                                margin="normal"
                                error={!!errorStore.image}
                                helperText={errorStore.image || ""}
                                fullWidth
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            >
                                <MenuItem value="IMAGE_TYPE_NOT_SET">Select An Option</MenuItem>
                                {selectable_images.map((e,i)=>{
                                    return(
                                        <MenuItem key={"image"+i} value={e}>{e}</MenuItem>
                                    )
                                })}
                            </TextField>}
                            {steptype === "SPARK" && image_type === "true" ?
                            <TextField
                                id={"requirements"}
                                label={"Requirements"}
                                name={"requirements"}
                                value={requirements && requirements.split(",").join("\n")}
                                disabled={readOnly}
                                multiline={true}
                                rows={4}
                                rowsMax={4}
                                onChange={this.handleSource}
                                onBlur={this.handleBlur}
                                margin="normal"
                                error={!!errorStore.requirements}
                                helperText={errorStore.requirements || ""}
                                fullWidth
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />:""}
                            {false &&
                            <TextField
                                id={"model_artifact_path"}
                                label={"Model Artifact Path"}
                                name={"model_artifact_path"}
                                value={model_artifact_path}
                                disabled={readOnly}
                                onChange={this.handleSource}
                                onBlur={this.handleBlur}
                                margin="normal"
                                error={!!errorStore.model_artifact_path}
                                helperText={errorStore.model_artifact_path || ""}
                                fullWidth
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            >
                            </TextField>}
                            {(/DEPLOY|ARTIFACT|TRAIN_DISTRIBUTED/gi).test(steptype) &&
                            <TextField
                                id={"runtime"}
                                label={"Runtime"}
                                select
                                name={"runtime"}
                                disabled={readOnly || type === 'REGISTERED_MODEL'}
                                value={runtime || "RUNTIME_NOT_SET"}
                                onChange={(e)=>{
                                    const value = e.target.value;
                                    if ((/NOT_SET/).test(value)) return;
                                    this.handleSource(e);
                                }}
                                onBlur={this.handleBlur}
                                margin="normal"
                                fullWidth
                                error={!!errorStore.runtime}
                                helperText={errorStore.runtime || ""}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            >
                                <MenuItem value="RUNTIME_NOT_SET">Select An Option</MenuItem>
                                {
                                    runtimeArr.map(e => <MenuItem value={e} key={e}>{l[e]}</MenuItem>)
                                }
                            </TextField>}
                            {steptype === "SPARK" &&
                            <TextField
                                id={"spark_runtime"}
                                label={"Runtime"}
                                select
                                name={"spark_runtime"}
                                disabled={readOnly}
                                value={spark_runtime || "RUNTIME_NOT_SET"}
                                onChange={(e)=>{
                                    const value = e.target.value;
                                    if ((/NOT_SET/).test(value)) return;
                                    this.handleSource(e);
                                }}
                                onBlur={this.handleBlur}
                                margin="normal"
                                fullWidth
                                error={!!errorStore.spark_runtime}
                                helperText={errorStore.spark_runtime || ""}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            >
                                <MenuItem value="RUNTIME_NOT_SET">Select An Option</MenuItem>
                                {
                                    sparkRuntime.map(e => <MenuItem value={e} key={e}>{l[e]}</MenuItem>)
                                }
                            </TextField>}
                            {steptype === "SPARK" &&
                            <TextField
                                id={"spark_version"}
                                label={"Version"}
                                select
                                name={"spark_version"}
                                disabled={readOnly}
                                value={spark_version || "VERSION_NOT_SET"}
                                onChange={(e)=>{
                                    const value = e.target.value;
                                    if ((/NOT_SET/).test(value)) return;
                                    this.handleSource(e);
                                }}
                                onBlur={this.handleBlur}
                                margin="normal"
                                fullWidth
                                error={!!errorStore.spark_version}
                                helperText={errorStore.spark_version || ""}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            >
                                <MenuItem value="VERSION_NOT_SET">Select An Option</MenuItem>
                                {
                                    sparkVersion.map(e => <MenuItem value={e} key={e}>{l[e]}</MenuItem>)
                                }
                            </TextField>}
                        </div>
                    </div>
                </div>
            </ControlledExpansionPanels>
        )
    }
}