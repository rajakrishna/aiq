import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { fetchSecrets } from "../../../ducks/jobStore";
import { TextField, MenuItem } from "@material-ui/core";
import MicroModal from 'micromodal';

class Git extends Component {
    state = {
        directory: (this.props.git && this.props.git.directory) || "",
        repository: (this.props.git && this.props.git.repository) || "",
        revision: (this.props.git && this.props.git.revision) || "",
        secret: (this.props.git && this.props.git.secret) || ""
    }

    componentDidMount() {
        if(this.props.project_id) {
            this.props.fetchSecrets({project_id: this.props.project_id});
        }
    }

    handleGit = (event) => {
        if(this.props.readOnly) return;
        const target = event.target;
        const name = target.name;
        const value = target.value;

        if(name === "secret") {
            this.setState({
                secret: {
                    name: value
                }
            })
            return;
        }

        this.setState({
            [name]: value
        });
    }

    handleBlur = () => {
        if(this.props.readOnly) return;
        this.props.handleChildren("git", {...this.state});
    }
    
    render() {
        const { directory, repository, revision, secret } = this.state;
        const { secretsList, project_id, status, errorStore, readOnly } = this.props;
        return (
            <div>
                <TextField
                    id={"repository"}
                    label={"Git Url"}
                    name={"repository"}
                    value={repository}
                    disabled={readOnly}
                    onChange={this.handleGit}
                    onBlur={this.handleBlur}
                    margin="normal"
                    error={!!errorStore.repository}
                    helperText={errorStore.repository || ""}
                    fullWidth
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
                <TextField
                    id={"revision"}
                    label={"Revision"}
                    name={"revision"}
                    value={revision}
                    disabled={readOnly}
                    onChange={this.handleGit}
                    onBlur={this.handleBlur}
                    margin="normal"
                    error={!!errorStore.revision}
                    helperText={errorStore.revision || ""}
                    fullWidth
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
                <TextField
                    id={"directory"}
                    label={"Directory"}
                    name={"directory"}
                    value={directory}
                    disabled={readOnly}
                    onChange={this.handleGit}
                    onBlur={this.handleBlur}
                    margin="normal"
                    error={!!errorStore.directory}
                    helperText={errorStore.directory || ""}
                    fullWidth
                    InputLabelProps={{
                        shrink: true,
                        "aria-readonly": true
                    }}
                />
                <div className="row">
                <div className="col-sm-11">   
                    <TextField 
                        label="Secret"
                        name={"secret"}
                        select
                        disabled={readOnly}
                        value={(secret && secret.name) || "SECRET_NOT_SET"}
                        margin="normal"
                        fullWidth
                        onChange={this.handleGit}
                        onBlur={this.handleBlur}
                        error={!!errorStore.secret}
                        helperText={errorStore.secret || ""}
                        InputLabelProps={{
                            shrink: true,
                        }}
                    >
                        <MenuItem value={"SECRET_NOT_SET"}>Select Secret</MenuItem>
                        {secretsList.map(secrets => (
                            <MenuItem key={secrets.name} value={secrets.name} >
                                {secrets.name}
                            </MenuItem>
                        ))}
                    </TextField>
                    {/* {project_id ?
                        <Link className="mt-sm" to={`/projects/${project_id}/secrets`} target="_blank">
                            Add Secrets
                        </Link>: ""
                    } */}
                    {project_id &&
                        <span
                            className="text-link"
                            onClick={()=>MicroModal.show("secret-modal")}
                        >
                            Add Secrets
                        </span>
                    }
                </div>
                <div className="col-sm-1" style={{marginTop: 34}}>
                    <button type="button" className="btn btn-sm btn-default" onClick={()=>{
                        if(!project_id) return;
                        this.props.fetchSecrets({project_id: project_id});
                    }}>
                        <i className={status === 'loading' ? "fa fa-sync rotate" : "fa fa-sync"} />
                    </button>
                </div>
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => {
    return {
        project_id: state.jobStore.jobStore.spec.project_id,
        secretsList: state.jobStore.secretsList,
        status: state.jobStore.status
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            fetchSecrets
        },
        dispatch
    );
};


export default connect(mapStateToProps, mapDispatchToProps)(Git);