import React, {Component} from "react";
import { Link } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { fetchSecrets } from "../../../ducks/jobStore";
import { TextField, MenuItem } from "@material-ui/core";
import { l } from "../../../utils/helper";
import ControlledExpansionPanels from '../../Collapse';
import MicroModal from "micromodal";

class Output extends Component {
    state = {
        outputs: this.props.outputs
    }

    handleOutputs = (event, index) => {
        if(this.props.readOnly) return;
        const target = event.target;
        const name = (target.name).replace(/\d/g,"");
        const value = target.value;
        let outputs = [...this.state.outputs];
        outputs[index][name] = value;
        this.setState({outputs});
    }

    handleSubOutputs = (event, index,type) => {
        if(this.props.readOnly) return;
        const target = event.target;
        const name = (target.name).replace(/\d/g,"");
        const value = target.value;
        let outputs = [...this.state.outputs];
        outputs[index][type][name] = value;
        this.setState({outputs});
    }

    addOutputs = () => {
        if(this.props.readOnly) return;
        const outputs = [...this.state.outputs];
        outputs.push({
            type: "",
            s3: {
                path: "",
                bucket: "",
                key: "",
                aws_secret: "",
            }
        });
        this.setState({
            outputs
        });
    }

    delOutputs = (i,outputs="outputs") => {
        if(this.props.readOnly) return;
        let outputss = [...this.state[outputs]];
        outputss.splice(i,1);
        this.setState({[outputs]: outputss});
        this.props.addToSpec(this.props.fieldName, [...outputss])
    }

    render() {
        const {outputs} = this.state;
        const { secretsList, project_id, status, fieldName, readOnly, errorStore } = this.props;
        return (
            <ControlledExpansionPanels active={false} heading={l[fieldName]}>
                <div className="container-fluid">
                    <div className="row mb">
                        <div className="col-sm-10"> 
                            <span className="text-gray">Click the + icon to add {l[fieldName]}</span>
                        </div>
                        <div className="col-sm-2">
                            <button className="btn btn-sm btn-primary" onClick={() => this.addOutputs()}>
                                <i className="fa fa-plus-circle" />
                            </button>
                        </div>
                    </div>
                    {outputs.map((e,i)=>{
                        return(
                        <div className="row">
                            <div className="col-sm-10">
                                <TextField 
                                    label="Type"
                                    name={"type"+i}
                                    select
                                    value={e.type || "TYPE_NOT_SET"}
                                    margin="normal"
                                    disabled={readOnly}
                                    fullWidth
                                    error={errorStore[i] && !!errorStore[i].type}
                                    onChange={(event) => {this.handleOutputs(event, i)}}
                                    onBlur={()=>{this.props.addToSpec(fieldName, [...outputs])}}
                                    helperText={errorStore[i] && errorStore[i].type}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                >
                                    <MenuItem value={"TYPE_NOT_SET"}>Select {l[fieldName]} type</MenuItem>
                                    <MenuItem value={"S3"}>S3</MenuItem>
                                </TextField>
                            </div>
                            <div className="col-sm-2" style={{marginTop: 34}}>
                                <button className="btn btn-sm btn-default" onClick={()=>{this.delOutputs(i)}}>
                                    <i className="text-danger fa fa-trash"></i>
                                </button>
                            </div>
                            {e.type === "S3" &&
                            <div className="container-fluid">
                                <div className="row">
                                    <div className="col-sm-5">   
                                        <TextField 
                                            label="Bucket" 
                                            fullWidth 
                                            value={e[(e.type).toLowerCase()]["bucket"]}
                                            error={errorStore[i] && !!errorStore[i][(e.type).toLowerCase()]["bucket"]}
                                            disabled={readOnly} 
                                            name={"bucket"+i} 
                                            onChange={(event)=>{this.handleSubOutputs(event,i,"s3")}} 
                                            onBlur={()=>{this.props.addToSpec(fieldName, [...outputs])}}
                                            helperText={errorStore[i] && errorStore[i][(e.type).toLowerCase()]["bucket"]}
                                        />
                                    </div>
                                    <div className="col-sm-5">   
                                        <TextField 
                                            label="Key" 
                                            fullWidth 
                                            value={e[(e.type).toLowerCase()]["key"]}
                                            error={errorStore[i] && !!errorStore[i][(e.type).toLowerCase()]["key"]}
                                            disabled={readOnly} 
                                            name={"key"+i} 
                                            onChange={(event)=>{this.handleSubOutputs(event,i,"s3")}} 
                                            onBlur={()=>{this.props.addToSpec(fieldName, [...outputs])}}
                                            helperText={errorStore[i] && errorStore[i][(e.type).toLowerCase()]["key"]}
                                        />
                                    </div>
                                    <div className="col-sm-10">   
                                        <TextField 
                                            label="Path" 
                                            fullWidth 
                                            value={e[(e.type).toLowerCase()]["path"]} 
                                            error={errorStore[i] && !!errorStore[i][(e.type).toLowerCase()]["path"]}
                                            disabled={readOnly}
                                            name={"path"+i} 
                                            onChange={(event)=>{this.handleSubOutputs(event,i,"s3")}} 
                                            onBlur={()=>{this.props.addToSpec(fieldName, [...outputs])}}
                                            helperText={(errorStore[i] && errorStore[i][(e.type).toLowerCase()]["path"]) || "S3 directory or file will be available at this path."}
                                        />
                                    </div>
                                    <div className="col-sm-10">   
                                        <TextField 
                                            label="Secret"
                                            name={"aws_secret"+i}
                                            select
                                            value={e[(e.type).toLowerCase()]["aws_secret"] || "SECRET_NOT_SET"}
                                            disabled={readOnly}
                                            margin="normal"
                                            fullWidth
                                            onChange={(event) => {this.handleSubOutputs(event, i, "s3")}}
                                            onBlur={()=>{this.props.addToSpec(fieldName, [...outputs])}}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                        >
                                            <MenuItem value={"SECRET_NOT_SET"}>Select Secret</MenuItem>
                                            {secretsList.map(secret => (
                                                <MenuItem key={secret.name} value={secret.name} >
                                                    {secret.name}
                                                </MenuItem>
                                            ))}
                                        </TextField>
                                        {/* {project_id ?
                                            <Link className="mt-sm" to={`/projects/${project_id}/secrets`} target="_blank">
                                                Add Secrets
                                            </Link>: ""
                                        } */}
                                        {project_id &&
                                            <span
                                                className="text-link"
                                                onClick={()=>MicroModal.show("secret-modal")}
                                            >
                                                Add Secrets
                                            </span>
                                        }
                                    </div>
                                    <div className="col-sm-2" style={{marginTop: 34}}>
                                        <button type="button" className="btn btn-sm btn-default" onClick={()=>{
                                            if(!project_id) return;
                                            this.props.fetchSecrets({project_id: project_id});
                                        }}>
                                            <i className={status === 'loading' ? "fa fa-sync rotate" : "fa fa-sync"} />
                                        </button>
                                    </div>
                                </div>
                            </div>
                            }
                            <div className="col-sm-12">
                                <hr />
                            </div>
                        </div>
                        )
                    })}
                </div>
            </ControlledExpansionPanels>
        )
    }
}

const mapStateToProps = state => {
    return {
        project_id: state.jobStore.jobStore.spec.project_id,
        secretsList: state.jobStore.secretsList,
        status: state.jobStore.status
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            fetchSecrets
        },
        dispatch
    );
};


export default connect(mapStateToProps, mapDispatchToProps)(Output);