export const validate = (jobStore, steps, saveError) => {
    const store = {...jobStore};
    let proto = {};
    proto = finalValidate(jobStore, "", store);
    proto["spec"] = finalValidate(jobStore["spec"], "");

    Object.keys(store.spec).map(e=>{
        if(steps.includes(e)) {
            if((/pre|post/gi).test(e) && store['spec'][e]) {
                proto['spec'][e] = proto['spec'][e] || [];
                store['spec'][e].map((elem, index) => {
                    proto['spec'][e][index] = finalValidate(store['spec'][e][index], "");
                });
            } else {
                proto['spec'][e] = finalValidate(store['spec'][e], "");
            }
        }
    })
    delKeys(proto);
    saveError({...proto});
    if(Object.keys(proto).length) {
        console.error("proto",proto);
        return false;
    } else {
        return true;
    }
}

const finalValidate = (spec = {}, specType) => {
    let proto = {...spec};
    const duplicateSpec = {...spec};

    Object.keys(duplicateSpec).map(e => {
        switch(e) {
            case "deploy_condition": 
                duplicateSpec[e] = validateDeployCondition(spec[e]);
            break;
            case "docker":
            case "docker_a":
            case "docker_b":
            case "model_docker":
            case "transformer_docker":
                duplicateSpec[e] = validateDocker(spec[e]);
            break;
            case "env":
            case "env_a":
            case "env_b":
            case "model_env":
            case "transformer_env":
            case "global_env_variables":
                duplicateSpec[e] = validateEnv(spec[e]);
            break;
            case "outputs":
            case "data_source":
                duplicateSpec[e] = validateOutput(spec[e]);
            break;
            case "resources":
                duplicateSpec[e] = validateResources(spec[e]);
            break;
            case "source":
            // case "model_source":
            case "transformer_source":
            case "source_a":
            case "source_b":
                console.log("error",validateSource(spec[e]));
                duplicateSpec[e] = validateSource(spec[e]);
            break;
            case "git_secrets":
            case "docker_push_secrets":
            case "image_pull_secrets":
                duplicateSpec[e] = validateSecrets(spec[e]);
            break;
            case "notifications":
                duplicateSpec[e] = validateNotifications(spec[e]);
            break;
            case "schedule":
                duplicateSpec[e] = validateSchedule(spec[e]);
            break;
            case "name":
            case "model_name":
                duplicateSpec[e] = validateName(spec[e]);
            break;
            case "project_id":
            case "project_name":
            case "sub_type":
                duplicateSpec[e] = validateText(spec[e]);
            break;
            default:
                duplicateSpec[e] = null;
        }
        if(!specType) {
            proto = duplicateSpec;
        } else {
            proto.spec[specType] = duplicateSpec;
        }
    })
    return proto;
}

function isEmpty(obj) {
    for(var key in obj) 
        return false;

    return true
}
  
function delKeys(app){
    for(var key in app){
        if(app[key] !== null && typeof(app[key]) === 'object' && app[key] !== ""){
            delKeys(app[key])
    
            if(isEmpty(app[key])) {
            delete app[key]
            }
        } 
        if(app[key] === null || !app[key]) {
            delete app[key]
        }
    }
}

function validateDocker(spec={}) {
    if(!spec) return null;
    return ({
        name: null,
        repository : null,//spec.repository ? null : "This field is required!",
        tag: null
    })
}

function validateEnv(spec={}) {
    return(null);

}

function validateOutput(spec={}) {
    if(!Array.isArray(spec)) return;
    return spec.map(e => {
        return(
            {
                "type": null,
                "s3": {
                    "path": e.type==="S3" && e.s3 && !(/^[\w\/\.\-\@]+$/gi).test(e.s3.path) ? "Please enter a valid path" : null,
                    "bucket": e.type==="S3" && e.s3 && !e.s3.bucket ? "This field is required" : null,
                    "key": e.type==="S3" && e.s3 && !e.s3.key ? "This field is required" : null,
                    "aws_secret": null
                }
            }
        )
    })
}

function validateResources(spec={}) {
    return(null)    
}

function validateSource(spec={}) {
    console.log("spec",spec)
    return({
        type: spec.type ? null : "This field is required!",
        command: null,
        image: null,
        model_artifact_path: null,
        runtime: null,
        spark_runtime: null,
        spark_version: null,
        git: {
            directory: null,
            repository: !(spec.type === 'GIT' ^ !!(spec.git && spec.git.repository && (/^(http|https):\/\/[^ "]+$/).test(spec.git && spec.git.repository))) ? null : "Invalid Git URI",
            revision: null,
            secret: null //!(!!(spec.git && spec.git.repository) ^ !!(spec.git &&spec.git.secret)) ? null : "This field is required!"
        },
        experiment: {
            id: !(spec.type === "EXPERIMENT" ^ !!(spec.experiment && spec.experiment.id)) ? null : "This field is required!",
           // requirements: !(spec.type === "EXPERIMENT" ^ !!(spec.experiment && spec.experiment.requirements)) ? null : "This field is required!"
        },
        model_artifact: {
            ml_library: null,
            data_source : [
                {
                  type: null,
                  s3: {
                    path: null,
                    bucket: null,
                    key: null,
                    aws_secret: null
                  }
                }
            ],
            // requirements: null
        },
      
        requirements: ValidateRequiremnts(spec)});
}

function ValidateRequiremnts(spec){
    if(spec.type === "EXPERIMENT" || spec.type === "REGISTERED_MODEL"){
        if(!spec.registered_model.requirements){
            return "This field is required!";
        }else{
            return null
        }
    }
   // !((spec.type === "EXPERIMENT" || spec.type === "REGISTERED_MODEL") ^ !!(spec.registered_model.requirements)) ? null : "This field is required!"    })
}

function validateSecrets(spec={}) {
    return(null)
}
function validateNotifications(spec={}) {
    if(!spec) return null;
    return({
        type: null,
        email: !(spec.type === "EMAIL" ^ !!(spec.email && spec.email.email_addresses)) ? null : "This field is required",
        slack: !(spec.type === "SLACK" ^ !!(spec.slack && spec.slack.webhook_url)) ? null : "This field is required",
    })
}
function validateSchedule(spec = {}) {
    if(!spec) return null;
    return({
        type: null,
        cron_expression: !(spec.type==="CRON" ^ !!spec.cron_expression) ? null : "This field is required!",
        duration: !((spec.type && spec.type !== "CRON") ^ !!spec.duration) ? null : "This field is required!"
    })
}
function validateText(spec) {
    return (
        spec ? null : "This field is required!"
    )
}

function validateName(spec="") {
    if(!spec) return ("This field can only contain Alphanumeric characters and '-'");
    const text = spec.replace(/[a-z0-9-]/gi,"");
    return (
        !text ? null : "This field can only contain Alphanumeric characters and '-'"
    )
}
function validateDeployCondition(spec={}) {
    return(null)
}