import React, {Component} from "react";
import { Link } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { fetchSecrets } from "../../../ducks/jobStore";
import { FormControl, InputLabel, Select, Input, Chip, MenuItem, FormHelperText} from '@material-ui/core';
import { parseEnvSrcToArray, parseArrayToEnvSrc, capitalizeVars } from "../../../utils/helper.js";
import { changeJobStore } from "../../../ducks/jobStore"
import MicroModal from "micromodal";
import Button from '@material-ui/core/Button';


let reset = false
class EnvSource extends Component {
    state = {
        env_source: this.props.env_source,
        env_source_cpy: parseEnvSrcToArray(this.props.env_source),
        }
    
    handleEnvSource = (event) => {
        if(this.props.readOnly) return;
        const target = event.target;
        const name = target.name;
        const value = target.value;
        this.setState({
            [name]: value
        })
    }
     
    handleBlur = () => {
        if(this.props.readOnly) return;
        const name = this.props.fieldName || 'env_source';
        this.props.addToSpec(
            name, parseArrayToEnvSrc(this.state.env_source_cpy)
        )

      
    }

    render() {
        const { env_source_cpy } = this.state;
        const { secretsList, project_id, status, desc, fieldName, readOnly,jobStore } = this.props;
        
        return (
            <div>
                {desc &&
                <div className="row mb">
                    <div className="col-sm-12">
                        <span className="text-gray">Add Secrets</span>
                    </div>
                </div>}
                <div className="row">
                    <div className="col-sm-10">
                        <FormControl className="fw" error={!project_id}>
                            <InputLabel htmlFor="select-multiple-chip">{desc ? "Secrets" : capitalizeVars(fieldName)}</InputLabel>
                            <Select
                                multiple
                                className="fw"
                                name="env_source_cpy"
                                value={env_source_cpy}
                                onChange={this.handleEnvSource}
                                disabled={readOnly} 
                                onBlur={(e)=>this.handleBlur()}
                                input={<Input id="env_source_cpy" />}
                                renderValue={selected => (
                                    <div>
                                        {selected.map(value => (
                                            <Chip key={value} label={value} />
                                        ))}
                                    </div>
                                )}
                            >
                                {secretsList.map(secret => (
                                    <MenuItem key={secret.name} value={secret.name} >
                                        {secret.name}
                                    </MenuItem>
                                ))}
                            </Select>                            
                            
                            {/* {project_id ?
                                <Link className="mt-sm" to={`/projects/${project_id}/secrets`} target="_blank">
                                    Add Secrets
                                </Link> :
                                <FormHelperText>Please select a project first!</FormHelperText>
                            } */}
                            {project_id &&
                                <span
                                    className="text-link"
                                    onClick={()=>MicroModal.show("secret-modal")}
                                >
                                    Add Secrets
                            </span>
                            }
                        </FormControl>
                    </div>
                    <div className="col-sm-2">
                        <button type="button" className="btn btn-sm btn-default mt-lg" onClick={()=>{
                            if(!project_id) return;
                            this.props.fetchSecrets({project_id: project_id});
                        }}>
                            <i className={status === 'loading' ? "fa fa-sync rotate" : "fa fa-sync"} />
                        </button>
                    </div>
                </div>
              
            </div>
        )
    }
}


const mapStateToProps = state => {
    return {
        project_id:state.projects.globalProject && state.projects.globalProject.id,
        secretsList: state.jobStore.secretsList,
        status: state.jobStore.status,
        selectedProject: state.projects.globalProject,
        jobStore: state.jobStore.jobStore,
        
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            fetchSecrets,
            changeJobStore
        },
        dispatch
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(EnvSource);