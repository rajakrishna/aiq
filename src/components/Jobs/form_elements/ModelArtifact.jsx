import React, {Component} from "react";
import { Link } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { fetchSecrets } from "../../../ducks/jobStore";
import { TextField, MenuItem } from "@material-ui/core";
import { capitalizeVars } from "../../../utils/helper";
import MicroModal from "micromodal";

const libraries = [
    "SKLEARN",
    // "TENSORFLOW",
    "KERAS",
    "PYTORCH",
    // "H2O",
    // "SPARKML",
    // "SAGEMAKER",
    // "TPOT",
    // "AUTO_SKLEARN",
    // "H2O_AUTOML"
]

class ModelArtifact extends Component {
    state = {
        ml_library: this.props.model_artifact.ml_library || "",
        data_source: this.props.model_artifact.data_source || {
            type : "S3",
            s3 : {
                path : null,
                bucket : "",
                key : "",
                aws_secret : ""
            }
        },
        requirements: this.props.requirements || ""
    }

    handleRequirements = (event) => {
        if(this.props.readOnly) return;
        const target = event.target;
        const value = target.value || "";
        this.setState({
            requirements: value.split("\n").join(",")
        })
    }

    handleSubModelArtifact = (event, type) => {
        if(this.props.readOnly) return;
        const target = event.target;
        const name = target.name;
        const value = (/NOT_SET/g).test(target.value) ? "" : target.value;
        let data_source = {...this.state.data_source};
        data_source[type][name] = value;
        this.setState({data_source});
    }

    handleBlur = () => {
        if(this.props.readOnly) return;
        const state = {...this.state};
        const requirements = state.requirements;
        delete state.requirements;
        this.props.handleChildren("model_artifact", {...state});
        this.props.handleChildren("requirements", requirements);
    }

    render() {
        const { ml_library, data_source, requirements } = this.state;
        const { secretsList, project_id, status, readOnly } = this.props;
        return (
            <div>
                <TextField
                    id={"ml_library"}
                    label={"ML Library"}
                    name={"ml_library"}
                    value={ml_library || "ML_LIBRARY_NOT_SET"}
                    disabled={readOnly}
                    select
                    onChange={(e)=>{
                        const value = e.target.value;
                        if((/NOT_SET/g).test(value)){
                            this.setState({
                                ml_library: ""
                            })
                        } else {
                            this.setState({
                                ml_library: value
                            })
                        }
                    }}
                    onBlur={this.handleBlur}
                    margin="normal"
                    fullWidth
                    InputLabelProps={{
                        shrink: true,
                    }}
                >
                    <MenuItem value="ML_LIBRARY_NOT_SET">Select ML Library</MenuItem>
                    {libraries.map((e,i)=>{
                        return(
                            <MenuItem value={e} key={"library"+i}>{capitalizeVars(e)}</MenuItem>
                        )
                    })}
                </TextField>
                <TextField 
                    label="Bucket" 
                    fullWidth 
                    value={data_source.s3 && data_source.s3.bucket}
                    disabled={readOnly}
                    name={"bucket"} 
                    onChange={(event)=>{this.handleSubModelArtifact(event,"s3")}}
                    onBlur={this.handleBlur}
                    margin="normal"
                />
                <TextField 
                    label="Key" 
                    fullWidth 
                    value={data_source.s3 && data_source.s3.key}
                    disabled={readOnly}
                    name={"key"} 
                    onChange={(event)=>{this.handleSubModelArtifact(event,"s3")}}
                    onBlur={this.handleBlur}
                    margin="normal"
                />
                <div className="container-fluid pd-0">
                    <div className="row">
                        <div className="col-sm-10">
                            <TextField 
                                label="Secret"
                                name={"aws_secret"}
                                select
                                value={(data_source.s3 && data_source.s3.aws_secret) || "SECRET_NOT_SET"}
                                disabled={readOnly}
                                margin="normal"
                                fullWidth
                                error={!project_id}
                                helperText={!project_id && "Please select a project"}
                                onChange={(event) => {this.handleSubModelArtifact(event, "s3")}}
                                onBlur={this.handleBlur}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            >
                                <MenuItem value={"SECRET_NOT_SET"}>Select Secret</MenuItem>
                                {secretsList.map(secret => (
                                    <MenuItem key={secret.name} value={secret.name} >
                                        {secret.name}
                                    </MenuItem>
                                ))}
                            </TextField>
                            {/* {project_id ?
                                <Link className="mt-sm" to={`/projects/${project_id}/secrets`} target="_blank">
                                    Add Secrets
                                </Link>: ""
                            } */}
                            {project_id &&
                                <span
                                    className="text-link"
                                    onClick={()=>MicroModal.show("secret-modal")}
                                >
                                    Add Secrets
                                </span>
                            }
                        </div>
                        <div className="col-sm-2" style={{marginTop: 34}}>
                            <button type="button" className="btn btn-sm btn-default" onClick={()=>{
                                if(!project_id) return;
                                this.props.fetchSecrets({project_id: project_id});
                            }}>
                                <i className={status === 'loading' ? "fa fa-sync rotate" : "fa fa-sync"} />
                            </button>
                        </div>
                    </div>
                </div>
                <TextField
                    id={"requirements"}
                    label={"Requirements"}
                    name={"requirements"}
                    value={requirements && requirements.split(",").join("\n")}
                    disabled={readOnly}
                    multiline={true}
                    rows={4}
                    rowsMax={4}
                    onChange={this.handleRequirements}
                    onBlur={this.handleBlur}
                    margin="normal"
                    placeholder={"sklearn\nscipy"}
                    fullWidth
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        project_id: state.jobStore.jobStore.spec.project_id,
        secretsList: state.jobStore.secretsList,
        status: state.jobStore.status
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            fetchSecrets
        },
        dispatch
    );
};


export default connect(mapStateToProps, mapDispatchToProps)(ModelArtifact);