import React, { Component } from "react";
import { TextField } from "@material-ui/core";
import { bindActionCreators } from "redux";
import { changeJobStore, changeJobParameters, errorStore, fetchSecrets } from "../../../ducks/jobStore";
import { globalProjectSelection } from "../../../ducks/projects";
import { connect } from "react-redux";
import ControlledExpansionPanels from '../../Collapse';

class WebhookflowParameters extends Component {
    state = {
        parameter: [],
        parameterErrors: []
    }

    componentDidMount = () => {
        const p1 = this.props.jobStore && this.props.jobStore.spec && this.props.jobStore.spec.parameters;
        const p2 = this.props.jobStore.parameters && this.props.jobStore.parameters.parameters;
        const p3 = this.props.jobStore.parameters && this.props.jobStore.parameters.spec && this.props.jobStore.parameters.spec.parameters;
        if (p1&&p1.length> 0 || p2&&p2.length>0 || p3&&p3.length>0) {
            let parameters;
            if (p1&&p1.length > 0) {
                parameters = p1;
            } else if (p2&&p2.length > 0) {
                parameters = p2
            } else if (p3&&p3.length > 0) {
                parameters = p3
            }
            const parameter = [...parameters]
            const parameterErrors = []
            parameter.map((eachItem) => parameterErrors.push({"name": false, "value" : false}))
            this.setState({parameter, parameterErrors})
        }
    }

    range = (start, end) => {
        var rangeResult = [];
        for (var i = start; i < end; i++) {
          rangeResult.push(i);
        }
        return rangeResult;
      }

    validateParameterValueCharCode = (newParameterValue) => {
        if (newParameterValue.length > 1) {
            return newParameterValue.split('').map((character) => {
                return String(character).charCodeAt(0);
              }).every((charCode) => {
                return (charCode > 31 && charCode <= 255 && charCode !== 127) || charCode === 9;
              });    
        } else {
            const newCharCode = String(newParameterValue).charCodeAt(0);
            return (newCharCode > 31 && newCharCode <= 255 && newCharCode !== 127) || newCharCode === 9;
        }

    }

    validateParameterNameCharCode = (newParameterName) => {
        var expression = /[a-zA-Z][a-zA-Z0-9-_]+/
        var response = newParameterName.match(expression);
    
        if(response === null) {
            return false
        }
        return true
    }

    validateParameterName = (newParameterName) => {
        let isNameValid;
        const isNameLengthValid = newParameterName.length === 0;
        const isNameCharcodeValid = this.validateParameterNameCharCode(newParameterName);
        isNameValid = isNameLengthValid || !isNameCharcodeValid;
        return isNameValid

    }

    validateParameterValue = (newParameterValue) => {
        let isValueValid;   
        const isValueLengthValid = newParameterValue.length === 0;
        const isValueCharcodeValid = this.validateParameterValueCharCode(newParameterValue);
        isValueValid = isValueLengthValid || !isValueCharcodeValid;
        return isValueValid
    }

    onFocussResetParameterErrorValue = (event, index) => {
        if(this.props.readOnly) return;
        const target = event.target;
        const name = (target.name).replace(/\d/g,"");
        const value = target.value;
        let parameterErrors = [...this.state.parameterErrors];
        name === 'name' ? (parameterErrors[index][name] = false) : (parameterErrors[index][name] = false);
        this.setState({parameterErrors});
        this.updateParameter();    
    }

    validateParameterNameAndValue = (event, index) => {
        if(this.props.readOnly) return;
        const target = event.target;
        const name = (target.name).replace(/\d/g,"");
        const value = target.value;
        let parameterErrors = [...this.state.parameterErrors];
        name === 'name' ? (parameterErrors[index][name] = this.validateParameterName(value)) : (parameterErrors[index][name] = this.validateParameterValue(value));
        this.setState({parameterErrors});
        this.updateParameter();        
    }

    handleParameter = (event, index) => {
        if(this.props.readOnly) return;
        const target = event.target;
        const name = (target.name).replace(/\d/g,"");
        const value = target.value;
        let parameter = [...this.state.parameter];
        parameter[index][name] = value;
        this.setState({parameter});
    }

    addParameter = () => {
        if(this.props.readOnly) return;
        const parameterErrors = [...this.state.parameterErrors];
        parameterErrors.push({name: false, value: false});
        const parameter = [...this.state.parameter];
        parameter.push({name:"", value:""});
        this.setState({
            parameter, parameterErrors
        })
    }

    delParameter = (i,parameter="parameter") => {
        if(this.props.readOnly) return;
        let parameterErrors = [...this.state.parameterErrors];
        parameterErrors.splice(i, 1)
        let parameters = [...this.state[parameter]];
        parameters.splice(i,1);
        this.setState({parameter: [...parameters], parameterErrors: [...parameterErrors]}, () => this.updateParameter());
    }

    updateParameter = () => {
        const {jobStore} = this.props;
        const {parameter} = this.state;
        const parameters = [...parameter]
        const newJobStore = jobStore.jobStore
        newJobStore.spec.parameters = parameters
        this.props.changeJobParameters(newJobStore);
        this.props.changeJobStore(newJobStore)
    }

    render() {
        const { parameter, parameterErrors } = this.state;
        const { readOnly } = this.props;
        let collapse = Object.keys(errorStore).length > 0;
        return (
            <ControlledExpansionPanels active={false} errorExpand={collapse} heading={"Parameters"}>
                <div className="mt-2 mb-3">
                    <span>Parameters</span>
                    <div className="container-fluid pl-0">
                        <div className="row mb">
                            <div className="col-sm-10 align-self-center"> 
                                <span className="text-gray">Click the + icon to add parameters</span>
                            </div>
                            <div className="col-sm-2">
                                <button className="btn btn-sm btn-primary" onClick={() => this.addParameter()}>
                                    <i className="fa fa-plus-circle" />
                                </button>
                            </div>
                        </div>
                        {parameter.map((e,i)=>{
                            return(
                            <div className="row" key={"EnvRow"+i}>
                                <div className="col-sm-5">
                                    <TextField 
                                        label="Name"
                                        name={"name"+i}
                                        value={e.name}
                                        margin="normal"
                                        disabled={readOnly}
                                        fullWidth
                                        onChange={(event) => {this.handleParameter(event, i)}}
                                        onBlur={(event) => this.validateParameterNameAndValue(event, i)}
                                        onFocus={(event) => this.onFocussResetParameterErrorValue(event, i)}
                                    >
                                    </TextField>
                                    {parameterErrors[i] && parameterErrors[i].name ? <p className="text-danger">*Invalid Name</p> : ""}
                                </div>
                                <div className="col-sm-5">
                                    <TextField 
                                        label="Value"
                                        name={"value"+i}
                                        value={e.value}
                                        margin="normal"
                                        disabled={readOnly}
                                        fullWidth
                                        onChange={(event) => this.handleParameter(event, i)}
                                        onBlur={(event) => this.validateParameterNameAndValue(event, i)}
                                        onFocus={(event) => this.onFocussResetParameterErrorValue(event, i)}
                                    >
                                    </TextField>
                                    {parameterErrors[i] && parameterErrors[i].value ? <p className="text-danger">*Invalid Value</p> : ""}
                                </div>
                                <div className="col-sm-2" style={{marginTop: 34}}>
                                    <button className="btn btn-sm btn-default" onClick={()=>{this.delParameter(i)}}>
                                        <i className="text-danger fa fa-trash"></i>
                                    </button>
                                </div>
                            </div>
                            )
                        })}
                    </div>
                </div>
            </ControlledExpansionPanels>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        jobStore: state.jobStore,
        selectedProject: state.projects.globalProject,
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            changeJobStore,
            changeJobParameters,
            errorStore,
            fetchSecrets,
            globalProjectSelection
        },
        dispatch
    );
}


export default connect(mapStateToProps, mapDispatchToProps) (WebhookflowParameters);