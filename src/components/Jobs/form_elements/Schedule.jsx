import React, { Component } from "react";
import { TextField, MenuItem, Checkbox, FormGroup, FormControlLabel } from "@material-ui/core";
import ControlledExpansionPanels from '../../Collapse';

export class Schedule extends Component {
    
    state = {
        type: this.props.schedule.type,
        cron_expression: this.props.schedule.cron_expression,
        duration: this.props.schedule.duration,
        suspend: this.props.schedule.suspend,
        cronValidation: false
    }

    isCronValid = (freq) => {
        const validity = freq.split(" ").length === 5
        return validity
      }

    handleSchedule = (event) => {
        if(this.props.readOnly) return;
        const target = event.target;
        // const value = (/NOT_SET/g).test(target.value) ? "" : target.value;
        const value = target.value;
        const name = target.name;

        if(name === 'type') {
            this.setState({
                type: value,
                cron_expression: "",
                duration: ""
            })
            return;
        }

        if (name === 'cron_expression') {
            const cronError = this.isCronValid(value)
            this.setState({cronValidation: cronError})
        }
        this.setState({
            [name]: value
        })
    }

    handleBlur = () => {
        if(this.props.readOnly) return;
        this.props.addToSpec(
            "schedule", {...this.state}
        )
    }

    handleCheckbox = () => {
        this.setState((prevState) => ({suspend: !prevState.suspend}));
    }

    render() {
        const { type, cron_expression, duration, suspend } = this.state;
        const { errorStore, readOnly } = this.props;
        let collapse = Object.keys(errorStore).length > 0;
        
        return (
            <ControlledExpansionPanels active={false} errorExpand={collapse} heading={"Schedule"}>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-12">
                        <TextField
                            id={'type'}
                            label={"Type"}
                            name={'type'}
                            type={"text"}
                            select
                            disabled={readOnly || suspend}
                            value={type || 'SCHEDULE_NOT_SET'}
                            onChange={(e)=>{
                                // const value = e.target.value;
                                // if((/NOT_SET/g).test(value)) return;
                                this.handleSchedule(e)
                            }}
                            onBlur={this.handleBlur}
                            margin="normal"
                            error={!!errorStore.type}
                            helperText={errorStore.type || ""}
                            fullWidth
                            InputLabelProps={{
                                shrink: true,
                            }}
                        >
                            <MenuItem value="SCHEDULE_NOT_SET">Select schedule type</MenuItem>
                            <MenuItem value="CRON">Cron</MenuItem>
                            {/* <MenuItem value='TIME_DRIVEN_DAYS'>Periodic Days</MenuItem>
                            <MenuItem value='TIME_DRIVEN_HOURS'>Periodic Hours</MenuItem>
                            <MenuItem value='TIME_DRIVEN_MINS'>Periodic Minutes</MenuItem> */}
                        </TextField>
                        {
                            type === 'CRON' &&
                            <div>
                                <TextField
                                    id={'cron_expression'}
                                    label={"Cron Expression"}
                                    placeholder="Please enter a valid cron expression"
                                    name={'cron_expression'}
                                    type={"text"}
                                    disabled={readOnly || suspend}
                                    value={cron_expression}
                                    onChange={this.handleSchedule}
                                    onBlur={this.handleBlur}
                                    margin="normal"
                                    error={!!errorStore.cron_expression}
                                    helperText={errorStore.cron_expression || ""}
                                    fullWidth
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                                {!this.state.cronValidation && <span className="text-danger">Please Enter valid cron expression</span>}
                                <FormGroup>
                                    <FormControlLabel
                                        disabled={readOnly}
                                        control={
                                        <Checkbox 
                                            onChange={()=>{this.handleCheckbox()}}
                                            checked={suspend}
                                            onBlur={this.handleBlur}
                                            color="primary"
                                        />
                                        } 
                                        label="Suspend" 
                                    />
                                </FormGroup>
                            </div>
                        }
                        {
                            ['TIME_DRIVEN_HOURS',
                            'TIME_DRIVEN_MINS',
                            'TIME_DRIVEN_SECS'].includes(type) &&
                            <TextField
                                id={'duration'}
                                label={"Duration"}
                                placeholder={"Please enter duration"}
                                name={'duration'}
                                type={"text"}
                                value={duration}
                                disabled={readOnly}
                                onChange={this.handleSchedule}
                                onBlur={this.handleBlur}
                                margin="normal"
                                error={!!errorStore.duration}
                                helperText={errorStore.duration || ""}
                                fullWidth
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        }
                        </div>
                    </div>
                </div>
            </ControlledExpansionPanels>
        )
    }
}