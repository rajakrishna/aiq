import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { TextField, MenuItem,Tooltip } from "@material-ui/core";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import { Env } from "./Env";
import EnvSource from "./EnvSource";
import { Schedule } from "./Schedule";
import { fetchSecrets, changeJobStore } from "../../../ducks/jobStore";
import { Notifications } from "./Notifications";
import WorkflowParameters from "./WorkflowParameters";
import { l } from "../../../utils/helper";

import AceEditor from "react-ace";
import "ace-builds/src-noconflict/mode-yaml";
import "ace-builds/src-noconflict/theme-monokai";
import { confirm } from "../../Confirm";
import MicroModal from "micromodal";
import PropTypes from "prop-types";



class SimpleInput extends Component {
    
    state = {
        [this.props.fieldName]: this.props[this.props.fieldName] || "",
        error: {},
        showAlert:false
    }
 componentDidMount(){
     this.updateProjectDetails()
     console.log("props spec data",this.props.jobStore)
 }
 componentDidUpdate(prevProps) {
    if (this.props.selectedProject && prevProps.selectedProject.id !==  this.props.selectedProject.id) {
     //   MicroModal.show("scaling-modal")
   //  this.updateProjectDetails();
    }
  }




 updateProjectDetails = () =>{
   // MicroModal.show("scaling-modal")
    const {jobStore} = this.props;
    this.props.fetchSecrets({project_id:this.props.selectedProject && this.props.selectedProject.id});
    const jobStoreCpy = {...jobStore};
    jobStoreCpy.spec.project_id = this.props.selectedProject && this.props.selectedProject.id;
    this.props.changeJobStore({...jobStoreCpy});
    const name ="project_id"
    this.setState({
        [name]: this.props.selectedProject && this.props.selectedProject.id
    })

 }
 
    handleChange = (e) => {
            
        if(this.props.readOnly) return;
        const target = e.target;
        const name = target.name;
        const value = target.value;
        console.log("name == "+target.name)
        if(name === 'run') {
            this.setState({
                run: value === 'true'
            })
            return;
        }
        this.setState({
            [name]: value
        })
        this.props.addToSpec(name, value);

    }

    handleBlur = (e) => {
        if(this.props.readOnly) return;
        const target = e.target;
        const name = target.name;
        const value = target.value;
        const { error } = this.state;
        this.props.addToSpec(name, value);
        if(!value) {
            error[name] = "This field is required!";  
        } else {
            error[name] = null;
        }
        this.setState({
            error
        })
    }

    renderInput = (name) => {
        const {jobTypes, errorStore, jobStore, readOnly} = this.props;
        const { error } = this.state;
        switch(name) {
            case "name":
                return(
                    <TextField
                        id={name}
                        label={"Name"}
                        name={name}
                        type={"text"}
                        placeholder={"Enter name"}
                        disabled={readOnly}
                        value={this.state[name]}
                        onChange={this.handleChange}
                        onBlur={this.handleBlur}
                        error={!!errorStore || !!error[name]}
                        helperText={errorStore || error[name]}
                        margin="normal"
                        fullWidth
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                )
            case "sub_type":
                return(
                    <TextField
                        id={name}
                        label={"Type"}
                        name={name}
                        type={"text"}
                        disabled={readOnly}
                        select
                        value={this.state[name] || "JOB_TYPE_NOT_SET"}
                        onChange={(e)=>{
                            const value = e.target.value;
                            if ((/NOT_SET/).test(value)) return;
                            this.handleChange(e);
                            this.props.changeType && this.props.changeType(e.target.value);
                        }}
                        onBlur={(e)=>{
                            this.props.addToSpec(e.target.name, e.target.value);
                        }}
                        error={!!errorStore || !!error[name]}
                        helperText={errorStore || error[name]}
                        margin="normal"
                        fullWidth
                        InputLabelProps={{
                            shrink: true,
                        }}
                    >
                        <MenuItem value="JOB_TYPE_NOT_SET">Select a Job Type</MenuItem>
                        {
                            jobTypes && jobTypes.map((e,i) => {
                                return (
                                    <MenuItem value={e} key={e+i}>{l[e]}</MenuItem>
                                )
                            })
                        }
                    </TextField>
                )
            case "replicas":
                return(
                    <TextField
                        id={name}
                        label={"Replicas"}
                        name={name}
                        disabled={readOnly}
                        type={"number"}
                        placeholder={"1"}
                        value={this.state[name]}
                        onChange={this.handleChange}
                        onBlur={this.handleBlur}
                        error={!!errorStore || !!error[name]}
                        helperText={(errorStore && errorStore) || error[name]}
                        margin="normal"
                        fullWidth
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                )
            // case "run":
            //     if(jobStore.id)
            //         return("");
            //     return(
            //         <div className="mt-lg">
            //             <FormControl component="fieldset">
            //                 <FormLabel component="legend" className="f-12">Run Job?</FormLabel>
            //                 <RadioGroup 
            //                     aria-label={name} 
            //                     name={name} 
            //                     value={String(this.state[name])} 
            //                     onChange={this.handleChange}
            //                     onBlur={this.handleBlur}
            //                     row

            //                 >
            //                     <FormControlLabel
            //                         value={'true'}
            //                         control={<Radio color="primary" />}
            //                         label="True"
            //                         labelPlacement="start"
            //                     />
            //                     <FormControlLabel
            //                         value={'false'}
            //                         control={<Radio color="primary" />}
            //                         label="False"
            //                         labelPlacement="start"
            //                     />
            //                 </RadioGroup>
            //             </FormControl>
            //         </div>
            //     )
            case "project_id":
                return(
                    <TextField
                        id={name}
                        label={"Project Name"}
                        name={name}
                        disabled={true}
                        value={this.props.jobStore.spec[name]}
                        onChange={(e) => {
                            this.handleChange(e);
                          
                        }}
                        onBlur={this.handleBlur}
                        error={!!errorStore || !!error[name]}
                        helperText={errorStore || error[name]}
                        margin="normal"
                        fullWidth
                        select
                        InputLabelProps={{
                            shrink: true,
                        }}
                    >
                        <MenuItem value="PROJECT_NOT_SET">Select a project</MenuItem>
                        {this.props.projects.map((e,i)=>{
                            return(
                                <MenuItem key={"projectSelect"+i} value={e.id}>{e.name}</MenuItem>
                            )
                        })}
                    </TextField>
                )
            case "model_name":
                return(
                    <TextField
                        id={name}
                        label={"Model Name"}
                        name={name}
                        disabled={readOnly}
                        type={"text"}
                        placeholder={"Please enter model name"}
                        value={this.props.jobStore.spec[name]}
                        onChange={this.handleChange}
                        onBlur={this.handleBlur}
                        error={!!errorStore || !!error[name]}
                        helperText={errorStore || error[name]}
                        margin="normal"
                        fullWidth
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                )
            case "global_env_variables":
                return(
                    <Env env={this.state[name] || []} addToSpec={this.props.addToSpec} fieldName={name} readOnly={readOnly}>
                        <EnvSource desc={true} env_source={this.props.global_env_source_variables || []} addToSpec={this.props.addToSpec} fieldName={"global_env_source_variables"} readOnly={readOnly} />    
                    </Env>
                );
            case "image_pull_secrets":
                return(
                    <div className="mt-lg">
                        <EnvSource env_source={this.state[name] || []} addToSpec={this.props.addToSpec} fieldName={name} readOnly={readOnly} />  
                    </div>
                )
            case "docker_push_secrets":
                return(
                    <div className="mt-lg">
                        <EnvSource env_source={this.state[name] || []} addToSpec={this.props.addToSpec} fieldName={name} readOnly={readOnly} />  
                    </div>
                )
            case "git_secrets":
                return(
                    <div className="mt-lg">
                        <EnvSource env_source={this.state[name] || []} addToSpec={this.props.addToSpec} fieldName={name} readOnly={readOnly} />  
                    </div>
                )
            case "notifications":
                return(
                    <div>
                        <div className="mb-5">
                            <WorkflowParameters errorStore={errorStore || {}} addToSpec={this.props.addToSpec} readOnly={readOnly} />
                        </div>
                        <div>
                            <Notifications notifications={this.state[name] || {}} errorStore={errorStore || {}} addToSpec={this.props.addToSpec} readOnly={readOnly} />
                        </div>
                    </div>
                )
            case "schedule":
                return(
                    <Schedule schedule={this.state[name] || {}} errorStore={errorStore || {}} addToSpec={this.props.addToSpec} readOnly={readOnly} />
                )
            case "yaml":
                return(
                <div className="mt-lg mb-lg">
                    <div className="mb f-12" style={{color: "rgba(0, 0, 0, 0.54)"}}>
                        YAML
                    </div>
                    <AceEditor
                        mode="yaml"
                        theme="monokai"
                        name="yaml"
                        onChange={(value)=>this.handleChange({
                            target: {
                                name: "yaml",
                                value
                            }
                        })}
                        onBlur={()=>{
                            this.handleBlur({
                               target: {
                                   name: "yaml",
                                   value: this.state[name]
                               } 
                            })
                        }}
                        value={this.state[name]}
                        editorProps={{ $blockScrolling: true }}
                        width={"100%"}
                        height={"300px"}
                        fontSize={16}
                    />
                </div>
            )
        }
    }
    render() {
        return(
            <div>
                {
                    this.renderInput(this.props.fieldName)
                }
        <div className="inline-block">
     
                          <div
          className="modal micromodal-slide"
          id="scaling-modal"
          aria-hidden="true"
        >
          <div className="modal__overlay" tabIndex="-1" data-micromodal-close>
            <div
              className="modal__container"
              role="dialog"
              aria-modal="true"
              aria-labelledby="secret-modal-title"
            >
              <header className="modal__header">
                <h2 className="modal__title">Are you sure you want to leave this page?</h2>
                <button
                  type="button"
                  className="modal__close"
                  aria-label="Close modal"
                  data-micromodal-close
                />
              </header>
              <main className="modal__content">
                <div className="modal-filter-form">
                  <div className="modal-body">
                  <p>You have unsaved changes that will be lost if you decide to continue.</p>
                  </div>
                  <div className="modal-footer">
                   
                    <button
                      onClick={() => MicroModal.close("scaling-modal")}
                      type="button"
                      className="btn btn-secondary"
                    >
                      Close
                    </button>
                    <button
                      type="button"
                      className="btn btn-primary"
                     // onClick={this.handleScaling}
                    >
                      Scale
                    </button>
                  </div>
                </div>
              </main>
            </div>
          </div>
        </div>
 </div>
            </div>
            
        )
    }
}

const mapStateToProps = state => {
    return {
        projects: state.projects.projects,
        jobStore: state.jobStore.jobStore,
        global_env_source_variables: state.jobStore.jobStore.spec.global_env_source_variables,
        selectedProject: state.projects.globalProject,
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            fetchSecrets,
            changeJobStore
        },
        dispatch
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(SimpleInput);
