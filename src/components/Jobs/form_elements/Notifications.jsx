import React, { Component } from "react";
import { TextField, MenuItem } from "@material-ui/core";
import ControlledExpansionPanels from '../../Collapse';
import WebhookHeaders from "./WebhookHeaders";

export class Notifications extends Component {
    state = {
        type: this.props.notifications.type,
        email: this.props.notifications.email && this.props.notifications.email.email_addresses,
        slack: this.props.notifications.slack && this.props.notifications.slack.webhook_url,
        webhook: "",
        webhookUrlError: false
    }

    handleNotifications = (event) => {
        if(this.props.readOnly) return;
        const target = event.target;
        const value = (/NOT_SET/g).test(target.value) ? "" : target.value;
        const name = target.name;

        if(name === "type") {
            this.setState({
                type: value,
                email: "",
                slack: ""
            })
            return;
        }

        this.setState({
            [name]: value
        })
    }

    handleBlur = () => {
        if(this.props.readOnly) return;
        const { email, slack, type, webhook } = this.state;
        let notification = {
            type: type
        }
        if(email) {
            notification.email = {
                email_addresses: email
            }
        } else {
            notification.slack = {
                webhook_url: slack
            }
        }
        this.props.addToSpec(
            "notifications", notification
        )
    }

    validateWebhookUrl = () => {
        const {webhook} = this.state
        var expression = /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g;
        var response = webhook.match(expression);
    
        let isWebhookUrlValid
        if(response === null) {
            this.setState({webhookUrlError: true})
        } else {
            this.setState({webhookUrlError: false})
        }
    }

    render() {
        const { type, email, slack, webhook } = this.state;
        const { errorStore, readOnly } = this.props;
        let collapse = Object.keys(errorStore).length > 0;
        return (
            <ControlledExpansionPanels active={false} errorExpand={collapse} heading={"Notifications"}>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-12">
                        <TextField
                            id={'type'}
                            label={"Type"}
                            name={'type'}
                            type={"text"}
                            select
                            disabled={readOnly}
                            value={type || 'NOTIFICATIONS_NOT_SET'}
                            onChange={this.handleNotifications}
                            onBlur={this.handleBlur}
                            error={!!errorStore.type}
                            helperText={errorStore.type || ""}
                            margin="normal"
                            fullWidth
                            InputLabelProps={{
                                shrink: true,
                            }}
                        >
                            <MenuItem value="NOTIFICATIONS_NOT_SET">Select notification type</MenuItem>
                            {/* <MenuItem value="EMAIL">Email</MenuItem>
                            <MenuItem value="SLACK">Slack</MenuItem> */}
                            <MenuItem value="WEBHOOK">Webhook</MenuItem>
                        </TextField>
                        {
                            type === 'EMAIL' &&
                            <TextField
                                id={'email'}
                                label={"Email Address"}
                                placeholder="Please enter a valid email address"
                                name={'email'}
                                type={"text"}
                                value={email}
                                disabled={readOnly}
                                onChange={this.handleNotifications}
                                onBlur={this.handleBlur}
                                margin="normal"
                                error={!!errorStore.email}
                                helperText={errorStore.email || ""}
                                fullWidth
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        }
                        {
                            type === 'SLACK' &&
                            <TextField
                                id={'slack'}
                                label={"Webhook Url"}
                                placeholder={"Please enter webhook url"}
                                name={'slack'}
                                type={"text"}
                                value={slack}
                                disabled={readOnly}
                                onChange={this.handleNotifications}
                                onBlur={this.handleBlur}
                                margin="normal"
                                error={!!errorStore.slack}
                                helperText={errorStore.slack || ""}
                                fullWidth
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        }
                        {
                            type === 'WEBHOOK' && 
                            <div>
                                <TextField
                                    id={'webhook'}
                                    label={"Webhook Url"}
                                    placeholder={"Please enter webhook url"}
                                    name={'webhook'}
                                    type={"text"}
                                    value={webhook}
                                    disabled={readOnly}
                                    onChange={(event) => this.setState({webhook: event.target.value})}
                                    onBlur={() => this.validateWebhookUrl()}
                                    margin="normal"
                                    error={!!errorStore.slack}
                                    helperText={errorStore.slack || ""}
                                    fullWidth
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                                {this.state.webhookUrlError ?  <p className="text-danger">*Please enter valid url</p> : ""}
                                <WebhookHeaders webhook_url={this.state.webhook} addToSpec={this.props.addToSpec} readOnly={this.props.readOnly} />
                            </div>
                        }
                        </div>
                    </div>
                </div>
            </ControlledExpansionPanels>
        )
    }
}