import React, { Component } from "react";
import { TextField, MenuItem } from "@material-ui/core";
import ControlledExpansionPanels from '../../Collapse';

export class Resources extends Component {

    state = {
        cpu: this.props.resources.cpu || "",
        gpu: this.props.resources.gpu || "",
        memory: this.props.resources.memory || "",
        ps: this.props.resources.ps || "",
        tf_version: this.props.resources.tf_version || "",
        tpu: this.props.resources.tpu || "",
        workers: this.props.resources.workers || ""
    }

    handleResources = (event) => {
        if(this.props.readOnly) return;
        const target = event.target;
        const name = target.name;
        if((/^[0-9\.]+$/g).test(target.value) || !target.value) {
            const value = name === 'memory' ? this.addM(target.value): target.value;
            this.setState({
                [name]: value
            });
        }
    }

    addM(val) {
        if(!val) return null;
        if(isNaN(val)) {
            val = val.replace(/[a-zA-Z]/g,"");
        }
        return val+"M";
    }

    saveResources = () => {
        if(this.props.readOnly) return;
        this.props.addToSpec("resources", {...this.state});
    }

    render() {
        const { cpu, gpu, memory, ps, tf_version, tpu, workers } = this.state;
        const { steptype, readOnly } = this.props;
        return (
            <ControlledExpansionPanels active={false} heading={"Resource Requirements"}>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-12">
                            <TextField
                                id={"cpu"}
                                label={"CPU"}
                                name={"cpu"}
                                placeholder={"0"}
                                inputProps={{ min: 0, step: 1 }}
                                type="number"
                                disabled={readOnly}
                                value={cpu}
                                onChange={this.handleResources}
                                onBlur={() => this.saveResources()}
                                margin="normal"
                                fullWidth
                                helperText={"vCPU/Core"}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                            <TextField
                                id={"gpu"}
                                label={"GPU"}
                                name={"gpu"}
                                placeholder={"0"}
                                inputProps={{ min: 0, step: 1 }}
                                type="number"
                                disabled={readOnly}
                                value={gpu}
                                onChange={this.handleResources}
                                onBlur={() => this.saveResources()}
                                margin="normal"
                                fullWidth
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                            <TextField
                                id={"memory"}
                                label={"Memory"}
                                name={"memory"}
                                placeholder={"0"}
                                inputProps={{ min: 0, step: 1 }}
                                type="number"
                                disabled={readOnly}
                                value={memory && String(memory).replace("M","")}
                                onChange={this.handleResources}
                                onBlur={() => this.saveResources()}
                                margin="normal"
                                fullWidth
                                helperText={"MB"}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                            <TextField
                                id={"tpu"}
                                label={"TPU"}
                                name={"tpu"}
                                placeholder={"Enter tpu details"}
                                inputProps={{ min: 0, step: 0.1 }}
                                type="number"
                                disabled={readOnly}
                                value={tpu}
                                onChange={this.handleResources}
                                onBlur={() => this.saveResources()}
                                margin="normal"
                                fullWidth
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                            {tpu ?
                                <TextField
                                    id={"tf_version"}
                                    label={"TF Version"}
                                    select
                                    name={"tf_version"}
                                    placeholder={"Enter tf_version details"}
                                    value={tf_version || "Select"}
                                    disabled={readOnly}
                                    onChange={this.handleResources}
                                    onBlur={() => this.saveResources()}
                                    margin="normal"
                                    fullWidth
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                >
                                    <MenuItem value="Select">Select An Option</MenuItem>
                                    <MenuItem value="TF_VERSION_1_10">TF Version v1.10</MenuItem>
                                    <MenuItem value="TF_VERSION_1_11">TF Version v1.11</MenuItem>
                                    <MenuItem value="TF_VERSION_1_12">TF Version v1.12</MenuItem>
                                </TextField>
                                : ""
                            }
                            {steptype === "TRAIN_DISTRIBUTED" && 
                            <TextField
                                id={"ps"}
                                label={"PS"}
                                name={"ps"}
                                placeholder={"1"}
                                disabled={readOnly}
                                inputProps={{ min: 1, step: 1 }}
                                type="number"
                                value={ps}
                                onChange={this.handleResources}
                                onBlur={() => this.saveResources()}
                                margin="normal"
                                fullWidth
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />}
                            {steptype === "TRAIN_DISTRIBUTED" && 
                            <TextField
                                id={"workers"}
                                label={"Workers"}
                                name={"workers"}
                                placeholder={"Enter workers details"}
                                disabled={readOnly}
                                inputProps={{ min: 0, step: 1 }}
                                type="number"
                                value={workers}
                                onChange={this.handleResources}
                                onBlur={() => this.saveResources()}
                                margin="normal"
                                fullWidth
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />}
                        </div>
                    </div>
                </div>
            </ControlledExpansionPanels>
        )
    }
}