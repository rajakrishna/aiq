import React, { Component } from "react";
import { TextField } from "@material-ui/core";
import ControlledExpansionPanels from '../../Collapse';
import { l } from "../../../utils/helper";

export class Env extends Component {
    state = {
        env: this.props.env
    }

    handleEnv = (event, index) => {
        if(this.props.readOnly) return;
        const target = event.target;
        const name = (target.name).replace(/\d/g,"");
        const value = target.value;
        let env = [...this.state.env];
        env[index][name] = value;
        this.setState({env});
    }

    addEnv = () => {
        if(this.props.readOnly) return;
        const env = [...this.state.env];
        env.push({name:"", value:""});
        this.setState({
            env
        })
    }

    delEnv = (i,env="env") => {
        if(this.props.readOnly) return;
        let envs = [...this.state[env]];
        envs.splice(i,1);
        this.setState({[env]: envs});
        this.props.addToSpec(this.props.fieldName, [...envs]);
    }

    render() {
        const { env } = this.state;
        const { fieldName, readOnly } = this.props;
        return (
            <ControlledExpansionPanels active={false} heading={l[fieldName]}>
                <div className="container-fluid">
                    <div className="row mb">
                        <div className="col-sm-10"> 
                            <span className="text-gray">Click the + icon to add environment variables</span>
                        </div>
                        <div className="col-sm-2">
                            <button className="btn btn-sm btn-primary" onClick={() => this.addEnv()}>
                                <i className="fa fa-plus-circle" />
                            </button>
                        </div>
                    </div>
                    {env.map((e,i)=>{
                        return(
                        <div className="row" key={"EnvRow"+i}>
                            <div className="col-sm-5">
                                <TextField 
                                    label="Name"
                                    name={"name"+i}
                                    value={e.name}
                                    margin="normal"
                                    disabled={readOnly}
                                    fullWidth
                                    onChange={(event) => {this.handleEnv(event, i)}}
                                    onBlur={()=>{this.props.addToSpec(fieldName, [...env])}}
                                >
                                </TextField>
                            </div>
                            <div className="col-sm-5">
                                <TextField 
                                    label="Value"
                                    name={"value"+i}
                                    value={e.value}
                                    margin="normal"
                                    disabled={readOnly}
                                    fullWidth
                                    onChange={(event) => this.handleEnv(event, i)}
                                    onBlur={()=>{this.props.addToSpec(fieldName, [...env])}}
                                >
                                </TextField>
                            </div>
                            <div className="col-sm-2" style={{marginTop: 34}}>
                                <button className="btn btn-sm btn-default" onClick={()=>{this.delEnv(i)}}>
                                    <i className="text-danger fa fa-trash"></i>
                                </button>
                            </div>
                        </div>
                        )
                    })}
                    <hr />
                    {this.props.children}
                </div>
            </ControlledExpansionPanels>
        )
    }
}