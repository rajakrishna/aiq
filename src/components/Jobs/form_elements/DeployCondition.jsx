import React, { Component } from "react";
import { TextField } from "@material-ui/core";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import ControlledExpansionPanels from '../../Collapse';
import { getErrorMessage } from "../../../utils/helper";

export class DeployCondition extends Component {
    state = {
        compare_with_previous: this.props.deploy_condition.compare_with_previous || "",
        deploy_metrics: this.props.deploy_condition.deploy_metrics || ""
    }

    handleDeployMetrics = (event, index) => {
        if(this.props.readOnly) return;
        const target = event.target;
        const name = (target.name).replace(/\d/g,"");
        const value = target.value;
        let dm = [...this.state.deploy_metrics];
        dm[index][name] = value;
        this.setState({
            deploy_metrics: dm
        });
    }

    addDeployMetrics = () => {
        if(this.props.readOnly) return;
        const deploy_metrics = (this.state.deploy_metrics && [...this.state.deploy_metrics]) || [];
        deploy_metrics.push({name:"", value:""});
        this.setState({
            deploy_metrics
        })
    }

    delDeployMetrics = (i, deploy_metrics="deploy_metrics") => {
        if(this.props.readOnly) return;
        let deploy_metrices = [...this.state[deploy_metrics]];
        deploy_metrices.splice(i,1);
        this.setState({[deploy_metrics]: deploy_metrices});
    }

    render() {
        const { compare_with_previous, deploy_metrics } = this.state;
        const { readOnly } = this.props;
        return (
            <ControlledExpansionPanels active={false} heading={"Deploy Condition"}>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-sm-12">
                        <FormControl component="fieldset">
                            <FormLabel component="legend">Compare With Previous</FormLabel>
                            <RadioGroup 
                                aria-label="compare_with_previous" 
                                name="compare_with_previous" 
                                value={compare_with_previous === true || compare_with_previous === "true"} 
                                onChange={(e)=>{
                                    this.setState({
                                        compare_with_previous: (e.target.value === 'true')
                                    });
                                }} 
                                onBlur={()=>{
                                    this.props.addToSpec("deploy_condition", {...this.state})
                                }}
                                row

                            >
                                <FormControlLabel
                                value={true}
                                control={<Radio color="primary" />}
                                label="True"
                                labelPlacement="start"
                                />
                                <FormControlLabel
                                value={false}
                                control={<Radio color="primary" />}
                                label="False"
                                labelPlacement="start"
                                />
                            </RadioGroup>
                        </FormControl>
                    </div>
                </div>
                {
                    compare_with_previous && 
                    <div className="row mt-lg">
                        <div className="col-sm-10"> 
                            <span className="text-gray">Click the + icon to add Metrics</span>
                        </div>
                        <div className="col-sm-2">
                            <button className="btn btn-sm btn-primary" onClick={() => this.addDeployMetrics()}>
                                <i className="fa fa-plus-circle" />
                            </button>
                        </div>
                    </div>
                }
                {compare_with_previous && deploy_metrics && deploy_metrics.map((e,i)=>{
                    return(
                        <div className="row">
                            <div className="col-sm-5">
                                <TextField 
                                    label="Name"
                                    name={"name"+i}
                                    disabled={readOnly}
                                    value={e.name}
                                    margin="normal"
                                    fullWidth
                                    onChange={(event) => {this.handleDeployMetrics(event, i)}}
                                    onBlur={()=>{
                                        this.props.addToSpec("deploy_condition", {...this.state})
                                    }}
                                >
                                </TextField>
                            </div>
                            <div className="col-sm-5">
                                <TextField 
                                    label="Goal"
                                    name={"goal"+i}
                                    disabled={readOnly}
                                    value={e.goal}
                                    margin="normal"
                                    fullWidth
                                    onChange={(event) => {this.handleDeployMetrics(event, i)}}
                                    onBlur={()=>{
                                        this.props.addToSpec("deploy_condition", {...this.state})
                                    }}
                                >
                                </TextField>
                            </div>
                            <div className="col-sm-2" style={{marginTop: 34}}>
                                <button className="btn btn-sm btn-default" onClick={()=>{this.delDeployMetrics(i)}}>
                                    <i className="text-danger fa fa-trash"></i>
                                </button>
                            </div>
                        </div>
                    )
                })}
                </div>
            </ControlledExpansionPanels>
        )
    }
}