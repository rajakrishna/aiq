import React, { Component } from 'react';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { TextField, MenuItem} from "@material-ui/core"
import { getRegisteredModels, getRegisteredModelsVersion } from '../../../ducks/models';
import { getObjValueFromArray } from "../../../utils/helper"

class RegisteredModel extends Component {
    state = {
        registered_model_version_id: (this.props.registered_model && this.props.registered_model.registered_model_version_id) || "",
        requirements: this.props.requirements || "",
        model_id: ""
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if(nextProps.model_id && !prevState.model_id) {
            return ({
                model_id: nextProps.model_id
            })
        }
        return null;
    }

    componentDidMount() {
        if(this.props.project_id && !(this.props.registeredModels && this.props.registeredModels.length)) {
            this.props.getRegisteredModels(
                {projectID: this.props.project_id}
            );
        }
        if(this.props.model_id) {
            this.props.getRegisteredModelsVersion(
                {registered_model_id: this.props.model_id}
            );
        }
    }

    handleInput = (event) => {
        if(this.props.readOnly) return;
        const target = event.target;
        const name = target.name;
        let value = (/NOT_SET/gi).test(target.value) ? '' : target.value;
        if(name === "registered_model_version_id" || name === "requirements") {
            this.setState({
                [name]: value,
                requirements: getObjValueFromArray(this.props.registeredModelVersion, value, "dependencies") || ""
            },()=>{
                this.props.handleChildren("runtime", getObjValueFromArray(this.props.registeredModelVersion, value, "runtime"))
            })
        }else {
            this.setState({
                [name]: value
            }, ()=>{
                if(name === "model_id") {
                    this.setState({
                        registered_model_version_id: ""
                    })
                    this.props.getRegisteredModelsVersion(
                        {registered_model_id: value}
                    );
                }
            })
        }
    }

    handleBlur = () => {
        if(this.props.readOnly) return;
        const state = {...this.state}
        delete state.model_id;
        const requirements = state.requirements;
       // delete state.requirements;
        this.props.handleChildren("registered_model", {...state});
     //   this.props.handleChildren("requirements", requirements);
    }

    render() {
        const { registered_model_version_id, requirements, model_id } = this.state;
        const { errorStore, readOnly, registeredModels, project_id, registeredModelVersion } = this.props;
        return (
            <div>
                <TextField
                    id={"model_id"}
                    label={"Model"}
                    select
                    name={"model_id"}
                    disabled={readOnly}
                    value={model_id}
                    onChange={this.handleInput}
                    onBlur={this.handleBlur}
                    margin="normal"
                    fullWidth
                    error={!project_id}
                    helperText={!project_id && "Please select a project first!"}
                    InputLabelProps={{
                        shrink: true,
                    }}
                >
                    <MenuItem value="REGISTERED_MODEL_NOT_SET">Select an Model</MenuItem>
                    {registeredModels && registeredModels.map((e,i)=>{
                        return(
                            <MenuItem key={"model_registered"+i} value={e.id}>{e.name}</MenuItem>
                        )
                    })}
                </TextField>
                <TextField
                    id={"registered_model_version_id"}
                    label={"Version"}
                    select
                    name={"registered_model_version_id"}
                    disabled={readOnly}
                    value={registered_model_version_id}
                    onChange={this.handleInput}
                    onBlur={this.handleBlur}
                    margin="normal"
                    fullWidth
                    error={!project_id}
                    helperText={!project_id && "Please select a project first!"}
                    InputLabelProps={{
                        shrink: true,
                    }}
                >
                    <MenuItem value="REGISTERED_MODEL_NOT_SET">Select an Model</MenuItem>
                    {registeredModelVersion && registeredModelVersion.map((e,i)=>{
                        return(
                            <MenuItem key={"model_registered"+i} value={e.id}>{e.registered_model_name+" v"+(e.version ? e.version : "" )}</MenuItem>
                        )
                    })}
                </TextField>
                <TextField
                    id={"requirements"}
                    label={"Requirements"}
                    name={"requirements"}
                    value={requirements && requirements.split(",").join("\n")}
                    disabled={true}
                    multiline={true}
                    rows={4}
                    rowsMax={4}
                    onChange={this.handleInput}
                    onBlur={this.handleBlur}
                    margin="normal"
                    error={!!errorStore.requirements}
                    helperText={errorStore.requirements || ""}
                    fullWidth
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        project_id: state.jobStore.jobStore.spec.project_id,
        secretsList: state.jobStore.secretsList,
        registeredModels: state.models.registeredModels,
        registeredModelVersion: state.models.registeredModelVersion,
        status: state.jobStore.status,
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            getRegisteredModels,
            getRegisteredModelsVersion
        },
        dispatch
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(RegisteredModel);