import React, { Component } from 'react';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { getExperimentList } from "../../../ducks/experiments";
import { TextField, MenuItem} from "@material-ui/core"

class Experiment extends Component {
    state = {
        id: (this.props.experiment && this.props.experiment.id) || "",
        requirements: this.props.requirements || ""
    }

    // static getDerivedStateFromProps(nextProps, prevState) {
    //     return({
    //         id: prevState.id || nextProps.experiment.id,
    //         requirements: prevState.requirements || nextProps.experiment.requirements
    //     })
    // }
    
    componentDidMount() {
        if(this.props.project_id && !this.props.experiments.length) {
            this.props.getExperimentList({
                statusEquals: 'EXPERIMENT',
                projectIDEquals: this.props.project_id,
                size: 5000
            })
        }
    }

    handleExperiment = (event) => {
        if(this.props.readOnly) return;
        const target = event.target;
        const name = target.name;
        let value = (/NOT_SET/gi).test(target.value) ? '' : target.value;
        if(name === "requirements") {
            value = value.split("\n").join(",");
        }
        this.setState({
            [name]: value
        })
    }

    handleBlur = () => {
        if(this.props.readOnly) return;
        this.props.handleChildren("experiment", {id: this.state.id});
        this.props.handleChildren("requirements", this.state.requirements);
    }

    render() {
        const { id, requirements } = this.state;
        const { errorStore, project_id, experiments, readOnly } = this.props;
        return (
            <div>
                <TextField
                    id={"id"}
                    select
                    label={"Experiment"}
                    name={"id"}
                    disabled={readOnly}
                    value={id || "EXPERIMENT_NOT_SET"}
                    onChange={this.handleExperiment}
                    onBlur={this.handleBlur}
                    margin="normal"
                    error={!project_id}
                    helperText={!project_id && "Please select a project first!"}
                    fullWidth
                    InputLabelProps={{
                        shrink: true,
                    }}
                >
                    <MenuItem value="EXPERIMENT_NOT_SET">Select an experiment</MenuItem>
                    {experiments.map((e,i)=>{
                        return(
                            <MenuItem value={e.id}>{e.name+" v"+e.version}</MenuItem>
                        )
                    })}
                </TextField>
                <TextField
                    id={"requirements"}
                    label={"Requirements"}
                    name={"requirements"}
                    value={requirements && requirements.split(",").join("\n")}
                    disabled={readOnly}
                    multiline={true}
                    rows={4}
                    rowsMax={4}
                    onChange={this.handleExperiment}
                    onBlur={this.handleBlur}
                    margin="normal"
                    error={!!errorStore.requirements}
                    helperText={errorStore.requirements || ""}
                    fullWidth
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        project_id: state.jobStore.jobStore.spec.project_id,
        secretsList: state.jobStore.secretsList,
        status: state.jobStore.status,
        experiments: state.experiments.experiments
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            getExperimentList
        },
        dispatch
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(Experiment);