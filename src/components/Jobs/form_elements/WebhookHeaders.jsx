import React, { Component } from "react";
import { TextField } from "@material-ui/core";
import { bindActionCreators } from "redux";
import { changeJobStore, errorStore, fetchSecrets } from "../../../ducks/jobStore";
import { globalProjectSelection } from "../../../ducks/projects";
import { connect } from "react-redux";

class WebhookHeaders extends Component {
    state = {
        fieldName: 'notifications',
        header: [],
        headerErrors: []
    }

    componentDidMount = () => {
        this.updateNotification()
    }

    range = (start, end) => {
        var rangeResult = [];
        for (var i = start; i < end; i++) {
          rangeResult.push(i);
        }
        return rangeResult;
      }

    validateHeaderValueCharCode = (newHeaderValue) => {
        if (newHeaderValue.length > 1) {
            return newHeaderValue.split('').map((character) => {
                return String(character).charCodeAt(0);
              }).every((charCode) => {
                return (charCode > 31 && charCode <= 255 && charCode !== 127) || charCode === 9;
              });    
        } else {
            const newCharCode = String(newHeaderValue).charCodeAt(0);
            return (newCharCode > 31 && newCharCode <= 255 && newCharCode !== 127) || newCharCode === 9;
        }

    }

    validateHeaderNameCharCode = (newHeaderName) => {
        var validCharCodes = []
            .concat(this.range(48, 58))  // 0-9
            .concat(this.range(65, 91))  // a-z
            .concat(this.range(97, 123)) // A-Z
            .concat([94, 95, 96, 124, 126]) // ^, _, `, |, ~
            .concat([33, 35, 36, 37, 38, 39, 42, 43, 45, 46]); // !, #, $, %, &, ', *, +, -, .,

            if (newHeaderName.length > 1) {
                return newHeaderName.split('').map(function (character) {
                return character.charCodeAt(0);
                }).every(function (charCode) {
                return validCharCodes.indexOf(charCode) !== -1;
                });
            } else {
                return validCharCodes.indexOf(newHeaderName) !== -1
            }

    }

    validateHeaderName = (newHeaderName) => {
        let isNameValid;
        const isNameLengthValid = newHeaderName.length === 0;
        const isNameCharcodeValid = this.validateHeaderNameCharCode(newHeaderName);
        isNameValid = isNameLengthValid || !isNameCharcodeValid;
        return isNameValid

    }

    validateHeaderValue = (newHeaderValue) => {
        let isValueValid;
        const isValueLengthValid = newHeaderValue.length === 0;
        const isValueCharcodeValid = this.validateHeaderValueCharCode(newHeaderValue);
        isValueValid = isValueLengthValid || !isValueCharcodeValid;
        return isValueValid
    }

    onFocussResetHeaderErrorValue = (event, index) => {
        if(this.props.readOnly) return;
        const target = event.target;
        const name = (target.name).replace(/\d/g,"");
        const value = target.value;
        let headerErrors = [...this.state.headerErrors];
        name === 'name' ? (headerErrors[index][name] = false) : (headerErrors[index][name] = false);
        this.setState({headerErrors});
        this.updateNotification();    
    }

    validateHeaderNameAndValue = (event, index) => {
        if(this.props.readOnly) return;
        const target = event.target;
        const name = (target.name).replace(/\d/g,"");
        const value = target.value;
        let headerErrors = [...this.state.headerErrors];
        name === 'name' ? (headerErrors[index][name] = this.validateHeaderName(value)) : (headerErrors[index][name] = this.validateHeaderValue(value));
        this.setState({headerErrors});
        this.updateNotification();        
    }

    handleHeader = (event, index) => {
        if(this.props.readOnly) return;
        const target = event.target;
        const name = (target.name).replace(/\d/g,"");
        const value = target.value;
        let header = [...this.state.header];
        header[index][name] = value;
        this.setState({header});
    }

    addHeader = () => {
        if(this.props.readOnly) return;
        const headerErrors = [...this.state.headerErrors];
        headerErrors.push({name: false, value: false});
        const header = [...this.state.header];
        header.push({name:"", value:""});
        this.setState({
            header, headerErrors
        })
    }

    delHeader = (i,header="header") => {
        if(this.props.readOnly) return;
        let headerErrors = [...this.state.headerErrors];
        headerErrors.splice(i, 1)
        let headers = [...this.state[header]];
        headers.splice(i,1);
        this.setState({header: [...headers], headerErrors: [...headerErrors]}, () => this.updateNotification());
    }

    updateNotification = () => {
        const {jobStore, notifications, webhook_url} = this.props;
        const {header} = this.state;
        const newNotificationsType = notifications ? notifications.type : 'WEBHOOK';
        const newNotifications = {
            type: newNotificationsType,
            webhook: {
                webhook_url: webhook_url,
                headers: [...header]
            }
        }
        const newJobStore = jobStore.jobStore
        newJobStore.spec.notifications = newNotifications
        this.props.changeJobStore(newJobStore)
    }

    render() {
        const { header, fieldName, headerErrors } = this.state;
        const { readOnly } = this.props;
        return (
            <div className="mt-4 mb-3">
                <span>Headers</span>
                <div className="container-fluid pl-0">
                    <div className="row mb">
                        <div className="col-sm-10 align-self-center"> 
                            <span className="text-gray">Click the + icon to add headers for webhook</span>
                        </div>
                        <div className="col-sm-2">
                            <button className="btn btn-sm btn-primary" onClick={() => this.addHeader()}>
                                <i className="fa fa-plus-circle" />
                            </button>
                        </div>
                    </div>
                    {header.map((e,i)=>{
                        return(
                        <div className="row" key={"EnvRow"+i}>
                            <div className="col-sm-5">
                                <TextField 
                                    label="Name"
                                    name={"name"+i}
                                    value={e.name}
                                    margin="normal"
                                    disabled={readOnly}
                                    fullWidth
                                    onChange={(event) => {this.handleHeader(event, i)}}
                                    onBlur={(event) => this.validateHeaderNameAndValue(event, i)}
                                    onFocus={(event) => this.onFocussResetHeaderErrorValue(event, i)}
                                >
                                </TextField>
                                {headerErrors[i] && headerErrors[i].name ? <p className="text-danger">*Invalid Header</p> : ""}
                            </div>
                            <div className="col-sm-5">
                                <TextField 
                                    label="Value"
                                    name={"value"+i}
                                    value={e.value}
                                    margin="normal"
                                    disabled={readOnly}
                                    fullWidth
                                    onChange={(event) => this.handleHeader(event, i)}
                                    onBlur={(event) => this.validateHeaderNameAndValue(event, i)}
                                    onFocus={(event) => this.onFocussResetHeaderErrorValue(event, i)}
                                >
                                </TextField>
                                {headerErrors[i] && headerErrors[i].value ? <p className="text-danger">*Invalid Value</p> : ""}
                            </div>
                            <div className="col-sm-2" style={{marginTop: 34}}>
                                <button className="btn btn-sm btn-default" onClick={()=>{this.delHeader(i)}}>
                                    <i className="text-danger fa fa-trash"></i>
                                </button>
                            </div>
                        </div>
                        )
                    })}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        jobStore: state.jobStore,
        notifications: state.jobStore.jobStore.spec.notifications,
        selectedProject: state.projects.globalProject,
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            changeJobStore,
            errorStore,
            fetchSecrets,
            globalProjectSelection
        },
        dispatch
    );
}

export default connect(mapStateToProps, mapDispatchToProps) (WebhookHeaders);