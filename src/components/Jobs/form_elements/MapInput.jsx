export const mapStateToSpec = (type, spec, jobStore, changeJobStore, index, callback) => {
    switch(type) {
        case 'deploy':
            jobStore = mapDeployJobs(spec, jobStore);
        break;
        case 'pre_deploy_steps':
            jobStore = mapPreDeployJobs(spec, jobStore, index);
        break;
        // case 'post_deploy_steps':
        //     jobStore = mapPostDeployJobs(spec, jobStore, index);
        // break;
        // case 'train':
        //     jobStore = mapTrainJobs(spec, jobStore);
        // break;
        // case 'pre_train_steps':
        //     jobStore = mapPreTrainJobs(spec, jobStore, index);
        // break;
        // case 'post_train_steps':
        //     jobStore = mapPostTrainJobs(spec, jobStore, index);
        // break;
        // case 'validate':
        //     jobStore = mapValidateJobs(spec, jobStore);
        // break;
        // case 'pre_validate_steps':
        //     jobStore = mapPreValidateJobs(spec, jobStore, index);
        // break;
        // case 'post_validate_steps':
        //     jobStore = mapPostValidateJobs(spec, jobStore, index);
        // break;
        // default:
        //     jobStore = mapJobs(spec, jobStore);
    }
    
    changeJobStore (
        {...jobStore}
    )
    // setTimeout(()=>{callback()},100);
  }
  
    function mapDeployJobs(spec, jobStore) {
        let deploy = {};
        if(spec.type==="DEPLOY_SIMPLE") {
            deploy = {
            "deploy_condition": spec.deploy_condition,
            "docker": spec.docker,
            "env": spec.env,
            "env_source": spec.env_source,
            "name": spec.name,
            "outputs": spec.outputs,
            "data_source": spec.data_source,
            "replicas": spec.replicas,
            "resources": spec.resources,
            "source": spec.source,
            "type": spec.type
            }
        } else if(spec.type==="DEPLOY_TF_SERVING") {
            deploy = {
            "deploy_condition": spec.deploy_condition,
            "docker": spec.docker,
            "env": spec.env,
            "env_source": spec.env_source,
            "name": spec.name,
            "outputs": spec.outputs,
            "data_source": spec.data_source,
            "replicas": spec.replicas,
            "resources": spec.resources,
            "source": spec.source,
            "type": spec.type
            }
        } else if(spec.type==="DEPLOY_AB_TEST") {
            deploy = {
            "deploy_condition": spec.deploy_condition,
            "docker_a": spec.docker_a,
            "docker_b": spec.docker_b,
            "env_a": spec.env_a,
            "env_b": spec.env_b,
            "env_source_a": spec.env_source_a,
            "env_source_b": spec.env_source_b,
            "name": spec.name,
            "outputs": spec.outputs,
            "data_source": spec.data_source,
            "replicas": spec.replicas,
            "resources": spec.resources,
            "source_a": spec.source_a,
            "source_b": spec.source_b,
            "type": spec.type
            }
        } else if(spec.type==="DEPLOY_TRANSFORMER") {
            deploy = {
            "deploy_condition": spec.deploy_condition,
            "transformer_docker": spec.transformer_docker,
            "model_docker": spec.model_docker,
            "model_env": spec.model_env,
            "transformer_env": spec.transformer_env,
            "transformer_env_source": spec.transformer_env_source,
            "model_env_source": spec.model_env_source,
            "name": spec.name,
            "outputs": spec.outputs,
            "data_source": spec.data_source,
            "replicas": spec.replicas,
            "resources": spec.resources,
            "model_source": spec.model_source,
            "transformer_source": spec.transformer_source,
            "type": spec.type
            }
        } else if(spec.type==="DEPLOY_GRAPH") {
            deploy = {
            "deploy_condition": spec.deploy_condition,
            "docker": spec.docker,
            "env": spec.env,
            "env_source": spec.env_source,
            "name": spec.name,
            "outputs": spec.outputs,
            "data_source": spec.data_source,
            "replicas": spec.replicas,
            "resources": spec.resources,
            "source": spec.source,
            "type": spec.type,
            "yaml": spec.yaml
            } 
        } else {
            deploy = {
            "deploy_condition": spec.deploy_condition,
            "docker": spec.docker,
            "env": spec.env,
            "env_source": spec.env_source,
            "name": spec.name,
            "outputs": spec.outputs,
            "data_source": spec.data_source,
            "replicas": spec.replicas,
            "resources": spec.resources,
            "source": spec.source,
            "type": spec.type
            }
        }
        jobStore.spec.deploy = deploy;
        return(jobStore);
    }

    function mapPreDeployJobs(spec, jobStore, index) {
        let pre_deploy = {};
        pre_deploy = {
            "deploy_condition": spec.deploy_condition,
            "docker": spec.docker,
            "env": spec.env,
            "env_source": spec.env_source,
            "name": spec.name,
            // "output": spec.output,
            "replicas": spec.replicas,
            "resources": spec.resources,
            "source": spec.source,
            "type": spec.type
        }
        jobStore.spec.pre_deploy_steps[index] = pre_deploy;
        return(jobStore);
  }