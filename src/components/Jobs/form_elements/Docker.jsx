import React, {useState, Component} from "react";
import { TextField } from "@material-ui/core";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import ControlledExpansionPanels from '../../Collapse';
import { getErrorMessage, capitalizeVars } from "../../../utils/helper";

export class Docker extends Component {
    state = {
        name : this.props.docker.name || "",
        repository : this.props.docker.repository || "", 
        tag : this.props.docker.tag || ""
    }


    render() {
        const { name, repository, tag } = this.state;
        const { fieldName, errorStore, readOnly } = this.props;
        let collapse = Object.keys(errorStore).length > 0;
        return (
            <ControlledExpansionPanels active={false} errorExpand={collapse} heading={capitalizeVars(fieldName)}>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-12">
                            <TextField
                                label={'Docker Repository'}
                                type={'text'}
                                placeholder={'Enter the Docker repository'}
                                value={repository}
                                onChange={(e)=>{
                                    if(readOnly) return;this.setState({repository: e.target.value})}}
                                onBlur={()=>{
                                    if(readOnly) return;this.props.addToSpec(fieldName, {...this.state})}}
                                margin="normal"
                                disabled={readOnly}
                                error={!!errorStore.repository}
                                helperText={errorStore.repository || ""}
                                fullWidth
                                InputLabelProps={{
                                    shrink: true
                                }}
                            />
                            <TextField
                                label={'Docker Name'}
                                type={'text'}
                                placeholder={'Enter the Docker name'}
                                value={name}
                                onChange={(e)=>{
                                    if(readOnly) return;this.setState({name: e.target.value})}}
                                onBlur={()=>{
                                    if(readOnly) return;this.props.addToSpec(fieldName, {...this.state})}}
                                margin="normal"
                                disabled={readOnly}
                                error={!!errorStore.name}
                                helperText={errorStore.name || ""}
                                fullWidth
                                InputLabelProps={{
                                    shrink: true
                                }}
                            />
                            <TextField
                                label={'Docker Tag'}
                                type={'text'}
                                placeholder={'Enter the Docker tag'}
                                value={tag}
                                onChange={(e)=>{
                                    if(readOnly) return;this.setState({tag: e.target.value})}}
                                onBlur={()=>{
                                    if(readOnly) return;this.props.addToSpec(fieldName, {...this.state})}}
                                margin="normal"
                                disabled={readOnly}
                                error={!!errorStore.tag}
                                helperText={errorStore.tag || ""}
                                fullWidth
                                InputLabelProps={{
                                    shrink: true
                                }}
                            />
                        </div>
                    </div>
                </div>
            </ControlledExpansionPanels>
        )
    }
}