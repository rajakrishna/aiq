import React, {Component} from "react";
import { connect } from "react-redux";
import { DeployCondition } from "./form_elements/DeployCondition.jsx";
import { Docker } from "./form_elements/Docker";
import { Env } from "./form_elements/Env";
import EnvSource from "./form_elements/EnvSource";
import Output from "./form_elements/Output";
import { Resources } from "./form_elements/Resources";
import { Source } from "./form_elements/Source";
import SimpleInput from "./form_elements/SimpleInput";
import Button from '@material-ui/core/Button';
import MicroModal from "micromodal";
import { changeJobStore, errorStore, fetchSecrets } from "../../ducks/jobStore";
import { globalProjectSelection } from "../../ducks/projects";

import { bindActionCreators } from "redux";

class StepForm extends Component {
  state = {
    keys: [],
    spec: {},
    error: {},
    reset: false,
    prevProjectId: "",
    projectId:""
  };

  static getDerivedStateFromProps(nextProps) {
    const keys = Object.keys(nextProps.spec);
    return {
      keys,
      spec: nextProps.spec,
      error: nextProps.errorStore || {},
    };
  }
  componentDidUpdate(prevProps) {
    if (this.props.selectedProject && prevProps.selectedProject.id !== this.props.selectedProject.id && this.props.selectedProject.id !==this.state.prevProjectId) {
      this.setState({ prevProjectId: prevProps.selectedProject.id });
      MicroModal.show("confirm-modal1");
    }
  }
  getSecrets() {
    this.props.fetchSecrets({ project_id: this.props.selectedProject.id });
  }
  getProjectName = (id) => {
    let name = "";
    this.props.projects.map((e) => {
      if (e.id === id) {
        name = e.name;
      }
    });
    return name;
  };
  handleContinue = (reset) => {
    this.setState({ reset: reset });
    this.getSecrets();
    let spec = {
      id: null,
      name: "",
      run: false,
      schedule: {},
      spec: {
        project_id: this.props.selectedProject.id,
        project_name: this.getProjectName(this.props.selectedProject.id),
        docker_push_secrets: [],
        git_secrets: [],
        global_env_source_variables: [],
        global_env_variables: [],
        image_pull_secrets: [],
        model_name: "",
        notifications: {},
        deploy: {},
        post_deploy_steps: [],
        post_test_steps: [],
        post_train_steps: [],
        post_validate_steps: [],
        pre_deploy_steps: [],
        pre_test_steps: [],
        pre_train_steps: [],
        pre_validate_steps: [],
        test: {},
        train: {},
        validate: {},
      },
    };
    spec["project_id"] = this.props.selectedProject.id;
    console.log("state spec",this.state.spec)
  //  this.props.resetData({...spec})
    this.mapStateToSpec(spec);
    MicroModal.close("confirm-modal1");
  };
  mapStateToSpec = (spec) => {
    this.props.changeJobStore({ ...spec });
    //  setTimeout(()=>{callback()},100)
  };

  handleCancel = () => {
    const selectedProject = this.props.projects.find(
      (item) => item.id == this.state.prevProjectId
    );
    this.props.globalProjectSelection(selectedProject);
    MicroModal.close("confirm-modal1");
  };
  resetStateValues = () => {
    const data = {};
    this.props.resetData(data);
  };
  addToSpec = (key, data) => {
    const spec = this.state.spec;
    spec["project_id"] = this.props.selectedProject && this.props.selectedProject.id;
    spec[key] = data;
    this.setState({ spec }, () => {
      this.props.saveData(this.state.spec);
    });
    // this.props.
  };

  resetDataToSpec = () => {
    console.log("props data", this.props);
  };

  getInputField = (fieldName, error, jobStore, readOnly = false) => {
    switch (fieldName) {
      case "deploy_condition":
        return (
          <DeployCondition
            deploy_condition={this.props.spec.deploy_condition || {}}
            errorStore={error[fieldName] || {}}
            addToSpec={this.addToSpec}
            readOnly={readOnly}
          />
        );
      case "docker":
      case "docker_a":
      case "docker_b":
      case "model_docker":
      case "transformer_docker":
        return (
          <Docker
            docker={this.props.spec[fieldName] || {}}
            fieldName={fieldName}
            errorStore={error[fieldName] || {}}
            addToSpec={this.addToSpec}
            readOnly={readOnly}
          />
        );
      case "env":
      case "env_a":
      case "env_b":
      case "model_env":
      case "transformer_env":
        return (
          <Env
            env={this.props.spec[fieldName] || []}
            fieldName={fieldName}
            errorStore={error[fieldName] || {}}
            addToSpec={this.addToSpec}
            readOnly={readOnly}
            resetDataToSpec={this.resetDataToSpec}
          >
            <EnvSource
              desc={true}
              env_source={this.props.spec.env_source || []}
              errorStore={error[fieldName.replace("env", "env_source")]}
              fieldName={fieldName.replace("env", "env_source")}
              addToSpec={this.addToSpec}
              resetDataToSpec={this.resetDataToSpec}
              readOnly={readOnly}
            />
          </Env>
        );
      case "outputs":
      case "data_source":
        return (
          <Output
            outputs={this.props.spec[fieldName] || []}
            fieldName={fieldName}
            errorStore={error[fieldName] || {}}
            addToSpec={this.addToSpec}
            readOnly={readOnly}
          />
        );
      case "resources":
        return (
          <Resources
            steptype={this.props.spec.sub_type}
            resources={this.props.spec.resources || {}}
            errorStore={error[fieldName] || {}}
            addToSpec={this.addToSpec}
            readOnly={readOnly}
          />
        );
      case "source":
      case "model_source":
      case "transformer_source":
      case "source_a":
      case "source_b":
        return (
          <Source
            formType={this.props.formType}
            model_id={jobStore.registered_model}
            steptype={this.props.spec.sub_type}
            source={this.props.spec[fieldName]}
            errorStore={error[fieldName] || {}}
            fieldName={fieldName}
            addToSpec={this.addToSpec}
            readOnly={readOnly}
          />
        );
      default:
        return (
          <SimpleInput
            changeType={this.props.changeType}
            jobTypes={this.props.jobTypes}
            errorStore={error[fieldName]}
            fieldName={fieldName}
            {...{ [fieldName]: this.props.spec[fieldName] }}
            addToSpec={this.addToSpec}
            readOnly={readOnly}
          />
        );
    }
  };

  render() {
    const { keys, error } = this.state;
    const { jobStore } = this.props;
    return (
      <div>
        {keys.map((e) => {
          return (
            <div key={e}>
              {this.getInputField(e, error, jobStore, jobStore.viewable)}
            </div>
          );
        })}
        <div className="mt-lg" style={{ height: 50 }}>
          {/* <button className="btn btn-sm btn-primary" onClick={()=>{
                        this.props.saveData(spec);
                    }}> 
                        Save
                    </button> */}
        </div>
        <div className="inline-block">
          <div
            className="modal micromodal-slide"
            id="confirm-modal1"
            aria-hidden="true"
          >
            <div className="modal__overlay" tabIndex="-1" data-micromodal-close>
              <div
                className="modal__container"
                role="dialog"
                aria-modal="true"
                aria-labelledby="secret-modal-title"
              >
                <header className="modal__header">
                  <h2 className="modal__title">
                    Are you sure you want to change the project?
                  </h2>
                </header>
                <main className="mt-3">
                  <div className="modal-filter-form">
                    <div>
                      <p>
                        You have unsaved changes that will be lost if you decide
                        to continue.
                      </p>
                    </div>
                    <div className="f-r">
                      <Button
                        color="secondary"
                        onClick={() => {
                          this.handleCancel();
                        }}
                      >
                        Cancel
                      </Button>
                      <Button
                        color="primary"
                        onClick={() => {
                          this.handleContinue(true);
                        }}
                      >
                        Continue
                      </Button>
                    </div>
                  </div>
                </main>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
    return {
        jobStore: state.jobStore.jobStore,
        selectedProject: state.projects.globalProject,
        jobError: state.jobStore.jobError,
        projects: state.projects.projects,
        summary: state.projects.summary,
    };
};
const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            changeJobStore,
            errorStore,
            fetchSecrets,
            globalProjectSelection
        },
        dispatch
    );
};

export default connect(mapStateToProps,mapDispatchToProps)(StepForm);