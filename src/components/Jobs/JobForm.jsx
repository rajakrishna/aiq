import React, { Component } from "react";
import { capitalizeVars, actLikeClick, getErrorMessage, getResponseError } from "../../utils/helper";
import PreTrainStepForm from './PreTrainStepForm.jsx';
import TrainStepForm from './TrainStepForm.jsx';
import PostTrainStepForm from './PostTrainStepForm.jsx';
import PreTestStepForm from './PreTestStepForm.jsx';
import TestStepForm from './TestStepForm.jsx';
import PostTestStepForm from './PostTestStepForm.jsx';
import PreValidateStepForm from './PreValidateStepForm.jsx';
import ValidateStepForm from './ValidateStepForm.jsx';
import PostValidateStepForm from './PostValidateStepForm.jsx';
import PreDeployStepForm from './PreDeployStepForm.jsx';
import DeployStepForm from './DeployStepForm.jsx';
import PostDeployStepForm from './PostDeployStepForm.jsx';
import StartStepForm from './StartStepForm.jsx';
import { JobLogs } from './job-run/JobLogs.jsx';
import { JobStatus } from './job-run/JobStatus.jsx';
import JobResource from './job-run/JobResource.jsx';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { toggleSteps, setStep } from "../../ducks/jobStore";

class JobForm extends Component {

    state = {
        visible: false,
        dockStyle: {},
        expanded: false
    }

    renderFormComponent = (name, index="") => {
        const { jobStore } = this.props;
        switch(name) {
        case "pre_train_steps":
            return <PreTrainStepForm index={index} key={name+index} spec={jobStore.spec.pre_train_steps || [{}]} />
        case "train":
            return <TrainStepForm key={name} spec={jobStore.spec.train} />
        case "post_train_steps":
            return <PostTrainStepForm index={index} key={name+index} spec={jobStore.spec.post_train_steps || [{}]} />
        case "pre_test_steps":
            return <PreTestStepForm index={index} key={name+index} spec={jobStore.spec.pre_test_steps || [{}]} />
        case "test":
            return <TestStepForm key={name} spec={jobStore.spec.test} />
        case "post_test_steps":
            return <PostTestStepForm index={index} key={name+index} spec={jobStore.spec.post_test_steps || [{}]} />
        case "pre_validate_steps":
            return <PreValidateStepForm index={index} key={name+index} spec={jobStore.spec.pre_validate_steps || [{}]} />
        case "validate":
            return <ValidateStepForm key={name} spec={jobStore.spec.validate} />
        case "post_validate_steps":
            return <PostValidateStepForm index={index} key={name+index} spec={jobStore.spec.post_validate_steps || [{}]}/>
        case "pre_deploy_steps":
            return <PreDeployStepForm index={index} key={name+index} spec={jobStore.spec.pre_deploy_steps || [{}]} />
        case "deploy":
            return <DeployStepForm key={name} spec={jobStore.spec.deploy} />
        case "post_deploy_steps":
            return <PostDeployStepForm index={index} key={name+index} spec={jobStore.spec.post_deploy_steps || [{}]} />
        case "ML_Workflow":
            return <StartStepForm spec={jobStore} key="ML_Workflow" />
        }
    }

    toggleMinimize = () => {
        let dockStyle = {...this.state.dockStyle};
        if(Object.keys(dockStyle).length) {
            this.setState({
                dockStyle: {},
                expanded: true
            })
        } else {
            this.setState({
                dockStyle: this.getStyle(),
                expanded: false
            })
        }
    }

    componentDidMount() {
        this.setState({
            dockStyle: this.getStyle()
        });
    }

    getStyle() {
        const timelineSection = document.querySelector(".timeline-section");
        return ({
            display: "block",
            top: `${timelineSection.offsetTop + timelineSection.offsetHeight + 5}px`,
            left: 0,
            height: `${document.body.scrollHeight - timelineSection.offsetTop - timelineSection.offsetHeight - 50}px`,
            minHeight: `${document.body.scrollHeight - timelineSection.offsetTop - timelineSection.offsetHeight - 130}px`
        })
    }

    render() {
        const { jobtype, jobIndex, jobStore, token, currentIndex } = this.props;
        const { dockStyle, expanded } = this.state;
        return (
            <div className={"overlay scrollable"} style={dockStyle}>
                <h4 className="overlay-heading pd-lg">
                    {capitalizeVars(jobtype)}
                    {/* {jobStore.viewable && jobtype === 'ML_Workflow' &&
                    <ul className="inline-tabs tabs-blue pd-30 mr-0 inline-block">
                        <li className={`react-tabs__tab${currentIndex === 1 && "--selected"}`} onClick={()=>{this.props.setCurrentIndex({currentIndex: currentIndex ^ 1})}}>Run History</li>
                    </ul>} */}
                    {jobStore.running &&
                    <ul className="inline-tabs tabs-blue pd-30 mr-0 inline-block">
                        <li className={`react-tabs__tab${currentIndex === 3 && "--selected"}`} 
                        onClick={()=>{this.props.setCurrentIndex({currentIndex: 3})}}
                        >
                        Status
                        </li>
                        <li className={`react-tabs__tab${currentIndex === 2 && "--selected"}`} 
                        onClick={()=>{this.props.setCurrentIndex({currentIndex: 2})}}
                        >
                        Logs
                        </li>
                        <li className={`react-tabs__tab${currentIndex === 0 && "--selected"}`} 
                        onClick={()=>{this.props.setCurrentIndex({currentIndex: 0})}}
                        >
                        Spec
                        </li>
                        <li className={`react-tabs__tab${currentIndex === 4 && "--selected"}`} 
                        onClick={()=>{this.props.setCurrentIndex({currentIndex: 4})}}
                        >
                        Resource Usage
                        </li>
                    </ul>}
                    <div className="float-right">
                        <button className="mr-sm btn btn-sm btn-default" onClick={()=>{this.toggleMinimize()}}>
                            {Object.keys(dockStyle).length ? 
                             <i className="fa fa-expand" />
                            : <i className="fa fa-compress" />}
                        </button>
                        {jobtype !== "ML_Workflow" && 
                            <button className="btn btn-sm btn-danger" onClick={(event)=>{this.props.setActiveStep(event)}}>
                                <i className="fa fa-times" />
                            </button>
                        }
                    </div>
                </h4>
                <div className="mt-xxl pd-lr-lg pd-b mlr-lg">
                    {currentIndex === 0 &&
                        this.renderFormComponent(jobtype, jobIndex)
                    }
                    {/* {currentIndex === 1 &&
                        <JobsRunList id={jobStore.id} />
                    } */}
                    {currentIndex === 2 &&
                        <JobLogs token={this.props.token} expanded={expanded} name={jobStore.metadata.name} spec={jobStore.spec[jobtype]} index={parseInt(jobIndex, 10)} type={jobtype}/>
                    }
                    {currentIndex === 3 &&
                        <JobStatus jobStore={jobStore} spec={jobStore.spec[jobtype]} index={parseInt(jobIndex, 10)} type={jobtype} />
                    }
                    {currentIndex === 4 &&
                      <JobResource jobStore={jobStore} />
                    }
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        selectedSteps: state.jobStore.selectedSteps,
        currentStep: state.jobStore.currentStep,
        jobStore: state.jobStore.jobStore,
        token: state.auth.token
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            toggleSteps,
            setStep
        },
        dispatch
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(JobForm);