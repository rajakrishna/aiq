import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { getCurrentUser, updateCurrentUser, updatePassword } from "../api";
import { alertMsg } from "../ducks/alertsReducer";
import { Loader1 } from "./home/modelOverviewLoader";
import { connect } from "react-redux";
import Mixpanel from "mixpanel-browser";
import { getErrorMessage } from "../utils/helper";

class Profile extends Component {
  state = {
    currentUser: [],
    profileUpdated: "",
    errorMsg: "",
    warning: "",
    oldPass: "",
    newPass: "",
    rePass: "",
    strength: ["", ""],
    view: 1
  };

  componentDidMount() {
    Mixpanel.track("Profile Page");
    getCurrentUser(this.props.token)
      .then(response => {
        if (response.status) {
          const data = response.data;
          Mixpanel.people.set({
            $first_name: data.firstName,
            $last_name: data.lastName,
            $name: `${data.firstName} ${data.lastName}`,
            Enabled: data.enabled,
            Status: data.confirmedStatus,
            Tier: data.tier,
            Role: data.role
          });
          this.setState({ currentUser: response.data });
        }
      })
      .catch(error => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while fetching User Profile, please try again.";
        Mixpanel.track("Get User Profile Failed", { error: errorMessage });
        this.props.alertMsg(errorMessage, "error");
      });
  }

  handleChange = e => {
    const target = e.target;
    const name = target.name;
    const value = target.value;
    if (name === "rePass") {
      this.setState({
        warning: value !== this.state.newPass ? "Password do not match" : ""
      });
    }
    this.setState({
      [name]: value
    });
  };

  clearStates = (...names) => {
    let obj = {};
    names.map((e, i) => {
      obj = { ...obj, [e]: "" };
    });
    this.setState({ ...obj });
  };

  getInitialsEmail = (email = "") => {
    const first = email.replace(/^(\w).*/, "$1");
    const second = email.replace(/^\w.*?\.([A-z]).*?@.*?$/, "$1");
    const str = second.length > 1 ? first : first + second;
    return str.toUpperCase() || "?";
  };

  updateUser = () => {
    let data = {
      firstName: this.state.firstName.trim(),
      lastName: this.state.lastName.trim()
    };
    updateCurrentUser(this.props.token, data)
      .then(response => {
        if (response.status) {
          Mixpanel.track("Update User Profile Successful", data);
          Mixpanel.people.set({
            $first_name: data.firstName,
            $last_name: data.lastName,
            $name: `${data.firstName} ${data.lastName}`
          });
          this.setState({ profileUpdated: "Profile updated sucessfully" });
        }
      })
      .catch(error => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while updating User Profile, please try again.";
        Mixpanel.track("Update User Profile Failed", { error: errorMessage });
        this.setState({ errorMsg: errorMessage });
      });
  };

  updatePass = () => {
    if (this.state.warning) {
      this.props.alertMsg(this.state.warning, "warning");
      return 0;
    }
    this.props.alertMsg("Updating Password...", "info");
    let data = {
      userName: this.state.currentUser.userName,
      oldPassword: this.state.oldPass,
      newPassword: this.state.newPass
    };
    updatePassword(this.props.token, data)
      .then(response => {
        this.props.alertMsg("Updating Password...", "info", false);
        if (response.status) {
          Mixpanel.track("Update Password Successful");
          this.props.alertMsg("Password updated successfully", "success");
          this.clearStates("oldPass", "rePass", "newPass");
        }
      })
      .catch(error => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while updating Password, please try again.";
        Mixpanel.track("Update Password Failed", { error: errorMessage });
        this.props.alertMsg(errorMessage, "error");
        this.clearStates("oldPass", "rePass", "newPass");
      });
  };

  render() {
    const { currentUser, view, firstName, lastName } = this.state;
    return (
      <div className="main-container">
        {/* <NavigationLinks path={this.props.match.path} /> */}
        {currentUser.length !== 0 ? (
          <div className="main-body">
            <div className="content-wraper content-wraper-sm">
              {this.state.profileUpdated && (
                <div className="success-placement alert alert-success">
                  <i className="fa fa-exclamation-triangle" />
                  <span>{this.state.profileUpdated}</span>
                </div>
              )}
              {this.state.errorMsg && (
                <div className="error-placement alert alert-danger">
                  <i className="fa fa-exclamation-triangle" />
                  <span>{this.state.errorMsg}</span>
                </div>
              )}
              <div className="profile-form">
                <div className="row">
                  <div className="col-md-4">
                    <div className="profile-card">
                      <div className="profile-avatar">
                        <span>
                          {this.getInitialsEmail(currentUser.userName)}
                        </span>
                      </div>
                      <div className="profile-user-name">
                        <p>
                          {currentUser.firstName} {currentUser.lastName}
                        </p>
                      </div>
                    </div>
                    <div className="profile-card mt-lg">
                      <div className="text-center">
                        <div
                          className={`side-menu text-link ${
                            view === 1 ? "active" : ""
                          }`}
                          onClick={() => {
                            this.setState({ view: 1 });
                          }}
                        >
                          Personal Details
                        </div>
                        <div
                          className={`side-menu text-link ${
                            view === 2 ? "active" : ""
                          }`}
                          onClick={() => {
                            this.setState({ view: 2 });
                          }}
                        >
                          Change Password
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-8">
                    {this.state.view === 1 ? (
                      <div className="profile-form-box">
                        <div className="profile-form-box-head">
                          <h4>Personal Details</h4>
                        </div>
                        <div className="profile-form-box-body">
                          <div className="form-group profile-input">
                            <input
                              type="text"
                              name="firstName"
                              className="form-control"
                              value={firstName || currentUser.firstName}
                              onChange={this.handleChange}
                            />
                          </div>
                          <div className="form-group profile-input">
                            <input
                              type="text"
                              name="lastName"
                              className="form-control"
                              value={lastName || currentUser.lastName}
                              onChange={this.handleChange}
                            />
                          </div>
                          <div className="form-group profile-input">
                            <input
                              type="text"
                              className="form-control"
                              value={currentUser.userName}
                            />
                          </div>
                          <div className="form-group profile-input">
                            <input
                              type="text"
                              className="form-control"
                              value={currentUser.role}
                            />
                          </div>

                          <div className="profile-action">
                            <button
                              type="submit"
                              className="btn btn-primary"
                              onClick={this.updateUser}
                            >
                              Submit
                            </button>
                          </div>
                        </div>
                      </div>
                    ) : (
                      <div className="profile-form-box">
                        <div className="profile-form-box-head">
                          <h4>Change Password</h4>
                        </div>
                        <div className="profile-form-box-body">
                          <div className="form-group profile-input">
                            <input
                              type="password"
                              className="form-control"
                              placeholder="Old Password"
                              value={this.state.oldPass}
                              name="oldPass"
                              onChange={this.handleChange}
                            />
                          </div>
                          <div className="form-group profile-input data-text">
                            <input
                              type="password"
                              name="newPass"
                              className="form-control"
                              placeholder="New Password"
                              value={this.state.newPass}
                              onChange={this.handleChange}
                            />
                          </div>
                          <span className={`text-${this.state.strength[0]}`}>
                            {this.state.strength[1]}
                          </span>
                          <div className="form-group profile-input data-text mt-lg">
                            <input
                              type="password"
                              name="rePass"
                              className="form-control"
                              placeholder="Re-enter New Password"
                              value={this.state.rePass}
                              onChange={this.handleChange}
                            />
                          </div>
                          <span className="f-12 text-danger">
                            {this.state.warning}
                          </span>
                          <div className="profile-action">
                            <button
                              type="submit"
                              className="btn btn-primary mt-lg"
                              onClick={this.updatePass}
                            >
                              Submit
                            </button>
                          </div>
                        </div>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <Loader1 />
        )}
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      alertMsg
    },
    dispatch
  );
};

const mapStateToProps = state => {
  return {
    token: state.auth.token
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
