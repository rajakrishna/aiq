import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';
import { bindActionCreators } from 'redux';
import { alertMsg } from '../../ducks/alertsReducer';
import { getUsersList } from '../../ducks/users';
import { createProjectAction } from '../../ducks/projects';
import { getProjectsSummary } from '../../ducks/projects';
import { connect } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import Select from "react-select";
import { Tooltip} from "@material-ui/core";

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const styles = theme => ({
  paper: {
    position: 'absolute',
    minWidth: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    outline: 'none',
  },
});

class ProjectModalFullScreen extends React.Component {
  state = {
    open: false,
    name: "",
    description: "",
    userData: "",
    projectParams: {
        page: 0,
        nameContains: ""
      },
    formError:false
  };

  componentDidMount() {
      this.props.getUsersList();
  }

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleChange = (e) => {
    const target = e.target;
    const name = target.name;
    const value = target.value;
    this.setState({
        [name]: value
    })
}

cleavalues= () => {
    this.setState({name:"",description:"",userData:""})
}

addProjectApi = () => {
    const { name, description, userData } = this.state;
    const data = {
        name: name,
        description: description,
        users: userData.split(",")
    }
    this.props.createProjectAction(data, 
        ()=>{
            this.setState({
                loader: false
            });
            this.cleavalues()
            this.props.alertMsg("Project created successfully", "success");
            this.props.getProjectsSummary(this.state.projectParams);
            // MicroModal.close("project-modal");
            this.handleClose();
        },
        (err)=>{
            this.setState({
                loader: false,
                formError:true
            });
            this.props.alertMsg(err, "error");
        }
    )
}

addProject = () => {
    const tenantAdmin = this.props.users.filter((user)=> (user.role === "TenantAdmin"))
                                        .map((e) => e.userName)
    let adminUserNames = tenantAdmin.toString();
    let allUsers = this.state.userData
    if(!allUsers.includes(adminUserNames)){
        if (allUsers) {
            allUsers += ","
        }
        allUsers += adminUserNames
        let setOfUsers =[... new Set(allUsers.split(","))].toString()
       this.setState({...this.state, userData:setOfUsers}, function() {
            this.addProjectApi()
       })
    } else {
        this.addProjectApi()
    }
}

  render() {
    const { classes } = this.props;
    const { users } = this.props;
    const { loader, name, description, userData } = this.state;

    return (
      <div className='d-inline'>
        <span className={this.props.dropdown === "dropdown" ? "dropdown-item" : ""} onClick={this.handleOpen}>New Project</span>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
        >
          <div style={getModalStyle()} className={classes.paper}>
          <div>
              <div
                  className="modal__container">
                  <header className="modal__header">
                      <h2 className="modal__title">
                          Add Project
                      </h2>
                      <button
                          type="button"
                          className="modal__close"
                          aria-label="Close modal"
                          onClick={() => {this.cleavalues(), this.handleClose()}}
                          data-micromodal-close
                      />
                  </header>
                  <main className="modal__content">
                      <div className="modal-filter-form">
                          <div className="modal-body">
                              <div className="form-group">
                                  <label htmlFor="#name">Project Name: </label>
                                  <input
                                      id="name"
                                      name="name"
                                      className="form-control mt" 
                                      onChange={this.handleChange}
                                      value={name} 
                                  />
                                  <span className="f-12 text-gray no-select">
                                      Name must start with a lowercase letter followed by up to 49 lowercase letters, numbers, or hyphens, and cannot end with a hyphen
                                  </span>
                              </div>
                              <div className="form-group">
                                  <label htmlFor="#description">Description: </label>
                                  <textarea
                                      id="description"
                                      name="description"
                                      className="form-control mt"
                                      onChange={this.handleChange}
                                      value={description}
                                      row={4}
                                  ></textarea>
                              </div>
                              <div className="form-group">
                                  <label htmlFor="#userData">User: </label>
                                  <Select
                                      closeOnSelect={true}
                                      multi
                                      onChange={e => this.setState({ userData: e })}
                                      options={users.map(e => {
                                          return ({
                                              label: e.firstName + " " + e.lastName,
                                              value: e.userName
                                          })}
                                      )}
                                      placeholder="Select User"
                                      removeSelected={true}
                                      simpleValue
                                      value={userData}
                                  />
                              </div>
                              {this.state.formError ? (
          <div className="row mt-15">
            <div className="col-sm-12">
              <Tooltip title="Double click to dismiss">
                <div
                  className="alert alert-danger f-13"
                  onDoubleClick={() => {
                    this.setState({
                      formError: false,
                    });
                  }}
                >
                  <strong>Error: </strong>
                  An error occurred while creating the Project, please try
                  again.
                </div>
              </Tooltip>
            </div>
          </div>
        ) : (
          <div></div>
        )}
                          </div>
                          <div className="modal-footer">
                          {loader && <CircularProgress size={20} disableShrink />}
                          <button
                              onClick={()=> {this.cleavalues(), this.handleClose()}}
                              type="button"
                              className="btn btn-secondary"
                          >
                              Close
                          </button>
                          <button type="button" className="btn btn-primary" onClick={()=>{this.setState({loader: true},this.addProject)}}>
                              Create Project
                          </button>
                          </div>
                      </div>
                  </main>
              </div>
          </div>
          </div>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token,
    projects: state.projects.projects,
    users: state.users.users
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
      {
          alertMsg,
          getUsersList,
          createProjectAction,
          getProjectsSummary
      },
      dispatch
  );
};

ProjectModalFullScreen.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ProjectModalFullScreen));