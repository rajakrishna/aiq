import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';
import { bindActionCreators } from 'redux';
import { alertMsg } from '../../ducks/alertsReducer';
import { getUsersList } from '../../ducks/users';
import { createProjectAction } from '../../ducks/projects';
import { getProjectsSummary } from '../../ducks/projects';
import { connect } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import Select from "react-select";
import { Tooltip} from "@material-ui/core";

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
    maxHeight: '80%',
    overflowY: 'scroll'
  };
}

const styles = theme => ({
  paper: {
    position: 'absolute',
    minWidth: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    outline: 'none',
  },
  tableOverflow: {
    maxHeight: '80%',
    overflowY: 'scroll'
  }
});

class PredictionInputsModalFullScreen extends React.Component {
  state = {
    open: false,
    name: "",
    description: "",
    userData: "",
    projectParams: {
        page: 0,
        nameContains: ""
      },
    formError:false
  };

  componentDidMount() {
      this.props.getUsersList();
  }

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes } = this.props;
    const { users } = this.props;
    const { loader, name, description, userData} = this.state;

    return (
      <div className='d-inline'>
        <button
          className="btn btn-primary btn-sm btn-rounded"
          onClick={this.handleOpen}
        >
          View Inputs
        </button>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
        >
          <div style={getModalStyle()} className={classes.paper}>
          <div>
          <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Prediction Inputs</h5>
                <button
                  type="button"
                  className="close"
                  onClick={this.handleClose}
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

              <table className="table table-bordered data-table insights-table">
                <thead>
                  <tr>
                    <td>Name</td>
                    <td>Value</td>
                  </tr>
                </thead>
                <tbody>
                    { this.props.predictionInputsList && this.props.predictionInputsList.map((input,index) => {
                      return(
                        <tr key={index}>
                          <td>{input.name}</td>
                          <td>{input.value}</td>
                        </tr>
                      )
                    })}
                </tbody>
              </table>
              
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  onClick={this.handleClose}
                >
                  Close
                </button>
              </div>
            </div>
          </div>
          </div>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token,
    projects: state.projects.projects,
    users: state.users.users
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
      {
          alertMsg,
          getUsersList,
          createProjectAction,
          getProjectsSummary
      },
      dispatch
  );
};

PredictionInputsModalFullScreen.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(PredictionInputsModalFullScreen));