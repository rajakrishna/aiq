import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import { bindActionCreators } from 'redux';
import { alertMsg } from '../../ducks/alertsReducer';
import { createSecretsAction } from '../../ducks/Secrets';
import { fetchSecrets } from '../../ducks/jobStore';
import { connect } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import { isDNS1035Label, isSecretKey } from '../../utils/validators';

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const styles = theme => ({
  paper: {
    position: 'absolute',
    minWidth: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    outline: 'none',
  },
});

class SecretModalFullScreen extends React.Component {
    // secretModalFunctions Starts
    state = {
        name: "",
        secretNameError: "",
        project: "",
        secretObj: [{name:"username",value:"",label:"Username"},{name:"password",value:"",label:"Password"}],
        secretErrorObject: [{nameError: '', valueError: '', lableError: ''}, {nameError: '', valueError: '', lableError: ''}],
        loader: false,
        secretTypes:[{name:"Git",value:""},{name:"AWS",value:""},{name:"AIQ",value:""},{name:"Generic",value:""}],
        secretType:"Git"
        
    }

    componentDidMount() {
      this.setEmptyValues();
    }

    setEmptyValues = () =>{
        this.setState({
        name: "",
        project: "",
        secretObj: [{name:"username",value:"",label:"Username"},{name:"password",value:"",label:"Password"}],
        secretErrorObject: [{nameError:"", valueError: "", lableError: ""}, {nameError: "", valueError: "", lableError: ""}],
        loader: false,
        secretTypes:[{name:"Git",value:""},{name:"AWS",value:""},{name:"AIQ",value:""},{name:"Generic",value:""}],
        secretType:"Git"
        })
    }

    addInp = () => {
        let arr = [...this.state.secretObj];
        let arrError = [...this.state.secretErrorObject];
        arr.push({name:``,value:``, label:``});
        arrError.push({nameError: '', valueError: '', lableError: ''});
        this.setState({secretObj: arr, secretErrorObject: arrError});
    }

    delInp = (i) => {
        let arr = [...this.state.secretObj];
        let arrError = [...this.state.secretErrorObject];
        if(arr.length > 1) {
            arr.splice(i,1);
            arrError.splice(i, 1);
            this.setState({secretObj: arr, secretErrorObject: arrError});
        }
    }
    
    handleObj = (value, type, index)=> {
        let secretObj = [...this.state.secretObj]
        secretObj[index][type] = value;
        this.setState({
            secretObj
        })
    }

    handleObjError = (value, type, index)=> {
        let secretErrorObject = [...this.state.secretErrorObject];
        if (type === 'nameError') {
            const validationResult = isSecretKey(value);
            validationResult.isValid
              ? (secretErrorObject[index][type] = "")
              : (secretErrorObject[index][type] = `Key ${validationResult.error}`);
        } else {
            secretErrorObject[index][type]
        }
        this.setState({
            secretErrorObject
        })
    }
    
    getObj = (arr) => {
        const newArr = arr.map((e,i)=>{
            if(e.name && e.value)
                return `"${e.name}":"${e.value}"`;
            else {
                return null;
            }
        });
        const filterArr = newArr.filter(e=>{
            return e!=null
        });

        let obj = {};
        try{
           obj = JSON.parse("{"+filterArr.join(",")+"}");
        } catch(e) {
        //    this.setState({valid: false});
        }
        return obj;
    }
    handleSecreType = (ev) => {
        if( ev.target.value =='Git')
        {
           let arr = [{name:"username",value:"",label:"Username"},{name:"password",value:"",label:"Password"}]
           let arrError = [{nameError:"",valueError:"",lableError:""},{nameError:"",valueError:"",lableError:""}]
           this.setState({secretType:ev.target.value,secretObj:arr, secretErrorObject: arrError})
   
        }else if(ev.target.value =='AWS'){
           let arr = [{name:"AWS_ACCESS_KEY_ID",value:"",label:"Access Key Id"},{name:"AWS_SECRET_ACCESS_KEY",value:"",label:"Secret Access Key"}]
           let arrError = [{nameError:"",valueError:"",lableError:""},{nameError:"",valueError:"",lableError:""}]
           this.setState({secretType:ev.target.value,secretObj:arr, secretErrorObject: arrError})
        }else if(ev.target.value =='AIQ'){
           let arr = [{name:"AIQ_USERNAME",value:"",label:"Username"},{name:"AIQ_PASSWORD",value:"",label:"Password"}];
           let arrError = [{nameError:"",valueError:"",lableError:""},{nameError:"",valueError:"",lableError:""}]
           this.setState({secretType:ev.target.value,secretObj:arr, secretErrorObject: arrError})
        }else if(ev.target.value =='Generic'){
           let arr = [{name:"",value:"",label:""},{name:"",value:"",label:""}]
           let arrError = [{nameError:"",valueError:"",lableError:""},{nameError:"",valueError:"",lableError:""}]
           this.setState({secretType:ev.target.value,secretObj:arr, secretErrorObject: arrError})
        }
    }

    handleSecretName = (event) => {
        const validationResult = isDNS1035Label(event.target.value);
        validationResult.isValid
            ? this.setState({ secretNameError: "" })
            : this.setState({ secretNameError: `Name ${validationResult.error}` });
        this.setState({name: event.target.value})
    }
    addSecret = () => {

        let data = {};
        data = {
            data: this.getObj(this.state.secretObj),
            name: this.state.name,
            project_id: this.props.selectedProject.id,
            project_name: this.props.selectedProject.name
        }
        this.props.createSecretsAction(data, 
            () => {
                this.setState({loader: false});
                this.props.fetchSecrets({
                    project_id: this.props.selectedProject.id
                });
                this.setEmptyValues()
                this.props.alertMsg("Secret created successfully","success");
                this.handleClose;
                },
            (err) => {
                this.setState({loader: false});
                this.props.alertMsg(err, "error");
            }
        );
    }

// secretModalFunctions Ends

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes } = this.props;
    const { loader,secretTypes, secretErrorObject, secretNameError } = this.state;

    return (
      <div className='d-inline'>
        {this.props.secretModelParent === "secretsTab" ? <button
            type="button"
            data-toggle="modal"
            className="btn btn-sm btn-primary"
            onClick={this.handleOpen}
          >
            Add New Secret
          </button>
          :
            <span
                className={`dropdown-item clickable ${!this.props.selectedProject ? "disabled":""}`}
                data-tip data-for="no_projects_secret"
                onClick={() => {
                    if(this.props.selectedProject)
                    this.handleOpen();
                }}
                >
                New Secret
            </span>
        }
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
        >
          <div style={getModalStyle()} className={classes.paper}>
          <div
        >
            <div>
                <div
                    className="modal-container"
                >
                    <header className="modal__header">
                        <h2 className="modal__title">
                            Add Secrets
                        </h2>
                        <button
                            type="button"
                            className="modal__close"
                            onClick={()=> {this.setEmptyValues(), this.handleClose()} }
                        />
                    </header>
                    <main className="modal__content">
                        <div className="modal-filter-form">
                            <div className="modal-body">
                            <div className="form-group">
                                <label htmlFor="#secretType">Select Secret Type: </label>
                                <select id="secretType" name="secretType" className="form-control mt" onChange={(e)=>{this.handleSecreType(e)}} value={this.state.secretType}>
                                    <option value="">Select Secret Type</option>
                                    {
                                        secretTypes.map((e,i)=>{
                                            return (
                                            <option key={"secretTYpe"+i} value={e.name}>{e.name}</option>)
                                        })
                                    }
                                </select>
                            </div>
                            <div className="form-group">
                                <label htmlFor="#secret_name">Secret Name: </label>
                                <input className="form-control mt" onChange={(event)=>{this.handleSecretName(event)}} value={this.state.name}/>
                                {secretNameError && <div>
                                    <p className='f-12 text-danger mb-0 mt-1'>{`${secretNameError}`}</p>
                                    </div>
                                    }
                            </div>
                            <div className="form-group add-secrets-data-items">
                                <label>Data: </label>
                                <button
                                    type="button"
                                    className="btn btn-default btn-sm float-right"
                                    disabled={this.state.secretType!='Generic'}
                                    onClick={this.addInp}
                                >
                                    <i className="fa fa-plus"></i>
                                </button>
                                {this.state.secretObj.map((e,i)=>{
                                    return(
                                        <div className="d-flex flex-row align-items-top mt-3" key={"secret"+i}>
                                            <div className="col-sm-5">
                                                <input id={"addnameNew"+i} type="text" disabled={this.state.secretType!='Generic'} placeholder={`key`} onChange={(e)=>{this.handleObj(e.target.value, "name", i); this.handleObjError(e.target.value, "nameError", i)}} className="form-control" value={e.name} />
                                                {secretErrorObject[i] && secretErrorObject[i]['nameError'] && <div>
                                                    <p className='f-12 text-danger mb-0 mt-1'>{`${secretErrorObject[i]['nameError']}`}</p>
                                                    </div>
                                                }
                                            </div>
                                            <div className="col-sm-5">
                                                <input 
                                                    id={"addvalueNew"+i} 
                                                    style={{ paddingRight: 30 }} 
                                                    type="password" 
                                                    placeholder="Value" 
                                                    onChange={(e)=>{this.handleObj(e.target.value, "value", i); this.handleObjError(e.target.value, "valueError", i)}}
                                                    className="form-control" 
                                                    value={e.value} 
                                                />
                                                <i 
                                                    className="fa fa-eye eye clickable"
                                                    id={"addEyeNew"+i}
                                                    onMouseDown={(elem)=>{
                                                        document.querySelector("#addvalueNew"+i).type = "text";
                                                        document.querySelector("#addEyeNew"+i).className += "text-link"
                                                    }}
                                                    onMouseUp={()=>{
                                                        document.querySelector("#addvalueNew"+i).type = "password"
                                                        document.querySelector("#addEyeNew"+i).className = document.querySelector("#addEyeNew"+i).className.replace("text-link","");
                                                    }}
                                                    onMouseOut={()=>{
                                                        document.querySelector("#addvalueNew"+i).type = "password";
                                                        document.querySelector("#addEyeNew"+i).className = document.querySelector("#addEyeNew"+i).className.replace("text-link","");
                                                    }}
                                                />
                                                {secretErrorObject[i] && secretErrorObject[i]['valueError'] && <p className='text-danger'>{`*${secretErrorObject[i]['valueError']}`}</p>}
                                            </div>
                                            
                                            <div className="col-sm-2">
                                                <button 
                                                    disabled={this.state.secretType!='Generic'}
                                                    type="button"
                                                    className="btn btn-sm btn-danger float-right mt"
                                                    onClick={()=>{this.delInp(i)}}
                                                >
                                                    <i className="fa fa-trash"></i>
                                                </button>
                                            </div>
                                
                                        </div>
                                    )
                                })}
                            </div>
                            </div>
                            <div className="modal-footer">
                            {loader && <CircularProgress size={20} disableShrink />}
                            <button
                                onClick={()=> {this.setEmptyValues(), this.handleClose()} }
                                type="button"
                                className="btn btn-secondary"
                            >
                                Close
                            </button>
                            <button type="button" className="btn btn-primary" onClick={this.addSecret}>
                                Save
                            </button>
                            </div>
                        </div>
                    </main>
                </div>
            </div>
        </div>
          </div>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => {
    return {
      token: state.auth.token,
      projects: state.projects.projects,
      selectedProject: state.projects.globalProject
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            alertMsg,
            fetchSecrets,
            createSecretsAction
        },
        dispatch
    );
};

SecretModalFullScreen.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(SecretModalFullScreen));