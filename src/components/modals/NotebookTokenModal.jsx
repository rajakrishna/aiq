import React from 'react';
import PropTypes from 'prop-types';
import Modal from '@material-ui/core/Modal';
import { connect } from 'react-redux';
import jwt_decode from "jwt-decode";
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import { getToken } from '../../api';
import { Tooltip } from '@material-ui/core';
import CircularProgress from "@material-ui/core/CircularProgress";
import { Copy } from "../Copy";

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const styles = theme => ({
  paper: {
    position: 'absolute',
    minWidth: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    outline: 'none',
  },
});

class NotebookTokenModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = { open: false, loader: false}
    }

    componentDidMount() {
        var decoded = jwt_decode(this.props.auth.token);
       this.props.history.listen(() => {
         if ((decoded.exp * 1000) < Date.now()) {
           localStorage.clear();
           this.handleOpen();
         }
       });
      }
      
    getToken = (notebook) => {
        this.setState({ loader: true });
        getToken(this.props.token, notebook.name)
          .then((res) => {
            this.setState({ tokenId: res.data.token });
            this.setState({ loader: false });
          })
          .catch((error) => {
            this.setState({ tokenError: true, loader: false });
          });
      };
    

    handleExpiry =()=>{
      window.location.reload(true);
     }

    handleOpen = () => {
      this.setState({ open: true });
    };

    handleClose = () => {
      this.setState({ open: false });
    };

    render() {
      const { classes } = this.props;
      const {loader} = this.state;

    return (
      <div className='d-inline'>
        <Tooltip title="token">
          <i
          className="fa fa-key ml clickable"
          onClick={() => {
            this.handleOpen(); this.getToken(this.props.notebookObj)
          }}  ></i>
        </Tooltip>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
        >
          <div style={getModalStyle()} className={classes.paper}>
          <div
            className="modal__container p-0"
            role="dialog"
            aria-modal="true"
            aria-labelledby="secret-modal-title"
          >
            <header className="modal__header">
              <h2 className="modal__title ml-15">Notebook Token</h2>
              <button
                type="button"
                className="modal__close"
                aria-label="Close modal"
                data-micromodal-close
                onClick={this.handleClose}
              />
            </header>

            <main className="modal__content mt-0 mb-0 h120">
              {loader ? (
                <div className="modal-filter-form">
                  <div className="modal-body f-13 ml-15 mt-30 mr">
                    <CircularProgress size={20} disableShrink />
                  </div>
                </div>
              ) : (
                <div>
                  {this.state.tokenError ? (
                    <div className="modal-filter-form">
                      <div className="modal-body alert alert-danger f-13 ml-15 mt-30 mr">
                        {/* onClick={this.getToken(this.notebook)} */}
                        <strong>Error: </strong>
                        An error occurred while fetching the Token, please try again.
                      </div>
                      <div className="ml-15 mt-30">
                        <button
                          onClick={() => {
                            this.getToken(this.notebook);
                          }}
                          type="button"
                          className="btn btn-primary"
                        >
                          Retry
                        </button>
                        <button
                          onClick={this.handleClose}
                          type="button"
                          className="btn btn-secondary ml-15"
                        >
                          Close
                        </button>
                      </div>{" "}
                    </div>
                  ) : (
                    <div className="modal-filter-form">
                      <div className="modal-body f-13">
                        <Copy className="pd-0">{`${this.state.tokenId}`}</Copy>

                        <div className="text-gray mt-lg">
                          You will be asked to provide a token when connecting to a
                          Notebook. Please use the above token when prompted
                        </div>
                      </div>
                      <div className="ml-15 mt-10">
                        {loader && <CircularProgress size={20} disableShrink />}

                        <button
                          onClick={this.handleClose}
                          type="button"
                          className="btn btn-primary"
                        >
                          Close
                        </button>
                      </div>
                    </div>
                  )}
                </div>
              )}
            </main>
          </div>
          </div>
        </Modal>
      </div>
    );
    }
  }

  const mapStateToProps = state => {
      return {
          auth: state.auth,
          token: state.auth.token
      };
  };

  NotebookTokenModal.propTypes = {
    classes: PropTypes.object.isRequired,
  };


  export default withRouter(connect(mapStateToProps)(withStyles(styles)(NotebookTokenModal)));