import React from 'react';
import PropTypes from 'prop-types';
import Modal from '@material-ui/core/Modal';
import { connect } from 'react-redux';
import { getNotebookList, getToken,updateNotebook, getCurrentUser } from "../../api";
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import { Tooltip } from '@material-ui/core';
import CircularProgress from "@material-ui/core/CircularProgress";
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const styles = theme => ({
  paper: {
    position: 'absolute',
    minWidth: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    outline: 'none',
  },
});

class EditNotebookModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            open: false, 
            loader: false,
            loading: false,
            showForm: false,
            tokenId:'',
            notebooks: [],
            notebook:{},
            tokenError:false,
            loader:false,
            cpuWarning:"",
            memWarning:""
        }
    }

      componentDidMount() {
        this.fetchNotebooks();
        this.setState({notebook: this.props.notebookObj})
      }

      componentDidUpdate(prevProps) {
        if (this.props.selectedProject && prevProps.selectedProject.id !==  this.props.selectedProject.id) {
          this.fetchNotebooks();
        }
      }

      updateNotebook =()=>{
        const gpuValue = (this.state.notebook.gpu == 0) ? {} : {"nvidia.com/gpu":this.state.notebook.gpu}
        const updateObj = {
          "cpu":this.state.notebook.cpu,
          "memory":this.state.notebook.mem,
          "extraResources": gpuValue
        }
        updateNotebook(this.props.token,this.props.selectedProject && this.props.selectedProject.id,
          this.state.notebook.name,updateObj).then((res)=>{
              console.log("res.status::",res.status,"res.data::",res.data)
              this.handleClose();
          })
          .catch((err)=>{
            console.log("error while notebook Update:",err.message)
          })
    
      }

      fetchNotebooks = () => {
        this.setState({
          loading: true,
        });
        getNotebookList(this.props.token,this.props.selectedProject && this.props.selectedProject.id)
          .then((res) => {
            this.setState({
              notebooks: res.data.notebooks,
              loading: false,
            });
          })
          .catch((err) => {
            this.setState({
              loading: false,
            });
          });
      };

    handleChange =(e)=>{
        const target = e.target;
        let name = target.name;
        let value = (name === "mem") ? target.value+"Gi" : target.value;
        this.setState({
            ...this.state,notebook: {...this.state.notebook, [name]: value}
        })
    }

    handleEdit =(name) =>{
        const editNotebook = this.state.notebooks.find((item) => item.name === name );
        this.setState({notebook:editNotebook});
        this.setState({cpuWarning:""});
        this.setState({memWarning:""});
    }
      
    getToken = (notebook) => {
        this.setState({ loader: true });
        getToken(this.props.token, notebook.name)
          .then((res) => {
            this.setState({ tokenId: res.data.token });
            this.setState({ loader: false });
          })
          .catch((error) => {
            this.setState({ tokenError: true, loader: false });
          });
      };
    

    handleExpiry =()=>{
      window.location.reload(true);
     }

    handleOpen = () => {
      this.setState({ open: true });
    };

    handleClose = () => {
      this.setState({ open: false });
    };

    render() {
      const { classes } = this.props;
      const {loader} = this.state;
    return (
      <div className='d-inline'>
        <Tooltip title="Edit">
            <i 
            className="fas fa-edit ml clickable"
            data-toggle="modal"
            data-target="#update_notebook"
            onClick={()=>{ this.handleOpen(); this.handleEdit(this.props.notebookObj)}}
            ></i>
            </Tooltip>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
        >
          <div style={getModalStyle()} className={classes.paper}>
          <div
            className="modal__container"
            role="dialog"
            aria-modal="true"
            aria-labelledby="project-modal-title"
          >
            <header className="modal__header">
              <h2 className="modal__title">{`Update Notebook : ${this.props.notebookObj.name}`}</h2>
              <button
                type="button"
                className="modal__close"
                aria-label="Close modal"
                data-micromodal-close
                onClick={this.handleClose}
              />
            </header>
            <main className="modal__content">
              <h5 className="mb-0">Resources</h5>
             <form>
              <div className="modal-filter-form">
                <div className="modal-body">
                 <div className="mt-30">
                  <FormControl fullWidth error={(this.props.user.tier === 'Standard Tier') && this.state.cpuWarning}>
                    <InputLabel htmlFor="notebook_cpu">CPU</InputLabel>
                    <Input
                       id={"notebook_cpu"}
                       name={"cpu"}
                      value={this.state.notebook ? this.state.notebook.cpu : this.props.notebookObj.cpu}
                      onBlur={this.handleBlurCpu}
                      onFocus={()=>this.setState({cpuWarning:""})}
                      onChange ={this.handleChange}
                      inputProps={{ min: 0.1, step: 0.1 }}
                      type="number"
                      aria-describedby="component-cpu-error-text"
                    />
                     <FormHelperText id="component-cpu-error-text">
                      {
                        (this.props.user.tier === 'Standard Tier') && this.state.cpuWarning ? this.state.cpuWarning:"vCPU/Core"
                      }
                      </FormHelperText>
                  </FormControl>
                 </div>
                 <div className="mt-30">
                    <FormControl fullWidth error={(this.props.user.tier === 'Standard Tier') && this.state.memWarning }>
                      <InputLabel htmlFor="notebook_memory">Memory</InputLabel>
                        <Input
                          id={"notebook_memory"}
                          name={"mem"}
                          inputProps={{ min: 0.5, step: 0.5 }}
                          type="number"
                          value={this.state.notebook && this.state.notebook.mem ? this.state.notebook.mem.replace("Gi","") : this.props.notebookObj.mem.replace("Gi", "")}
                          onChange = {this.handleChange}
                          onBlur={this.handleBlurMem}
                          onFocus={()=>this.setState({memWarning:""})}
                          aria-describedby="component-mem-error-text"
                        />
                      <FormHelperText id="component-mem-error-text">
                        {
                          (this.props.user.tier === 'Standard Tier') && this.state.memWarning ? this.state.memWarning : "Gi"
                        }
                      </FormHelperText>
                    </FormControl>
                    </div>
                    <div className="mt-30">
                    <FormControl   fullWidth  disabled = {this.props.user.tier === 'Standard Tier'}>
                      <InputLabel htmlFor="notebook_gpu">GPU</InputLabel>
                      <Input
                       id="notebook_gpu"
                       name={"gpu"} 
                       inputProps={{ min: 0, step: 1 }}
                       type="number"
                       value={this.state.notebook && this.state.notebook.gpu && this.state.notebook.gpu || 0}
                       onChange = {this.handleChange} 
                       />
                      <FormHelperText>
                        {
                          (this.props.user.tier === 'Standard Tier') ? "Note: for current subscription you are not allowed to change GPU" : ""
                        }
                      </FormHelperText>
                    </FormControl>
                   </div>   
                  </div>
                
                <div className="modal-footer">
                  {loader && <CircularProgress size={20} disableShrink />}
                  <button
                    onClick={this.handleClose}
                    type="button"
                    className="btn btn-secondary"
                  >
                    Cancel
                  </button>
                  <button
                    type="button"
                    className={`btn btn-primary clickable ${(this.state.cpuWarning || this.state.memWarning) ? "disabled":""}`}
                    onClick={this.updateNotebook}
                    disabled={this.state.cpuWarning || this.state.memWarning}
                  >
                    Update 
                  </button>
                </div>
              </div>
            </form> 
            </main>
          </div>
          </div>
        </Modal>
      </div>
    );
    }
  }

  const mapStateToProps = state => {
      return {
          auth: state.auth,
          token: state.auth.token,
          user: state.users.user
      };
  };

  EditNotebookModal.propTypes = {
    classes: PropTypes.object.isRequired,
  };


  export default withRouter(connect(mapStateToProps)(withStyles(styles)(EditNotebookModal)));