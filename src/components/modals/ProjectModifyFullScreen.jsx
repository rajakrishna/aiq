import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import { bindActionCreators } from 'redux';
import { alertMsg } from '../../ducks/alertsReducer';
import { getUsersList } from '../../ducks/users';
import { createProjectAction } from '../../ducks/projects';
import { getProjectsSummary } from '../../ducks/projects';
import { getProjectsList } from '../../ducks/projects';
import { globalProjectSelection } from '../../ducks/projects';
import { connect } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import { updateProject } from '../../api';

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const styles = theme => ({
  paper: {
    position: 'absolute',
    minWidth: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    outline: 'none',
  },
});

class ProjectModifyFullScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      name: this.props.project.name,
      description: this.props.project.description,
      userData: "",
      projectParams: {
        page: 0,
        nameContains: "",
      },
    };
  }

  componentDidUpdate = (prevstate) => {
    if (prevstate.selectedProject.id !== this.props.selectedProject.id) {
      this.setState({
        name: this.props.selectedProject.name,
        description: this.props.selectedProject.description,
      });
    }
  };

  handleChange = (e) => {
    const target = e.target;
    const name = target.name;
    const value = target.value;
    this.setState({
      [name]: value,
    });
  };

  editProject = () => {
    const { name, description, userData } = this.state;
    this.props.project.name = name;
    this.props.project.description = description;
    updateProject(this.props.token, this.props.project)
      .then((res) => {
        this.props.alertMsg("Project updated successfully", "success");
        // this.props.getProjectsSummary(this.state.projectParams);
        this.props.getProjectsList({ size: 500 });
        this.handleClose();
      })
      .catch((err) => {
        this.props.alertMsg(err.response.data.title, "error");
      });
  };

  componentDidMount() {
      this.props.getUsersList();
  }

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes } = this.props;
    const { loader, name, description, userData } = this.state;

    return (
      <div className='d-inline'>
        <button 
          className="btn btn-default btn-sm mr"
          onClick={this.handleOpen}                
          >
            <i className="fa fa-edit"></i>
        </button>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
        >
          <div style={getModalStyle()} className={classes.paper}>
          <div>
              {/* Update Modal Content Starts */}
                <div>
                  <div
                    className="modal-container"
                    role="dialog"
                    aria-modal="true"
                    aria-labelledby="project-modal-title"
                  >
                    <header className="modal__header">
                      <h2 className="modal__title">update Project</h2>
                      <button
                        type="button"
                        className="modal__close"
                        onClick={this.handleClose}
                      />
                    </header>
                    <main className="modal__content">
                      <div className="modal-filter-form">
                        <div className="modal-body">
                          <div className="form-group">
                            <label htmlFor="#name">Project Name: </label>
                            <input
                              id="name"
                              name="name"
                              className="form-control mt"
                              onChange={this.handleChange}
                              value={this.state.name}
                            />
                            <span className="f-12 text-gray no-select">
                              Name must start with a lowercase letter followed by up to
                              49 lowercase letters, numbers, or hyphens, and cannot end
                              with a hyphen
                            </span>
                          </div>
                          <div className="form-group">
                            <label htmlFor="#description">Description: </label>
                            <textarea
                              id="description"
                              name="description"
                              className="form-control mt"
                              onChange={this.handleChange}
                              value={this.state.description}
                              row={4}
                            ></textarea>
                          </div>
                        </div>
                        <div className="modal-footer">
                          {loader && <CircularProgress size={20} disableShrink />}
                          <button
                            onClick={this.handleClose}
                            type="button"
                            className="btn btn-secondary"
                          >
                            Cancel
                          </button>
                          <button
                            type="button"
                            className="btn btn-primary"
                            onClick={() => {
                              this.setState({ loader: true }, this.editProject);
                            }}
                          >
                            Update Project
                          </button>
                        </div>
                      </div>
                    </main>
                  </div>
                </div>
              {/* Update Modal Content Ends */}
          </div>
          </div>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token,
    projects: state.projects.projects,
    users: state.users.users,
    selectedProject: state.projects.globalProject,
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
      {
          alertMsg,
          getUsersList,
          createProjectAction,
          getProjectsSummary,
          alertMsg,
          getProjectsList,
          globalProjectSelection,
      },
      dispatch
  );
};

ProjectModifyFullScreen.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ProjectModifyFullScreen));