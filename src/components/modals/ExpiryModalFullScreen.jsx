import React from 'react';
import PropTypes from 'prop-types';
import Modal from '@material-ui/core/Modal';
import { connect } from 'react-redux';
import jwt_decode from "jwt-decode";
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const styles = theme => ({
  paper: {
    position: 'absolute',
    minWidth: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    outline: 'none',
  },
});

class ExpiryModalFullScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = { }
    }

    componentDidMount() {
        var decoded = jwt_decode(this.props.auth.token);
       this.props.history.listen(() => {
         if ((decoded.exp * 1000) < Date.now()) {
           localStorage.clear();
           this.handleOpen();
         }
       });
      }      
    

    handleExpiry =()=>{
      window.location.reload(true);
     }

    handleOpen = () => {
      this.setState({ open: true });
    };

    handleClose = () => {
      this.setState({ open: false });
    };

    render() {
      const { classes } = this.props;

    return (
      <div className='d-inline'>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
        >
          <div style={getModalStyle()} className={classes.paper}>
            <div
            className="modal-container"
            role="dialog"
            aria-modal="true"
            aria-labelledby="modal_expiry-title"
            >
            <main className="modal__content" id="modal_expiry-content">
                <p>
                Your session has expired. Please sign in again.
                </p>
            </main>
            <footer className="modal__footer">
                <button
                className="modal__btn float-right"
                data-micromodal-close
                aria-label="Close this dialog window"
                style={{outline:"none"}}
                onClick={this.handleExpiry}
                >
                OK
                </button>
                <div className="clearfix"></div>
            </footer>
            </div>
          </div>
        </Modal>
      </div>
    );
    }
  }

  const mapStateToProps = state => {
      return {
          auth: state.auth
      };
  };

  ExpiryModalFullScreen.propTypes = {
    classes: PropTypes.object.isRequired,
  };


  export default withRouter(connect(mapStateToProps)(withStyles(styles)(ExpiryModalFullScreen)));