import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import "react-select/dist/react-select.css";
import Monitoring from "./Monitoring";
import ModelCatalog from "./ModelCatalog";
import DeploymentsList from "./deploymentsList";
import Secrets from "./Secrets";
import ExperimentsList from "./experimentsList";
import { getProject, getAllUsers, updateProject } from "../api";
import { filter, indexOf, uniq } from "lodash";
import MicroModal from "micromodal";
import User from "./shared/User";
import { alertMsg } from "../ducks/alertsReducer";
import Mixpanel from "mixpanel-browser";
import { getErrorMessage } from "../utils/helper";
import Notebooks from "./Notebooks";
import JobsList from "./JobsList";
import { globalProjectSelection } from "../ducks/projects";
import ManageTeam from "./ManageTeam";

const pathname = [
  "",
  "notebooks",
  "experiments",
  "models",
  "deployments",
  "monitored",
  "workflows",
  "manage-team",
  "secrets",
];

class ProjectShow extends Component {
  project = "";
  projectOwner = "";
  allUsers = "";
  state = {
    run: false,
    filter: {
      status: "",
    },
    allUsers: [],
    selectionUsers: [],
    selectedUser: null,
    projectOwner: "",
    projectUsers: [],
    currentUrl: "",
    currentIndex: 1,
    project: "",
    open: false,
    labelWidth: 50,
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const url = nextProps.location.pathname;
    if (url !== prevState.currentUrl) {
      let index = pathname.findIndex((e, i) => {
        if (url.includes(e)) return i;
        return null;
      });
      if (index < 1) {
        index = 0;
      } else {
        index--;
      }
      return {
        currentUrl: url,
        currentIndex: index,
      };
    }
    return {
      run: nextProps.history.location.state
        ? nextProps.history.location.state.tutorial
        : false,
    };
  }

  async componentDidMount() {
    Mixpanel.track("Project Details Page", {
      project_id: this.props.match.params.id,
    });
    MicroModal.init();
    await getProject(this.props.token, this.props.match.params.id)
      .then((response) => {
        if (response.status) {
          this.project = response.data;
          this.projectOwner = response.data.owner;
          this.props.globalProjectSelection(this.project);
        }
      })
      .catch((error) => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while fetching Project details, please try again.";
        Mixpanel.track("Get Project Failed", {
          project_id: this.props.match.params.id,
          error: errorMessage,
        });
        this.props.alertMsg(errorMessage, "error");
      });

    getAllUsers(this.props.token).then((response) => {
      if (response.status) {
        this.allUsers = response.data;
        this.setProjectUsersState(this.project);
      }
    });
  }

  setProjectUsersState = (project) => {
    var projectUserEmails = project.users || [];
    var allUsers = this.allUsers;
    projectUserEmails.push(project.owner);

    const selectionUsers = filter(allUsers, function(u) {
      return indexOf(projectUserEmails, u.userName) < 0;
    });

    const projectUsers = filter(allUsers, function(u) {
      return indexOf(projectUserEmails, u.userName) >= 0;
    });
    this.setState({
      selectionUsers: selectionUsers,
      projectUsers: projectUsers,
      allUsers,
      project,
      projectOwner: this.projectOwner,
    });
  };

  removeUser = (userName) => {
    var project = this.state.project;
    if (userName) {
      var userList = this.state.project.users || [];
      userList = uniq(userList);
      userList.splice(indexOf(userList, userName), 1);
      project.users = userList;
      updateProject(this.props.token, project)
        .then((resp) => {
          if (resp.status) {
            Mixpanel.track("Update Project - Remove User Successful", {
              removeUser: userName,
              project_id: project.id,
            });
            this.props.alertMsg(`${userName} removed successfully`, "success");
            this.setProjectUsersState(resp.data);
          }
        })
        .catch((err) => {
          const errorMessage =
            getErrorMessage(err) ||
            "An error occurred while removing the User, please try again.";
          Mixpanel.track("Update Project - Remove User Failed", {
            removeUser: userName,
            project_id: project.id,
            error: errorMessage,
          });
          this.props.alertMsg(errorMessage, "error");
        });
    }
  };

  addUser = () => {
    var project = this.state.project;
    if (this.state.selectedUser) {
      var userList = this.state.project.users || [];
      userList = uniq(userList);
      userList.push(this.state.selectedUser);
      project.users = userList;
      updateProject(this.props.token, project)
        .then((resp) => {
          MicroModal.close("modal-1");
          if (resp.status) {
            Mixpanel.track("Update Project - Add User Successful", {
              addUser: this.state.selectedUser,
              project_id: project.id,
            });
            this.props.alertMsg(
              `${this.state.selectedUser} added successfully`,
              "success"
            );
            this.setProjectUsersState(resp.data);
          }
        })
        .catch((err) => {
          const errorMessage =
            getErrorMessage(err) ||
            "An error occurred while adding the User, please try again.";
          Mixpanel.track("Update Project - Add User Failed", {
            addUser: this.state.selectedUser,
            project_id: project.id,
            error: errorMessage,
          });
          this.props.alertMsg(errorMessage, "error");
        });
    }
  };

  userRoleClass = (role) => {
    switch (role) {
      case "TenantUser":
        return "status-info";
      case "TenantAdmin":
        return "status-success";
      default:
        return "status-info";
    }
  };

  render() {
    var { projectOwner, currentIndex } = this.state;
    const { url } = this.props.match;
    var userRoleClass = this.userRoleClass;
    var removeUser = this.removeUser;
    return (
      <div className="main-container projects-show-container">
        {/* <NavigationLinks path={this.props.match.path} /> */}
        {/* <Sidebar path={this.props.match.path}/> */}
        {this.state.project && (
          <div className="main-body">
            <div>
              <div className="project-tabs-wraper">
                <div className="content-wraper-sm">
                  <div className="page-breadcrumb">
                    <ul>
                      <li>
                        <Link to="/projects">Projects</Link>
                      </li>
                      <li className="active">
                        {this.props.globalProject.name}
                      </li>
                    </ul>
                  </div>

                  <div className="project-tab-nav">
                    <ul>
                      <li
                        className={`react-tabs__tab${currentIndex === 0 &&
                          "--selected"}`}
                        onClick={() => {
                          Mixpanel.track(
                            "Project Details Page - Notebooks Tab",
                            {
                              project_id: this.props.match.params,
                            }
                          );
                          this.props.history.push(`${url}/${pathname[1]}`);
                        }}
                      >
                        Notebooks
                      </li>
                      <li
                        className={`react-tabs__tab${currentIndex === 1 &&
                          "--selected"}`}
                        onClick={() => {
                          Mixpanel.track(
                            "Project Details Page - Experiments Tab",
                            { project_id: this.props.match.params.id }
                          );
                          this.props.history.push(`${url}/${pathname[2]}`);
                        }}
                      >
                        Experiments
                      </li>
                      <li
                        className={`react-tabs__tab${currentIndex === 2 &&
                          "--selected"}`}
                        onClick={() => {
                          Mixpanel.track("Project Details Page - Models Tab", {
                            project_id: this.props.match.params.id,
                          });
                          this.props.history.push(`${url}/${pathname[3]}`);
                        }}
                      >
                        Model Catalog
                      </li>
                      <li
                        className={`react-tabs__tab${currentIndex === 3 &&
                          "--selected"}`}
                        onClick={() => {
                          Mixpanel.track(
                            "Project Details Page - Deployments Tab",
                            {
                              project_id: this.props.match.params.id,
                            }
                          );
                          this.props.history.push(`${url}/${pathname[4]}`);
                        }}
                      >
                        Deployments
                      </li>
                      <li
                        className={`react-tabs__tab${currentIndex === 4 &&
                          "--selected"}`}
                        onClick={() => {
                          Mixpanel.track(
                            "Project Details Page - Monitored Tab",
                            {
                              project_id: this.props.match.params.id,
                            }
                          );
                          this.props.history.push(`${url}/${pathname[5]}`);
                        }}
                      >
                        Monitoring
                      </li>
                      <li
                        className={`react-tabs__tab${currentIndex === 5 &&
                          "--selected"}`}
                        onClick={() => {
                          Mixpanel.track(
                            "Project Details Page - Workflows Tab",
                            {
                              project_id: this.props.match.params,
                            }
                          );
                          this.props.history.push(`${url}/${pathname[6]}`);
                        }}
                      >
                        Workflows
                      </li>
                      <li
                        className={`react-tabs__tab${currentIndex === 6 &&
                          "--selected"}`}
                        onClick={() => {
                          Mixpanel.track("Project Details Page - Team Tab", {
                            project_id: this.props.match.params,
                          });
                          this.props.history.push(`${url}/${pathname[7]}`);
                        }}
                      >
                        Manage Team
                      </li>
                      <li
                        className={`react-tabs__tab${currentIndex === 7 &&
                          "--selected"}`}
                        onClick={() => {
                          Mixpanel.track("Project Details Page - Secrets Tab", {
                            project_id: this.props.match.params,
                          });
                          this.props.history.push(`${url}/${pathname[8]}`);
                        }}
                      >
                        Secrets
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              {currentIndex === 0 && <Notebooks project={this.state.project} />}
              {currentIndex === 1 && (
                <ExperimentsList
                  tabName={"Experiments"}
                  run={this.state.run}
                  history={this.props.history}
                  match={this.props.match}
                  listType={"EXPERIMENT"}
                  projectID={this.state.project.id}
                  countersVisibility={false}
                />
              )}
              {currentIndex === 2 && (
                <ModelCatalog
                  tabName={"Models In Production"}
                  history={this.props.history}
                  match={this.props.match}
                  projectID={this.state.project.id}
                  countersVisibility={false}
                />
              )}
              {currentIndex === 3 && (
                <DeploymentsList
                  tabName={"Models In Production"}
                  history={this.props.history}
                  match={this.props.match}
                  projectID={this.state.project.id}
                  projectName={this.state.project.name}
                  countersVisibility={false}
                />
              )}
              {currentIndex === 4 && (
                <Monitoring
                  tabName={"Models In Production"}
                  history={this.props.history}
                  match={this.props.match}
                  listType={"DEPLOYED"}
                  projectID={this.state.project.id}
                  countersVisibility={false}
                />
              )}
              {currentIndex === 5 && (
                <JobsList
                  tabName={"Workflows"}
                  history={this.props.history}
                  match={this.props.match}
                  listType={"WORKFLOWS"}
                  projectID={this.state.project.id}
                  projectName={this.state.project.name}
                  countersVisibility={false}
                />
              )}
              {currentIndex === 6 && (
                <div className="project-team-wraper">
                  <div className="project-team-header clearfix">
                    <h4>Project Members</h4>
                    <button
                      onClick={() => MicroModal.show("modal-1")}
                      type="button"
                      data-toggle="modal"
                      //data-target="#new-project-member"
                      className="btn btn-primary"
                    >
                      Add New Member
                    </button>
                  </div>
                  <div className="card">
                    <div className="card-body">
                      <table className="table no-border tab-middle">
                        <tbody>
                          {this.state.projectUsers.map((u) => {
                            return (
                              <tr key={u.userName}>
                                <td style={{ maxWidth: 20 }}>
                                  <User user={u.userName} />
                                </td>
                                <td>{u.firstName + " " + u.lastName}</td>
                                <td>
                                  <label
                                    className={`model-item-status ${userRoleClass(
                                      u.role
                                    )} mr-sm`}
                                  >
                                    {u.role}
                                  </label>
                                  {projectOwner === u.userName && (
                                    <label
                                      className={`model-item-status status-failed`}
                                    >
                                      Owner
                                    </label>
                                  )}
                                </td>
                                <td>
                                  <div className="float-right">
                                    {projectOwner !== u.userName && (
                                      <button
                                        className="btn btn-danger"
                                        onClick={() => removeUser(u.userName)}
                                      >
                                        <i className="fa fa-trash" />
                                      </button>
                                    )}
                                  </div>
                                </td>
                              </tr>
                            );
                          })}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              )}
              {currentIndex === 7 && (
                <Secrets
                  tabName={"Secrets"}
                  history={this.props.history}
                  match={this.props.match}
                  project={this.state.project}
                  countersVisibility={false}
                />
              )}
            </div>
            <div
              className="modal micromodal-slide"
              id="modal-1"
              aria-hidden="true"
            >
              <div
                className="modal__overlay"
                tabIndex="-1"
                data-micromodal-close
              >
                <div
                  className="modal__container"
                  role="dialog"
                  aria-modal="true"
                  aria-labelledby="modal-1-title"
                >
                  <header className="modal__header">
                    <h2 className="modal__title" id="modal-1-title">
                      Edit Member
                    </h2>
                    <button
                      className="modal__close"
                      aria-label="Close modal"
                      data-micromodal-close
                    />
                  </header>
                  <main className="modal__content" id="modal-1-content">
                    <div className="modal-filter-form">
                      <div className="modal-body">
                        <div className="form-group">
                          <label
                            htmlFor="recipient-name"
                            className="col-form-label"
                          >
                            Select Member
                          </label>
                          <select
                            className="form-control"
                            value={this.state.selectedUser || ""}
                            onChange={(event) =>
                              this.setState({
                                selectedUser: event.target.value || null,
                              })
                            }
                          >
                            <option value="">Please select</option>
                            {this.state.selectionUsers.map(function(user) {
                              return (
                                <option
                                  value={user.userName}
                                  key={user.userName}
                                >
                                  {user.firstName + " " + user.lastName}
                                </option>
                              );
                            })}
                          </select>
                        </div>
                      </div>
                      <div className="modal-footer">
                        <button
                          onClick={() => MicroModal.close("modal-1")}
                          type="button"
                          className="btn btn-secondary"
                        >
                          Close
                        </button>
                        <button
                          type="button"
                          className="btn btn-primary"
                          onClick={() => this.addUser()}
                        >
                          Add Member
                        </button>
                      </div>
                    </div>
                  </main>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    projectDetails: state.projectDetails.projectDetails,
    globalProject: state.projects.globalProject,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      globalProjectSelection,
      alertMsg,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(ProjectShow);
