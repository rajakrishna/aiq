import React, { Component } from "react";
import { connect } from "react-redux";
import { TextField, Tooltip } from "@material-ui/core";
import { Config } from "./Notebook.config.js";
import { spawnNotebook, getAllProjects, getCurrentUser, getNotebooksUnusedVolumes } from "../api";

import ChooseSizeForm from "./choosesize";
import ChoosePackageForm from "./choosepackage";
import AddStorageForm from "./addstorageform";
import ReviewForm from "./reviewForm";
import Button from "@material-ui/core/Button";
import {
  withStyles,
  MuiThemeProvider,
  createMuiTheme,
} from "@material-ui/core/styles";
import blue from "@material-ui/core/colors/blue";

const theme = createMuiTheme({
  palette: {
    primary: blue,
  },
  typography: {
    useNextVariants: true,
  },
});

const pathname = ["chooseSizeForm", "choosePackageForm"];

class NotebookForm extends Component {
  state = {
    nm: "",
    ns: Config.spawnerFormDefaults.namespace.value,
    imageType: "standard",
    standardImages: Config.spawnerFormDefaults.image.value,
    customImage: "",
    cpu: Config.spawnerFormDefaults.cpu.value,
    memory: Config.spawnerFormDefaults.memory.value,
    ws_type: Config.spawnerFormDefaults.workspaceVolume.value.type.value,
    ws_name: Config.spawnerFormDefaults.workspaceVolume.value.name.value,
    ws_size: Config.spawnerFormDefaults.workspaceVolume.value.size.value,
    ws_mount_path:
      Config.spawnerFormDefaults.workspaceVolume.value.mountPath.value,
    ws_access_modes:
      Config.spawnerFormDefaults.workspaceVolume.value.accessModes.value,
    extraResources: "{}",
    error: "",
    project_id: "",
    dataVols: [],
    projects: [],
    chooseSizeForm: false,
    choosePackageForm: true,
    addStorageForm: false,
    reivewForm: false,
    size: "Nano",
    gpu: 0,
    currentUser: [],
    maxLimit: process.env.REACT_APP_WORKSPACE_VOLUME_LIMIT,
    availableVolumes: [],
    volumesError:""

  };

  constructor(props) {
    super();
    this.state.project_id = props.selectedProject.id;
    this.state.nm = props.selectedProject.name;
    this.state.ws_name = props.selectedProject.name;
  }

  componentDidMount() {
    getAllProjects(this.props.token, {}).then((res) => {
      if (res.status === 200) {
        const projects = res.data.map((e, i) => {
          return { name: e.name, id: e.id };
        });
      }
    });
    getCurrentUser(this.props.token)
    .then(response => {
      if (response.status) {
        
        this.setState({ currentUser: response.data },()=>console.log("currentUser::",this.state.currentUser));
        if(this.state.currentUser.tier ==="Free Tier"){
          this.props.history.push("/notebooks");
        }
      }
    })
    .catch(error => {
     
    });
    this.handleUnusedVols();
  }
  

  componentDidUpdate(prevProps) {
    if (prevProps.selectedProject.id !==  this.props.selectedProject.id) {
       this.setState({project_id:this.props.selectedProject.id,
                      nm:this.props.selectedProject.name,
                      ws_name:this.props.selectedProject.name
                    })

                     this.handleUnusedVols();
    }
  }

  handleUnusedVols =()=>{
    getNotebooksUnusedVolumes(this.props.token,this.props.selectedProject.id).then((res)=>{
      this.setState({availableVolumes:res.data.volumes})
    })
    .catch((err)=>console.log("getNotebooksVolumes::NotebookForm::err.message::",err.message))
  }

  handleVolumesValidation =()=>{
    getNotebooksUnusedVolumes(this.props.token,this.props.selectedProject.id).then((res)=>{
      let volsNames = res.data.volumes.map((item)=> item.name)

        let existCheck = volsNames.includes(this.state.ws_name)
        if(!existCheck){
          this.setState({...this.state,volumesError:`${this.state.ws_name} is already in use`})
        }
        
          this.state.dataVols.map((item)=>{
            let checkDataVols = volsNames.includes(item.vol_name)
            if((volsNames.length !== 0) && !checkDataVols){
              this.setState({...this.state,volumesError:`${item.vol_name} is already in use`})
            }
          })
    })
    .catch((err)=>console.log("getNotebooksVolumes::reviewForm::err.message::",err.message))
  }

  handleChange = (e) => {
    const target = e.target;
    if (target.name === "nm") {
      this.setState({
        nm: target.value,
        ws_name: target.value,
      });
      return 0;
    }
    if (target.name === "gpu" && parseInt(target.value) > 0) {
      var obj = {
        "nvidia.com/gpu": target.value,
      };
      obj = JSON.stringify(obj);
      this.setState({
        extraResources: obj,
      });
    }else if(target.name === "gpu"){
      this.setState({extraResources:"{}"})
    }

    this.setState({
      [target.name]: target.value,
    },()=>{
      if(this.state.ws_type === 'Existing' && target.name === 'ws_name'){
        let reqvolumes = this.state.availableVolumes.find((item)=>item.name === target.value)
        this.setState({...this.state,
                        ws_size:reqvolumes && reqvolumes.size.replace("Gi",""),
                        ws_access_modes:reqvolumes && reqvolumes.access_mode[0]
                       })
      }
    });
  };

  handleVolumesError =(err)=>{
     this.setState({...this.state,volumesError:err})
  }

  setPackage = (e) => {
    const target = e.target;
    this.setState({
      standardImages: target.value,
    });
  };

  setSize = (target) => {
    this.setState({
      extraResources: target.extraResources,
      cpu: target.cpu,
      memory: target.memory,
    });
  };

  handlePrevious = (e) => {
    this.setState({...this.state,volumesError:""})
    if (this.state.chooseSizeForm) {
      this.setState({
        choosePackageForm: true,
        chooseSizeForm: false,
        reivewForm: false,
        addStorageForm: false,
      });
    } else if (this.state.addStorageForm) {
      this.setState({
        choosePackageForm: false,
        chooseSizeForm: true,
        reivewForm: false,
        addStorageForm: false,
      });
    } else if (this.state.reivewForm) {
      this.setState({
        choosePackageForm: false,
        chooseSizeForm: false,
        reivewForm: false,
        addStorageForm: true,
      });
    }
  };

  handleNext = (e) => {
    if (this.state.choosePackageForm) {
      this.setState({
        choosePackageForm: false,
        chooseSizeForm: true,
        reivewForm: false,
        addStorageForm: false,
      });
    } else if (this.state.chooseSizeForm) {
      this.setState({
        choosePackageForm: false,
        chooseSizeForm: false,
        reivewForm: false,
        addStorageForm: true,
      });
    } else if (this.state.addStorageForm) {
      this.setState({
        choosePackageForm: false,
        chooseSizeForm: false,
        reivewForm: true,
        addStorageForm: false,
      });
    }
  };

  chooseSize = () => {
    this.setState({
      reivewForm: false,
      choosePackageForm: false,
      chooseSizeForm: true,
      addStorageForm: false,
    });
  };

  choosePackage = () => {
    this.setState({
      choosePackageForm: true,
      chooseSizeForm: false,
      addStorageForm: false,
      reivewForm: false,
    });
  };

  addStorage = () => {
    this.setState({
      choosePackageForm: false,
      chooseSizeForm: false,
      addStorageForm: true,
      reivewForm: false,
    });
  };

  addReview = () => {
    this.setState({
      choosePackageForm: false,
      chooseSizeForm: false,
      addStorageForm: false,
      reivewForm: true,
    });
  };

  addVols = () => {
    const dataVols = [...this.state.dataVols];
    const i = dataVols.length;
    dataVols.push({
      vol_type: "New",
      vol_size: "10",
      vol_name: `data-vol-${i + 1}`,
      vol_mount_path: `/home/jovyan/data-vol-${i + 1}`,
      vol_access_modes: "ReadWriteOnce",
    });
    this.setState({ dataVols });
  };

  remVols = (index) => {
    let dataVols = [...this.state.dataVols];
    dataVols.splice(index, 1);
    this.setState({ dataVols });
  };

  handleVols = (index, name, value) => {
    let dataVol = [...this.state.dataVols];
    dataVol[index][name] = value;
    this.setState({
      dataVols: dataVol,
    },()=>{
      if(this.state.dataVols[index].vol_type === "Existing" && name === "vol_name"){
        let reqAddVols = this.state.availableVolumes.find((item)=>item.name === value)
        dataVol[index]["vol_size"]=reqAddVols && reqAddVols.size.replace("Gi","");
        dataVol[index]["vol_access_modes"] =reqAddVols && reqAddVols.access_mode[0];
        this.setState({...this.state, dataVols:dataVol})
      }
    });
  };

  handleSubmit = (event) => {
    this.handleVolumesValidation();
    if (
      this.checkMaxLimits() ||
      this.state.currentUser.tier == "Professional Tier"
    ) {
      event.preventDefault();
      const state = { ...this.state };
      const dataVols = [...this.state.dataVols];
      delete state.dataVols;
      delete state.error;
      delete state.availableVolumes;
      dataVols.forEach((e, i) => {
        Object.keys(e).forEach((key) => {
          state[key + (i + 1)] = e[key];
        });
      });
      const arr = Object.keys(state).map((e, i) => {
        return e + "=" + encodeURIComponent(state[e]);
      });
      const string = arr.join("&");
      spawnNotebook(this.props.token, string).then((res) => {
        if (res.data && !res.data.success) {
          this.setState({
            error:
              res.data.log &&
              res.data.log.replace(/(\.?kubeflow.org|kubeflow)/gi, ""),
          });
        } else {
          this.props.history.goBack();
         // this.props.showTable();
        }
      });
   }
  };

  checkMaxLimits = () => {
    let totalSize = parseInt(this.state.ws_size) + this.getDataVolumeSize();
    if (totalSize > this.state.maxLimit) {
      this.setState({
        error:
          "Your subscription does not allow creating notebooks with total storage more than " +
          this.state.maxLimit + "Gi. " +
          "Please contact support@predera.com to increase the limits.",
      });
      return false;
    } else {
      return true;
    }
  };

  getDataVolumeSize = () => {
    let totalSize = 0;
    this.state.dataVols.forEach((item) => {
      totalSize = totalSize + parseInt(item.vol_size);
    });
    return totalSize;
  };

  render() {
    var {
      chooseSizeForm,
      choosePackageForm,
      addStorageForm,
      reivewForm,
    } = this.state;

    return (
      <div className="main-container">
        <div className="main-body">
          <div className="content-wraper-sm">
            <div className="card notebookcard">
            <div className="card-header mt-2 pb-0 pl-0">
                {/* <span
                  className="clickable"
                  onClick={() => {
                        this.props.history.goBack()
                  }}
                >
                  <i class="fa fa-angle-left" aria-hidden="true"></i>{" "}
                </span>   */}
                <span className="notebook-form-title pl-4">
                  CREATE NOTEBOOK
                </span>
            </div>
      <div className="card-body pt-0 pl-0">
      <div className="notebookform-content">
        <form autoComplete="off">
          <div className="pl-0">
            <div className="row mb-lg pl-4">
              <div className="col-sm-6 pl-3">
                <TextField
                  className="notebookform-nm-input"
                  name="nm"
                  id="nm"
                  label={`Name`}
                  value={this.state.nm}
                  onChange={this.handleChange}
                  margin="normal"
                  fullWidth
                  required
                />
                <div className="f-12 text-gray">
                  Specify the name of the Notebook
                </div>
                <div>{/* <ChooseSizeForm /> */}</div>
              </div>
              <div className="col-sm-6"></div>

              <div className="project-tab-nav mt-46 mt-3 pl-3">
                <ul className="notebookform-tab-name pl-0">
                  <li
                    className={`react-tabs__tab${this.state.choosePackageForm &&
                      "--selected"} pl-0`}
                    onClick={this.choosePackage}
                  >
                    1. Choose Package
                  </li>
                  <li
                    className={`react-tabs__tab${this.state.chooseSizeForm &&
                      "--selected"}`}
                    onClick={this.chooseSize}
                  >
                    2. Choose Size
                  </li>
                  <li
                    className={`react-tabs__tab${this.state.addStorageForm &&
                      "--selected"}`}
                    onClick={this.addStorage}
                  >
                    3. Add Storage
                  </li>
                  <li
                    className={`react-tabs__tab${this.state.reivewForm &&
                      "--selected"}`}
                    onClick={this.addReview}
                  >
                    4. Review
                  </li>
                </ul>
              </div>
              <React.Fragment>
                {choosePackageForm ? (
                  <div className="col-sm-12">
                    <ChoosePackageForm
                      setPackage={this.setPackage}
                      package={this.state}
                    />
                  </div>
                ) : (
                  <div></div>
                )}
              </React.Fragment>
              <React.Fragment>
                {chooseSizeForm ? (
                  <div className="col-sm-12">
                    <ChooseSizeForm
                      setSize={this.setSize}
                      size={this.state}
                      userTier={this.state.currentUser.tier}
                      handleChange={this.handleChange}
                    />
                  </div>
                ) : (
                  <div></div>
                )}
              </React.Fragment>
              <React.Fragment>
                {addStorageForm ? (
                  <div className="col-sm-12">
                    <AddStorageForm
                      handleChange={this.handleChange}
                      handleVols={this.handleVols}
                      addVols={this.addVols}
                      remVols={this.remVols}
                      workSpace={this.state}
                    />
                  </div>
                ) : (
                  <div></div>
                )}
              </React.Fragment>
              <React.Fragment>
                {reivewForm ? (
                  <div className="col-sm-12">
                    <ReviewForm
                      data={this.state}
                      chooseSize={this.chooseSize}
                      choosePackage={this.choosePackage}
                      addStorage={this.addStorage}
                      handleVolumesError={this.handleVolumesError}
                    />
                  </div>
                ) : (
                  <div></div>
                )}
              </React.Fragment>
            </div>
            {
          this.state.volumesError &&
           <div className="row mb-lg">
                <div className="col-sm-12">
                    <div className="alert alert-danger f-13">
                      <strong>Error: </strong>
                      {this.state.volumesError}
                    </div>
                </div>
             </div>
        }
            {this.state.error && (
              <div className="row mb-lg">
                <div className="col-sm-12">
                  <Tooltip title="Double click to dismiss">
                    <div
                      className="alert alert-danger f-13"
                      onDoubleClick={() => {
                        this.setState({
                          error: "",
                        });
                      }}
                    >
                      <strong>Error: </strong>
                      {this.state.error}
                    </div>
                  </Tooltip>
                </div>
              </div>
            )}
            <div className="mt-lg f-r">
              <MuiThemeProvider theme={theme}>
                <Button
                  name="cancel"
                  onClick={() => {
                    this.props.history.goBack()
                  }}
                  variant="outlined"
                  color="primary"
                  className='ml-3 '
                >
                  Cancel
                </Button>
              </MuiThemeProvider>
              {!choosePackageForm ? (
                <MuiThemeProvider theme={theme}>
                  <Button
                    variant="contained"
                    onClick={this.handlePrevious}
                    style={{ backgroundColor: "#3D69F9", color: "#FFFFFF" }}
                    className="ml-3"
                  >
                    Previous
                  </Button>
                </MuiThemeProvider>
              ) : (
                <span></span>
              )}
              {!reivewForm ? (
                <MuiThemeProvider theme={theme}>
                  <Button
                    variant="contained"
                    onClick={this.handleNext}
                    style={{ backgroundColor: "#3D69F9", color: "#FFFFFF" }}
                    className="ml-3"
                  >
                    Next
                  </Button>
                </MuiThemeProvider>
              ) : (
                <span></span>
              )}
              {reivewForm ? (
                <MuiThemeProvider theme={theme}>
                  <Button
                    onClick={this.handleSubmit}
                    variant="contained"
                    style={{ backgroundColor: "#3D69F9", color: "#FFFFFF"}}
                    className={`ml-3 clickable ${this.state.volumesError ? "disabled":""}`}
                    disabled={this.state.volumesError}
                  >
                    Create
                  </Button>
                </MuiThemeProvider>
              ) : (
                <span></span>
              )}
            </div>
          </div>
        </form>
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>

    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    selectedProject: state.projects.globalProject,
  };
};

export default connect(mapStateToProps)(NotebookForm);

/* Submission Hint

nm=testing&ns=kai&imageType=standard&standardImages=gcr.io%2Fkubeflow-images-public%2Ftensorflow-1.13.1-notebook-cpu%3Av0.5.0&customImage=&cpu=0.5&memory=1.0Gi&ws_type=New&ws_name=testing&ws_size=10&ws_mount_path=%2Fhome%2Fjovyan&ws_access_modes=ReadWriteOnce&vol_type1=New&vol_name1=data-vol-1&vol_size1=10&vol_mount_path1=%2Fhome%2Fjovyan%2Fdata-vol-1&vol_access_modes1=ReadWriteOnce&vol_type2=New&vol_name2=data-vol-2&vol_size2=10&vol_mount_path2=%2Fhome%2Fjovyan%2Fdata-vol-2&vol_access_modes2=ReadWriteOnce&vol_type3=New&vol_name3=data-vol-3&vol_size3=10&vol_mount_path3=%2Fhome%2Fjovyan%2Fdata-vol-3&vol_access_modes3=ReadWriteOnce&extraResources=%7B%7D
 

***PARSED***

nm=testing
ns=kai
imageType=standard
standardImages=gcr.io/kubeflow-images-public/tensorflow-1.13.1-notebook-cpu:v0.5.0
customImage=
cpu=0.5
memory=1.0Gi
ws_type=New
ws_name=testing
ws_size=10
ws_mount_path=/home/jovyan
ws_access_modes=ReadWriteOnce
vol_type1=New
vol_name1=data-vol-1
vol_size1=10
vol_mount_path1=/home/jovyan/data-vol-1
vol_access_modes1=ReadWriteOnce
vol_type2=New
vol_name2=data-vol-2
vol_size2=10
vol_mount_path2=/home/jovyan/data-vol-2
vol_access_modes2=ReadWriteOnce
vol_type3=New
vol_name3=data-vol-3
vol_size3=10
vol_mount_path3=/home/jovyan/data-vol-3
vol_access_modes3=ReadWriteOnce
extraResources={}

*/
