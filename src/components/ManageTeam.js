import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import "react-select/dist/react-select.css";
import { getProject, getAllUsers, updateProject } from "../api";
import { filter, indexOf, uniq } from "lodash";
import MicroModal from "micromodal";
import User from "./shared/User";
import { alertMsg } from "../ducks/alertsReducer";
import Mixpanel from "mixpanel-browser";
import { getErrorMessage } from "../utils/helper";
import { globalProjectSelection } from "../ducks/projects";
import { getCurrentUser } from "../api";
import { confirm } from "./Confirm";


class ManageTeam extends Component {
  project = "";
  projectOwner = "";
  allUsers = "";
  constructor(props) {
    super(props);
    this.state = {
      projectUsers: [],
      allUsers: [],
      selectionUsers: [],
      project: "",
      projectOwner: "",
      user: "",
    };
  }

  async componentDidMount() {

    Mixpanel.track("Project Details Page", {
      project_id: this.props.selectedProject.id,
    });
    
    MicroModal.init();
    this.getProjectAndUserDetails();
    getCurrentUser(this.props.token)
      .then((response) => {
        if (response.status) {
          const data = response.data;
          this.setState({ user: response.data.userName });
        }
      })
      .catch((error) => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while fetching User Profile, please try again.";
        this.props.alertMsg(errorMessage, "error");
      });
  }

  componentDidUpdate(prevState) {
    if (prevState.selectedProject.id !== this.props.selectedProject.id) {
      this.getProjectAndUserDetails();
    }
  }

  getProjectAndUserDetails = () => {
    getProject(this.props.token, this.props.selectedProject.id)
      .then((response) => {
        if (response.status) {
          this.project = response.data;
          this.projectOwner = response.data.owner;
          this.props.globalProjectSelection(this.project);
        }
      })
      .catch((error) => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while fetching Project details, please try again.";
        Mixpanel.track("Get Project Failed", {
          project_id: this.props.match.params.id,
          error: errorMessage,
        });
        this.props.alertMsg(errorMessage, "error");
      });

    getAllUsers(this.props.token).then((response) => {
      if (response.status) {
        this.allUsers = response.data;
        this.setProjectUsersState(this.project);
      }
    });
  };

  setProjectUsersState = (project) => {
    var projectUserEmails = project.users || [];
    var allUsers = this.allUsers;
    projectUserEmails.push(project.owner);

    const selectionUsers = filter(allUsers, function(u) {
      return indexOf(projectUserEmails, u.userName) < 0;
    });

    const projectUsers = filter(allUsers, function(u) {
      return indexOf(projectUserEmails, u.userName) >= 0;
    });
    this.setState({
      selectionUsers: selectionUsers,
      projectUsers: projectUsers,
      allUsers,
      project,
      projectOwner: this.projectOwner,
    });
  };

  removeUser = (userName) => {
    var project = this.state.project;
    if (userName) {
      var userList = this.state.project.users || [];
      userList = uniq(userList);
      userList.splice(indexOf(userList, userName), 1);
      project.users = userList;
      updateProject(this.props.token, project)
        .then((resp) => {
          if (resp.status) {
            Mixpanel.track("Update Project - Remove User Successful", {
              removeUser: userName,
              project_id: project.id,
            });
            this.props.alertMsg(`${userName} removed successfully`, "success");
            this.setProjectUsersState(resp.data);
          }
        })
        .catch((err) => {
          const errorMessage =
            getErrorMessage(err) ||
            "An error occurred while removing the User, please try again.";
          Mixpanel.track("Update Project - Remove User Failed", {
            removeUser: userName,
            project_id: project.id,
            error: errorMessage,
          });
          this.props.alertMsg(errorMessage, "error");
        });
    }
  };

  addUser = () => {
    var project = this.state.project;
    if (this.state.selectedUser) {
      var userList = this.state.project.users || [];
      userList = uniq(userList);
      userList.push(this.state.selectedUser);
      project.users = userList;
      updateProject(this.props.token, project)
        .then((resp) => {
          MicroModal.close("modal-1");
          if (resp.status) {
            Mixpanel.track("Update Project - Add User Successful", {
              addUser: this.state.selectedUser,
              project_id: project.id,
            });
            this.props.alertMsg(
              `${this.state.selectedUser} added successfully`,
              "success"
            );
            this.setProjectUsersState(resp.data);
          }
        })
        .catch((err) => {
          const errorMessage =
            getErrorMessage(err) ||
            "An error occurred while adding the User, please try again.";
          Mixpanel.track("Update Project - Add User Failed", {
            addUser: this.state.selectedUser,
            project_id: project.id,
            error: errorMessage,
          });
          this.props.alertMsg(errorMessage, "error");
        });
    }
  };

  userRoleClass = (role) => {
    switch (role) {
      case "TenantUser":
        return "status-info";
      case "TenantAdmin":
        return "status-success";
      default:
        return "status-info";
    }
  };

  render() {
    var removeUser = this.removeUser;
    var { projectOwner, currentIndex } = this.state;
    var userRoleClass = this.userRoleClass;

    return (
      <div className="project-team-wraper ml-3">
        <div className="project-team-header clearfix mb-3">
          <h4>Project Members</h4>
          <button
            onClick={() => MicroModal.show("modal-1")}
            type="button"
            data-toggle="modal"
            //data-target="#new-project-member"
            className="btn btn-sm btn-primary"
            disabled={projectOwner !== this.state.user}
          >
            Add New Member
          </button>
          <div
            className="modal micromodal-slide"
            id="modal-1"
            aria-hidden="true"
          >
            <div className="modal__overlay" tabIndex="-1" data-micromodal-close>
              <div
                className="modal__container"
                role="dialog"
                aria-modal="true"
                aria-labelledby="modal-1-title"
              >
                <header className="modal__header">
                  <h2 className="modal__title" id="modal-1-title">
                    Edit Member
                  </h2>
                  <button
                    className="modal__close"
                    aria-label="Close modal"
                    data-micromodal-close
                  />
                </header>
                <main className="modal__content" id="modal-1-content">
                  <div className="modal-filter-form">
                    <div className="modal-body">
                      <div className="form-group">
                        <label
                          htmlFor="recipient-name"
                          className="col-form-label"
                        >
                          Select Member
                        </label>
                        <select
                          className="form-control"
                          value={this.state.selectedUser || ""}
                          onChange={(event) =>
                            this.setState({
                              selectedUser: event.target.value || null,
                            })
                          }
                        >
                          <option value="">Please select</option>
                          {this.state.selectionUsers.map(function(user) {
                            return (
                              <option value={user.userName} key={user.userName}>
                                {user.firstName + " " + user.lastName}
                              </option>
                            );
                          })}
                        </select>
                      </div>
                    </div>
                    <div className="modal-footer">
                      <button
                        onClick={() => MicroModal.close("modal-1")}
                        type="button"
                        className="btn btn-secondary"
                      >
                        Close
                      </button>
                      <button
                        type="button"
                        className="btn btn-primary"
                        onClick={() => this.addUser()}
                      >
                        Add Member
                      </button>
                    </div>
                  </div>
                </main>
              </div>
            </div>
          </div>
        </div>
        <table className="table data-table no-border">
          <thead>
            <tr>
              <td className="pl-0">User</td>
              <td>Role</td>
              <td className="float-right pr-0">Action</td>
            </tr>
          </thead>
          <tbody>
            {this.state.projectUsers.map((u) => {
              return (
                <tr key={u.userName}>
                  <td className="pl-0">
                    {" "}
                    <User user={u.userName} />
                    <span className="m-1">
                      {u.firstName + " " + u.lastName}
                    </span>
                  </td>
                  <td>
                    <label
                      className={`model-item-status ${userRoleClass(
                        u.role
                      )} mr-sm`}
                    >
                      {u.role}
                    </label>
                    {projectOwner === u.userName && (
                      <label className={`model-item-status status-failed`}>
                        Owner
                      </label>
                    )}
                  </td>
                  <td className="pr-0">
                    <div className="float-right">
                      {projectOwner !== u.userName && (
                        <button
                          className="btn btn-danger"
                          disabled={projectOwner !== this.state.user}
                          onClick={(elem) => {
                            confirm("Are you sure?", "Remove User").then(
                              () => {
                                removeUser(u.userName)                              },
                              () => {
                                console.log("Cancel!");
                              }
                            );
                          }}
                        >
                          <i className="fa fa-trash" />
                        </button>
                      )}
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    projectDetails: state.projectDetails.projectDetails,
    globalProject: state.projects.globalProject,
    selectedProject: state.projects.globalProject,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      globalProjectSelection,
      alertMsg,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(ManageTeam);
