import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import NavigationLinks from './NavigationLinks';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  getAllProjectsNamesList,
  getAllModelsByProjectNames
 } from '../ducks/models';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import moment from 'moment';
import ModelHealth from './chart/ModelHealth';
import ModelPrediction from './chart/ModelPrediction';
import Library from './shared/Library';

class Models extends Component {

  state = {
    models: [],
    version: [],
  }

  componentDidMount() {
    this.props.getAllProjectsNamesList();
  }

  setDefaultProject(projectName) {
    if (projectName) {
      this.setState({selectedProject: projectName})
      this.props.getAllModelsByProjectNames(projectName);
    }
  }
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (this.state.selectedProject == null) {
      if (nextProps.projectsNames[0]) {
        this.setDefaultProject(nextProps.projectsNames[0].value);
      }
    }
    if (nextProps.models !== this.state.models) {
      Array.prototype.groupBy = function(prop) {
        return nextProps.models.reduce(function(groups, item) {
          const val = item[prop]
          groups[val] = groups[val] || []
            groups[val].push(item)
          return groups
        }, {})
      }

      let sortedListOfModels = []
      for (var i in nextProps.models) {
        if (nextProps.models.hasOwnProperty(i)) {
          if (nextProps.models[i].status === 'DEPLOYED') {
            sortedListOfModels.push(nextProps.models[i])
          }
        }
      }
      const groupByName = sortedListOfModels.groupBy('name');
      this.setState({ models: groupByName });
      let selectionModel = {}
      for(let i in groupByName) {
        selectionModel[i] = groupByName[i][0]
      }
      this.setState({selectionModel})
    }

  }

  handleChange = (event) => {
    if (event) {
      this.setState({selectedProject: event.value })
      this.props.getAllModelsByProjectNames(event.value);
    }
  }

  handleVersionChange= (event) => {
    let selectionModel = this.state.selectionModel
    selectionModel[event.value.name] = event.value

    this.setState({selectionModel})
  }

  getModelVersions(models) {
    const versionList = [];

    models.forEach(model => {
      versionList.push({ value: model , label: model.version  })
    })
    return versionList || [];
  }

  percentFormat(value) {
    return `${(value *100).toFixed(1)}%`
  }

  render() {
    return(
      <div className="main-container">
        <NavigationLinks path={this.props.match.path}/>
        {/* <Sidebar path={this.props.match.path}/> */}
        <div className="main-body">
          <div className="content-wraper-sm">
            <div className="projects-wraper ml-5 mr-5">
              { this.props.message &&
                <div className="error-placement alert alert-danger">
                <i className="fa fa-exclamation-triangle"></i>
                <span>{this.props.message}</span>
                </div>
              }
              <div className="model-header">
                <h1>Models</h1>
                <Select
                  name="form-field-name"
                  placeholder="Select Project Name"
                  onChange={this.handleChange}
                  options={ this.props.projectsNames }
                  value={ this.state.selectedProject }
                  className="project-select"
                />
              </div>

            { Object.keys(this.state.models).length > 0 &&
              <div className="model-item">
              { Object.entries(this.state.models).map((model,i ) => {
                return (
                  <div className="panel" key={i}>
                    <div className="panel-header">
                    <Link to={`/models/${this.state.selectionModel[model[0]].id}`}>
                      <div className="model-title-info">
                      <figure><Library library={this.state.selectionModel[model[0]].ml_library}/></figure>
                      <h4>{this.state.selectionModel[model[0]].name}</h4>
                      <p>{this.state.selectionModel[model[0]].algorithm}</p>
                      </div>
                    </Link>
                      <div className="model-actions">
                        <label className="model-status">{this.state.selectionModel[model[0]].status}</label>
                        <div className="version-drop">
                        <label>Version</label>
                        <div className="version-select">
                          <Select
                          searchable={false}
                          name="form-field-name"
                          onChange={this.handleVersionChange}
                          options={ this.getModelVersions(model[1])}
                          placeholder={this.state.selectionModel[model[0]].version}
                          />
                        </div>

                        </div>
                      </div>
                    </div>
                    <div className="panel-body">
                      <div className="row">
                        <div className="col-md-4">
                          <div className="form-group progress-bar-item">
                            <p>AUC  <span className="progress-percentage">{this.percentFormat(this.state.selectionModel[model[0]].auc)}</span></p>
                            <div className="progress">
                              <div className="progress-bar"
                                style={
                                  {
                                    width: this.percentFormat(this.state.selectionModel[model[0]].auc),
                                    backgroundColor: '#1bc5dc'
                                  }
                                }>
                              </div>
                            </div>
                          </div>

                          <div className="form-group progress-bar-item">
                            <p>ACCURACY <span className="progress-percentage">{this.percentFormat(this.state.selectionModel[model[0]].accuracy)}</span></p>
                            <div className="progress">
                              <div className="progress-bar"
                                style={
                                  {
                                    width: this.percentFormat(this.state.selectionModel[model[0]].accuracy),
                                    backgroundColor: '#feb822'
                                  }
                                }>
                              </div>
                            </div>
                          </div>

                          <div className="form-group progress-bar-item">
                            <p>PRECISION  <span className="progress-percentage">{this.percentFormat(this.state.selectionModel[model[0]].precision)}</span></p>
                            <div className="progress">
                              <div className="progress-bar"
                                style={
                                  {
                                    width: this.percentFormat(this.state.selectionModel[model[0]].precision),
                                    backgroundColor: '#2655c0'
                                  }
                                }>
                              </div>
                            </div>
                          </div>

                          <div className="form-group progress-bar-item">
                            <p>RECALL  <span className="progress-percentage">{this.percentFormat(this.state.selectionModel[model[0]].recall)}</span></p>
                            <div className="progress">
                              <div className="progress-bar"
                                style={
                                  {
                                    width: this.percentFormat(this.state.selectionModel[model[0]].recall),
                                    backgroundColor: '#999999'
                                  }
                                }>
                              </div>
                            </div>
                          </div>

                        </div>
                        <div className="col-md-4">
                          <ModelHealth model_id={this.state.selectionModel[model[0]].id} key={this.state.selectionModel[model[0]].id}/>
                        </div>
                        <div className="col-md-4">
                          <ModelPrediction model_id={this.state.selectionModel[model[0]].id} key={this.state.selectionModel[model[0]].id}/>
                        </div>
                      </div>
                    </div>
                    <div className="panel-footer">
                      <div className="model-created-at">
                        Created at { moment(this.state.selectionModel[model[0]].created_date).format('LLL') }
                      </div>
                      <div className="model-languages">
                        <ul className="list-inline">
                          <Library library={this.state.selectionModel[model[0]].ml_library}/>
                          <Library library={this.state.selectionModel[model[0]].ml_library}/>
                          <Library library={this.state.selectionModel[model[0]].ml_library}/>
                        </ul>
                      </div>
                    </div>
                  </div>
                )
                }) }
              </div>
            }
            { Object.keys(this.state.models).length === 0 &&
              <div className="model-item">
                <div className="panel">
                  <div className="panel-header">
                    <div className="model-title-info">
                      <h1>No Models found for this project</h1>
                    </div>
                  </div>
                </div>
              </div>
            }
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    projectsNames : state.models.projectsNames,
    models: state.models.models,
    message: state.projects.message,
    successMessage: state.projects.successMessage,
  }
};

const mapDispatchToProps = dispatch => {
  return (
    bindActionCreators({
      getAllProjectsNamesList,
      getAllModelsByProjectNames
    }, dispatch)
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(Models);
