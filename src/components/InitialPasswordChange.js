import React, { Component } from 'react';
import logo from "../images/predera-logo.png"
import { verifyUserAndLogin, authReset } from '../ducks/auth';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Mixpanel from "mixpanel-browser";

class InitialPasswordChange extends Component {

  state = {
    userName: this.props.match.params.username,
    password: "",
    newPassword: "",
    confirmPassword: "",
    errorMessage: "",
    loading: false,
    policy: false
  }

  componentDidMount() {
    Mixpanel.track("Initial Password Change Page");
  }

  onSubmit = e => {
    this.setState({
      loading: true,
      errorMessage: ""
    })
    this.props.authReset();
    e.preventDefault();
    if(this.state.confirmPassword === this.state.newPassword){
      const registrationDetails = {
        userName: this.state.userName,
        password: this.state.password,
        newPassword: this.state.newPassword,
      }
      this.props.verifyUserAndLogin(registrationDetails);
    } else {
      this.setState({errorMessage: "Passwords do not match"});
      this.setState({
        loading: false
      })
    }
  }

  render() {
    const token = this.props.auth && this.props.auth.token;
    return(
      token ?
      <Redirect to='/' /> :
      <div className="auth-wraper">
        <form onSubmit={this.onSubmit}>
          <div className="auth-box">
            <div className="logo-head">
              <img src={logo} alt=""/>
            </div>
            <h2 className="auth-heading">Congratulations on Your Successful Login!</h2>
            <p className="">Please change your password before proceeding</p>
            {/* error messages */}
            { this.state.errorMessage &&
              <div className="error-placement alert alert-danger">
                <i className="fa fa-exclamation-triangle"></i>
                <span>{this.state.errorMessage}</span>
              </div>
            }
            { this.props.auth.message &&
              <div className="error-placement alert alert-danger">
                <i className="fa fa-exclamation-triangle"></i>
                <span>{this.props.auth.message}</span>
              </div>
            }

            <div className="form-group password-input form-material">
              <label>Current Password</label>
              <input
                onChange={(e) => this.setState({password: e.target.value})}
                type="password" className="form-control" required/>
            </div>
            <div className="form-group password-input form-material">
              <label>New Password</label>
              <input
                onChange={(e) => this.setState({newPassword: e.target.value})}
                type="password" className="form-control" required/>
            </div>
            <div className="form-group password-input form-material mb">
              <label>Confirm New Password</label>
              <input
                onChange={(e) => this.setState({confirmPassword: e.target.value})}
                type="password" className="form-control" required/>
            </div>
            <div className="f-12 pd-tb text-gray">
              <div className="alert alert-warning">
              <b className="clickable" onClick={()=>this.setState({
                policy: !this.state.policy
              })}>Password Policy <i className={!(this.state.policy || this.props.auth.message) ? "fa fa-caret-down" : "fa fa-caret-up"}></i></b>
              {(this.state.policy || this.props.auth.message) && 
              <ul className="mb-0">
                <li>Password length must be atleast 8</li>
                <li>Password must contain numeric characters</li>
                <li>Password must contain lowercase characters</li>
                <li>Password must contain uppercase characters</li>
              </ul>
              }
              </div>
            </div>
            <div className="form-group">
                <button 
                  type="submit" 
                  className="btn btn-primary form-btn"
                  disabled={this.state.loading && this.props.auth.loading ? "disabled" : ""}
                >
                  {this.state.loading && this.props.auth.loading ? "Changing Password..." : "Confim and Continue"}
                </button>
            </div>

          </div>
        </form>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  }
};

const mapDispatchToProps = dispatch => {
  return (
    bindActionCreators({ 
      verifyUserAndLogin,
      authReset
    }, dispatch)
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(InitialPasswordChange);
