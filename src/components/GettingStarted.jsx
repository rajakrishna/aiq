import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import DownloadAgent from "./introduction/DownloadAgent";
import LogExperiment from "./introduction/LogExperiment";
import DeployModel from "./introduction/DeployModel";
import LogMonitor from "./introduction/LogMonitor.jsx";
import { getAllArtifacts, downloadArtifact } from "../api";
import { Switch, Route, Link } from "react-router-dom";
import { alertMsg } from "../ducks/alertsReducer";
import FileDownload from "js-file-download";

import Mixpanel from "mixpanel-browser";
import { getErrorMessage, getResponseError } from "../utils/helper";

const pathname = [
  "",
  "Download-and-install",
  "Log-experiment",
  "Deploy-model",
  "Log-prediction"
];

class GettingStarted extends Component {
  state = {
    view: 1
  };

  componentDidMount() {
    this.track(this.state.view);
  }

  track(view) {
    switch (view) {
      case 1:
        Mixpanel.track(`Getting Started ${pathname[1]}`);
        break;
      case 2:
        Mixpanel.track(`Getting Started ${pathname[2]}`);
        break;
      case 3:
        Mixpanel.track(`Getting Started ${pathname[3]}`);
        break;
      case 4:
        Mixpanel.track(`Getting Started ${pathname[4]}`);
        break;
      default:
        Mixpanel.track("Getting Started Page");
        break;
    }
  }

  getPlatform = () => {
    if (/Windows/gi.test(navigator.userAgent)) {
      return "Windows";
    }
    if (/Mac/gi.test(navigator.userAgent)) {
      return "Mac";
    }
    if (/Linux/gi.test(navigator.userAgent)) {
      return "Linux";
    }
  };

  download = () => {
    this.props.alertMsg("Downloading...", "info");
    const token = this.props.token;
    getAllArtifacts(token)
      .then(res => {
        if (res.status === 200) {
          const platform = this.getPlatform();
          const files = res.data[platform];
          downloadArtifact(token, platform, files[0])
            .then(res => {
              if (res.status === 200) {
                this.props.alertMsg("", "", false);
                FileDownload(res.data, files[0]);
                Mixpanel.track("Download Artifact Successful", {
                  filename: files[0]
                });
              } else {
                const errorMessage =
                  getResponseError(res) ||
                  "An error occurred, please try again.";
                Mixpanel.track("Download Artifact Failed", {
                  filename: files[0],
                  error: errorMessage
                });
                this.props.alertMsg(errorMessage, "error");
              }
            })
            .catch(err => {
              const errorMessage =
                getErrorMessage(err) || "An error occurred, please try again.";
              Mixpanel.track("Download Artifact Failed", {
                filename: files[0],
                error: errorMessage
              });
              this.props.alertMsg(errorMessage, "error");
            });
        } else {
          const errorMessage =
            getResponseError(res) || "Unable to fetch files.";
          Mixpanel.track("Get Artifacts Failed", { error: errorMessage });
          this.props.alertMsg(errorMessage, "error");
        }
      })
      .catch(err => {
        const errorMessage = getErrorMessage(err) || "Unable to fetch files.";
        Mixpanel.track("Get Artifacts Failed", { error: errorMessage });
        this.props.alertMsg(errorMessage, "error");
      });
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const url = nextProps.location.pathname;
    if (url) {
      if (url.includes(pathname[1])) {
        return { view: 1 };
      }
      if (url.includes(pathname[2])) {
        return { view: 2 };
      }
      if (url.includes(pathname[3])) {
        return { view: 3 };
      }
      if (url.includes(pathname[4])) {
        return { view: 4 };
      }
    }
    return null;
  }

  changeView(number, option = 0) {
    if ((number < 4 && option > 0) || (number > 1 && option < 0)) {
      number += option;
    }
    this.setState({ view: number });
    this.track(number);
    switch (number) {
      case 1:
        this.props.history.push(`/Getting-Started/${pathname[1]}`);
        break;
      case 2:
        this.props.history.push(`/Getting-Started/${pathname[2]}`);
        break;
      case 3:
        this.props.history.push(`/Getting-Started/${pathname[3]}`);
        break;
      case 4:
        this.props.history.push(`/Getting-Started/${pathname[4]}`);
        break;
      default:
        this.props.history.push("/");
        break;
    }
  }

  render() {
    const { view } = this.state;
    const { path } = this.props.match;
    return (
      <div className="main-container">
        <div className="main-body">
          <div className="container">
            <div className="row">
              <div className="col-sm-12">
                <h2>Getting Started</h2>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-3">
                <Link
                  to={`${path}/${pathname[1]}`}
                  className="link no-decoration"
                  onClick={() => {
                    this.track(1);
                  }}
                >
                  <div
                    className={`card h180 content-center startTab ${
                      view === 1 ? "active" : ""
                    }`}
                  >
                    Install Agent
                  </div>
                </Link>
              </div>
              <div className="col-sm-3">
                <Link
                  to={`${path}/${pathname[2]}`}
                  className="link no-decoration"
                  onClick={() => {
                    this.track(2);
                  }}
                >
                  <div
                    className={`card h180 content-center startTab ${
                      view === 2 ? "active" : ""
                    }`}
                  >
                    Log an Experiment
                  </div>
                </Link>
              </div>
              <div className="col-sm-3">
                <Link
                  to={`${path}/${pathname[3]}`}
                  className="link no-decoration"
                  onClick={() => {
                    this.track(3);
                  }}
                >
                  <div
                    className={`card h180 content-center startTab ${
                      view === 3 ? "active" : ""
                    }`}
                  >
                    Deploy a Model
                  </div>
                </Link>
              </div>
              <div className="col-sm-3">
                <Link
                  to={`${path}/${pathname[4]}`}
                  className="link no-decoration"
                  onClick={() => {
                    this.track(4);
                  }}
                >
                  <div
                    className={`card h180 content-center startTab ${
                      view === 4 ? "active" : ""
                    }`}
                  >
                    Monitor Deployed Models
                  </div>
                </Link>
              </div>
            </div>
          </div>
          <div className="bg-wt mt">
            <div className="getting-started-content container pd-b">
              <div className="row">
                <div className="col-sm-12">
                  <div className="mt pd">
                    <Switch>
                      <Route
                        exact
                        path={`${path}`}
                        component={() => {
                          return <DownloadAgent download={this.download} />;
                        }}
                      />
                      <Route
                        exact
                        path={`${path}/${pathname[1]}`}
                        component={() => {
                          return <DownloadAgent download={this.download} />;
                        }}
                      />
                      <Route
                        exact
                        path={`${path}/${pathname[2]}`}
                        component={LogExperiment}
                      />
                      <Route
                        exact
                        path={`${path}/${pathname[3]}`}
                        component={DeployModel}
                      />
                      <Route
                        exact
                        path={`${path}/${pathname[4]}`}
                        component={LogMonitor}
                      />
                    </Switch>
                  </div>
                  <div className="mt of-auto pd">
                    <div className="float-left">
                      {view > 1 ? (
                        <button
                          className="btn btn-primary btn-sm btn-rounded"
                          type="button"
                          onClick={e => {
                            this.changeView(view, -1);
                          }}
                        >
                          <i className="fa fa-arrow-left"></i> Previous
                        </button>
                      ) : (
                        ""
                      )}
                    </div>
                    <div className="float-right">
                      {view < 4 ? (
                        <button
                          className="btn btn-primary btn-sm btn-rounded"
                          type="button"
                          onClick={e => {
                            this.changeView(view, 1);
                          }}
                        >
                          Next <i className="fa fa-arrow-right"></i>
                        </button>
                      ) : (
                        <button
                          className="btn btn-primary btn-sm btn-rounded"
                          type="button"
                          onClick={e => {
                            this.props.history.push("/", { tutorial: false });
                          }}
                        >
                          Go to Dashboard!
                        </button>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      alertMsg
    },
    dispatch
  );
};

const mapStateToProps = state => {
  return {
    token: state.auth.token
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GettingStarted);
