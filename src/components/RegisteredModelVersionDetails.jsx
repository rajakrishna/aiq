import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import LeftArrow from "../images/left-chevron.png";
import {
    getRegisteredModelDetail,
    downloadAnyModelFile,
    deletingRegisteredModelVersion,
    clearRegisteredModelStore,
    actionDeployRegisteredModel
} from '../ducks/models';
import {
    getDeploymentDetail
} from '../ducks/deployments';
import RegModelDepList from "./models/RegModelDepList";
import RegModelWorkflow from "./models/RegModelWorkflow";
import Library from "./shared/Library";
import { Link } from "react-router-dom";
import moment from "moment";
import User from "./shared/User";
import Mixpanel from "mixpanel-browser";
import { alertMsg } from "../ducks/alertsReducer";
import { CircularProgress } from "@material-ui/core";
import { jobProto } from './Jobs/jobProto';

class RegisteredModelVersionDetails extends Component {

    apiInterval = null;
    timeInterval = 5000;

    state = {
        downloading: false,
        deploying: false,
        registeredModelDetail: {},
        event_id: "",
        currentIndex: 0
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        const events = nextProps.events;
        const { registeredModelDetail, event_id } = prevState;
        
        if(!registeredModelDetail.id) {
            return({
                registeredModelDetail: nextProps.registeredModelDetail,
                currentIndex: nextProps.registeredModelDetail.ml_deployments ? 0 : nextProps.registeredModelDetail.ml_workflows ? 1 : ""
            })
        }
        
        if(events && events.command === "MESSAGE" && events.headers && event_id !== events.headers["message-id"]) {
            const body = JSON.parse(events.body);
            if(nextProps.match.params.id !== body.body.id) return null
            return({
                registeredModelDetail: body.body,
                event_id: events.headers["message-id"]
            })
        }
        return null;
    }

    componentDidMount() {
        this.props.getRegisteredModelDetail({ id: this.props.match.params.id })
    }

    componentWillUnmount() {
        this.props.clearRegisteredModelStore();
    }

    manageDownload = (id) => {
        if (this.state.downloading) return;
        Mixpanel.track('Model Download Button', { 'model_id': id });
        this.setState({
            downloading: true
        })
        this.props.downloadAnyModelFile(id,
            () => {
                this.setState({
                    downloading: false
                })
            },
            (err) => {
                this.setState({
                    downloading: false
                })
                this.props.alertMsg(err, "error")
            });
    }

    handleDelete = (id) => {
        this.props.deletingRegisteredModelVersion(id,
            () => {
                this.props.history.push("/registered-model-versions");
            },
            () => {
                this.props.alertMsg("Unable to delete Model Version, please try again", "error");
            }
        )
    }

    handleDeploy = () => {
        const { deploying, registeredModelDetail } = this.state;
        if (deploying) return;

        this.setState({
            deploying: true
        })

        const deployData = {
            ...jobProto,
            // viewable: false,
            // render_job: true,
            "metadata": {
                "generateName": registeredModelDetail.registered_model_name + "-deploy-",
                "labels": {}
            },
            // registered_model: registeredModelDetail.registered_model_id,
            "name": registeredModelDetail.registered_model_name + "-deploy",
                "spec": {
                "project_id": registeredModelDetail.project_id,
                "project_name": registeredModelDetail.project_name,
                "model_name": registeredModelDetail.registered_model_name,
                "deploy": {
                    "name": registeredModelDetail.registered_model_name,
                    "replicas": "1",
                    "source": {
                        "type": "REGISTERED_MODEL",
                        "runtime": registeredModelDetail.runtime,
                        "registered_model": {
                        "registered_model_version_id": registeredModelDetail.id,
                    },
                    "requirements": registeredModelDetail.dependencies
                    },
                    "sub_type": "DEPLOY_ARTIFACT",
                    "type": "DEPLOY"
                }
            }
        }

        // this.props.history.push("/workflows/new", deployData);

        this.props.actionDeployRegisteredModel(deployData,
            () => {
                // this.runDeploymentLoop(this.props.deployedModel);
                this.setState({
                    deploying: false
                })
            },
            () => {
                this.setState({
                    deploying: false
                })
                this.props.alertMsg("Deployment unsuccessful, please try again!", "error");
            }
        )
    }

    runDeploymentLoop = (deployedModel) => {
        const status = deployedModel.status.state;
        const name = deployedModel.metadata.name;
        setTimeout(()=>{
            this.fetchingDeployment(status, name);
        }, this.timeInterval);
    }

    fetchingDeployment = (status, name) => {
        if(status !== "Available") {
            this.props.getDeploymentDetail(name,
            () => {
                this.runDeploymentLoop(this.props.deployment)
            })
        } else {
            this.setState({
                deploying: false
            })
        }
    }

    fetchDeploymentBtn = () =>{
        const {
            deploying,
            event_id,
            registeredModelDetail
        } = this.state;
        return (
            <RegModelWorkflow 
                handleDeploy={this.handleDeploy} 
                deploying={deploying} 
                key={"mlworkflow"+event_id+registeredModelDetail.last_mlworkflow_run} 
                registeredModelDetail={registeredModelDetail} 
            />
        );
    }

    render() {
        const { loading } = this.props;
        const { downloading, registeredModelDetail, deploying, currentIndex, event_id } = this.state;
        if (loading) return (<div />);
        else
            return (
                <div className="main-container">
                    <div className="main-body" style={{ overflow: "hidden" }}>
                        <div className="content-wraper-sm">
                            <div className="m-details-card">
                                <div className="m-details-card-head pl-4 mr-5">
                                    {/* <div
                                        className="back-action"
                                        onClick={() => { this.props.history.goBack() }}
                                    >
                                        <img src={LeftArrow} alt="left arrow" />
                                    </div> */}
                                    <div className="m-title-info">
                                        <h4>{registeredModelDetail.registered_model_name}</h4>
                                        <small>
                                            <Link to={"/models/" + registeredModelDetail.registered_model_id}>
                                            {registeredModelDetail.registered_model_name}
                                            </Link>
                                        </small>
                                    </div>
                                    <div className="m-tech-stack">
                                        {registeredModelDetail.type === "FILE" &&
                                        <button
                                            className="btn btn-sm btn-default mr-sm"
                                            type="button"
                                            onClick={() => {
                                                if (registeredModelDetail.model_file_id) {
                                                    this.manageDownload(registeredModelDetail.model_file_id);
                                                }
                                            }}
                                        >
                                            {downloading ?
                                                <CircularProgress size={14} color={"inherit"} className="v-middle" />
                                                : <i className="fa fa-download" />}
                                        </button>}
                                        {!registeredModelDetail.last_mlworkflow_run ?
                                        <button
                                            className="btn btn-sm btn-default mr-sm"
                                            type="button"
                                            onClick={this.handleDeploy}
                                        >
                                            {deploying && <CircularProgress size={14} color={"inherit"} className="v-middle" />} Deploy
                                        </button>
                                        : this.fetchDeploymentBtn()}
                                        <button
                                            className="btn btn-sm btn-danger"
                                            type="button"
                                            onClick={() => {
                                                this.handleDelete(registeredModelDetail.id);
                                            }}
                                        >
                                            <i className="fa fa-trash" />
                                        </button>
                                    </div>
                                </div>
                                <div className="m-info-table model-details">
                                    <table className="table data-table no-border modeldetails-table">
                                        <thead>
                                            <tr>
                                                <td>Version</td>
                                                <td>Type</td>
                                                {registeredModelDetail.ml_algorithm &&
                                                <td>Algorithm</td>}
                                                {registeredModelDetail.ml_library && !(/NOT_SET/ig).test(registeredModelDetail.ml_library) &&
                                                <td>Stack</td>}
                                                <td>Created By</td>
                                                <td>Created Date</td>
                                                <td>Last Modified</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{registeredModelDetail.version}</td>
                                                <td>{registeredModelDetail.type}</td>
                                                {registeredModelDetail.ml_algorithm &&
                                                <td>{registeredModelDetail.ml_algorithm}</td>}
                                                {registeredModelDetail.ml_library && !(/NOT_SET/ig).test(registeredModelDetail.ml_library) &&
                                                <td className="model-library">
                                                    <div className="project-members-info">
                                                        <ul className="project-members-list">
                                                            <Library library={registeredModelDetail.ml_library} />
                                                        </ul>
                                                    </div>
                                                </td>}
                                                <td>
                                                    <div className="project-members-info">
                                                        <ul className="project-members-list">
                                                            <User user={registeredModelDetail.created_by} />
                                                        </ul>
                                                    </div>
                                                </td>
                                                <td>
                                                    {registeredModelDetail.created_date &&
                                                        moment(registeredModelDetail.created_date).format("LL")}
                                                </td>
                                                <td>
                                                    {registeredModelDetail.last_modified_date &&
                                                        moment(registeredModelDetail.last_modified_date).format("LL")}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div className="pl-2">
                                        <h5 className="mt-lg">Properties</h5>
                                        <table className="table data-table no-border properties-table">
                                            <tbody>
                                            {registeredModelDetail.mlflow_experiment && registeredModelDetail.mlflow_experiment.host &&
                                                <tr>
                                                    <td>ML Flow Host:</td> 
                                                    <td>
                                                        <a
                                                            className="clip-text mxwd250 inline-block"
                                                            href={registeredModelDetail.mlflow_experiment.host}
                                                            target="_blank"
                                                        >
                                                            {registeredModelDetail.mlflow_experiment.host}
                                                        </a>
                                                    </td>
                                                </tr>}
                                                {registeredModelDetail.mlflow_experiment && registeredModelDetail.mlflow_experiment.experiment_id &&
                                                <tr>
                                                    <td>ML Flow Experiment:</td> 
                                                    <td>
                                                        <a
                                                            href={registeredModelDetail.mlflow_experiment.host+"/#/experiments/"+registeredModelDetail.mlflow_experiment.experiment_id}
                                                            target="_blank"
                                                        >
                                                            {registeredModelDetail.mlflow_experiment.experiment_id}
                                                        </a>
                                                    </td>
                                                </tr>}
                                                {registeredModelDetail.mlflow_experiment && registeredModelDetail.mlflow_experiment.run_id &&
                                                <tr>
                                                    <td>ML Flow Run:</td> 
                                                    <td>
                                                        <a
                                                            href={registeredModelDetail.mlflow_experiment.host+"/#/experiments/"+registeredModelDetail.mlflow_experiment.experiment_id+"/runs/"+registeredModelDetail.mlflow_experiment.run_id}
                                                            target="_blank"
                                                        >
                                                            {registeredModelDetail.mlflow_experiment.run_id}
                                                        </a>
                                                    </td>
                                                </tr>}
                                                {registeredModelDetail.runtime &&
                                                <tr>
                                                    <td>Runtime:</td> <td>{registeredModelDetail.runtime}</td>
                                                </tr>}
                                                {registeredModelDetail.experiment && registeredModelDetail.experiment.id &&
                                                    <tr>
                                                        <td>Experiment:</td>
                                                        <td>
                                                            <Link
                                                                to={"/monitoring/" + registeredModelDetail.experiment.id}
                                                            >
                                                                {registeredModelDetail.experiment.id}
                                                            </Link>
                                                        </td>
                                                    </tr>}
                                                    {registeredModelDetail.dependencies &&
                                                <tr>
                                                    <td>Dependencies:</td> <td>{registeredModelDetail.dependencies}</td>
                                                </tr>} 
                                                {registeredModelDetail.experiment && registeredModelDetail.experiment.requirements &&
                                                    <tr>
                                                        <td>Requirements:</td>
                                                        <td>
                                                            {registeredModelDetail.registeredModelDetail.experiment.requirements}
                                                        </td>
                                                    </tr>}
                                                {registeredModelDetail.s3 &&
                                                    <tr>
                                                        <td>Bucket:</td>
                                                        <td>
                                                            {registeredModelDetail.s3.bucket}
                                                        </td>
                                                    </tr>}
                                                {registeredModelDetail.s3 &&
                                                    <tr>
                                                        <td>Key:</td>
                                                        <td>
                                                            {registeredModelDetail.s3.key}
                                                        </td>
                                                    </tr>}
                                                {registeredModelDetail.s3 &&
                                                    <tr>
                                                        <td>AWS Secret:</td>
                                                        <td>
                                                            {registeredModelDetail.s3.aws_secret}
                                                        </td>
                                                    </tr>}
                                            </tbody>
                                        </table>
                                    </div>
                                    {registeredModelDetail.ml_deployments && registeredModelDetail.ml_deployments.length ?
                                    <div>
                                        <hr />
                                        <div>
                                            <ul className="inline-tabs tabs-blue mt-xl p-0 inline-block">
                                                {registeredModelDetail.ml_deployments && registeredModelDetail.ml_deployments.length ?
                                                    <li className={`react-tabs__tab${currentIndex === 0 && "--selected"}`} onClick={()=>{this.setState({currentIndex: 0})}}>Deployments</li> : ""}
                                                {/* {registeredModelDetail.ml_workflows && registeredModelDetail.ml_workflows.length ?
                                                    <li className={`react-tabs__tab${currentIndex === 1 && "--selected"}`} onClick={()=>{this.setState({currentIndex: 1})}}>Workflows</li> : ""} */}
                                            </ul>
                                                {registeredModelDetail.ml_deployments && registeredModelDetail.ml_deployments.length && currentIndex === 0 ?
                                                    <RegModelDepList key={"deployments"+event_id} mlDeployments={registeredModelDetail.ml_deployments} /> 
                                                    : ""
                                                }
                                                {/* {registeredModelDetail.ml_workflows && registeredModelDetail.ml_workflows.length && currentIndex === 1 ?
                                                    <RegModelWorkflow key={"mlworkflow"+event_id} mlWorkflows={registeredModelDetail.ml_workflows} /> 
                                                    : ""
                                                } */}
                                        </div>
                                    </div> : ""}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token,
        user: state.users.user,
        deployment: state.deployments.deployment,
        registeredModelDetail: state.models.registeredModelDetail,
        deployedModel: state.models.deployedModel,
        loading: state.models.loader,
        events: state.common && state.common.events
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            deletingRegisteredModelVersion,
            actionDeployRegisteredModel,
            clearRegisteredModelStore,
            getRegisteredModelDetail,
            downloadAnyModelFile,
            getDeploymentDetail,
            alertMsg
        },
        dispatch
    );
};


export default connect(mapStateToProps, mapDispatchToProps)(RegisteredModelVersionDetails);
