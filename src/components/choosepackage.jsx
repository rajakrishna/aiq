import React, { Component } from "react";
import { connect } from "react-redux";
import { Config } from "./Notebook.config.js";
import sklearnlogo_logo from "../images/library/sklearnlogo.png";
import tensorflowlogo_logo from "../images/library/tensorflowlogo.jpg";
import Rlogo_logo from "../images/library/Rlogo.png";
import julialogo_logo from "../images/library/julialogo.png";
import spark_logo_logo from "../images/library/spark-logo.png";
import { Radio, RadioGroup, FormControlLabel } from "@material-ui/core";
import Divider from "@material-ui/core/Divider";
import lightBlue from "@material-ui/core/colors/lightBlue";
import {
  withStyles,
  MuiThemeProvider,
  createMuiTheme,
} from "@material-ui/core/styles";

const theme = createMuiTheme({
  palette: {
    primary: lightBlue,
  },
  typography: {
    useNextVariants: true,
  },
});
class ChoosePackageForm extends Component {
  state = {
    packages: Config.packages,
    imageType: "",
    standardImages: "",
  };

  constructor(props) {
    super();
    this.state.standardImages = props.package.standardImages;
    this.setState({
      standardImages: props.package.standardImages,
    });
  }
  handleChange = (e) => {
    const target = e.target;
    this.setState({
      standardImages: target.value,
    });
    this.props.setPackage(e);
  };

  render() {
    return (
      <div className="package-content">
        <form>
          <div className="pd-lg pl-0">
            <div className="step-title">Step 1: Choose a Notebook Package</div>

            <div className="mt-lg step-description">
              A Notebook Package is a template that contains the software
              configuration required to launch your notebook.
            </div>

            <div className="mt-48">
              {this.state.packages.map((item) => (
                <RadioGroup
                  aria-label="standardImages"
                  name="standardImages"
                  value={this.props.package.standardImages}
                  onChange={this.handleChange}
                  row
                >
                  <div className="mt-3 package-row">
                    {item.package ==
                    "gcr.io/aiops-224805/scipy-notebook:latest" ? (
                      <div>
                        <img
                          className="img-scipy"
                          src={sklearnlogo_logo}
                          alt=""
                        />
                      </div>
                    ) : (
                      <div></div>
                    )}
                    {item.package ==
                    "gcr.io/aiops-224805/tensorflow-notebook:latest" ? (
                      <div>
                        <img
                          className="package-img1"
                          src={tensorflowlogo_logo}
                          alt=""
                        />
                      </div>
                    ) : (
                      <div></div>
                    )}
                    {item.package == "gcr.io/aiops-224805/r-notebook:latest" ? (
                      <div className="package-img2">
                        <img className="img-scipy" src={Rlogo_logo} alt="" />
                      </div>
                    ) : (
                      <div></div>
                    )}
                    {item.package ==
                    "gcr.io/aiops-224805/datascience-notebook:latest" ? (
                      <div className="img-scipy">
                        <img
                          className="package-img2"
                          src={julialogo_logo}
                          alt=""
                        />
                      </div>
                    ) : (
                      <div></div>
                    )}
                    {item.package ==
                    "gcr.io/aiops-224805/all-spark-notebook:latest" ? (
                      <div>
                        <img
                          className="package-img3"
                          src={spark_logo_logo}
                          alt=""
                        />
                      </div>
                    ) : (
                      <div></div>
                    )}

                    <div className="package-flex ">
                      <div className="package-name">{item.name}</div>
                      <div className="package-desc">{item.packageDesc}</div>
                      <Divider className="ml-5 mt-3" variant="inset" />
                    </div>

                    <div>
                      <MuiThemeProvider theme={theme}>
                        <FormControlLabel
                          value={item.package}
                          className="package-radio-align"
                          checked={
                            item.package === this.props.package.standardImages
                          }
                          control={<Radio color="primary" />}
                        />
                      </MuiThemeProvider>
                    </div>
                  </div>
                </RadioGroup>
              ))}
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
  };
};

export default connect(mapStateToProps)(ChoosePackageForm);
