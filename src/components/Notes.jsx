import React, { Component, createRef } from "react";
import { Tooltip, CircularProgress } from "@material-ui/core";
import Quill from 'quill';
import "quill/dist/quill.snow.css";
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';

export default class Notes extends Component {
    state = {
        notes: "",
        notesEditable: "",
        editable: this.props.editable
    }
    noteRef = createRef();
    quill = {}
    toolbarRef = createRef();

    static getDerivedStateFromProps(nextProps) {
        return({
            notes: nextProps.notes
        })
    }

    componentDidMount() {
        this.quill = new Quill(this.noteRef.current, 
            {
                modules: {
                    toolbar: "#toolbar"
                },
                placeholder: 'Click on the edit icon to edit!',
                readOnly: false,
                theme: 'snow'
            });
        let notes = "";
        try {
            notes = JSON.parse(this.state.notes);
        } catch(err) {
            notes = ""
        }
        this.quill.setContents(notes);
        this.quill.on('text-change', this.editNotes);
        this.quill.enable(this.state.editable);
    }

    submitNotes = () => {
        const content = this.quill.getContents();
        this.props.saveNotes(JSON.stringify(content))
    }

    editNotes = (value, delta, source) => {
        if(source === 'api'){
            return;
        }
        // this.quill.setContents(value);
        const content = this.quill.getContents();
        this.setState({
            notesEditable: JSON.stringify(content)
        });
    }

    render(){
        const { editable } = this.state;
        const { loader } = this.props;
        return(
            <div>
                <div id="toolbar" ref={this.toolbarRef} style={editable ? {display: ""} : {display: "none"}}>
                    <select className="ql-size">
                        <option value="small"></option> 
                        <option selected></option>
                        <option value="large"></option>
                        <option value="huge"></option>
                    </select>
                    <button className="ql-bold"></button>
                    <button className="ql-italic"></button>
                    <button className="ql-underline"></button>
                    <button className="ql-list" value="ordered"></button>
                    <button className="ql-list" value="bullet"></button>
                    <select className="ql-color">
                        <option value="black"></option>
                        <option value="red"></option>
                        <option value="blue"></option>
                        <option value="green"></option>
                    </select>
                </div>
                <div 
                    className="notesContainer" 
                    style={editable ? 
                        {height: 300, width: "100%"} :
                        {height: 300, width: "100%", border: "1px solid #ccc"}
                    } 
                    ref={this.noteRef}
                >
                </div>
                {this.props.userName === this.props.owner ?
                <div className="mt">
                    {!editable ? 
                        <button
                            className="btn btn-sm btn-default"
                            type="button"
                            onClick={()=>{
                                const condition = editable;
                                if(this.props.userName !== this.props.owner) {
                                    return;
                                }
                                this.setState({
                                    editable: !condition
                                },()=>{
                                    this.quill.enable(!condition);
                                })
                            }}
                        >
                            <i className="fa fa-edit" /> Edit
                        </button>
                    : 
                    <button
                        className="btn btn-sm btn-default"
                        type="button"
                        onClick={()=>{
                            const condition = editable;
                            this.setState({
                                editable: !condition
                            },()=>{
                                this.quill.enable(!condition);
                            })
                            this.submitNotes();
                        }}
                    >
                        <i className="fa fa-save" /> Save {loader && <CircularProgress size={13} disableShrink />}
                    </button>}
                </div> : ""}
            </div>
        )
    }
}