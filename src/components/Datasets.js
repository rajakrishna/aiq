import React, { Component } from 'react';
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import { enabled } from "../easyAiRedux/DataSourceData";


const styles = (theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "start",
    overflow: "hidden",
    padding: 2,
  },
  card: {
    display: "flex",
    width: 400,
    height: 136,
    marginBottom: 25,
    marginRight: 30,
    boxShadow: 'none',
    border: '1px solid #e0e0e4'
  },
  details: {
    display: "flex",
    flexDirection: "column",
    width: 265
  },
  content: {
    flex: "1 0 auto"
  },
  cover: {
    width: 138,
  },
  title: {
    fontWeight: 600,
    fontSize: 19,
    '&:hover': {
      cursor: "pointer",
      color: "blue"
    }
  },
  subtitle: {
    fontWeight: 550,
    lineHeight: 1,
    fontSize: 12
  },
  body: {
    width: 230,
    maxWidth: 230,
    maxHeight: 42,
    overflow: 'hidden',
    marginTop: 8,
    lineHeight: 1
  },

});



class Datasets extends Component {
  constructor(props) {
    super(props);
    this.state = {
      enabledData: enabled,
      filterType: 'ALL',
      searchQuery: ""
    }
  }

  datasourceDetails = (name, id) => {
    this.props.history.push(`/datasets/${name}`, { ref_id: id })
  }

  handleTypeFilter = (value) => {
    this.setState({ filterType: value }, () => {
      this.setState({ enabledData: (this.state.filterType === 'ALL') ? enabled : enabled.filter((obj) => obj.type === this.state.filterType) })
    })
  }

  handleSearch = (value) => {
    this.setState({ searchQuery: value }, () => {
      this.setState({ enabledData: enabled.filter((item) => item.name.toLowerCase().includes(this.state.searchQuery)) })
    })
  }


  render() {
    const { classes } = this.props;
    const dropdownData = [... new Set(enabled.map((item) => item.type))]
    return (
      <div className="main-container">
        <div className="main-body">
          <div className="content-wraper-sm">
          <div className="">
              <div className="card-header header-with-search pl-4 pt-4">
                <h4 className="datasets_title">DATASETS</h4>
                {/* <div className="projects-search">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Search datasource"
                    value={this.state.searchQuery}
                    onChange={(event) => this.handleSearch(event.target.value)}
                  />
                </div> */}
                {/* <div className="project-tools mr-5">
                  <button
                    className="btn btn-primary mr-2"
                    onClick={() => this.props.history.push("/datasets/datasource_catalog")}
                  >
                    New Datasource
                  </button>
                  <button
                    className="btn btn-default"
                  >
                    <i className="fa fa-sync-alt" />
                  </button>
                </div> */}
              </div>
              </div>
          <div className="card-body h-min-370">
            <div className="empty-items-box">
              <h4>Connect and manage your datasets from various sources</h4>
              <h4>Coming soon!</h4>
            </div>
          </div>
            {/* <div className="card exp-list">
              <div className="card-header header-with-search pl-4">
                <h4 className="datasets_title">DATASETS</h4>
                <div className="projects-search">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Search datasource"
                    value={this.state.searchQuery}
                    onChange={(event) => this.handleSearch(event.target.value)}
                  />
                </div>
                <div className="project-tools mr-5">
                  <button
                    className="btn btn-primary mr-2"
                    onClick={() => this.props.history.push("/datasets/datasource_catalog")}
                  >
                    New Datasource
                  </button>
                  <button
                    className="btn btn-default"
                  >
                    <i className="fa fa-sync-alt" />
                  </button>
                </div>
              </div>
              <div className="model-filter-header pl-4 mr-5">
                <div
                  className="filter-btn"
                  style={{ flexBasis: 211 }}
                >
                  <button
                    className="btn btn-primary btn-sm btn-rounded mr"
                    data-micromodal-trigger="modal-1"
                  >
                    Add Filter +
                  </button>
                </div>
                <div className="applied-filters">
                  <ul className="filter-items-list">
                  </ul>
                </div>
                <div className="filter-right-actions">
                  <div className="row align-items-center justify-content-between">
                    <div className="col">
                      <label><small>Sort By Type</small></label>
                      <select
                        className="form-control select-field"
                        type="select"
                        value={this.state.filterType}
                        onChange={(event) => this.handleTypeFilter(event.target.value)}
                      >
                        <option value="ALL" defaultValue="ALL">All</option>
                        {
                          dropdownData.map((item) => {
                            return (

                              <option value={item} key={item}>{item}</option>
                            )
                          }
                          )
                        }
                      </select>
                    </div>
                  </div>

                </div>
              </div>
              <div className="card-body p-3">
                <div className={classes.root}>
                  {
                    this.state.enabledData.map((eachItem) => {
                      return (
                        <Card className={classes.card} key={eachItem.ref_id}>
                          <CardMedia
                            className={classes.cover}
                            image={eachItem.imgUrl}
                            title="Live from space album cover"
                          >
                          </CardMedia>
                          <div className={classes.details}>
                            <CardContent className={classes.content}>
                              <Typography
                                variant="h4"
                                className={classes.title}
                                onClick={() => this.datasourceDetails(eachItem.name, eachItem.ref_id)}
                              >
                                {eachItem.name}
                              </Typography>
                              <Typography variant="subtitle1" color="textSecondary" className={classes.subtitle}>
                                {eachItem.type}
                              </Typography>
                              <Typography variant="body2" className={classes.body}>
                                {eachItem.desc}
                              </Typography>
                            </CardContent>
                          </div>
                        </Card>
                      )
                    })
                  }

                </div>
              </div>
            </div> */}
          </div>
        </div>
      </div>
    );
  }
}

Datasets.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired
};

export default withStyles(styles, { withTheme: true })(Datasets);