import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import LeftArrow from "../images/left-chevron.png";
import { getRegisteredModels, getRegisteredModelsVersion, deletingRegisteredModelVersion } from '../ducks/models';
import Library from "./shared/Library";
import moment from "moment";
import User from "./shared/User";
import { Link, withRouter } from "react-router-dom";
import { alertMsg } from "../ducks/alertsReducer";
import CardLoader from "./models/cardLoader";
import Pagination from "./common/pagination";
import { startCase } from "lodash";
import MicroModal from "micromodal";
import FilterRegisteredModelVersion from "./models/FilterRegisteredModelVersion";
import _ from "lodash";

class RegisteredModelVersions extends Component {

    state = {
        projectID: "",
        modelsList: [],
        appliedFilters: {},
        filters: {
            projectName: ""
        },
        currentPage: 0,
        totalModels: 0,
    }

    componentDidMount() {
        // this.props.getRegisteredModelsVersion({ registered_model_id: this.props.match.params.id })
        !this.props.registeredModelId && this.props.getRegisteredModels({idEquals: this.props.match.params.id});
        this.getModelVersions();
    }

    getModelVersions = (filters={...this.state.filters}) => {
        const { currentPage } = this.state;
        const { registeredModelId } = this.props;
        filters.registered_model_id = registeredModelId || this.props.match.params.id;
        filters.size = registeredModelId ? 6 : null;
        this.props.getRegisteredModelsVersion(
            filters,
            (data) => {
                this.setState({
                    totalModels: data.headers["x-total-count"],
                    currentPageSize: data.data.length,
                    currentPage: currentPage
                })
            },
            (err) => {
                this.props.alertMsg("Unable to fetch Model, please try again", "error")
            }
        );
    }

    handleInputChange = e => {
        let filters = { ...this.state.filters };
        filters[e.target.name] = e.target.value;
        this.applyFilters(filters)
    };
    
    applyFilters = filters => {
        this.setState({ filters, currentPage: 1 });
        this.getModelVersions(_.pickBy(filters, _.identity));
    }; 

    handleRemoveFilter = filter => {
        let updatedFilters = { ...this.state.filters };
        updatedFilters[filter] = null;
        this.setState({ filters: updatedFilters });
        this.applyFilters(updatedFilters);
    };

    handlePageChange = pageAction => {
        let pageNumber = this.state.currentPage;
        pageAction === "nextPage" ? (pageNumber += 1) : (pageNumber -= 1);
        let params = { page: pageNumber, ...this.state.filters };
        this.getModelVersions(params);
    };
    
    getFilterTab = (data) => {
        switch (typeof (data)) {
            case "object":
                if (data.value) {
                    return `${data.key}-${data.value}`
                }
                if (data.key == "isBetween") {
                    return `${data.key}-${data.value1}&${data.value2}`
                }
                if (data.value1) {
                    return `${data.key}-${data.value1}`
                }
                break;
            default:
                return data
        }
    }


    renderTable = (registeredModelVersion, loader) => {
        return (
            <div className="card-body h-min-370 pl-0">
                <table className="table data-table no-border">
                    <thead>
                        <tr>
                            <td className="pl-0">Name</td>
                            <td>Version</td>
                            <td>Type</td>
                            <td>Runtime</td>
                            <td>Algorithm</td>
                            <td>Stack</td>
                            <td>Owner</td>
                            <td>Created Date</td>
                            <td>Last Modified</td>
                            {/* <td></td> */}
                        </tr>
                    </thead>
                    <tbody>
                        {registeredModelVersion && registeredModelVersion.map((registeredModel, index) => {
                            return (
                                <tr key={"registered" + index}>
                                    <td className="pl-0" style={{ minWidth: 150 }}>
                                        <Link to={`/registered-model-versions/${registeredModel.id}`}>
                                            {registeredModel.registered_model_name}
                                        </Link>
                                        {/* <p
                                            className="data-text blank-link clickable"
                                            onClick={() => { this.props.history.push('/projects/' + registeredModel.project_name) }}
                                        >
                                            <small>{registeredModel.project_name}</small>
                                        </p> */}
                                    </td>
                                    <td>
                                        {registeredModel.version}
                                    </td>
                                    <td>
                                        {registeredModel.type}
                                    </td>
                                    <td>
                                        {registeredModel.runtime}
                                    </td>
                                    <td>
                                        {registeredModel.ml_algorithm}
                                    </td>
                                    <td>
                                        <div className="project-members-info">
                                            <ul className="project-members-list">
                                                <Library library={registeredModel.ml_library} />
                                            </ul>
                                        </div>
                                    </td>
                                    <td>
                                        {registeredModel.created_by && <User user={registeredModel.created_by} />}
                                    </td>
                                    <td>
                                        {registeredModel.created_date && moment(registeredModel.created_date).format("LL")}
                                    </td>
                                    <td>
                                        {registeredModel.last_modified_date && moment(registeredModel.last_modified_date).format("LL")}
                                    </td>
                                    {/* <td className="p-0">
                                        <button
                                            style={{margin: 6}}
                                            className="btn btn-sm btn-danger"
                                            onClick={()=>{
                                                this.handleDelete(registeredModel.id);
                                            }}
                                        >
                                            <i className="fa fa-trash" />
                                        </button>
                                    </td> */}
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
                {loader && <CardLoader />}
            </div>
        )
    }

    handleDelete = (id) => {
        this.props.deletingRegisteredModelVersion(id, 
            () => {
                this.props.getRegisteredModelsVersion({ registered_model_id: this.props.match.params.id });
            },
            () => {
                this.props.alertMsg("Unable to delete Model Version, please try again", "error");
            } 
        )
    }

    addNewVersion = () => {
        const { registeredModels } = this.props;
        const spec = {
            id: registeredModels[0].id,
            name: registeredModels[0].name,
            description: registeredModels[0].description,
            project_id: registeredModels[0].project_id,
            project_name: registeredModels[0].project_name
        }
        this.props.history.push("/models/new", { spec })
    }

    render() {
        const { registeredModels, registeredModelVersion, loader, registeredModelId } = this.props;
        const { filters, totalModels, currentPage, currentPageSize } = this.state;
        let appliedFilters = _.pickBy(filters, _.identity);
        return (
            <div className={registeredModelId ? "" : "main-container"}>
                <div className="main-body">
                    <div className="content-wraper-sm">
                        <div className={registeredModelId ? "" : "m-details-card"}>
                            {!registeredModelId &&
                            <div>
                                <div className="m-details-card-head pl-4 mr-5">
                                    {/* <div
                                        className="back-action clickable"
                                        onClick={() => { this.props.history.goBack() }}
                                    >
                                        <img src={LeftArrow} alt="left arrow" />
                                    </div> */}
                                    <div className="m-title-info">
                                        <h4>{registeredModels && registeredModels[0] && registeredModels[0].name}</h4>
                                        <small>
                                            <Link to={"/project_details"}>
                                                {registeredModels && registeredModels[0] && registeredModels[0].project_name}
                                            </Link>
                                        </small>
                                    </div>
                                    <div className="m-tech-stack"></div>
                                </div>
                                <div className="m-info-table model-details">
                                    <table className="table data-table no-border modeldetails-table">
                                        <thead>
                                            <tr>
                                                <td>Description</td>
                                                <td>Project Name</td>
                                                <td>Created By</td>
                                                <td>Created Date</td>
                                                <td>Last Modified</td>
                                            </tr>
                                        </thead>
                                        {registeredModels && registeredModels[0] &&
                                        <tbody>
                                            <tr>
                                                <td>{registeredModels[0].description}</td>
                                                <td>{registeredModels[0].project_name}</td>
                                                <td>
                                                    <div className="project-members-info">
                                                        <ul className="project-members-list">
                                                            <User user={registeredModels[0].created_by} />
                                                        </ul>
                                                    </div>
                                                </td>
                                                <td>
                                                    {registeredModels[0].created_date &&
                                                        moment(registeredModels[0].created_date).format("LL")}
                                                </td>
                                                <td>
                                                    {registeredModels[0].last_modified_date &&
                                                        moment(registeredModels[0].last_modified_date).format("LL")}
                                                </td>
                                            </tr>
                                        </tbody>}
                                    </table>
                                </div>
                            <hr />
                            </div>}
                            <div className="pd-lr-lg pl-4">
                                <div className="header-with-search pd-lr-lg pl-0">
                                    <h5 className="wd200 mt">Versions</h5>
                                    <div className="card-search">
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="nameContains"
                                            value={filters.nameContains || ""}
                                            placeholder="SEARCH"
                                            onChange={(e) => {
                                                filters.nameContains = e.target.value
                                                this.setState({ filters })
                                                //this.applyFilters(filters)
                                            }}
                                            onKeyPress={event => {
                                                if (event.key === "Enter") {
                                                    this.handleInputChange(event)
                                                }
                                            }}
                                        />
                                    </div>
                                    <div className="card-tools mr-5">
                                        <div className="model-pagination inline-flex">
                                            {totalModels > 20 && (
                                                <Pagination
                                                    itemsCount={totalModels}
                                                    currentPage={currentPage}
                                                    currentPageSize={currentPageSize}
                                                    pageSize={20}
                                                    onPageChange={this.handlePageChange}
                                                />
                                            )}
                                        </div>
                                        <div className="model-pagination inline-flex">
                                            <button className="btn btn-sm btn-primary ml" onClick={() => { this.addNewVersion() }} type="button">New Version</button>
                                            <button className="btn btn-sm btn-default ml" onClick={() => { this.getModelVersions(_.pickBy(this.state.filters, _.identity)); }} type="button"><i className="fa fa-sync-alt" /></button>
                                        </div>
                                    </div>
                                </div>
                                <div className="model-filter-header pd-lr-lg mr-5 pl-0">
                                    <div className="filter-btn">
                                        <button
                                            className="btn btn-primary btn-sm btn-rounded"
                                            onClick={() => MicroModal.show('modal-version')}
                                        >
                                            Add Filter +
                                        </button>
                                    </div>
                                    <div className="applied-filters">
                                        <ul className="filter-items-list">
                                            {!_.isEmpty(appliedFilters) &&
                                                Object.keys(appliedFilters).map(key => (
                                                    <li key={key} className="filter-tag-item">
                                                        {startCase(key)}-{this.getFilterTab(appliedFilters[key])}
                                                        <i className="fa fa-times" style={{ marginTop: 3 }} onClick={() => this.handleRemoveFilter(key)}>
                                                        </i>
                                                    </li>
                                                ))}
                                        </ul>
                                    </div>
                                    <div className="filter-right-actions">
                                        <div className="row align-items-center justify-content-between">
                                            <div className="col">
                                                <label><small>Sort By</small></label>
                                                <select
                                                    name="sort"
                                                    onChange={this.handleInputChange}
                                                    id=""
                                                    className="form-control select-field"
                                                    value={appliedFilters.sort || ''}
                                                >
                                                    <option value=''>Select</option>
                                                    <option value="name,asc">Name</option>
                                                    <option value="ml_library,asc">Library</option>
                                                    <option value="ml_algorithm,asc">Algorithm</option>
                                                    <option value="created_date,desc">Created</option>
                                                </select>
                                            </div>
                                            <div className="col">
                                                <label><small>Created Date</small></label>
                                                <select
                                                    name="created_date"
                                                    id=""
                                                    onChange={this.handleInputChange}
                                                    className="form-control select-field"
                                                    value={appliedFilters.created_date || ''}
                                                >
                                                    <option value=''>Select</option>
                                                    <option value="now-1d">Last 1 day</option>
                                                    <option value="now-7d">Last 7 days</option>
                                                    <option value="now-30d">Last Month</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-body no-pad">
                                    {
                                       !loader ? 
                                        this.renderTable(registeredModelVersion, loader)
                                       : <CardLoader />
                                    }
                                </div>
                            </div>
                            </div>
                        <div>
                        </div>
                    </div>
                </div>
                <FilterRegisteredModelVersion
                    filterType={"models"}
                    applyFilters={this.applyFilters}
                    filters={this.state.filters}
                    project={(/project/).test(this.props.match.path)}
                />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.users.user,
        token: state.auth.token,
        loader: state.models.loader,
        registeredModelVersion: state.models.registeredModelVersion,
        registeredModels: state.models.registeredModels,
        loader: state.models.loader
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            getRegisteredModels,
            getRegisteredModelsVersion,
            deletingRegisteredModelVersion,
            alertMsg
        },
        dispatch
    );
};


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(RegisteredModelVersions));
