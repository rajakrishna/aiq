import React, { Component } from 'react';
class Group extends Component {
    state = {
        condName: "",
        conditions: "",
        condValue: ""
    }
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if(!prevState.condName) {
            const condName = nextProps.name;
            return { condName: condName };
        }
        return null;
    }

    handleChange(e) {
        const target = e.target;
        const name = target.name;
        const value = target.value;
        this.setState({
            [name]: value
        })
    }

    render() {
        return(
            <div className="bd w-bg pd-sm mt">
                <div className="wd310 bd bd-r inline-block mr-sm">
                    <div className="wd145 inline-block mr-sm">
                        <input className="form-control pd-sm h-auto f13" value={this.state.condName} onChange={this.handleChange} placeholder="Property" name="condName" />
                    </div>
                    <div className="wd145 inline-block">
                        <select className="form-control h-auto f13" style={{padding: 7}} value={this.state.conditions} onChange={this.handleChange} placeholder="Conditions" name="conditions" >
                        
                            <option value="Less Than">Less Than</option>
                            <option value="Greater Than">Greater Than</option>
                            <option value="Equals To">Equals To</option>
                            <option value="Less or Equals">Less or Equals</option>
                            <option value="Greater or Equals">Greater or Equals</option>
                        
                        </select>
                    </div>
                </div>
                <div className="wd100 inline-block">
                    <input className="form-control pd-sm h-auto f13" value={this.state.condValue} onChange={this.handleChange} placeholder="Value" name="condValue" />
                </div>
                <div className="float-right">
                    <button className="btn btn-sm btn-danger" onClick={()=>{this.props.deleteMe(this.props.index)}}>Delete</button>
                </div>
            </div>
        )
    }
}

export default Group;