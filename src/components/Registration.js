import React, { Component } from "react";
import { Link } from "react-router-dom";

class Registration extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="text-center">
        <div className="mt-5">
          <i class="fa fa-check-circle h1" aria-hidden="true"></i>

          <h1 className="mt-4">Woohoo!</h1>
          <h1>Successfully Registered</h1>
          <p className="mt-5 mb-1">check your e-mail for log-in details</p>
          <p>
            <span>click on </span>
            <Link to="/sign_in">sign-in</Link>
            <span> to begin</span>
          </p>
        </div>
      </div>
    );
  }
}

export default Registration;
