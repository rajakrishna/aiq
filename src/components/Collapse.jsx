import React from 'react';
import {  withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const styles = (theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
  details: {
      display: "block",
  },
  doubleMarginTop: {
      marginTop: theme.spacing.unit,
  }
});

class ControlledExpansionPanels extends React.Component{
  state = {
    expanded: false,
  };

  componentDidMount() {
    this.setState({expanded: this.props.active});
  }

  handleChange = (event, expanded) => {
    if(event)
        this.setState({
            expanded: expanded ? true : false,
        });
  };

  render() {
    const { classes, children, heading, errorExpand } = this.props;
    const { expanded } = this.state;
    if(children.length > 0 || children.length === undefined) {
        return (
          <div className="mt-lg">
            <ExpansionPanel expanded={expanded || errorExpand} onChange={this.handleChange}>
              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                <Typography variant="h6" gutterBottom className={classes.doubleMarginTop}>{heading}</Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails className={classes.details}>
                  {children}
              </ExpansionPanelDetails>
            </ExpansionPanel>
          </div>
        );
    } else {
        return(
            <div></div>
        )
    }
  }
}


export default withStyles(styles)(ControlledExpansionPanels);