import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getAllArtifacts, downloadArtifact } from "../api";
import { Link } from "react-router-dom";
import { Loader1 } from "./home/modelOverviewLoader";
import FileDownload from "js-file-download";
import { alertMsg } from "../ducks/alertsReducer";
import Mixpanel from "mixpanel-browser";
import { getErrorMessage } from "../utils/helper"

class Help extends Component {

  state = {
    folders: null
  }

  async componentDidMount() {
    Mixpanel.track("Help Page");
    const artifacts = await getAllArtifacts(this.props.token);
    const folders = artifacts.data;
    this.setState({
      folders
    })
  }

  download(folder, file) {
    this.props.alertMsg("Downloading...", "info");
    downloadArtifact(this.props.token, folder, file)
    .then(res => {
      if (res.status === 200) {
        FileDownload(res.data, file);
        Mixpanel.track("Download Artifact Successful", {filename: file});
        this.props.alertMsg("", "", false);
      }
    })
    .catch(error => {
      const errorMessage = getErrorMessage(error) || "An error occurred, please try again.";
      Mixpanel.track("Download Artifact Failed", { filename: file, error: errorMessage});
      this.props.alertMsg(errorMessage, "error");
    });
  }

  createLinks(folders) {
    let count = 0;
    let length = Object.keys(folders).length;
    let full_l = Math.floor(length/3);
    return( 
      Object.keys(folders).map((e, i) => {
      const folder_name = e;
      let len = 1;
      count++;
      if(count > 3) {
        count = 1;
        full_l--;
        length -= 3;
      }
      if(length > 3) {
        len = 3;
      } else {
        len = length;
      }
      return (
        <div className={`col-sm-${Math.floor(12/len)} mt-10`}>
          <h5><b>{folder_name.toLocaleUpperCase()}</b></h5>
          {
            folders[folder_name].map((elem, index) => {
              const file = elem;
              return (
                <a className='help-box__link' key={file} href='JavaScript:void(0)' onClick={() => {
                  this.download(folder_name, file)
                }}>{file}</a>
              )
            })
          }
        </div>
      )
    }));
  }

  render() {
    const { folders } = this.state;
    return (
      <div className="main-container">
        <div className="main-body">
          <div className="container margin-top-8rem">
            <div className='row item-center pd-0'>
              <div className='col-sm-6'>
                <div className='card h-min-370'>
                  <div className='card-body pd-tb-lg text-center'>
                    <div className='help-box__ico help-box__ico--manual' />
                    <h3>Documentation</h3>
                    <Link to='/Getting-Started/' className='help-box__link'> Getting Started</Link>
                  </div>
                </div>
              </div>
              <div className='col-sm-6'>
                <div className='card h-min-370'>
                  <div className='card-body pd-tb-lg text-center'>
                    <div className='help-box__ico help-box__ico--email' />
                    <h3>Contact</h3>
                    <a className='help-box__link' rel="noopener noreferrer" target='_blank' href='mailto:support@predera.com'>Email Support</a>
                    <a 
                      className='help-box__link' 
                      rel="noopener noreferrer" 
                      target='_blank' 
                      href='https://join.slack.com/t/predera/shared_invite/enQtNzg1NDU4MTk2MDA1LTQ2ODg2MWEwYjVjMGI2ODI5YjhjM2ZkNTFjNTUyMTI4MzRhMWY2NDk2ZDRmMzM2NzU0NGFlOGE0ZTkwZGE3Y2Y'
                    >Slack Channel</a>
                  </div>
                </div>
              </div>
              <div className='col-sm-12'>
                <div className='card'>
                  <div className='card-body pd-tb-lg text-center'>
                    <div className='help-box__ico help-box__ico--download' />
                    <h3>Download</h3>
                    {
                      folders ?
                      <div className="container-fluid">
                        <div className="row">
                          {this.createLinks(folders)}
                        </div>
                      </div> :
                      <Loader1 />
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
        alertMsg
    },
    dispatch
  );
};

const mapStateToProps = state => {
  return {
    token: state.auth.token
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Help);
