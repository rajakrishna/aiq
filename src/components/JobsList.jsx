import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getAllJobs, delJob, runJob } from "../api";
import Pagination from "./common/pagination";
import MicroModal from "micromodal";
import CardLoader from "./models/cardLoader";
import FilterJobs from "./Jobs/filterJobs";
import moment from "moment";
import { Link } from "react-router-dom";
import User from "./shared/User";
import { alertMsg } from "../ducks/alertsReducer";
import { confirm } from "./Confirm";
import { startCase } from "lodash";
import _ from "lodash";
import Mixpanel from "mixpanel-browser";
import { getErrorMessage, getResponseError } from "../utils/helper";
import JobrunPopupWithParameters from "./Jobs/job-run/JobrunPopupWithParameters";

class JobsList extends Component {
  state = {
    listType: "WORKFLOWS",
    projectName: "",
    emptyJob: false,
    jobsList: [],
    card_loader: true,
    appliedFilters: {},
    filters: {
      projectName: "",
      healthScore: 0,
      dataDrift: 0,
      typein: ""
    },
    currentPage: 0,
    totalJobs: 0
  };

  constructor(props) {
    super(props);
    if (props.projectName) {
      this.state.projectName = props.projectName;
      this.state.filters.projectName = props.projectName;
    }
  }
  componentDidUpdate(prevProps) {
    if (this.props.selectedProject && prevProps.selectedProject.id !==  this.props.selectedProject.id) {
      this.getJobs({projectIDContains: this.props.selectedProject.id});
    }
  }
  componentDidMount() {
    Mixpanel.track("Workflow Page");
    MicroModal.init({
      disableScroll: true,
      disableFocus: false,
      awaitCloseAnimation: false,
      debugMode: true
    });
    if (this.props.selectedProject && this.props.selectedProject.id) {
      this.getJobs({projectIDContains: this.props.selectedProject.id});
    } else {
      this.getJobs();
    }
  }
  

  runJobAction = (id) => {
    this.props.alertMsg("Submitting...", "info");
    runJob(this.props.token, id)
    .then((res)=>{
      if (res.status === 200) {
        Mixpanel.track("Submit Workflow Run Successful", { 'job_id': id });
        this.props.alertMsg("Submitted!", "success");
      } else {
        const errorMessage = getResponseError(res) || "An error occurred while submitting the Workflow Run, please try again.";
        Mixpanel.track("Submit Workflow Run Failed", { 'job_id': id, error: errorMessage });
        this.props.alertMsg(errorMessage, "error");
      }
    })
    .catch((err)=>{
      const errorMessage = getErrorMessage(err) || "An error occurred while submitting the Workflow Run, please try again.";
      Mixpanel.track("Submit Workflow Run Failed", { 'job_id': id, error: errorMessage });
      this.props.alertMsg(errorMessage, "error");
    })
  }

  delJobAction(id) {
    this.props.alertMsg("Deleting Workflow...", "info");
    delJob(this.props.token, id)
      .then(res => {
        Mixpanel.track("Delete Workflow Successful", { 'job_id': id });
        this.props.alertMsg("Workflow deleted successfully", "success");
        this.getJobs();
      })
      .catch(error => {
        const errorMessage = getErrorMessage(error) || "An error occurred while deleting the Workflow, please try again.";
        Mixpanel.track("Delete workflow Failed", { 'job_id': id, error: errorMessage });
        this.props.alertMsg(errorMessage, "error");
      });
  }

  editJobAction(job) { 
    this.props.history.push("/workflows/"+job.id);
  }

  // getJobs fetches jobs based on different search criteria
  getJobs = (params) => {
    this.setState({ jobsList: [], card_loader: true });
    const defaultParams = {
      sort: "created_date,desc",
      projectIDContains:this.props.selectedProject && this.props.selectedProject.id
    };
    let allParams = { ...defaultParams, ...params };
    let currentPage = allParams.page ? allParams.page : 0;
    getAllJobs(this.props.token, allParams)
      .then(response => {
        if (response.status) {
          // var startPageIndex = this.state.startPageIndex ? this.state.startPageIndex : 0
          this.setState({
            jobsList: response.data,
            card_loader: false,
            emptyJob: response.data.length > 0 ? false : true, 
            totalJobs: response.headers["x-total-count"],
            currentPageSize: response.data.length,
            currentPage: currentPage
          });
        }
      })
      .catch(error => {
        const errorMessage = getErrorMessage(error) || "An error occurred while fetching Workflow.";
        Mixpanel.track("Get Workflow Failed", { error: errorMessage });
        this.props.alertMsg(errorMessage, "error");
      });
  };

  handleInputChange = e => {
    let filters = { ...this.state.filters };
    filters[e.target.name] = e.target.value;
    this.applyFilters(filters)
  };
  
  applyFilters = filters => {
    filters['delete'] = 1;
    this.setState({ filters, currentPage: 1 },()=>{
      filters['delete'] = null;
      this.setState({
        filters
      })
    });
    this.getJobs(_.pickBy(filters, _.identity));
  };
  
  handleRemoveFilter = filter => {
    let updatedFilters = { ...this.state.filters };
    updatedFilters[filter] = null;
    this.applyFilters(updatedFilters);
  };

  handlePageChange = pageAction => {
    let pageNumber = this.state.currentPage;
    pageAction === "nextPage" ? (pageNumber += 1) : (pageNumber -= 1);
    let params = { page: pageNumber, ...this.state.filters };
    this.getJobs(params);
  };

  getFilterTab = (data) => {
    switch(typeof(data)){
      case "object":
        if(data.value){
          return `${data.key}-${data.value}`
        }
        if(data.key == "isBetween"){
          return `${data.key}-${data.value1}&${data.value2}`
        }
        if(data.value1){
          return `${data.key}-${data.value1}`
        }
      break;
      default:
        return data
    }
  }

  renderJobList = (jobsListData) =>{
      const jobsList = jobsListData;
      return (
        <div>
          <div className="card-body">
            <table className="table data-table no-border">
              <thead>
                <tr>
                  <td>Name</td>
                  {/* <td>Type</td> */}
                  <td>Created By</td>
                  <td>Created Date</td>
                  <td>Last Run</td>
                  <td>Number of Runs</td>
                  <td>Scheduled</td>
                  <td></td>
                </tr>
              </thead>
              <tbody>
                {jobsList &&
                  jobsList.map((job, index) => {
                    return (
                      <tr key={index}>
                        <td>
                          <Link to={`/workflows/${job.id}`}>
                            {job.name}
                          </Link>
                        </td>
                        {/* <td>
                          <p className="data-text">
                            <small>
                            {job.type && job.type.replace(/_/g," ").replace(/(\w)(\w{1,})/g,(str,grp1,grp2) => {
                                return(grp1+grp2.toLowerCase());
                            })}
                            </small>
                        </p>
                        </td> */}
                        <td>
                            <div className="project-members-info">
                            <ul className="project-members-list">
                            {job.created_by && <User user={job.created_by} />}
                            </ul>
                          </div>
                        </td>
                        <td>{job.created_date && moment(job.created_date).format("LL")}</td>
                        <td>{job.last_run_date && moment(job.last_run_date).format("LL")}</td>
                        <td>
                            {job.number_of_runs || 0}
                        </td>
                        <td>
                          {job.schedule && !job.schedule.suspend ? 'Yes' : 'No'}
                        </td>
                        <td>
                              {job.spec.parameters && job.spec.parameters.length > 0 ? 
                              <JobrunPopupWithParameters jobId={job.id} parameters={job.spec.parameters} />
                              :
                              <button
                                type="button"
                                className="btn btn-default btn-sm mr-sm"
                                data-toggle="tooltip" title="Run"
                                onClick={()=>{
                                  confirm("Are you sure?", "Run").then(
                                    () => {
                                      this.runJobAction(job.id)
                                    }
                                  )
                                }}
                                disabled={job.schedule && !job.schedule.suspend}
                              >
                                <i className="fa fa-play"></i>
                              </button>
                          }
                        <button
                          type="button"
                          className="btn btn-default btn-sm"
                          data-toggle="tooltip" title="Edit"
                          onClick={()=>{
                            confirm("Are you sure?", "Edit").then(
                              () => {
                                this.editJobAction(job)
                              }
                            )
                          }}
                        >
                          <i className="fa fa-edit"></i>
                        </button>
                        <button
                          type="button"
                          className="btn btn-danger btn-sm ml-sm"
                          data-toggle="tooltip" title="Delete"
                          onClick={()=>{
                            confirm("Are you sure?", "Delete").then(
                              () => {
                                this.delJobAction(job.id)
                              }
                            )
                          }}
                        >
                          <i className="fa fa-trash"></i>
                        </button>
                        </td>
                      </tr>
                    );
                  })}
              </tbody>
            </table>
          </div>
        </div>
      );
  }

  render() {
    const {
      jobsList,
      filters,
      totalJobs,
      currentPage,
      currentPageSize
    } = this.state;
    let appliedFilters = _.pickBy(filters, _.identity);
    const countersVisibility = this.props.countersVisibility;
    return (
      <div className="main-container">
        <div className="main-body">
          <div className="content-wraper-sm">
              <div>
                <div className="card">
                  <div className="card-header header-with-search pl-4 pt-2 mt-1 mr-5">
                    <h4>Workflows</h4>
                    <div className="card-search">
                      <input
                        type="text"
                        className="form-control"
                        placeholder="SEARCH"
                        onKeyPress={event => {
                          if (event.key === "Enter") {
                            let filters = {...this.state.filters}
                            filters.nameContains = event.target.value;
                            this.applyFilters(filters)
                            // this.getJobs(_.pickBy(filters, _.identity));
                          }
                        }}
                      />
                    </div>
                    <div className="card-tools">
                      <div className="inline-flex mr-sm">
                        <Link to="/run-history" className="model-actions" style={{ padding: 7 }}>
                          Run History
                        </Link>
                        <Link to="/workflows/new" className="model-actions">
                          <button 
                          className={`btn bt btn-primary mr clickable ${!this.props.selectedProject ? "disabled":""}` }
                          type="button" 
                          disabled={!this.props.selectedProject}
                          >
                          New Workflow
                          </button>
                        </Link>
                        <button className="btn bt btn-default" data-toggle="tooltip" title="Refresh List" onClick={()=>{this.getJobs(_.pickBy(this.state.filters, _.identity))}} type="button"><i className="fa fa-sync-alt" /></button>
                      </div>
                        {totalJobs > 20 && (
                          <div style={{minWidth: 164}} className="model-pagination inline-flex ml-sm mr-sm">
                            <Pagination
                              itemsCount={totalJobs}
                              currentPage={currentPage}
                              currentPageSize={currentPageSize}
                              pageSize={20}
                              onPageChange={this.handlePageChange}
                            />
                          </div>
                        )}
                    </div>
                  </div>
                  <div className="model-filter-header pl-4 mr-5">
                    <div className="filter-btn">
                      <button
                        className="btn btn-primary btn-sm btn-rounded"
                        data-micromodal-trigger="modal-1"
                      >
                        Add Filter +
                      </button>
                    </div>
                    <div className="applied-filters">
                      <ul className="filter-items-list">
                        {!_.isEmpty(appliedFilters) &&
                          Object.keys(appliedFilters).map(key => (
                            <li key={key} className="filter-tag-item">
                              {startCase(key)}-{this.getFilterTab(appliedFilters[key])}
                              <i className="fa fa-times" style={{marginTop: 3}} onClick={() => this.handleRemoveFilter(key)}>
                              </i>
                            </li>
                          ))}
                      </ul>
                    </div>
                    <div className="filter-right-actions mr-lg">
                      <div className="row align-items-center justify-content-between">
                        <div className="col">
                          <div className="float-right">
                            <label>
                              <small>Sort By</small>
                            </label>
                            <select 
                              name="sort" 
                              id=""
                              onChange={this.handleInputChange}
                              value={appliedFilters.sort || ''}
                              className="form-control select-field mwd150"
                            >
                              <option value="">Select</option>
                              <option value="name,asc">Name</option>
                              {/* <option value="type,asc">Type</option> */}
                              <option value="created_by,asc">Created By</option>
                              <option value="created_date,desc">Created Date</option>
                              <option value="last_run_date,desc">Last Run</option>
                              <option value="number_of_runs,desc">Number of runs</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card-body no-pad">
                    {this.state.card_loader ? <CardLoader /> : ""}
                    {jobsList.length !== 0 && (
                      this.renderJobList(jobsList)
                    )}
                    { jobsList.length === 0 && !this.state.card_loader &&
                      (<div className="empty-items-box">
                      <h4>Sorry you dont have any { _.capitalize(this.state.listType)}!</h4>
                    </div>)
                    }
                  </div>
                </div>
              </div>
          </div>
        </div>
        <FilterJobs
          applyFilters={this.applyFilters}
          filters={this.state.filters}
        />
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
        alertMsg
    },
    dispatch
  );
};

const mapStateToProps = state => {
  return {
    token: state.auth.token,
    user: state.users.user,
    selectedProject: state.projects.globalProject,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(JobsList);
