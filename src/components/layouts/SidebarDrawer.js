import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';

import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import NavigationLinks from '../NavigationLinks';
import SidebarNestedList from './SidebarNestedList';
import Avatar from '@material-ui/core/Avatar';
import ReactTooltip from 'react-tooltip'

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import { logout } from '../../ducks/auth';
import { getUserDetail } from '../../ducks/users';
import { notificationsList, notificationUnreadCounts } from '../../ducks/notifications';
import { markAsRead } from '../../ducks/notifications';
import { eventPosters } from '../../ducks/common';
import { globalProjectSelection, getProjectsSummary } from '../../ducks/projects';

const drawerWidth = 220;

const styles = theme => ({
  root: {
    display: 'flex',
    backgroundColor: '#fff',
    overflowX: 'hidden',
    overflowY: 'hidden',
    height: '100vh',
    overflowY: 'scroll',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    backgroundColor: '#fff',
    color: '#000',
    boxShadow: '0px 1px #e0e0e4',
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    boxShadow: '0px 1px #e0e0e4',
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    overflowY: 'hidden',
    overflowX: 'hidden',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing.unit * 7 + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9 + 1,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: '0px !important',
    height: '4.5rem',
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    // padding: theme.spacing.unit * 3,
    padding: '0px !important',
  },
});

class SidebarDrawer extends React.Component {
  state = {
    open: true,
  };

  handleDrawerToggle = () => {
    this.setState((prevState) => ({ open: !prevState.open }));
  }

  getInitialsProjectName = (projectName="") => {
    projectName = projectName || "";
    const first = projectName.replace(/^(\w).*/,'$1');
    const second = projectName.replace(/^\w.*?\.([A-z]).*?@.*?$/,'$1');
    const str = second.length > 1 ? first : first+second;
    return str.toUpperCase() || '?';
  }

  renderProjectIcon = (projectName) => {
    return (
      <span data-tip='' data-for={projectName} className="no-border pd-0">
        <Avatar className="avatar mt-0 mr-1 rounded">{this.getInitialsProjectName(projectName)}</Avatar>
        {/* {props.isMenu ? <i className="fa fa-angle-down ml-sm" /> : ''} */}
        <ReactTooltip id={projectName} place="bottom" type="dark" getContent={() => { return projectName }}/>
      </span>
    )
  }

  renderBreadCrumbs = () => {
    const pathList = this.props.location.pathname.split("/")
    return (
          <nav aria-label="breadcrumb">
            <ol className="breadcrumb m-0 p-0 bg-white">
              <li className="breadcrumb-item"><Link to="/project_details">{this.props.selectedProject.name}</Link></li>
              {pathList.map((eachItem) => eachItem.length > 0 && eachItem !== pathList[pathList.length - 1] && <li className='breadcrumb-item'><Link to={`/${eachItem}`}>{eachItem}</Link></li>)
              } 
              <li className="breadcrumb-item active" aria-current="page">{pathList[pathList.length-1] === 'project_details' ? 'Project Information' : pathList[pathList.length-1]}</li>
            </ol>
          </nav>
    )
  }

  render = () => {
    const { classes, theme } = this.props;
    return (
        <div className={classes.root}>
          <CssBaseline />
          <AppBar
            position="fixed"
            className={classNames(classes.appBar, {
              [classes.appBarShift]: this.state.open,
            })}
          >
            <NavigationLinks />
            <div style={{marginTop: '80px', overflowY: 'hidden'}}>
              <Toolbar disableGutters={!this.state.open}>
                <IconButton
                  color="inherit"
                  aria-label="Open drawer"
                  className={classNames(classes.menuButton, {
                    [classes.hide]: this.state.open,
                  })}
                >
                  {this.renderProjectIcon(this.props.selectedProject.name)}
                </IconButton>
                {this.renderBreadCrumbs()}
              </Toolbar>
            </div>
          </AppBar>
          <Drawer
            variant="permanent"
            className={classNames(classes.drawer, {
              [classes.drawerOpen]: this.state.open,
              [classes.drawerClose]: !this.state.open,
            })}
            classes={{
              paper: classNames({
                [classes.drawerOpen]: this.state.open,
                [classes.drawerClose]: !this.state.open,
              }),
            }}
            open={this.state.open}
          >
            <div className='header-top-margin'>

              <div className={classes.toolbar}>
                <div>
                  <p style={{maxWidth: '170px'}} className='m-0 p-0 ml-4 pl-1 pt-2 text-truncate'>
                    <Link to="/project_details" className='button' style={{textDecoration: 'none'}}>
                      {this.renderProjectIcon(this.props.selectedProject.name)}
                      <span className='font-weight-bold text-dark'>
                        {this.props.selectedProject.name}
                      </span>
                    </Link>
                  </p>
                </div>
              </div>
              <Divider />
              <SidebarNestedList />
              {/* <div style={{minHeight: '79vh'}}>
              </div>
              <div className='btn w-100 pl-2' onClick={this.handleDrawerToggle}>
                  {this.state.open ? <p className='text-left'>{` << Collapse Sidebar`}</p> : <p className='w-100 text-center'>{` >> `}</p>}
              </div> */}
            </div>
          </Drawer>
          <div className='header-top-margin' style={{minWidth: '86vw'}}>
            <main className={classes.content}>
              <div className={classes.toolbar} />
              {this.props.children}
            </main>
        </div>
      </div>
    );
  }

}

SidebarDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

// export default withStyles(styles, { withTheme: true })(SidebarDrawer);

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    user: state.users.user,
    notifications: state.notification.notifications,
    notifications_count: state.notification.notifications_unread_count,
    workflowCounts: state.jobs.counter,
    modelCounts: state.models.registered_model_counter,
    experimentCounts: state.experiments.counter,
    projectCounts: state.projects.counter,
    projects: state.projects.summary,
    selectedProject: state.projects.globalProject,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators(
      {
        logout,
        getUserDetail,
        notificationsList,
        notificationUnreadCounts,
        markAsRead,
        eventPosters,
        globalProjectSelection,
        getProjectsSummary,
      },
      dispatch
    ),
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles, { withTheme: true })(SidebarDrawer)))
