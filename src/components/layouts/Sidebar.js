import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import logo from "../../images/predera-logo.png";
import User from "../shared/User";
import sample_company_logo from "../../images/company.svg";
import { toast } from "react-toastify";
import { listenNotifications, listenEvents } from "../../api";
import notification_icon from "../../images/notification-icon.svg";
import moment from "moment";
import { logout } from "../../ducks/auth";
import {
  notificationsList,
  notificationUnreadCounts,
  markAsRead,
} from "../../ducks/notifications";
import { getUserDetail } from "../../ducks/users";
import { getProjectsSummary } from "../../ducks/projects";
import { eventPosters } from "../../ducks/common";
import Mixpanel from "mixpanel-browser";
import { l } from "../../utils/helper";
import { Badge } from "@material-ui/core";
import MicroModal from "micromodal";
import ExpiryModalFullScreen from "../modals/ExpiryModalFullScreen";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import { globalProjectSelection } from "../../ducks/projects";
import FormControl from "@material-ui/core/FormControl";
import Button from "@material-ui/core/Button";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import MenuList from "@material-ui/core/MenuList";

const theme = createMuiTheme({
  overrides: {
    MuiSelect: {
      select: {
        paddingLeft: "20px !important",
        maxWidth: "100px",
      },
      icon: {
        color: "#3D69F9",
      },
    },
    MuiMenuItem: {
      root: {
        "&$selected": {
          backgroundColor: "#e6eefc !important",
          color: "blue !important",
        },
      },
    },
  },
  typography: {
    useNextVariants: true,
    fontFamily: "inherit",
    letterSpacing: "inherit",
    button: { textTransform: "none", fontSize: [16, "!important"] },
  },
});

class Sidebar extends Component {
  state = {
    builtMenu: null,
    notifications_count: 0,
    notifications: [],
    open: false,
    openDeploy: false,
    openBuild: false,
    openApplication: false,
    openAutomate: false,
    project: {},
    projectParams: {
      page: 0,
      nameContains: "",
    },
    projectId: 0,
    expand: true,
    expandSidebarBuildButton: false,
    expandSidebarDeployButton: false,
    expandSidebarAutomateButton: false
  };

  paths = [
    // "/home",
    // "/projects",
    // "/experiments",
    // "/models",
    // "/deployments",
    // "/monitoring",
    // "/workflows",
    // "/run-history",
    // "/workflows/new",
    // "/notifications"
  ];

  promptToast = (msg) => {
    if (msg) {
      toast(<ul className="notification-list">{msg}</ul>, {
        position: toast.POSITION.TOP_RIGHT,
        autoClose: 60000,
        type: toast.TYPE.DEFAULT,
        closeButton: false,
        closeOnClick: true,
      });
    } else {
      toast.info("You have a new notification", {
        position: toast.POSITION.BOTTOM_RIGHT,
        autoClose: 60000,
        closeButton: false,
      });
    }
  };

  loadNotifications = async (notificationData) => {
    this.props.notificationsList({
      sort: "created_date,desc",
      size: 20,
    });
    this.props.notificationUnreadCounts();
  };

  checkIfRecent = (date, sec) => {
    const date1 = new Date(date);
    const date2 = new Date();
    if ((date2.getTime() - date1.getTime()) / 100 < sec) {
      return true;
    }
    return false;
  };

  componentDidUpdate(prevProps) {
    
    if (this.props.user && this.props.user.userName && !this.enabledListener) {
      this.enabledListener = 1;
      listenNotifications(this.props.user.userName, this.loadNotifications);
      listenEvents(this.props.user.userName, this.props.eventPosters);
    }
    if (
      this.props.notifications &&
      prevProps.notifications &&
      this.props.notifications[0] &&
      prevProps.notifications[0] &&
      prevProps.notifications[0].id !== this.props.notifications[0].id
    ) {
      if (this.checkIfRecent(this.props.notifications[0].created_date, 60)) {
        this.promptToast(this.renderNotification(this.props.notifications[0]));
      }
    }
  }

  componentDidMount() {
    this.props.getUserDetail();
    this.loadNotifications();
    // this.props.getProjectsSummary(this.state.projectParams);
  }

  markAsRead = (data) => {
    let notificationId = typeof data === "object" ? data : [data];
    if (notificationId.length > 1) {
      const tempData = data.map((e) => {
        if (!e.read) return e.id;
      });
      notificationId = tempData.filter((e) => e);
    }
    if (!notificationId.length) {
      return;
    }
    this.props.markAsRead({ ids: notificationId });
    this.loadNotifications();
  };

  getClass = (pathname, currentPath) => {
    if (pathname !== currentPath && pathname.includes(currentPath + "/")) {
      return "nav-action nav-menu-item active";
    } else if (
      pathname === currentPath ||
      ((pathname === "" || pathname === "/") && currentPath === "/home")
    ) {
      return "nav-action nav-menu-item active";
    } else {
      return "nav-action nav-menu-item";
    }
  };

  getLink = (id, type) => {
    switch (type) {
      case "MLWorkflow":
        return "/workflows/" + id;
      case "MLWorkflowRun":
        return "/run-history/" + id;
      case "RegisteredModelVersion":
        return "/registered-model-versions/" + id;
      case "Project":
        return "/projects/" + id;
      case "Insight":
      default:
        return "/monitoring/" + id;
    }
  };

  setDefaultProject = () => {
    this.setState({ project: this.props.globalProject });
  };
  handleChange = (event) => {
    const target = event.target;
    const name = target.name;
    const value = target.value;

    const selectedProject = this.props.projects.filter(
      (item) => item.id == value
    );
    this.props.globalProjectSelection(selectedProject[0]);
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleToggleHome = () => {
    this.props.history.push("/projects");
    const expand = this.state.expand;
  };

  handleToggleDeploy = () => {
    const expand = this.state.expand
    this.setState((state) => ({ openDeploy: !state.openDeploy }));
    this.handleSidebarDeploydButtonExpand()
    if (!expand) {
      this.handleSideBarTogle()
    }
    
  };

  handleCloseDeploy = (event) => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }
    this.setState({ openDeploy: false });
  };

  handleCloseDeployDeployments = (event) => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }
    this.setState({ openDeploy: false });
    const expand = this.state.expand
    this.props.history.push("/deployments");

  };

  handleCloseDeployModelCatalog = (event) => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }
    this.setState({ openDeploy: false });
    this.props.history.push("/models");
  };

  handleCloseDeployMonitoring = (event) => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }
    this.setState({ openDeploy: false });
    this.props.history.push("/monitoring");
  };

  handleToggleBuild = () => {
    console.log("handleToggleBuild")
    const expand = this.state.expand
    this.setState((state) => ({ openBuild: !state.openBuild }));
    this.handleSidebarBuildButtonExpand()
    if (!expand) {
      this.handleSideBarTogle()
    }
  };

  handleCloseBuild = (event) => {
    console.log("handleCloseBuild")
    if (this.builtMenu.contains(event.target)) {
      return;
    }
    this.setState({ openBuild: false });
  };

  handleCloseBuildNotebooks = (event) => {
    if (this.builtMenu.contains(event.target)) {
      return;
    }
    this.setState({ openBuild: false });
    this.props.history.push("/notebooks");
  };

  handleCloseBuildExperiments = (event) => {
    if (this.builtMenu.contains(event.target)) {
      return;
    }
    this.setState({ openBuild: false });
    this.props.history.push("/experiments");
  };

  handleCloseBuildDatasets = (event) => {
    if (this.builtMenu.contains(event.target)) {
      return;
    }
    this.setState({ openBuild: false });
    // not avialble in app.js
    this.props.history.push("/datasets");
  };

  handleToggleApplications = () => {
    this.setState((state) => ({ openApplication: !state.openApplication }));
    this.props.history.push("/applications");
  };

  handleToggleAutomate = () => {
    this.setState((state) => ({ openAutomate: !state.openAutomate }));
    this.handleSidebarAutomateButtonExpand()
  };

  handleCloseAutomate = (event) => {
    if (this.automateMenu.contains(event.target)) {
      return;
    }
    this.setState({ openAutomate: false });
  };
  handleCloseAutomateWorkflows = (event) => {
    if (this.automateMenu.contains(event.target)) {
      return;
    }
    this.setState({ openAutomate: false });
    this.props.history.push("/workflows");
  };
  handleCloseAutomateRunHistory = (event) => {
    if (this.automateMenu.contains(event.target)) {
      return;
    }
    this.setState({ openAutomate: false });
    this.props.history.push("/run-history");
  };

  renderNotification = (notification, key = new Date().getTime()) => {
    return (
      <li className="notification-item" key={key}>
        <span className="notification-icon">
          <img
            src={notification_icon}
            data-tip=""
            data-for={`mark-as-read-${key}`}
            alt="notification icon"
          />
        </span>
        <span className="notification-msg">
          <Link
            to={this.getLink(
              notification.entity_id,
              notification.reference_entity
            )}
            onClick={() => this.markAsRead(notification.id)}
          >
            <h6 style={notification.read ? { fontWeight: "normal" } : {}}>
              {notification.message}
            </h6>
          </Link>
          <time>{moment(notification.created_date).format("LLL")}</time>
        </span>
      </li>
    );
  };

  renderSidebarContent = () => {
    const { builtMenu } = this.state;
    const { open } = this.state;
    const { openDeploy, openBuild, openApplication, openAutomate } = this.state;

    const { pathname } = this.props.location;
    const {
      user,
      notifications,
      notifications_count,
      workflowCounts,
      projectCounts,
      modelCounts,
      experimentCounts,
    } = this.props;
    const counter = {
      "/workflows": workflowCounts,
      "/projects": projectCounts,
      "/models": modelCounts,
      "/experiments": experimentCounts,
    };
    return (
      <div className="navbar navbar-expand-md navbar-default main-nav">
        <div className="container-fluid">
          <Link to="/projects" className="navbar-brand">
            <img src={logo} alt="" />
          </Link>
          <FormControl>
            <MuiThemeProvider theme={theme}>
              {this.props.projects.length > 0 && this.props.selectedProject &&(
                  <Select
                  className="mt-10"
                  disableUnderline
                  open={this.state.open}
                  onClose={this.handleClose}
                  onOpen={this.handleOpen}
                  value={this.props.selectedProject.id}
                  onChange={this.handleChange}
                  displayEmpty
                  inputProps={{
                    name: "project",
                  }}
                  >
                  {this.props.projects.map((e, i) => {
                    return (
                      <MenuItem
                        color="primary"
                        key={"projectSelect" + i}
                        value={e.id}
                      >
                        {e.name}
                      </MenuItem>
                    );
                  })}
                  </Select>
              )}
             <span></span>
            </MuiThemeProvider>
          </FormControl>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarCollapse"
            aria-controls="navbarCollapse"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarCollapse">
            <div className="menu-bar">
              <MuiThemeProvider theme={theme}>
                <Button
                  style={{
                    border: "none",
                    outline: "none",
                    backgroundColor:
                      this.props.location.pathname === "/" ||
                      this.props.location.pathname === "/projects"
                        ? "#e6eefc"
                        : null,
                  }}
                  color={
                    this.props.location.pathname === "/" ||
                    this.props.location.pathname === "/projects"
                      ? "primary"
                      : null
                  }
                  onClick={this.handleToggleHome}
                  className="mr-2"
                >
                  Home
                </Button>
                <Button
                  style={{
                    border: "none",
                    outline: "none",
                    backgroundColor:
                      this.props.location.pathname.includes("datasets") ||
                      this.props.location.pathname.includes("notebooks") ||
                      this.props.location.pathname.includes("experiments")
                        ? "#e6eefc"
                        : null,
                  }}
                  buttonRef={(node) => {
                    this.builtMenu = node;
                  }}
                  aria-owns={openBuild ? "menu-list-grow" : undefined}
                  aria-haspopup="true"
                  color={
                    this.props.location.pathname.includes("datasets") ||
                    this.props.location.pathname.includes("notebooks") ||
                    this.props.location.pathname.includes("experiments")
                      ? "primary"
                      : null
                  }
                  onClick={this.handleToggleBuild}
                  className="mr-2"
                >
                  Build
                </Button>
                <Popper
                  open={openBuild}
                  anchorEl={this.builtMenu}
                  transition
                  disablePortal
                >
                  {({ TransitionProps, placement }) => (
                    <Grow
                      {...TransitionProps}
                      id="menu-list-grow"
                      style={{
                        transformOrigin:
                          placement === "bottom"
                            ? "center top"
                            : "center bottom",
                      }}
                    >
                      <Paper>
                        <ClickAwayListener onClickAway={this.handleCloseBuild}>
                          <MenuList>
                            <MenuItem
                              selected={
                                this.props.location.pathname.includes(
                                  "datasets"
                                )
                                  ? "selected"
                                  : ""
                              }
                              onClick={this.handleCloseBuildDatasets}
                            >
                              Datasets
                            </MenuItem>
                            <MenuItem
                              selected={
                                this.props.location.pathname.includes(
                                  "notebooks"
                                )
                                  ? "selected"
                                  : ""
                              }
                              onClick={this.handleCloseBuildNotebooks}
                            >
                              Notebooks
                            </MenuItem>
                            <MenuItem
                              selected={
                                this.props.location.pathname.includes(
                                  "experiments"
                                )
                                  ? "selected"
                                  : ""
                              }
                              onClick={this.handleCloseBuildExperiments}
                            >
                              Experiments
                            </MenuItem>
                          </MenuList>
                        </ClickAwayListener>
                      </Paper>
                    </Grow>
                  )}
                </Popper>

                <Button
                  style={{
                    border: "none",
                    outline: "none",
                    backgroundColor:
                      this.props.location.pathname.includes("models") ||
                      this.props.location.pathname.includes("deployments") ||
                      this.props.location.pathname.includes("monitoring") ||
                      this.props.location.pathname.includes(
                        "registered-model-versions"
                      )
                        ? "#e6eefc"
                        : null,
                  }}
                  buttonRef={(node) => {
                    this.anchorEl = node;
                  }}
                  aria-owns={openDeploy ? "menu-list-grow" : undefined}
                  aria-haspopup="true"
                  color={
                    this.props.location.pathname.includes("models") ||
                    this.props.location.pathname.includes("deployments") ||
                    this.props.location.pathname.includes("monitoring") ||
                    this.props.location.pathname.includes(
                      "registered-model-versions"
                    )
                      ? "primary"
                      : null
                  }
                  onClick={this.handleToggleDeploy}
                  className="mr-2"
                >
                  Deploy
                </Button>
                <Popper
                  open={openDeploy}
                  anchorEl={this.anchorEl}
                  transition
                  disablePortal
                >
                  {({ TransitionProps, placement }) => (
                    <Grow
                      {...TransitionProps}
                      id="menu-list-grow"
                      style={{
                        transformOrigin:
                          placement === "bottom"
                            ? "center top"
                            : "center bottom",
                      }}
                    >
                      <Paper>
                        <ClickAwayListener onClickAway={this.handleCloseDeploy}>
                          <MenuList>
                            <MenuItem
                              selected={
                                this.props.location.pathname.includes(
                                  "models"
                                ) ||
                                this.props.location.pathname.includes(
                                  "registered-model-versions"
                                )
                                  ? "selected"
                                  : ""
                              }
                              onClick={this.handleCloseDeployModelCatalog}
                            >
                              Model Catalog
                            </MenuItem>
                            <MenuItem
                              selected={
                                this.props.location.pathname.includes(
                                  "deployments"
                                )
                                  ? "selected"
                                  : ""
                              }
                              onClick={this.handleCloseDeployDeployments}
                            >
                              Deployments
                            </MenuItem>
                            <MenuItem
                              selected={
                                this.props.location.pathname.includes(
                                  "monitoring"
                                )
                                  ? "selected"
                                  : ""
                              }
                              onClick={this.handleCloseDeployMonitoring}
                            >
                              Monitoring
                            </MenuItem>
                          </MenuList>
                        </ClickAwayListener>
                      </Paper>
                    </Grow>
                  )}
                </Popper>
                <Button
                  style={{
                    border: "none",
                    outline: "none",
                    backgroundColor: this.props.location.pathname.includes(
                      "applications"
                    )
                      ? "#e6eefc"
                      : null,
                  }}
                  aria-owns={openApplication ? "menu-list-grow" : undefined}
                  aria-haspopup="true"
                  color={
                    this.props.location.pathname.includes("applications")
                      ? "primary"
                      : null
                  }
                  onClick={this.handleToggleApplications}
                  className="mr-2"
                >
                  Applications
                </Button>
                <Button
                  style={{
                    border: "none",
                    outline: "none",
                    backgroundColor:
                      this.props.location.pathname.includes("workflows") ||
                      this.props.location.pathname.includes("run-history") ||
                      this.props.location.pathname.includes("workflow")
                        ? "#e6eefc"
                        : null,
                  }}
                  buttonRef={(node) => {
                    this.automateMenu = node;
                  }}
                  aria-owns={openAutomate ? "menu-list-grow" : undefined}
                  aria-haspopup="true"
                  color={
                    this.props.location.pathname.includes("workflows") ||
                    this.props.location.pathname.includes("run-history") ||
                    this.props.location.pathname.includes("workflow")
                      ? "primary"
                      : null
                  }
                  onClick={this.handleToggleAutomate}
                  className="mr-2"
                >
                  Automate
                </Button>
                <Popper
                  open={openAutomate}
                  anchorEl={this.automateMenu}
                  transition
                  disablePortal
                >
                  {({ TransitionProps, placement }) => (
                    <Grow
                      {...TransitionProps}
                      id="menu-list-grow"
                      style={{
                        transformOrigin:
                          placement === "bottom"
                            ? "center top"
                            : "center bottom",
                      }}
                    >
                      <Paper>
                        <ClickAwayListener
                          onClickAway={this.handleCloseAutomate}
                        >
                          <MenuList>
                            <MenuItem
                              selected={
                                this.props.location.pathname.includes(
                                  "workflows"
                                ) ||
                                this.props.location.pathname.includes(
                                  "workflow"
                                )
                                  ? "selected"
                                  : ""
                              }
                              onClick={this.handleCloseAutomateWorkflows}
                            >
                              Workflows
                            </MenuItem>
                            <MenuItem
                              selected={
                                this.props.location.pathname.includes(
                                  "run-history"
                                )
                                  ? "selected"
                                  : ""
                              }
                              onClick={this.handleCloseAutomateRunHistory}
                            >
                              Run History
                            </MenuItem>
                          </MenuList>
                        </ClickAwayListener>
                      </Paper>
                    </Grow>
                  )}
                </Popper>
              </MuiThemeProvider>
            </div>

            <ul className="nav navbar-nav" style={{ paddingTop: 7 }}>
              {this.paths.map((e, i) => {
                if (i < 5)
                  return (
                    <li key={"nav" + i} className={this.getClass(pathname, e)}>
                      <Link
                        to={e}
                        onClick={() => {
                          Mixpanel.track(l[pathname]);
                        }}
                      >
                        {l[e]}
                        {counter[e] ? (
                          <span className="badge badge-pill badge-secondary ml v-middle">
                            {counter[e]}
                          </span>
                        ) : (
                          ""
                        )}
                      </Link>
                    </li>
                  );
              })}
              {this.paths.indexOf(pathname) >= 5 ? (
                <li
                  key={"hidden-path" + pathname}
                  className="nav-action nav-menu-item active"
                >
                  <Link
                    to={pathname}
                    onClick={() => {
                      Mixpanel.track(l[pathname]);
                    }}
                  >
                    {l[pathname]}
                  </Link>
                </li>
              ) : (
                ""
              )}
            </ul>
            <ul className="nav navbar-nav ml-auto">
              {process.env.REACT_APP_COMPONENT_NOTEBOOKS != "DISABLED" && (
                <li className="nav-action">
                  <Link to="/notebooks">
                    <i className="nav-icon notebooks-icon" />
                  </Link>
                </li>
              )}
              <li
                className="nav-action dropdown mt-sm"
                onClick={() => {
                  toast.dismiss();
                  this.markAsRead(notifications);
                }}
              >
                <a data-toggle="dropdown">
                  <Badge color="secondary" badgeContent={notifications_count}>
                    <i className="fa fa-bell" style={{ fontSize: "1.2em" }}></i>
                  </Badge>
                </a>
                <div
                  className="dropdown-menu dropdown-arrow dropdown-menu-right notification-dropdown"
                  aria-labelledby="dropdownMenuButton"
                >
                  <div className="notification-wraper">
                    <ul className="notification-list">
                      {notifications.length !== 0 ? (
                        notifications.map((notification, key) => {
                          return this.renderNotification(notification, key);
                        })
                      ) : (
                        <li className="notification-item">
                          <div className="pd-tb text-gray content-center f-12">
                            No notifications!
                          </div>
                        </li>
                      )}
                    </ul>
                    <hr className="m-0" />
                    {notifications.length ? (
                      <div className="content-center pd-tb">
                        <Link to="/notifications">
                          See all notifications ({notifications_count})
                        </Link>
                      </div>
                    ) : (
                      ""
                    )}
                  </div>
                </div>
              </li>
              <li className="nav-action dropdown mt-sm pt-xs">
                <a data-toggle="dropdown">
                  <i className="fa fa-plus" style={{ fontSize: "1.2em" }} />
                </a>
                <div
                  className="dropdown-menu dropdown-arrow dropdown-menu-right no-select"
                  aria-labelledby="dropdownMenuButton"
                >
                  <span
                    className="dropdown-item"
                    onClick={() => {
                      MicroModal.show("project-modal");
                    }}
                  >
                    New Project
                  </span>
                  <Link to="/workflows/new" className="dropdown-item">
                    New Workflow
                  </Link>
                  <span
                    className="dropdown-item"
                    onClick={() => {
                      MicroModal.show("secret-modal");
                    }}
                  >
                    New Secret
                  </span>
                  <Link to="/import-model" className="dropdown-item">
                    Import Model
                  </Link>
                </div>
              </li>
              <li className="nav-divider" />
              <li className="dropdown nav-dropdown profile-links-dropdown">
                <a
                  href=""
                  className="nav-link"
                  id="dropdownMenuButton"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <User user={user.userName} isMenu={1} />
                </a>
                <div
                  className="dropdown-menu dropdown-arrow dropdown-menu-right"
                  aria-labelledby="dropdownMenuButton"
                >
                  <Link className="dropdown-item" to="/profile">
                    <i className="fa fa-user" /> Profile
                  </Link>
                  <Link to="/help" className="dropdown-item">
                    <i className="fa fa-question-circle" /> Help
                  </Link>
                  <Link
                    to=""
                    className="dropdown-item"
                    onClick={this.props.logout}
                  >
                    <i className="fa fa-sign-out" /> Logout
                  </Link>
                </div>
              </li>
              {user.role === "TenantAdmin" && <li className="nav-divider" />}
              {user.role === "TenantAdmin" && (
                <li className="company-logo">
                  <Link to="/company">
                    <img src={sample_company_logo} alt="" />
                  </Link>
                </li>
              )}
            </ul>
          </div>
        </div>
        <ExpiryModalFullScreen />
        {/* To disable autocomplete */}
        <input style={{ opacity: 0, position: "absolute", zIndex: -1 }} />
        <input
          type="password"
          style={{ opacity: 0, position: "absolute", zIndex: -1 }}
        />
      </div>
    );
  };

  handleSideBarTogle = () => {
    this.setState((prevState) => ({expand : !prevState.expand}))
    if (this.state.expandSidebarDeployButton) {
      this.handleSidebarDeploydButtonExpand()
    }
    
    if (this.state.expandSidebarBuildButton) {
      this.handleSidebarBuildButtonExpand()
    } 
    
    if (this.state.expandSidebarAutomateButton) {
      this.handleSidebarAutomateButtonExpand()
    }
  }

  handleSidebarBuildButtonExpand = () => {
    this.setState((prevState) => ({expandSidebarBuildButton: !prevState.expandSidebarBuildButton}))
  }

  handleSidebarDeploydButtonExpand = () => {
    this.setState((prevState) => ({expandSidebarDeployButton: !prevState.expandSidebarDeployButton}))
  }

  handleSidebarAutomateButtonExpand = () => {
    this.setState((prevState) => ({expandSidebarAutomateButton: !prevState.expandSidebarAutomateButton}))
  }
  
  render () {
    const path = this.props.location.pathname
    const expand = this.state.expand
    const expandSidebarBuildButton = this.state.expandSidebarBuildButton
    const expandSidebarDeployButton = this.state.expandSidebarDeployButton
    const expandSidebarAutomateButton = this.state.expandSidebarAutomateButton
    const newClassName = expand === true ? 'sidebar-large' : 'sidebar';
    const buttonWidth = expand === true ? '200px' : '80px';
    const { openDeploy, openBuild, openAutomate } = this.state;

      return(
        <div className={newClassName}>
          <ul className="dashboard-nav nav-align-top" style={{paddingLeft: '30px', minHeight: '88vh'}}>
              <li className={path === '/home'? 'active': ''} style={{marginTop: '20px'}}>
                <div data-toggle="tooltip" title={this.props.selectedProject.name} style={{marginLeft: '5px', marginRight: '10px'}}>
                  {expand === true ? <h4 className="text-truncate">{this.props.selectedProject.name}</h4> : <h4 style={{fontSize: '40px', color: 'blue', fontWeight: 'bold'}}>{this.props.selectedProject.name.charAt(0).toUpperCase()}</h4>}
                </div>
              </li>
              <li style={{display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}} className="sidebar-scroll-down-button-container">
              <Button
                style={{
                  border: "none",
                  outline: "none",
                  width: buttonWidth,
                  alignItems: 'flex-start',
                  justifyContent: 'flex-start',
                  backgroundColor:
                    this.props.location.pathname.includes("datasets") ||
                    this.props.location.pathname.includes("notebooks") ||
                    this.props.location.pathname.includes("experiments")
                      ? "#e6eefc"
                      : null,
                }}
                buttonRef={(node) => {
                  this.builtMenu = node;
                }}
                aria-owns={openBuild ? "menu-list-grow" : undefined}
                aria-haspopup="true"
                color={
                  this.props.location.pathname.includes("datasets") ||
                  this.props.location.pathname.includes("notebooks") ||
                  this.props.location.pathname.includes("experiments")
                    ? "primary"
                    : null
                }
                onClick={this.handleToggleBuild}
                className="mr-2"
              >
                {expand === true ? `Build` : <i className="nav-icon dashboard-icon"></i>}
              </Button>
              <p className={expandSidebarBuildButton === true ? 'sidebar-scroll-down-icon-active' : 'sidebar-scroll-down-icon-inactive'} style={{margin: '0px', marginRight: '10px', marginTop: '3px'}}>{`>`}</p>
            </li>

            {expandSidebarBuildButton && <li>
              <div className="sidebar-button-expand-container">
                <MenuList style={{marginLeft: '10px'}}>
                  <MenuItem
                    selected={
                      this.props.location.pathname.includes(
                        "datasets"
                      )
                        ? "selected"
                        : ""
                    }
                    onClick={this.handleCloseBuildDatasets}
                  >
                    Datasets
                  </MenuItem>
                  <MenuItem
                    selected={
                      this.props.location.pathname.includes(
                        "notebooks"
                      )
                        ? "selected"
                        : ""
                    }
                    onClick={this.handleCloseBuildNotebooks}
                  >
                    Notebooks
                  </MenuItem>
                  <MenuItem
                    selected={
                      this.props.location.pathname.includes(
                        "experiments"
                      )
                        ? "selected"
                        : ""
                    }
                    onClick={this.handleCloseBuildExperiments}
                  >
                    Experiments
                  </MenuItem>
                </MenuList>
              </div>
              </li>}

              <li style={{display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}} className="sidebar-scroll-down-button-container">
              <Button
                style={{
                  border: "none",
                  outline: "none",
                  width: buttonWidth,
                  alignItems: 'flex-start',
                  justifyContent: 'flex-start',
                  backgroundColor:
                    this.props.location.pathname.includes("models") ||
                    this.props.location.pathname.includes("deployments") ||
                    this.props.location.pathname.includes("monitoring") ||
                    this.props.location.pathname.includes(
                      "registered-model-versions"
                    )
                      ? "#e6eefc"
                      : null,
                }}
                buttonRef={(node) => {
                  this.anchorEl = node;
                }}
                aria-owns={openDeploy ? "menu-list-grow" : undefined}
                aria-haspopup="true"
                color={
                  this.props.location.pathname.includes("models") ||
                  this.props.location.pathname.includes("deployments") ||
                  this.props.location.pathname.includes("monitoring") ||
                  this.props.location.pathname.includes(
                    "registered-model-versions"
                  )
                    ? "primary"
                    : null
                }
                onClick={this.handleToggleDeploy}
                className="mr-2"
              >
                {expand === true ? `Deploy` : <i className="nav-icon projects-icon"></i>}
              </Button>
              <p className={expandSidebarDeployButton === true ? 'sidebar-scroll-down-icon-active' : 'sidebar-scroll-down-icon-inactive'} style={{margin: '0px', marginRight: '10px', marginTop: '3px'}}>{`>`}</p>
            </li>

            {expandSidebarDeployButton && <li>
              <div className="sidebar-button-expand-container">
                <MenuList style={{marginLeft: '10px'}}>
                  <MenuItem
                    selected={
                      this.props.location.pathname.includes(
                        "models"
                      ) ||
                      this.props.location.pathname.includes(
                        "registered-model-versions"
                      )
                        ? "selected"
                        : ""
                    }
                    onClick={this.handleCloseDeployModelCatalog}
                  >
                    Model Catalog
                  </MenuItem>
                  <MenuItem
                    selected={
                      this.props.location.pathname.includes(
                        "deployments"
                      )
                        ? "selected"
                        : ""
                    }
                    onClick={this.handleCloseDeployDeployments}
                  >
                    Deployments
                  </MenuItem>
                  <MenuItem
                    selected={
                      this.props.location.pathname.includes(
                        "monitoring"
                      )
                        ? "selected"
                        : ""
                    }
                    onClick={this.handleCloseDeployMonitoring}
                  >
                    Monitoring
                  </MenuItem>
                </MenuList>
              </div>
              </li>}
            <li style={{display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}} className="sidebar-scroll-down-button-container">
              <Button
                style={{
                  border: "none",
                  outline: "none",
                  width: buttonWidth,
                  alignItems: 'flex-start',
                  justifyContent: 'flex-start',
                  width: '100%', 
                  backgroundColor:
                    this.props.location.pathname.includes("workflows") ||
                    this.props.location.pathname.includes("run-history") ||
                    this.props.location.pathname.includes("workflow")
                      ? "#e6eefc"
                      : null,
                }}
                buttonRef={(node) => {
                  this.automateMenu = node;
                }}
                aria-owns={openAutomate ? "menu-list-grow" : undefined}
                aria-haspopup="true"
                color={
                  this.props.location.pathname.includes("workflows") ||
                  this.props.location.pathname.includes("run-history") ||
                  this.props.location.pathname.includes("workflow")
                    ? "primary"
                    : null
                }
                onClick={this.handleToggleAutomate}
                className="mr-2"
              >
                {expand === true ? `Automate` : <i className="nav-icon experiment-icon"></i>}
              </Button>
              <p className={expandSidebarAutomateButton === true ? 'sidebar-scroll-down-icon-active' : 'sidebar-scroll-down-icon-inactive'} style={{margin: '0px', marginRight: '10px', marginTop: '3px'}}>{`>`}</p>
            </li>

            {expandSidebarAutomateButton && <li>
              <div className="sidebar-button-expand-container">
                <MenuList style={{marginLeft: '10px'}}>
                  <MenuItem
                    selected={
                      this.props.location.pathname.includes(
                        "workflows"
                      ) ||
                      this.props.location.pathname.includes(
                        "workflow"
                      )
                        ? "selected"
                        : ""
                    }
                    onClick={this.handleCloseAutomateWorkflows}
                  >
                    Workflows
                  </MenuItem>
                  <MenuItem
                    selected={
                      this.props.location.pathname.includes(
                        "run-history"
                      )
                        ? "selected"
                        : ""
                    }
                    onClick={this.handleCloseAutomateRunHistory}
                  >
                    Run History
                  </MenuItem>
                </MenuList>
              </div>
              </li>}
          </ul>
          <div>
              <a onClick={this.handleSideBarTogle}>
                <p className={expand ? 'sidebar-menu-icon-last' : 'sidebar-menu-icon-middle'}>
                  {expand === true ? `<< Collapse` : `Expand >>`}
                </p>
              </a>
          </div>
        </div>
      )
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    user: state.users.user,
    notifications: state.notification.notifications,
    notifications_count: state.notification.notifications_unread_count,
    workflowCounts: state.jobs.counter,
    modelCounts: state.models.registered_model_counter,
    experimentCounts: state.experiments.counter,
    projectCounts: state.projects.counter,
    projects: state.projects.summary,
    selectedProject: state.projects.globalProject,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators(
      {
        logout,
        getUserDetail,
        notificationsList,
        notificationUnreadCounts,
        markAsRead,
        eventPosters,
        globalProjectSelection,
        getProjectsSummary,
      },
      dispatch
    ),
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(Sidebar)
);
