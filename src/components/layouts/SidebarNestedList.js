import React from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { Typography } from '@material-ui/core';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import { withRouter } from "react-router-dom";
import { FormatBold,
   FormatAlignLeftSharp,
   Autorenew, 
   DataUsageTwoTone, 
   EventNoteRounded,
   LabelImportantRounded,
   DeveloperModeRounded,
   DepartureBoardRounded,
   MonochromePhotosRounded,
   GroupWorkRounded,
   ChangeHistoryRounded
  } from '@material-ui/icons';
import { logout } from '../../ducks/auth';
import { getUserDetail } from '../../ducks/users';
import { notificationsList, notificationUnreadCounts } from '../../ducks/notifications';
import { markAsRead } from '../../ducks/notifications';
import { eventPosters } from '../../ducks/common';
import { globalProjectSelection, getProjectsSummary } from '../../ducks/projects';
import { green } from '@material-ui/core/colors';

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 240,
    backgroundColor: theme.palette.background.paper,
    paddingRight: 0,
    paddingTop: '1.2rem'
  },
  listItem: {
    paddingLeft: '1.875rem',
    paddingTop: 4,
    paddingBottom: 4,
  },
  nested: {
    paddingLeft: theme.spacing.unit,
    paddingTop: 4,
    paddingBottom: 4,
  },
});

class SidebarNestedList extends React.Component {
  state = {
    openBuild: true,
    openDeploy: true,
    openAutomate: true
  };

  handleClickBuildButton = () => {
    this.setState(state => ({ openBuild: !state.openBuild }));
  };

  handleClickDeployButton = () => {
    this.setState(state => ({ openDeploy: !state.openDeploy }));
  };

  handleClickAutomateButton = () => {
    this.setState(state => ({ openAutomate: !state.openAutomate }));
  };

  render() {
    const { classes } = this.props;
    const routerPathName = this.props.location.pathname

    return (
      <List
        component="nav"
        className={classes.root}
      >
        {/*Sidebar Project Information Button Start*/}
        <ListItem
          className={classes.listItem}
          button
        >
          <ListItemText primary={<Typography className={(routerPathName.includes('/project_details')) && 'font-weight-bold'} onClick={() => this.props.history.push('/project_details')}>Project Information</Typography>} />
        </ListItem>
        {/*Sidebar Project Information Button Ends}
S
        {/*Sidebar Build Button Start*/}

        <ListItem 
        className={classes.listItem} 
        button 
        // onClick={this.handleClickBuildButton}
        >
          {/* <ListItemIcon>
            <FormatBold />
          </ListItemIcon> */}
          <ListItemText primary={<Typography className={(routerPathName.includes('/datasets') || routerPathName.includes('/notebooks') || routerPathName.includes('/experiments')) && 'font-weight-bold'}>Build</Typography>} />
                {/* {this.state.openBuild ? <ExpandLess /> : <ExpandMore />} */}
        </ListItem>

        {/*Sidebar Build Button End*/}


        {/*Sidebar Build Button Collapse Start*/}

        <Collapse in={this.state.openBuild} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>

            {/* Datasets Button Start */}

            <ListItem button className={classes.nested} onClick={() => this.props.history.push('/datasets')}>
                {/* <ListItemIcon>
                  <DataUsageTwoTone />
                </ListItemIcon> */}
                <ListItemText inset primary={<Typography className={(routerPathName.includes('/datasets')) && 'font-weight-bold'}>Datasets</Typography>} />
            </ListItem>

            {/* Datasets Button End */}

            {/* Noteboks Button Start */}

            <ListItem button className={classes.nested} onClick={() => this.props.history.push('/notebooks')}>
              {/* <ListItemIcon>
                <EventNoteRounded />
              </ListItemIcon> */}
              <ListItemText inset primary={<Typography className={(routerPathName.includes('/notebooks')) && 'font-weight-bold'}>Notebooks</Typography>} />
            </ListItem>

            {/* Noteboks Button End */}

            {/* Experients Button Start */}

            <ListItem button className={classes.nested} onClick={() => this.props.history.push('/experiments')}>
              {/* <ListItemIcon>
                <LabelImportantRounded />
              </ListItemIcon> */}
              <ListItemText inset primary={<Typography className={(routerPathName.includes('/experiments')) && 'font-weight-bold'}>Experiments</Typography>} />
            </ListItem>

            {/* Experients Button End */}

          </List>
        </Collapse>

        {/*Sidebar Build Button Collapse End*/}

        {/*Sidebar Deploy Button Start*/}

        <ListItem 
          button 
          className={classes.listItem} 
          // onClick={this.handleClickDeployButton}
          >
          {/* <ListItemIcon>
            <FormatAlignLeftSharp />
          </ListItemIcon> */}
          <ListItemText primary={<Typography className={(routerPathName.includes('/models') || routerPathName.includes('/deployments') || routerPathName.includes('/monitoring')) && 'font-weight-bold'}>Deploy</Typography>} />
          {/* {this.state.openBuild ? <ExpandLess /> : <ExpandMore />} */}
        </ListItem>

        {/*Sidebar Deploy Button End*/}


        {/*Sidebar Deploy Button Collapse Start*/}

        <Collapse in={this.state.openDeploy} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>

        {/* Model Catalog Button Start */}

        <ListItem button className={classes.nested} onClick={() => this.props.history.push('/models')}>
          {/* <ListItemIcon>
            <DeveloperModeRounded />
          </ListItemIcon> */}
          <ListItemText inset primary={<Typography className={(routerPathName.includes('/models')) && 'font-weight-bold'}>Model Catalog</Typography>} />
        </ListItem>

        {/* Model Catalog Button End */}

        {/* Deployments Button Start */}

        <ListItem button className={classes.nested} onClick={() => this.props.history.push('/deployments')}>
          {/* <ListItemIcon>
            <DepartureBoardRounded />
          </ListItemIcon> */}
          <ListItemText inset primary={<Typography className={(routerPathName.includes('/deployments')) && 'font-weight-bold'}>Deployments</Typography>} />
        </ListItem>

        {/* Deployments Button End */}

        {/* Monitoring Button Start */}

        <ListItem button className={classes.nested} onClick={() => this.props.history.push('/monitoring')}>
          {/* <ListItemIcon>
            <MonochromePhotosRounded />
          </ListItemIcon> */}
          <ListItemText inset primary={<Typography className={(routerPathName.includes('/monitoring')) && 'font-weight-bold'}>Monitoring</Typography>} />
        </ListItem>

        {/* Monitoring Button End */}

        </List>
        </Collapse>

        {/*Sidebar Deploy Button Collapse End*/}

        {/*Sidebar Automate Button Start*/}

        <ListItem
          button
          className={classes.listItem} 
          // onClick={this.handleClickAutomateButton}
          >
          {/* <ListItemIcon>
            <Autorenew />
          </ListItemIcon> */}
          <ListItemText primary={<Typography className={(routerPathName.includes('/workflows') || routerPathName.includes('/run-history')) && 'font-weight-bold'}>Automate</Typography>} />
          {/* {this.state.openAutomate ? <ExpandLess /> : <ExpandMore />} */}
        </ListItem>

        {/*Sidebar Automate Button End*/}


        {/*Sidebar Automate Button Collapse Start*/}

        <Collapse in={this.state.openAutomate} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>

            {/* Workflows Button Start */}

            <ListItem button className={classes.nested} onClick={() => this.props.history.push('/workflows')}>
              {/* <ListItemIcon>
                <GroupWorkRounded />
              </ListItemIcon> */}
              <ListItemText inset primary={<Typography className={(routerPathName.includes('/workflows')) && 'font-weight-bold'}>Workflows</Typography>} />
            </ListItem>

            {/* Workflows Button End */}

            {/* Run History Button Start */}

            <ListItem button className={classes.nested} onClick={() => this.props.history.push('/run-history')}>
              {/* <ListItemIcon>
                <ChangeHistoryRounded />
              </ListItemIcon> */}
              <ListItemText inset primary={<Typography className={(routerPathName.includes('/run-history')) && 'font-weight-bold'}>Run History</Typography>} />
            </ListItem>

            {/* Run History Button End */}
              </List>
            </Collapse>

        {/*Sidebar Automate Button Collapse End*/}
      </List>
    );
  }
}

SidebarNestedList.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    user: state.users.user,
    notifications: state.notification.notifications,
    notifications_count: state.notification.notifications_unread_count,
    workflowCounts: state.jobs.counter,
    modelCounts: state.models.registered_model_counter,
    experimentCounts: state.experiments.counter,
    projectCounts: state.projects.counter,
    projects: state.projects.summary,
    selectedProject: state.projects.globalProject,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators(
      {
        logout,
        getUserDetail,
        notificationsList,
        notificationUnreadCounts,
        markAsRead,
        eventPosters,
        globalProjectSelection,
        getProjectsSummary,
      },
      dispatch
    ),
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(SidebarNestedList)))

