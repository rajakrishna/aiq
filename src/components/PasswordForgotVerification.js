import React, { Component } from "react";
import logo from "../images/predera-logo.png";
import { comfirmPasswordReset } from "../api";
import Mixpanel from "mixpanel-browser";
import { getErrorMessage } from "../utils/helper";

class PasswordForgotVerification extends Component {
  state = {
    userName: this.props.location.state || "",
    confirmationCode: "",
    newPassword: "",
    newPasswordComfirm: "",
    error: ""
  };

  componentDidMount() {
    Mixpanel.track("Forgot Password Verification Page");
  }

  comfirmPassword = comfirmPassword => {
    if (comfirmPassword.length === 0) {
      this.setState({ error: "Password field cannot be blank" });
    } else if (comfirmPassword !== this.state.newPassword) {
      this.setState({ error: "Password does not match" });
    } else {
      this.setState({ error: "" });
    }
    this.setState({ newPasswordComfirm: comfirmPassword });
  };

  onSubmit = event => {
    event.preventDefault();
    let data = {
      userName: this.state.userName,
      confirmationCode: this.state.confirmationCode,
      newPassword: this.state.newPassword
    };
    if (data.newPassword !== this.state.newPasswordComfirm) {
      this.setState({ error: "Password does not match" });
    } else if (
      data.userName.length === 0 ||
      data.confirmationCode.length === 0 ||
      data.newPassword === 0
    ) {
      this.setState({ error: "Fields can not be blank" });
    } else {
      comfirmPasswordReset(data)
        .then(response => {
          if (response.status) {
            Mixpanel.track("Forgot Password Verification Successful");
          }
        })
        .catch(error => {
          const errorMessage =
            getErrorMessage(error) ||
            "An error occurred during Forgot Password Verification, please try again.";
          Mixpanel.track("Forgot Password Verification Failed", {
            error: errorMessage
          });
          this.setState({
            error: errorMessage
          });
        });
    }
  };

  render() {
    return (
      <div className="auth-wraper">
        <form onSubmit={this.onSubmit}>
          <div className="auth-box">
            <div className="logo-head">
              <img src={logo} alt="" />
            </div>
            {this.state.error && (
              <div className="error-placement  alert alert-danger">
                <i className="fa fa-exclamation-triangle" />
                <span>{this.state.error}</span>
              </div>
            )}

            <div className="form-group">
              <label>Confirmation Code</label>
              <div className="input-group">
                <span className="input-group-addon">
                  <i className="fa fa-envelope"></i>
                </span>
                <input
                  value={this.state.confirmationCode}
                  onChange={e =>
                    this.setState({ confirmationCode: e.target.value })
                  }
                  type="text"
                  className="form-control"
                  placeholder="Enter confirmation code "
                  required
                />
              </div>
            </div>

            <div className="form-group">
              <label>New Password</label>
              <div className="input-group">
                <span className="input-group-addon">
                  <i className="fa fa-envelope"></i>
                </span>
                <input
                  value={this.state.newPassword}
                  onChange={e => this.setState({ newPassword: e.target.value })}
                  type="password"
                  className="form-control"
                  placeholder="Enter confirmation code "
                  required
                />
              </div>
            </div>

            <div className="form-group">
              <label>Confirmation Code</label>
              <div className="input-group">
                <span className="input-group-addon">
                  <i className="fa fa-envelope"></i>
                </span>
                <input
                  value={this.state.newPasswordComfirm}
                  onChange={e => this.comfirmPassword(e.target.value)}
                  type="password"
                  className="form-control"
                  placeholder="Enter confirmation code "
                  required
                />
              </div>
            </div>

            <div className="form-group">
              <button type="submit" className="btn btn-primary form-btn">
                Confirm
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default PasswordForgotVerification;
