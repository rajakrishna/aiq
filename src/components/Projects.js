import React, { Component } from "react";
import { Link } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { getProjectsSummary, projectReset } from "../ducks/projects";
import { deleteProject } from "../api";
import User from "./shared/User";
import Select from "react-select";
import Library from "./shared/Library";
import moment from "moment";
import Pagination from "./common/pagination";
import CardLoader from "./common/cardLoader";
import { alertMsg } from "../ducks/alertsReducer";
import { confirm } from "./Confirm";
import Joyride from "react-joyride";
import Mixpanel, { disable } from "mixpanel-browser";
import { getErrorMessage } from "../utils/helper";
import ReactTooltip from "react-tooltip";
import MicroModal from "micromodal";
import { Avatar } from "@material-ui/core";
import { getCurrentUser } from "../api";
import TutorialModal from "./walkthrough/tutorialModal";
import { globalProjectSelection } from "../ducks/projects";
import { experimentCounter } from "../ducks/experiments";
import zIndex from "@material-ui/core/styles/zIndex";
import ProjectModalFullScreen from "./modals/projectModalFullScreen";



const PAGE_SIZE = 20;  

class Projects extends Component {
  clicked = false;
  state = {
    userName: "",
    tutorial: true,
    name: "",
    description: "",
    userList: [],
    delNumber: 1,
    projectParams: {
      page: 0,
      nameContains: "",
    },
    currentPage: 0,
    currentPageSize: PAGE_SIZE,
    emptyModel: false,
    run: false,
    steps: [
      {
        target: ".project-list-item",
        content: (
          <div className="fl">
            <h4>Projects</h4>
            <p>
              A project space contains all the experiments, model artifacts and
              jobs built by the team
            </p>
          </div>
        ),
        locale: { last: <strong aria-label="next">Next</strong> },
        placement: "right",
        disableOverlayClose: true,
        hideCloseButton: true,
        disableBeacon: true,
      },
    ],
  };

  componentDidMount() {
    Mixpanel.track("Projects Page");
    //this.props.getProjectsSummary(this.state.projectParams);
    // this.props.getUsersList();
    getCurrentUser(this.props.token)
      .then((response) => {
        if (response.status) {
          const data = response.data;
          this.setState({ userName: response.data.userName });
        }
      })
      .catch((error) => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while fetching User Profile, please try again.";
        this.props.alertMsg(errorMessage, "error");
      });
    if(this.state.tutorial) {
      this.props.experimentCounter((count)=>{if(count === 0) MicroModal.show('tutorial-modal')});
    }
   // this.openJoyRide();
  }
  static getDerivedStateFromProps(nextProps) {
    if(nextProps.history.location.state && !nextProps.history.location.state.tutorial) {
      let tutorial = nextProps.history.location.state.tutorial;
      return {
        tutorial
      }
    }
    return null;
  }
  static getDerivedStateFromProps(nextProps) {
    if(nextProps.history.location.state && !nextProps.history.location.state.tutorial) {
      let tutorial = nextProps.history.location.state.tutorial;
      return {
        tutorial
      }
    }
    return null;
  }
  static getDerivedStateFromProps(nextProps) {
    if(nextProps.history.location.state && !nextProps.history.location.state.tutorial) {
      let tutorial = nextProps.history.location.state.tutorial;
      return {
        tutorial
      }
    }
    return null;
  }
  static getDerivedStateFromProps(nextProps) {
    if(nextProps.history.location.state && !nextProps.history.location.state.tutorial) {
      let tutorial = nextProps.history.location.state.tutorial;
      return {
        tutorial
      }
    }
    return null;
  }

  openJoyRide(){
    if(this.props.summary.length===0){
     MicroModal.show('tutorial-modal')
    }
  }

  

  openJoyRide(){
    if(this.props.summary.length===0){
     MicroModal.show('tutorial-modal')
    }
  }

  

  openJoyRide(){
    if(this.props.summary.length===0){
     MicroModal.show('tutorial-modal')
    }
  }

  

  openJoyRide(){
    if(this.props.summary.length===0){
     MicroModal.show('tutorial-modal')
    }
  }

  

  static getDerivedStateFromProps(nextProps) {
    return {
      run: nextProps.history.location.state
        ? nextProps.history.location.state.tutorial
        : false,
    };
  }

  userListForSelection() {
    let userList = [];
    if (this.props.users) {
      this.props.users.map((user) => {
        return userList.push({
          label: user.firstName + " " + user.lastName,
          value: user.userName,
        });
      });
    } else {
      userList = [];
    }
    return userList;
  }

  onSubmit = (e) => {
    e.preventDefault();
    const users =
      this.state.userList && this.state.userList.length > 0
        ? this.state.userList.split(",")
        : [];
    const data = {
      name: this.state.name,
      description: this.state.description,
      users: users,
    };
    this.props.createProjectAction(data);
    this.props.getProjectsSummary(this.state.projectParams);
  };

  handleInputChange = (e) => {
    let params = this.state.projectParams;
    params["nameContains"] = e.target.value;
    this.setState({ projectParams: params });
  };

  handleRefresh = () => {
    this.props.projectReset();
    var projectParams = {
      page: 0,
      nameContains: "",
    };
    this.setState({ projectParams: projectParams });
    this.props.getProjectsSummary(projectParams);
  };

  handleSearch = () => {
    this.props.projectReset();
    this.props.getProjectsSummary(this.state.projectParams);
  };

  handlePageChange = (pageAction) => {
    this.props.projectReset();
    let pageNumber = this.state.currentPage;
    pageAction === "nextPage" ? (pageNumber += 1) : (pageNumber -= 1);
    let params = { ...this.state.projectParams, pageNumber: pageNumber };
    this.props.getProjectsSummary(params);
    this.setState({ currentPage: pageNumber });
  };

  handleDeletion(id) {
    this.props.projectReset();
    if (this.clicked) {
      return 0;
    }
    this.props.alertMsg("Deleting Project...", "info");
    this.clicked = true;
    deleteProject(this.props.token, id)
      .then((res) => {
        if (res.status === 200) {
          this.props.alertMsg("Project deleted successfully!", "success");
          Mixpanel.track("Delete Project Successful", { project_id: id });
        }
        this.clicked = false;
        this.setState({
          delNumber: this.state.delNumber + 1,
        });
        this.props.getProjectsSummary(this.state.projectParams);
      })
      .catch((err) => {
        const errorMessage =
          getErrorMessage(err) ||
          "An error occurred while deleting the Project, please try again.";
        Mixpanel.track("Delete Project Failed", {
          project_id: id,
          error: errorMessage,
        });
        this.clicked = false;
        this.props.alertMsg(errorMessage, "error");
      });
  }

  handleJoyrideCallback = (data) => {
    if(data.action === "next" && data.status === "finished") {
      this.props.history.push("/projects/"+this.props.summary[0].id+"/experiments", {tutorial: true})
    }
  };

  handleDetails = (summary) => {
    this.props.globalProjectSelection(summary);
  };
  getProjectItems = (loading) => {
    const { steps, run } = this.state;
    if (loading) {
      return <CardLoader />;
    }
    if (this.props.summary.length !== 0) {
      return (
        <div className="row projects-list-container">
          {this.props.summary.filter((each) => each.name.includes(this.state.projectParams.nameContains)).map((summary, i) => {
            return (
              <div className="col-sm-3 project-list-item" key={i}>
                <div className="project-item">
                  <div className="prediction-o-item mb">
                    <div className="col-sm-10 pd-0">
                      <Link to={`/project_details`}>
                        <div className="project-title">
                          <h4
                            className="text-truncate"
                            data-tip
                            data-for={`projectName${summary.name}`}
                            onClick={() => this.handleDetails(summary)}
                          >
                            {summary.name}
                          </h4>
                          <ReactTooltip
                            id={`projectName${summary.name}`}
                            aria-haspopup="true"
                          >
                            <h6 style={{ padding: "10px" }}>{summary.name}</h6>
                          </ReactTooltip>
                        </div>
                      </Link>
                    </div>
                    <div className="col-sm-2 pd-0">
                      <div className="btn-group float-right">
                        <span
                          className="dropdown-toggle pd-xs content-none clickable"
                          data-toggle="dropdown"
                          aria-haspopup="true"
                          aria-expanded="false"
                        >
                          <i className="fa fa-ellipsis-v ellipsis-menu"></i>
                        </span>
                      <div className="dropdown-menu"  data-tip data-for={`deleteTip_${summary.id}` } >
                          <span
                            className={`dropdown-item clickable ${this.state.userName !== summary.owner ? "disabled" : ""}`}
                            onClick={(elem) => {
                             { this.state.userName === summary.owner ? (
                              confirm("Are you sure?", "Delete project").then(
                                () => {
                                  this.handleDeletion(summary.id);
                                },
                                () => {
                                  console.log("Cancel!");
                                }
                              )
                              ):(null);
                             }
                            }}
                          >
                            Delete
                          </span>
                        </div>
                         { this.state.userName !== summary.owner &&
                          <ReactTooltip 
                            id={`deleteTip_${summary.id}` }
                              aria-haspopup="true"
                              className="custom-tooltip"
                           >
                              <h6 style={{ padding: "10px" }}>You do not have permissions to delete this Project.</h6>
                               
                          </ReactTooltip>
                            }
                      </div>
                    </div>
                  </div>
                  <div className="project-body">
                    <div className="project-status-info">
                      <div className="p-status-item">
                        <span className="p-info-left">
                          <i />
                          {summary.summary.no_of_models} Model
                          {summary.summary.no_of_models > 1 ? "s" : ""}
                        </span>
                        {summary.summary.last_activity_date_models && (
                          <span className="p-info-right">
                            <i />
                            {moment(
                              summary.summary.last_activity_date_models
                            ).fromNow()}
                          </span>
                        )}
                      </div>
                      <div className="p-status-item">
                        <span className="p-info-left">
                          <i />
                          {summary.summary.no_of_experiments} Experiment
                          {summary.summary.no_of_experiments > 1 ? "s" : ""}
                        </span>
                        {summary.summary.last_activity_date_experiments && (
                          <span className="p-info-right">
                            <i />
                            {moment(
                              summary.summary.last_activity_date_experiments
                            ).fromNow()}
                          </span>
                        )}
                      </div>
                    </div>
                    <div className="project-members-info">
                      <ul
                        style={{ display: "inline-flex" }}
                        className="project-members-list"
                      >
                        {Object.keys(summary.summary.libraries).length !== 0 &&
                          Object.entries(summary.summary.libraries).map(
                            (library, i) => {
                              if (i < 4) {
                                return (
                                  <Library
                                    library={library[0]}
                                    count={library[1]}
                                    key={`${summary.project}_${i}`}
                                    index={i}
                                  />
                                );
                              } else if (i == 4) {
                                return (
                                  <li  key={`${summary.project}_${i}`}>
                                    <a
                                      data-tip
                                      data-for="global"
                                      style={{ padding: "5px 0px 0px 4px" }}
                                    >
                                      {" "}
                                      +
                                      {Object.keys(summary.summary.libraries)
                                        .length - 4}{" "}
                                    </a>
                                    <ReactTooltip
                                      id="global"
                                      aria-haspopup="true"
                                    >
                                      {Object.entries(
                                        summary.summary.libraries
                                      ).map((library, i) => {
                                        if (i > 3)
                                          return (
                                            <div key={i}>
                                              {library[0]} {library[1]}
                                            </div>
                                          );
                                      })}
                                    </ReactTooltip>
                                  </li>
                                );
                              }
                            }
                          )}
                        {Object.keys(summary.summary.languages).length !== 0 &&
                          Object.keys(summary.summary.languages).map(
                            (library, i) => {
                              return (
                                <Library
                                  library={library}
                                  key={`${summary.project}_${i}`}
                                />
                              );
                            }
                          )}
                        {Object.keys(summary.summary.environments).length !==
                          0 &&
                          Object.keys(summary.summary.environments).map(
                            (library, i) => {
                              return (
                                <Library
                                  library={library}
                                  key={`${summary.project}_${i}`}
                                />
                              );
                            }
                          )}
                      </ul>
                    </div>
                    <hr />
                    <div className="project-members-info">
                      <ul className="project-members-list">
                        {summary.users &&
                          summary.users.map((user, i) => {
                            if (i == 5 && summary.users.length > 7) {
                              return (
                                <span key={user}>
                                  <a
                                    data-tip
                                    data-for={`${summary.project}_${i}`}
                                    style={{ padding: "5px 0px 0px 4px" }}
                                  >
                                    <Avatar className="avatar">
                                      {" "}
                                      +{summary.users.length - 5}{" "}
                                    </Avatar>
                                  </a>
                                  <ReactTooltip
                                    id={`${summary.project}_${i}`}
                                    aria-haspopup="true"
                                  >
                                    {summary.users.map((user, i) => {
                                      if (i > 4) return <div key={user}>{user}</div>;
                                    })}
                                  </ReactTooltip>
                                </span>
                              );
                            } else if (i <= 5) {
                              return (
                                <User
                                  user={user}
                                  key={`${summary.project}_${i}`}
                                />
                              );
                            }
                          })}
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      );
    } else {
      return <div className="text-center">No projects found</div>;
    }
  };

  render() {
    const { currentPageSize } = this.state;
    const { loading } = this.props;
    const totalModels = this.props.projects_count;
    const currentPage = this.state.currentPage;
    return (
      <div className="main-container">
        <div className="main-body">
          <div className="content-wraper-sm">
            <div className="projects-wraper ml-5 mr-5 projects-list-container">
              <div className="mt-3 mb-3 d-flex flex-row align-items-center justify-content-between">
                <div>
                  <h4 className="mr-3 m-title-info">Projects</h4>
                </div>
                <div className="projects-search">
                  <input
                    type="text"
                    value={this.state.projectParams.nameContains}
                    className="form-control"
                    placeholder="Search projects"
                    onChange={this.handleInputChange}
                    onKeyPress={(event) => {
                      if (event.key === "Enter") {
                        this.handleSearch();
                      }
                    }}
                  />
                  <button
                    className="btn btn-primary"
                    // onClick={this.handleSearch}
                  >
                    <i className="fa fa-search" />
                  </button>
                </div>
                <div>
                  <button
                    style={{ margin: 10 }}
                    className="btn btn-default"
                    onClick={this.handleRefresh}
                  >
                    <i className="fa fa-sync-alt" />
                  </button>
                  <button
                    className="btn btn-sm btn-primary mr"
                  >
                    <ProjectModalFullScreen />
                  </button>
                </div>
              </div>
              {/* {this.props.message && (
                <div className="error-placement alert alert-danger">
                  <i className="fa fa-exclamation-triangle" />
                  <span>{this.props.message}</span>
                </div>
              )} */}
              {/* project List */}
                {this.getProjectItems(loading)}

                {totalModels > PAGE_SIZE && (
                  <Pagination
                    itemsCount={totalModels}
                    currentPage={currentPage}
                    currentPageSize={currentPageSize}
                    pageSize={PAGE_SIZE}
                    onPageChange={this.handlePageChange}
                  />
                  )}
            </div>
          </div>
        </div>

        {/* New project form */}
        <div className="modal" tabIndex="-1" role="dialog" id="new-project">
          <div className="modal-dialog modal-md" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">New Project</h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              {this.props.message && (
                <div className="error-placement alert alert-danger">
                  <i className="fa fa-exclamation-triangle" />
                  <span>{this.props.message}</span>
                </div>
              )}
              {this.props.successMessage && (
                <div className="alert alert-success">
                  <span>{this.props.successMessage}</span>
                </div>
              )}
              <form onSubmit={this.onSubmit}>
                <div className="modal-body">
                  <div className="form-group form-material">
                    <label>Project Name</label>
                    <input
                      type="text"
                      className="form-control"
                      value={this.state.name}
                      // aria-error={this.state.projectError}
                      onChange={(e) => {
                        if (!/^[a-z0-9\\-]{4,50}$/g.test(e.target.value)) {
                          this.setState({ projectError: true });
                        } else {
                          this.setState({ projectError: false });
                        }
                        this.setState({ name: e.target.value });
                      }}
                      required
                    />
                    <div
                      className={
                        this.state.projectError
                          ? "f-12 text-danger no-select"
                          : "f-12 text-gray no-select"
                      }
                    >
                      Name must start with a lowercase letter followed by up to
                      49 lowercase letters, numbers, or hyphens, and cannot end
                      with a hyphen
                    </div>
                  </div>
                  <div className="form-group form-material">
                    <label>Description</label>
                    <input
                      type="text"
                      className="form-control"
                      value={this.state.description}
                      onChange={(e) =>
                        this.setState({ description: e.target.value })
                      }
                      required
                    />
                  </div>
                  <div className="form-group form-material">
                    <label>User</label>
                    <Select
                      closeOnSelect={true}
                      multi
                      onChange={(e) => this.setState({ userList: e })}
                      options={this.userListForSelection()}
                      placeholder="Select User"
                      removeSelected={true}
                      simpleValue
                      value={this.state.userList}
                    />
                  </div>
                </div>
                <div className="modal-footer">
                  <button
                    type="submit"
                    className="btn btn-primary"
                    disabled={this.state.projectError}
                  >
                    Create Project
                  </button>
                  <button
                    type="button"
                    className="btn btn-secondary"
                    data-dismiss="modal"
                  >
                    Close
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    summary: state.projects.summary,
    loading: state.projects.loading,
    projects_count: state.projects.projects_count,
    message: state.projects.message,
    successMessage: state.projects.successMessage,
    users: state.users.users,
    token: state.auth.token,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      projectReset,
      getProjectsSummary,
      alertMsg,
      globalProjectSelection,
      experimentCounter
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Projects);
