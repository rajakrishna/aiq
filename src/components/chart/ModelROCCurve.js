import React, { Component } from 'react';
import { LineChart } from 'react-chartkick';

class ModelROCCurve extends Component  {

  state = { }
  componentDidMount() {
    let rocData =[]
    let data = []
    for (var i in this.props.roc_points) {
      if (this.props.roc_points.hasOwnProperty(i)) {
        data.push([this.props.roc_points[i].x || 0,this.props.roc_points[i].y || 0])
      }
    }
    rocData.push({'name':'ROC Curve', 'data': data})
    
    rocData.push({'name':'Random Guess', 'data': [[0,0],[1,1]]})
    this.setState({data:rocData})
  }

  render() {
    return(
      <LineChart data={this.state.data}
        height="600px"
        width="1200px"
        curve={true}
        ytitle="True Positive Rate"
        xtitle="False Positive Rate"
        messages={{empty: "No data"}}
        points={false}
        legend={"top"}
        library={{title: 'ROC Curve'}} />
    );
  }
}

export default ModelROCCurve;
