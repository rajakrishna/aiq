import React, { Component } from 'react';
import ReactEcharts from 'echarts-for-react'; 

class RequestSecChart extends Component {
    constructor(props) {
        super(props);
    }
    state = {  }

    renderRequestSecChart=()=>{
           const options = {
            title: { 
              show:this.props.yCpuData.length === 0,
              textStyle: 
              {
               color: "grey",
               fontSize: 16
             },
               text: "No Data available",
               left: "center",
               top: "25%"
             },
            grid: {
              left: 100,
              right: 100,
              bottom: 160,
              top: 80
            },
            xAxis: {
              axisLabel: {
                interval: 0,
                rotate: 70
              },
              type: 'category',
              name:'Time',
              nameLocation: 'middle',
              nameGap: 50,
              data: this.props.xCpuData
            },
            yAxis: {
              type: 'value',
              name:'Requests',
              nameLocation: 'middle',
              nameGap: 50
            },
            series: [
              {
                name: 'Request/sec',
                data: this.props.yCpuData,
                type: 'line',
                animationDuration: 5000,
                smooth: true,
                showSymbol: false,
              }
            ],
            tooltip: {
                trigger: "axis"
              }
        
          }; 
          const styles={
            height:"500px",
            width:"100%"
          }
        
            return  <ReactEcharts
            option={options}
            style={styles}
          />
      }


    render() { 
        return ( 
            <div>
                {this.renderRequestSecChart()}
            </div>
         );
    }
}
 
export default RequestSecChart;
