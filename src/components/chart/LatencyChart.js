import React, { Component } from "react";
import ReactEcharts from "echarts-for-react";
import moment from "moment";

class LatencyChart extends Component {
  constructor(props) {
    super(props);
  }
  state = {};
  getlegends = (data) => {
    let array = [];
    data.forEach((item) => {
      if(item.data.result.length >0){
        array.push(item.data.result[0].metric.quantile);

      }

    });
    return array;
  };
  getxaxisData = (data) => {
    if(data[0].data.result.length>0){
      let data1 = data[0].data.result[0].values;
      const xAxisData = data1.map((item) => {
        let cpuDate = new Date(item[0] * 1000);
        return moment(cpuDate).format("HH:mm");
      });
      return xAxisData;
    }else{
      []
    }
   
  };
  getSeriesData = (data) => {
    let array = [];
    data.forEach((item) => {
      if (item.data.result.length > 0) {
        const yAxisCpuData = item.data.result[0].values.map(
          (element) => element[1] * 1000
        );
        let obj = {
          name: item.data.result[0].metric.quantile,
          type: "line",
          stack: "Total",
          animationDuration: 5000,
          smooth: true,
          showSymbol: false,
          data: yAxisCpuData,
        };

        array.push(obj);
      }

      // array.push(item.data.result[0].metric.quantile);
    });
    return array;
  };

  renderLatencyChart = () => {
    const options = {
      title: { 
        show: this.props.latencyData.length === 0,
        textStyle: 
        {
         color: "grey",
         fontSize: 16
       },
         text: "No Data Avaialble",
         left: "center",
         top: "25%"
       },
      tooltip: {
        trigger: "axis",
      },
      legend: {
        data: this.getlegends(this.props.latencyData),
      },
      grid: {
        left: 100,
        right: 100,
        bottom: 160,
        top: 80,
        containLabel: true,
      },
      xAxis: {
        axisLabel: {
          interval: 0,
          rotate: 70,
        },
        type: "category",
        nameLocation: "middle",
        name:'Time',
        nameGap: 50,
        data: this.getxaxisData(this.props.latencyData),
      },
      yAxis: {
        type: "value",
        name:'Requests',
        nameLocation: "middle",
        nameGap: 50,
      },
       series: this.getSeriesData(this.props.latencyData),
    };
    const styles = {
      height: "500px",
      width: "100%",
    };

    return <ReactEcharts option={options} style={styles} />;
  };

  render() {
    return <div>{this.renderLatencyChart()}</div>;
  }
}

export default LatencyChart;
