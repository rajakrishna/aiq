import React, { Component } from 'react';
import { LineChart } from 'react-chartkick';

export class PredictionLineChart extends Component  {

  render() {
    return(
      <div className="inline-block">
        <LineChart data={this.props.data}
          width="400px"
          height="300px"
          ytitle="Prediction Trend"
          xtitle="Timestamp"
          id="models-health"
          messages={{empty: "No data for Prediction"}}
          library={{ 
            vAxis: { 
              gridlines: { 
                color: "transparent" 
              } }, 
            hAxis: { 
              format: this.props.dayOne ? 'hh:mm a': 'MMM dd YYYY'
            } 
          }}
        />
      </div>
    );
  }
}
