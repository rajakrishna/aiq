import React, { Component } from 'react';
import ReactEcharts from 'echarts-for-react';
import moment from 'moment';
 
class MemoryChart extends Component {
    constructor(props) {
        super(props);
    }
    state = { }
    
   
  
    renderMemoryChart=()=>{

      const options = {
        title: {
          show:this.props.memoryData.length === 0,
          textStyle: {
              color: "grey",
              fontSize: 16
          },
          text: "No memory data",
          left: "center",
          top: "25%"
        },
        grid: {
          left: 100,
          right: 100,
          bottom: 160,
          top: 80
        },
        xAxis: {
          show:this.props.memoryData.length !== 0,
          axisLabel: {
            formatter: function (value, index) {
              const date = new Date(value*1000);
              return  moment(date).format('HH:mm:ss'); 
            },
          },
          type: 'time',
          name: 'Time',
          splitNumber:2,
          nameLocation: 'middle',
          nameGap: 50,
        },
        yAxis: {
          show:this.props.memoryData.length !== 0,
          axisLabel: {
            formatter: function (value, index) {
              return  (value/(1024*1024*1000)).toFixed(2); 
            },
          },
          type: 'value',
          name:'Memory (GB)',
          nameLocation: 'middle',
          nameGap: 70
        },
        series: [
          {
            name: 'Memory (GB)',
            data: this.props.memoryData,
            type: 'line',
            animationDuration: 5000,
            smooth: true,
            showSymbol: false,
          }
        ],
        tooltip: {
            trigger: "axis",
            formatter: function (params, ticket, callback) {
              const date = new Date(params[0].data[0]*1000)
              const value = params[0].data[1]
            return 'Time: '+ moment(date).format('YYYY-MM-DD HH:mm:ss')+'<br/>'+
                    params[0].seriesName+': '+(value/(1024*1024*1000)).toFixed(4);
        }
          }
    
      }; 
      const styles={
        height:"500px",
        width:"100%"
      }
    
        return  <ReactEcharts
        option={options}
        style={styles}
      />
        }

    render() { 
        return ( 
            <div>
               {this.renderMemoryChart()}
            </div>
         );
    }
}
 
export default MemoryChart;