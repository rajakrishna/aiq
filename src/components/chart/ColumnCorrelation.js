import React, {Component} from 'react';
import Chart from 'chart.heatmap.js';
import Select from 'react-select';
import {filter} from 'lodash'

class ColumnCorrelation extends Component {

  state = {
    testDatasets: [],
    options: [],
    selectedOptions: [],
    removeSelected: true,
    disabled: false,
    crazy: false,
    stayOpen: false,
    value: [],
    rtl: false,
    allColumns: [],
  }
  
  componentDidMount(){
    const testDatasets = this.props.testDatasets? this.props.testDatasets : [];
      this.drawChart(testDatasets);
      var allColumns = []
      testDatasets.map(function(dataset){
        var index = allColumns.indexOf(dataset.y)
        if ( index === -1){
          const item =   { value: dataset.y, label: dataset.y }
          allColumns.push(item)
        }
      })
      this.setState({ testDatasets: testDatasets, allColumns: allColumns })
  }

  handleSelect = (selectedOptions)=> {
      this.setState({ selectedOptions });
      this.filterColumns(selectedOptions)
  }

  filterColumns = (selectedOptions)=> {
    const keys = selectedOptions.map(function(option){return option.value})
    var selectedDatasets = this.state.testDatasets
    if (keys && keys.length > 0){
      selectedDatasets  = filter(this.state.testDatasets,function(dataset){
        return (keys.indexOf(dataset.y) >= 0 && keys.indexOf(dataset.x) >= 0 )
      })
    }
    this.drawChart(selectedDatasets)
  }



  drawChart = (testDatasets) => {
    var data = {
      labels: [],
      datasets: []
    }

    let temp = []
    let count = 0;

    for (let i = 0; i < testDatasets.length; i++) {
      if(data.labels.length === 0 || data.labels.indexOf(testDatasets[i].y) === -1) {
        data.labels.push(testDatasets[i].y)
      }
    }

    for (let i = 0; i < testDatasets.length; i++) {
      
      if(count === data.labels.length - 1) {
        if (testDatasets[i].value)
          temp.push(parseInt(testDatasets[i].value * 1000000000));
        else
          temp.push(0);
        data.datasets.push({
          label: testDatasets[i].x,
          data: temp
        })
        temp = []
        count = 0
      } else {
        if (testDatasets[i].value)
          temp.push(parseInt(testDatasets[i].value * 1000000000))
        else
          temp.push(0);
        count = count + 1;
      }
      
    }

    var options = {
      backgroundColor: '#fff',
      responsive:false,
      overflowX: "scroll",
      stroke: false,
      strokePerc: 0.05,
      strokeColor: "rgb(128,128,128)",
      highlightStrokeColor: "rgb(192,192,192)",
      rounded: true,
      roundedRadius: 0.1,
      paddingScale: 0.05,
      colorInterpolation: "gradient",
      colors: [ "rgba(220,220,220,0.9)", "rgba(151,187,205,0.9)"],
      colorHighlight: true, 
      colorHighlightMultiplier: 0.92,
      showLabels: false, 
      labelScale: 0.2,
      labelFontFamily: '"HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif',
      labelFontStyle: "normal",
      labelFontColor: "rgba(0,0,0,0.5)",
      tooltipTemplate: "X: <%= yLabel %> \n Y: <%= xLabel %> \n  Value: <%= value / 1000000000 %>",
    }

    var ctx = document.getElementById('heatmap').getContext('2d');
    new Chart(ctx).HeatMap(data, options);
  }

  render(){
    const { selectedOptions,allColumns } = this.state;
    return (
      <div className="data-section">
        <div className="data-sec-head">
          <h3>Column Correlation</h3>
        </div>
        <div className="row" style={{marginBottom: 20}}>
          <div className="col-md-12">
            <label>Features</label>
            <Select
              value={selectedOptions}
              onChange={this.handleSelect}
              options={allColumns}
              multi
              isSearchable={true}
            />
          </div>
        </div>
        <div className="data-section-body">
          <div style={{"overflowX": "scroll"}} width="1200" height="800" >
            <canvas id="heatmap" 
              width="1200"
              height="800"
              >
            </canvas>
          </div>
        </div>
        <hr />
      </div>
    )
  }
}

export default ColumnCorrelation;
