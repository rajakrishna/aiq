import React, { Component } from 'react';

class ModelScatterPlot extends Component  {

  state = { 
    data: [],
  }
  componentDidMount() {
    if (this.props.scatter_plot) {
      let scatterPlot = this.props.scatter_plot;
      const google = window.google;
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {

        let dataTable = [['ID', 'X', 'Y', 'Value']];

        for (const key in scatterPlot) {
          if (scatterPlot.hasOwnProperty(key)) {
            let temp = []
            temp.push('')//temp.push(key)
            temp.push(scatterPlot[key].x)
            temp.push(scatterPlot[key].y)
            temp.push(scatterPlot[key].value)
            dataTable.push(temp)
          }
        }

        var data = google.visualization.arrayToDataTable(dataTable)

        var options = {
          title: 'Scatter Plot',
          colorAxis: {colors: ['blue','yellow','green','red',]},
          sizeAxis: {minValue: 2,  maxSize: 5}
        };

        var chart = new google.visualization.BubbleChart(document.getElementById('scatter_plot'));
        chart.draw(data, options);
      }
    }
  }

  render() {
    return <div id="scatter_plot" style={{width: '900px', height: '500px'}}></div>
  }
}

export default ModelScatterPlot;
