import React, { Component } from 'react';
import { LineChart } from 'react-chartkick';

class ModelLiftCurve extends Component  {

  state = { }
  componentDidMount() {
    let rocData =[]
    let data = []
    for (var i in this.props.lift_points) {
      if (this.props.lift_points.hasOwnProperty(i)) {
        data.push([this.props.lift_points[i].x || 0,this.props.lift_points[i].y || 0])
      }
    }
    rocData.push({'name':'Lift Curve', 'data': data})
    rocData.push({'name':'Random Guess', 'data': [[0,0.1],[10,0.1]]})
    this.setState({data:rocData})
  }

  render() {
    return(
      <LineChart data={this.state.data}
        height="600px"
        width="1200px"
        curve={true}
        ytitle="Churn Rate"
        xtitle="Decile"
        messages={{empty: "No data"}}
        points={false}
        legend={"top"}
        library={{title: 'Lift Curve'}} />
    );
  }
}

export default ModelLiftCurve;
