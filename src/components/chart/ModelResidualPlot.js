import React, { Component } from 'react';
import { ScatterChart } from 'react-chartkick';

class ModelResidualPlot extends Component  {

  state = {
    data: []
   }
  componentDidMount() {
    if (this.props.residual_plot) {
      let residual_plot = this.props.residual_plot;
      let data = []
      for (const key in residual_plot) {
        if (residual_plot.hasOwnProperty(key)) {
          let temp = [];
          temp.push(residual_plot[key].x)
          temp.push(residual_plot[key].y)
          data.push(temp)
        }
      }
       this.setState({data})                                                                                                                                                                                                                                                                    
    }
  }

  render() {
    return(
      <ScatterChart data={this.state.data}
        height="400px"
        curve={true}
        messages={{empty: "No data"}}
        points={false}
        legend={"top"}
        library={{title: 'Residual Plot'}} />
    );
  }
}

export default ModelResidualPlot;
