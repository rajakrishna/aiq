import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getModelHealth } from '../../api/model';
import { LineChart } from 'react-chartkick';
import moment from 'moment';

class ModelHealth extends Component  {
  state = {
  }
  componentDidMount() {
    let healthData = []
    let latestHealth = ''
    getModelHealth(this.props.token, this.props.model_id)
    .then(response => {
      const data = response.data;
      if(response.status) {
        if (data.length !== 0) {
          for (var i in data) {
            if (data.hasOwnProperty(i)) {
              let date = moment(data[i].timestamp).format('YYYY-MM-DD');
              const arr = []
              arr[0]=date
              arr[1]=data[i].health
              healthData.push(arr)
              latestHealth = data[i].health
            }
          }
        } else {
          healthData = []
          latestHealth = 0
        }
      }
      this.setState({ [this.props.model_id]: healthData });
      this.setState({latestHealth})
    })
    .catch(error => {
      console.log(error);
    })

  }

  render() {
    return(
      <div className="inline-block">
        <label>Health: {this.state.latestHealth}%</label>
        <LineChart data={this.state[this.props.model_id]}
          width="400px"
          height="300px"
          id="models-health"
          messages={{empty: "No data for Health"}}
          library={{vAxis: {gridlines: {color: "transparent"}}}}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.token
  }
}

export default connect(mapStateToProps, null)(ModelHealth);
