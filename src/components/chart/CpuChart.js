import React, { Component } from 'react';
import ReactEcharts from 'echarts-for-react'; 
import moment from 'moment';
class CpuChart extends Component {
    constructor(props) {
        super(props);
    }
    state = {  }
    

    renderCpuChart=()=>{

           const options = {
            title: {
              show:this.props.cpuData.length === 0,
              textStyle: {
                  color: "grey",
                  fontSize: 16
              },
              text: "No cpu data",
              left: "center",
              top: "25%"
            },
            grid: {
              left: 100,
              right: 100,
              bottom: 160,
              top: 80
            },
            xAxis: {
              show:this.props.cpuData.length !== 0,
              axisLabel: {
                formatter: function (value, index) {
                  const date = new Date(value*1000);
                  return  moment(date).format('HH:mm:ss'); 
                },
              },
              type: 'time',
              name: 'Time',
              nameLocation: 'middle',
              nameGap: 50,
              splitNumber:2
            },
            yAxis: {
              show:this.props.cpuData.length !== 0,
              axisLabel: {
                formatter: function (value, index) {
                  return  value*1000; 
                },
                showMaxLabel: false
              },
              type: 'value',
              name:'CPU (millis)',
              nameLocation: 'middle',
              nameGap: 50
            },
            series: [
              {
                name: 'CPU (millis)',
                type: 'line',
                animationDuration: 5000,
                smooth: true,
                showSymbol: false,
                data:this.props.cpuData
              }
            ],
            tooltip: {
                trigger: "axis",
                formatter: function (params, ticket, callback) {
                    const date = new Date(params[0].data[0]*1000)
                    const value = params[0].data[1]
                  return 'Time: '+ moment(date).format('YYYY-MM-DD HH:mm:ss')+'<br/>'+
                  params[0].seriesName+': '+(value*1000).toFixed(4);
              }
                
              }
        
          }; 
          const styles={
            height:"500px",
            width:"100%"
          }
        
            return  <ReactEcharts
            option={options}
            style={styles}
          />
      }


    render() { 
        return ( 
            <div>
                {this.renderCpuChart()}
            </div>
         );
    }
}
 
export default CpuChart;
