import React, { Component } from "react";
import { AreaChart } from "react-chartkick";
import moment from 'moment';

class ModelHealthAreaChart extends Component {
  state = {
    modelHealth: {},
    loader: true
  };

  componentDidMount() {
    if (this.props.modelHealthDetails) {
      let modelHealth = {};
      this.props.modelHealthDetails.map(model => {
        return (modelHealth[moment(model.timestamp).format(this.props.format || "ll")] = model.health_score);
      });

      this.setState({
        modelHealth,
        loader: false,
      });
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.modelHealthDetails) {
      let modelHealth = {};
      nextProps.modelHealthDetails.map(model => {
        return (modelHealth[moment(model.timestamp).format(this.props.format || "ll")] = model.health_score);
      });

      this.setState({
        modelHealth
      });
    }
  }

  render() {
    const { modelHealth } = this.state;
    return (
      <AreaChart
        data={modelHealth}
        width="650px"
        height="300px"
        id="models-health"
        messages={{ empty: "No data for Health" }}
        colors={[this.props.chartColor || "blue"]}
        library={{ 
          vAxis: { 
            gridlines: { 
              color: "transparent" 
            } }, 
          hAxis: { 
            format: this.props.dayOne ? 'hh:mm a': 'MMM dd YYYY'
          } 
        }}
      />
    );
  }
}

export default ModelHealthAreaChart;
