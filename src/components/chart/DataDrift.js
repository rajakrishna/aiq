import React, { Component } from 'react';
import { LineChart } from 'react-chartkick';

export class DataDriftChart extends Component  {

  render() {
    return(
      <div className="inline-block">
        <LineChart data={this.props.data}
          width="400px"
          height="300px"
          ytitle="Data Drift Trend"
          xtitle="Timestamp"
          id="models-health"
          messages={{empty: "No data for DataDrift"}}
          library={{ 
            vAxis: { 
              gridlines: { 
                color: "transparent" 
              } }, 
            hAxis: { 
              format: this.props.dayOne ? 'hh:mm a': 'MMM dd YYYY'
            } 
          }}
        />
      </div>
    );
  }
}
