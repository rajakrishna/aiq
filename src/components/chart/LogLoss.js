import React, { Component } from 'react';
import { LineChart } from 'react-chartkick';
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";

class LogLoss extends Component  {

  state = {
    lossData: [],
    accuracyData: [],
   }
  componentDidMount() {
    const lossData = [],
          accuracyData = [],
          {training_log_loss, validation_log_loss} = this.props,
          trainingLoss = this.getData(training_log_loss.loss),
          trainingAcc = this.getData(training_log_loss.accuracy),
          validationLoss = this.getData(validation_log_loss.loss),
          validationAcc = this.getData(validation_log_loss.accuracy);

    if(trainingLoss && trainingLoss.length) {
      lossData.push(
        {name: "Training", data: trainingLoss}
      )
    }
    if(trainingAcc && trainingAcc.length) {
      accuracyData.push(
        {name: "Training", data: trainingAcc}
      )
    }
    if(validationLoss && validationLoss.length) {
      lossData.push(
        {name: "Validation", data: validationLoss}
      )
    }
    if(validationAcc && validationAcc.length) {
      accuracyData.push(
        {name: "Validation", data: trainingAcc}
      )
    }

    this.setState({
      lossData,
      accuracyData
    })
  }

  getData = (propData) => {
    if(!propData) return;
    const plots = propData;
    const data = []
    for (const key in plots) {
      if (plots.hasOwnProperty(key)) {
        let temp = [key,plots[key]];
        data.push(temp)
      }
    }
    return data;
  }

  render() {
    const {lossData, accuracyData} = this.state;
    return(
      <div>
        {lossData && lossData.length ?
        <div>
          <LineChart 
            data={lossData}
            height="400px"
            curve={true}
            messages={{empty: "No data"}}
            points={false}
            legend={"bottom"}
            library={{title: 'Loss'}} 
          />
        </div> : ""}
        {accuracyData && accuracyData.length ?
        <div className="mt">
          <LineChart 
            data={accuracyData}
            height="400px"
            curve={true}
            messages={{empty: "No data"}}
            points={false}
            legend={"bottom"}
            library={{title: 'Accuracy'}} 
          />
        </div> : ""}
      </div>
    );
  }
}

export default LogLoss;
