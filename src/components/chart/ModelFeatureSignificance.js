import React, { Component } from 'react';
import { LineChart } from 'react-chartkick';

class ModelFeatureSignificance extends Component  {

  state = { }
  componentDidMount() {
    let data = []
    for (var i in this.props.feature_significance) {
      if (this.props.feature_significance.hasOwnProperty(i)) {
        let temp = {
          'name': this.props.feature_significance[i].name,
          'data': {
            [this.props.feature_significance[i].name]:this.props.feature_significance[i].significance,
          }
        }
        data.push(temp)
      }
    }
    this.setState({data})
  }

  render() {
    return(
      <LineChart data={this.state.data}
        height="400px"
        curve={true}
        ytitle="Significance"
        messages={{empty: "No data"}}
        library={{title: 'Feature Significance'}} />
    );
  }
}

export default ModelFeatureSignificance;
