import React, {Component} from 'react';
import Chart from 'chart.heatmap.js';

class ModelConfusionMatrix extends Component {
  
  componentDidMount(){

    let n = Math.floor(Math.sqrt(this.props.confusion_matrix.length))

    var data = {
      labels: n && [...Array(n).keys()],
      datasets: []
    }

    let temp = []
    var count = 0

    for (let i = 0; i < this.props.confusion_matrix.length; i++) {
      
      if(count === n-1){
        temp.push(parseInt(this.props.confusion_matrix[i].count )|| 0)
        data.datasets.push({
          label: this.props.confusion_matrix[i].actual_class,
          data: temp
        })
        temp = []
        count = 0
      }else {
        temp.push(parseInt(this.props.confusion_matrix[i].count )|| 0)
        count = count + 1;
      }
    }

    var options = {
      backgroundColor: '#fff',
      stroke: false,
      strokePerc: 0.05,
      strokeColor: "rgb(128,128,128)",
      highlightStrokeColor: "rgb(192,192,192)",
      rounded: true,
      roundedRadius: 0.1,
      paddingScale: 0.05,
      colorInterpolation: "gradient",
      colors: [ "rgba(220,220,220,0.9)", "rgba(151,187,205,0.9)"],
      colorHighlight: true, 
      colorHighlightMultiplier: 0.92,
      showLabels: true, 
      labelScale: 0.2,
      labelFontFamily: '"HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif',
      labelFontStyle: "normal",
      labelFontColor: "rgba(0,0,0,0.5)",
      tooltipTemplate: "Predicted Class: <%= xLabel %> \n Actual Class: <%= yLabel %> \n Count: <%= value %>",
      legend: true,
      legendTemplate : '<div class="<%= name.toLowerCase() %>-legend">'+
        '<span class="<%= name.toLowerCase() %>-legend-text">'+
        '<%= min %>'+
        '</span>'+
        '<% for (var i = min; i <= max; i += (max-min)/6){ %>'+ // change 6 to number of divisions required
        '<span class="<%= name.toLowerCase() %>-legend-box" style="background-color: <%= colorManager.getColor(i).color %>;">  </span>'+
        '<% } %>'+
        '<span class="<%= name.toLowerCase() %>-legend-text">'+
        '<%= max %>'+
        '</span>'+
        '</div>'
    }

    var ctx = document.getElementById('heatmap').getContext('2d');
    new Chart(ctx).HeatMap(data, options);
  }

  render(){
    return (
      <div className="pd-lr-lg pl-4" style={{overflowX: "scroll"}} width="900" height="450" >
        <canvas id="heatmap" 
          width="900" 
          height="450"
          >
        </canvas>
      </div>
    )
  }
}

export default ModelConfusionMatrix;
