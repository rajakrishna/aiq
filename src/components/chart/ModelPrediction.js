import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getAllPredictions } from '../../api';
import { LineChart } from 'react-chartkick';
import moment from 'moment';

class ModelHealth extends Component  {
  state = {
  }
  componentDidMount() {
    let predictionData = []
    let totalCount = 0;
    getAllPredictions(this.props.token, { modelIdEquals: this.props.model_id})
    .then(response => {
      const data = response.data;
      if(response.status) {
        if (data.length !== 0) {
          for (var i in data) {
            if (data.hasOwnProperty(i)) {
              let date = moment(data[i].timestamp).format('YYYY-MM-DD');
              const arr = []
              arr[0]=date
              arr[1]=data[i].count
              predictionData.push(arr)
              totalCount += data[i].count
            }
          }
        } else {
          predictionData = []
          totalCount = 0
        }
      }
      this.setState({ [this.props.model_id]: predictionData });
      this.setState({ totalCount })
    })
    .catch(error => {
      console.log(error);
    })

  }

  render() {
    return(
      <div className="inline-block">
        <label>Prediction: {this.state.totalCount}</label>
        <LineChart data={this.state[this.props.model_id]}
          width="400px"
          height="300px"
          id="models-prediction"
          messages={{empty: "No data for Prediction"}}
          library={{vAxis: {gridlines: {color: "transparent"}}}}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.token
  }
}

export default connect(mapStateToProps, null)(ModelHealth);
