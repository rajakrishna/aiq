import React, { Component } from 'react';
import { LineChart } from 'react-chartkick';

export class FeatureDistributionChart extends Component  {

  render() {
    return(
      <div className="inline-block">
        <LineChart data={this.props.data}
          width="550px"
          height="300px"
          curve={true}
          ytitle="Feature Distribution"
          xtitle="Timestamp"
          id="feature-distribution"
          points={false}
          messages={{empty: "No data for Feature Distribution"}}
          library={{
            colors: ["black", "blue", "green"],
            series: [
              {},
              {type: 'area'},
              {}
            ],
            vAxis: { 
              gridlines: { 
                color: "transparent" 
              } }, 
            // hAxis: { 
            //   format: this.props.dayOne ? 'hh:mm a': 'MMM dd YYYY'
            // } 
          }}
        />
      </div>
    );
  }
}

export default FeatureDistributionChart;