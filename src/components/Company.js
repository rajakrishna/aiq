import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Loader1, Loader2, Loader3 } from "./home/modelOverviewLoader";
import sample_company_logo from "../images/company.svg";
import {FullLoader} from "../components/FullLoader";
import MicroModal from "micromodal";
import { toast } from "react-toastify";
import {
  getAllUsers,
  addUser,
  deleteUser,
  getTenant,
  updateUser,
  disableUser,
  enableUser
} from "../api";
import moment from "moment";
import jwtDecode from "jwt-decode";
import ToggleButton from "react-toggle-button";
import Mixpanel from "mixpanel-browser";

class Company extends Component {
  state = {
    users: [],
    company: [],
    firstName: "",
    lastName: "",
    userName: "",
    role: "",
    successMsg: "",
    errorMsg: "",
    modalIsOpen: false,
    modalIsOpen2: false,
    editUser: {
      firstName: "",
      lastName: "",
      userName: "",
      role: ""
    },
    createLoader: false,
    editLoader: false
  };

  async componentDidMount() {
    Mixpanel.track('Company Page');
    MicroModal.init();
    let jwtcode = jwtDecode(this.props.token);
    let Companydetails = await getTenant(
      this.props.token,
      jwtcode["custom:tenant_id"]
    );
    let UsersList = await getAllUsers(this.props.token);
    this.setState({
      users: UsersList.data,
      company: Companydetails.data
    });
  }

  createUser = event => {
    event.preventDefault();
    this.setState({ createLoader: true });
    let data = {
      firstName: this.state.firstName.trim(),
      lastName: this.state.lastName.trim(),
      userName: this.state.userName.trim(),
      role: this.state.role.trim()
    };
    addUser(this.props.token, data)
      .then(response => {
        if (response.status) {
          this.setState({ createLoader: false });
          toast.success("Member Created Successfully!", {
            position: toast.POSITION.TOP_CENTER
          });
          getAllUsers(this.props.token)
            .then(res => {
              MicroModal.close("company-new-member");
              this.setState({ users: res.data });
            })
            .catch(error => console.log(error));
        }
      })
      .catch(error => toast.error(error.message));
  };

  deleteUser = id => {
    deleteUser(this.props.token, id)
      .then(response => {
        if (response.status) {
          getAllUsers(this.props.token)
            .then(res => {
              this.setState({ users: res.data });
            })
            .catch(error => console.log(error));
        }
      })
      .catch(error => console.log(error));
  };

  editUserDetails = user => {
    this.showModal("edit-member");
    this.setState({
      editUser: user
    });
  };

  editUser = e => {
    this.setState({ editLoader: true });
    e.preventDefault();
    const data = {
      firstName: this.state.editUser.firstName.trim(),
      lastName: this.state.editUser.lastName.trim(),
      userName: this.state.editUser.userName.trim(),
      role: this.state.editUser.role.trim()
    };
    updateUser(this.props.token, data)
      .then(response => {
        if (response.status) {
          this.setState({ editLoader: false });
          MicroModal.close("edit-member");
          toast.success("User Updated Successfully", {
            position: toast.POSITION.TOP_CENTER
          });
          getAllUsers(this.props.token)
            .then(res => {
              this.setState({ users: res.data });
            })
            .catch(error => console.log(error));
        }
      })
      .catch(error => console.log(error));
  };

  updateEditUser = (value, field) => {
    let editUser = this.state.editUser;
    if (field === "firstName") {
      editUser.firstName = value;
    } else if (field === "lastName") {
      editUser.lastName = value;
    } else if (field === "userName") {
      editUser.userName = value;
    } else if (field === "role") {
      editUser.role = value;
    }
    this.setState({ editUser });
  };

  userStatus = (status, user) => {
    let data = {
      firstName: user.firstName,
      lastName: user.lastName,
      userName: user.userName,
      role: user.role
    };
    if (status === true) {
      disableUser(this.props.token, data).then(response => {
        getAllUsers(this.props.token)
          .then(res => {
            this.setState({ users: res.data });
          })
          .catch(error => console.log(error));
      });
    } else {
      enableUser(this.props.token, data).then(response => {
        getAllUsers(this.props.token)
          .then(res => {
            this.setState({ users: res.data });
          })
          .catch(error => console.log(error));
      });
    }
  };

  showModal = modalId => {
    MicroModal.show(modalId);
  };

  render() {
    const { users, company } = this.state;
    return (
      <div className="main-container">
        {/* <NavigationLinks path={this.props.match.path} /> */}
        <div className="main-body">
          <div className="content-wraper content-wraper-sm">
            {company.length !== 0 ? (
              <div className="company-card">
                <div className="company-fields-body clearfix">
                  <div className="company-header">
                    <div className="company-logo-img">
                      <img src={sample_company_logo} alt="" />
                    </div>
                  </div>
                  <div className="comapany-details-block">
                    <ul className="model-data-list">
                      <li>
                        <label className="data-label">Company Name</label>
                        <p className="data-text">{company.companyName}</p>
                      </li>
                      <li>
                        <label className="data-label">Account Name</label>
                        <p className="data-text">{company.accountName}</p>
                      </li>
                      <li>
                        <label className="data-label">Owner Name</label>
                        <p className="data-text">{company.ownerName}</p>
                      </li>
                      <li>
                        <label className="data-label">Email</label>
                        <p className="data-text">{company.email}</p>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            ): <Loader2 />}
            <div className="company-card">
              <div className="company-card-header clearfix">
                <h4>Manage Team</h4>
                <button
                  className="btn btn-primary"
                  onClick={() => this.showModal("company-new-member")}
                >
                  Create New Member
                </button>
              </div>
              <div className="company-card-body">
                <table className="table">
                  <thead>
                    <tr>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>User Name</th>
                      <th>Role</th>
                      <th>Active</th>
                      <th>Created At</th>
                      <th width="120px" />
                    </tr>
                  </thead>
                  <tbody>
                    {users.lenght !== 0 &&
                      users.map((user, index) => {
                        return (
                          <tr key={index}>
                            <td>{user.firstName}</td>
                            <td>{user.lastName}</td>
                            <td>{user.userName}</td>
                            <td>{user.role}</td>
                            <td>
                              <ToggleButton
                                inactiveLabel={"Inactive"}
                                activeLabel={"Active"}
                                value={user.enabled}
                                onToggle={value => this.userStatus(value, user)}
                              />
                            </td>
                            <td>{moment(user.dateCreated).format("LLL")}</td>
                            <td className="user-actions">
                              <a
                                onClick={() => this.editUserDetails(user)}
                                className="user-edit-btn"
                                data-micromodal-trigger="edit-member"
                              >
                                <i className="fa fa-pencil" />
                              </a>
                              <a
                                onClick={() => this.deleteUser(user.userName)}
                                className="user-delete-btn"
                              >
                                <i className="fa fa-trash" />
                              </a>
                            </td>
                          </tr>
                        );
                      })}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

        <div
          className="modal micromodal-slide"
          id="company-new-member"
          aria-hidden="true"
        >
          <div className="modal__overlay" tabIndex="-1" data-micromodal-close>
            <div
              className="modal__container"
              role="dialog"
              aria-modal="true"
              aria-labelledby="modal-1-title"
            >
              <header className="modal__header">
                <h2 className="modal__title" id="modal-1-title">
                  New Member
                </h2>
                <button
                  className="modal__close"
                  aria-label="Close modal"
                  data-micromodal-close
                />
              </header>
              <main className="modal__content" id="modal-1-content">
                <div className="modal-filter-form">
                  <form onSubmit={this.createUser}>
                    <div>
                      {this.state.errorMsg && (
                        <div className="error-placement alert alert-danger">
                          <i className="fa fa-exclamation-triangle" />
                          <span>{this.state.errorMsg}</span>
                        </div>
                      )}
                      {this.state.successMsg && (
                        <div className="alert alert-success">
                          <span>{this.state.successMsg}</span>
                        </div>
                      )}
                      <div className="form-group">
                        <label
                          htmlFor="recipient-name"
                          className="col-form-label"
                        >
                          First Name:
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          required="required"
                          onChange={e =>
                            this.setState({ firstName: e.target.value })
                          }
                        />
                      </div>
                      <div className="form-group">
                        <label
                          htmlFor="recipient-name"
                          className="col-form-label"
                        >
                          Last Name:
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          required="required"
                          onChange={e =>
                            this.setState({ lastName: e.target.value })
                          }
                        />
                      </div>
                      <div className="form-group">
                        <label
                          htmlFor="recipient-name"
                          className="col-form-label"
                        >
                          Email Address:
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          required="required"
                          onChange={e =>
                            this.setState({ userName: e.target.value })
                          }
                        />
                      </div>
                      <div className="form-group">
                        <label
                          htmlFor="recipient-name"
                          className="col-form-label"
                        >
                          Role
                        </label>
                        <select
                          name="role"
                          className="form-control"
                          required="required"
                          onChange={e =>
                            this.setState({ role: e.target.value })
                          }
                        >
                          <option>Select Role</option>
                          <option value="TenantUser">User</option>
                          <option value="TenantAdmin">Admin</option>
                        </select>
                      </div>
                    </div>
                    <footer className="modal__footer filter-form-actions">
                      <button
                        type="submit"
                        className="btn btn-primary btn-rounded"
                        disabled={this.state.createLoader && "disabled"}
                      >
                        {this.state.createLoader
                          ? "Creating...."
                          : "Create User"}
                      </button>
                      <button
                        className="btn btn-default btn-rounded"
                        data-micromodal-close
                        aria-label="Close this dialog window"
                      >
                        Cancel
                      </button>
                    </footer>
                  </form>
                </div>
              </main>
            </div>
          </div>
        </div>

        <div
          className="modal micromodal-slide"
          id="edit-member"
          aria-hidden="true"
        >
          <div className="modal__overlay" tabIndex="-1" data-micromodal-close>
            <div
              className="modal__container"
              role="dialog"
              aria-modal="true"
              aria-labelledby="modal-1-title"
            >
              <header className="modal__header">
                <h2 className="modal__title" id="modal-1-title">
                  Edit Member
                </h2>
                <button
                  className="modal__close"
                  aria-label="Close modal"
                  data-micromodal-close
                />
              </header>
              <main className="modal__content" id="modal-1-content">
                <div className="modal-filter-form">
                  <form onSubmit={this.editUser}>
                    <div>
                      {this.state.errorMsg && (
                        <div className="error-placement alert alert-danger">
                          <i className="fa fa-exclamation-triangle" />
                          <span>{this.state.errorMsg}</span>
                        </div>
                      )}
                      {this.state.successMsg && (
                        <div className="alert alert-success">
                          <span>{this.state.successMsg}</span>
                        </div>
                      )}
                      <div className="form-group">
                        <label
                          htmlFor="recipient-name"
                          className="col-form-label"
                        >
                          First Name:
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          value={this.state.editUser.firstName}
                          required="required"
                          onChange={e =>
                            this.updateEditUser(e.target.value, "firstName")
                          }
                        />
                      </div>
                      <div className="form-group">
                        <label
                          htmlFor="recipient-name"
                          className="col-form-label"
                        >
                          Last Name:
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          value={this.state.editUser.lastName}
                          required="required"
                          onChange={e =>
                            this.updateEditUser(e.target.value, "lastName")
                          }
                        />
                      </div>
                      <div className="form-group">
                        <label
                          htmlFor="recipient-name"
                          className="col-form-label"
                        >
                          Username:
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          value={this.state.editUser.userName}
                          required="required"
                          onChange={e =>
                            this.updateEditUser(e.target.value, "userName")
                          }
                        />
                      </div>
                      <div className="form-group">
                        <label
                          htmlFor="recipient-name"
                          className="col-form-label"
                        >
                          Role
                        </label>
                        <select
                          name="role"
                          className="form-control"
                          required="required"
                          value={this.state.editUser.role}
                          onChange={e =>
                            this.setState({ role: e.target.value })
                          }
                        >
                          <option>Select Role</option>
                          <option value="TenantUser">User</option>
                          <option value="TenantAdmin">Admin</option>
                        </select>
                      </div>
                    </div>
                    <footer className="modal__footer filter-form-actions">
                      <button
                        type="submit"
                        className="btn btn-primary btn-rounded"
                        disabled={this.state.editLoader && "disabled"}
                      >
                        {this.state.editLoader ? "Updating...." : "Update User"}
                      </button>
                      <button
                        className="btn btn-default btn-rounded"
                        data-micromodal-close
                        aria-label="Close this dialog window"
                      >
                        Cancel
                      </button>
                    </footer>
                  </form>
                </div>
              </main>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({}, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Company);
