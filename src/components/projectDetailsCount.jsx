import React, { Component } from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getRegisteredModels } from '../ducks/models';
import { alertMsg } from "../ducks/alertsReducer";
import { getDeployments } from "../ducks/deployments";
import {getExperimentList} from "../ducks/experiments";
import {jobsListAction} from "../ducks/jobs";
import { getNotebookList } from "../api";
import OverViewCard from './overViewCard';
import { Link } from "react-router-dom";



class ProjectDetailsCount extends Component {
    constructor(props) {
        super(props);
    }
    state = { 
        filters: {
            projectName: "",
        },
        totalModels: 0,
        totalNotebooks:0,
     }

     componentDidMount() {
        this.getModels(),
        this.props.getExperimentList({projectIDEquals: this.props.selectedProject.id }),
        this.props.getDeployments({ projectID: this.props.selectedProject.id }),
        this.fetchNotebooks(),
        this.props.jobsListAction({projectIDContains: this.props.selectedProject.id});
    }

    componentDidUpdate = (prevProps) => {
        if (prevProps.selectedProject.id !== this.props.selectedProject.id) {
            this.getModels(),
            this.props.getExperimentList({projectIDEquals: this.props.selectedProject.id }),
            this.props.getDeployments({ projectID: this.props.selectedProject.id }),
            this.fetchNotebooks(),
            this.props.jobsListAction({projectIDContains: this.props.selectedProject.id});

        }
    }

    getModels = (filters={...this.state.filters}) => {
        const projectID = this.props.selectedProject.id;
        if(projectID)
            filters = {...filters, projectID: projectID}
        this.props.getRegisteredModels(
            filters,
            (data) => {
                this.setState({
                    totalModels: data.headers["x-total-count"],
                })
            },
            (err) => {
                this.props.alertMsg("Unable to get the model count, please try again", "error")
            }
        );
    }


    fetchNotebooks = () => {

      getNotebookList(this.props.token, this.props.selectedProject.id)
        .then((res) => {
          this.setState({
            totalNotebooks: res.data.notebooks.length,
          });
        })
        .catch((error) => {
          console.error("error fetching notebooks count: ", error);
        });
    };    



    render() {
        
        return ( 
                <div className="project-team-wraper m-0">
                        <div>
                         <div className='row mb-4'>
                            <div className='col-3'>
                              <Link to={"/notebooks"} className="nounderline">
                                  <OverViewCard name="Notebooks" count = {this.state.totalNotebooks}/>
                              </Link>
                            </div>
                             <div className='col-3'>
                               <Link to={"/experiments"} className="nounderline">
                                  <OverViewCard name="Experiments" count = {this.props.experiments.length}/>
                                </Link>
                             </div>
                             <div className='col-3'>
                               <Link to={"/models"} className="nounderline">
                                  <OverViewCard name="Models" count = {this.state.totalModels}/>
                                </Link>
                             </div>
                         </div>
                         <div className='row'>
                            <div className='col-3'>
                              <Link to={"/deployments"} className="nounderline">
                                 <OverViewCard name="Deployments" count = {this.props.deployments.length}/>
                               </Link>
                            </div>
                            <div className='col-3'>
                              <Link to={"/workflows"} className="nounderline">
                                 <OverViewCard name="Workflows" count = {this.props.jobs.length}/>
                              </Link>
                            </div>  
                          </div>                                                                                                                                                    
                        </div>
                </div>
         );
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token,
        registeredModels: state.models.registeredModels,
        selectedProject: state.projects.globalProject,
        deployments: state.deployments.deployments,
        experiments: state.experiments.experiments,
        jobs: state.jobs.jobs,
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            getRegisteredModels,
            alertMsg,
            getDeployments,
            getExperimentList,
            jobsListAction
        },
        dispatch
    );
};
export default connect(mapStateToProps, mapDispatchToProps)(ProjectDetailsCount);
