import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import { updateJobAction, runJobAction } from "../ducks/jobs";
import { alertMsg } from "../ducks/alertsReducer";
import {delKeys} from "../utils/helper";
import { confirm } from "./Confirm";

const schType = [
    'CRON',
    'TIME_DRIVEN_HOURS',
    'TIME_DRIVEN_MINS',
    'TIME_DRIVEN_SECS'
]

class Scheduler extends Component {
    constructor(props){
        super(props);
        this.state = {
            type: 0,
            cron_expression: '',
            duration: '',
            subtype: '',
            buttonDisabled: false
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleStop = this.handleStop.bind(this);
    }

    componentDidMount() {
        if(this.props.jobDetails.schedule) {
            this.setState({
                type: schType.indexOf(this.props.jobDetails.schedule.type),
                cron_expression: this.props.jobDetails.schedule.cron_expression,
                duration:  this.props.jobDetails.schedule.duration,
                subtype: schType.indexOf(this.props.jobDetails.schedule.type)
            })
        }
    }

    UNSAFE_componentWillReceiveProps(props) {

        if(props.successMessage === "Job successfully updated") {
            this.setState({
                buttonDisabled: !this.state.buttonDisabled
            }, ()=>{
                this.props.alertMsg("Updated!","success")    
                this.props.goBack()
            });
        }
    }

    validate = () => {
        if(!this.state.cron_expression && this.state.type === 0) {
            this.props.alertMsg("Invalid Cron Expression","error");
            return false;
        }

        if(this.state.cron_expression.length < 8 && this.state.type === 0) {
            this.props.alertMsg("Invalid Cron Expression","error");
            return false;
        }

        if(this.state.type > 0 && !this.state.duration){
            this.props.alertMsg("Invalid Duration","error");
            return false;
        }

        return true;
    }

    handleSubmit() {
        if(!this.validate())
            return 0;
        
        
        this.props.alertMsg("Scheduling...","info");
        let finalData = {...this.props.jobDetails};
        let parsedData = {
            type: schType[this.state.type],
            duration: parseInt(this.state.duration, 10),
            cron_expression:  this.state.cron_expression
        }
        delKeys(parsedData);
        finalData.schedule = {...parsedData};
        this.props.updateJobAction(this.props.token, finalData);
    }

    handleStop() {
        this.props.alertMsg("Deleting...","warning");
        let finalData = {...this.props.jobDetails};
        finalData.schedule = null;
        this.props.updateJobAction(this.props.token, finalData);
    }

    handleChange(e) {
        const target = e.target;
        let name = target.name;
        const value = target.value;

        if(name === "subtype" || name === "type") {
            this.setState({type: value, subtype: value});
        } else {
            this.setState({[name]: value});
        }
    }

    render() {
        const scheduled = this.props.jobDetails.schedule ? true : false
        return(
            <div style={{width: 300}}>
                <TextField
                    label={"Scheduler Type"}
                    value={this.state.type >= 1 ? 1 : 0}
                    margin="normal"
                    fullWidth
                    select
                    onChange={this.handleChange}
                    name="type"
                    InputLabelProps={{
                        shrink: true,
                    }}
                >
                    <MenuItem value={0}>
                        Cron
                    </MenuItem>
                    <MenuItem value={1}>
                        Periodic
                    </MenuItem>
                </TextField>

                {
                    this.state.type === 0 ?
                    <div>
                        <TextField
                            label={"Cron expression"}
                            value={this.state.cron_expression}
                            fullWidth
                            margin="normal"
                            placeholder="Enter Cron exp"
                            name="cron_expression"
                            onChange={this.handleChange}
                            InputLabelProps={{
                                shrink: true,
                            }} 
                        />
                    </div> :
                    <div>
                        <TextField
                            label={"Period"}
                            value={this.state.subtype}
                            fullWidth
                            margin="normal"
                            select
                            name="subtype"
                            onChange={this.handleChange}
                            
                            InputLabelProps={{
                                shrink: true,
                            }}
                        >
                            <MenuItem value={1}>
                                Hour
                            </MenuItem>
                            <MenuItem value={2}>
                                Minute
                            </MenuItem>
                            <MenuItem value={3}>
                                Seconds
                            </MenuItem>
                        </TextField>
                        <TextField
                            label={"Duration"}
                            value={this.state.duration}
                            fullWidth
                            margin="normal"
                            placeholder="0"
                            type={"number"}
                            name="duration"
                            inputProps={{ min: "0", step: "1" }}
                            onChange={this.handleChange}
                            InputLabelProps={{
                                shrink: true,
                            }}
                        >
                        </TextField>
                    </div>
                }
                <div style={{marginTop: 15}} >
                    <button className="btn btn-sm btn-primary" type="button" onClick={()=>{
                        confirm("Are you sure?").then(
                            () => {
                                this.handleSubmit();
                            },
                            () => {
                                this.props.alertMsg("Cancelled", "error")
                            }
                        );
                    }} name="save">
                        Update
                    </button>
                    {scheduled ?
                    <button className="btn btn-sm btn-danger ml" type="button" onClick={()=>{
                        confirm("Are you sure?").then(
                            () => {
                                this.handleStop();
                            },
                            () => {
                                this.props.alertMsg("Cancelled", "error")
                            }
                        );
                    }} name="stop">
                        Delete
                    </button> : 
                    ""}
                </div>
            </div>
        )
    }

}

const mapStateToProps = state => {
  return {
    successMessage: state.jobs.successMessage
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
        updateJobAction,
        runJobAction,
        alertMsg
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Scheduler);