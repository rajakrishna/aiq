import React, { Component } from "react";
import { connect } from "react-redux";
import { TextField } from "@material-ui/core";
import { getNotebooksUnusedVolumes } from "../api";

import { Config } from "./Notebook.config.js";


class AddStorageForm extends Component {
  state = {
    dataVols: [],
    ws_type: Config.spawnerFormDefaults.workspaceVolume.value.type.value,
    ws_name: Config.spawnerFormDefaults.workspaceVolume.value.name.value,
    ws_size: Config.spawnerFormDefaults.workspaceVolume.value.size.value,
    ws_mount_path:
      Config.spawnerFormDefaults.workspaceVolume.value.mountPath.value,
    ws_access_modes:
      Config.spawnerFormDefaults.workspaceVolume.value.accessModes.value,
    remainedVolumes:[]
  };
  constructor(props) {
    super();
    this.state.dataVols = props.workSpace.dataVols;
  }

  componentDidMount(){
    getNotebooksUnusedVolumes(this.props.token,this.props.selectedProject.id).then((res)=>{
      console.log("getNotebooksVolumes::addstorgeform::res.data.volumes::",res.data.volumes)
      this.setState({remainedVolumes:res.data.volumes})
    })
    .catch((err)=>console.log("getNotebooksVolumes::addstorageform::err.message::",err.message))
  }

  addVols = () => {
    const dataVols = [...this.state.dataVols];
    const i = dataVols.length;
    dataVols.push({
      vol_type: "New",
      vol_size: "10",
      vol_name: `data-vol-${i + 1}`,
      vol_mount_path: `/home/jovyan/data-vol-${i + 1}`,
      vol_access_modes: "ReadWriteOnce",
    });
    this.setState({ dataVols });
    this.props.addVols();
  };

  remVols = (index) => {
    let dataVols = [...this.state.dataVols];

    dataVols.splice(index, 1);
    this.setState({ dataVols });

    this.props.workSpace.dataVols.splice(index, 1);
  };

  handleVols = (index, name, value) => {
    let dataVol = [...this.state.dataVols];
    dataVol[index].vol_name = "";
    dataVol[index][name] = value;
    this.setState({
      dataVols: dataVol,
    },
    ()=>{
      if(this.state.dataVols[index].vol_type === 'Existing' && this.state.dataVols[index].vol_name){
        let selectedVolume = this.state.remainedVolumes.find((item)=> item.name === dataVol[index].vol_name)
 
        dataVol[index]["vol_size"] = selectedVolume.size.replace("Gi","");
        dataVol[index]["vol_access_modes"] = selectedVolume.access_mode[0];
        this.setState({...this.state,dataVols:dataVol})
       
           }
      }
      );
    this.props.handleVols(index, name, value);
  };

  handleChange = (e) => {
    const target = e.target;
    this.setState({ws_name:""})
    if (target.name === "nm") {
      this.setState({
        nm: target.value,
        ws_name: target.value,
      });
      return 0;
    }

    this.setState({
      [target.name]: target.value,
    });
    this.props.handleChange(e);
  };



  render() {
   
    return (
      <div className="package-content">
        <form onSubmit={this.handleSubmit} autoComplete="off">
          <div className="pd-lg pl-0">
            <div className="step-title">Step 3: Add Storage</div>
            <div className="mt-lg step-description">
              Your Notebook will be created within the following volumes. You
              can attach additional data volumes to your Notebook, or edit the
              settings of the workspace volume
            </div>

            <div className="col-sm-12 ml-0 pl-0 mt-5">
              <div className="storage-wrksp mt-0">Workspace volume</div>
              <div className="storage-wrksp-deac">
                Configure the Volume to be mounted as your personal Workspace.
              </div>
            </div>
            <div className="row mt-2 ml-0">
              <div className="col-sm-2 pl-0">
                <TextField
                  name="ws_type"
                  id="ws_type"
                  label={`Type`}
                  select
                  value={this.props.workSpace.ws_type}
                  onChange={this.handleChange}
                  margin="normal"
                  InputLabelProps={{ shrink: true }}
                  fullWidth
                  required
                >
                  <option className="clickable" value="New">
                    New
                  </option>
                  <option className="clickable" value="Existing">
                    Existing
                  </option>
                  <option className="clickable" value="None">
                    None
                  </option>
                </TextField>
              </div>
              <div className="col-sm-2">
              {
                (this.props.workSpace.ws_type === "Existing") ?
                <TextField
                name="ws_name"
                id="ws_name"
                label={`Name`}
                select
                onChange={this.handleChange}
                className={this.state.ws_type === "None" ? "text-gray" : ""}
                value={this.props.workSpace.ws_name}
                margin="normal"
                fullWidth
              >
                {
                this.state.remainedVolumes.map((item)=>{
                    let selectedInAddVols = [...this.state.dataVols] && 
                   [...this.state.dataVols].find((obj)=>obj.vol_name === item.name)
                  return(
                  <option
                   className={`clickable ${selectedInAddVols ? "disabled":""}`}
                   key={`${item.name}`} 
                   value= {item.name}
                   disabled={selectedInAddVols} 
                   >
                    {item.name}
                  </option>
                   )
                  }
                  )
                }
              </TextField>
              :
              <TextField
              name="ws_name"
              id="ws_name"
              label={`Name`}
              onChange={this.handleChange}
              className={this.state.ws_type === "None" ? "text-gray" : ""}
              value={this.props.workSpace.ws_name}
              margin="normal"
              fullWidth
            />
              }
              </div>
              <div className="col-sm-2">
                {
                  (this.props.workSpace.ws_type === "Existing") ?
                  <TextField
                    name="ws_size"
                    id="ws_size"
                    label={`Size (Gi)`}
                    type="number"
                    value={this.props.workSpace.ws_size}
                    onChange={this.handleChange}
                    margin="normal"
                    fullWidth
                    InputProps={{
                      readOnly:true,
                      disabled:true,
                    }}
                  />
                  :
                  <TextField
                  name="ws_size"
                  id="ws_size"
                  label={`Size (Gi)`}
                  type="number"
                  value={this.props.workSpace.ws_size}
                  onChange={this.handleChange}
                  margin="normal"
                  fullWidth
                  required={this.state.ws_type === "New"}
                  inputProps={{
                    min: 0,
                    step: 0.5,
                  }}
                  InputProps={{
                    readOnly: this.props.workSpace.ws_type !== "New",
                    disabled: this.props.workSpace.ws_type !== "New",
                  }}
                />

                }
              </div>
              <div className="col-sm-3">
                <TextField
                  name="ws_mount_path"
                  id="ws_mount_path"
                  label={`Mount Path`}
                  className={
                    this.props.workSpace.ws_type === "None" ? "text-gray" : ""
                  }
                  value={this.props.workSpace.ws_mount_path}
                  onChange={this.handleChange}
                  margin="normal"
                  fullWidth
                  required={this.props.workSpace.ws_type !== "None"}
                  InputProps={{
                    readOnly: this.props.workSpace.ws_type === "None",
                    disabled: this.props.workSpace.ws_type === "None",
                  }}
                />
              </div>
              <div className="col-sm-3">
                {
                  (this.props.workSpace.ws_type === "Existing") ?
                  <TextField
                  name="ws_access_modes"
                  id="ws_access_modes"
                  label={`Access Mode`}
                  value={this.props.workSpace.ws_access_modes}
                  onChange={this.handleChange}
                  margin="normal"
                  fullWidth
                  InputProps={{
                    readOnly:true,
                    disabled:true,
                  }}
                />
                :
                <TextField
                  name="ws_access_modes"
                  id="ws_access_modes"
                  label={`Access Mode`}
                  value={this.props.workSpace.ws_access_modes}
                  onChange={this.handleChange}
                  margin="normal"
                  fullWidth
                  // select
                  InputProps={{
                    readOnly:true,
                    disabled:true,
                  }}
                >
                  <option className="clickable" value="ReadWriteOnce">
                    ReadWriteOnce
                  </option>
                  <option className="clickable" value="ReadWriteMany">
                    ReadWriteMany
                  </option>
                  <option className="clickable" value="ReadOnlyMany">
                    ReadOnlyMany
                  </option>
                </TextField>
                }
              </div>
            </div>
            <div className="col-sm-12 mt-5 ml-0 pl-0">
              <div className="storage-datavolumes">Data volumes</div>
              <div className="storage-datavolumes-desc">
                Configure the Volume to be mounted as your personal Datasets.
              </div>
            </div>

            {this.state.dataVols.map((e, i) => {
              return (
                <div className="row ml-0" key={"data-vol" + i}>
                  <div className="col-sm-2">
                    <TextField
                      name={"vol_type" + i}
                      id={"vol_type" + i}
                      label={`Type`}
                      select
                      value={this.state.dataVols[i].vol_type}
                      onChange={(e) => {
                        this.handleVols(i, "vol_type", e.target.value);
                      }}
                      margin="normal"
                      InputLabelProps={{ shrink: true }}
                      fullWidth
                      required
                    >
                      <option className="clickable" value="New">
                        New
                      </option>
                      <option className="clickable" value="Existing">
                        Existing
                      </option>
                      <option className="clickable" value="None">
                        None
                      </option>
                    </TextField>
                  </div>
                  <div className="col-sm-2">
                    {
                     (this.state.dataVols[i].vol_type === 'Existing') ?
                     <TextField
                     name={"vol_name" + i}
                     id={"vol_name" + i}
                     label={`Name`}
                     value={this.state.dataVols[i].vol_name}
                     onChange={(e) => {
                       this.handleVols(i, "vol_name", e.target.value);
                     }}
                     margin="normal"
                     fullWidth
                     select
                   >
                          {
                       this.state.remainedVolumes && this.state.remainedVolumes.map((item)=>{
                          let selectedInAddVols = [...this.state.dataVols] && 
                          [...this.state.dataVols].find((obj)=>obj.vol_name === item.name)
                         return(
                            <option 
                              className={`clickable ${( selectedInAddVols ||  (item.name === this.props.workSpace.ws_name)) ? "disabled":""}`}
                              disabled={ selectedInAddVols || (item.name === this.props.workSpace.ws_name)} 
                              key={`${item.name}`} 
                              value={item.name}
                            >
                              {item.name}
                            </option>
                            )
                          }
                        )
                      }
                  </TextField>
                   :
                   <TextField
                      name={"vol_name" + i}
                      id={"vol_name" + i}
                      label={`Name`}
                      value={this.state.dataVols[i].vol_name}
                      onChange={(e) => {
                        this.handleVols(i, "vol_name", e.target.value);
                      }}
                      margin="normal"
                      fullWidth
                      required={this.state.dataVols[i].vol_type !== "None"}
                      InputProps={{
                        readOnly: this.state.dataVols[i].vol_type === "None",
                        disabled: this.state.dataVols[i].vol_type === "None",
                      }}
                    />
                    }

                  </div>
                  <div className="col-sm-2">
                    <TextField
                      name={"vol_size" + i}
                      id={"vol_size" + i}
                      label={`Size (Gi)`}
                      value={this.state.dataVols[i].vol_size}
                      type="number"
                      onChange={(e) => {
                        this.handleVols(i, "vol_size", e.target.value);
                      }}
                      margin="normal"
                      fullWidth
                      required={this.state.dataVols[i].vol_type === "New"}
                      inputProps={{
                        min: 0,
                        step: 0.1,
                      }}
                      InputProps={{
                        readOnly: this.state.dataVols[i].vol_type !== "New",
                        disabled: this.state.dataVols[i].vol_type !== "New",
                      }}
                    />
                  </div>
                  <div className="col-sm-2">
                    <TextField
                      name={"vol_mount_path" + i}
                      id={"vol_mount_path" + i}
                      label={`Mount Path`}
                      value={this.state.dataVols[i].vol_mount_path}
                      onChange={(e) => {
                        this.handleVols(i, "vol_mount_path", e.target.value);
                      }}
                      margin="normal"
                      fullWidth
                      required={this.state.dataVols[i].vol_type !== "None"}
                      InputProps={{
                        readOnly: this.state.dataVols[i].vol_type === "None",
                        disabled: this.state.dataVols[i].vol_type === "None",
                      }}
                    />
                  </div>
                  <div className="col-sm-3">
                    {
                      (e.vol_type === "Existing") ?
                      <TextField
                      name={"vol_access_modes" + i}
                      id={"vol_access_modes" + i}
                      label={`Access Mode`}
                      value={this.state.dataVols[i].vol_access_modes}
                      onChange={(e) => {
                        this.handleVols(i, "vol_access_modes", e.target.value);
                      }}
                      margin="normal"
                      fullWidth
                      InputLabelProps={{ shrink: true }}
                      InputProps={{
                        readOnly: true,
                        disabled:true,
                      }}
                       />
                       :
                    <TextField
                      name={"vol_access_modes" + i}
                      id={"vol_access_modes" + i}
                      label={`Access Mode`}
                      value={this.state.dataVols[i].vol_access_modes}
                      onChange={(e) => {
                        this.handleVols(i, "vol_access_modes", e.target.value);
                      }}
                      margin="normal"
                      fullWidth
                      // select
                      // required={this.state.dataVols[i].vol_type === "New"}
                      // InputProps={{
                      //   readOnly: this.state.dataVols[i].vol_type !== "New",
                      //   disabled: this.state.dataVols[i].vol_type !== "New",
                      // }}
                      InputProps={{
                        readOnly:true,
                        disabled:true,
                      }}
                    >
                      <option className="clickable" value="ReadWriteOnce">
                        ReadWriteOnce
                      </option>
                      <option className="clickable" value="ReadWriteMany">
                        ReadWriteMany
                      </option>
                      <option className="clickable" value="ReadOnlyMany">
                        ReadOnlyMany
                      </option>
                    </TextField>
                    }
                  </div>
                  <div className="col-sm-1">
                    <button
                      className="btn btn-danger btn-sm mt-4"
                      type="button"
                      onClick={() => {
                        this.remVols(i);
                      }}
                    >
                      <i className="fas fa-trash" />
                    </button>
                  </div>
                </div>
              );
            })}
            <div className="col-sm-12 mt-4 pl-0">
              <button
                className="btn btn-default btn-sm"
                type="button"
                onClick={() => this.addVols()}
              >
                <i className="fas fa-plus" /> ADD
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    selectedProject: state.projects.globalProject,
  };
};

export default connect(mapStateToProps)(AddStorageForm);
