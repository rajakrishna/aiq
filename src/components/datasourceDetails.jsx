import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { enabled } from "../easyAiRedux/DataSourceData"; 


const styles = theme => ({
    root: {
      flexGrow: 1,
      marginTop:24,
      marginBottom:24
    },
    paper: {
      textAlign: 'start',
    },
    edit:{
        marginTop:20,
        textTransform: 'none'
    },
    delete:{
        marginTop:20,
        textTransform: 'none',
        marginLeft:30
    },
    backbutton:{
        '&:hover': {
            cursor: "pointer",
            color: "blue"
          }
    },
    datasource:{
        fontWeight:550
    },
    params:{
        fontWeight:550
    },
    actionButton:{
      textTransform:'none'
    }
  });
  
  
class DataSourceDetails extends Component {
    constructor(props) {
        super(props);
    }
    state = { 
      open: false,
      details:{}
     }

  componentDidMount =()=>{
    if (!this.props.location.state) {
      this.props.history.push("/datasets")
      return
    }
    if(this.props.location.state.ref_id){
      this.setState({...this.state,details:enabled.find((item) => item.ref_id === this.props.location.state.ref_id)})
    }
    else{
      this.setState({...this.state, details:enabled[0]})
    }
  }

    handleBack =()=>{
        this.props.history.goBack()
    }

    handleEdit =(id)=>{ 
      this.props.history.push("/datasource_form",{ref_id:id})
    }

    handleDelete =()=>{
      this.props.history.push("/datasets")
      this.setState({ open: false });
    }

    handleClickOpen = () => {
      this.setState({ open: true });
    };

    handleClose = () => {
      this.setState({ open: false });
    };

    render() { 
        const { classes } = this.props;
        return ( 
            <div className='main-container bg-grey'>
                <div className="main-body pt-4 pl-4">
                    <div className="content-wraper-sm">
                        <div className="projects-wraper ml-5 mr-5">
                            <div className="model-header">
                              <h6 
                                className={classes.backbutton}
                                onClick={this.handleBack}
                               >
                                {`< Back`}
                                </h6>
                            </div>
                            <div className="card-body h-min-370 p-0">
                                <h4 className={classes.datasource}>Datasource</h4>
                                <div className={classes.root}>
                                 <Grid container spacing={24}>
                                    <Grid item xs={2}>
                                      <Typography  className={classes.paper}>
                                          Name
                                      </Typography>
                                    </Grid>
                                    <Grid item xs={10}>
                                       <Typography className={classes.paper}>
                                       {this.state.details.name}
                                      </Typography>
                                    </Grid>
                                    <Grid item xs={2}>
                                      <Typography  className={classes.paper}>
                                          Description
                                      </Typography>
                                    </Grid>
                                    <Grid item xs={10}>
                                       <Typography className={classes.paper}>
                                      {this.state.details.desc}
                                      </Typography>
                                    </Grid>
                                    <Grid item xs={2}>
                                      <Typography  className={classes.paper}>
                                          Created At
                                      </Typography>
                                    </Grid>
                                    <Grid item xs={10}>
                                       <Typography className={classes.paper}>
                                       {this.state.details.created_at}
                                      </Typography>
                                    </Grid>
                                    <Grid item xs={2}>
                                      <Typography  className={classes.paper}>
                                          Created By
                                      </Typography>
                                    </Grid>
                                    <Grid item xs={10}>
                                       <Typography className={classes.paper}>
                                          {this.state.details.created_by}
                                      </Typography>
                                    </Grid>
                                    <Grid item xs={2}>
                                      <Typography  className={classes.paper}>
                                          Updated At
                                      </Typography>
                                    </Grid>
                                    <Grid item xs={10}>
                                       <Typography className={classes.paper}>
                                      {this.state.details.updated_at}
                                      </Typography>
                                    </Grid>
                                    <Grid item xs={2}>
                                      <Typography  className={classes.paper}>
                                          Updated By
                                      </Typography>
                                    </Grid>
                                    <Grid item xs={10}>
                                       <Typography className={classes.paper}>
                                        {this.state.details.updated_by}
                                      </Typography>
                                    </Grid>
                                 </Grid>
                                </div>
                                 <h5 className={classes.params}>Parameters</h5>
                                    {this.state.details.params && this.state.details.params.map((item) => {
                                     
                                        return (
                                        <Grid container spacing={24}>
                                        <Grid item xs={2}>
                                          <Typography  className={classes.paper}>
                                            {item.name}
                                           </Typography>
                                       </Grid>
                                       <Grid item xs={10}>
                                         <Typography className={classes.paper}>
                                         {(item.name === 'password')?('*'.repeat(item.value.length)):item.value}
                                         </Typography>
                                       </Grid>
                                       </Grid>
                                  
                                        )
                                    })
                                  }
                               <Button 
                               variant="contained" 
                               size="small" 
                               color="primary"
                               onClick={()=>this.handleEdit(this.state.details.ref_id)} 
                               className={classes.edit}
                               >
                                  Edit
                                </Button>
                                <Button 
                                variant="contained" 
                                size="small" 
                                color="secondary"
                                // onClick={this.handleDelete} 
                                onClick={this.handleClickOpen}
                                className={classes.delete}
                                >
                                  Delete
                                </Button>
                                <Dialog
                                    open={this.state.open}
                                    onClose={this.handleClose}
                                    aria-labelledby="alert-dialog-title"
                                    aria-describedby="alert-dialog-description"
                                  >
                                    <DialogTitle id="alert-dialog-title">{"Are you sure you want to Delete?"}</DialogTitle>
                                    <DialogContent>
                                      <DialogContentText id="alert-dialog-description">
                                        {this.state.details.name} will be deleted completely
                                      </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                      <Button
                                      className={classes.actionButton} 
                                      onClick={this.handleClose} 
                                      size='small' 
                                      color="primary"
                                      >
                                        Cancel
                                      </Button>
                                      <Button
                                       className={classes.actionButton}
                                       onClick={this.handleDelete} 
                                       size='small' 
                                       color="primary"
                                       >
                                        Delete
                                      </Button>
                                    </DialogActions>
                                  </Dialog>
                            </div>

                        </div>

                    </div>

                </div>

            </div>
         );
    }
}

DataSourceDetails.propTypes = {
    classes: PropTypes.object.isRequired,
  };
 
export default withStyles(styles)(DataSourceDetails);