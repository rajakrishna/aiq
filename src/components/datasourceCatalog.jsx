import React, { Component } from 'react';
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import Button from '@material-ui/core/Button';
import { available } from "../easyAiRedux/DataSourceData";

const styles = (theme) => ({
    root: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "start",
      overflow: "hidden",
      padding: 2,
    },
    card: {
      display: "flex",
      width: 400,
      height: 136,
      marginBottom: 24,
      marginRight: 16
    },
    details: {
      display: "flex",
      flexDirection: "column",
      width: 265
    },
    content: {
      flex: "1 0 auto"
    },
    cover: {
      width: 138,
    },
    controls: {
      display: "flex",
      alignItems: "center",
      paddingLeft: theme.spacing.unit,
      paddingBottom: theme.spacing.unit
    },
    heading: {
      fontWeight: 600,
      fontSize: 19
    },
    subtitle: {
      fontWeight: 550,
      lineHeight: 1,
      fontSize: 12
    },
    body2: {
      width: 240,
      maxWidth: 240,
      maxHeight: 34,
      overflow: 'hidden',
      marginTop: 5
    },
    button: {
      marginLeft: 8,
      fontWeight: 550,
      textTransform: 'none'
    },
  
  });

class DataSourceCatalog extends Component {
    constructor(props) {
        super(props);
    }
    state = { 
      availableDatasource:available,
      query:""
     }

    handleAdd = (type) => {
        this.props.history.push("/datasource_form", { type: type })
      }

      handleSearch =(value)=>{
        this.setState({query:value}, () => {
          this.setState({availableDatasource:available.filter((item)=>item.type.toLowerCase().includes(this.state.query))})
        })
      }


    render() { 
        const { classes } = this.props;


        return ( 
            <div className="main-container pt-1 pl-4 bg-grey">
            <div className="main-body">
              <div className="content-wraper-sm">
                <div className="projects-wraper ml-5 mr-5">
                <div className="project-list-head clearfix mt-2">
                <h4 className="datasets_title">AVAILABLE</h4>
                <div className="projects-search">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Search projects"
                    value={this.state.query}
                    onChange={(event)=> this.handleSearch(event.target.value)}
                  />
                  <button
                    className="btn btn-primary"
                  >
                    <i className="fa fa-search" />
                  </button>
                </div>
                <div className="project-tools mr-5">
                  <button
                    className="btn btn-default"
                  >
                    <i className="fa fa-sync-alt" />
                  </button>
                </div>
                </div>
                 <div className="card-body h-min-370 p-0">
                 <div className={classes.root}>
                      {
                        this.state.availableDatasource.map((item) => {
                          return (
                            <Card className={classes.card} key={item.id}>
                              <CardMedia
                                className={classes.cover}
                                image={item.imgUrl}
                                title="datasource image"
                              />
                              <div className={classes.details}>
                                <CardContent className={classes.content} style={{ paddingTop: '10px', paddingBottom: '10px' }}>
                                  <Typography variant="title" className={classes.heading}>
                                    {item.heading}
                                  </Typography>
                                  <Typography variant="caption" color="textSecondary" className={classes.subtitle}>
                                    {item.subheading}
                                  </Typography>
                                  <Typography variant="caption" className={classes.body2}>
                                    Live From Space  Live From Space  Live From Space  Live From Space
                                  </Typography>
                                </CardContent>
                                <div className={classes.controls}>
                                  <Button
                                    variant="outlined"
                                    size='small'
                                    color="default"
                                    onClick={() => this.handleAdd(item.type)}
                                    className={classes.button}
                                  >
                                    Add
                                  </Button>
                                </div>
                              </div>
                            </Card>
                          )
                        })
                      }
                    </div>
                 </div>
                </div>
               </div>
              </div>
            </div>
         );
    }
}

DataSourceCatalog.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired
  };
 
export default withStyles(styles, { withTheme: true })(DataSourceCatalog);