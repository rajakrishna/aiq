import React from 'react';

const JobView = (props) => {
  const jobDetails = props.jobDetails;
  const spreadTab = props.spreadTab;
  const spreadTabName = props.spreadTabName;
  const spreadTabValue = props.spreadTabValue;
  return(
      <div>
        <div className="m-info-table">
          <h4>Source</h4>
          <table className="table data-table no-border modeldetails-table">
            <tbody>
                {spreadTab(jobDetails.spec.train ? jobDetails.spec.train.source : jobDetails.spec.deploy.source)}
            </tbody>
          </table>
        </div>
        <hr />
        <div className="m-info-table">
          <h4>Environment Variables</h4>
          <table className="table data-table no-border modeldetails-table">
            <thead>
              <tr>
                  <td>Name</td>
                  <td>Value</td>
              </tr>
            </thead>
            <tbody>
                {jobDetails.spec.train && jobDetails.spec.train.env ? 
                  jobDetails.spec.train.env.map((obj,i) => {
                    return(<tr key={obj.name+i}>
                      <td>{obj.name}</td>
                      <td>{obj.value}</td>
                    </tr>)
                  }) :
                  jobDetails.spec.deploy && jobDetails.spec.deploy.env && jobDetails.spec.deploy.env.map((obj,i) => {
                    return(<tr key={obj.name+i}>
                      <td>{obj.name}</td>
                      <td>{obj.value}</td>
                    </tr>)
                  })
                }
            </tbody>
          </table>
        </div>
        {jobDetails.spec.notifications &&
        <div>
          <hr />
          <div className="m-info-table">
            <h4>Notifications</h4>
            <table className="table data-table no-border modeldetails-table">
              <thead>
                <tr>
                  {spreadTabName(jobDetails.spec.notifications)}
                </tr>
              </thead>
              <tbody>
                <tr>
                  {spreadTabValue(jobDetails.spec.notifications)}
                </tr>
              </tbody>
            </table>
          </div>
        </div>}
      </div>
  )
}

export default JobView;