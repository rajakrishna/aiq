import React, { Component } from "react";
import { connect } from "react-redux";
import TextField from '@material-ui/core/TextField';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import { externalEndpoint, internalEndpoint } from "../api"
import CircularProgress from '@material-ui/core/CircularProgress';

class TestApi extends Component {
    state = {
        urls: [],
        url: "",
        input: `
        {
            "data": {
               "names": [],
               "ndarray": [[]]
            }
        }`,
        output: "",
        submitLoad: false
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.urls.join(",") !== prevState.urls.join(",")) {
            return {
                urls: nextProps.urls,
                url: nextProps.urls[0]
            }
        }
        return null;
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleSubmit = () => {
        this.setState({
            submitLoad: true
        })
        if (!this.state.url) {
            this.setState({
                output: "Invalid Url, Please select different option!",
                submitLoad: false
            })
            return 0;
        }
        let data = {};
        let err = 0;
        try {
            const inp = (this.state.input).replace(/\'/g, "\"");
            data = JSON.parse(inp);
        } catch (e) {
            this.setState({
                output: e.message,
                submitLoad: false
            });
            err = 1;
        }
        if (err) {
            return 0;
        }
        if (this.state.urls.indexOf(this.state.url)) {
            externalEndpoint(this.props.token, this.state.url, data)
                .then((res) => {
                    if (res.status === 200) {
                        this.setResponse(res);
                    } else {
                        this.setResponse(res);
                    }
                })
                .catch((err) => {
                    this.setResponse(err, 1)
                });
        } else {
            internalEndpoint(this.props.token, this.props.name, data)
                .then((res) => {
                    if (res.status === 200) {
                        this.setResponse(res);
                    } else {
                        this.setState({
                            submitLoad: false
                        })
                    }
                })
                .catch((err) => {
                    this.setResponse(err, 1)
                });
        }
    }

    setResponse = (res, err) => {
        if(err) {
            this.setState({
                headers: res.response && res.response.headers,
                status: res.response && res.response.status,
                statusText: res.response && res.response.statusText,
                output: res.response && res.response.data,
                submitLoad: false
            })
        } else {
            this.setState({
                headers: res.headers,
                status: res.status,
                statusText: res.statusText,
                output: res.data,
                submitLoad: false
            })
        }
    }

    render() {
        const { urls } = this.state;
        return (
            <div className="m-details-body" style={{ marginTop: 30 }}>
                <FormControl component="fieldset">
                    <FormLabel component="legend">REST API URL</FormLabel>
                    <RadioGroup
                        aria-label="Urls"
                        name="url"
                        value={this.state.url}
                        className="mlr"
                        onChange={this.handleChange}
                    >
                        {urls.map((e, i) => {
                            let type = ["Internal", "External"]
                            return (
                                <FormControlLabel key={'option' + i} value={e} control={<Radio />} label={<span><b>{type[i]}</b> <span style={{ color: "rgba(0, 0, 0, 0.54)" }}>({e})</span></span>} />
                            )
                        })}
                    </RadioGroup>
                </FormControl>
                <TextField
                    fullWidth={true}
                    label={"Request Body"}
                    value={this.state.input}
                    onChange={this.handleChange}
                    className={"mt-0"}
                    rows={10}
                    name={"input"}
                    margin="normal"
                    multiline
                />
                <button type="button" className="btn btn-primary mt-lg" onClick={this.handleSubmit} name="test">
                    Test
                </button>
                {
                    this.state.submitLoad &&
                    <div className="btn-loader ml">
                        <CircularProgress size={30} />
                    </div>
                }
                {this.state.status &&
                    <div className="mt-lg">
                        <label style={{ color: "rgba(0, 0, 0, 0.54)" }}>Response Status</label>
                        <pre className="mt-sm">
                            {this.state.status} {this.state.statusText}
                        </pre>
                    </div>
                }
                {this.state.headers &&
                    <div className="mt-lg">
                        <label style={{ color: "rgba(0, 0, 0, 0.54)" }}>Response Headers</label>
                        <pre className="h500 mt-sm">
                            {typeof (this.state.headers) === "object" ? JSON.stringify(this.state.headers, null, "  ").replace(/^\{.*?\n|\}$|^\s{2,}/gm,"") : this.state.headers}
                        </pre>
                    </div>
                }
                {this.state.output &&
                    <div className="mt-lg">
                        <label style={{ color: "rgba(0, 0, 0, 0.54)" }}>Response Body</label>
                        <pre className="h500 mt-sm">
                            {typeof (this.state.output) === "object" ? JSON.stringify(this.state.output, null, "  ").replace(/^\{.*?\n|\}$|^\s{2,}/gm,"") : this.state.output}
                        </pre>
                    </div>
                }

            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token
    };
};

export default connect(mapStateToProps)(TestApi);
