import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getAllModels, deleteModel } from "../api";
import Pagination from "./common/pagination";
import MicroModal from "micromodal";
import CardLoader from "./models/cardLoader";
import EditColumns from "./models/EditColumns";
import CompareExperiments from "./models/CompareExperiments";
import FilterModal from "./models/filterModal";
import Library from "./shared/Library";
import moment from "moment";
import { confirm } from "./Confirm";
import { alertMsg } from "../ducks/alertsReducer";
import User from "./shared/User";
import{startCase} from "lodash";
import _ from "lodash";
import Joyride from 'react-joyride';
// import UpArrowGreen from "../images/up-arrow-green.png";
// import { applyMiddleware } from "redux";
import Mixpanel from "mixpanel-browser";
import { getErrorMessage, getResponseError, capitalizeVars } from "../utils/helper";
import { Tooltip, Dialog, AppBar, Toolbar, Typography, IconButton, DialogContent, DialogActions, Checkbox } from "@material-ui/core";
import CloseIcon from '@material-ui/icons/Close';

class ExperimentsList extends Component {
  _isMounted = false;
  state = {
    listing: window.localStorage.expListing ? window.localStorage.expListing.split(",") : 
    [
      'type',
      'ml_algorithm',
      'created_date',
      'feature_significance',
      'training_duration',
      'testing_duration'
    ],
    listType: "EXPERIMENT",
    projectID: "",
    emptyModel: false,
    modelsList: [],
    card_loader: true,
    appliedFilters: {},
    compareDialogShow: false,
    compareList: [],
    compareChecks: [],
    filters: {
      projectName: "",
      healthScore: 0,
      dataDrift: 0,
      mlLibraryContains: "",
    },
    currentPage: 0,
    totalModels: 0,
    run: false,
    steps: [
      {
        target: '.exp-list',
        content: (
          <div className="fl">
            <h4>Experiments</h4>
            <p>
              All experiments in a project are automatically versioned. You can filter, search and sort experiments on date, algorithm, team member, ML library, etc.
            </p>
          </div>
        ),
        hideCloseButton: true,
        disableBeacon: true,
        disableOverlayClose: true,
        styles: {
          options: {
            width: 500
          }
        }
      },
      {
        target: '.model-name',
        content: (
          <div className="fl">
            <p>
              Click on an Experiment to see more details.
            </p>
          </div>
        ),
        locale: { last: <strong aria-label="last">Next</strong> },
        hideCloseButton: true,
        disableBeacon: true,
        disableOverlayClose: true,
      }
    ]
  };

  static getDerivedStateFromProps(nextProps) {
    return ({ 
      run: nextProps.history.location.state ? nextProps.history.location.state.tutorial : false
    });
  }

  handleJoyrideCallback = (data) => {
    if(data.action === "next" && data.status === "finished") {
      this.props.history.push("/experiments/"+this.state.modelsList[0].id, {tutorial: true});
    }
  }

  componentDidMount() {
    this._isMounted = true;
    this.setState({
      listType: "EXPERIMENT",
      projectID:this.props.selectedProject && this.props.selectedProject.id
    });
    MicroModal.init({
      disableScroll: true,
      disableFocus: false,
      awaitCloseAnimation: false,
      debugMode: true
    });
    if(this.props.selectedProject && this.props.selectedProject.id){
      this.getModels();
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.selectedProject && prevProps.selectedProject.id !==  this.props.selectedProject.id) {
      this.setState({
        listType: "EXPERIMENT",
        projectID: this.props.selectedProject.id
      });
      if(this.state.filters){
        this.applyFilters(this.state.filters);
      }else{
        this.getModels();
      }
     }
  }
  componentWillUnmount() {
    this._isMounted = false;
  }

  delExpAction = (id) => {
    this.props.alertMsg("Deleting Experiment...", "info");
    deleteModel(this.props.token, id)
    .then(res => {
      if(res.status === 200) {
        this.getModels(_.pickBy(this.state.filters, _.identity));
        this.props.alertMsg("Experiment deleted successfully", "success");
        Mixpanel.track('Delete Experiment Successful', { 'model_id': id });
      } else {
        const errorMessage = getResponseError(res) || "An error occurred while deleting the Experiment, please try again.";
        Mixpanel.track('Delete Experiment Failed', { 'model_id': id, error: errorMessage });
        this.props.alertMsg(errorMessage, "error");
      }
    })
    .catch(err => {
      const errorMessage = getErrorMessage(err) || "An error occurred while deleting the Experiment, please try again.";
      Mixpanel.track('Delete Experiment Failed', { 'model_id': id, error: errorMessage});
      this.props.alertMsg(errorMessage, "error");
    })
  }

  // getModels fetches models based on different search criteria
  getModels = (params) => {
    this.setState({ modelsList: [], card_loader: true });
    const defaultParams = {
      // statusEquals: this.props.listType || this.state.listType,
      projectIDEquals:this.props.selectedProject && this.props.selectedProject.id || this.state.projectID
    };
    let allParams = { ...defaultParams, ...params };
    let currentPage = allParams.page ? allParams.page : 0;
    
    let ml_algorithm = params && params.ml_algorithm
    if (ml_algorithm && ml_algorithm.value && ml_algorithm.value != ""){
      allParams[`mlAlgorithm${ml_algorithm.key}`] = ml_algorithm.value
    }
    let type = params && params.type
    if (type && type.value && type.value != ""){
      allParams[`type${type.key}`] = type.value
    }
    if(!allParams.sort) {
      allParams = {...allParams, sort: "created_date,desc"};
    }
    getAllModels(this.props.token, allParams)
      .then(response => {
        if (response.status && this._isMounted) {
          // var startPageIndex = this.state.startPageIndex ? this.state.startPageIndex : 0
          this.setState({
            modelsList: response.data,
            card_loader: false,
            emptyModel: response.data.length > 0 ? false : true, 
            totalModels: response.headers["x-total-count"],
            currentPageSize: response.data.length,
            currentPage: currentPage
          });
        }
      })
      .catch(error => {
        console.error("error fetching models: ", error);
      });
  };
  handleInputChange = e => {
    let filters = { ...this.state.filters };
    filters[e.target.name] = e.target.value;
    this.applyFilters(filters)
  };
  applyFilters = filters => {
    this.setState({ filters, currentPage: 1 });
    this.getModels(_.pickBy(filters, _.identity));
  };
  handleRemoveFilter = filter => {
    let updatedFilters = { ...this.state.filters };
    updatedFilters[filter] = null;
    this.setState({ filters: updatedFilters });
    this.applyFilters(updatedFilters);
  };

  handlePageChange = pageAction => {
    let pageNumber = this.state.currentPage;
    pageAction === "nextPage" ? (pageNumber += 1) : (pageNumber -= 1);
    let params = { page: pageNumber, ...this.state.filters };
    this.getModels(params);
  };

  handleCheckbox = (id, data) => {
    const { compareChecks } = this.state,
          tempArr = [...compareChecks],
          index = tempArr.indexOf(id),
          compareList = [...this.state.compareList];
    if(index > -1) {
      tempArr.splice(index, 1);
      this.setState({
        compareChecks: tempArr
      })
    } else {
      tempArr.push(id);
      compareList.push(data);
      this.setState({ 
        compareChecks: tempArr,
        compareList
      })
    }
  }

  getCells = (data, listing) => {
    const {compareChecks} = this.state
    const cells = [];
    cells.push(
      <td className="model-name" key={"cb"+data.id}>
        <Checkbox
          checked={compareChecks.includes(data.id)}
          onChange={()=>{this.handleCheckbox(data.id, data)}}
          inputProps={{ 'aria-label': 'primary checkbox' }}
        />
      </td>,
      <td className="model-name" key={"name"+data.id}>
        <span className="text-link" onClick={()=>{this.props.history.push(`/experiments/${data.id}`,{prevUrl:this.props.history.location.pathname})}}>
          {data.name}
        </span>
        <p 
          className="data-text blank-link clickable"
          onClick={()=>{this.props.history.push('/projects/'+data.project_id)}}
        >
          <small>{data.project_name}</small>
        </p>
      </td>,
      <td key={"version"+data.id}>{data.version}</td>,
      <td key={"library"+data.id}>
        <ul className="stack-list">
          <Library library={data.ml_library} />
        </ul>
      </td>,
      <td key={"user"+data.id}>
        <div className="project-members-info">
          <ul className="project-members-list">
            <User user={data.owner} />
          </ul>
        </div>
      </td>
    );
    listing.forEach(e => {
      if(e.indexOf("performance") > -1) {
        cells.push(
          <td key={e+data.id}>{this.expandPerfMetrics(data.performance_metrics, e, data)}</td>
        );
      } else if(e.indexOf("hyper") > -1) {
        cells.push(
         <td key={"hyperparameters"+data.id}>{data.hyperparameters[e.replace(/^\w+\./g,"")] || "-"}</td>
        )
      } else if(e === "created_date") {
        cells.push(
        <td key={"created_date"+data.id}>{data[e] && moment(data[e]).format("LL")}</td>
        )
      } else if(e === "feature_significance") {
        cells.push(
        <td key={"feature_significance"+data.id}>{(data[e] && data[e].length) || "-"}</td>
        )
      } else {
        cells.push(
          <td key={e+data.id}>{data[e] ? data[e] : "-"}</td>
        )
      }
    });
    return cells.flat();
  }

  expandPerfMetrics = (data, key, spec) => {
    const name = key.replace(/^\w+\./g,"");
    let cell = "-";
    if(!data || (data && !data[0])) return cell;
    for(let i in data[0].metrics) {
      if(data[0].metrics[i].name === name) {
        cell = data[0].metrics[i].value
      }
    }

    /* 
      Code below can be used to compare the
      equal values among all the Metrics in the given data.
    */

    // const metrics = []
    // data.forEach(e => {
    //   e.metrics && e.metrics.forEach(val => {
    //     if(val.name === name)
    //       metrics.push(val)
    //   })
    // })
    // for(let i in metrics) {
    //   let flag = 0;
    //   for(let j in metrics) {
    //     if(i !== j) {
    //       if(metrics[i].value === metrics[j].value) {
    //         flag++;
    //         if(flag === data.length-1) {
    //           cell = <td>{metrics[i].value}</td>
    //         }
    //       }
    //     }
    //   }
    // }
    
    return cell
  }

  compareDialog = () => {
    const {compareDialogShow, compareList} = this.state;
    return(
      <Dialog maxWidth="xl" open={compareDialogShow}>
        {/* <AppBar className="relative" color="inherit">
          <Toolbar style={{width: '200px'}}>
            <Typography variant="h6" color="inherit" style={{flex: 1}}>
              Comparing Experiments
            </Typography>
            <IconButton edge="start" color="inherit" onClick={()=>{this.setState({compareDialogShow: false})}} aria-label="close">
                <CloseIcon />
            </IconButton>
          </Toolbar>
        </AppBar> */}
        <DialogContent dividers="true">
          <Toolbar>
            <Typography variant="h6" color="inherit" style={{flex: 1}}>
              Comparing Experiments
            </Typography>
            <IconButton edge="start" color="inherit" onClick={()=>{this.setState({compareDialogShow: false})}} aria-label="close">
                <CloseIcon />
            </IconButton>
          </Toolbar>
          <div className="container-fluid mt-xxl">
            <div className="row mb">
              <div className="col-md-12">
                <CompareExperiments
                  expandPerfMetrics={this.expandPerfMetrics}
                  compareList={compareList}
                /> 
              </div>
            </div>
          </div>
        </DialogContent>
      </Dialog>)
  }

  renderExperimentList = (modelList, listing) =>{
      return (
        <div>
          <Joyride
            continuous={true}
            run={this.state.run}
            spotlightPadding={0}
            scrollOffset={100}
            scrollToFirstStep={true}
            showProgress={false}
            showSkipButton={false}
            steps={this.state.steps}
            callback={this.handleJoyrideCallback}
          />
          <div className="card-body horizontal-scroll mr-5">
            <table className="table data-table no-border">
              <thead>
                <tr>
                  <td></td>
                  <td>Name</td>
                  <td>Version</td>
                  <td>Stack</td>
                  <td>Owner</td>
                  {listing.map((e,i) => {
                    return <td key={"theadCells"+i}>{capitalizeVars(e.replace(/^\w+\./gi,""))}</td>
                  })}
                </tr>
              </thead>
              <tbody>
                {modelList &&
                  Object.keys(modelList).map((model, index) => {
                    return (
                      <tr key={index}>
                        {this.getCells(modelList[model], listing)}
                      </tr>
                    );
                  })}
              </tbody>
            </table>
          </div>
        </div>
      );
  }

  getFilterTab = (data) => {
    switch(typeof(data)){
      case "object":
        if(data.value){
          return `${data.key}-${data.value}`
        }
        if(data.key == "isBetween"){
          return `${data.key}-${data.value1}&${data.value2}`
        }
        if(data.value1){
          return `${data.key}-${data.value1}`
        }
      break;
      default:
        return data
    }
  }

  changeListing = (data) => {
    this.setState({
      listing: data
    },()=>{window.localStorage.setItem("expListing", data)})
  }

  handleCompareDialog = () => {
    const {compareChecks, compareList} = this.state;
    const tempList = compareList.filter(e => {
      return compareChecks.includes(e.id);
    });
    this.setState({
      compareList: tempList,
      compareDialogShow: true
    })
  }

  render() {
    const {
      modelsList,
      filters,
      totalModels,
      currentPage,
      currentPageSize,
      listing,
      compareChecks
    } = this.state;
    let appliedFilters = _.pickBy(filters, _.identity);
    const countersVisibility = this.props.countersVisibility;
    return (
      <div className="main-container">
        <div className="main-body">
          <div className="content-wraper-sm">
              <div>
                {/* {countersVisibility !== false ? <Counters /> : ""} */}
                <div className="card exp-list">
                  <div className="card-header header-with-search pl-4 mr-5 pt-3">
                    <h4>{this.state.listType}S</h4>
                    <div className="card-search">
                      <input
                        type="text"
                        className="form-control"
                        name="nameContains"
                        value={filters.nameContains || ""}
                        placeholder="SEARCH"
                        onChange={(e) => {
                          filters.nameContains = e.target.value
                          this.setState({filters})
                          //this.applyFilters(filters)
                        }}
                        onKeyPress={event => {
                          if (event.key === "Enter") {
                            this.handleInputChange(event)
                          }
                        }}
                      />
                    </div>
                    <div className="card-tools">
                    <div className="model-pagination inline-flex">
                        {totalModels > 20 && (
                          <Pagination
                            itemsCount={totalModels}
                            currentPage={currentPage}
                            currentPageSize={currentPageSize}
                            pageSize={20}
                            onPageChange={this.handlePageChange}
                          />
                        )}
                      </div>
                      <div className="model-pagination inline-flex">
                      {modelsList && modelsList.length ?
                        <Tooltip title="Edit View">
                          <button
                            className="btn btn-sm btn-default ml"
                            onClick={()=>{MicroModal.show("modal-edit-columns")}}
                            type="button"
                          >
                            <i className="fa fa-table"></i>
                          </button>
                        </Tooltip>
                         : ""}
                        <Tooltip title="Reload">
                          <button 
                            className="btn btn-sm btn-default ml" 
                            onClick={()=>{this.getModels(_.pickBy(this.state.filters, _.identity));}} 
                            type="button"
                          >
                            <i className="fa fa-sync-alt" />
                          </button>
                        </Tooltip>
                      </div>
                    </div>
                  </div>
                  <div className="model-filter-header pl-4 mr-5">
                    <div 
                      className="filter-btn"
                      style={{flexBasis: 211}}
                    >
                      <button
                        className="btn btn-primary btn-sm btn-rounded mr"
                        data-micromodal-trigger="modal-1"
                      >
                        Add Filter +
                      </button>
                      {compareChecks.length > 1 ?
                      <button
                        className="btn btn-primary btn-sm btn-rounded"
                        onClick={()=>{
                          this.handleCompareDialog();
                        }}
                      >
                        Compare
                      </button> :
                      <Tooltip title="Select atleast 2 experiments!">
                        <button
                          className="btn btn-disabled btn-sm btn-rounded"
                          disabled
                        >
                          Compare
                        </button>
                      </Tooltip>}
                    </div>
                    <div className="applied-filters">
                      <ul className="filter-items-list">
                        {!_.isEmpty(appliedFilters) &&
                          Object.keys(appliedFilters).map(key => (
                            <li key={key} className="filter-tag-item">
                              {startCase(key)}-{this.getFilterTab(appliedFilters[key])}
                              <i className="fa fa-times" style={{marginTop: 3}} onClick={() => this.handleRemoveFilter(key)}>
                              </i>
                            </li>
                          ))}
                      </ul>
                    </div>
                    <div className="filter-right-actions">
                      <div className="row align-items-center justify-content-between">
                        <div className="col">
                          <label><small>Sort By</small></label>
                          <select
                            name="sort"
                            onChange={this.handleInputChange}
                            id=""
                            className="form-control select-field"
                            value={appliedFilters.sort || ''}
                          >
                            <option value=''>Select</option>
                            <option value="type,asc">Type</option>
                            <option value="ml_library,asc">Library</option>
                            <option value="ml_algorithm,asc">Algorithm</option>
                            <option value="project_name,asc">Project</option>
                            <option value="created_date,desc">Created</option>
                          </select>
                        </div>
                        <div className="col">
                          <label><small>Created Date</small></label>
                          <select
                            name="created_date"
                            id=""
                            onChange={this.handleInputChange}
                            className="form-control select-field"
                            value={appliedFilters.created_date || ''}
                          >
                            <option value=''>Select</option>
                            <option value="now-1d">Last 1 day</option>
                            <option value="now-7d">Last 7 days</option>
                            <option value="now-30d">Last Month</option>
                          </select>
                        </div>
                      </div>
                      
                    </div>
                  </div>
                  <div className="card-body no-pad">
                    {this.state.card_loader ? <CardLoader /> : ""}
                    {modelsList.length !== 0 && (
                      this.renderExperimentList(modelsList, listing)
                    )}
                    { modelsList.length === 0 && !this.state.card_loader &&
                      (<div className="empty-items-box">
                      <h4>You dont have any Experiments!</h4>
                    </div>)
                    }
                  </div>
                </div>
              </div>
          </div>
        </div>
        <FilterModal
          applyFilters={this.applyFilters}
          filters={this.state.filters}
          project={(/project/).test(this.props.match.path)}
        />
        {modelsList && modelsList.length ? 
        <EditColumns
          modelsList={modelsList}
          changeListing={this.changeListing}
          listing={listing}
          key={modelsList && modelsList.length ? modelsList[0].id : "columns-edit"}
        /> : "" }
        {this.compareDialog()}
      </div>
    );
  }
}
const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
        alertMsg
    },
    dispatch
  );
};

const mapStateToProps = state => {
  return {
    token: state.auth.token,
    selectedProject: state.projects.globalProject,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ExperimentsList);
