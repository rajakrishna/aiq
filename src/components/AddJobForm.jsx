import React, { Component } from 'react';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { jobProto } from "./Jobs/jobProto";
import { toggleSteps, changeJobStore, changeJobStoreWebhook, changeJobParameters, errorStore, resetJobStore } from "../ducks/jobStore";
import { createJobAction, updateJobAction, resetJobs } from "../ducks/jobs";
import { getjobRunDetails } from "../ducks/jobRunDetails";
import { getProjectsList } from "../ducks/projects";
import StepsPicker from "./Jobs/StepsPicker";
import StepsTimeLine from "./Jobs/StepsTimeLine";
import { alertMsg } from "../ducks/alertsReducer";
import { validate } from './Jobs/form_elements/ValidateInput';
import { parseEnvSrcToArray } from "../utils/helper.js";

import { getJob, getJobRun } from '../api';

// import JobForm from "./Jobs/JobForm";

class AddJobs extends Component {
    steps = [
        "pre_train_steps",
        "train",
        "post_train_steps",
        "pre_validate_steps",
        "validate",
        "post_validate_steps",
        "pre_deploy_steps",
        "deploy",
        "post_deploy_steps"
    ]

    state = {
        proto: jobProto,
        timeLineKey: 0
    }

    static getDerivedStateFromProps(nextProps) {
        if(nextProps.history.location.state && nextProps.history.location.state.name) {
            return ({
                proto: nextProps.history.location.state
            });
        }
        return null;
    }

    componentDidMount() {
        this.props.resetJobStore();
        this.fetchDetails()
    }

    componentWillUnmount() {
        this.props.resetJobStore();
    }

    fetchDetails = () => {
        const proto = this.state.proto || jobProto;
        const id = this.props.match.params.id;
        const url = this.props.match.url;
        this.props.getProjectsList({size: 500});
        if(id && !(/run-history/g).test(url)) {
            getJob(this.props.token, id)
            .then(res => {
                if(res.status === 200) {
                    const newJobStore = JSON.parse(JSON.stringify(res.data));
                    this.props.changeJobStoreWebhook({newJobStore, viewable: true});
                    const parameters = JSON.parse(JSON.stringify(res.data)).spec.parameters;
                    this.props.changeJobParameters({parameters , viewable: true});
                    this.props.changeJobStore({...res.data, viewable: true});
                    this.setState({
                        proto: {...res.data, viewable: true}
                    });
                }
            })
            .catch(err => {
                this.props.alertMsg("Network Error, please try again!", "error");
            })
        } else if(id && (/run-history/g).test(url)) {
            const syncBtn = document.querySelector("#syncBtnIco");
            if(syncBtn) {
                if((/rotate/gi).test(syncBtn.className)) return;
                syncBtn.className += " rotate";
            }
            this.props.getjobRunDetails(id,
            (data) => {
                this.props.changeJobStore({...data, viewable: true, running: true});
                this.setState({
                    proto: {...data, viewable: true, running: true}
                });
                syncBtn && (syncBtn.className = "fa fa-sync");
                setTimeout(()=>{this.setState({timeLineKey: this.state.timeLineKey+1});},1000)
            },
            (err) => {
                this.props.alertMsg("Network Error, please try again!", "error");
                syncBtn && (syncBtn.className = "fa fa-sync");
            })
        } else {
            this.props.changeJobStore({...proto});
        }
    }

    isEmpty(obj) {
        for(var key in obj) 
            return false;

        return true
    }
      
    delKeys(app){
        for(var key in app){
            if(app[key] !== null && typeof(app[key]) === 'object' && app[key] !== ""){
                this.delKeys(app[key])
        
                if(this.isEmpty(app[key])) {
                delete app[key]
                }
            } 
            if(app[key] === null || !app[key]) {
                delete app[key]
            }
        }
    }

    submitJob = () => {
        if (this.props.submitStatus || this.props.updateStatus) return;
        const data = this.props.jobStore;
        const formData = {...data};
       console.log("formData",formData)
       localStorage.setItem("ttest",JSON.stringify(formData))
        if (!validate(formData, this.steps, this.props.errorStore)) {
            console.error(validate(formData, this.steps, this.props.errorStore));
            this.props.alertMsg("Please resolve all the error", "error");
            return;
        }
        this.delKeys(formData);
        const p1 = this.props.jobStore.spec.parameters;
        const p2 = this.props.mainJobStore.parameters && this.props.mainJobStore.parameters.parameters;
        const p3 = this.props.mainJobStore.parameters && this.props.mainJobStore.parameters.spec && this.props.mainJobStore.parameters.spec.parameters;
        if (p1&&p1.length> 0 || p2&&p2.length>0 || p3&&p3.length>0) {
            let parameters;
            if (p1&&p1.length > 0) {
                parameters = p1;
            } else if (p2&&p2.length > 0) {
                parameters = p2
            } else if (p3&&p3.length > 0) {
                parameters = p3
            }
            formData.spec.parameters = [...parameters]
        }
        this.props.alertMsg("Saving, please wait...", "info");
        if(formData.id) {
            this.props.updateJobAction(formData, 
                ()=>{
                    this.props.alertMsg("Workflow Updated!", "success");
                    this.fetchDetails();
                   // this.props.history.push(`/workflows`);
                    this.props.history.push(`/workflows/${this.props.jobStore.id || this.props.jobId}`,{prevUrl: "/workflow/new"});
                },
                (err)=>{
                    this.props.alertMsg(err, "error");
                }
            );
        } else {
           
            this.props.createJobAction(formData,
                ()=>{
                    this.props.alertMsg("Workflow Saved!", "success");
                    // this.fetchDetails();
                    // this.props.resetJobStore();/${this.props.jobStore.id || this.props.jobId}
                    this.props.history.push(`/workflows`);
                    // this.props.history.push(`/workflows/${this.props.jobStore.id || this.props.jobId}`,{prevUrl: "/workflow/new"});
                },
                (err)=>{
                    this.props.alertMsg(err, "error");
                }
            );
        }
    }

    componentDidUpdate(nextProps) {
        if(this.props.errorMsg && nextProps.errorMsg !== this.props.errorMsg) {
            this.props.alertMsg(this.props.errorMsg,"error");
        }
    }

    render() {
        const { proto, timeLineKey } = this.state;
        if(this.props.match.params.id && !proto.spec.project_id) return (<div></div>);
        return(
            <div className="main-container">
                <div className="main-body" style={{overflow: "hidden"}}>   
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-sm-12 pd-0">
                                <StepsPicker fetchMainDetails={this.fetchDetails} steps={this.steps} submitJob={this.submitJob} history={this.props.history}/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12 pd-0" style={{marginTop: 10}}>
                                <StepsTimeLine key={timeLineKey} steps={this.steps} proto={proto} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token,
        selectedSteps: state.jobStore.selectedSteps,
        jobStore: state.jobStore.jobStore,
        mainJobStore: state.jobStore,
        jobError: state.jobStore.jobError,
        user: state.users.user,
        projects: state.projects.projects,
        errorMsg: state.jobs.message,
        submitStatus: state.jobs.submitStatus,
        jobId: state.jobs.jobId,
        updateStatus: state.jobs.updateStatus
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            toggleSteps,
            getjobRunDetails,
            changeJobStore,
            changeJobStoreWebhook,
            changeJobParameters,
            getProjectsList,
            createJobAction,
            updateJobAction,
            errorStore,
            alertMsg,
            resetJobStore,
            resetJobs
        },
        dispatch
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(AddJobs);