import React, { Component } from 'react';
import createClass from 'create-react-class';
import Select from 'react-select';
import {
	getProjectsSummary,
	globalProjectSelection,
	getProjectsList
	} from "../ducks/projects";
 import { withRouter,Link } from "react-router-dom";
 import { connect } from "react-redux";
 import { bindActionCreators } from "redux";
 import ReactTooltip from 'react-tooltip';
import projectIcon from '../images/project_dropdown_icon.svg';


const InputValue = createClass({

	render () {

		return (
			<div className="Select-value" id='project_select_value'>
				<span 
				className="Select-value-label"
				 id='Select_project_label'
				 data-tip='' 
				 data-for="projectSearchDropdown_label"
				 >
					{this.props.children}
					<ReactTooltip 
					  id="projectSearchDropdown_label" 
					  place="bottom" 
					  type="dark" 
					  className='projectSearchDropdown_tooltip'
					  getContent={() => { return  <p className='project_icon_tooltip'>{this.props.children}</p> }}
					/>
				</span>
			</div>
		);
	}
});

class ProjectSearchDropdown extends Component {
    constructor(props) {
        super(props);
    }

	componentDidMount(){
		this.props.getProjectsList({ size: 5000 });
	  }

	setValue =(value)=> {
        if(value){
			const projectRequiredFind = this.props.projects.find((item)=> item.id === value.id)
			this.props.globalProjectSelection(projectRequiredFind);
        }
	}


	render () {
		let placeholder = <span className="project_select_placeholder">Select Project</span>;

		const projectOptions = this.props.projects.map(e => ({"label": e.name,"value":e.name,"id":e.id}))

		let iconStyle = {
		};

		return (
			<div className="section d-flex flex-row" style={{minWidth:'200px'}} id='project_select_section'>	
					<Link to={`/project_details`}>
						<span 
						style={iconStyle} 
						data-tip='' 
						data-for="projectSearchDropdown_value"
						>
							<img
							style={{maxHeight:'2.2rem'}}
							className="project_icon"
							src={projectIcon}
							alt=""
							/>
							<ReactTooltip 
							id="projectSearchDropdown_value" 
							place="bottom" 
							type="dark" 
							className='projectSearchDropdown_tooltip'
							getContent={() => { return  <p className='project_icon_tooltip'>click here to see project details</p> }}
							/>
						</span>
					</Link>
					<Select
						className='Select-to-border'
						onChange={this.setValue}
						options={projectOptions}
						placeholder={placeholder}
						value={this.props.selectedProject && this.props.selectedProject.name}
						valueComponent={InputValue}
						clearable={false}
						inputProps={{ style: { width: '150px',paddingLeft:'36px'}}}
					/>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
    return {
      projects: state.projects.projects,
      projectsSummary: state.projects.summary,
      selectedProject: state.projects.globalProject,
    };
  };
  
  const mapDispatchToProps = (dispatch) => {
    return {
      ...bindActionCreators(
        {
          globalProjectSelection,
          getProjectsSummary,
          getProjectsList
        },
        dispatch
      ),
    };
  };


export default withRouter(
	connect(mapStateToProps, mapDispatchToProps)(ProjectSearchDropdown)
	);