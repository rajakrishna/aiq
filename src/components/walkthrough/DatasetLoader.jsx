import React, { Component } from "react";
import { connect } from "react-redux";
import CircularProgress from '@material-ui/core/CircularProgress';
import { datasetSample } from "./SampleObject";
import { createDataset } from "../../api";

class DatasetLoader extends Component {
    datasetTimes = datasetSample.length;
    state = {
        loading: 1,
        message: "",
        id:[],
    };

    componentDidMount() {
        this.datasetCreation();
    }

    fetchDatasetData = (error = false) => {
        let str = btoa(String((new Date()).getTime()));
        str = str.replace(/[^\w]/g,"");
        let data = datasetSample[this.datasetTimes - 1];
        data.id = str;
        data.created_by = this.props.user && this.props.user.userName
        this.datasetTimes = error ? this.datasetTimes : --this.datasetTimes;
        return data;
    }

    datasetCreation = (error = false) => {
        let data = this.fetchDatasetData(error);
        
        createDataset(this.props.token, data)
        .then(res => {
            const arr = [...this.state.id];
            arr.push(res.data.id);
            if(!this.datasetTimes) {
                this.setState({
                    loading: 0,
                    id: [...arr],
                    message: <div> <i className="fa fa-check" style={{color: "green"}}/> Sample datasets have been created successfully!</div>
                },()=>{
                    this.props.updateState({
                        level: 3,
                        datasetId: [...this.state.id]
                    })
                })
            } else {
                this.setState({
                    id: [...arr]
                },() =>{
                this.datasetCreation()
                });
            }
        })
        .catch(res => {
            this.setState({
                loading: 0,
                message: 
                    <div> 
                        <i className="fa fa-times" style={{color: "red"}}/> Dataset creation failed, trying again!<br />
                        <CircularProgress size={20} />
                    </div>
            },()=>{
                setTimeout(() => {
                    this.setState({
                        loading: 1
                    });
                    this.datasetCreation(true);
                }, 5000);
            })
        })
    }

    render() {
        const { message } = this.state;
        return(
            <div>
                {
                    this.state.loading ?
                    <div className="mt-lg">
                        <CircularProgress size={20} /> Creating datasets...
                    </div> :
                    <div className="mt-lg">
                        {message}
                    </div>
                }
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        user: state.users.user,
        token: state.auth.token
    };
};
  
export default connect(mapStateToProps)(DatasetLoader);