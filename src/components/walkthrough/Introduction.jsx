import React from "react";

export const Introduction = () => {
    return (
        <div>
            <h4>Welcome to Predera!</h4>
            <p>Predera AIQ helps you collaborate, build, monitor and autofix your ML models.</p>
        </div>
    )
}