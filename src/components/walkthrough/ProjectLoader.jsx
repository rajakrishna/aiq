import React, { Component } from "react";
import { connect } from "react-redux";
import CircularProgress from '@material-ui/core/CircularProgress';
import { createProject } from "../../api";
import { Link } from "react-router-dom";

class ProjectLoader extends Component {
    state = {
        loading: 1,
        message: "",
    }

    componentDidMount() {
        this.projectCreation();
    }

    projectCreation = () => {
        let str = btoa(String((new Date()).getTime()));
        str = str.replace(/[^\w]/g,"");
        const randStr = str.slice(str.length - 5, str.length);
        let name = "churn-"+randStr;
        const data = {
            name: name,
            description: "Customer Churn Example",
            users: this.props.user && [this.props.user.userName]
        };
        createProject(this.props.token, data)
        .then(res => {
            this.setState({
                loading: 0,
                message: <div> 
                    <i className="fa fa-check" style={{color: "green"}}/> Sample Project has been created successfully!
                </div>
            },()=>{
                this.props.updateState({
                    level:2,
                    project_name: name,
                    project_id: res.data.id
                })
            })
        })
        .catch(res => {
            this.setState({
                loading: 0,
                message: 
                    <div> 
                        <i className="fa fa-times" style={{color: "red"}}/> Project <b>{name}</b> creation failed, trying again!<br />
                        <CircularProgress size={20} />
                    </div>
            },()=>{
                setTimeout(() => {
                    this.setState({
                        loading: 1
                    });
                    this.projectCreation();
                }, 5000);
            })
        })
    }

    render() {
        const { message } = this.state;
        return(
            <div>
                {
                    this.state.loading ?
                    <div className="mt-lg">
                        <CircularProgress size={20} /> Creating a project...
                    </div> :
                    <div className="mt-lg">
                        {message}
                    </div>
                }
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        user: state.users.user,
        token: state.auth.token
    };
};
  
export default connect(mapStateToProps)(ProjectLoader);