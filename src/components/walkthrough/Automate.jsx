import React, { Component } from "react";
import { connect } from "react-redux";
import ProjectLoader from "./ProjectLoader";
import ExperimentsLoader from "./ExperimentsLoader";
import { LinearProgress } from "@material-ui/core";
import DatasetLoader from "./DatasetLoader";
// import DeploymentLoader from "./DeploymentLoader";
// import ModelLoader from "./ModelLoader";
// import JobLoader from "./JobLoader";

class Automate extends Component {
    state = {
        level: 1,
        project_name: "",
        project_id: ""
    }

    updateState = (obj) => {
        this.setState(obj);
        this.props.update(obj);
    }

    render() {
        const { level, project_name, project_id } = this.state;
        return (
            <div>
                <h4>Telecom Customer Churn Prediction</h4>
                <div>
                    Customer churn, also known as customer attrition, is the loss of clients or customers.
                    Churn model predicts customer churn by assessing their propensity of risk to churn.
                </div>
                <br/>
                <div>The setup will take less than a minute. {level!==4 && <LinearProgress className="mt" />}</div>
                {
                    level >= 1 && <ProjectLoader updateState={this.updateState} />
                }
                {
                    level >= 2 && <DatasetLoader updateState={this.updateState} />
                }
                {
                    level >= 3 && <ExperimentsLoader datasetId={this.state.datasetId} updateState={this.updateState} project_name={project_name} project_id={project_id} />
                }
                {
                    level == 4 && 
                    <div className="mt-lg">
                        <b>Setup completed!</b>
                    </div>
                }
                {/* {
                    this.state.level === 3 && <DeploymentLoader />
                }
                {
                    this.state.level === 4 && <ModelLoader />
                }
                {
                    this.state.level === 5 && <JobLoader />
                }*/}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token
    };
};
  
export default connect(mapStateToProps)(Automate);