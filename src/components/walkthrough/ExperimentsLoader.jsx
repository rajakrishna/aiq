import React, { Component } from "react";
import { connect } from "react-redux";
import CircularProgress from '@material-ui/core/CircularProgress';
import { experimentSample, insightSample } from "./SampleObject";
import { createModel, createInsight } from "../../api";

class ExperimentsLoader extends Component {
    insightTimes = insightSample.length;
    experimentTimes = experimentSample.length;
    file_id = (process.env.REACT_APP_MODELS_FILE_ID).split(",");
    file_path = (process.env.REACT_APP_MODELS_FILE_PATH).split(",");
    state = {
        loading: 1,
        message: "",
    };

    componentDidMount() {
        this.experimentCreation();
    }

    fetchExperimentData = () => {
        let data = experimentSample[this.experimentTimes - 1];
        data.project_id = this.props.project_id;
        data.project_name = this.props.project_name;
        data.training_dataset_id = this.props.datasetId[this.experimentTimes + 1];
        data.test_dataset_id = this.props.datasetId[this.experimentTimes - 1];
        data.model_file_id = this.file_id[this.experimentTimes - 1] === "null" ? null : this.file_id[this.experimentTimes - 1];
        data.model_file_path = this.file_path[this.experimentTimes - 1] === "null" ? null : this.file_path[this.experimentTimes - 1];
        this.experimentTimes--;
        return data;
    }

    experimentCreation = () => {
        let data = this.fetchExperimentData();
        
        createModel(this.props.token, data)
        .then(res => {
            if(!this.experimentTimes) {
                this.insightTimes = insightSample.length;
                this.insightCreation(res.data.id, res.data.name, res.data.version);
                this.setState({
                    loading: 0,
                    message: <div> <i className="fa fa-check" style={{color: "green"}}/> Sample experiments have been created successfully!</div>
                })
            } else {
                this.insightTimes = insightSample.length;
                this.insightCreation(res.data.id, res.data.name, res.data.version);
                this.experimentCreation();
            }
        })
        .catch(res => {
            ++this.experimentTimes;
            this.setState({
                loading: 0,
                message: 
                    <div> 
                        <i className="fa fa-times" style={{color: "red"}}/> Experiment creation failed, trying again!<br />
                        <CircularProgress size={20} />
                    </div>
            },()=>{
                setTimeout(() => {
                    this.setState({
                        loading: 1
                    });
                    this.experimentCreation();
                }, 5000);
            })
        })
    }

    fetchInsightData = (id, name, version, error=false) => {
        let data = insightSample[this.insightTimes - 1];
        data.model_id = id;
        data.model_name = name;
        data.project_id = this.props.project_id;
        data.project_name = this.props.project_name;
        this.insightTimes = error ? this.insightTimes : --this.insightTimes;
        return data;
    }

    insightCreation = (id, name, version, error=false) => {
        let data = this.fetchInsightData(id, name, version, error);
        createInsight(this.props.token, data)
        .then(res => {
            if(!this.insightTimes && !this.experimentTimes) {
                this.props.updateState({
                    level: 4
                })
            } else {
                this.insightCreation(id, name, version);
            }
        })
        .catch(res => {
            ++this.insightTimes;
            this.insightCreation(id, name, version, true);
        })
    }

    render() {
        const { message } = this.state;
        return(
            <div>
                {
                    this.state.loading ?
                    <div className="mt-lg">
                        <CircularProgress size={20} /> Creating experiments...
                    </div> :
                    <div className="mt-lg">
                        {message}
                    </div>
                }
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        user: state.users.user,
        token: state.auth.token
    };
};
  
export default connect(mapStateToProps)(ExperimentsLoader);