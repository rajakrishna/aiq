import React, {Component} from "react";
import { connect } from "react-redux";
import { Introduction } from "./Introduction";
import Automate from "./Automate";
import { Link } from "react-router-dom";
import MicroModal from "micromodal";
import { getProjectsSummary } from "../../ducks/projects";
import { bindActionCreators } from "redux";


class TutorialModal extends Component {
    state = {
       slide: 0,
       projectParams: {
        page: 0,
        nameContains: ""
      },
    }

    updateState = (obj) => {
        this.setState(obj);
    }

    render() {
        const { slide, level, project_id } = this.state;
        return (
        <div
            className="modal micromodal-slide"
            id="tutorial-modal"
            aria-hidden="true"
        >
            <div className="modal__overlay" tabIndex="-1">
                <div
                    className="modal__container"
                    role="dialog"
                    aria-modal="true"
                    aria-labelledby="tutorial-modal-title"
                >
                    {/* <header className="modal__header">
                        <h2 className="modal__title">
        
                        </h2>
                        <button
                            type="button"
                            className="modal__close"
                            aria-label="Close modal"
                            data-micromodal-close
                        />
                    </header> */}
                    <main className="modal__content mb-0">
                        <div className="modal-filter-form">
                            <div className="modal-body">
                                {
                                    slide === 0 && <Introduction />
                                }
                                {
                                    slide === 1 && <Automate update={this.updateState} />
                                }
                            </div>
                            <div className="modal-footer content-center">
                            <div className="mb-lg">
                                {slide === 0 &&
                                <div className="inline-block mr-20">
                                    <div>Feeling Lucky!</div>
                                    <button type="button" className="btn btn-sm btn-primary" onClick={() => {
                                        this.setState({
                                            slide: slide+1
                                        })
                                    }} >
                                        Play with Existing Experiments
                                    </button>
                                </div>}
                                {slide === 0 &&
                                <div className="inline-block">
                                    <div>Adventurous?</div>
                                    <Link
                                        to="/Getting-Started"
                                        className="btn btn-sm btn-primary"
                                    >
                                        Log your first ML Experiment
                                    </Link>
                                </div>}
                            </div>
                            {slide === 0 &&
                            <Link
                                to="#"
                                className="f-12"
                                data-micromodal-close
                            >
                                Skip and go to My Repository
                            </Link>}
                            {slide === 1 && level === 4 &&
                            <button
                                type="button"
                                className="btn btn-sm btn-primary"
                                onClick={()=>{
                                    MicroModal.close('tutorial-modal')
                                    this.props.getProjectsSummary(this.state.projectParams)
                                    this.props.history.push("/projects", {tutorial: true});
                                }}
                            >
                                Explore Now!
                            </button>}
                            </div>
                        </div>
                    </main>
                </div>
            </div>
        </div>
        )
    }
}
const mapDispatchToProps = dispatch => {
    return bindActionCreators(
      {
       
        getProjectsSummary,
      
      },
      dispatch
    );
  };
const mapStateToProps = state => {
    return {
        token: state.auth.token,
    };
};
  
export default connect(mapStateToProps,mapDispatchToProps)(TutorialModal);