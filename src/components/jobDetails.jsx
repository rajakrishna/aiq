import React, { Component } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import LeftArrow from "../images/left-chevron.png";
import { getjobDetails } from "../ducks/jobDetails";
import TextField from '@material-ui/core/TextField';
import { Loader1 } from "./home/modelOverviewLoader";
import Scheduler from './scheduler';
import moment from "moment";
import JobsRunList from "./JobsRunList";
import { delJob } from "../api";
import JobView from "./jobView";
import JobViewSpl from "./jobViewSpl";
import User from "./shared/User";
import { Route } from "react-router-dom";
import Mixpanel from 'mixpanel-browser';
import { getErrorMessage, getResponseError } from "../utils/helper";
import { confirm } from "./Confirm";

const pathname = [
  '',
  // 'Details',
  'Job-runs',
  'Schedule'
]

class JobDetails extends Component {
  state = {
    currentUrl: ""
  }
  constructor(props) {
    super(props);
    this.getJobs = this.getJobs.bind(this);
  }

  componentDidMount() {
    Mixpanel.track('Job Details Page', { 'job_id': this.props.match.params.id });
    this.getJobs();
    if(this.props.history.location.state) {
      let prevUrl = this.props.history.location.state.prevUrl;
      this.setState({prevUrl})
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const url = nextProps.location.pathname;
    if(url !== prevState.currentUrl) {
      let index = pathname.findIndex((e,i)=>{
        if(url.includes(e))
          return i;
        return null
      })
      if(index<1) {
        index = 0;
      } else {
        index--;
      }
      return {
        currentUrl: url,
        currentIndex: index
      };
    }
    return null;
  }

  getJobs() {
    this.props.getjobDetails(this.props.match.params.id);
  }

  delJobAction(id) {
    this.props.alertMsg("Deleting Job...", "info");
    delJob(this.props.token, id)
      .then(res => {
        Mixpanel.track("Delete Job Successful", { 'job_id': id });
        this.props.alertMsg("Job deleted successfully", "success");
        this.props.history.push("/jobs");
      })
      .catch(error => {
        const errorMessage = getErrorMessage(error) || "An error occurred while deleting the Job, please try again.";
        Mixpanel.track("Delete Job Failed", { 'job_id': id, error: errorMessage });
        this.props.alertMsg(errorMessage, "error");
      });
  }

  editJobAction = (job) => { 
    this.props.history.push("/AddJobs", {...job});
  }

  parseString(str) {
      str = str.toUpperCase();
      return str.replace(/_/g," ").replace(/(\w)(\w{1,})/g,(str,grp1,grp2) => {
          return(grp1+grp2.toLowerCase());
      });
  }

  spreadObject(jobDetails) {
    return (
      jobDetails &&
      Object.keys(jobDetails).map((name,i)=>{
        let value = jobDetails[name];
        if(value == null || typeof value !== 'object') {
          return(
            <TextField
                key={"name"+i}
                label={this.parseString(name)}
                value={value ? value : "NULL"}
                margin="normal"
                readOnly={true}
                fullWidth
                InputLabelProps={{
                    shrink: true,
                }}
            />
          )
        } else {
          return(this.spreadObject(value));
        }
      })
    )
  }

  spreadTab = (obj) => {
    return (
      obj &&
        Object.keys(obj).map((name,i)=>{
        if(obj[name] == null) {
          return null;
        } else if(typeof obj[name] === "object") {
          return this.spreadTab(obj[name]);
        } else {
          return(<tr key={name+"row"+1}><td>{name}</td><td>{obj[name]}</td></tr>);
        }
      }) 
    )
  }

  spreadTabName = (obj) => {
    return (
      obj &&
        Object.keys(obj).map((name,i)=>{
        if(obj[name] == null) {
          return null;
        } else if(typeof obj[name] === "object") {
          return this.spreadTabName(obj[name]);
        } else {
          return(<td key={name+"col"+i}>{name}</td>)
        }
      }) 
    )
  }

  spreadTabValue = (obj) => {
    return (
      obj &&
        Object.keys(obj).map((name,i)=>{
        if(obj[name] == null) {
          return null;
        } else if(typeof obj[name] === "object") {
          return this.spreadTabValue(obj[name]);
        } else {
          return(<td key={name+"val"+i}>{obj[name]}</td>)
        }
      }) 
    )
  }

  render() {
    const { jobDetails } = this.props;
    const { url } = this.props.match;
    if(!Object.keys(jobDetails).length) {
      return (<Loader1 />);
    }
    return (
        <div className="main-container">
          {/* <NavigationLinks path={this.props.match.path} /> */}
          {/* <Sidebar path={this.props.match.path}/> */}
          <div className="main-body" style={{overflow: "hidden"}}>
            
            <div className="content-wraper-sm">
              <div className="m-details-card">
                <div className="m-details-card-head pl-4 mr-5">
                  {/* <div
                    className="back-action"
                    onClick={() => {this.state.prevUrl ? this.props.history.push(this.state.prevUrl) : this.props.history.goBack()}}
                  >
                    <img src={LeftArrow} alt="left arrow" />
                  </div> */}
                  <div className="m-title-info">
                    <h4>{jobDetails.name}</h4>
                    <small>{jobDetails.spec.project_name || "No Project Assigned"}</small>
                  </div>
                  {/* <div className="m-status-row">
                    {modelDetails.failing === false && (
                      <label className="model-item-status status-success">
                        {modelDetails.status}
                      </label>
                    )}
                    {modelDetails.failing === true && (
                      <label className="model-item-status status-failed">
                        {modelDetails.status}
                      </label>
                    )}
                  </div> */}
                  <div className="m-tech-stack">
                    <button
                      type="button"
                      className="btn btn-default btn-sm"
                      data-toggle="tooltip" title="Edit"
                      onClick={()=>{
                        confirm("Are you sure?", "Edit").then(
                          () => {
                            this.editJobAction(jobDetails)
                          },
                          () => {
                            this.props.alertMsg("Cancelled", "error")
                          }
                        )
                      }}
                    >
                      <i className="fa fa-edit"></i>
                    </button>
                    <button
                      type="button"
                      className="btn btn-danger btn-sm ml-sm"
                      data-toggle="tooltip" title="Delete"
                      onClick={()=>{
                        confirm("Are you sure?", "Delete").then(
                          () => {
                            this.delJobAction(this.props.match.params.id)
                          },
                          () => {
                            this.props.alertMsg("Cancelled", "error")
                          }
                        )
                      }}
                    >
                      <i className="fa fa-trash"></i>
                    </button>
                  </div>
                </div>

                <div className="m-info-table">
                  <table className="table data-table no-border modeldetails-table">
                    <thead>
                      <tr>
                        <td>Model Name</td>
                        <td>Type</td>
                        {jobDetails.type === "DEPLOY_AB_TEST" &&
                        <td>Ratio</td>}
                        <td>Created By</td>
                        <td>Created On</td>
                        <td>Last Run</td>
                        <td>Number of Runs</td>
                        <td>Scheduled</td>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>{jobDetails.spec.model_name}</td>
                        <td>{jobDetails.type}</td>
                        {jobDetails.type === "DEPLOY_AB_TEST" &&
                        <td>{jobDetails.spec.deploy && jobDetails.spec.deploy.ratio}</td>}
                        <td>
                          <div className="project-members-info">
                            <ul className="project-members-list">
                              <User user={jobDetails.created_by} />
                            </ul>
                          </div>
                        </td>
                        <td>{jobDetails.created_date && moment(jobDetails.created_date).format("LL")}</td>
                        <td>
                        {jobDetails.last_modified_date && moment(jobDetails.last_modified_date).format("LL")}
                        </td>
                        <td>
                          {jobDetails.number_of_runs || 0}
                        </td>
                        <td>
                          {jobDetails.schedule && jobDetails.schedule.type !== 'JOB_SCHEDULE_TYPE_NOT_SET' ? 'Yes' : 'No'}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <hr />
                <div className="m-details-body">
                  <Tabs selectedIndex={this.state.currentIndex}>
                    <TabList className="inline-tabs tabs-blue pad-50">
                      <React.Fragment>
                        {/* <Tab onClick={()=>{
                            Mixpanel.track('Job Details Page - Details Tab', { 'job_id': this.props.match.params.id });
                            this.props.history.push(`${url}/${pathname[1]}`);
                          }}>
                          DETAILS
                        </Tab> */}
                        <Tab onClick={()=>{
                            Mixpanel.track('Job Details Page - Job Runs Tab', { 'job_id': this.props.match.params.id });
                            this.props.history.push(`${url}/${pathname[1]}`);
                          }}>
                          JOB RUNS
                        </Tab>
                        <Tab onClick={()=>{
                            Mixpanel.track('Job Details Page - Schedule Tab', { 'job_id': this.props.match.params.id });
                            this.props.history.push(`${url}/${pathname[2]}`);
                          }}>
                          SCHEDULE
                        </Tab>
                      </React.Fragment>
                    </TabList>
                    <hr />
                    <React.Fragment>
                      <TabPanel>
                        <Route exact path={`(${url}/${pathname[1]}|${url})`} component={()=>{
                            if(jobDetails.type !== "DEPLOY_TRANSFORMER" && jobDetails.type !== "DEPLOY_AB_TEST")
                                return(
                                    <JobView 
                                        jobDetails={jobDetails} 
                                        spreadTab={this.spreadTab}
                                        spreadTabName={this.spreadTabName}
                                        spreadTabValue={this.spreadTabValue}
                                    />
                                );
                            else
                                return(
                                    <JobViewSpl
                                        jobDetails={jobDetails} 
                                        spreadTab={this.spreadTab}
                                        spreadTabName={this.spreadTabName}
                                        spreadTabValue={this.spreadTabValue}
                                    />
                                )
                            }} 
                        />
                      </TabPanel>
                      <TabPanel>
                        <Route exact path={`(${url}/${pathname[1]}|${url})`} component={()=>{
                          return(
                          <div className="m-info-table">
                            <JobsRunList id={jobDetails.id} token={this.props.token} updateListing={this.getJobs} />
                          </div>
                        )}} />
                      </TabPanel>
                      <TabPanel>
                        <Route exact path={`${url}/${pathname[2]}`} component={()=>{
                          return(
                          <div className="m-info-table">
                            <h4>Schedule</h4>
                            <div style={{margin:"0 10px"}}>
                              <Scheduler goBack={this.props.history.goBack} jobDetails={jobDetails} token={this.props.token} />
                            </div>
                          </div>
                        )}} />
                      </TabPanel>
                    </React.Fragment>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
        </div> 
      );
  }
}

const mapStateToProps = state => {
  return {
    jobDetails: state.jobDetails.jobDetails,
    token: state.auth.token
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getjobDetails
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(JobDetails);
