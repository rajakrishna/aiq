import React from 'react';
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";

const JobViewSpl = (props) => {
  const jobDetails = props.jobDetails;
  const type = jobDetails.type;
  const source_a = type === "DEPLOY_TRANSFORMER" ? { key: "model_source", name: "Model" } : { key: "source_a", name: "A" };
  const source_b = type === "DEPLOY_TRANSFORMER" ? { key: "transformer_source", name: "Transformer" } : { key: "source_b", name: "B" };
  const docker_a = type === "DEPLOY_TRANSFORMER" ? { key: "model_docker", name: "Model" } : { key: "docker_a", name: "A" };
  const docker_b = type === "DEPLOY_TRANSFORMER" ? { key: "transformer_docker", name: "Transformer" } : { key: "docker_b", name: "B" };
  const env_a = type === "DEPLOY_TRANSFORMER" ? { key: "model_env", name: "Model" } : { key: "env_a", name: "A" };
  const env_b = type === "DEPLOY_TRANSFORMER" ? { key: "transformer_env", name: "Transformer" } : { key: "env_b", name: "B" };
  const spreadTab = props.spreadTab;
  const spreadTabName = props.spreadTabName;
  const spreadTabValue = props.spreadTabValue;
  return (
    <div className="mlr">
      <Tabs>
        <TabList className="inline-tabs tabs-gray">
          <React.Fragment>
            <Tab>
              {source_a.name}
            </Tab>
            <Tab>
              {source_b.name}
            </Tab>
          </React.Fragment>
        </TabList>
        <React.Fragment>
          <TabPanel>
            <div className="row mlr-30">
              <div className="m-info-table col-sm-12">
                <h4>Source</h4>
                <table className="table data-table no-border modeldetails-table">
                  <tbody>
                    {spreadTab(jobDetails.spec.deploy[source_a.key])}
                  </tbody>
                </table>
              </div>
            </div>
            <hr />
            <div className="row mlr-30">
              <div className="m-info-table col-sm-12">
                <h4>Docker</h4>
                <table className="table data-table no-border modeldetails-table">
                  <tbody>
                    {spreadTab(jobDetails.spec.deploy[docker_a.key])}
                  </tbody>
                </table>
              </div>
            </div>
            <hr />
            <div className="row mlr-30">
              <div className="m-info-table col-sm-12">
                <h4>Environment Variables</h4>
                <table className="table data-table no-border modeldetails-table">
                  <thead>
                    <tr>
                      <td>Name</td>
                      <td>Value</td>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      jobDetails.spec.deploy && jobDetails.spec.deploy[env_a.key] && jobDetails.spec.deploy[env_a.key].map((obj, i) => {
                        return (<tr key={obj.name + i}>
                          <td>{obj.name}</td>
                          <td>{obj.value}</td>
                        </tr>)
                      })
                    }
                  </tbody>
                </table>
              </div>
            </div>
          </TabPanel>
          <TabPanel>
            <div className="row mlr-30">
              <div className="m-info-table col-sm-12">
                <h4>Source</h4>
                <table className="table data-table no-border modeldetails-table">
                  <tbody>
                    {spreadTab(jobDetails.spec.deploy[source_b.key])}
                  </tbody>
                </table>
              </div>
            </div>
            <hr />
            <div className="row mlr-30">
              <div className="m-info-table col-sm-12">
                <h4>Docker</h4>
                <table className="table data-table no-border modeldetails-table">
                  <tbody>
                    {spreadTab(jobDetails.spec.deploy[docker_b.key])}
                  </tbody>
                </table>
              </div>
            </div>
            <hr />
            <div className="row mlr-30">
              <div className="m-info-table col-sm-12">
                <h4>Environment Variables</h4>
                <table className="table data-table no-border modeldetails-table">
                  <thead>
                    <tr>
                      <td>Name</td>
                      <td>Value</td>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      jobDetails.spec.deploy && jobDetails.spec.deploy[env_b.key] && jobDetails.spec.deploy[env_b.key].map((obj, i) => {
                        return (<tr key={obj.name + i}>
                          <td>{obj.name}</td>
                          <td>{obj.value}</td>
                        </tr>)
                      })
                    }
                  </tbody>
                </table>
              </div>
            </div>
          </TabPanel>
        </React.Fragment>
      </Tabs>
      {jobDetails.spec.notifications &&
        <div className="mlr-30">
          <hr />
          <div className="m-info-table">
            <h4>Notifications</h4>
            <table className="table data-table no-border modeldetails-table">
              <thead>
                <tr>
                  {spreadTabName(jobDetails.spec.notifications)}
                </tr>
              </thead>
              <tbody>
                <tr>
                  {spreadTabValue(jobDetails.spec.notifications)}
                </tr>
              </tbody>
            </table>
          </div>
        </div>}
    </div>
  )
}

export default JobViewSpl;