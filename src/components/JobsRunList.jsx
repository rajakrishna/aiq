import React, { Component } from "react";
import { connect } from "react-redux";
import Pagination from "./common/pagination";
import { workflowRunList, workflowRunListById, workflowRunDelete } from "../ducks/jobRunDetails";
import MicroModal from "micromodal";
import { Loader1 } from "./home/modelOverviewLoader";
import { bindActionCreators } from "redux";
import { Link } from "react-router-dom";
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { runJobAction } from "../ducks/jobs";
import { alertMsg } from "../ducks/alertsReducer";
import _ from "lodash";
import LeftArrow from "../images/left-chevron.png";
import {startCase} from "lodash";
import FilterJobRun from "./filterJobRun";
import Mixpanel from "mixpanel-browser";
import { parseDepList, sortJobRunByCreatedDate } from "../utils/helper";
import { confirm } from "./Confirm";

class JobsRunList extends Component {
  state = {
    jobsRunList: [],
    allJobRuns: [],
   
    notLoaded: false,
    totalJobRuns: 0,
    currentPage: 0,
    currentPageSize: 10,
    filters: {
      projectId: this.props.selectedProject && this.props.selectedProject.id,
      jobId: "",
      modelId: ""
    },
    appliedFilters: {},
    keyword: ""
  };
  componentDidUpdate(prevProps){
    if(this.props.selectedProject && prevProps.selectedProject.id !== this.props.selectedProject.id){
     
      this.getJobRuns();
     } 
   
  };
  
  static getDerivedStateFromProps(nextProps, prevState) {
  
    if(nextProps.jobRunList && nextProps.jobRunList.length && !prevState.keyword) {
      
       return ({
        allJobRuns: sortJobRunByCreatedDate(nextProps.jobRunList),
        jobsRunList: parseDepList(nextProps.jobRunList, nextProps.jobRunList.length, prevState.currentPage, 10),
        totalJobRuns: nextProps.jobRunList.length,
        currentPageSize: 10,
        notLoaded: nextProps.loader
      })
    }
    else if(!nextProps.jobRunList || !nextProps.jobRunList.length && !prevState.keyword)
    {
      return ({
        allJobRuns: sortJobRunByCreatedDate([]),
        jobsRunList: parseDepList([], [],1, 10),
        totalJobRuns: 0,
        currentPageSize: 10,
        notLoaded: nextProps.loader
      })
    }
   
  }

  componentDidMount() {
    if (!this.props.id) {
      Mixpanel.track('Run History Page');
    }
    MicroModal.init({
      disableScroll: true,
      disableFocus: false,
      awaitCloseAnimation: false,
      debugMode: true
    });
    if(window.sessionStorage.run) {
      window.sessionStorage.removeItem("run");
    }
    if(this.props.history.location.state && this.props.history.location.state.jobId) {
    
      this.setState({
        filters: {
          projectId: this.props.selectedProject.id,
          jobId: this.props.history.location.state.jobId,
          modelId: ""
        },
        appliedFilters: {
          projectId: this.props.selectedProject.id,
          jobId: this.props.history.location.state.jobId,
          modelId: ""
        }
      }, () => {
        this.getJobRuns({...this.state.filters});
      });
    } else 
  
      this.getJobRuns();
  }
  
  
  getJobRuns = () => {
    let params = { ...this.state.filters };
    params['projectId'] = this.props.selectedProject && this.props.selectedProject.id;
    this.setState({
      filters:params
    })
    this.setState({ jobsRunList: []});
    if(this.props.id) {
      this.props.workflowRunListById(this.props.id);
    } else {
      this.props.workflowRunList(params);
      delete params['projectId']
    }
  }

  getColor(type) {
    const status = {
      'CREATED':"info",
      'STARTED':"info",
      'RUNNING':"info",
      'FAILED':"failed",
      'ERRORED':"failed",
      'SUCCEEDED':"success",
      'TERMINATED':"primary"
    }
    const color = status[type] ? status[type] : 'primary';
    return color;
  }

  longDate(timestamp) {
    if(!timestamp)
      return '';

    let dt = new Date(timestamp);
    return dt.toLocaleTimeString('en-us',{year: 'numeric', month: 'long', day: 'numeric' });
  }

  findDuration(startTime, endTime) {
    if(!startTime || !endTime)
      return '';

    let st = new Date(startTime),
        et = new Date(endTime),
        diff = ((et.getTime() - st.getTime()) / 1000);

        let seconds = Math.floor(diff);
        let minutes = Math.floor(seconds / 60);
        let hours = Math.floor(minutes / 60);
      
        seconds = seconds % 60;
        minutes = minutes % 60;
        hours = hours % 24;

        let durationString
        if (hours === 0 && minutes === 0) {
          durationString = `${seconds.toString().padStart(2, '0')} Sec`
        } 
        else if (hours === 0) {
          durationString = `${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')} Min`
        } else {
          durationString = `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')} Hours`
        }

        return durationString;
  }

  getSteps(steps) {
    if(!steps)
      return "";
  }

  runJob() {
    let finalData = this.props.id;
    this.props.runJobAction(this.props.token, finalData);
    setTimeout(()=>{
      this.getJobRuns()
    }, 100);
  }

  applyFilters = filters => {
    filters['delete'] = 1;
    this.setState({ filters, currentPage: 1 },()=>{
      filters['delete'] = null;
      this.setState({
        filters
      })
    });
    this.getJobRuns(_.pickBy(filters, _.identity));
  };
  
  handleRemoveFilter = filter => {
    let updatedFilters = { ...this.state.filters };
    updatedFilters[filter] = null;
    if(filter === "projectId") {
      updatedFilters["jobId"] = null;
      updatedFilters["modelId"] = null;
    }
    this.applyFilters(updatedFilters);
  };

  handleSearch = (keyword) => {
    
    const { jobRunList } = this.props;
    if(!keyword) {
      this.setState({
        jobsRunList: parseDepList(jobRunList, jobRunList.length, 0, 10),
        currentPage: 0,
        totalJobRuns: jobRunList.length,
        keyword: ""
      });
      return;
    }else{
     
      keyword = keyword.toLowerCase();
      const parsedList = jobRunList.filter(e => {
        return (e.metadata.name).includes(keyword);
      });
      console.log("parsedList",parsedList);
      const jobsRunList = parseDepList(parsedList, parsedList.length, 0, 10);
      console.log("jobsRunList",jobsRunList);
      this.setState({
        keyword: keyword,
        jobsRunList:[...jobsRunList],
        currentPage: 0,
        totalJobRuns: parsedList.length
      });
  
    }
    
  }

  getFilterTab = (data) => {
    switch(typeof(data)){
      case "object":
        if(data.value){
          return `${data.key}-${data.value}`
        }
        if(data.key == "isBetween"){
          return `${data.key}-${data.value1}&${data.value2}`
        }
        if(data.value1){
          return `${data.key}-${data.value1}`
        }
      break;
      default:
        return data
    }
  }

  workflowRunDelete = (name) => {
    if(!name) return;
    confirm("Are you sure?", "Delete").then(
      () => {
        this.props.workflowRunDelete(name, () => {this.getJobRuns()});
      }
    )
  }

  handlePageChange = pageAction => {
    let pageNumber = this.state.currentPage;
    const { currentPageSize, allJobRuns } = this.state;
    pageAction === "nextPage" ? (pageNumber += 1) : (pageNumber -= 1);
    this.setState({
      currentPage: pageNumber,
      jobsRunList: parseDepList(allJobRuns, allJobRuns.length, pageNumber, currentPageSize)
    })

  };

  renderJobRunList = () => {
   // const {jobsRunList} = this.state.jobsRunList);
    const { notLoaded,jobsRunList } = this.state;
    return(
      <div className="margin-top-30 mr-2">
        {
          this.props.user && this.props.user.tier === "Free Tier" && jobsRunList.length > 9 ?
          <div className="error-placement alert alert-danger">
              <i className="fa fa-exclamation-triangle" />
              <span>{"Maximum run limit exceeded, please upgrade your tier"}</span>
          </div> : ""
        }
        {notLoaded ?
        <Loader1 /> :
        <Grid container spacing={24} direction="row" justify="flex-start">
          {jobsRunList.length ? jobsRunList.map((jobRun,i)=>{
              return(
              <Grid item xs={12} key={'jobRuns'+i}>
                <Paper>
                <Grid container spacing={0} direction="row" justify="flex-start">
                  <Grid item xs={5} style={{padding: 20}}>
                    <strong>
                        <Link to={`/run-history/${jobRun.metadata.name}`}>
                          {jobRun.metadata.name}
                        </Link>
                    </strong>
                    <div className="message"> 
                        {jobRun.status && jobRun.status && this.getSteps(jobRun.status && jobRun.status.steps)}
                    </div>
                  </Grid>
                  <Grid item xs={3} style={{padding: 20}}>
                        <label
                          className={`model-item-status status-${this.getColor(jobRun.status && jobRun.status.phase)}`}
                        >
                          {jobRun.status && jobRun.status.phase}
                        </label>
                  </Grid>
                  <Grid item xs={4} style={{background:"#F5FBFD",padding: 20}}>
                    <Grid container className="text-gray" spacing={8} direction="row" justify="flex-start">
                        <Grid item xs={4}><small>NAME:</small></Grid>
                        <Grid item xs={7}><small>{jobRun.metadata.name}</small></Grid>
                        <Grid item xs={1}>
                          <button className="btn btn-sm btn-danger"
                            onClick={()=>{this.workflowRunDelete(jobRun.metadata.name)}}
                          >
                            <i className="fa fa-trash" />
                          </button>
                        </Grid>
                        <Grid item xs={4}><small>CREATED AT:</small></Grid>
                        <Grid item xs={8}><small>{this.longDate(jobRun.metadata.creationTimestamp)}</small></Grid>
                        <Grid item xs={4}><small>STARTED AT:</small></Grid>
                        <Grid item xs={8}><small>{this.longDate(jobRun.status && jobRun.status.started_at)}</small></Grid>
                        {
                          jobRun.status && jobRun.status.finished_at ?
                              <Grid item xs={4}><small>FINISHED AT:</small></Grid>
                          : ""
                        }    
                        {
                          jobRun.status && jobRun.status.finished_at ?
                              <Grid item xs={8}><small>{this.longDate(jobRun.status && jobRun.status.finished_at)}</small></Grid>
                          : ""
                        }    
                        {
                              jobRun.status && jobRun.status.finished_at ?
                              <Grid item xs={4}><small>DURATION:</small></Grid>
                          : ""
                        }    
                        {
                          jobRun.status && jobRun.status.finished_at ?
                              <Grid item xs={8}><small>{this.findDuration(jobRun.status && jobRun.status.started_at, jobRun.status && jobRun.status.finished_at)}</small></Grid>
                          : ""
                        }
                    </Grid>
                  </Grid>
                </Grid>
                </Paper>
              </Grid>
              )
            }) : <div className="h180 content-center text-gray no-select" style={{ flexGrow: 1 }}>
                  <div class="empty-items-box">
                    <h4>No run history found!</h4>
                  </div>
                </div>
          }
        </Grid>}
      </div>
    )
  }

  renderFullJobRunList = () => {
    const appliedFilters = _.pickBy(this.state.filters, _.identity);
    const { totalJobRuns, currentPageSize, currentPage } = this.state;
    return (
      <div className="main-container">
        <div className="main-body" style={{overflow: "hidden"}}>
          <div className="content-wraper-sm">
            <div className="m-details-card pt-3">
              <div className="m-details-card-head pl-4 mr-5">    
                {/* <div
                className="back-action"
                onClick={() => {this.state.prevUrl ? this.props.history.push(this.state.prevUrl) : this.props.history.goBack()}}
                >
                  <img src={LeftArrow} alt="left arrow" />
                </div> */}
                <div className="m-title-info fw d-flex flex-row align-items-center justify-content-between">
                  <h4>RUN HISTORY</h4>
                  <div className="card-search inline-block pd-lr margin-right-20rem">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="SEARCH"
                      onChange={event => {
                        this.handleSearch(event.target.value);
                      }}
                    />
                  </div>
                  <button 
                    className="btn bt btn-sm btn-default mr-sm float-right" 
                    onClick={()=>{
                      this.getJobRuns({...this.state.filters})
                    }} 
                    type="button"
                  >
                    <i className="fa fa-sync-alt" />
                  </button>
                </div>
                <div className="m-tech-stack" />
            </div>
            <div className="model-filter-header pd-lg mr-5 p-0 pl-4 pr-3" style={{marginTop: '2rem'}}>
              <div className="filter-btn mt-1">
                <button
                  className="btn btn-primary btn-sm btn-rounded"
                  onClick={()=>{MicroModal.show("modal-1")}}
                >
                  Add Filter +
                </button>
              </div>
              <div style={{marginRight: "auto"}}>
                <ul className="filter-items-list">
                  {!_.isEmpty(appliedFilters) &&
                    Object.keys(appliedFilters).map(key => (
                      <li key={key} className="filter-tag-item" style={{marginBottom: 0}}>
                        {startCase(key)}-{this.getFilterTab(appliedFilters[key])}
                        <i className="fa fa-times" style={{marginTop: 3}} onClick={() => this.handleRemoveFilter(key)}>
                        </i>
                      </li>
                    ))}
                </ul>
              </div>
                {totalJobRuns > 10 && (
                  <div style={{minWidth: 164}} className="model-pagination inline-flex ml-sm mr-sm">
                    <Pagination
                      itemsCount={totalJobRuns}
                      currentPage={currentPage}
                      currentPageSize={currentPageSize}
                      pageSize={10}
                      onPageChange={this.handlePageChange}
                    />
                  </div>
                )}
              </div>
              <div className="pd-lr-lg mr-5">
              {this.renderJobRunList()}
              </div>
            </div>
          </div>
        </div>
        <FilterJobRun
          applyFilters={this.applyFilters}
          filters={this.state.filters}
        />
      </div>
    )
  }

  render() {
    return(
        <div>
          {this.props.id ?
            this.renderJobRunList() :
            this.renderFullJobRunList()
          }
        </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token,
    user: state.users.user,
    jobRunList: state.jobRunDetails.jobRunList,
    loader: state.jobRunDetails.loader,
    selectedProject: state.projects.globalProject,
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      runJobAction,
      alertMsg,
      workflowRunList,
      workflowRunListById,
      workflowRunDelete
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(JobsRunList);
