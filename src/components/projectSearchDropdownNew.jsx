import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import grey from '@material-ui/core/colors/grey';
import {
	getProjectsSummary,
	globalProjectSelection,
	getProjectsList
	} from "../ducks/projects";
 import { withRouter,Link } from "react-router-dom";
 import { connect } from "react-redux";
 import { bindActionCreators } from "redux";
import ProjectModalFullScreen from './modals/projectModalFullScreen';

const styles = theme => ({
  root: {
    position: 'relative',
  },
  paper: {
    position: 'absolute',
    top: 36,
    right: 0,
    left: 0,
    width: 300
  }
});

class ProjectSearchDropdownNew extends React.Component {
  state = {
    open: false,
    projectSearchValue: '',
  };

  handleClick = () => {
    this.setState(state => ({
      open: !state.open,
    }));
  };

  handleClickAway = () => {
    this.setState({
      open: false,
    });
  };

  setValue =(value)=> {
    if(value){
        const projectRequiredFind = this.props.projectsSummary.find((item)=> item.id === value.id)
        this.props.globalProjectSelection(projectRequiredFind);
        this.handleClickAway();
    }
  }

  renderProjectSearchInput = () => {
    return (
      <div class="border border-grey rounded p-1 pl-2">
        <i class="fa fa-search"></i>
        <input 
          value={this.state.projectSearchValue} 
          onChange={(event) => this.setState({projectSearchValue: event.target.value})} 
          type="search" 
          className="outline-none ml-2" 
          placeholder="Search Project" 
          aria-label="Search" 
          aria-describedby="search-addon" />
      </div>
    )
  }

  render() {
    const { classes } = this.props;
    const { open } = this.state;
    const projectOptions = this.props.projectsSummary.map(e => ({"label": e.name,"value":e.name,"id":e.id}));
    const projectOptionsSearch = projectOptions.filter(item => item.value.includes(this.state.projectSearchValue));

    return (
      <div className={classes.root}>
        <ClickAwayListener onClickAway={this.handleClickAway}>
          <div>
            <Button
                  style={{
                    border: "none",
                    outline: "none",
                    backgroundColor:
                    this.props.location.pathname === "/" ||
                    this.props.location.pathname === "/projects"
                    ? "#e6eefc"
                    : null,
                  }}
                  color={
                    this.props.location.pathname === "/" ||
                    this.props.location.pathname === "/projects"
                    ? "primary"
                    : null
                  }
                  onClick={this.handleClick}
                  className="mr-2"
                >
                  Projects
                </Button>
            {open ? (
              <Paper className={classes.paper}>
                <div className='m-3'>
                </div>
                <div className="m-3">
                    {projectOptions.length > 0 ? 
                      <div>
                        {this.renderProjectSearchInput()}
                        <div className='mt-4'>
                          <h6 className='font-weight-bold'>Recently visited projects</h6>
                        </div>
                        {projectOptions.filter(item => item.value.includes(this.state.projectSearchValue)).slice(0, 5).map(eachItem => 
                        <div className='mt-3 mb-3'>
                          <Link className="link no-decoration mt-5" 
                                style={{marginTop: '20px'}} 
                                to="/project_details" 
                                onClick={() => this.setValue(eachItem)}
                                >
                                  {eachItem.value}
                          </Link>
                        </div>)}
                        <Link className="link no-decoration btn btn-light w-100 mt-0 mb-0" to="/projects" onClick={this.handleClickAway}>View all projects</Link>
                      </div>
                      :
                      <div>
                        You don't have projects. Click{' '}
                        <Link to="" className="mt-5" style={{marginTop: '20px'}}
>                           <ProjectModalFullScreen />
                        </Link>
                        {' '}To create one.
                      </div>
                    }
                </div>
              </Paper>
            ) : null}
          </div>
        </ClickAwayListener>
      </div>
    );
  }
}

ProjectSearchDropdownNew.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
    return {
      projects: state.projects.projects,
      projectsSummary: state.projects.summary,
      selectedProject: state.projects.globalProject,
    };
  };
  
  const mapDispatchToProps = (dispatch) => {
    return {
      ...bindActionCreators(
        {
          globalProjectSelection,
          getProjectsSummary,
          getProjectsList
        },
        dispatch
      ),
    };
  };


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ProjectSearchDropdownNew)));