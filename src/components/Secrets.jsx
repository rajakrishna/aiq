import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  getAllSecrets,
  createSecrets,
  updateSecrets,
  deleteSecrets,
} from "../api";
import MicroModal from "micromodal";
import { alertMsg } from "../ducks/alertsReducer";
import { Loader1 } from "./home/modelOverviewLoader";
import moment from "moment";
import { globalProjectSelection } from "../ducks/projects";
import { confirm } from "./Confirm";
import SecretModalFullScreen from "./modals/SecretModalFullScreen";


class Secrets extends Component {
  _clicked = false;
  _isMounted = false;
  state = {
    secretData: [],
    name: "",
    secretObj: [{ name: "", value: "" }],
    loading: "",
  };

  componentDidMount() {
    this._isMounted = true;
    this.getSecrets();
  }

  componentDidUpdate(prevState) {
    if (prevState.selectedProject.id !== this.props.selectedProject.id) {
      this.getSecrets();
    }
  }
  componentWillUnmount() {
    this._isMounted = false;
  }

  addInp = () => {
    let arr = [...this.state.secretObj];
    arr.push({ name: "", value: "" });
    this.setState({ secretObj: arr });
  };

  delInp = (i) => {
    let arr = [...this.state.secretObj];
    if (arr.length > 1) {
      arr.splice(i, 1);
      this.setState({ secretObj: arr });
    }
  };

  getSecrets = () => {
    
    getAllSecrets(this.props.token, {
      project_name: this.props.project.name,
      project_id: this.props.project.id,
    })
      .then((res) => {
        if (res.status === 200) {
          this._isMounted &&
            this.setState({
              secretData: res.data,
              loading: null,
            });
        } else {
          this._isMounted &&
            this.setState({
              loading: null,
            });
        }
        this._clicked = false;
      })
      .catch((err) => {
        this._isMounted &&
          this.setState({
            loading: null,
          });
        this._clicked = false;
      });
  };

  delSecrets = (name) => {
    if (this._clicked) {
      return false;
    }
    this.props.alertMsg("Deleting...", "info");
    this._clicked = true;
    deleteSecrets(this.props.token, name)
      .then((res) => {
        if (res.status === 200) {
          this.props.alertMsg("Deleted!", "success");
          this.getSecrets();
        } else {
          this.props.alertMsg("Some error occured, please try again!", "error");
          this._clicked = false;
        }
      })
      .catch((err) => {
        this.props.alertMsg("Some error occured, please try again!", "error");
        this._clicked = false;
      });
  };

  addSecret = () => {
    if (this._clicked) {
      return false;
    }
    this._clicked = true;
    this.props.alertMsg("Saving...", "info");
    const data = {
      data: this.getObj(this.state.secretObj),
      name: this.state.name,
      project_id: this.props.project.id,
      project_name: this.props.project.name,
    };
    createSecrets(this.props.token, data)
      .then((res) => {
        this.props.alertMsg("Saving...", "info", false);
        if (res.status === 201) {
          this.props.alertMsg("Secret saved successfully!", "success");
          this.setState(
            {
              name: "",
              secretObj: [{ name: "", value: "" }],
            },
            MicroModal.close("secret-modal")
          );
          this.getSecrets();
        } else {
          this.props.alertMsg(
            "Some error occurred, please try again!",
            "error"
          );
        }
        this._clicked = false;
      })
      .catch((err) => {
        this.props.alertMsg("Saving...", "info", false);
        this.props.alertMsg(err.response.data.title, "error");
        this._clicked = false;
      });
  };

  editSecret = () => {
    if (this._clicked) {
      return false;
    }
    this._clicked = true;
    this.props.alertMsg("Saving...", "info");
    const data = {
      data: this.getObj(this.state.secretObj),
      name: this.state.name,
      project_id: this.props.project.id,
      project_name: this.props.project.name,
    };
    updateSecrets(this.props.token, data)
      .then((res) => {
        this.props.alertMsg("Saving...", "info", false);
        if (res.status === 201 || res.status === 200) {
          this.props.alertMsg("Secret saved successfully!", "success");
          this.getSecrets();
        } else {
          this.props.alertMsg(
            "Some error occurred, please try again!",
            "error"
          );
        }
        this._clicked = false;
      })
      .catch((err) => {
        this.props.alertMsg("Saving...", "info", false);
        this.props.alertMsg(err.response.data.title, "error");
        this._clicked = false;
      });
  };

  handleEdit = (secret) => {
    this.setState(
      {
        secretObj: this.getArr(secret.data),
        name: secret.name,
      },
      () => MicroModal.show("edit-modal")
    );
  };

  getObj = (arr) => {
    const newArr = arr.map((e, i) => {
      if (e.name && e.value) return `"${e.name}":"${e.value}"`;
      else {
        return null;
      }
    });

    const filterArr = newArr.filter((e) => {
      return e != null;
    });

    let obj = {};
    try {
      obj = JSON.parse("{" + filterArr.join(",") + "}");
    } catch (e) {
      this.setState({ valid: false });
    }
    return obj;
  };

  getArr = (obj) => {
    let arr = Object.keys(obj).map((key, i) => {
      return { name: key, value: obj[key] };
    });
    return arr;
  };

  handleObj = (value, type, index) => {
    let secretObj = [...this.state.secretObj];
    secretObj[index][type] = value;
    this.setState({
      secretObj,
    });
  };

  render() {
    const { project } = this.props;
    const { loading } = this.state;
    return (
      <div className="project-team-wraper pl-0 ml-0">

        <div className="project-team-header clearfix">
          <h4>Secrets</h4>
          <SecretModalFullScreen getSecrets={this.getSecrets} selectedProject={this.props.selectedProject} secretModelParent="secretsTab"/>
        </div>
        {this.state.secretData && this.state.secretData.length ? (
          <div className="m-info-table model-details pl-0 pr-0">
            <table className="table data-table no-border">
              <thead>
                <tr>
                  <td className="pl-0">Name</td>
                  <td>Created Date</td>
                  <td>Keys</td>
                  <td className="float-right pr-0">Action</td>
                </tr>
              </thead>
              <tbody>
                {this.state.secretData.map((e, i) => {
                  if (
                    e.project_name === project.name &&
                    e.project_id === project.id
                  )
                    return (
                      <tr key={"row" + i}>
                        <td className="pl-0">{e.name}</td>
                        <td>{moment(e.created_date).format("LL")}</td>
                        <td>
                          {Object.keys(e.data).map((elem, i) => {
                            return (
                              <label
                                className="model-item-status mr-sm"
                                key={elem}
                              >
                                {elem}
                              </label>
                            );
                          })}
                        </td>
                        <td className="float-right pr-0">
                          <button
                            className="btn btn-sm btn-default mr-sm"
                            type="button"
                            onClick={(event) => {
                              this.handleEdit(e);
                            }}
                          >
                            <i className="fa fa-edit"></i>
                          </button>
                          <button
                            className="btn btn-sm btn-danger"
                            type="button"
                            onClick={(elem) => {
                              confirm("Are you sure?", "Delete Secret").then(
                                () => {
                                  this.delSecrets(e.name);
                                },
                                () => {
                                  console.log("Cancel!");
                                }
                              );
                            }}
                           
                          >
                            <i className="fa fa-trash"></i>
                          </button>
                        </td>
                      </tr>
                    );
                })}
              </tbody>
            </table>
          </div>
        ) : loading ? (
          loading
        ) : (
          <div className="m-info-table mlr-30">
            <div className="empty-items-box">
              <h4>No secrets found!</h4>
            </div>
          </div>
        )}
        <div
          className="modal micromodal-slide"
          id="edit-modal"
          aria-hidden="true"
        >
          <div className="modal__overlay" tabIndex="-1" data-micromodal-close>
            <div
              className="modal__container"
              role="dialog"
              aria-modal="true"
              aria-labelledby="secret-modal-title"
            >
              <header className="modal__header">
                <h2 className="modal__title">Edit Secrets</h2>
                <button
                  type="button"
                  className="modal__close"
                  aria-label="Close modal"
                  data-micromodal-close
                />
              </header>
              <main className="modal__content">
                <div className="modal-filter-form">
                  <div className="modal-body">
                    <div className="form-group">
                      <label htmlFor="#secret_name">Secret Name: </label>
                      <input
                        className="form-control mt"
                        defaultValue={this.state.name}
                        readOnly
                      />
                    </div>
                    <div className="form-group">
                      <label>Data: </label>
                      <button
                        type="button"
                        className="btn btn-default btn-sm float-right"
                        onClick={this.addInp}
                      >
                        <i className="fa fa-plus"></i>
                      </button>
                      {this.state.secretObj.map((e, i) => {
                        return (
                          <div className="row mt" key={"secret" + i}>
                            <div className="col-sm-5">
                              <input
                                id={"editname" + i}
                                type="text"
                                placeholder="Key"
                                onChange={(e) => {
                                  this.handleObj(e.target.value, "name", i);
                                }}
                                className="form-control"
                                value={e.name}
                              />
                            </div>
                            <div className="col-sm-5 relative">
                              <input
                                id={"editvalue" + i}
                                style={{ paddingRight: 30 }}
                                type="password"
                                placeholder="Value"
                                onChange={(e) => {
                                  this.handleObj(e.target.value, "value", i);
                                }}
                                className="form-control"
                                value={e.value}
                              />
                              <i
                                className="fa fa-eye eye clickable"
                                id={"editEye" + i}
                                onMouseDown={(elem) => {
                                  document.querySelector(
                                    "#editvalue" + i
                                  ).type = "text";
                                  document.querySelector(
                                    "#editEye" + i
                                  ).className += " text-link";
                                }}
                                onMouseUp={() => {
                                  document.querySelector(
                                    "#editvalue" + i
                                  ).type = "password";
                                  document.querySelector(
                                    "#editEye" + i
                                  ).className = document
                                    .querySelector("#editEye" + i)
                                    .className.replace("text-link", "");
                                }}
                                onMouseOut={() => {
                                  document.querySelector(
                                    "#editvalue" + i
                                  ).type = "password";
                                  document.querySelector(
                                    "#editEye" + i
                                  ).className = document
                                    .querySelector("#editEye" + i)
                                    .className.replace("text-link", "");
                                }}
                              />
                            </div>
                            <div className="col-sm-2">
                              <button
                                type="button"
                                className="btn btn-sm btn-danger float-right mt"
                                onClick={() => {
                                  this.delInp(i);
                                }}
                              >
                                <i className="fa fa-trash"></i>
                              </button>
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                  <div className="modal-footer">
                    <button
                      onClick={() => {MicroModal.close("edit-modal"); this.getSecrets();}}
                      type="button"
                      className="btn btn-secondary"
                    >
                      Close
                    </button>
                    <button
                      type="button"
                      className="btn btn-primary"
                      onClick={() => {
                        this.editSecret();
                        MicroModal.close("edit-modal");
                        this.getSecrets();
                      }}
                    >
                      Save
                    </button>
                  </div>
                </div>
              </main>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    projectDetails: state.projectDetails.projectDetails,
    globalProject: state.projects.globalProject,
    selectedProject: state.projects.globalProject  
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      alertMsg,
      globalProjectSelection,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Secrets);
