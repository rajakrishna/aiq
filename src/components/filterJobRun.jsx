import React, { Component } from "react";
// import InputRange from "react-input-range";
import "react-input-range/lib/css/index.css";
import { getAllProjects, getAllModels, getAllJobs } from "../api";
import { connect } from "react-redux";
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';
class FilterModal extends Component {
  constructor(props) {
    super(props);
    this._isMounted = false;
    this.state = { 
      filters: props.filters,
      filterType: props.filterType,
      projectList: [],
      modelList: [],
      jobList: [],
      showLoader: false,
      jobLoad: false,
      modelLoad: false
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if(nextProps.filters.delete) {
        prevState.filters = {};
    }
    const filters = {...nextProps.filters, ...prevState.filters};
    const filterType = nextProps.filterType;
    return {
        filters,
        filterType
    }
  }
  componentDidUpdate(prevProps){
    if(this.props.selectedProject && prevProps.selectedProject.id !== this.props.selectedProject.id){
     
      this.getData({});
     } 
   
  };
  componentDidMount() {
    this._isMounted = true;
    this.getData({});
  }

  getData = (params = {}) => {
    this.setState({modelLoad: true, jobLoad: true});

    getAllModels(this.props.token, params)
    .then(response => {
      let data = response.data;
        let modelList = [];
        data.map((model) => {
          if(model.project_id === this.props.selectedProject.id)
          return modelList.push({
            label: model.name+" v"+model.version,
            value: model.id
          });
        });
        if(this._isMounted)
          this.setState({ modelList, modelLoad:false });
    })
    .catch(error => {
      this.setState({modelLoad: false});
    });

    getAllJobs(this.props.token, params)
    .then(response => {
      let data = response.data;
      if (response.status) {
        let jobList = [];
        data.map(job => {
          if(job.project_id === this.props.selectedProject.id)
          return jobList.push({
            label: job.name,
            value: job.id
          });
        });
        if(this._isMounted)
          this.setState({ jobList, jobLoad:false });
      }
    })
    .catch(error => this.setState({jobLoad: false}));
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  handleProjectChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    if(name === "projectId" && value) {
      this.getData({projectIDContains: value});
    }
    if(name === "projectId" && !value) {
      this.setState({
        modelList: [],
        jobList: [],
        jobId: "",
        modelId: ""
      })
    }
    let filters = { ...this.state.filters };
    filters[name] = value;
    this.setState({filters: filters })
  } 
  handleApplyFilters = () => {
    this.props.applyFilters(this.state.filters);
  };
  handleResetFilters = () => {
    this.setState({ filters: {} });
    this.props.applyFilters({});
  };

  render() {
    const { modelList, projectList, jobList, jobLoad, modelLoad } = this.state;
    const { projectId, modelId, jobId } = this.state.filters;
    return (
      <div className="modal micromodal-slide" id="modal-1" aria-hidden="true">
        <div className="modal__overlay" tabIndex="-1" data-micromodal-close>
          <div
            className="modal__container"
            role="dialog"
            aria-modal="true"
            aria-labelledby="modal-1-title"
            style={{minHeight: 400}}
          >
            <header className="modal__header">
              <h2 className="modal__title" id="modal-1-title">
                Filter Jobs
              </h2>
              <button
                className="modal__close"
                aria-label="Close modal"
                data-micromodal-close
              />
            </header>
            <main className="modal__content mb-lg" id="modal-1-content">
              <div className="modal-filter-form row">
               
                <div className="form-group col-sm-11">
                    <TextField
                        label="Models"
                        select
                        fullWidth
                        onChange={this.handleProjectChange}
                        placeholder="Select Model"
                        name="modelId"
                        value={modelId || ""}
                    >
                      <MenuItem value="">Select Model</MenuItem>
                      {modelList.map((e,i)=>{
                        return (
                          <MenuItem key={e.value} value={e.value}>{e.label}</MenuItem>
                        )
                      })}
                    </TextField>
                </div>
                <div className="col-sm-1" style={{padding: "25px 0 0 0"}}>
                  {modelLoad && <CircularProgress size={20} disableShrink />}
                </div>
                <div className="form-group col-sm-11">
                    <TextField
                        label="Jobs"
                        select
                        fullWidth
                        onChange={this.handleProjectChange}
                        placeholder="Select Job"
                        name="jobId"
                        value={jobId || ""}
                    >
                      <MenuItem value="">Select Job</MenuItem>
                      {jobList.map((e,i)=>{
                        return (
                          <MenuItem key={e.value} value={e.value}>{e.label}</MenuItem>
                        )
                      })}
                    </TextField>
                </div>
                <div className="col-sm-1" style={{padding: "25px 0 0 0"}}>
                  {jobLoad && <CircularProgress size={20} disableShrink />}
                </div>
              </div>
            </main>
            <footer className="modal__footer filter-form-actions">
              <button
                className="btn btn-primary btn-rounded"
                data-micromodal-close
                onClick={this.handleApplyFilters}
              >
                APPLY FILTERS
              </button>
              <button
                className="btn btn-default btn-rounded"
                data-micromodal-close
                aria-label="Close this dialog window"
                onClick={this.handleResetFilters}
              >
                RESET FILTERS
              </button>
            </footer>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token,
    selectedProject: state.projects.globalProject,
  };
};

export default connect(mapStateToProps)(FilterModal);
