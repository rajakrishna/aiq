import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { changeJobStore, resetJobStore } from "../../ducks/jobStore";
import LeftArrow from "../../images/left-chevron.png";
import DeploySimpleForm from "../deployments/DeploymnetSimpleForm";
import { jobProto } from "../Jobs/jobProto";

class DeploymentForm extends Component {
  componentWillMount() {
    this.props.resetJobStore();
    const jobStore = { ...jobProto };
    jobStore.spec.project_id = this.props.projectID;
    this.props.changeJobStore(jobStore);
  }

  componentWillUnmount() {
    this.props.resetJobStore();
  }

  render() {
    return (
      <div className="main-container">
        <div className="content-wraper-sm wd-half">
          <div className="m-details-card pl-2">
            <div className="m-details-card-head pl-3 mr-5">
              {/* <div className="back-action" onClick={()=>{this.props.history.push("/deployments")}}>
                <img src={LeftArrow} alt="left arrow" />
              </div> */}
              <div className="m-title-info">
                <h4 className="bold-heading">Create Deployment</h4>
              </div>
              <div className="m-tech-stack"></div>
            </div>
            <div className="card-body h-min-370 pl-3 pt-0">
              <div>
                <DeploySimpleForm
                  key="deploy"
                  projectID={this.props.projectID}
                  spec={{ sub_type: "DEPLOY_ARTIFACT" }}
                  onCancel={()=>{this.props.history.push("/deployments")}}
                  projectName={this.props.projectName}
                />
              </div>
              {/* <div className="mt-lg">
            <button
              type="button"
              className="btn btn-primary mr-sm"
              name="create"
            >
              Create
            </button>
            <button
              type="button"
              className="btn btn-default mr-sm"
              onClick={this.props.onCancel}
              name="cancel"
            >
              Cancel
            </button>
          </div> */}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    jobStore: state.jobStore.jobStore,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      resetJobStore,
      changeJobStore,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(DeploymentForm);
