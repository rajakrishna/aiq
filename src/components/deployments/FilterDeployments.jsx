import React, { Component } from "react";
// import InputRange from "react-input-range";
import "react-input-range/lib/css/index.css";
import { connect } from "react-redux";
import Select from 'react-select';
import { getAllRegisteredModels } from "../../api";
import 'react-select/dist/react-select.css';
import { bindActionCreators } from "redux";
import { globalProjectSelection } from "../../ducks/projects";
class FilterDeployment extends Component {
  constructor(props) {
    super(props);
    this._isMounted = false;
    this.state = { 
      filters: props.filters,
      filterType: props.filterType,
      modelNameList: []
    };
  }
  UNSAFE_componentWillReceiveProps({filters}) {
    this.setState({ filters });
  }

  componentDidMount() {
    this._isMounted = true;
    this.getModels(this.props.selectedProject && this.props.selectedProject.id);
    let filters = { ...this.state.filters };
    delete filters['modelID'];
    filters['projectID'] = this.props.selectedProject && this.props.selectedProject.id;
    this.setState({filters: filters });
}

  componentDidUpdate = (prevProps) => {
    if (this.props.selectedProject && this.props.selectedProject.id !== prevProps.selectedProject.id) {
      this.getModels(this.props.selectedProject.id);
      let filters = { ...this.state.filters };
      delete filters['modelID'];
      filters['projectID'] = this.props.selectedProject.id;
      this.setState({filters: filters });
    }
  }

  getModels = (projectID) => {
    let allParams = {projectID: projectID}
    getAllRegisteredModels(this.props.token, allParams)
    .then(response => {
      if (response.status && this._isMounted) {
        let data = response.data;
        if (response.status) {
          let modelNameList = [];
          modelNameList = data.map(model => {
            return ({
              label: `${model.name} v${model.latest_version}`,
              value: model.id
            });
          });
          if(this._isMounted)
            this.setState({ modelNameList });
        }
      }
    })
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  handleModelChange = e => {
    let filters = { ...this.state.filters };
    filters['modelID'] = e;
    this.setState({filters: filters })
  }

  handleApplyFilters = () => {
    this.props.applyFilters(this.state.filters);
  };

  handleResetFilters = () => {
    this.setState({ filters: {} });
    this.props.applyFilters({});
  };

  render() {
    const { modelNameList } = this.state;
    const { noModel } = this.props;
    let {
      projectID,
      modelID
    } = this.state.filters;
    return (
      <div className="modal micromodal-slide" id="modal-deploy" aria-hidden="true">
        <div className="modal__overlay" tabIndex="-1" data-micromodal-close>
          <div
            className="modal__container"
            role="dialog"
            aria-modal="true"
            aria-labelledby="modal-deploy-title"
            style={{minHeight: "400px"}}
          >
            <header className="modal__header">
              <h2 className="modal__title" id="modal-deploy-title">
                Filter Models
              </h2>
              <button
                className="modal__close"
                aria-label="Close modal"
                data-micromodal-close
              />
            </header>
            <main className="modal__content mb-lg" id="modal-deploy-content">
              <div className="modal-filter-form">
                {!noModel && modelNameList &&
                  <div className="form-group row">
                    <label htmlFor="" className="col-md-3">Model</label>
                    <Select
                      closeOnSelect={true}
                      className= "select-field col-md-8"
                      onChange={this.handleModelChange}
                      options={modelNameList}
                      placeholder="Select Model"
                      removeSelected={true}
                      simpleValue
                      value={modelID}
                    />
                  </div>
                }
              </div>
            </main>
            <footer className="modal__footer filter-form-actions">
              <button
                className="btn btn-primary btn-rounded"
                data-micromodal-close
                onClick={this.handleApplyFilters}
              >
                APPLY FILTERS
              </button>
              <button
                className="btn btn-default btn-rounded"
                data-micromodal-close
                aria-label="Close this dialog window"
                onClick={this.handleResetFilters}
              >
                RESET FILTERS
              </button>
            </footer>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token,
    selectedProject: state.projects.globalProject
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      globalProjectSelection
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(FilterDeployment);
