import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { changeJobStore, errorStore, setStepType } from "../../ducks/jobStore";
import {
  createDeploymentAction,
  getDeployments,
} from "../../ducks/deployments";
import { Tooltip, TextField, MenuItem } from "@material-ui/core";

import { createDeployment, getAllSecrets } from "../../api";

import ReactSelect from "react-select";
import { globalProjectSelection } from "../../ducks/projects";

class DeploySimpleForm extends Component {
  state = {
    formError: false,
    form: {
      name: "",
      implementation: "SKLEARN_SERVER",
      replicas: 1,
      modelUri: "",
      projectId: this.props.projectID,
      projectName: this.props.projectName,
      secretRefName: "",
    },
    formErrors: {
      name: null,
      implementation: null,
      replicas: null,
      modelUri: null,
    },
    serverList: [
      { value: "SKLEARN_SERVER", label: "scikit-learn" },
      { value: "XGBOOST_SERVER", label: "XGBoost" },
      { value: "TENSORFLOW_SERVER", label: "TensorFlow" },
      { value: "MLFLOW_SERVER", label: "MLflow" },
      { value: "H2O_PY_SERVER", label: "H2O Python" },
      { value: "H2O_JAVA_SERVER", label: "H2O Java" },
    ],
    secrets: [],
  };
  componentDidMount() {
    let { form } = this.state;
    form["projectId"] = this.props.selectedProject && this.props.selectedProject.id;
    form["projectName"] = this.props.selectedProject && this.props.selectedProject.name;
    this.getSecrets();
  }

  componentDidUpdate = (prevProps) => {
    if (this.props.selectedProject && prevProps.selectedProject.id !== this.props.selectedProject.id) {
      this.getSecrets();
      let { form } = this.state;
      form["projectId"] = this.props.selectedProject.id;
      form["projectName"] = this.props.selectedProject.name;
      form["secretRefName"] = "";
      this.setState({ form }, () => {});
    }
  };

  getSecrets = () => {
    getAllSecrets(this.props.token, {
      project_name: this.props.selectedProject.name,
      project_id: this.props.selectedProject.id,
    })
      .then((res) => {
        if (res.status === 200) {
          this.setState({
            secrets: res.data,
          });
        }
      })
      .catch((err) => {});
  };

  handleChange = (e) => {
    const { name, value } = e.target;
    const { form, formErrors } = this.state;
    let formObj = {};

    formObj = {
      ...form,
      [name]: value,
    };

    this.setState({ form: formObj }, () => {
      if (!Object.keys(formErrors).includes(name)) return;
      let formErrorsObj = {};
      const errorMsg = this.validateField(name, value);
      formErrorsObj = { ...formErrors, [name]: errorMsg };
      this.setState({ formErrors: formErrorsObj });
    });
  };

  static getDerivedStateFromProps(nextProps) {
    return {
      error: nextProps.jobError &&
        nextProps.jobError.spec && { ...nextProps.jobError.spec.deploy },
    };
  }

  validateField = (name, value) => {
    let errorMsg = null;
    switch (name) {
      case "name":
        if (!value) errorMsg = "Please enter Name.";
        else if (!/^[a-z0-9\\-]{4,50}$/g.test(value))
          errorMsg =
            "Name must start with a lowercase letter followed by up to 49 lowercase letters, numbers, or hyphens, and cannot end with a hyphen";
        break;
      case "implementation":
        if (!value) errorMsg = "Please Select Implementation.";

        break;
      case "replicas":
        if (!value) errorMsg = "Please enter Replicas.";
        else if (parseInt(value) === 0)
          errorMsg = "Replicas should not be a zero";
        break;

      case "modelUri":
        if (!value) errorMsg = "Please enter Model URI.";
        break;

      default:
        break;
    }
    return errorMsg;
  };
  validateForm = (form, formErrors, validateFunc) => {
    const errorObj = {};
    Object.keys(formErrors).map((x) => {
      let refValue = null;

      const msg = validateFunc(x, form[x], refValue);
      if (msg) errorObj[x] = msg;
    });
    return errorObj;
  };

  handleSubmit = () => {
    const { form, formErrors } = this.state;
    const errorObj = this.validateForm(form, formErrors, this.validateField);
    if (Object.keys(errorObj).length !== 0) {
      this.setState({ formErrors: { ...formErrors, ...errorObj } });
      return false;
    }

    createDeployment(this.props.token, form)
      .then((res) => {
        this.props.getDeployments({ projectId: this.props.selectedProject.id });

        this.props.onCancel();
      })
      .catch((error) => {
        this.setState({
          formError: true,
        });
      });
  };

  render() {
    const { form, formErrors } = this.state;

    return (
      <div>
        <div className="signup-box">
          <div className="row">
            <div className="col-md-12">
              <div className="form-group">
                <TextField
                  id={"name"}
                  label={"Name"}
                  name={"name"}
                  type={"text"}
                  placeholder={"Enter name"}
                  value={this.state.form.name}
                  onChange={this.handleChange}
                  onBlur={this.handleChange}
                  margin="normal"
                  fullWidth
                />
                {formErrors.name && (
                  <span className="err">{formErrors.name}</span>
                )}
              </div>
            </div>
            <div className="col-md-12">
              <div className="form-group">
                <TextField
                  id={"implementation"}
                  label={"Implementation"}
                  name={"implementation"}
                  select
                  value={form.implementation}
                  onChange={this.handleChange}
                  margin="normal"
                  fullWidth
                >
                  {this.state.serverList.map((e, i) => {
                    return (
                      <MenuItem
                        key={"implementationSelect" + i}
                        value={e.value}
                      >
                        {e.label}
                      </MenuItem>
                    );
                  })}
                </TextField>
              </div>
            </div>
            <div className="col-md-12">
              <div className="form-group">
                <TextField
                  id={"modelUri"}
                  label={"ModelUri"}
                  name={"modelUri"}
                  type={"text"}
                  placeholder={"Enter ModelUri"}
                  value={this.state.form.modelUri}
                  onChange={this.handleChange}
                  onBlur={this.handleChange}
                  margin="normal"
                  fullWidth
                />
                {formErrors.modelUri && (
                  <span className="err">{formErrors.modelUri}</span>
                )}
              </div>
            </div>
            <div className="col-md-12">
              <div className="form-group">
                <TextField
                  id={"secretRefName"}
                  label={"Secret"}
                  name={"secretRefName"}
                  select
                  value={form.secretRefName}
                  onChange={this.handleChange}
                  margin="normal"
                  fullWidth
                >
                  {this.state.secrets.map((e, i) => {
                    return (
                      <MenuItem key={"secretSelect" + i} value={e.name}>
                        {e.name}
                      </MenuItem>
                    );
                  })}
                </TextField>
              </div>
            </div>
            <div className="col-md-12">
              <div className="form-group">
                <TextField
                  id={"replicas"}
                  label={"Replicas"}
                  name={"replicas"}
                  type={"number"}
                  min={"1"}
                  value={this.state.form.replicas}
                  onChange={this.handleChange}
                  onBlur={this.handleChange}
                  margin="normal"
                  fullWidth
                />
                {formErrors.replicas && (
                  <span className="err">{formErrors.replicas}</span>
                )}
              </div>
            </div>
          </div>
          {this.state.formError ? (
            <div className="row mb-lg">
              <div className="col-sm-12">
                <Tooltip title="Double click to dismiss">
                  <div
                    className="alert alert-danger f-13"
                    onDoubleClick={() => {
                      this.setState({
                        formError: false,
                      });
                    }}
                  >
                    <strong>Error: </strong>
                    An error occurred while creating the Deployment, please try
                    again.
                  </div>
                </Tooltip>
              </div>
            </div>
          ) : (
            <div></div>
          )}

          <div className="form-group">
            <input
              type="button"
              className="btn btn-primary "
              value="Submit"
              onClick={this.handleSubmit}
            />

            <input
              type="button"
              className="btn btn-secondary ml-2"
              value="Cancel"
              onClick={this.props.onCancel}
            />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    jobStore: state.jobStore.jobStore,
    stepTypes: state.jobStore.stepTypes,
    jobError: state.jobStore.jobError,
    token: state.auth.token,
    selectedProject: state.projects.globalProject,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      changeJobStore,
      errorStore,
      setStepType,
      createDeploymentAction,
      getDeployments,
      globalProjectSelection,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(DeploySimpleForm);
