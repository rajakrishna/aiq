import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Tooltip, TextField, MenuItem, Chip } from "@material-ui/core";
import Divider from "@material-ui/core/Divider";
import Dropzone from "react-dropzone";

import {
  getAllSecrets,
  submitBatchScore,
  uploadDatasets,
  getBatchScoringRuns,
} from "./../api";
import { alertMsg } from "./../ducks/alertsReducer";
import {getBatchScoreRuns} from "./../ducks/deployments";

class BatchScoringFormComponent extends Component {
  state = {
    importFileType: "FILE",
    secrets: [],
    formError: false,
    form: {
      outputBucket: "",
      outputKey: "",
      outputAwsSecret: "",
      inputBucket: "",
      inputKey: "",
      inputAwsSecret: "",
    },
    formErrors: {
      outputBucket: null,
      outputKey: null,
      outputAwsSecret: null,
      inputBucket: null,
      inputKey: null,
      inputAwsSecret: null,
    },
    formError: false,
    fileError: false
  };

  componentDidMount() {
    this.getSecrets();
    this.loadBatchScoreRuns();
  }

  getSecrets = () => {
    const { deployment } = this.props;
    getAllSecrets(this.props.token, {
      project_name: deployment.metadata.labels["aiops.predera.com/projectName"],
      project_id: deployment.metadata.labels["aiops.predera.com/projectId"],
    })
      .then((res) => {
        if (res.status === 200) {
          this.setState({
            secrets: res.data,
          });
        }
      })
      .catch((err) => {});
  };

  validateImport = () => {
    const {
      importFileType,
      experiment,
      key,
      bucket,
      file_id,
      filename,
      hostname,
    } = this.state;
    if (
      (key && bucket && importFileType === "S3") ||
      ((file_id || filename) && importFileType === "FILE")
    ) {
      this.setState({
        validatedImport: true,
      });
    } else {
      this.setState({
        validatedImport: false,
      });
    }
  };

  handleTabChange = (e) => {
    const target = e.target;
    const name = target.name;
    const value = /NOT_SET/gi.test(target.value) ? "" : target.value;
    this.setState({
      [name]: value,
    });
  };

  handleChange = (e) => {
    const { name, value } = e.target;
    const { form, formErrors } = this.state;
    let formObj = {};

    formObj = {
      ...form,
      [name]: value,
    };

    this.setState({ form: formObj }, () => {
      if (!Object.keys(formErrors).includes(name)) return;
      let formErrorsObj = {};
      const errorMsg = this.validateField(name, value);
      formErrorsObj = { ...formErrors, [name]: errorMsg };
      this.setState({ formErrors: formErrorsObj });
    });
  };

  validateField = (name, value) => {
    let errorMsg = null;
    switch (name) {
      case "outputBucket":
        if (!value) errorMsg = "Please Enter Bucket. ";

        break;
      case "outputKey":
        if (!value) errorMsg = "Please Enter Key.";
        break;

      case "inputBucket":
        if (!value) errorMsg = "Please Enter Bucket. ";

        break;
      case "inputKey":
        if (!value) errorMsg = "Please Enter Key.";
        break;

      default:
        break;
    }
    return errorMsg;
  };

  validateForm = (form, formErrors, validateFunc) => {
    const errorObj = {};
    Object.keys(formErrors).map((x) => {
      let refValue = null;

      const msg = validateFunc(x, form[x], refValue);
      if (msg) errorObj[x] = msg;
    });
    return errorObj;
  };

  handlePredictionSubmit = () => {
    const { importFileType, file } = this.state;
    if (importFileType === "FILE") {
      file ? this.upload() : this.setState({fileError: true});
    } else if (importFileType === "S3") {
      this.handleSubmit();
    }
  };

  upload = () => {
    const { file } = this.state;
    const { deployment } = this.props;

    const name = file.name;
    let extension = "";
    if (name.includes(".")) {
      extension = name.split(".");
      extension = "." + extension[extension.length - 1];
    }

    const data = {
      file,
      deploymentName: deployment.metadata.name,
    };
    this.setState(
      {
        filename: name,
        uploading: true,
      },
      () => {
        uploadDatasets(this.props.token, data)
          .then((res) => {
            this.props.alertMsg("File uploaded successfully", "success");
            this.loadBatchScoreRuns();
            this.setState({
              uploading: false,
              file_id: data.id,
              fileError: false,
              filename: "",
              file: null
            });
          })
          .catch((error) => {
            console.log("error", error);
            this.setState({
              uploading: false,
              fileError: true,
            });
          });
      }
    );
  };

  handleSubmit = () => {
    const { form, formErrors } = this.state;
    const { deployment } = this.props;
    const errorObj = this.validateForm(form, formErrors, this.validateField);
    if (Object.keys(errorObj).length !== 0) {
      this.setState({ formErrors: { ...formErrors, ...errorObj } });
      return false;
    }
    const inputObj = {
      bucket: form.inputBucket,
      key: form.inputKey,
      aws_secret: form.inputAwsSecret,
    };
    const outputObj = {
      bucket: form.outputBucket,
      key: form.outputKey,
      aws_secret: form.outputAwsSecret,
    };
    const obj = {
      projectName: deployment.metadata.labels["aiops.predera.com/projectName"],
      projectId: deployment.metadata.labels["aiops.predera.com/projectId"],
      inputs: inputObj,
      outputs: outputObj,
    };
    submitBatchScore(this.props.token, deployment.metadata.name, obj)
      .then((res) => {
        this.props.alertMsg(
          "Batch Preddiction Submitted Successfully",
          "success"
        );
        this.loadBatchScoreRuns();
      })
      .catch((error) => {
        this.setState({
          formError: true,
        });
      });
  };

  loadBatchScoreRuns() {
    const { deployment } = this.props;
    var obj = {
      startDate: null,
      endDate: null,
      name: deployment.metadata.name,
      sort: 'requested_date,desc'
    };
    this.props.getBatchScoreRuns(obj);
  }

  render() {
    const {
      importFileType,
      experiment,
      key,
      bucket,
      file_id,
      filename,
      hostname,
    } = this.state;
    return (
      <div>
        <div className="">
          <div>
            <div class="data-sec-head mt-15"></div>
            <ul className="inline-tabs tabs-blue mt mb p-0 inline-block">
              <li
                className={`react-tabs__tab${importFileType === "FILE" &&
                  "--selected"}`}
                onClick={() => {
                  this.handleTabChange({
                    target: {
                      name: "importFileType",
                      value: "FILE",
                    },
                  });
                }}
              >
                Desktop
              </li>

              <li
                className={`react-tabs__tab${importFileType === "S3" &&
                  "--selected"}`}
                onClick={() => {
                  this.handleTabChange({
                    target: {
                      name: "importFileType",
                      value: "S3",
                    },
                  });
                }}
              >
                Cloud
              </li>
            </ul>
          </div>
          {importFileType === "FILE" && !filename && (
            <div className="mt ml-15">
              <Dropzone
                onDrop={(acceptedFiles) => {
                  if (acceptedFiles) {
                    this.setState({
                      filename: acceptedFiles[0].name,
                      file: acceptedFiles[0],
                    });
                    this.validateImport();
                  }
                }}
              >
                {({ getRootProps, getInputProps }) => (
                  <div
                    {...getRootProps()}
                    className="content-center upload-space"
                  >
                    <input {...getInputProps()} />
                    <p>
                      <i
                        className="fa fa-file-upload"
                        style={{ fontSize: 60 }}
                      />
                    </p>
                    <p>Drag 'n' drop file here, or click to select file</p>
                  </div>
                )}
              </Dropzone>
            </div>
          )}
          {importFileType === "FILE" && filename && (
            <div style={{ height: 100 }}>
              <Chip
                label={filename}
                onDelete={() => {
                  this.setState({
                    filename: "",
                    file_id: "",
                    fileError: false,
                  });
                }}
              />
            </div>
          )}
          {importFileType === "FILE" && this.state.fileError ? (
            <div className="row mt-15">
              <div className="col-sm-12">
                <Tooltip title="Double click to dismiss">
                  <div
                    className="alert alert-danger f-13"
                    onDoubleClick={() => {
                      this.setState({
                        fileError: false,
                      });
                    }}
                  >
                    <strong>Error: </strong>
                    No file is selected. Please select a file before you Submit batch prediction.
                  </div>
                </Tooltip>
              </div>
            </div>
          ) : (
            <div></div>
          )}

          {importFileType === "S3" && (
            <div className="mt">
              <div className="modal-body">
                <div className="mt-10">
                  <strong>S3 Input File</strong>
                </div>
                <div className="row mt-15">
                  <div className="col-sm-6">
                    <TextField
                      label="Bucket"
                      fullWidth
                      name={"inputBucket"}
                      value={this.state.form.inputBucket}
                      onChange={this.handleChange}
                      onBlur={this.handleChange}
                    />
                    {this.state.formErrors.inputBucket && (
                      <span className="err">
                        {this.state.formErrors.inputBucket}
                      </span>
                    )}
                  </div>
                  <div className="col-sm-6">
                    <TextField
                      label="Key"
                      fullWidth
                      name={"inputKey"}
                      value={this.state.form.inputKey}
                      onChange={this.handleChange}
                      onBlur={this.handleChange}
                    />
                    {this.state.formErrors.inputKey && (
                      <span className="err">
                        {this.state.formErrors.inputKey}
                      </span>
                    )}
                  </div>

                  <div className="col-sm-12">
                    <TextField
                      label="Secret"
                      name={"inputAwsSecret"}
                      select
                      fullWidth
                      value={this.state.form.inputAwsSecret}
                      onChange={this.handleChange}
                      onBlur={this.handleChange}
                    >
                      <MenuItem value={""}>Select Secret</MenuItem>
                      {this.state.secrets.map((secret) => (
                        <MenuItem key={secret.name} value={secret.name}>
                          {secret.name}
                        </MenuItem>
                      ))}
                    </TextField>
                    {this.state.formErrors.inputAwsSecret && (
                      <span className="err">
                        {this.state.formErrors.inputAwsSecret}
                      </span>
                    )}
                  </div>
                </div>
                <div className="mt-48">
                  <strong>S3 Output File</strong>
                </div>
                <div className="row">
                  <div className="col-sm-6">
                    <TextField
                      label="Bucket"
                      fullWidth
                      name={"outputBucket"}
                      value={this.state.form.outputBucket}
                      onChange={this.handleChange}
                      onBlur={this.handleChange}
                    />
                    {this.state.formErrors.outputBucket && (
                      <span className="err">
                        {this.state.formErrors.outputBucket}
                      </span>
                    )}
                  </div>
                  <div className="col-sm-6">
                    <TextField
                      label="Key"
                      fullWidth
                      name={"outputKey"}
                      value={this.state.form.outputKey}
                      onChange={this.handleChange}
                      onBlur={this.handleChange}
                    />
                    {this.state.formErrors.outputKey && (
                      <span className="err">
                        {this.state.formErrors.outputKey}
                      </span>
                    )}
                  </div>

                  <div className="col-sm-12">
                    <TextField
                      label="Secret"
                      name={"outputAwsSecret"}
                      select
                      value={this.state.form.outputAwsSecret}
                      onChange={this.handleChange}
                      fullWidth
                    >
                      <MenuItem value={""}>Select Secret</MenuItem>
                      {this.state.secrets.map((secret) => (
                        <MenuItem key={secret.name} value={secret.name}>
                          {secret.name}
                        </MenuItem>
                      ))}
                    </TextField>
                    {this.state.formErrors.outputAwsSecret && (
                      <span className="err">
                        {this.state.formErrors.outputAwsSecret}
                      </span>
                    )}
                  </div>
                </div>
                {this.state.formError ? (
                  <div className="row mt-15">
                    <div className="col-sm-12">
                      <Tooltip title="Double click to dismiss">
                        <div
                          className="alert alert-danger f-13"
                          onDoubleClick={() => {
                            this.setState({
                              formError: false,
                            });
                          }}
                        >
                          <strong>Error: </strong>
                          An error occurred while submitting the Batch
                          Predictions, please try again.
                        </div>
                      </Tooltip>
                    </div>
                  </div>
                ) : (
                  <div></div>
                )}
              </div>
            </div>
          )}
          <button
            type="button"
            className="btn btn-primary mt-48 ml-0"
            onClick={this.handlePredictionSubmit}
          >
            Submit
          </button>
          <div className="mt-15">
            <Divider />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      alertMsg,
      getBatchScoreRuns
    },
    dispatch
  );
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BatchScoringFormComponent);
