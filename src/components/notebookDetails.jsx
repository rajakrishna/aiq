import React, {Component} from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getCpuUsageByNotebook, getMemoryUsageByNotebook, getNotebookList, getToken, updateNotebook, deleteNotebook } from "../api";
import LeftArrow from "../images/left-chevron.png";
import moment from "moment";
import CpuChart from './chart/CpuChart';
import MemoryChart from './chart/MemoryChart';
import sklearnlogo_logo from '../images/library/sklearnlogo.png';
import tensorflowlogo_logo from "../images/library/tensorflowlogo.jpg";
import Rlogo_logo from "../images/library/Rlogo.png";
import julialogo_logo from "../images/library/julialogo.png";
import spark_logo_logo from "../images/library/spark-logo.png";
import Divider from "@material-ui/core/Divider";
import CircularProgress from "@material-ui/core/CircularProgress";
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import { confirm } from "./Confirm";
import MicroModal from "micromodal";
import { Copy } from "./Copy";
import Tooltip from "@material-ui/core/Tooltip";

class NotebookDetails extends Component {

      state = {
        notebookDetails: {},
        loader: false,
        tokenId: '',
        tokenError: false,
        notebook:{},
        tab: 0,
        cpuTab: 0,
        cpuUsage: {},
        cpuData: [],
        memoryTab: 0,
        memoryUsage: {},
        memoryData: []
      }

      handleDelete = (name) => {
        deleteNotebook(this.props.token, name).then((res) => {
          this.fetchNotebooks();
        });
      };

      handleChange =(e)=>{
        const target = e.target;
        let name = target.name;
        let value = (name === "mem") ? target.value+"Gi" : target.value;
        this.setState({
          ...this.state,notebookDetails: {...this.state.notebookDetails, [name]: value}
        })
      }
    
      handleBlurCpu =()=>{
        if((this.props.user.tier === 'Standard Tier') && this.state.notebook.cpu > '2'){
          this.setState({cpuWarning:"Max CPU size allowed for this subscription is 2."})
        }
      }
    
      handleBlurMem =()=>{
        if((this.props.user.tier === 'Standard Tier') && this.state.notebook.mem > '4Gi'){
          this.setState({memWarning:"Max Memory size allowed for this subscription is 4Gi."})
        }
      }

      updateNotebook =()=>{
        const gpuValue = (this.state.notebookDetails.gpu == 0) ? {} : {"nvidia.com/gpu":this.state.notebookDetails.gpu}
        const updateObj = {
          "cpu":this.state.notebookDetails.cpu,
          "memory":this.state.notebookDetails.mem,
          "extraResources": gpuValue
        }
        updateNotebook(this.props.token,this.props.selectedProject && this.props.selectedProject.id,
          this.state.notebook.name,updateObj).then((res)=>{
              MicroModal.close("modify-modal");
          })
          .catch((err)=>{
            console.log("error while notebook Update:",err.message)
          })
      }

      handleEdit =() =>{
        this.setState({notebook:this.state.notebookDetails});
        this.setState({cpuWarning:""});
        this.setState({memWarning:""});
      }

      getToken = (notebook) => {
        this.setState({ loader: true });
        MicroModal.show("notebook-token-modal");
        getToken(this.props.token, notebook && notebook.name)
          .then((res) => {
            this.setState({ tokenId: res.data.token });
            this.setState({ loader: false });
          })
          .catch((error) => {
            this.setState({ tokenError: true, loader: false });
          });
      };

      getImageLogoFromImage(image) {
        let logo;
        let logoDescription;
        switch (image) {
          case 'scipy-notebook':
            logo = sklearnlogo_logo
            logoDescription = 'Includes popular package from the scientific Python ecosystem'
            break;
          case 'tensorflow-notebook':
            logo = tensorflowlogo_logo
            logoDescription = 'Includes popular Python deep learning libraries'
            break;
          case 'r-notebook':
            logo = Rlogo_logo
            logoDescription = 'Includes popular popular packages from the R ecosystem'
            break;
          case 'datascience-notebook':
            logo = julialogo_logo
            logoDescription = 'Includes libraries for data analysis from the julia, Python and R communities'
            break;
          case 'all-spark-notebook':
            logo = spark_logo_logo,
            logoDescription = 'Includes Python, R and Scala support for Apache Spark'
            break;
        
          default:
            logo = ''
            logoDescription = ''
            break;
        }
        return {logo, logoDescription};
      }

      getStepTimeForCpuMemoryApi(tabValue) {
        let stepTime;
        let timeDiff;
        switch (tabValue) {
          case 1:
            stepTime = '4m'
            timeDiff = 60
            break;
          case 2:
            stepTime = '2h'
            timeDiff = 1440
            break;
          case 3:
            stepTime = '12h'
            timeDiff = 10080
            break;
          case 4:
            stepTime = '16h'
            timeDiff = 43200
            break;        
          default:
            stepTime = '1m'
            timeDiff = 15
            break;
        }
        return {stepTime, timeDiff};
      }

      getCpuUsageByNotebookApi(name) {
        const startDate = new Date();
        startDate.setMinutes(startDate.getMinutes() - this.getStepTimeForCpuMemoryApi(this.state.cpuTab).timeDiff);
        const newStartIsoDate = startDate.toISOString();
        const endDate = new Date();
        const newEndIsoDate = endDate.toISOString();
        getCpuUsageByNotebook(this.props.token, name, newStartIsoDate, newEndIsoDate, this.getStepTimeForCpuMemoryApi(this.state.cpuTab).stepTime)
        .then((res)=> {
          const data = res.data.data.result[0].values;
          // const xAxisCpuData = data.map((item)=>{
          //   let cpuDate = new Date(item[0]*1000)
          //   return (moment(cpuDate).format('HH:mm') ) })
          // const yAxisCpuData = data.map((element)=>element[1]*1000)
          // this.setState({xCpuData:xAxisCpuData})
          this.setState({cpuData: data})
        })
        .catch((err)=>console.log("getCpuUsageByNotebook :: ",err));
      }
    
      getMemoryUsageByNotebookApi(name) {
        const startDate = new Date();
        startDate.setMinutes(startDate.getMinutes() - this.getStepTimeForCpuMemoryApi(this.state.memoryTab).timeDiff);
        const newStartIsoDate = startDate.toISOString();
        const endDate = new Date();
        const newEndIsoDate = endDate.toISOString();
        getMemoryUsageByNotebook(this.props.token, name , newStartIsoDate, newEndIsoDate, this.getStepTimeForCpuMemoryApi(this.state.memoryTab).stepTime)
        .then((res)=> {
          const data = res.data.data.result[0].values;
          // const xAxisMemoryData = data.map((item)=>{
          //   let memoryDate = new Date(item[0]*1000)
          //   return (moment(memoryDate).format('HH:mm') ) })
          // const yAxisMemoryData = data.map((element)=>element[1]/1000)
          // this.setState({xMemoryData:xAxisMemoryData})
          this.setState({memoryData:data})
        })
        .catch((err)=>console.log("getMemoryUsageByNotebook :: ",err));
      }

      fetchNotebooks = () => {
        const pathName = window.location.pathname.split("/");
        const notebookName = pathName[pathName.length-1];
        getNotebookList(this.props.token, this.props.selectedProject && this.props.selectedProject.id)
          .then((res) => {
            const notebookDetails = {...res.data.notebooks.find((eachItem) => eachItem.name === notebookName)};
            this.getCpuUsageByNotebookApi(notebookDetails.name);
            this.getMemoryUsageByNotebookApi(notebookDetails.name);
            this.setState({
              notebookDetails: {...res.data.notebooks.find((eachItem) => eachItem.name === notebookName)},
            });
          })
          .catch((err) => {
            console.log("fetch notebook error :: ", err )
          });
      };

      componentDidMount = () => {
        this.fetchNotebooks(); 
      }

      getNotebookDetails = () => {
        return (
          <div className="m-details-body">
            <div className="m-info-table pl-3 pr-4 pt-4 ml-2 mr-5">

            <div className="d-flex flex-row align-items-center mb-4">
                <img
                  className="img-scipy-notebook-logo"
                  src={this.getImageLogoFromImage(this.state.notebookDetails.srt_image).logo}
                  alt=""
                />

                <div className="review-size ml-4">
                  {this.state.notebookDetails.srt_image}
                  <p className="f-13 text-gray mb-0">Package</p>
                </div>

                <div className="review-size ml-4">
                  {this.state.notebookDetails.status && 'Running'}
                  <p className="f-13 text-gray mb-0">Status</p>
                </div>

                <div className="review-size ml-4">
                  {this.state.notebookDetails.status && this.state.notebookDetails.status.running  && new Date(this.state.notebookDetails.status.running.startedAt).toDateString()}
                  <p className="f-13 text-gray mb-0">Started</p>
                </div>
            </div>
              
            <div className="review-package mt-5">
              Notebook Size
            </div>

            <div className="mt-15">
              <Divider />
            </div>

            <div className="review-dfmr mt-24">
              <div className="review-size">
                {this.state.notebookDetails.cpu}
                <p className="f-13 text-gray">CPU</p>
              </div>
              <div className="review-size">
                {this.state.notebookDetails.mem}
                <p className="f-13 text-gray">Memory</p>
              </div>
              <div className="review-size">
                {this.state.notebookDetails.gpu}
                <p className="f-13 text-gray">GPU</p>
              </div>
            </div>

            <div className="ml-10 review-notebook mt-3">
              Storage
            </div>

            <div className="mt-15">
              <Divider />
            </div>

            {this.state.notebookDetails.volumes && this.state.notebookDetails.volumes.map((item) => {
              return(
              <div className="mt-15" style={{ display: "flex" }}>
                <div className="review-flx-150">
                  {item.name}
                  <p className="f-13 text-gray">Name</p>
                </div>
                <div className="review-flx-150">
                  {item.persistentVolumeClaim.claimName}
                  <p className="f-13 text-gray">Claim Name</p>
                </div>
              </div>
              )
           })}
           
            </div>
            <hr />
            <div>
              <ul className="inline-tabs tabs-blue pad-50 ml-2">      
                <li
                  className={`react-tabs__tab${this.state.tab === 0 &&
                    "--selected"}`}
                  onClick={() => {
                    this.setState({ tab: 0 });
                  }}
                >
                  Resource Usage
                </li>
              </ul>
            </div>
            <hr />
            <div>
              {this.state.tab === 0 && (
                <div className="m-info-table pl-3 pr-4 pt-1 ml-2 mr-5" style={{ minHeight: 452 }}>
                <div className="mt-lg">
                  <div className="inline-block" style={{ width: "49%" }}>
                    <h4>CPU Usage</h4>
                    <ul className="inline-tabs tabs-gray mb-0 mt-lg pl-0">
                      <li
                        className={`react-tabs__tab${this.state.cpuTab === 0 &&
                          "--selected"}`}
                        onClick={() => {
                          this.setState({ cpuTab: 0 }, this.fetchNotebooks()  );
                        }}
                      >
                        15 MINUTES
                      </li>
                      <li
                        className={`react-tabs__tab${this.state.cpuTab === 1 &&
                          "--selected"}`}
                        onClick={() => {
                          this.setState({ cpuTab: 1 }, this.fetchNotebooks());
                        }}
                      >
                        1 HOUR
                      </li>
                      <li
                        className={`react-tabs__tab${this.state.cpuTab === 2 &&
                          "--selected"}`}
                        onClick={() => {
                          this.setState({ cpuTab: 2 }, () => this.fetchNotebooks());
                        }}
                      >
                        24 HOURS
                      </li>
                      <li
                        className={`react-tabs__tab${this.state.cpuTab === 3 &&
                          "--selected"}`}
                        onClick={() => {
                          this.setState({ cpuTab: 3 }, () => this.fetchNotebooks());
                        }}
                      >
                        7 DAYS
                      </li>
                      <li
                        className={`react-tabs__tab${this.state.cpuTab === 4 &&
                          "--selected"}`}
                        onClick={() => {
                          this.setState({ cpuTab: 4 }, () => this.fetchNotebooks());
                        }}
                      >
                        30 DAYS
                      </li>
                    </ul>
                    <CpuChart cpuData={this.state.cpuData} />
                  </div>
                  <div className="inline-block" style={{ width: "49%" }}>
                    <h4>Memory Usage</h4>
                    <ul className="inline-tabs tabs-gray mb-0 mt-lg pl-0">
                      <li
                        className={`react-tabs__tab${this.state.memoryTab === 0 &&
                          "--selected"}`}
                        onClick={() => {
                          this.setState({ memoryTab: 0 }, () => this.fetchNotebooks());
                        }}
                      >
                        15 MINUTES
                      </li>
                      <li
                        className={`react-tabs__tab${this.state.memoryTab === 1 &&
                          "--selected"}`}
                        onClick={() => {
                          this.setState({ memoryTab: 1 }, () => this.fetchNotebooks());
                        }}
                      >
                        1 HOUR
                      </li>
                      <li
                        className={`react-tabs__tab${this.state.memoryTab === 2 &&
                          "--selected"}`}
                        onClick={() => {
                          this.setState({ memoryTab: 2 }, () => this.fetchNotebooks());
                        }}
                      >
                        24 HOURS
                      </li>
                      <li
                        className={`react-tabs__tab${this.state.memoryTab === 3 &&
                          "--selected"}`}
                        onClick={() => {
                          this.setState({ memoryTab: 3 }, () => this.fetchNotebooks());
                        }}
                      >
                        7 DAYS
                      </li>
                      <li
                       className={`react-tabs__tab${this.state.memoryTab === 4 &&
                        "--selected"}`}
                      onClick={() => {
                        this.setState({ memoryTab: 4 }, () => this.fetchNotebooks())
                      }}
                      >
                        30 DAYS
                      </li>
                    </ul>
                    <MemoryChart memoryData={this.state.memoryData} />
                  </div>
                </div>
              </div>
              )}
            </div>
          </div>
        );
      };

      render() {
        return (
            <div className="main-container">
            <div className="main-body" style={{ overflow: "hidden" }}>
              <div className="content-wraper-sm">
                <div className="m-details-card">
                  <div className="m-details-card-head">
                    {/* <div
                      className="back-action clickable"
                      onClick={() => {
                        this.props.history.goBack();
                      }}
                    >
                      <img src={LeftArrow} alt="left arrow" />
                    </div> */}
                    <div className="m-title-info">
                      <h4>{this.state.notebookDetails.name}</h4>
                      <small>NOTEBOOK</small>
                    </div>
                    <div className="m-tech-stack mr-5 d-flex flex-row align-items-center">
                      {this.state.notebookDetails.status && this.state.notebookDetails.status.running ? (
                        <Tooltip title="Connect Notebook">
                          <a
                            className="mr"
                            style={{fontSize: '13px'}}
                            href={`${process.env.REACT_APP_API_URL}/../notebook/kai/${this.state.notebookDetails.name}`}
                            target="_blank"
                          >
                            CONNECT
                          </a>
                        </Tooltip>
                      ) : (
                        <span className="text-gray mr clickable disabled">
                          CONNECT
                        </span>
                      )}
                      <Tooltip 
                        title="Refresh Notebook">
                        <i 
                          className="fa fa-sync-alt ml mr clickable"
                          onClick={() => {
                            this.fetchNotebooks();
                          }}
                          ></i>
                      </Tooltip>
                      <Tooltip title="Notebook Token">
                        <i 
                          onClick={() => {
                            this.getToken(this.state.notebookDetails)
                          }}
                          className="fa fa-key ml mr clickable"></i>
                      </Tooltip>
                      <Tooltip title="Update Notebook">
                        <i 
                          onClick={
                            ()=>{
                              MicroModal.show("modify-notebook");
                              this.handleEdit(this.state.notebookDetails.name)
                            }
                          }
                          className="fas fa-edit ml clickable mr"
                          data-toggle="modal"
                          data-target=" #update_notebook"
                        ></i>
                      </Tooltip>
                      <Tooltip title="Delete Notebook">
                        <i
                          onClick={() => {
                            confirm(
                              "This will delete the Notebook but not the Volumes. A new Notebook can be created later using the same Volumes to restore data.",
                              "Delete Notebook?",
                              "delete"
                            ).then(() => {
                              this.handleDelete(this.state.notebookDetails.name);
                            });
                          }}
                          className="fa fa-trash ml clickable"
                        ></i>
                      </Tooltip>
                    </div>
                  </div>
                  {this.getNotebookDetails()}
                </div>
              </div>
            </div>
            <div
        className="modal micromodal-slide"
        id="notebook-token-modal"
        aria-hidden="true"
      >
        <div className="modal__overlay" tabIndex="-1" data-micromodal-close>
          <div
            className="modal__container"
            role="dialog"
            aria-modal="true"
            aria-labelledby="secret-modal-title"
          >
            <header className="modal__header">
              <h2 className="modal__title ml-15">Notebook Token</h2>
              <button
                type="button"
                className="modal__close"
                aria-label="Close modal"
                data-micromodal-close
              />
            </header>

            <main className="modal__content mt-0 mb-0 h120">
              {this.state.loader ? (
                <div className="modal-filter-form">
                  <div className="modal-body f-13 ml-15 mt-30 mr">
                    <CircularProgress size={20} disableShrink />
                  </div>
                </div>
              ) : (
                <div>
                  {this.state.tokenError ? (
                    <div className="modal-filter-form">
                      <div className="modal-body alert alert-danger f-13 ml-15 mt-30 mr">
                        {/* onClick={this.getToken(this.notebook)} */}
                        <strong>Error: </strong>
                        An error occurred while fetching the Token, please try again.
                      </div>
                      <div className="ml-15 mt-30">
                        <button
                          onClick={() => {
                            this.getToken(this.notebook);
                          }}
                          type="button"
                          className="btn btn-primary"
                        >
                          Retry
                        </button>
                        <button
                          onClick={() => MicroModal.close("notebook-token-modal")}
                          type="button"
                          className="btn btn-secondary ml-15"
                        >
                          Close
                        </button>
                      </div>{" "}
                    </div>
                  ) : (
                    <div className="modal-filter-form">
                      <div className="modal-body f-13">
                        <Copy className="pd-0">{`${this.state.tokenId}`}</Copy>

                        <div className="text-gray mt-lg">
                          You will be asked to provide a token when connecting to a
                          Notebook. Please use the above token when prompted
                        </div>
                      </div>
                      <div className="ml-15 mt-10">
                        {this.props.loader && <CircularProgress size={20} disableShrink />}

                        <button
                          onClick={() => MicroModal.close("notebook-token-modal")}
                          type="button"
                          className="btn btn-primary"
                        >
                          Close
                        </button>
                      </div>
                    </div>
                  )}
                </div>
              )}
            </main>
          </div>
        </div>
      </div>
      {/* notebook update modal */}
      <div
        className="modal micromodal-slide"
        id="modify-notebook"
        aria-hidden="true"
      >
        <div className="modal__overlay" tabIndex="-1" data-micromodal-close>
          <div
            className="modal__container"
            role="dialog"
            aria-modal="true"
            aria-labelledby="project-modal-title"
          >
            <header className="modal__header">
              <h2 className="modal__title">{`Update Notebook : ${this.state.notebookDetails.name}`}</h2>
              <button
                type="button"
                className="modal__close"
                aria-label="Close modal"
                data-micromodal-close
              />
            </header>
            <main className="modal__content">
              <h5 className="mb-0">Resources</h5>
             <form>
              <div className="modal-filter-form">
                <div className="modal-body">
                 <div className="mt-30">
                  <FormControl fullWidth error={(this.props.user && this.props.user.tier === 'Standard Tier') && this.state.cpuWarning}>
                    <InputLabel htmlFor="notebook_cpu">CPU</InputLabel>
                    <Input
                       id={"notebook_cpu"}
                       name={"cpu"}
                      value={this.state.notebookDetails.cpu || ""}
                      onBlur={this.handleBlurCpu}
                      onFocus={()=>this.setState({cpuWarning:""})}
                      onChange ={(e) => this.handleChange(e)}
                      inputProps={{ min: 0.1, step: 0.1 }}
                      type="number"
                      aria-describedby="component-cpu-error-text"
                    />
                     <FormHelperText id="component-cpu-error-text">
                      {
                        (this.props.user.tier === 'Standard Tier') && this.state.cpuWarning ? this.state.cpuWarning:"vCPU/Core"
                      }
                      </FormHelperText>
                  </FormControl>
                 </div>
                 <div className="mt-30">
                    <FormControl fullWidth error={(this.props.user.tier === 'Standard Tier') && this.state.memWarning }>
                      <InputLabel htmlFor="notebook_memory">Memory</InputLabel>
                        <Input
                          id={"notebook_memory"}
                          name={"mem"}
                          inputProps={{ min: 0.5, step: 0.5 }}
                          type="number"
                          value={this.state.notebookDetails.mem && this.state.notebookDetails.mem.replace("Gi","") || ""}
                          onChange = {(e) => this.handleChange(e)}
                          onBlur={this.handleBlurMem}
                          onFocus={()=>this.setState({memWarning:""})}
                          aria-describedby="component-mem-error-text"
                        />
                      <FormHelperText id="component-mem-error-text">
                        {
                          (this.props.user.tier === 'Standard Tier') && this.state.memWarning ? this.state.memWarning : "Gi"
                        }
                      </FormHelperText>
                    </FormControl>
                    </div>
                    <div className="mt-30">
                    <FormControl   fullWidth  disabled = {this.props.user.tier === 'Standard Tier'}>
                      <InputLabel htmlFor="notebook_gpu">GPU</InputLabel>
                      <Input
                       id="notebook_gpu"
                       name={"gpu"} 
                       inputProps={{ min: 0, step: 1 }}
                       type="number"
                       value={this.state.notebookDetails.gpu || 0}
                       onChange = {this.handleChange} 
                       />
                      <FormHelperText>
                        {
                          (this.props.user.tier === 'Standard Tier') ? "Note: for current subscription you are not allowed to change GPU" : ""
                        }
                      </FormHelperText>
                    </FormControl>
                   </div>   
                  </div>
                
                <div className="modal-footer">
                  {this.props.loader && <CircularProgress size={20} disableShrink />}
                  <button
                    onClick={() => MicroModal.close("modify-notebook")}
                    type="button"
                    className="btn btn-secondary"
                  >
                    Cancel
                  </button>
                  <button
                    type="button"
                    className={`btn btn-primary clickable ${(this.state.cpuWarning || this.state.memWarning) ? "disabled":""}`}
                    onClick={this.updateNotebook}
                    disabled={this.state.cpuWarning || this.state.memWarning}
                  >
                    Update 
                  </button>
                </div>
              </div>
            </form> 
            </main>
          </div>
        </div>
      </div>
          </div>
        )
      }
}

const mapStateToProps = (state) => {
    return {
      token: state.auth.token,
      user: state.users.user,
      selectedProject: state.projects.globalProject,
    };
  };

  
  const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
      {
      },
      dispatch
    );
  };

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NotebookDetails);