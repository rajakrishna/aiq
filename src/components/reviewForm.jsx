import React, { Component } from "react";
import { connect } from "react-redux";

import { getNotebooksUnusedVolumes } from "../api";
import { Config } from "./Notebook.config.js";
import sklearnlogo_logo from "../images/library/sklearnlogo.png";
import tensorflowlogo_logo from "../images/library/tensorflowlogo.jpg";
import Rlogo_logo from "../images/library/Rlogo.png";
import julialogo_logo from "../images/library/julialogo.png";
import spark_logo_logo from "../images/library/spark-logo.png";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";

import BorderColorIcon from "@material-ui/icons/BorderColor";
import Divider from "@material-ui/core/Divider";
const styles = (theme) => ({
  root: {
    width: "100%",
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: "33.33%",
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
});
class ReviewForm extends Component {
  state = {
    expanded: "panel1",

    standardImages: "",
    packages: Config.packages,
    availableVolumesNames:[],
    volumesError:""
  };
  constructor(props) {
    super();
    this.setState({
      standardImages: props.data.standardImages,
    });
  }

  componentDidMount =()=>{
    this.handleVolumesValidation();

  }


  handleVolumesValidation =()=>{
    getNotebooksUnusedVolumes(this.props.token,this.props.data.project_id).then((res)=>{
      let volsNames = res.data.volumes.map((item)=> item.name)
      this.setState({availableVolumesNames:volsNames},()=>{

        let existCheck = this.state.availableVolumesNames.includes(this.props.data.ws_name)
        if(!existCheck){
          this.setState({volumesError:`${this.props.data.ws_name} is already in use`},()=>{
            this.props.handleVolumesError(this.state.volumesError)
          })
        }
        
          this.props.data.dataVols.map((item)=>{
            let checkDataVols = this.state.availableVolumesNames.includes(item.vol_name)
            if((this.state.availableVolumesNames.length !== 0) && !checkDataVols){
              this.setState({volumesError:`${item.vol_name} is already in use`},()=>{
                this.props.handleVolumesError(this.state.volumesError)
              })
            }
          })
        
      })
    })
    .catch((err)=>console.log("getNotebooksVolumes::reviewForm::err.message::",err.message))
  }

  render() {
    const { expanded } = this.state;
    return (
      <div className="package-content">
        <form>
          <div className="pd-lg pl-0">
            <div className="step-title">Step 4: Review</div>
            <div className="mt-lg step-description">
              Please review your Notebook configuration details. You can go back
              to the edit changes for each section. Click Create to complete the
              notebook creation.
            </div>

            <div className="mt-5 review-package">
              Package Details
              <BorderColorIcon
                className="f-r"
                onClick={this.props.choosePackage}
              ></BorderColorIcon>
            </div>

            <div className="mt-15">
              <Divider />
            </div>

            {this.state.packages.map((item) => (
              <div className="mt-24">
                {item.package == this.props.data.standardImages ? (
                  <List>
                    <ListItem>
                      {item.package ==
                      "gcr.io/aiops-224805/scipy-notebook:latest" ? (
                        <img
                          className="img-scipy"
                          src={sklearnlogo_logo}
                          alt=""
                        />
                      ) : (
                        <div></div>
                      )}

                      {item.package ==
                      "gcr.io/aiops-224805/tensorflow-notebook:latest" ? (
                        <img
                          className="package-img1"
                          src={tensorflowlogo_logo}
                          alt=""
                        />
                      ) : (
                        <div></div>
                      )}

                      {item.package ==
                      "gcr.io/aiops-224805/r-notebook:latest" ? (
                        <img className="img-scipy" src={Rlogo_logo} alt="" />
                      ) : (
                        <div></div>
                      )}

                      {item.package ==
                      "gcr.io/aiops-224805/datascience-notebook:latest" ? (
                        <img
                          className="package-img2"
                          src={julialogo_logo}
                          alt=""
                        />
                      ) : (
                        <div></div>
                      )}

                      {item.package ==
                      "gcr.io/aiops-224805/all-spark-notebook:latest" ? (
                        <img
                          className="package-img3"
                          src={spark_logo_logo}
                          alt=""
                        />
                      ) : (
                        <div></div>
                      )}

                      <ListItemText
                        primary={item.name}
                        secondary={item.packageDesc}
                      />
                    </ListItem>
                  </List>
                ) : (
                  <span></span>
                )}
              </div>
            ))}
            <div className="review-notebook">
              Notebook Size
              <BorderColorIcon
                className="f-r"
                onClick={this.props.chooseSize}
              ></BorderColorIcon>
            </div>
            <div className="mt-15">
              <Divider />
            </div>
            <div className="review-dfmr mt-24">
              <div className="review-size ">
                {this.props.data.size}
                <p className="f-13 text-gray">Size</p>
              </div>
              <div className="review-size">
                {this.props.data.cpu}
                <p className="f-13 text-gray">CPU</p>
              </div>
              <div className="review-size">
                {this.props.data.memory}
                <p className="f-13 text-gray">Memory</p>
              </div>
              <div className="review-size">
                {this.props.data.gpu}
                <p className="f-13 text-gray">GPU</p>
              </div>
            </div>

            <div className="ml-10 review-notebook">
              {" "}
              Storage
              <BorderColorIcon
                className="f-r"
                onClick={this.props.addStorage}
              ></BorderColorIcon>
            </div>
            <div className="mt-15">
              <Divider />
            </div>

            <div className="review-dfmr mt-24">
              <div className="review-flx-150">
                {this.props.data.ws_type}
                <p className="f-13 text-gray">Type</p>
              </div>
              <div className="review-flx-150">
                {this.props.data.ws_name}
                <p className="f-13 text-gray">Name</p>
              </div>
              <div className="review-flx-150">
                {this.props.data.ws_size}
                <p className="f-13 text-gray">Size</p>
              </div>
              <div className="review-flx-150">
                {this.props.data.ws_mount_path}
                <p className="f-13 text-gray">Path</p>
              </div>
              <div className="review-flx-150">
                {this.props.data.ws_access_modes}
                <p className="f-13 text-gray">Access Mode</p>
              </div>
            </div>
            {this.props.data.dataVols.map((item) => {
              return(
              <div className="mt-15" style={{ display: "flex" }}>
                <div className="review-flx-150">
                  {item.vol_type}
                  <p className="f-13 text-gray">Type</p>
                </div>
                <div className="review-flx-150">
                  {item.vol_name}
                  <p className="f-13 text-gray">Name</p>
                </div>
                <div className="review-flx-150">
                  {item.vol_size}
                  <p className="f-13 text-gray">Size</p>
                </div>
                <div className="review-flx-150">
                  {item.vol_mount_path}
                  <p className="f-13 text-gray">Path</p>
                </div>
                <div className="review-flx-150">
                  {item.vol_access_modes}
                  <p className="f-13 text-gray">Access Mode</p>
                </div>
              </div>
              )
           })}
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
  };
};

export default connect(mapStateToProps)(ReviewForm);
