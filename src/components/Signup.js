import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Redirect, Link } from "react-router-dom";

import Mixpanel from "mixpanel-browser";
import { registerTenant, authReset } from "../ducks/auth";
import logo from "../images/predera-logo.png";

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      firstName: "",
      lastName: "",
      email: "",
      companyName: "",
      tier: "Free Tier",
      loader: false,
    };
  }

  componentDidMount() {
    Mixpanel.track("SignUp Page");
    this.props.authReset();
  }

  onSubmit = (e) => {
    this.props.authReset();
    e.preventDefault();
    const registrationDetails = {
      id: this.state.id,
      firstName: this.state.firstName.trim(),
      lastName: this.state.lastName.trim(),
      email: this.state.email.trim(),
      companyName: this.state.companyName.trim(),
      accountName: this.state.companyName.trim(),
      userName: this.state.email.trim(),
      ownerName: this.state.email.trim(),
      tier: this.state.tier,
    };
    this.setState({ loader: true });
    this.props.registerTenant(registrationDetails);
  };

  handleChange = (event) => {
    this.setState({ tier: event.target.value });
  };

  static getDerivedStateFromProps(nextProps) {
    if (nextProps.auth.message || nextProps.auth.regMessage) {
      return {
        loader: false,
      };
    }
    return null;
  }

  render() {
    const token = this.props.auth && this.props.auth.token;

    return token ? (
      <Redirect to="/" />
    ) : (
      <div className="auth-wraper">
        <form onSubmit={this.onSubmit}>
          <div className="auth-box">
            <div className="logo-head">
              <img src={logo} alt="" />
            </div>
            <h2 className="auth-heading">Sign Up</h2>
            {/* error messages */}
            {this.props.auth.regMessage && (
              <div className="error-placement alert alert-danger">
                <i className="fa fa-exclamation-triangle" />{" "}
                <span>{this.props.auth.regMessage}</span>
              </div>
            )}
            {this.props.auth.sucessMessage && (
              <div className="alert alert-success">
                <span>{this.props.auth.sucessMessage}</span>
              </div>
            )}
            {this.props.auth.sucessMessage &&
              // <div className="alert alert-success">
              //   <span>{this.props.auth.sucessMessage}</span>
              // </div>
              this.props.history.push("/registration")}

            {/* <div className="form-group category-check-wraper">
              <label className="category-checkbox">
                <input type="radio" name="category" value="Individual"/>
                <i></i>Individual
              </label>
              <label className="category-checkbox">
                <input type="radio" name="category" value="Enterprise"/>
                <i></i>Enterprise
              </label>
            </div>*/}
            {!this.props.auth.sucessMessage && (
              <div>
                <div className="form-group form-material">
                  <label>First Name</label>
                  <input
                    value={this.state.firstName}
                    onChange={(e) =>
                      this.setState({ firstName: e.target.value })
                    }
                    type="text"
                    className="form-control"
                    placeholder="Enter first name "
                    required
                  />
                </div>
                <div className="form-group form-material">
                  <label>Last Name</label>
                  <input
                    value={this.state.lastName}
                    onChange={(e) =>
                      this.setState({ lastName: e.target.value })
                    }
                    type="text"
                    className="form-control"
                    placeholder="Enter last name "
                    required
                  />
                </div>
                <div className="form-group form-material">
                  <label>Email</label>
                  <input
                    value={this.state.email}
                    onChange={(e) => this.setState({ email: e.target.value })}
                    type="text"
                    className="form-control"
                    placeholder="Enter email "
                    required
                  />
                </div>
                <div className="form-group form-material">
                  <label>Company</label>
                  <input
                    value={this.state.companyName}
                    onChange={(e) =>
                      this.setState({ companyName: e.target.value })
                    }
                    type="text"
                    className="form-control"
                    placeholder="Enter compnay "
                    required
                  />
                </div>

                <div className="form-group form-material">
                  <label>Plan</label>
                  <select
                    value={this.state.tier}
                    onChange={this.handleChange}
                    className="form-control"
                    required
                  >
                    <option value="Free Tier">Free Tier</option>
                    {/* <option value="Standard Tier" disabled>Standard Tier</option>
                  <option value="Professional Tier" disabled>Professional Tier</option> */}
                  </select>
                </div>
                <br />
                <div className="form-group">
                  <button
                    type="submit"
                    className="btn btn-primary form-btn"
                    disabled={this.state.loader && "disabled"}
                  >
                    {this.state.loader ? "Registering...." : "Register"}
                  </button>
                </div>
              </div>
            )}
            <div className="forgot-link">
              <Link to="/sign_in">Sign In</Link>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      registerTenant,
      authReset,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
