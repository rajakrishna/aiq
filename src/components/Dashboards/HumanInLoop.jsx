import React, { Component } from "react";
import NavigationLinks from "../NavigationLinks";

class HumanInLoop extends Component {
  state = {};

  render() {
    return (
      <div className="main-container">
        <NavigationLinks path={this.props.match.path} />
        {/* <Sidebar path={this.props.match.path}/> */}
        <div className="main-body">
          <div className="content-wraper">
            <h1>HumanInLoop</h1>
          </div>
        </div>
      </div>
    );
  }
}

export default HumanInLoop;
