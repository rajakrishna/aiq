import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import moment from "moment";
import { Copy } from "../Copy";
import { AreaChart, LineChart, ScatterChart } from "react-chartkick";
import {
  getDeployments,
  getDeploymentDetail,
  getDeploymentLogs,
  getDeploymentsCpu,
  getDeploymentsMemory,
  scaleDeployment,
  deleteDeployment,
  downloadPrediction,
  startDeployment,
  stopDeployment,
  reStartDeployment,
  getMetricsRequestrate,
  getMetricsSuccess,
  getMetrics5xx,
  getMetrics4xx,
  getMetricsReqpersec,
  getMetricsLatency,
} from "../../ducks/deployments";
import { Loader1 } from "../home/modelOverviewLoader";
import TestApi from "../TestApi";
import { DeploymentGraph } from "../DeploymentGraph";
import LeftArrow from "../../images/left-chevron.png";
import DeploymentLogs from "./DeploymentLogs";
import Mixpanel from "mixpanel-browser";
import { getDeployementType } from "../../utils/helper";
import { Link } from "react-router-dom";
import { confirm } from "../Confirm";
import { alertMsg } from "../../ducks/alertsReducer";
import MicroModal from "micromodal";
import { metricsLatency } from "../../api";

import {
  Tooltip,
  TextField,
  MenuItem,
  Card,
  CardContent,
  Typography,
  CardActions,
  Button,
  LinearProgress,
} from "@material-ui/core";

import {
  getAllSecrets,
  submitBatchScore,
  getBatchScoringRuns,
} from "../../api";

import Axios from "axios";
import { CircularProgress } from "@material-ui/core";

import BatchScoringComponent from "../batchScoringComponent";
import CpuChart from "../chart/CpuChart";
import MemoryChart from "../chart/MemoryChart";
import RequestSecChart from "../chart/RequestSecChart";
import LatencyChart from "../chart/LatencyChart";

class Deployment extends Component {
  state = {
    deployment: {},
    logs: "",
    tab: 0,
    cpu: [],
    memory: [],
    index: 0,
    updated: 0,
    reload: false,
    secrets: [],
    formError: false,
    form: {
      outputBucket: "",
      outputKey: "",
      outputAwsSecret: "",
      inputBucket: "",
      inputKey: "",
      inputAwsSecret: "",
    },
    formErrors: {
      outputBucket: null,
      outputKey: null,
      outputAwsSecret: null,
      inputBucket: null,
      inputKey: null,
      inputAwsSecret: null,
    },
    formError: false,
    fileError: false,
    score: {
      name: "",
      input: {},
      output: {},
    },
    showPrediction: true,
    importFileType: "FILE",
    filename: null,
    file: null,
    validatedImport: false,
    key: "",
    bucket: "",
    file_id: "",
    uploading: false,
    batchRuns: [],
    loadPercent: 100,
    card_loader: false,
    appliedFilters: {},
    filters: {},
    xCpuData: [],
    yCpuData: [],
    xMemoryData: [],
    yMemoryData: [],
    metricsRequestrateData: "0",
    metricsSuccessData: "0",
    metrics5xxData: "0",
    metrics4xxData: "0",
    metricsReqpersecData: [],
    xLatencyData: [],
    latencyData: [],
    xRequestSecData: [],
    yRequestSecData: [],
    memoryData:[],
    cpuData:[]
  };
  cpuDateFormat = "hh:mm a";
  memoryDateFormat = "hh:mm a";

  constructor(props) {
    super(props);
    this.getDetails = this.getDetails.bind(this);
    this.getLogs = this.getLogs.bind(this);
  }

  componentDidMount() {
    if (this.props.match) {
      Mixpanel.track("Deployment Details Page", {
        deployment_name: this.props.match.params.name,
      });
    }
    this.getDetails();
  }

  handleDownloadButtonClick = (id) => {
    const CancelToken = Axios.CancelToken;
    this.downloadCancelToken = CancelToken.source();
    this.props.downloadPrediction(
      id,
      this.props.match.params.name,
      this.monitorProgress,
      this.downloadCancelToken
    );
  };

  getFilterTab = (data) => {
    if (data) {
      switch (typeof data) {
        case "object":
          if (data.value) {
            return `${data.key}-${data.value}`;
          }
          if (data.key == "isBetween") {
            return `${data.key}-${data.value1}&${data.value2}`;
          }
          if (data.value1) {
            return `${data.key}-${data.value1}`;
          }
          break;
        default:
          return data;
      }
    }
  };

  monitorProgress = (data) => {
    if (typeof data === "object")
      this.setState({
        dataLoaded: data.loaded,
        dataTotal: data.total,
        loadPercent: Math.floor((data.loaded / data.total) * 100),
      });
    else
      this.setState(
        {
          dataLoaded: 0,
          dataTotal: 0,
          loadPercent: 100,
        },
        this.props.alertMsg("Download Failed - " + data, "error")
      );
  };

  handleCancel = () => {
    this.downloadCancelToken.cancel("Operation cancelled by user");
    this.setState({
      loadPercent: 100,
    });
  };

  loadBatchScoreRuns() {
    const { deployment } = this.props;
    var obj = {
      startDate: null,
      endDate: null,
      name: deployment.metadata.name,
    };
    getBatchScoringRuns(this.props.token, obj)
      .then((res) => {
        if (res.status === 200) {
          this.setState({
            batchRuns: res.data,
          });
        }
      })
      .catch((err) => {});
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return {
      logs: nextProps.deployment_log,
    };
  }

  getSecrets = () => {
    const { deployment } = this.props;
    getAllSecrets(this.props.token, {
      project_name: deployment.metadata.labels["aiops.predera.com/projectName"],
      project_id: deployment.metadata.labels["aiops.predera.com/projectId"],
    })
      .then((res) => {
        if (res.status === 200) {
          this.setState({
            secrets: res.data,
          });
        }
      })
      .catch((err) => {});
  };

  handleChange = (e) => {
    const { name, value } = e.target;
    const { form, formErrors } = this.state;
    let formObj = {};

    formObj = {
      ...form,
      [name]: value,
    };

    this.setState({ form: formObj }, () => {
      if (!Object.keys(formErrors).includes(name)) return;
      let formErrorsObj = {};
      const errorMsg = this.validateField(name, value);
      formErrorsObj = { ...formErrors, [name]: errorMsg };
      this.setState({ formErrors: formErrorsObj });
    });
  };

  validateField = (name, value) => {
    let errorMsg = null;
    switch (name) {
      case "outputBucket":
        if (!value) errorMsg = "Please Enter Bucket. ";

        break;
      case "outputKey":
        if (!value) errorMsg = "Please Enter Key.";
        break;

      case "inputBucket":
        if (!value) errorMsg = "Please Enter Bucket. ";

        break;
      case "inputKey":
        if (!value) errorMsg = "Please Enter Key.";
        break;

      default:
        break;
    }
    return errorMsg;
  };

  validateForm = (form, formErrors, validateFunc) => {
    const errorObj = {};
    Object.keys(formErrors).map((x) => {
      let refValue = null;
      const msg = validateFunc(x, form[x], refValue);
      if (msg) errorObj[x] = msg;
    });
    return errorObj;
  };

  handleSubmit = () => {
    const { form, formErrors } = this.state;
    const { deployment } = this.props;
    const errorObj = this.validateForm(form, formErrors, this.validateField);
    if (Object.keys(errorObj).length !== 0) {
      this.setState({ formErrors: { ...formErrors, ...errorObj } });
      return false;
    }
    const inputObj = {
      bucket: form.inputBucket,
      key: form.inputKey,
      aws_secret: form.inputAwsSecret,
    };
    const outputObj = {
      bucket: form.outputBucket,
      key: form.outputKey,
      aws_secret: form.outputAwsSecret,
    };
    const obj = {
      projectName: deployment.metadata.labels["aiops.predera.com/projectName"],
      projectId: deployment.metadata.labels["aiops.predera.com/projectId"],
      inputs: inputObj,
      outputs: outputObj,
    };
    submitBatchScore(this.props.token, this.props.match.params.name, obj)
      .then((res) => {
        this.props.alertMsg(
          "Batch Preddiction Submitted Successfully",
          "success"
        );
        this.loadBatchScoreRuns();
      })
      .catch((error) => {
        this.setState({
          formError: true,
        });
      });
  };

  handleScaling = () => {
    this.setState({
      loader: true,
    });
    const { scale } = this.state;
    const { deployment } = this.props;
    const details = {
      replicas: scale,
      name: deployment.metadata.name,
    };
    this.props.scaleDeployment(
      details,
      () => {
        this.setState({
          loader: false,
        });
        this.getDetails(1);
        MicroModal.close("scaling-modal");
      },
      (err) => {
        this.setState({
          loader: false,
        });
        this.props.alertMsg(err, "error");
        MicroModal.close("scaling-modal");
      }
    );
  };

  handleTabChange = (e) => {
    const target = e.target;
    const name = target.name;
    const value = /NOT_SET/gi.test(target.value) ? "" : target.value;

    this.setState({
      [name]: value,
    });
  };

  createGraph = (data1, name) => {
    const data = data1.data.result[0].values;
    if (name === "memory") {
      // const xAxisMemoryData = data.map((item) => {
      //   let memoryDate = new Date(item[0] * 1000);
      //   return moment(memoryDate).format("HH:mm");
      // });
      // const yAxisMemoryData = data.map((element) => (element[1] / 1000)/1000);

      // this.setState({ xMemoryData: xAxisMemoryData });
      this.setState({ memoryData: data });
    } else if (name === "cpu") {
      // const xAxisCpuData = data.map((item) => {
      //   let cpuDate = new Date(item[0] * 1000);
      //   return moment(cpuDate).format("HH:mm");
      // });
      // const yAxisCpuData = data.map((element) => element[1] * 1000);
      // this.setState({ xCpuData: xAxisCpuData });
      this.setState({ cpuData: data });
    } else if (name === "getMetricsRequestrate") {
      let res = data[data.length - 1];
      this.setState({ metricsRequestrateData: res[1] });
    } else if (name === "metricsSuccessData" && data.length > 0) {
      let res = data[data.length - 1];

      this.setState({ metricsSuccessData: res[1].toFixed(2) });
    } else if (name === "metrics5xxData" && data.length > 0) {
      let res = data[data.length - 1];

      this.setState({ metrics5xxData: res[1].toFixed(2) });
    } else if (name === "metrics4xxData" && data.length > 0) {
      let res = data[data.length - 1];
      this.setState({ metrics4xxData: res[1].toFixed(2) });
    } else {
      const xAxisCpuData = data.map((item) => {
        let cpuDate = new Date(item[0] * 1000);
        return moment(cpuDate).format("HH:mm");
      });
      const yAxisCpuData = data.map((element) => element[1] * 1000);
      this.setState({ xRequestSecData: xAxisCpuData });
      this.setState({ yRequestSecData: yAxisCpuData });
      this.setState({ metricsReqpersecData: data });
    }
  };

  setActive = (e) => {
    const target = e.target;
    target.parentElement.querySelectorAll("li").forEach((e) => {
      e.className = "react-tabs__tab";
    });
    target.className = "react-tabs__tab react-tabs__tab--selected";
  };

  getColor(type) {
    const status = {
      Creating: "info",
      Failed: "failed",
      Available: "success",
    };
    const color = status[type] ? status[type] : "primary";
    return color;
  }

  getDetails(silent = 0) {
    this.setState({
      reload: true,
    });
    const name = this.props.match && this.props.match.params.name;
    if (name)
      this.props.getDeploymentDetail(
        name,
        () => {
          this.getLogs();
          this.getMetrix("15 MINUTES");
          //  this.getMemory("15 MINUTES");
          this.loadBatchScoreRuns();
          this.setState({
            reload: false,
          });
        },
        () => {
          this.setState({
            deploymentErr: "No deployment data!",
            reload: false,
          });
        },
        silent
      );
  }
  getParams = (duration) => {
    let params = {
      start: "",
      end: "",
      step: "",
      name: this.props.deployment.metadata.name,
    };
    switch (duration) {
      case "15 MINUTES": {
        const currentDate = new Date();
        const fiftenMins = new Date(
          currentDate.getTime() - 1000 * 15 * 60
        ).toISOString();
        params.start = currentDate.toISOString();
        params.end = fiftenMins;
        params.step = "1m";

        return params;
      }
      case "1 HOUR": {
        const currentDate = new Date();
        const hourAgo = new Date(
          currentDate.getTime() - 1000 * 60 * 60
        ).toISOString();
        params.start = currentDate.toISOString();
        params.end = hourAgo;
        params.step = "5m";

        return params;
      }
      case "24 HOURS": {
        const currentDate = new Date();
        const dayAgo = new Date(
          currentDate.getTime() - 1000 * 24 * 60 * 60
        ).toISOString();
        params.start = currentDate.toISOString();
        params.end = dayAgo;
        params.step = "1h";

        return params;
      }
      case "7 DAYS": {
        const currentDate = new Date();
        const sevenDaysAgo = new Date(
          currentDate.getTime() - 1000 * 7 * 24 * 60 * 60
        ).toISOString();

        params.start = currentDate.toISOString();
        params.end = sevenDaysAgo;
        params.step = "24h";

        return params;
      }
      case "30 DAYS": {
        const currentDate = new Date();
        const thirtyDaysAgo = new Date(
          currentDate.getTime() - 1000 * 30 * 24 * 60 * 60
        ).toISOString();
        params.step = "24h";

        params.start = currentDate.toISOString();
        params.end = thirtyDaysAgo;

        return params;
      }
      default: {
        const currentDate = new Date();
        const fiftenMins = new Date(
          currentDate.getTime() - 1000 * 15 * 60
        ).toISOString();
      }
    }
  };

  getMetrix = (duration) => {
    this.setState({
      metrics4xxData: "0",
      metricsSuccessData: "0",
      metrics5xxData: "0",
    });
    let data = this.getParams(duration);

    this.props.getMetricsRequestrate(data, () => {
      this.createGraph(
        this.props.metricsRequestrateData,
        "getMetricsRequestrate"
      );
    });
    this.props.getMetricsSuccess(data, () => {
      this.createGraph(this.props.metricsSuccessData, "metricsSuccessData");
    });
    this.props.getMetrics5xx(data, () => {
      this.createGraph(this.props.metrics5xxData, "metrics5xxData");
    });
    this.props.getMetrics4xx(data, () => {
      this.createGraph(this.props.metrics4xxData, "metrics4xxData");
    });
    this.props.getMetricsReqpersec(data, () => {
      this.createGraph(this.props.metricsReqpersecData, "metricsReqpersecData");
    });
    metricsLatency(this.props.token, data).then((data) => {
      //  this.createGraph(data, "metricsLatency");
      this.setState({ latencyData: data.data });
    });
    this.props.getMetricsLatency(data, () => {});
    this.getCpu(duration);
    this.getMemory(duration);
  };

  getCpu = (duration) => {
    let data = this.getParams(duration);

    this.props.getDeploymentsCpu(data, () => {
      this.createGraph(this.props.deployment_cpu, "cpu");
    });
  };

  getMemory = (duration) => {
    let data = this.getParams(duration);

    this.props.getDeploymentsMemory(data, () => {
      this.createGraph(this.props.deployment_memory, "memory");
    });
  };

  getLogs(rep = 0, clear = 1) {
    const name = this.props.deployment.metadata.name;
    if (clear)
      this.setState({
        logs: "",
      });
    this.props.getDeploymentLogs(
      { name: name, replica: rep },
      () => {
        this.setState({
          index: rep,
          updated: this.state.updated + 1,
        });
      },
      () => {
        this.setState({
          index: rep,
          updated: this.state.updated + 1,
        });
      }
    );
  }

  getAvailableReplicas = (deployment) => {
    var keys = Object.keys(deployment.status.deploymentStatus),
      item = deployment.status.deploymentStatus[keys[0]];
    if (item.availableReplicas) {
      return item.availableReplicas;
    } else {
      return 0;
    }
  };

  getDuration(e) {
    if (e.started_date && e.ended_date) {
      var date1 = new Date(e.started_date);
      var date2 = new Date(e.ended_date);
      var Difference_In_Time = date2.getTime() - date1.getTime();
      var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
      return ~~Difference_In_Days;
    } else {
      var date1 = new Date(e.requested_date);
      var date2 = new Date();
      var Difference_In_Time = date2.getTime() - date1.getTime();
      var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
      return ~~Difference_In_Days;
    }
  }

  getDeploymentDetails = () => {
    const { deployment } = this.props;
    const {
      updated,
      loader,
      importFileType,
      filename,
      fileError,
      loadPercent,
      filters,
      appliedFilters,
    } = this.state;
    return (
      <div className="m-details-body">
        <div className="m-info-table mlr-30 ml-0">
          <table className="table data-table no-border modeldetails-table">
            <thead>
              <tr>
                <td>Name</td>
                <td className="text-center">Status</td>
                <td>Created date</td>
                <td>Type</td>
                <td>Replicas</td>
                <td>Available replicas</td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{deployment.metadata.name}</td>
                <td className="text-center">
                  {deployment.status && (
                    <div>
                      {deployment.status.description ? (
                        <Tooltip title={deployment.status.description}>
                          <label
                            className={`model-item-status status-${this.getColor(
                              deployment.status.state
                            )}`}
                          >
                            {deployment.status.state}
                          </label>
                        </Tooltip>
                      ) : (
                        <label
                          className={`model-item-status status-${this.getColor(
                            deployment.status.state
                          )}`}
                        >
                          {deployment.status.state}
                        </label>
                      )}
                    </div>
                  )}
                </td>
                <td>
                  {moment(deployment.metadata.creationTimestamp).format("LL")}
                </td>
                <td>{getDeployementType(deployment)}</td>
                <td>
                  {deployment.spec &&
                    deployment.spec.predictors &&
                    deployment.spec.predictors[0].replicas}
                </td>
                <td>{this.getAvailableReplicas(deployment)}</td>
              </tr>
            </tbody>
          </table>
          <hr />
          <table className="table data-table no-border modeldetails-table">
            <thead>
              <tr>
                <td>Project</td>
                <td>Model</td>
                <td>Run</td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <Link to={"/project_details"}>
                    {
                      deployment.metadata.labels[
                        "aiops.predera.com/projectName"
                      ]
                    }
                  </Link>
                </td>
                {deployment.metadata.labels["aiops.predera.com/modelId"] ? (
                  <td>
                    <Link
                      to={`/registered-model-versions/${deployment.metadata.labels["aiops.predera.com/modelId"]}`}
                    >
                      {
                        deployment.metadata.labels[
                          "aiops.predera.com/modelName"
                        ]
                      }
                    </Link>
                  </td>
                ) : (
                  <td>NOT_AVAILABLE</td>
                )}
                {deployment.metadata.labels[
                  "aiops.predera.com/mlworkflowrun"
                ] ? (
                  <td>
                    <Link
                      to={`/run-history/${deployment.metadata.labels["aiops.predera.com/mlworkflowrun"]}`}
                    >
                      {
                        deployment.metadata.labels[
                          "aiops.predera.com/mlworkflowrun"
                        ]
                      }
                    </Link>
                  </td>
                ) : (
                  <td>NOT_AVAILABLE</td>
                )}
              </tr>
            </tbody>
          </table>
          <hr />
          <table className="table data-table no-border modeldetails-table">
            <thead>
              <tr>
                <td>REST API Endpoint (Internal)</td>
                <td>REST API Endpoint (External)</td>
              </tr>
            </thead>
            <tbody>
              <tr>
                {deployment.status.address.url ? (
                  <td>
                    <Copy>{`${deployment.status.address.url}`}</Copy>
                  </td>
                ) : (
                  <td>NOT_AVAILABLE</td>
                )}
                {deployment.status.address.url ? (
                  <td>
                    <Copy>{`${window.location.origin}/models/${deployment.metadata.name}/api/v1.0/predictions`}</Copy>
                  </td>
                ) : (
                  <td>NOT_AVAILABLE</td>
                )}
              </tr>
            </tbody>
          </table>
        </div>
        <hr />
        <div>
          <ul className="inline-tabs tabs-blue pad-50">
            <li
              className={`react-tabs__tab${this.state.tab === 0 &&
                "--selected"}`}
              onClick={() => {
                this.setState({ tab: 0 });
              }}
            >
              Graph
            </li>
            <li
              className={`react-tabs__tab${this.state.tab === 1 &&
                "--selected"}`}
              onClick={() => {
                this.setState({ tab: 1 });
              }}
            >
              Test APIs
            </li>

            <li
              className={`react-tabs__tab${this.state.tab === 3 &&
                "--selected"}`}
              onClick={() => {
                this.setState({ tab: 3 });
              }}
            >
              Logs
            </li>

            {this.state.showPrediction &&
              deployment.status.state == "Available" && (
                <li
                  className={`react-tabs__tab${this.state.tab === 4 &&
                    "--selected"}`}
                  onClick={() => {
                    this.setState({ tab: 4 });
                    this.getSecrets();
                  }}
                >
                  Batch Predictions
                </li>
              )}
            <li
              className={`react-tabs__tab${this.state.tab === 2 &&
                "--selected"}`}
              onClick={() => {
                this.setState({ tab: 2 });
              }}
            >
              Metrics
            </li>
          </ul>
        </div>
        <hr />
        <div>
          {this.state.tab === 4 && (
            <div>
              <BatchScoringComponent deployment={this.props.deployment} />
            </div>
          )}
          {this.state.tab === 3 && (
            <div>
              <div className="m-info-table mlr-30">
                <h4>Logs</h4>
                <DeploymentLogs
                  logs={this.state.logs}
                  updated={updated}
                  getLogs={this.getLogs}
                  index={this.state.index}
                  replicas={
                    deployment.spec &&
                    deployment.spec.predictors &&
                    deployment.spec.predictors[0].replicas
                  }
                />
              </div>
            </div>
          )}
          {this.state.tab === 2 && (
            <div className="m-info-table mlr-30" style={{ minHeight: 452 }}>
              <div className="mt-lg pd">
                <h4> Metrics</h4>
                <div style={{ float: "right" }}>
                  <ul className="inline-tabs tabs-gray mb-0 mt-lg">
                    <li
                      className="react-tabs__tab react-tabs__tab--selected"
                      onClick={(e) => {
                        this.setActive(e);
                        this.cpuDateFormat = "hh:mm a";
                        this.getMetrix("15 MINUTES");
                      }}
                    >
                      15 MINUTES
                    </li>
                    <li
                      className="react-tabs__tab"
                      onClick={(e) => {
                        this.setActive(e);
                        this.cpuDateFormat = "hh:mm a";
                        this.getMetrix("1 HOUR");
                      }}
                    >
                      1 HOUR
                    </li>
                    <li
                      className="react-tabs__tab"
                      onClick={(e) => {
                        this.setActive(e);
                        this.cpuDateFormat = "hh:mm a";
                        this.getMetrix("24 HOURS");
                      }}
                    >
                      24 HOURS
                    </li>
                    <li
                      className="react-tabs__tab"
                      onClick={(e) => {
                        this.setActive(e);
                        this.cpuDateFormat = "MMM dd";
                        this.getMetrix("7 DAYS");
                      }}
                    >
                      7 DAYS
                    </li>
                    <li
                      className="react-tabs__tab"
                      onClick={(e) => {
                        this.setActive(e);
                        this.cpuDateFormat = "MMM dd";
                        this.getMetrix("30 DAYS");
                      }}
                    >
                      30 DAYS
                    </li>
                  </ul>
                </div>
                <div className="row mt-5">
                  <div className="col-3">
                    <Card className="mt-5">
                      <CardContent>
                        <Typography
                          style={{ textAlign: "center" }}
                          color="textSecondary"
                          gutterBottom
                        >
                          Total Requests
                        </Typography>
                        <Typography
                          style={{ textAlign: "center" }}
                          variant="h5"
                          className="mt-2"
                          component="h2"
                        >
                          {this.state.metricsRequestrateData}
                        </Typography>

                        <Typography
                          style={{ textAlign: "center" }}
                          className="mt-2"
                          component="p"
                        >
                          {/* <LinearProgress variant="determinate" value={100} /> */}
                        </Typography>
                      </CardContent>
                    </Card>
                  </div>
                  <div className="col-3">
                    <Card className="mt-5">
                      <CardContent>
                        <Typography
                          color="textSecondary"
                          gutterBottom
                          style={{ textAlign: "center" }}
                        >
                          Success
                        </Typography>
                        <Typography
                          style={{ textAlign: "center" }}
                          className="mt-2"
                          variant="h5"
                          component="h2"
                        >
                          {this.state.metricsSuccessData} %
                        </Typography>

                        <Typography className="mt-2" component="p">
                          <LinearProgress
                            variant="determinate"
                            value={this.state.metricsSuccessData}
                          />
                        </Typography>
                      </CardContent>
                    </Card>
                  </div>
                  <div className="col-3">
                    <Card className="mt-5">
                      <CardContent>
                        <Typography
                          color="textSecondary"
                          gutterBottom
                          style={{ textAlign: "center" }}
                        >
                          Invalid Requests
                        </Typography>
                        <Typography
                          style={{ textAlign: "center" }}
                          className="mt-2"
                          variant="h5"
                          component="h2"
                        >
                          {this.state.metrics4xxData} %
                        </Typography>

                        <Typography className="mt-2" component="p">
                          <LinearProgress
                            variant="determinate"
                            value={this.state.metrics4xxData}
                          />
                        </Typography>
                      </CardContent>
                    </Card>
                  </div>
                  <div className="col-3">
                    <Card className="mt-5">
                      <CardContent>
                        <Typography
                          color="textSecondary"
                          gutterBottom
                          style={{ textAlign: "center" }}
                        >
                          Server Errors
                        </Typography>
                        <Typography
                          style={{ textAlign: "center" }}
                          className="mt-2"
                          variant="h5"
                          component="h2"
                        >
                          {this.state.metrics5xxData} %
                        </Typography>

                        <Typography className="mt-2" component="p">
                          <LinearProgress
                            variant="determinate"
                            value={this.state.metrics5xxData}
                          />
                        </Typography>
                      </CardContent>
                    </Card>
                  </div>
                </div>
                <div className="row mt-5">
                  <div className="col-6">
                    <h4>Requests/Sec</h4>

                    <RequestSecChart
                      xCpuData={this.state.xRequestSecData}
                      yCpuData={this.state.yRequestSecData}
                    />
                   
                  </div>
                  <div className="col-6">
                    <h4>Latency</h4>

                      {this.state.latencyData.length>0 && (<LatencyChart
                      latencyData={this.state.latencyData}
                      />)}
                    
                  </div>
                </div>
                <div className="inline-block" style={{ width: "49%" }}>
                  <h4>CPU Usage</h4>
                 
                  <CpuChart
                   cpuData={this.state.cpuData}
                  />
                </div>
                <div className="inline-block" style={{ width: "49%" }}>
                  <h4>Memory Usage</h4>
              
                  <MemoryChart
                   memoryData={this.state.memoryData}
                  />
                </div>
              </div>
            </div>
          )}
          {this.state.tab === 1 && (
            <div className="m-info-table mlr-30">
              <h4>Test Prediction APIs</h4>
              {deployment.status.address.url ? (
                <TestApi
                  urls={[
                    `${deployment.status.address.url}`,
                    `${window.location.origin}/models/${deployment.metadata.name}/api/v1.0/predictions`,
                  ]}
                  name={deployment.metadata.name}
                />
              ) : (
                <div>NOT AVAILABLE</div>
              )}
            </div>
          )}
          {this.state.tab === 0 && (
            <div className="m-info-table mlr-30">
              {deployment.spec.predictors &&
              deployment.spec.predictors &&
              deployment.spec.predictors[0] ? (
                <DeploymentGraph
                  deployment={deployment}
                  key={deployment.metadata.name}
                />
              ) : (
                <div className="empty-items-box">
                  <h4>No Graph data found!</h4>
                </div>
              )}
            </div>
          )}
        </div>
      </div>
    );
  };

  deleteDeployments = (name = this.props.match.params.name) => {
    const { deployment } = this.props;

    this.props.deleteDeployment(
      name,
      () => {
        this.props.getDeployments({
          projectID: deployment.metadata.labels["aiops.predera.com/projectId"],
        });
        this.props.history.goBack();
      },
      () => {
        this.props.alertMsg(
          "Unable to delete deployment, please try again!",
          "error"
        );
      }
    );
  };
  startDeployment = (name = this.props.match.params.name) => {
    this.props.startDeployment(
      name,
      () => {
        this.props.alertMsg(
          "Deployment has been started successfully!",
          "success"
        );
      },
      () => {
        this.props.alertMsg(
          "Unable to start deployment, please try again!",
          "error"
        );
      }
    );
  };
  stopDeployment = (name = this.props.match.params.name) => {
    this.props.stopDeployment(
      name,
      () => {
        this.props.alertMsg(
          "Deployment has been stopped successfully!",
          "success"
        );
      },
      () => {
        this.props.alertMsg(
          "Unable to stop deployment, please try again!",
          "error"
        );
      }
    );
  };
  reStartDeployment = (name = this.props.match.params.name) => {
    const { deployment } = this.props;

    this.props.reStartDeployment(
      name,
      () => {
        this.props.alertMsg(
          "Deployment has been restarted successfully!",
          "success"
        );
      },
      () => {
        this.props.alertMsg(
          "Unable to start deployment, please try again!",
          "error"
        );
      }
    );
    this.startDeployment();
    this.getDetails();
  };
  render() {
    const { deployment } = this.props;
    const { loader, scale, reload } = this.state;
    if (typeof deployment != "object" || !Object.keys(deployment).length) {
      return typeof deployment != "object" ? (
        <div className="h180 content-center">
          Deployment data not available!
        </div>
      ) : (
        <Loader1 />
      );
    }
    return (
      <div>
        {this.props.match ? (
          <div className="main-container">
            <div className="main-body" style={{ overflow: "hidden" }}>
              <div className="content-wraper-sm">
                <div className="m-details-card">
                  <div className="m-details-card-head pl-4 mr-5">
                    {/* <div
                      className="back-action clickable"
                      onClick={() => {
                        this.state.prevUrl
                          ? this.props.history.push(this.state.prevUrl)
                          : this.props.history.goBack();
                      }}
                    >
                      <img src={LeftArrow} alt="left arrow" />
                    </div> */}
                    <div className="m-title-info">
                      <h4>{this.props.match.params.name}</h4>
                      <small>DEPLOYMENT</small>
                    </div>
                    <div className="m-tech-stack">
                      {deployment.status && deployment.status.description && (
                        <Tooltip title={deployment.status.description}>
                          <span className="insight-label mr content-none clickable">
                            <i className="fa fa-lightbulb"></i>
                          </span>
                        </Tooltip>
                      )}

                      {deployment.status &&
                        deployment.status.state == "Available" && (
                          <button
                            type="button"
                            className="btn btn-sm btn-info mr"
                            title="Restart Deployment"
                            onClick={() => {
                              confirm(
                                "Are you sure?",
                                "Restart Deployment"
                              ).then(() => {
                                this.reStartDeployment();
                              });
                            }}
                          >
                            <i className="fa fa-sync" />
                          </button>
                        )}
                      {deployment.status &&
                        deployment.status.state == "Available" && (
                          <button
                            type="button"
                            className="btn btn-sm btn-danger mr"
                            title="Stop Deployment"
                            onClick={() => {
                              confirm("Are you sure?", "Stop Deployment").then(
                                () => {
                                  this.stopDeployment();
                                }
                              );
                            }}
                          >
                            <i className="fa fa-stop" />
                          </button>
                        )}
                      {deployment.status &&
                        deployment.status.state == "Stopped" && (
                          <button
                            type="button"
                            title="Start Deployment"
                            className="btn btn-sm btn-primary mr"
                            onClick={() => {
                              confirm("Are you sure?", "Start").then(() => {
                                this.startDeployment();
                              });
                            }}
                          >
                            <i className="fa fa-play" />
                          </button>
                        )}

                      <button
                        type="button"
                        className="btn btn-sm btn-default mr"
                        onClick={() => {
                          MicroModal.show("scaling-modal");
                        }}
                      >
                        Scale
                      </button>
                      <button
                        type="button"
                        className="btn btn-sm btn-danger mr"
                        onClick={() => {
                          confirm("Are you sure?", "Delete").then(() => {
                            this.deleteDeployments();
                          });
                        }}
                      >
                        <i className="fa fa-trash" />
                      </button>
                    </div>
                  </div>
                  {this.getDeploymentDetails()}
                </div>
              </div>
            </div>
          </div>
        ) : (
          this.getDeploymentDetails()
        )}
        <div
          className="modal micromodal-slide"
          id="scaling-modal"
          aria-hidden="true"
        >
          <div className="modal__overlay" tabIndex="-1" data-micromodal-close>
            <div
              className="modal__container"
              role="dialog"
              aria-modal="true"
              aria-labelledby="secret-modal-title"
            >
              <header className="modal__header">
                <h2 className="modal__title">Scale Deployment</h2>
                <button
                  type="button"
                  className="modal__close"
                  aria-label="Close modal"
                  data-micromodal-close
                />
              </header>
              <main className="modal__content">
                <div className="modal-filter-form">
                  <div className="modal-body">
                    <div className="row mt">
                      <div className="col-sm-3">
                        <span className="inline-block pd-tb">Replicas:</span>
                      </div>
                      <div className="col-sm-9">
                        <input
                          id={"scale"}
                          name="scale"
                          min={1}
                          type="number"
                          placeholder={
                            (deployment.spec &&
                              deployment.spec.predictors &&
                              deployment.spec.predictors[0].replicas) ||
                            0
                          }
                          onChange={(e) => {
                            this.setState({
                              scale: e.target.value < 1 ? 1 : e.target.value,
                            });
                          }}
                          className="form-control"
                          value={scale}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="modal-footer">
                    {loader && <CircularProgress size={20} disableShrink />}
                    <button
                      onClick={() => MicroModal.close("scaling-modal")}
                      type="button"
                      className="btn btn-secondary"
                    >
                      Close
                    </button>
                    <button
                      type="button"
                      className="btn btn-primary"
                      onClick={this.handleScaling}
                    >
                      Scale
                    </button>
                  </div>
                </div>
              </main>
            </div>
          </div>
        </div>
        <div
          className="modal micromodal-slide"
          id="batch-scoring-modal"
          aria-hidden="true"
        >
          <div className="modal__overlay" tabIndex="-1" data-micromodal-close>
            <div
              className="modal__container"
              role="dialog"
              aria-modal="true"
              aria-labelledby="secret-modal-title"
            >
              <header className="modal__header">
                <h2 className="modal__title">Batch Predictions</h2>
                <button
                  type="button"
                  className="modal__close"
                  aria-label="Close modal"
                  data-micromodal-close
                />
              </header>
              <main className="modal__content mt-0">
                <div className="modal-filter-form">
                  <div className="modal-body">
                    <div className="mt-48">
                      <strong>S3 Input File</strong>
                    </div>
                    <div className="row mt-15">
                      <div className="col-sm-6">
                        <TextField
                          label="Bucket"
                          fullWidth
                          name={"inputBucket"}
                          value={this.state.form.inputBucket}
                          onChange={this.handleChange}
                          onBlur={this.handleChange}
                        />
                        {this.state.formErrors.inputBucket && (
                          <span className="err">
                            {this.state.formErrors.inputBucket}
                          </span>
                        )}
                      </div>
                      <div className="col-sm-6">
                        <TextField
                          label="Key"
                          fullWidth
                          name={"inputKey"}
                          value={this.state.form.inputKey}
                          onChange={this.handleChange}
                          onBlur={this.handleChange}
                        />
                        {this.state.formErrors.inputKey && (
                          <span className="err">
                            {this.state.formErrors.inputKey}
                          </span>
                        )}
                      </div>

                      <div className="col-sm-12">
                        <TextField
                          label="Secret"
                          name={"inputAwsSecret"}
                          select
                          fullWidth
                          value={this.state.form.inputAwsSecret}
                          onChange={this.handleChange}
                          onBlur={this.handleChange}
                        >
                          <MenuItem value={""}>Select Secret</MenuItem>
                          {this.state.secrets.map((secret) => (
                            <MenuItem key={secret.name} value={secret.name}>
                              {secret.name}
                            </MenuItem>
                          ))}
                        </TextField>
                        {this.state.formErrors.inputAwsSecret && (
                          <span className="err">
                            {this.state.formErrors.inputAwsSecret}
                          </span>
                        )}
                      </div>
                    </div>
                    <div className="mt-48">
                      <strong>S3 Output File</strong>
                    </div>
                    <div className="row">
                      <div className="col-sm-6">
                        <TextField
                          label="Bucket"
                          fullWidth
                          name={"outputBucket"}
                          value={this.state.form.outputBucket}
                          onChange={this.handleChange}
                          onBlur={this.handleChange}
                        />
                        {this.state.formErrors.outputBucket && (
                          <span className="err">
                            {this.state.formErrors.outputBucket}
                          </span>
                        )}
                      </div>
                      <div className="col-sm-6">
                        <TextField
                          label="Key"
                          fullWidth
                          name={"outputKey"}
                          value={this.state.form.outputKey}
                          onChange={this.handleChange}
                          onBlur={this.handleChange}
                        />
                        {this.state.formErrors.outputKey && (
                          <span className="err">
                            {this.state.formErrors.outputKey}
                          </span>
                        )}
                      </div>

                      <div className="col-sm-12">
                        <TextField
                          label="Secret"
                          name={"outputAwsSecret"}
                          select
                          value={this.state.form.outputAwsSecret}
                          onChange={this.handleChange}
                          fullWidth
                        >
                          <MenuItem value={""}>Select Secret</MenuItem>
                          {this.state.secrets.map((secret) => (
                            <MenuItem key={secret.name} value={secret.name}>
                              {secret.name}
                            </MenuItem>
                          ))}
                        </TextField>
                        {this.state.formErrors.outputAwsSecret && (
                          <span className="err">
                            {this.state.formErrors.outputAwsSecret}
                          </span>
                        )}
                      </div>
                    </div>
                    {this.state.formError ? (
                      <div className="row mt-15">
                        <div className="col-sm-12">
                          <Tooltip title="Double click to dismiss">
                            <div
                              className="alert alert-danger f-13"
                              onDoubleClick={() => {
                                this.setState({
                                  formError: false,
                                });
                              }}
                            >
                              <strong>Error: </strong>
                              An error occurred while submitting the Batch
                              Predictions, please try again.
                            </div>
                          </Tooltip>
                        </div>
                      </div>
                    ) : (
                      <div></div>
                    )}
                  </div>
                  <div className="modal-footer">
                    <button
                      onClick={() => MicroModal.close("batch-scoring-modal")}
                      type="button"
                      className="btn btn-secondary mt-15"
                    >
                      Close
                    </button>
                    <button
                      type="button"
                      className="btn btn-primary mt-15"
                      onClick={this.handleSubmit}
                    >
                      Submit
                    </button>
                  </div>
                </div>
              </main>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    deployments: state.deployments.deployments,
    deployment: state.deployments.deployment,
    deployment_log: state.deployments.deployment_log,
    deployment_cpu: state.deployments.deployment_cpu,
    deployment_memory: state.deployments.deployment_memory,
    token: state.auth.token,
    metricsRequestrateData: state.deployments.metricsRequestrateData,
    metricsSuccessData: state.deployments.metricsSuccessData,
    metrics5xxData: state.deployments.metrics5xxData,
    metrics4xxData: state.deployments.metrics4xxData,
    metricsReqpersecData: state.deployments.metricsReqpersecData,
    metricsLatency: state.deployments.metricsReqpersecData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      getDeployments,
      getDeploymentDetail,
      getDeploymentLogs,
      getDeploymentsCpu,
      getDeploymentsMemory,
      deleteDeployment,
      scaleDeployment,
      alertMsg,
      downloadPrediction,
      startDeployment,
      stopDeployment,
      reStartDeployment,
      getMetricsRequestrate,
      getMetricsSuccess,
      getMetrics5xx,
      getMetrics4xx,
      getMetricsReqpersec,
      getMetricsLatency,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Deployment);
