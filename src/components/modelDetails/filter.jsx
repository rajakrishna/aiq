import React, { Component } from "react";

class Filter extends Component {
  constructor(props) {
    super(props);
    this.state = { filters: props.filters };
  }
  UNSAFE_componentWillReceiveProps(props) {
    this.setState({ filters: props.filters });
  }
  handleApplyFilters = () => {
    this.props.applyFilters();
  };
  handleResetFilters = () => {
    this.props.resetFilters();
  };

  render() {
    return (
      <div
        className="modal micromodal-slide"
        id="prediction-filter-modal"
        aria-hidden="true"
      >
        <div className="modal__overlay" tabIndex="-1" data-micromodal-close>
          <div
            className="modal__container"
            role="dialog"
            aria-modal="true"
            aria-labelledby="modal-1-title"
          >
            <header className="modal__header">
              <h2 className="modal__title" id="modal-1-title">
                Filter Predictions
              </h2>
              <button
                className="modal__close"
                aria-label="Close modal"
                data-micromodal-close
              />
            </header>
            <main className="modal__content" id="modal-1-content">
              <div className="modal-filter-form">{this.props.children}</div>
            </main>
            <footer className="modal__footer filter-form-actions">
              <button
                className="btn btn-primary btn-rounded"
                data-micromodal-close
                onClick={this.handleApplyFilters}
              >
                APPLY FILTERS
              </button>
              <button
                className="btn btn-default btn-rounded"
                data-micromodal-close
                aria-label="Close this dialog window"
                onClick={this.handleResetFilters}
              >
                RESET FILTERS
              </button>
            </footer>
          </div>
        </div>
      </div>
    );
  }
}

export default Filter;
