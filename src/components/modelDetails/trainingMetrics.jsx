import React, { Component } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { connect } from "react-redux";
import ModelFeatureSignificance from "../chart/ModelFeatureSignificance";
import ModelConfusionMatrix from "../chart/ModelConfusionMatrix";
import ModelROCCurve from "../chart/ModelROCCurve";
import ModelLiftCurve from "../chart/ModelLiftCurve";
import ModelScatterPlot from "../chart/ModelScatterPlot";
import ModelResidualPlot from "../chart/ModelResidualPlot";
import LogLoss from "../chart/LogLoss";

const pathname = [
  '',
  'Hyper-parameters',
  'Feature-significance',
  'Performance-metrics',
  'Confusion-metrics'
]

class TrainingMatrix extends Component {
  state = {
    currentUrl: "",
    currentIndex: ""
  };

  graphClasses = {
    train_test_ratio:"progress-bar progress-bar-blue",
    recall:"progress-bar progress-bar-green",
    precision:"progress-bar progress-bar-yellow",
    f_score:"progress-bar progress-bar-sky-blue",
    accuracy:"progress-bar progress-bar-rose",
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const url = nextProps.location.pathname;
    // console.log(url,'=====================');
    if(url !== prevState.currentUrl) {
      let index = pathname.findIndex((e,i)=>{
        if(url.includes(e))
          return i;
        return null
      })
      if(index<1) {
        index = 0;
      } else {
        index--;
      }
      return { 
        currentUrl: url,
        currentIndex: index
      };
    }
    return null;
  }

  getGraphClass = (name) => {
    return this.graphClasses[name] ? this.graphClasses[name] : "progress-bar progress-bar-blue"
  }

  getPerformanceMatric = (data)=>{
    return(
      <div>
        {data.map(i=>{
          var outputClass = i.output_class
          var metrics = i.metrics
          return(
            <div key={outputClass}
                  className="performance-matrix-content"
                >
                Class: {outputClass}
                <div className="row">
                {
                  metrics.map((metric,index) => {
                    return(
                        <div className="col-md-3" key={outputClass+""+index}>
                          <div className="p-m-item">
                            <label>{metric.name}</label>
                            <span className="p-m-item-value">
                              {metric.value}
                            </span>
                            <div className="progress">
                              <div
                                className={this.getGraphClass(metric.name)}
                                style={{
                                  width: `${metric.value * 100}%`
                                }}
                              />
                            </div>
                          </div>
                        </div>
                    )
                  })
                }
              </div>
            </div>
            )
          })
        }
      </div>
    )
  }

  render() {
    const { modelDetails } = this.props;
    return (
      <React.Fragment>
        <Tabs>
          <TabList className="inline-tabs tabs-gray tabs-md pad-50">
            {modelDetails.hyperparameters && <Tab className="hyper-parameters">HYPER PARAMETERS</Tab>}
            {modelDetails.feature_significance && (
              <Tab className="feature-significance">FEATURE SIGNIFICANCE</Tab>
            )}
            {modelDetails.performance_metrics && <Tab className="performance-metrics">PERFORMANCE METRICS</Tab>}
            {modelDetails.confusion_matrix && <Tab className="confusion-matrix">CONFUSION MATRIX</Tab>}
            {modelDetails.roc_points && <Tab className="roc-curve">ROC CURVE</Tab>}
            {modelDetails.lift_points && <Tab className="lift-curve">LIFT CURVE</Tab>}
            {modelDetails.residual_plot && <Tab className="residual-plot">RESIDUAL PLOT</Tab>}
            {modelDetails.scatter_plot && <Tab className="scatter-plot">SCATTER PLOT</Tab>}
            {modelDetails.training_log_loss && <Tab className="scatter-plot">LOG LOSS</Tab>}
          </TabList>

          {modelDetails &&
            modelDetails.hyperparameters && (
              <TabPanel>
                <div className="hyperparameters-content pl-4">
                  <table className="table table-bordered">
                    <tbody>
                      {Object.keys(modelDetails.hyperparameters).map(
                        (key, index) => {
                          return (
                            <tr key={index}>
                              <td>{key}</td>
                              <td>{modelDetails.hyperparameters[key]}</td>
                            </tr>
                          );
                        }
                      )}
                    </tbody>
                  </table>
                </div>
              </TabPanel>
            )}

          {modelDetails.feature_significance && (
            <TabPanel>
              <div className="featureSignificance-content">
                <ModelFeatureSignificance
                  feature_significance={modelDetails.feature_significance}
                />
              </div>
            </TabPanel>
          )}

          {modelDetails &&
            modelDetails.performance_metrics && (
              <TabPanel>
                <div className="performanceMetrics-content">
                  {this.getPerformanceMatric(modelDetails.performance_metrics)}
                </div>
              </TabPanel>
            )}

          {modelDetails.confusion_matrix && (
            <TabPanel>
              <div className="confusionMatrix-content">
                <ModelConfusionMatrix
                  confusion_matrix={modelDetails.confusion_matrix}
                />
              </div>
            </TabPanel>
          )}

          {modelDetails.roc_points && (
            <TabPanel>
              <div className="rocCurve-content">
                <ModelROCCurve roc_points={modelDetails.roc_points} />
              </div>
            </TabPanel>
          )}

          {modelDetails.lift_points && (
            <TabPanel>
              <div className="liftCurve-content">
                <ModelLiftCurve lift_points={modelDetails.lift_points} />
              </div>
            </TabPanel>
          )}

          {modelDetails.residual_plot && (
            <TabPanel>
              <ModelResidualPlot residual_plot={modelDetails.residual_plot} />
            </TabPanel>
          )}

          {(modelDetails.training_log_loss || modelDetails.validation_log_loss) && (
            <TabPanel>
              <LogLoss training_log_loss={modelDetails.training_log_loss} validation_log_loss={modelDetails.validation_log_loss} />
            </TabPanel>
          )}

          {modelDetails.scatter_plot && (
            <TabPanel>
              <ModelScatterPlot scatter_plot={modelDetails.scatter_plot} />
            </TabPanel>
          )}
        </Tabs>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    rocPoints: state.modelDetails.modelDetails.roc_points
  };
};

export default connect(mapStateToProps)(TrainingMatrix);
