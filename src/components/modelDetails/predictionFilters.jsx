import React, { Component } from "react";

class predictionFilter extends Component {
  constructor(props) {
    super(props);
    this.state = { filters: props.filters };
  }

  handleInputChange = e => {
    let filters = { ...this.state.filters };
    filters[e.target.name] = e.target.value;
    this.setState({ filters });
  };

  handleApplyFilters() {
    console.log("handled");
  }
  handleResetFilters() {
    console.log("Reset filters");
  }

  render() {
    return (
      <div
        className="modal micromodal-slide"
        id="prediction-filter-modal"
        aria-hidden="true"
      >
        <div className="modal__overlay" tabIndex="-1" data-micromodal-close>
          <div
            className="modal__container"
            role="dialog"
            aria-modal="true"
            aria-labelledby="modal-1-title"
          >
            <header className="modal__header">
              <h2 className="modal__title" id="modal-1-title">
                Filter Predictions
              </h2>
              <button
                className="modal__close"
                aria-label="Close modal"
                data-micromodal-close
              />
            </header>
            <main className="modal__content" id="modal-1-content">
              <div className="modal-filter-form">
                <div className="form-group inline-input input-group-field">
                  <label>From Date</label>
                  <select
                    name="from_date_range"
                    id=""
                    className="form-control select-field"
                  >
                    <option value="==">==</option>
                    <option value="<">&lt;</option>
                    <option value=">">&gt;</option>
                    <option value="<=">&lt;=</option>
                    <option value=">=">&gt;=</option>
                  </select>
                  <input
                    type="text"
                    name="from_date"
                    className="form-control input-sm"
                    placeholder="Ex: 06/10/2018"
                  />
                </div>
                <div className="form-group inline-input input-group-field">
                  <label>To Date</label>
                  <select
                    name="to_date_range"
                    id=""
                    className="form-control select-field"
                  >
                    <option value="==">==</option>
                    <option value="<">&lt;</option>
                    <option value=">">&gt;</option>
                    <option value="<=">&lt;=</option>
                    <option value=">=">&gt;=</option>
                  </select>
                  <input
                    type="text"
                    name="to_date"
                    className="form-control input-sm"
                    placeholder="Ex: 06/10/2018"
                  />
                </div>
                <div className="form-group inline-input input-group-field">
                  <label>Output</label>
                  <select
                    name="output_range"
                    id=""
                    className="form-control select-field"
                  >
                    <option value="==">==</option>
                    <option value="<">&lt;</option>
                    <option value=">">&gt;</option>
                    <option value="<=">&lt;=</option>
                    <option value=">=">&gt;=</option>
                  </select>
                  <input
                    type="text"
                    name="output_value"
                    className="form-control input-sm"
                    placeholder="Ex: 0.946"
                  />
                </div>
                <div className="form-group inline-input input-group-field">
                  <label>probability</label>
                  <select
                    name="probability_range"
                    id=""
                    className="form-control select-field"
                  >
                    <option value="==">==</option>
                    <option value="<">&lt;</option>
                    <option value=">">&gt;</option>
                    <option value="<=">&lt;=</option>
                    <option value=">=">&gt;=</option>
                  </select>
                  <input
                    type="text"
                    name="probability_value"
                    className="form-control input-sm"
                    placeholder="Ex: 0.77"
                  />
                </div>
                <hr />
                <div className="form-group inline-input">
                  <label>Prediction Input</label>
                  <input
                    type="number"
                    className="form-control input-sm"
                    placeholder="Ex: 2"
                  />
                </div>
                <div className="form-group inline-input">
                  <label>Prediction Reason</label>
                  <input
                    type="number"
                    className="form-control input-sm"
                    placeholder="Ex: 2"
                  />
                </div>
                <div className="form-group" />
              </div>
            </main>
            <footer className="modal__footer filter-form-actions">
              <button
                className="btn btn-primary btn-rounded"
                data-micromodal-close
                onClick={this.handleApplyFilters}
              >
                APPLY FILTERS
              </button>
              <button
                className="btn btn-default btn-rounded"
                data-micromodal-close
                aria-label="Close this dialog window"
                onClick={this.handleResetFilters}
              >
                RESET FILTERS
              </button>
            </footer>
          </div>
        </div>
      </div>
    );
  }
}

export default predictionFilter;
