import React, { Component } from "react";
import { connect } from "react-redux";
import ReactTooltip from "react-tooltip";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import MicroModal from "micromodal";
import ModelHealthAreaChart from "../chart/ModelHealthAreaChart";
import Filter from "./filter";
import SubFields from "./subFields";
import _ from "lodash";
import {
  getAllModelHealth,
  getAllInsights,
  getAllPredictions,
  getAllFilteredPredictions
} from "../../api";
import CloseImage from "../../images/cross-out.svg";
import moment from 'moment';
import DateRangePicker from 'react-bootstrap-daterangepicker';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-daterangepicker/daterangepicker.css';
import PredictionInputsModalFullScreen from "../modals/PredictionInputsModalFullScreen";

class Runtime extends Component {
  _isMounted = false;
  state = {
    modelHealth: [],
    modelInsight: [],
    modelPrediction: [],
    predictionFilters: {
      output:{
        output_range: "output.equals",
        output_value: ""
      },
      probability: {
        probability_range: "probability.equals",
        probability_value: '',
        probability_value2: '',
      },
      inputs: [{
        id: 0,
        field: '',
        range:'equals',
        value: ''
      }],
      reasons: [{
        id: 0,
        field: '',
        range:'equals',
        value: ''
      }],
      properties: [{
        id: 0,
        field: '',
        range:'equals',
        value: ''
      }],
      dateRange: {
        startDate: '',
        endDate: ''
      }

    },
    displayProperties: true,
    appliedFilters: {}
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  async componentDidMount() {
    this._isMounted = true;
    MicroModal.init({
      disableScroll: true,
      disableFocus: false,
      awaitCloseAnimation: false,
      debugMode: true
    });
    if (this.props.modelDetails) {
      const modelDetailsId = this.props.modelDetails.id;
      let modelHealth = await getAllModelHealth(this.props.token, {
        modelIdEquals: modelDetailsId,
        timestampGreaterOrEqualThan: "now-1d",
        interval: "hour",
        sort: "timestamp,desc"
      });

      let modelInsight = await getAllInsights(this.props.token, {
        modelIdEquals: modelDetailsId,
        sort: "timestamp,desc"
      });

      let modelPrediction = await getAllPredictions(this.props.token, {
        modelIdEquals: modelDetailsId,
        sort: "prediction_date,desc"
      });
      let displayProperties = modelPrediction.data[0] && modelPrediction.data[0].properties == null ? false:true

      let insighPointsLost = 0;
      modelInsight.data.forEach(insightItem => {
        insighPointsLost = insighPointsLost + insightItem.points_lost;
      });
      if(this._isMounted)
        this.setState({
          modelHealth: modelHealth.data,
          modelInsight: modelInsight.data,
          modelPrediction: modelPrediction.data,
          insighPointsLost: insighPointsLost,
          displayProperties: displayProperties,
        });
    }
  }

  modelHealthData = (duration, interval="day") => {
    getAllModelHealth(this.props.token, {
      modelIdEquals: this.props.modelDetails.id,
      timestampGreaterOrEqualThan: duration,
      interval: interval,
      sort: "timestamp,desc"
    }).then(response => {
      if (response.status) {
        this.setState({ modelHealth: response.data });
      }
    });
  };

  handleInputChange = e => {
    let predictionFilters = { ...this.state.predictionFilters };
    predictionFilters[e.target.name] = e.target.value;
    this.setState({ predictionFilters });
  };

  predictionOutputChange = e => {
    let predictionFilters = { ...this.state.predictionFilters };
    predictionFilters.output[e.target.name] = e.target.value;
    this.setState({ predictionFilters });
  };

  predictionProbablityChange = e => {
    let predictionFilters = { ...this.state.predictionFilters };
    predictionFilters.probability[e.target.name] = e.target.value;
    this.setState({ predictionFilters });
  };

  predictionInputChange = (e,index) => {
    let predictionFilters = { ...this.state.predictionFilters };
    predictionFilters.inputs[index][e.target.name] = e.target.value;
    this.setState({ predictionFilters });
  };

  predictionReasonChange = (e,index) => {
    let predictionFilters = { ...this.state.predictionFilters };
    predictionFilters.reasons[index][e.target.name] = e.target.value;
    this.setState({ predictionFilters });
  };

  predictionPropertyChange = (e,index) => {
    let predictionFilters = { ...this.state.predictionFilters };
    predictionFilters.properties[index][e.target.name] = e.target.value;
    this.setState({ predictionFilters });
  };

  changeTimestamp = e => {
    let predictionFilters = { ...this.state.predictionFilters };
    predictionFilters[e.target.name] = e.target.value;
    this.setState({ predictionFilters }, () => {this.handleApplyFilters();});
  };

  handleApplyFilters = () => {
    // this.setState({ appliedFilters: this.state.predictionFilters });
    let { predictionFilters } = this.state;
    let filter = `model_id.equals=${this.props.modelDetails.id}&sort=prediction_date,desc`
    if(predictionFilters.output.output_value.length !== 0){
      if(predictionFilters.output.output_range === "output.in"){
        let array = predictionFilters.output.output_value.split(',');
        array.forEach(element => {
          filter = filter + `&${predictionFilters.output.output_range}=${Number(element)}`
        });
      } else {
        filter = filter + `&${predictionFilters.output.output_range}=${predictionFilters.output.output_value}`
      }
    }
    if(predictionFilters.probability.probability_value.length !== 0){
      if (predictionFilters.probability.probability_range === "probability.in") {
        let array = predictionFilters.probability.probability_value.split(',');
        array.forEach(element => {
          filter = filter + `&${predictionFilters.probability.probability_range}=${Number(element)}`
        });
      } else if(predictionFilters.probability.probability_range === "isBetween") {
        filter = filter + `&probability.greaterOrEqualThan=${predictionFilters.probability.probability_value}`
        filter = filter + `&probability.lessOrEqualThan=${predictionFilters.probability.probability_value2}`
      }else if(predictionFilters.probability.probability_range !== "isBetween" && predictionFilters.probability.probability_range !== "probability.in"){
        filter = filter + `&${predictionFilters.probability.probability_range}=${predictionFilters.probability.probability_value}`
      }
    }
    if(predictionFilters.timestamp){
      filter = filter + `&prediction_date.greaterOrEqualThan=${predictionFilters.timestamp}`
    }

    for (const i in predictionFilters.inputs) {
      if (predictionFilters.inputs.hasOwnProperty(i)) {
        if(predictionFilters.inputs[i].field.length !== 0 && predictionFilters.inputs[i].value.length !== 0){
          if(predictionFilters.inputs[i].range === "in"){
            let array = predictionFilters.inputs[i].value.split(',');
            array.forEach(element => {
              filter = filter + `&${predictionFilters.inputs[i].field}.${predictionFilters.inputs[i].range}=${Number(element)}`;
            });
          } else if (predictionFilters.inputs[i].range === "isBetween" && predictionFilters.inputs[i].value2.length !== 0){
            filter = filter + `&${predictionFilters.inputs[i].field}.greaterOrEqualThan=${predictionFilters.inputs[i].value}`;
            filter = filter + `&${predictionFilters.inputs[i].field}.lessOrEqualThan=${predictionFilters.inputs[i].value2}`
          }else {
            filter = filter + `&${predictionFilters.inputs[i].field}.${predictionFilters.inputs[i].range}=${predictionFilters.inputs[i].value}`
          }
        }
      }
    }

    for (const i in predictionFilters.reasons) {
      if (predictionFilters.reasons.hasOwnProperty(i)) {
        if(predictionFilters.reasons[i].field.length !== 0 && predictionFilters.reasons[i].value.length !== 0){
          if(predictionFilters.reasons[i].range === "in"){
            let array = predictionFilters.reasons[i].value.split(',');
            array.forEach(element => {
              filter = filter + `&${predictionFilters.reasons[i].field}.${predictionFilters.reasons[i].range}=${Number(element)}`;
            });
          } else if (predictionFilters.reasons[i].range === "isBetween" && predictionFilters.reasons[i].value2.length !== 0){
            filter = filter + `&${predictionFilters.reasons[i].field}.greaterOrEqualThan=${predictionFilters.reasons[i].value}`;
            filter = filter + `&${predictionFilters.reasons[i].field}.lessOrEqualThan=${predictionFilters.reasons[i].value2}`
          }else {
            filter = filter + `&${predictionFilters.reasons[i].field}.${predictionFilters.reasons[i].range}=${predictionFilters.reasons[i].value}`
          }
        }
      }
    }

    for (const i in predictionFilters.properties) {
      if (predictionFilters.properties.hasOwnProperty(i)) {
        if(predictionFilters.properties[i].field.length !== 0 && predictionFilters.properties[i].value.length !== 0){
          if(predictionFilters.properties[i].range === "in"){
            let array = predictionFilters.properties[i].value.split(',');
            array.forEach(element => {
              filter = filter + `&${predictionFilters.properties[i].field}.${predictionFilters.properties[i].range}=${element}`;
            });
          } else {
            filter = filter + `&${predictionFilters.properties[i].field}.${predictionFilters.properties[i].range}=${predictionFilters.properties[i].value}`
          }
        }
      }
    }

    if(predictionFilters.dateRange.startDate !== ''){
      filter = filter + `&prediction_date.greaterOrEqualThan=${moment(predictionFilters.dateRange.startDate).toISOString()}&prediction_date.lessOrEqualThan=${moment(predictionFilters.dateRange.endDate).toISOString()}`
    }

    getAllFilteredPredictions(this.props.token, filter)
    .then(response => {
      if (response.status) {
        this.setState({modelPrediction: response.data})
      }
    })
    .catch(error => console.log(error))

  };

  handleResetFilters = () => {
    this.setState({
      predictionFilters: {
        output:{
          output_range: "output.equals",
          output_value: ""
        },
        probability: {
          probability_range: "probability.equals",
          probability_value: '',
          probability_value2: '',
        },
        inputs: [{
          id: 0,
          field: '',
          range:'equals',
          value: ''
        }],
        reasons: [{
          id: 0,
          field: '',
          range:'equals',
          value: ''
        }],
        properties: [{
          id: 0,
          field: '',
          range:'equals',
          value: ''
        }],
        dateRange: {
          startDate: '',
          endDate: ''
        }
      }
    })
    let filter = `model_id.equals=${this.props.modelDetails.id}`
    getAllFilteredPredictions(this.props.token, filter)
    .then(response => {
      if (response.status) {
        this.setState({modelPrediction: response.data})
      }
    })
    .catch(error => console.log(error))
  };

  handleRemoveFilter = (filter,id = null) => {
    let predictionFilters = { ...this.state.predictionFilters };
    if(filter === 'output') {
      predictionFilters.output.output_value = '';
      predictionFilters.output.output_range = 'output.equals';
    }else if(filter === 'probability') {
      predictionFilters.probability.probability_value = '';
      predictionFilters.probability.probability_value2 = '';
      predictionFilters.probability.probability_range = "probability.equals";
    } else if(filter === 'inputs'){
      const inputs = predictionFilters.inputs.filter((item) => item.id !== id);
      predictionFilters.inputs = inputs;
    } else if(filter === 'reasons'){
      const reasons = predictionFilters.reasons.filter((item) => item.id !== id);
      predictionFilters.reasons = reasons;
    } else if(filter === 'properties'){
      const properties = predictionFilters.properties.filter((item) => item.id !== id);
      predictionFilters.properties = properties;
    }

    this.setState({
      predictionFilters
    },() => {this.handleApplyFilters()});
  };

  addPredictionInput = () => {
    let predictionFilters = this.state.predictionFilters;
    predictionFilters.inputs.push({
      id: predictionFilters.inputs.length,
      field: '',
      range:'equals',
      value: ''
    })

    this.setState({predictionFilters})
  }

  deletePredictionInput = (id) => {
    let predictionFilters = this.state.predictionFilters;
    const inputs = predictionFilters.inputs.filter((item) => item.id !== id);
    predictionFilters.inputs = inputs;
    this.setState({predictionFilters})
  }

  addPredictionReason = () => {
    let predictionFilters = this.state.predictionFilters;
    predictionFilters.reasons.push({
      id: predictionFilters.reasons.length,
      field: '',
      range:'equals',
      value: ''
    })

    this.setState({predictionFilters})
  }

  deletePredictionReason = (id) => {
    let predictionFilters = this.state.predictionFilters;
    const reasons = predictionFilters.reasons.filter((item) => item.id !== id);
    predictionFilters.reasons = reasons;
    this.setState({predictionFilters})
  }

  addPredictionProperty = () => {
    let predictionFilters = this.state.predictionFilters;
    predictionFilters.properties.push({
      id: predictionFilters.properties.length,
      field: '',
      range:'',
      value: ''
    })

    this.setState({predictionFilters})
  }

  deletePredictionProperty = (id) => {
    let predictionFilters = this.state.predictionFilters;
    const properties = predictionFilters.properties.filter((item) => item.id !== id);
    predictionFilters.properties = properties;
    this.setState({predictionFilters})
  }

  handleEvent = (event, picker) => {
    let { predictionFilters } = this.state;
    predictionFilters.dateRange.startDate = picker.startDate._d;
    predictionFilters.dateRange.endDate = picker.endDate._d;
    this.setState({predictionFilters},() => {this.handleApplyFilters()})

  }

  removeDateRange = () => {
    let { predictionFilters } = this.state;
    predictionFilters.dateRange.startDate = predictionFilters.dateRange.endDate = '';
    this.setState({predictionFilters},() => {this.handleApplyFilters()})
  }
  healthScoreColor = score =>{
    if (score <= 50) {
      return "#dc3545";
    } else if (score > 50 && score < 75) {
      return "#ffc107";
    } else {
      return "#28a745";
    }
  }
  addScoreTextClass = score => {
    if (score <= 50) {
      return "text-danger";
    } else if (score > 50 && score < 75) {
      return "text-warning";
    } else {
      return "text-success";
    }
  };
  renderPredictionsTable = (modelPrediction)=>{
    if(modelPrediction && modelPrediction.length > 0){
      return(
        <table className="table table-bordered data-table insights-table">
            <thead>
              <tr>
                <td>Prediction Timestamp</td>
                <td>Prediction Duration</td>
                <td>Prediction Output</td>
                <td>Top Reason</td>
                { this.state.displayProperties &&  <td>Properties</td>}
                <td>Prediction Inputs</td>
              </tr>
            </thead>
            <tbody>
              {modelPrediction.map((predictionItem, index) => {
                return (
                  <tr key={index}>
                    <td>{moment(predictionItem.prediction_date).format('llll')}</td>
                    <td>{predictionItem.prediction_duration}</td>
                    <td>
                      <div className="prediction-o-item">
                        <label>Output:</label>
                        <span>
                          {" "}
                          {(predictionItem.prediction_output &&
                            predictionItem.prediction_output.output) ||
                            "N/A"}
                        </span>
                      </div>

                      <div className="prediction-o-item">
                        <label>Probability:</label>
                        <span>
                          {" "}
                          {(predictionItem.prediction_output &&
                            predictionItem.prediction_output.probability) ||
                            "N/A"}
                        </span>
                      </div>
                      <div
                        className="prediction-more-info-link"
                        data-tip="light"
                      >
                        <p data-tip='' data-for={predictionItem.id}>
                          <i className="fa fa-info-circle" /> More
                        </p>
                        <ReactTooltip place="bottom" type="light" id={predictionItem.id} >
                          {
                            <SubFields
                              data={predictionItem.prediction_output.outputs}
                            />
                          }
                        </ReactTooltip>
                      </div>
                    </td>
                    <td>
                      {
                        predictionItem.prediction_reasons[0] &&
                        <div>
                          <div>
                            {predictionItem.prediction_reasons[0].name}: {predictionItem.prediction_reasons[0].value}
                          </div>
                          <div
                            className="prediction-more-info-link"
                            data-tip="light"
                          >
                            <p data-tip='' data-for={"top_reason"+predictionItem.id}>
                              <i className="fa fa-info-circle" /> More
                            </p>
                            <ReactTooltip place="bottom" type="light" id={"top_reason"+predictionItem.id} >
                              {
                                <SubFields
                                  data={predictionItem.prediction_reasons}
                                />
                              }
                            </ReactTooltip>
                          </div>
                        </div>
                      }
                    </td>
                    { this.state.displayProperties &&
                    <td>
                      { predictionItem.properties ?
                        Object.keys(predictionItem.properties).map((key,j) => {
                          return (
                            <div key={j}>
                              {key}:{predictionItem.properties[key]}
                            </div>
                          );
                        }) : 'NA'}
                    </td>
                    }
                    <td>
                    {/* <a
                      href=""
                      className="btn btn-primary btn-sm btn-rounded"
                      data-toggle="modal"
                      data-target="#view-inputs"
                      onClick={() => this.setState({ selectedPredictionInputs: predictionItem.prediction_input })}
                    >
                      View Inputs
                    </a> */}
                    <PredictionInputsModalFullScreen predictionInputsList = {predictionItem.prediction_input}/>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
      )
    }else{
      return(<span>No data found</span>)
    }
  }
  render() {
    const { modelDetails } = this.props;
    let appliedFilters = _.pickBy(this.state.appliedFilters, _.identity);
    let { predictionFilters } = this.state;
    return (
      <div className="runtime-content">
        <div className="data-section pl-4">
          <div className="row">
            <div className="col-md-5">
              <div className="data-sec-head mt-0">
                <h4>Health Score</h4>
                <div className="data-sec-right">
                  <div className="health-score-counter">
                    <span className={this.addScoreTextClass(modelDetails.health_score)}>{modelDetails.health_score && modelDetails.health_score.toFixed(2)}</span>
                  </div>
                </div>
              </div>
              <div className="data-section-body">
                <Tabs>
                  <TabList className="inline-tabs tabs-gray">
                    <Tab onClick={() => this.modelHealthData("now-1d", "hour")}>
                      LAST 1 DAY
                    </Tab>
                    <Tab onClick={() => this.modelHealthData("now-7d")}>
                      LAST 7 DAYS
                    </Tab>
                    <Tab onClick={() => this.modelHealthData("now-30d")}>
                      LAST MONTH
                    </Tab>
                  </TabList>
                  <TabPanel>
                    <ModelHealthAreaChart
                      modelHealthDetails={this.state.modelHealth}
                      chartColor={this.healthScoreColor(modelDetails.health_score)}
                      format="LT"
                      dayOne={true}
                    />
                  </TabPanel>
                  <TabPanel>
                    <ModelHealthAreaChart
                      modelHealthDetails={this.state.modelHealth}
                      chartColor={this.healthScoreColor(modelDetails.health_score)}
                      format="ll"
                    />
                  </TabPanel>
                  <TabPanel>
                    <ModelHealthAreaChart
                      modelHealthDetails={this.state.modelHealth}
                      chartColor={this.healthScoreColor(modelDetails.health_score)}
                      format="ll"
                    />
                  </TabPanel>
                </Tabs>
              </div>
            </div>
            <div className="col-md-1">
              <div className="vertical-border-line"></div>
            </div>
            <div className="col-md-5">
              <div className="data-sec-head mt-0">
                <h4>Insights</h4>
              </div>
              <div className="data-section-body">
                {(this.state.modelInsight && this.state.modelInsight.length > 0) ?
                  (<div className="fixed-height-table fixed-content-box">
                    <table className="table table-bordered data-table insights-table">
                      <thead>
                        <tr>
                          <td>Date</td>
                          <td>Insight</td>
                          <td>Category</td>
                        </tr>
                      </thead>
                      <tbody>
                        {this.state.modelInsight &&
                          this.state.modelInsight.map((insightItem, index) => {
                            return (
                              <tr key={index}>
                                <td>{moment(insightItem.timestamp).format('llll')}</td>
                                <td>{insightItem.reason}</td>
                                <td>{insightItem.points_category}</td>
                              </tr>
                            );
                          })}
                      </tbody>
                    </table>
                  </div>
                  ) : ( <div className="no-data-msg"><p className="text-center text-gray">No data for Insights</p></div>)
                }
              </div>
            </div>
          </div>
        </div>
        <hr />
        <div className="data-section pl-4">
          <div className="data-sec-head">
            <h4>Predictions</h4>
            <div className="data-sec-right">
              <div className="insight-points">
                <span>Predictions Count</span>
                <strong>{modelDetails.predictions_count}</strong>
              </div>
            </div>
          </div>
          <div className="action-row">
            <div className="action-left-col">
              <button
                type="button"
                className="btn btn-primary btn-sm btn-rounded"
                data-micromodal-trigger="prediction-filter-modal"
              >
                ADD FILTER +
              </button>
            </div>
            <div className="applied-filters">
              <ul className="filter-items-list">
                { predictionFilters.output.output_value &&
                  <li className="filter-tag-item">
                    output-{predictionFilters.output.output_value}
                    <i className="fa fa-times" style={{marginTop: 3}} onClick={() => this.handleRemoveFilter('output')}>
                    </i>
                  </li>
                }

                { predictionFilters.probability.probability_value &&
                  <li className="filter-tag-item">
                    probability-{predictionFilters.probability.probability_value}
                    <i onClick={() => this.handleRemoveFilter('probability')}>
                      <img
                        src={CloseImage}
                        alt="close"
                        width="10"
                        height="10"
                      />
                    </i>
                  </li>
                }
                { predictionFilters.inputs && 
                  predictionFilters.inputs[0] &&
                  predictionFilters.inputs[0].value && 
                  predictionFilters.inputs.map((input,index) => {
                  return(
                    <li className="filter-tag-item" key={index}>
                      {input.field}-{input.value}
                      <i onClick={() => this.handleRemoveFilter('inputs', input.id)}>
                        <img
                          src={CloseImage}
                          alt="close"
                          width="10"
                          height="10"
                        />
                      </i>
                    </li>
                  )})
                }
                { predictionFilters.reasons && 
                  predictionFilters.reasons[0] &&
                  predictionFilters.reasons[0].value && 
                  predictionFilters.reasons.map((reason,index) => {
                  return(
                    <li className="filter-tag-item" key={index}>
                      {reason.field}-{reason.value}
                      <i onClick={() => this.handleRemoveFilter('reasons', reason.id)}>
                        <img
                          src={CloseImage}
                          alt="close"
                          width="10"
                          height="10"
                        />
                      </i>
                    </li>
                  )})
                }
                { predictionFilters.properties && 
                  predictionFilters.properties[0] &&
                  predictionFilters.properties[0].value && 
                  predictionFilters.properties.map((property,index) => {
                  return(
                    <li className="filter-tag-item" key={index}>
                      {property.field}-{property.value}
                      <i onClick={() => this.handleRemoveFilter('properties', property.id)}>
                        <img
                          src={CloseImage}
                          alt="close"
                          width="10"
                          height="10"
                        />
                      </i>
                    </li>
                  )})
                }
                { predictionFilters.dateRange && 
                  predictionFilters.dateRange.startDate &&
                  <li className="filter-tag-item">
                    DateRange: {moment(predictionFilters.dateRange.startDate).format('L')} - {moment(predictionFilters.dateRange.endDate).format('L')}
                    <i onClick={() => this.removeDateRange()}>
                      <img
                        src={CloseImage}
                        alt="close"
                        width="10"
                        height="10"
                      />
                    </i>
                  </li>
                }
              </ul>
            </div>
            <div className="action-right-col right-select-actions">
              <div className="right-col-content">
                <select
                  className="form-control select-field"
                  name="timestamp"
                  className="form-control select-field"
                  onChange={this.changeTimestamp}
                  defaultValue={predictionFilters.timestamp}>
                  <option value="">Select Timestamp</option>
                  <option value="now-1d">Last 1 Day</option>
                  <option value="now-7d">Last 7 Days</option>
                  <option value="now-30d">Last Month</option>
                </select>
                <DateRangePicker 
                  startDate={new Date()} 
                  endDate={new Date()}
                  onApply={this.handleEvent}>
                  <button className="btn btn-primary btn-sm btn-rounded">Select Date Range</button>
                </DateRangePicker>
                {/* <button
                  type="button"
                  className="btn btn-primary btn-sm btn-rounded"
                >
                  DOWNLOAD
                </button> */}
              </div>
            </div>
          </div>
            {this.renderPredictionsTable(this.state.modelPrediction)}
          </div>

        <Filter
          filters={this.state.predictionFilters}
          applyFilters={this.handleApplyFilters}
          resetFilters={this.handleResetFilters}
        >
          <div className="form-group row">
            <label className="col-md-3">Output</label>
            <div className="col-md-4">
              <select
                name="output_range"
                id=""
                className="form-control select-field"
                onChange={this.predictionOutputChange}
                defaultValue={predictionFilters.output.output_range}
              >
                <option value='output.equals'>equals</option>
                <option value='output.contains'>contains</option>
                <option value='output.in'>is on off</option>
              </select>
            </div>
            { predictionFilters && predictionFilters.output.output_range !== 'output.in' &&

              <input
                type="string"
                name="output_value"
                className="form-control input-sm col-md-3"
                placeholder="Ex: 0.11"
                onChange={this.predictionOutputChange}
                value={predictionFilters.output.output_value}
              />
            }
            { predictionFilters && predictionFilters.output.output_range === 'output.in' &&
                <input
                  type="string"
                  name="output_value"
                  className="form-control input-sm col-md-3"
                  placeholder="Ex: 0.11,0.2,0.33"
                  onChange={this.predictionOutputChange}
                  value={predictionFilters.output.output_value}
                />
            }
          </div>
          <div className="form-group row">
            <label className="col-md-3">probability</label>
            <div className="col-md-4">
              <select
                name="probability_range"
                id=""
                className="form-control select-field"
                onChange={this.predictionProbablityChange}
                defaultValue={predictionFilters.probability.probability_range}
              >
                <option value="probability.equals">is</option>
                <option value="probability.in">is one of</option>
                <option value="probability.lessThan">is less than</option>
                <option value="probability.lessOrEqualThan">is less or equal than</option>
                <option value="probability.greaterThan">is greater than</option>
                <option value="probability.greaterOrEqualThan">is greater or equal than</option>
                <option value="isBetween">is between</option>
              </select>
            </div>
            {predictionFilters && 
              predictionFilters.probability.probability_range !== 'output.in' &&
              predictionFilters.probability.probability_range !== 'isBetween' &&
            <input
              type="number"
              name="probability_value"
              className="form-control input-sm col-md-3"
              placeholder="Ex: 0.77"
              onChange={this.predictionProbablityChange}
              value={predictionFilters.probability.probability_value}
            />}
            {predictionFilters && predictionFilters.probability.probability_range === 'output.in' &&
            <input
              type="string"
              name="probability_value"
              className="form-control input-sm col-md-3"
              placeholder="Ex: 0.11,0.44,0.99"
              onChange={this.predictionProbablityChange}
              value={predictionFilters.probability.probability_value}
            />}
            {predictionFilters && predictionFilters.probability.probability_range === 'isBetween' &&
              <React.Fragment>
                <input
                  type="string"
                  name="probability_value"
                  className="form-control input-sm col-md-2"
                  placeholder="Ex: 0.11"
                  onChange={this.predictionProbablityChange}
                  value={predictionFilters.probability.probability_value}
                />
                <input
                  type="string"
                  name="probability_value2"
                  className="form-control input-sm col-md-2"
                  placeholder="Ex: 0.77"
                  onChange={this.predictionProbablityChange}
                  value={predictionFilters.probability.probability_value2}
                />
              </React.Fragment>
            }
          </div>
          <hr/>
          <div className="action-row">
            <h4>Prediction Input</h4>
            <div className="action-right-col">
              <button
                type="button"
                className="pull-right btn btn-primary btn-sm btn-rounded"
                onClick={() => this.addPredictionInput()}
              >
                +
              </button>
            </div>
          </div>
          <div>
            { this.state.predictionFilters && this.state.predictionFilters.inputs.map((input,index) => {
              return(
                <div className="form-group row" key={index}>
                  <div className="col-md-3">
                    <input
                      type="string"
                      name="field"
                      className="form-control input-sm"
                      placeholder="field"
                      onChange={(e) =>this.predictionInputChange(e,index)}
                      value={input.field}
                    />
                  </div>
                  <div className="col-md-4">
                    <select
                      name="range"
                      id=""
                      className="form-control select-field"
                      onChange={(e) =>this.predictionInputChange(e,index)}
                      defaultValue={input.range}
                    >
                      <option value="equals">is</option>
                      <option value="in">is one of</option>
                      <option value="lessThan">is less than</option>
                      <option value="lessOrEqualThan">is less or equal than</option>
                      <option value="greaterThan">is greater than</option>
                      <option value="greaterOrEqualThan">is greater or equal than</option>
                      <option value="isBetween">is between</option>
                    </select>
                  </div>
                  { input.range !== "isBetween" &&
                    <input
                      type="string"
                      name="value"
                      className="form-control input-sm col-md-3"
                      placeholder={input.range === "in"? "Ex: val1,val2":"Ex: value" }
                      onChange={(e) => this.predictionInputChange(e,index)}
                      value={input.value}
                    />}
                  { input.range === "isBetween" &&
                    <React.Fragment>
                      <input
                        type="string"
                        name="value"
                        className="form-control input-sm col-md-2"
                        placeholder="val1"
                        onChange={(e) => this.predictionInputChange(e,index)}
                        value={input.value}
                      />
                      <input
                        type="string"
                        name="value2"
                        className="form-control input-sm col-md-2"
                        placeholder="val2"
                        onChange={(e) => this.predictionInputChange(e,index)}
                        value={input.value2}
                      />
                    </React.Fragment>
                  }
                  <button
                    className="modal__close"
                    onClick={() => this.deletePredictionInput(input.id)}
                  >X</button>
                </div>
              )})
            }
          </div>
          <hr/>
          <div className="action-row">
            <h4>Prediction Reason</h4> 
            <div className="action-right-col">
              <button
                type="button"
                className="pull-right btn btn-primary btn-sm btn-rounded"
                onClick={() => this.addPredictionReason()}
              >
                +
              </button>
            </div>
          </div>
          <div>
            { this.state.predictionFilters && this.state.predictionFilters.reasons.map((reason,index) => {
              return(
                <div className="form-group row" key={index}>
                  <div className="col-md-3">
                    <input
                      type="string"
                      name="field"
                      className="form-control input-sm"
                      placeholder="Field"
                      onChange={(e) =>this.predictionReasonChange(e,index)}
                      value={reason.field}
                    />
                  </div>
                  <div className="col-md-4">
                    <select
                      name="range"
                      id=""
                      className="form-control select-field"
                      onChange={(e) =>this.predictionReasonChange(e,index)}
                      defaultValue={reason.range}
                    >
                      <option value="equals">is</option>
                      <option value="in">is one of</option>
                      <option value="lessThan">is less than</option>
                      <option value="lessOrEqualThan">is less or equal than</option>
                      <option value="greaterThan">is greater than</option>
                      <option value="greaterOrEqualThan">is greater or equal than</option>
                      <option value="isBetween">is between</option>
                    </select>
                  </div>
                  { reason.range !== "isBetween" &&
                    <input
                      type="string"
                      name="value"
                      className="form-control input-sm col-md-3"
                      placeholder={reason.range === "in"? "Ex: val1,val2":"Ex: value" }
                      onChange={(e) => this.predictionReasonChange(e,index)}
                      value={reason.value}
                    />}
                  { reason.range === "isBetween" &&
                    <React.Fragment>
                      <input
                        type="string"
                        name="value"
                        className="form-control input-sm col-md-2"
                        placeholder="val1"
                        onChange={(e) => this.predictionReasonChange(e,index)}
                        value={reason.value}
                      />
                      <input
                        type="string"
                        name="value2"
                        className="form-control input-sm col-md-2"
                        placeholder="val2"
                        onChange={(e) => this.predictionReasonChange(e,index)}
                        value={reason.value2}
                      />
                    </React.Fragment>
                  }
                  <button
                    className="modal__close"
                    onClick={() => this.deletePredictionReason(reason.id)}
                  >X</button>
                </div>
              )})
            }
          </div>
          <hr/>
          <div className="action-row"> 
            <h4>Prediction Properties</h4> 
            <div className="action-right-col">
              <button
                type="button"
                className="pull-right btn btn-primary btn-sm btn-rounded"
                onClick={() => this.addPredictionProperty()}
              >
                +
              </button>
            </div>
          </div>
          <br/>
          <div>
            { this.state.predictionFilters && this.state.predictionFilters.properties && this.state.predictionFilters.properties.map((property,index) => {
              return(
                <div className="form-group row" key={index}>
                    <div className="col-md-3">
                    <input
                      type="string"
                      name="field"
                      className="form-control input-sm"
                      placeholder="Field"
                      onChange={(e) => this.predictionPropertyChange(e,index)}
                      value={property.field}
                    />
                  </div>

                  <div className="col-md-4">
                    <select
                      name="range"
                      id=""
                      className="form-control select-field"
                      onChange={(e) => this.predictionPropertyChange(e,index)}
                      defaultValue={property.range}
                    >
                      <option value='equals'>equals</option>
                      <option value='contains'>contains</option>
                      <option value='in'>is on off</option>
                    </select>
                  </div>
                  <input
                    type="string"
                    name="value"
                    className="form-control input-sm col-md-3"
                    placeholder={property.range === "in"? "Ex: val1,val2":"Ex: value" }
                    onChange={(e) => this.predictionPropertyChange(e,index)}
                    value={property.value}
                  />
                  <button
                    className="modal__close"
                    onClick={() => this.deletePredictionProperty(property.id)}
                  >X</button>
                </div>
              )})
            }
          </div>
          <div className="form-group" />
        </Filter>

        <div className="modal" tabIndex="-1" role="dialog" id="view-inputs">
          <div className="modal-dialog modal-md" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Prediction Inputs</h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

              <table className="table table-bordered data-table insights-table">
                <thead>
                  <tr>
                    <td>Name</td>
                    <td>Value</td>
                  </tr>
                </thead>
                <tbody>
                    { this.state.selectedPredictionInputs && this.state.selectedPredictionInputs.map((input,index) => {
                      return(
                        <tr key={index}>
                          <td>{input.name}</td>
                          <td>{input.value}</td>
                        </tr>
                      )
                    })}
                </tbody>
              </table>
              
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    data-dismiss="modal"
                  >
                    Close
                  </button>
                </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token
  };
};

export default connect(mapStateToProps)(Runtime);
