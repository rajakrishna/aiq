import React, { Component } from "react";
import { connect } from "react-redux";
import {getTnRSignificance} from "../../api";
import Select from 'react-select';
import {filter} from 'lodash'

const column_keys = ["count", "max", "min", "mean", "median", "significance", "stddv", "unique"]

class TNRSignificanceFeature extends Component{
  state = {
    tnrSignificance: [],
    filteredTNRSignificance: [],
    selectedOptions: [],
    selectOptions: [],
    columnOptions: [],
    selectedColumns: [],
  }
  constructor(props){
    super(props);
  }

  async componentDidMount() {
    let response = await getTnRSignificance(this.props.token,this.props.modelID);
    let data = response.data
    let selectOptions = []
    data.map((item)=>{
      const option =   { value: item.feature, label: item.feature }
      selectOptions.push(option)
    })
    let columnOptions = []
    column_keys.map((item) => {
      const option =   { value: item, label: item }
      columnOptions.push(option)
    })
    this.setState({
      tnrSignificance: data,
      filteredTNRSignificance: data,
      selectOptions: selectOptions,
      columnOptions: columnOptions
    })
  }

  handleSelect = (selectedOptions)=> {
    this.setState({ selectedOptions });
    this.filterRows(selectedOptions)
  }

  handleColumnSelect = (selectedColumns)=>{
    this.setState({selectedColumns})
  }

  displayColumns = (columnGroup) =>{
    let {selectedColumns} = this.state
    var displayColumns = false
    if (selectedColumns && selectedColumns.length > 0){
      selectedColumns.map((item) => {
        if (item.value === columnGroup){
          displayColumns = true
          return
        }
      })
    }else{
      displayColumns = true
    }
    return displayColumns
  }

  filterRows = (selectedOptions)=> {
    const keys = selectedOptions.map(function(option){return option.value})
    var filteredTNRSignificance = this.state.tnrSignificance
    if (keys && keys.length > 0){
      filteredTNRSignificance  = filter(this.state.tnrSignificance,function(column){
        return (keys.indexOf(column.feature) >= 0 )
      })
    }
    this.setState({filteredTNRSignificance})
  }

  render(){
    const { filteredTNRSignificance,selectOptions,selectedOptions,columnOptions,selectedColumns} = this.state;
    return(
      <div className="data-section">
          <div className="data-sec-head">
            <h4>Training And Runtime Significant Features</h4>
          </div>

          <div className="row" style={{marginBottom: 20}}>
            <div className="col-md-6">
              <label>Features</label>
              <Select
                  value={selectedOptions}
                  onChange={this.handleSelect}
                  options={selectOptions}
                  multi
                  placeholder={"Select..."}
                  isSearchable={true}
              />
            </div>
            <div className="col-md-6">
              <label>Metrics</label>
              <Select
                value={selectedColumns}
                onChange={this.handleColumnSelect}
                options={columnOptions}
                placeholder={"Select..."}
                multi
                isSearchable={true}
              />
            </div>
          </div>

          {filteredTNRSignificance && (filteredTNRSignificance.length > 0) && 
          <div className="table-responsive fixed-height-table">
            <table className="table table-bordered data-table insights-table">
              <thead>
                <tr>
                  <td>Feature</td>
                  {this.displayColumns("count") &&
                    <React.Fragment>
                      <td>Runtime Count</td>
                      <td>Runtime Count (hr)</td>
                      <td>Runtime Count (day)</td>
                    </React.Fragment>
                  }
                  {this.displayColumns("max") &&
                    <React.Fragment>
                      <td>Runtime Max </td>
                      <td>Training Max </td>
                      <td>Runtime Max (hr)</td>
                      <td>Runtime Max (day)</td>
                    </React.Fragment>
                  }
                  {this.displayColumns("mean") &&
                    <React.Fragment>
                      <td>Runtime Mean </td>
                      <td>Training Mean </td>
                      <td>Runtime Mean (hr)</td>
                      <td>Runtime Mean (day)</td>
                    </React.Fragment>
                  }

                  {this.displayColumns("median") &&
                    <React.Fragment>
                      <td>Runtime Median </td>
                      <td>Training Median </td>
                      <td>Runtime Median (hr)</td>
                      <td>Runtime Median (day)</td>
                    </React.Fragment>
                  }
                  {this.displayColumns("min") &&
                    <React.Fragment>
                      <td>Runtime Min </td>
                      <td>Training Min </td>
                      <td>Runtime Min (hr)</td>
                      <td>Runtime Min (day)</td>
                    </React.Fragment>
                  }

                  {this.displayColumns("significance") &&
                    <React.Fragment>
                      <td>Runtime Significance </td>
                      <td>Training Significance </td>
                      <td>Runtime Significance (hr)</td>
                      <td>Runtime Significance (day)</td>
                    </React.Fragment>
                  }

                  {this.displayColumns("stddv") &&
                    <React.Fragment>
                      <td>Runtime Stddv </td>
                      <td>Training Stddv </td>
                      <td>Runtime Stddv (hr)</td>
                      <td>Runtime Stddv (day)</td>
                    </React.Fragment>
                  }
                  {this.displayColumns("unique") &&
                    <React.Fragment>
                      <td>Runtime Unique </td>
                      <td>Training Unique </td>
                      <td>Runtime Unique (hr)</td>
                      <td>Runtime Unique (day)</td>
                    </React.Fragment>
                  }

                </tr>
              </thead>
              <tbody>
                {filteredTNRSignificance && (filteredTNRSignificance.length > 0) && 
                  filteredTNRSignificance.map((item,index)=>{
                    return(
                      <tr key={index}>
                        <td>{item["feature"]}</td>
                        {this.displayColumns("count") &&
                          <React.Fragment>
                            <td>{item["runtime_drift"] && item["runtime_drift"].count}</td>
                            <td>{item["runtime_metrics_hr"] && item["runtime_metrics_hr"].count}</td>
                            <td>{item["runtime_metrics_day"] && item["runtime_metrics_day"].count}</td>
                          </React.Fragment>
                        }

                        {this.displayColumns("max") &&
                          <React.Fragment>
                            <td>{item["runtime_drift"] && item["runtime_drift"].max}</td>
                            <td>{item["training_metrics"] && item["training_metrics"].max}</td>
                            <td>{item["runtime_metrics_hr"] && item["runtime_metrics_hr"].max}</td>
                            <td>{item["runtime_metrics_day"] && item["runtime_metrics_day"].max}</td>
                          </React.Fragment>
                        }
                        {this.displayColumns("mean") &&
                          <React.Fragment>
                            <td>{item["runtime_drift"] && item["runtime_drift"].mean}</td>
                            <td>{item["training_metrics"] && item["training_metrics"].mean}</td>
                            <td>{item["runtime_metrics_hr"] && item["runtime_metrics_hr"].mean}</td>
                            <td>{item["runtime_metrics_day"] && item["runtime_metrics_day"].mean}</td>
                          </React.Fragment>
                        }
                        {this.displayColumns("median") &&
                          <React.Fragment>
                            <td>{item["runtime_drift"] && item["runtime_drift"].median}</td>
                            <td>{item["training_metrics"] && item["training_metrics"].median}</td>
                            <td>{item["runtime_metrics_hr"] && item["runtime_metrics_hr"].median}</td>
                            <td>{item["runtime_metrics_day"] && item["runtime_metrics_day"].median}</td>
                          </React.Fragment>
                        }
                        {this.displayColumns("min") &&
                          <React.Fragment>
                            <td>{item["runtime_drift"] && item["runtime_drift"].min}</td>
                            <td>{item["training_metrics"] && item["training_metrics"].min}</td>
                            <td>{item["runtime_metrics_hr"] && item["runtime_metrics_hr"].min}</td>
                            <td>{item["runtime_metrics_day"] && item["runtime_metrics_day"].min}</td>
                          </React.Fragment>
                        }
                        {this.displayColumns("significance") &&
                          <React.Fragment>
                            <td>{item["runtime_drift"] && item["runtime_drift"].significance}</td>
                            <td>{item["training_metrics"] && item["training_metrics"].significance}</td>
                            <td>{item["runtime_metrics_hr"] && item["runtime_metrics_hr"].significance}</td>
                            <td>{item["runtime_metrics_day"] && item["runtime_metrics_day"].significance}</td>
                          </React.Fragment>
                        }
                        {this.displayColumns("stddv") &&
                          <React.Fragment>
                            <td>{item["runtime_drift"] && item["runtime_drift"].stddv}</td>
                            <td>{item["training_metrics"] && item["training_metrics"].stddv}</td>
                            <td>{item["runtime_metrics_hr"] && item["runtime_metrics_hr"].stddv}</td>
                            <td>{item["runtime_metrics_day"] && item["runtime_metrics_day"].stddv}</td>
                          </React.Fragment>
                        }
                        {this.displayColumns("unique") &&
                          <React.Fragment>
                            <td>{item["runtime_drift"] && item["runtime_drift"].unique}</td>
                            <td>{item["training_metrics"] && item["training_metrics"].unique}</td>
                            <td>{item["runtime_metrics_hr"] && item["runtime_metrics_hr"].unique}</td>
                            <td>{item["runtime_metrics_day"] && item["runtime_metrics_day"].unique}</td>
                          </React.Fragment>
                        }
                      </tr>)
                  }) 
                }
              </tbody>
            </table>
          </div> }

        </div>

    )
  }

}

const mapStateToProps = state => {
  return {
    token: state.auth.token
  };
};

export default connect(mapStateToProps)(TNRSignificanceFeature);