import React, { Component } from "react";

class SubFields extends Component {
  getTableHeader = keys => {
    return (
      <thead>
        <tr>
          {keys &&
            keys.map((key, index) => {
              return <td key={index}>{key}</td>;
            })}
        </tr>
      </thead>
    );
  };

  getTableBody = (keys, data) => {
    return (
      <tbody>
        {data &&
          data.map((item, itemIndex) => {
            return (
              <tr key={itemIndex}>
                {keys &&
                  keys.map((key, index) => {
                    return <td key={index}>{item[key]}</td>;
                  })}
              </tr>
            );
          })}
      </tbody>
    );
  };

  render() {
    var data = this.props.data;
    var keys = Object.keys(data[0]);
    return (
      <table className="table table-bordered">
        {this.getTableHeader(keys)}
        {this.getTableBody(keys, data)}
      </table>
    );
  }
}

export default SubFields;
