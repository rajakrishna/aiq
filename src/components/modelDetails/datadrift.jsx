import React, { Component } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { connect } from "react-redux";
import {
  getDataDriftsTrend,
  getAllInsights,
  getAllPredictions,
  getTnRSignificance,
  getAllFilteredPredictions,
} from "../../api";
import CloseImage from "../../images/cross-out.svg";
import PredictionInputFilter,{buildFilters} from "../filters/prediction_input_filter"
import getDefaultFilters from "../filters/default_filters"
import TNRSignificanceFeature from './tnr_significance_feature'
import { DataDriftChart } from "../chart/DataDrift";
import moment from 'moment';
import DateRangePicker from 'react-bootstrap-daterangepicker';

class Datadrift extends Component {
  state = {
    dataDrift: [],
    dataDriftQuality: [],
    predictionData: [],
    displayProperties: true,
    predictionInputFilters: getDefaultFilters(),
  };

  async componentDidMount() {
    if (this.props.modelDetails) {
      let dataDriftData = await getDataDriftsTrend(this.props.token, {
        timestampGreaterOrEqualThan: "now-1d",
        id: this.props.modelDetails.id
      });

      this.setState({
        dataDrift: dataDriftData.data,
      });

      let dataDriftQuality = await getAllInsights(this.props.token, {
        modelIdEquals: this.props.modelDetails.id,
        pointsCategoryEquals: "data_drift",
        sort: "timestamp,desc"
      });
      
      this.setState({
        dataDriftQuality: dataDriftQuality.data,
        dataDriftQualityPoints: this.props.modelDetails.data_drift,
      });

      let predictionData = await getAllPredictions(this.props.token, {
        modelIdEquals: this.props.modelDetails.id,
        sort: "prediction_date,desc"
      });

      this.setState({
        predictionData: predictionData.data
      });

      let tnrSignificance = await getTnRSignificance(this.props.token,this.props.modelDetails.id);

      this.setState({
        tnrSignificance: tnrSignificance.data
      });
    }
  }

  addScoreTextClassDrift = score => {
    if (score <= 50) {
      return "text-success";
    } else if (score > 50 && score < 75) {
      return "text-warning";
    } else {
      return "text-danger";
    }
  };

  fetchFilteredPredictions = (predictionInputFilters) => {
    var filters = buildFilters(predictionInputFilters,this.props.modelDetails.id)
    getAllFilteredPredictions(this.props.token,filters).then(response => {
      if (response.status) {
        this.setState({predictionData: response.data,predictionInputFilters: predictionInputFilters})
      }
    })
  }

  changeTimestamp = e => {
    let predictionInputFilters = { ...this.state.predictionInputFilters };
    predictionInputFilters[e.target.name] = e.target.value;
    predictionInputFilters.dateRange = {
      startDate: '',
      endDate: ''
    }
    this.fetchFilteredPredictions(predictionInputFilters)
  };

  getChartData = data => {
    return getDataDriftsTrend(this.props.token, {
      ...data,
      id: this.props.modelDetails.id
    })
      .then(response => {
        if (response.status) {
          this.setState({ dataDrift: response.data });
        }
      })
      .catch(error => console.log(error));
  };

  getColumns = predictionData => {
    return (
      predictionData.prediction_input &&
      predictionData.prediction_input.map((childObj, index) => {
        return <td key={index}>{childObj["value"]}</td>;
      })
    );
  };

  handleEvent = (event, picker) => {
    let { predictionInputFilters } = this.state;
    predictionInputFilters.timestamp = null
    predictionInputFilters.dateRange.startDate = picker.startDate._d;
    predictionInputFilters.dateRange.endDate = picker.endDate._d;
    this.fetchFilteredPredictions(predictionInputFilters)
  }
  removeDateRange = () => {
    let { predictionInputFilters } = this.state;
    predictionInputFilters.dateRange.startDate = predictionInputFilters.dateRange.endDate = '';
    this.fetchFilteredPredictions(predictionInputFilters)    
  }

  handlePredictionInputFilterData = (data,filters) => {
    this.setState({predictionData: data,predictionInputFilters: filters})
  }

  handleRemoveFilter = (filter,id = null) => {
    let predictionInputFilters = { ...this.state.predictionInputFilters };
    if(filter === 'output') {
      predictionInputFilters.output.output_value = '';
      predictionInputFilters.output.output_range = 'output.equals';
    }else if(filter === 'probability') {
      predictionInputFilters.probability.probability_value = '';
      predictionInputFilters.probability.probability_value2 = '';
      predictionInputFilters.probability.probability_range = "probability.equals";
    } else if(filter === 'inputs'){
      const inputs = predictionInputFilters.inputs.filter((item) => item.id !== id);
      predictionInputFilters.inputs = inputs;
    } else if(filter === 'reasons'){
      const reasons = predictionInputFilters.reasons.filter((item) => item.id !== id);
      predictionInputFilters.reasons = reasons;
    } else if(filter === 'properties'){
      const properties = predictionInputFilters.properties.filter((item) => item.id !== id);
      predictionInputFilters.properties = properties;
    }
    this.fetchFilteredPredictions(predictionInputFilters)
  };

  renderPredictionInput(){
    if((this.state.predictionData) && (this.state.predictionData.length !== 0 )){
      return(
          <div className="table-responsive fixed-height-table">
            <table className="table table-bordered data-table insights-table">
              <thead>
                <tr>
                  <td>Date</td>
                  {this.state.predictionData &&
                    this.state.predictionData[0] &&
                    this.state.predictionData[0].prediction_input.map(key => {
                      return <td key={key["name"]}>{key["name"]}</td>;
                    })}
                  <td />
                </tr>
              </thead>
              <tbody>
                {this.state.predictionData &&
                  this.state.predictionData.map((obj, index) => {
                    return (
                      <tr key={index}>
                        <td className="mwd150"><span>{moment(obj.prediction_date).format('llll')}</span></td>
                        {this.getColumns(obj)}
                      </tr>
                    );
                  })}
              </tbody>
            </table>
          </div>)
    }else{
      return(
        <span>No data found</span>
      )
    }
  }

  render() {
    const { dataDrift, dataDriftQuality,tnrSignificance,predictionInputFilters } = this.state;
    return (
      <div className="runtime-content">
        <div className="data-section">
          <div className="row">
            <div className="col-md-6">
              <div className="data-sec-head">
                <h4>Data Drift</h4>
                <div className="data-sec-right">
                  <div className="health-score-counter">
                    <span className={this.addScoreTextClassDrift(this.props.modelDetails.data_drift)}>{this.props.modelDetails.data_drift}</span>
                  </div>
                </div>
              </div>
              <div className="data-section-body">
                <Tabs>
                  <TabList className="inline-tabs tabs-gray">
                    <Tab
                      onClick={() =>
                        this.getChartData({
                          timestampGreaterOrEqualThan: "now-1d"
                        })
                      }
                    >
                      LAST 1 DAY
                    </Tab>
                    <Tab
                      onClick={() =>
                        this.getChartData({
                          timestampGreaterOrEqualThan: "now-7d"
                        })
                      }
                    >
                      LAST 7 DAYS
                    </Tab>
                    <Tab
                      onClick={() =>
                        this.getChartData({
                          timestampGreaterOrEqualThan: "now-30d"
                        })
                      }
                    >
                      LAST MONTH
                    </Tab>
                  </TabList>
                  <TabPanel>
                    <DataDriftChart data={dataDrift} dayOne={true} />
                  </TabPanel>
                  <TabPanel>
                    <DataDriftChart data={dataDrift} />
                  </TabPanel>
                  <TabPanel>
                    <DataDriftChart data={dataDrift} />
                  </TabPanel>
                </Tabs>
              </div>
            </div>

            <div className="col-md-6">
              <div className="data-sec-head">
                <h4>Data Drift / Quality</h4>
              </div>
              <div className="data-section-body fixed-height-table">
                {(dataDriftQuality && dataDriftQuality.length > 0) ? (
                  <table className="table table-bordered data-table insights-table">
                    <thead>
                      <tr>
                        <td>Date</td>
                        <td>Insight</td>
                      </tr>
                    </thead>
                    <tbody>
                      {dataDriftQuality &&
                        dataDriftQuality.map((driftItem, index) => {
                          return (
                            <tr key={index}>
                              <td>{moment(driftItem.timestamp).format('llll')}</td>
                              <td>{driftItem.reason}</td>
                            </tr>
                          );
                        })}
                    </tbody>
                  </table>
                  ) : (<div className="no-data-msg"><p className="text-center text-gray">No data for DataDrift/Quality</p></div>)
                }
              </div>
            </div>
          </div>
        </div>

        <hr />

        <TNRSignificanceFeature
          modelID={this.props.modelDetails.id}
        />
        <hr />

        <div className="data-section">
          <div className="data-sec-head">
            <h4>Prediction Input</h4>
          </div>
          <div className="action-row">
            <div className="action-left-col">
              <button
                type="button"
                className="btn btn-primary btn-sm btn-rounded"
                data-micromodal-trigger="prediction-filter-modal"
              >
                ADD FILTER +
              </button>
            </div>
            <div className="applied-filters">
              <ul className="filter-items-list">
                { predictionInputFilters.output.output_value &&
                  <li className="filter-tag-item">
                    output-{predictionInputFilters.output.output_value}
                    <i className="fa fa-times" style={{marginTop: 3}} onClick={() => this.handleRemoveFilter('output')}>
                    </i>
                  </li>
                }

                { predictionInputFilters.probability.probability_value &&
                  <li className="filter-tag-item">
                    probability-{predictionInputFilters.probability.probability_value}
                    <i onClick={() => this.handleRemoveFilter('probability')}>
                      <img
                        src={CloseImage}
                        alt="close"
                        width="10"
                        height="10"
                      />
                    </i>
                  </li>
                }
                { predictionInputFilters.inputs &&
                  predictionInputFilters.inputs[0] &&
                  predictionInputFilters.inputs[0].value &&
                  predictionInputFilters.inputs.map((input,index) => {
                  return(
                    <li className="filter-tag-item" key={index}>
                      {input.field}-{input.value}
                      <i onClick={() => this.handleRemoveFilter('inputs', input.id)}>
                        <img
                          src={CloseImage}
                          alt="close"
                          width="10"
                          height="10"
                        />
                      </i>
                    </li>
                  )})
                }
                { predictionInputFilters.reasons &&
                  predictionInputFilters.reasons[0] &&
                  predictionInputFilters.reasons[0].value &&
                  predictionInputFilters.reasons.map((reason,index) => {
                  return(
                    <li className="filter-tag-item" key={index}>
                      {reason.field}-{reason.value}
                      <i onClick={() => this.handleRemoveFilter('reasons', reason.id)}>
                        <img
                          src={CloseImage}
                          alt="close"
                          width="10"
                          height="10"
                        />
                      </i>
                    </li>
                  )})
                }
                { predictionInputFilters.properties &&
                  predictionInputFilters.properties[0] &&
                  predictionInputFilters.properties[0].value &&
                  predictionInputFilters.properties.map((property,index) => {
                  return(
                    <li className="filter-tag-item" key={index}>
                      {property.field}-{property.value}
                      <i onClick={() => this.handleRemoveFilter('properties', property.id)}>
                        <img
                          src={CloseImage}
                          alt="close"
                          width="10"
                          height="10"
                        />
                      </i>
                    </li>
                  )})
                }
                { predictionInputFilters.dateRange &&
                  predictionInputFilters.dateRange.startDate &&
                  <li className="filter-tag-item">
                    DateRange: {moment(predictionInputFilters.dateRange.startDate).format('L')} - {moment(predictionInputFilters.dateRange.endDate).format('L')}
                    <i onClick={() => this.removeDateRange()}>
                      <img
                        src={CloseImage}
                        alt="close"
                        width="10"
                        height="10"
                      />
                    </i>
                  </li>
                }
              </ul>
            </div>
            <div className="action-right-col right-select-actions">
              <div className="right-col-content">
                <select
                  className="form-control select-field"
                  name="timestamp"
                  onChange={this.changeTimestamp}
                  value={predictionInputFilters.timestamp || ""}
                >
                  <option value="">Select Timestamp</option>
                  <option value="now-1d">Last 1 Day</option>
                  <option value="now-7d">Last 7 Days</option>
                  <option value="now-30d">Last Month</option>
                </select>
                <DateRangePicker 
                  startDate={new Date()} 
                  endDate={new Date()}
                  onApply={this.handleEvent}>
                  <button className="btn btn-primary btn-sm btn-rounded">Select Date Range</button>
                </DateRangePicker>
              </div>
            </div>
          </div>
          {this.renderPredictionInput()}
        </div>
      <PredictionInputFilter
        handleFilter={this.handlePredictionInputFilterData}
        modelID={this.props.modelDetails.id}
        defaultFilters={this.state.predictionInputFilters}
      />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token
  };
};

export default connect(mapStateToProps)(Datadrift);
