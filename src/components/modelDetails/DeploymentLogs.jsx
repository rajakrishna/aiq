import React, { Component } from "react";
import { connect } from "react-redux";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { Loader1 } from "../home/modelOverviewLoader";
class DeploymentLogs extends Component {

    state = {
        rotate: 0
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if(nextProps.updated != prevState.updated) {
            return ({
                rotate: 0,
                updated: nextProps.updated
            })
        }
        return ({
            updated: nextProps.updated
        });
    }

    getTabs = () => {
        let i = 0;
        const elemArr = [];
        while (i < this.props.replicas) {
            elemArr.push(
                <Tab key={"replica" + (i + 1)}>
                    {"Replica " + (i + 1)}
                </Tab>
            )
            i++;
        }
        return elemArr;
    }

    getPanels = () => {
        let i = 0;
        const elemArr = [];
        while (i < this.props.replicas) {
            elemArr.push(
                <TabPanel key={"panel" + i}>
                    <Tabs>
                        <TabList className="inline-tabs tabs-gray pd-0">
                            {Object.keys(this.props.logs).map((e, i) => {
                                return (
                                    <Tab key={e + i}>
                                        {e}
                                    </Tab>
                                )
                            })}
                        </TabList>
                        {Object.keys(this.props.logs).map((e, i) => {
                            return (
                                <TabPanel key={"logpanel"+i}>
                                    <div className="relative">
                                        <div className="copy-code">
                                            <pre className="mt-lg" style={{
                                                minHeight: 250,
                                                maxHeight: 287
                                            }}>
                                                {this.props.logs[e]}
                                            </pre>
                                        </div>
                                        <span className="clickable copy-btn mt mr" onClick={()=>{this.setState({rotate: 1}); this.props.getLogs(this.props.index, 0)}}>
                                            <i className={`fa fa-sync ${this.state.rotate && "rotate"}`}></i>
                                        </span>
                                    </div>
                                </TabPanel>
                            )
                        })}
                    </Tabs>
                </TabPanel>
            )
            i++;
        }
        return elemArr;
    }


    showLoader = () => {
        let i = 0;
        const elemArr = [];
        while (i < this.props.replicas) {
            elemArr.push(
                <TabPanel key={"loader" + (i + 1)}>
                    {this.props.logs ? 
                        <div className="empty-items-box">
                            <h4>No Logs found!</h4>
                        </div> : 
                        <Loader1 />
                    }
                </TabPanel>
            )
            i++;
        }
        return elemArr;
    }

    render() {
        return (
            <div className="mt-xl">
                <Tabs onSelect={(a, b) => { this.props.getLogs(a) }} defaultIndex={this.props.index}>
                    <TabList className="inline-tabs tabs-blue pd-0">
                        {this.getTabs()}
                    </TabList> 
                    {this.props.logs && Object.keys(this.props.logs).length ? this.getPanels() : this.showLoader()}
                </Tabs>
            </div>
        )
    }

}

const mapStateToProps = state => {
    return {
        token: state.auth.token
    };
};

export default connect(mapStateToProps)(DeploymentLogs);
