import React, {Component} from 'react';
import Select from 'react-select';
import {filter,sortBy} from 'lodash'

class DatasetMetrics extends Component {
  state = {
    selectedOptions: [],
    datasetColumns: this.props.datasetColumns? this.props.datasetColumns : [],
    selectedDatasetColumns: this.props.datasetColumns? this.props.datasetColumns : [],
    title: this.props.title? this.props.title : "Datasets",
    allColumns: [],
    sortHeaders: [],
    sortedBy: null,
  }

  constructor(props){
    super(props);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const  datasetColumns  = nextProps.datasetColumns;
    if (datasetColumns &&  (datasetColumns.length > 0) && !prevState.allColumns.length){
      var allColumns = []
      var sortHeaders = []

      datasetColumns.map(function(column){
          const item =   { value: column.name, label: column.name }
          allColumns.push(item)
      })

      {datasetColumns[0].metrics.map((col) => {
        sortHeaders.push(col.name)
      })}

      return({
        selectedOptions: [],
        datasetColumns: nextProps.datasetColumns? nextProps.datasetColumns : [],
        selectedDatasetColumns: nextProps.datasetColumns? nextProps.datasetColumns : [],
        title: nextProps.title? nextProps.title : "Datasets",
        allColumns: allColumns,
        sortHeaders: sortHeaders,
      });
    }
    return null;
  }

  handleSelect = (selectedOptions)=> {
    this.setState({ selectedOptions });
    this.filterColumns(selectedOptions)
  }
  filterColumns = (selectedOptions)=> {
    const keys = selectedOptions.map(function(option){return option.value})
    var selectedDatasetColumns = this.state.datasetColumns
    if (keys && keys.length > 0){
      selectedDatasetColumns  = filter(this.state.datasetColumns,function(column){
        return (keys.indexOf(column.name) >= 0 )
      })
    }
    this.setState({selectedDatasetColumns})
  }

  sortByHeaders = (e) => {
    var header = e.target.value;
    if (header){
    var { selectedDatasetColumns} = this.state;
      var keys = []
      {selectedDatasetColumns[0].metrics.map((col) => {
        keys.push(col.name)
      })}
      selectedDatasetColumns = sortBy(selectedDatasetColumns,function(dc){
        if(dc.metrics[keys.indexOf(header)])
          return dc.metrics[keys.indexOf(header)].value
      })
      this.setState({selectedDatasetColumns: selectedDatasetColumns,sortedBy: header })
    }
  }

  renderDatasetMetrics = (datasetColumns) => {
    if ((datasetColumns == null) || (datasetColumns.length == 0)){
      return(
        <div>No records</div>
      )
    }
    return(
      <div className="dataset-table fixed-height-table">
        <table className="table table-bordered data-table">
          <thead>
            <tr>
              <td>Features</td>
              {datasetColumns[0].metrics.map((col, index) => {
                return <td key={"features"+index}>{col.name}</td>;
              })}
            </tr>
          </thead>
          <tbody>
            {datasetColumns.map((item, index) => {
              return (
                <tr key={"dscol"+index}>
                  <td>{item.name}</td>
                  {item.metrics.map((i, metrixIndex) => (
                    <td key={metrixIndex}>{i.value}</td>
                  ))}
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }

  render(){
    const { selectedDatasetColumns,title,allColumns,selectedOptions,sortHeaders,sortedBy} = this.state;
    return(
      <div className="data-section pl-0">
        <div className="data-sec-head">
          <h3>{title}</h3>
        </div>

        <div className="row" style={{marginBottom: 20}}>
          <div className="col-md-9">
            <label>Features</label>
            <Select
              value={selectedOptions}
              onChange={this.handleSelect}
              options={allColumns}
              multi
              isSearchable={true}
            />
          </div>
          <div className="col-md-3">
            <label>Sort By</label>
            <select
                className="form-control select-field"
                onChange={this.sortByHeaders}
                defaultValue={sortedBy}
              >
                <option value={null}>Select...</option>;
                {sortHeaders.map((header,i) => {
                  return <option key={"sortby"+i} value={header}>{header}</option>;
                })}
              </select>
          </div>
        </div>

        {this.renderDatasetMetrics(selectedDatasetColumns)}
        <hr />
    </div>
    )
  }
}

export default DatasetMetrics;
