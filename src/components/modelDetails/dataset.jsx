import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { getDatasetById,getDatasetSampleRecords } from "../../api";
import { Loader1 } from "../home/modelOverviewLoader";
import ColumnCorrelation from "../chart/ColumnCorrelation";
import DatasetMetrics from "./datasetMetrics"
import {find,remove} from 'lodash';
import { getErrorMessage } from "../../utils/helper";
import { alertMsg } from "../../ducks/alertsReducer";
import moment from "moment";

class Dataset extends Component {
  _isMounted = false;
  state = {
    loading: 0,
    datasets: [],
    rawDatasets: [],
    testDatasets: []
  };

  async componentDidMount() {
    this._isMounted = true;
    this.getDatasets( "trainingSampleReport", "datasets", this.props.modelDetails.training_dataset_id );
    if(this.props.modelDetails.test_dataset_id) {
      this.getDatasets( "testDatasets", "datasets", this.props.modelDetails.test_dataset_id );
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getDatasets = (type="", dataset="" ,id="") => {
    this.setState({loading: 1});
    if (id) {
      getDatasetById(this.props.auth.token, id)
        .then(response => {
          if (response.status && this._isMounted) {
            this.setState({ [dataset]: response.data});
          }
        })
        .catch(error => {
          if(this._isMounted) {
            console.log(error);
            this.props.alertMsg("Unable to fetch Datasets: "+getErrorMessage(error), "error");
          }
        })
        .finally(()=>{
          getDatasetSampleRecords(this.props.auth.token, id)
          .then(response => {
            if (response.status && this._isMounted) {
              this.setState({ [type]: response.data, loading: 0});
            }
          })
          .catch(error => {
            if(this._isMounted) {
              this.setState({loading: 0});
              console.log(error);
              this.props.alertMsg("Unable to fetch Sample Report: "+getErrorMessage(error), "error");
            }
          });
        })
    }
  };

  renderDatasetMetrics = (datasets) => {
    return(
      <div className="dataset-table fixed-height-table">
        <table className="table table-bordered data-table">
          <thead>
            <tr>
              <td>Features</td>
              {datasets.columns[0].metrics.map((col, index) => {
                return <td key={index}>{col.name}</td>;
              })}
            </tr>
          </thead>
          <tbody>
            {datasets.columns.map((item, index) => {
              return (
                <tr key={index}>
                  <td>{item.name}</td>
                  {item.metrics.map((i, metrixIndex) => (
                    <td key={metrixIndex}>{i.value}</td>
                  ))}
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }

  renderSampleRecord = (sampleRecord) =>{
    if (sampleRecord && sampleRecord.length > 2){
      var tableHeaders = sampleRecord[0]
      var dataRecords = remove(sampleRecord,0)
      return(
        <div className="data-section pl-0">
          <div className="data-sec-head">
            <h3>Sample Records</h3>
          </div>
          <div style={{"overflowX": "scroll"}} className="fixed-height-table" width="900" height="350">
            <table className="table table-bordered data-table">
              <thead>
                <tr>
                  {tableHeaders.map((headings,index) =>{
                    return <td key={index}>{headings}</td>
                  })}
                </tr>
              </thead>
              <tbody>
                {dataRecords.map((row,rowIndex) => {
                  if(rowIndex)
                  return(<tr key={rowIndex}>
                    {row.map((column,columnIndex) => {
                     return <td key={columnIndex} id={columnIndex}>{column}</td>
                    })}
                  </tr>)
                })}
              </tbody>
            </table>
          </div>
        </div>
      )

    }
    return
  }

  renderMetaInfo = (modelData) => {
    if(modelData && modelData.filename && modelData.metrics){
      let column_metric = find(modelData.metrics,function(m){return m.name === "columns"})
      let row_metric = find(modelData.metrics,function(m){ return m.name === "rows"})
      return(
        <div className="data-section">
          <div>
            <table className="table data-table no-border modeldetails-table">
              <thead>
                <tr>
                  <td>Filename</td>
                  <td>Uploaded By</td>
                  <td>Uploaded Date</td>
                  <td>Upload duration</td>
                  <td>No of row</td>
                  <td>Now of columns</td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{modelData.filename}</td>
                  <td>{modelData.created_by}</td>
                  <td>{ moment(modelData.created_date).format("LL")}</td>
                  <td>{modelData.upload_duration}</td>
                  <td>{ row_metric &&  row_metric.value}</td>
                  <td>{column_metric && column_metric.value}</td>

                </tr>
              </tbody>
            </table>
          </div>
          <hr />
        </div>
      );
    }
  }

  render() {
    const { datasets, testDatasets, trainingSampleReport,testSampleReport } = this.state;
    const { modelDetails } = this.props;
    // let displayRawDataSet = this.props.modelDetails.raw_dataset_id == null ? "none" : "";
    let displayTrainingDataSet = this.props.modelDetails.training_dataset_id;
    // let displayTestDataSet =
    //   this.props.modelDetails.test_dataset_id == null ? "none" : "";
  
    return (
      <div className="dataset-content pl-4">
        <Tabs>
          <TabList className="inline-tabs tabs-gray tabs-md pad-50 pl-0">
            <Tab onClick={() => this.getDatasets( "trainingSampleReport", "datasets", this.props.modelDetails.training_dataset_id )}>
              TRAINING DATASET
            </Tab>
            <Tab onClick={() => this.getDatasets( "testSampleReport", "testDatasets", this.props.modelDetails.test_dataset_id )}>
              TEST DATASET
            </Tab>
          </TabList>
          <TabPanel>
            {this.state.loading ? <Loader1 /> : ""}
            {this.renderMetaInfo(datasets)}
            
            {modelDetails.training_dataset_id &&
              datasets &&
              displayTrainingDataSet ? 
              (
                <DatasetMetrics
                  datasetColumns={datasets.columns}
                  title={"Dataset Metrics"}
                />
              ): this.state.loading ? "" : 
                <div className="empty-items-box">
                  No Dataset Metrics found!
                </div>
              }
            
            {datasets && datasets.column_correlations && (
              <ColumnCorrelation
                testDatasets={datasets && datasets.column_correlations}
              />)
            }
            
            {this.renderSampleRecord(trainingSampleReport)}

          </TabPanel>
          <TabPanel>
            {this.state.loading ? <Loader1 /> : ""}
            {this.renderMetaInfo(testDatasets)}
            
            {modelDetails.test_dataset_id &&
              testDatasets ?
              (
                <DatasetMetrics 
                  datasetColumns={testDatasets.columns} 
                  title={"Dataset Metrics"} 
                />
              ): ""}
            
            {testDatasets && testDatasets.column_correlations && testDatasets.length !== 0 && (
              <ColumnCorrelation
                testDatasets={testDatasets.column_correlations}
              />)
            }
            {this.renderSampleRecord(testSampleReport)}
          </TabPanel>
        </Tabs>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    auth: state.auth,
    displayRawDataSet: state.modelDetails.raw_dataset_id == null ? "none" : "",
    displayTrainingDataSet:
      state.modelDetails.training_dataset_id == null ? "none" : "",
    displayTestDataSet: state.modelDetails.test_dataset_id == null ? "none" : ""
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
      {
          alertMsg
      },
      dispatch
  );
};


export default connect(mapStateToProps, mapDispatchToProps)(Dataset);
