import React, { Component, createRef } from "react";
import { Tooltip, CircularProgress } from "@material-ui/core";

export default class RequirementEditor extends Component {
    state = {
        req: this.props.req,
        editable: this.props.editable,
        loader: false,
        expanded: false
    }

    handleChange = (e) =>{
        let val = e.target.value;
        val = val ? val.split('\n').join(',') : "";
        this.setState({
            req: val
        });
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        const expanded = nextProps.expanded,
              editable = nextProps.editable;
            return({
                expanded,
                editable
            })
        return null;
    }

    submitReq = () => {
        this.setState({ 
            loader: true
        },
        this.props.saveRequirements(this.props.id, this.state.req,
            ()=>{
                this.setState({ 
                    loader: false
                })
            },
            ()=>{
                this.setState({ 
                    loader: false
                })
            })
        )
    }

    render(){
        const { editable, req, loader, expanded } = this.state;
        return(
            <div>
                <div 
                    className="notesContainer f-13 text-dark-grey pd"
                    style={expanded ? 
                        {height: 300, width: "100%", border: "1px solid #ccc"} :
                        {height: 41, width: "100%", border: "1px solid #ccc"}
                    }
                >
                    {editable ?
                    <textarea
                        onChange={this.handleChange}
                        style={{height: 280}}
                        className="form-control p-0 no-border"
                        value={req && req.split(",").join("\n")}
                    >
                    </textarea> :
                    expanded ? req && req.split(",").map(e=>(<div>{e}</div>)) : req}
                </div>
                {this.props.userName === this.props.owner ?
                expanded &&
                <div className="mt">
                    {!editable ? 
                        <button
                            className="btn btn-sm btn-default"
                            type="button"
                            onClick={()=>{
                                const condition = editable;
                                if(this.props.userName !== this.props.owner) {
                                    return;
                                }
                                this.props.handleReqExpand(!condition, true)
                            }}
                        >
                            <i className="fa fa-edit" /> Edit
                        </button>
                    : 
                    <button
                        className="btn btn-sm btn-default"
                        type="button"
                        onClick={()=>{
                            const condition = editable;
                            this.props.handleReqExpand(!condition)
                            this.submitReq();
                        }}
                    >
                        <i className="fa fa-save" /> Save {loader && <CircularProgress size={13} disableShrink />}
                    </button>}
                </div> : ""}
            </div>
        )
    }
}