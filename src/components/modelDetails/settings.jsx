import React, { Component } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import {
  getBiasByModelId, updateBias
} from "../../api";
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-daterangepicker/daterangepicker.css';
import Button from '@material-ui/core/Button';

class Settings extends Component {
  _isMounted = false;
  state = {
    bias: [],
    featureDistributions: [],
    biasUpdated: "",
    errorMsg: "",
    filters: {
      model_id: this.props.modelDetails.id,
      model_name: this.props.modelDetails.name,
      project_id: this.props.modelDetails.project_id,
      project_name: this.props.modelDetails.project_name,
      output: '',
      features: [{
        id: 0,
        name: '',
        value: '',
        distribution_threshold: ''
      }]

    },
  };

  async getBias() {
    getBiasByModelId(this.props.token, this.props.modelDetails.id)
      .then(res => {
        const biases = res.data.filter(bias => bias.active === 'Yes');
        if (biases && biases.length > 0 && this._isMounted) {
          const bias = biases[0];
          this.setState({ bias: bias });
          this.setState({ filters: bias });
        } else {
          console.log('No active biases found');
        }
      })
      .catch(err => {
        console.log('Error fetching Bias Config:', err);
      });
  }

  // defaultBias = () => {
  //   this.setState({ filters: this.state.bias });
  // }
  
  async componentDidMount() {
    this._isMounted = true;
    this.getBias();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  updateBias = () => {
    let { filters } = this.state;
    // console.log("filter: ", filters)

    updateBias(this.props.token, filters)
      .then(response => {
        if (response.status) {
          this.setState({ biasUpdated: "Bias updated sucessfully" });
        }
      })
      .catch(error => this.setState({ errorMsg: error.message }));
  };

  predictionOutputChange = e => {
    let filters = { ...this.state.filters };
    filters.output = e.target.value.replace("-", "|");
    this.setState({ filters });
  };

  predictionInputChange = (e, index) => {
    let filters = { ...this.state.filters };
    filters.features[index][e.target.name] = e.target.value.replace("-", "|");
    this.setState({ filters });
  };

  addPredictionInput = () => {
    let filters = this.state.filters;
    filters.features.push({
      id: Math.random(),
      name: '',
      value: '',
      distribution_threshold: ''
    })
    this.setState({ filters })
  }

  deletePredictionInput = (id) => {
    let filters = this.state.filters;
    const features = filters.features.filter((item) => item.id !== id);
    filters.features = features;
    this.setState({ filters })
  }

  render() {
    // console.log("default filter: ", this.state.filters)

    const featureOptions = this.props.modelDetails &&
      this.props.modelDetails.feature_significance &&
      this.props.modelDetails.feature_significance.map(feature =>
        <option key={feature.name} value={feature.name}>{feature.name}</option>
      ) || [];

    // console.log('Render - this.state: ', this.state);
    // console.log('Render - bias: ', bias);

    return (
      // <div className="runtime-content">
      // <div className="data-section">
      <div className="row">
        <div className="col-md-6">
          <div className="data-sec-head">
            <h4>Bias Configuration</h4>
          </div>
          <div className="data-section-body">

            <div className="form-group inline-input input-group-field">
              <input
                type="string"
                name="output_value"
                className="form-control input-sm"
                placeholder="output"
                onChange={this.predictionOutputChange}
                defaultValue={this.state.bias.output}
                // value={this.state.filters.output}
              />
            </div>

            <button
              type="button"
              className="pull-right btn btn-primary btn-sm btn-rounded"
              onClick={() => this.addPredictionInput()}
            >
              +
          </button>
            <br />
            <br></br>
            <div>
              {this.state.filters && this.state.filters.features.map((input, index) => {
                return (
                  <div className="form-group inline-input input-group-field" key={index}>
                    {/* <div className="col-md-4"> */}
                      <select name="name" style={{width: '100%'}}
                        className="form-control select-field"
                        onChange={(e) => this.predictionInputChange(e, index)}
                        defaultValue={input.name}
                      >
                        <option value="" disabled> {input.name}</option>
                        {featureOptions}
                      </select>
                    {/* </div> */}
                    <div className="col-md-4">
                      <input
                        type="string"
                        name="value"
                        className="form-control input-sm"
                        placeholder="value"
                        onChange={(e) => this.predictionInputChange(e, index)}
                        defaultValue={input.value}
                      />
                    </div>
                    <div className="col-md-4">
                      <input
                        type="string"
                        name="distribution_threshold"
                        className="form-control input-sm"
                        placeholder="threshold"
                        onChange={(e) => this.predictionInputChange(e, index)}
                        defaultValue={input.distribution_threshold}
                      />
                    </div>
                    <button
                      className="modal__close"
                      onClick={() => this.deletePredictionInput(input.id)}
                    >X</button>
                  </div>
                )
              })
              }
            </div>

            <div>
              <Button variant="contained" size="small" color="primary" type="submit" onClick={this.updateBias}>
                save
              </Button> 
              
              {/* &nbsp; &nbsp; */}

              {/* <Button variant="contained" size="small" color="" type="submit" onClick={this.defaultBias}>
                Restore defaults
              </Button> */}
            </div>
          </div>
        </div>
      </div>
      // </div>
      // </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token
  };
};

export default connect(mapStateToProps)(Settings);
