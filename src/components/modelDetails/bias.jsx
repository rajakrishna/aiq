import React, { Component } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import { getBiasByModelId, getAllInsights } from "../../api";
import { getFeatureDistributionsByModelId, getFeatureDistributionsByType, getAvgFeatureDistributionsByType } from "../../api";
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-daterangepicker/daterangepicker.css';
import { FeatureDistributionChart } from "../chart/FeatureDistributionChart";
import Settings from "../modelDetails/settings";
import moment from 'moment';
import { func } from "prop-types";

class Bias extends Component {
  _isMounted = false;
  state = {
    bias: undefined,
    biasData: [],
    featureDistribution1: [],
    featureDistribution2: [],
    featureDistribution3: [],
  };

  async getBias() {
    getBiasByModelId(this.props.token, this.props.modelDetails.id)
      .then(res => {
        const biases = res.data.filter(bias => bias.active === 'Yes');
        if (biases && biases.length > 0 && this._isMounted) {
          const bias = biases[0];
          this.getFeatureDistribution(bias.id, bias.features[0].name);
          this.setState({ bias: bias });
        } else {
          console.log('No active biases found');
        }
      })
      .catch(err => {
        console.log('Error fetching Bias Config:', err);
      });
  }

  async getBiasTableData() {
    getAllInsights(this.props.token, {
      modelIdEquals: this.props.modelDetails.id,
      pointsCategoryEquals: "bias",
      sort: "timestamp,desc"
    }).then(response => {
      if (response.status && this._isMounted) {
        this.setState({ biasData: response.data });
      }
    })
      .catch(error => {
        console.log("Error while fetching Insights for category bias: ", error)
      });
  }

  async getFeatureDistribution(biasId, biasFeature) {
    getFeatureDistributionsByModelId(this.props.token, this.props.modelDetails.id, biasId, biasFeature)
      .then(res => {
        const data = res.data;
        this.setState({ featureDistribution1: data });
      })
      .catch(err => console.log('Error fetching feature distributions: ', err));

    getFeatureDistributionsByType(this.props.token, this.props.modelDetails.id, biasId, biasFeature, 'train')
      .then(res => {
        const data = res.data;
        this.setState({ featureDistribution3: data });
      })
      .catch(err => console.log('Error fetching feature distributions: ', err));

    getAvgFeatureDistributionsByType(this.props.token, this.props.modelDetails.id, biasId, biasFeature, 'runtime')
      .then(res => {
        const data = _.pickBy(res.data, function (value) {
          return value !== null && value !== undefined && value !== "Infinity";
        });
        this.setState({ featureDistribution2: data });
      })
      .catch(err => console.log('Error fetching feature distributions: ', err));
  }

  componentDidMount() {
    this._isMounted = true
    this.getBias();
    this.getBiasTableData();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getFeatureDistributionOnEvent = event => {
    this.getFeatureDistribution(this.state.bias.id, event.target.value);
  }

  render() {
    const { bias, biasData, featureDistribution1, featureDistribution2, featureDistribution3 } = this.state;

    let data = []

    // console.log('Render - state: ', this.state);

    const featureOptions = bias &&
      bias.features &&
      bias.features.map(feature =>
        <option key={feature.name} value={feature.name}>{feature.name}</option>
      ) || [];

    if (!featureDistribution1 && !featureDistribution2 && !featureDistribution3) {
      console.log("issue while fetching data")
    }

    let runtimeDistribution = { "name": "runtime distribution", "data": featureDistribution2 }

    if (Object.entries(runtimeDistribution).length > 0) {
      const thresholdData = []
      if (featureDistribution2 && featureDistribution2[0]) {
        for (let index = 0; index < Object.entries(featureDistribution2).length; index++) {
          thresholdData.push([Object.entries(featureDistribution2)[index][0], featureDistribution2[index].threshold])
        }
      }
      let runtimeThreshold = { "name": "threshold", "data": thresholdData }

      const trainDistributionData = []
      if (featureDistribution3 && featureDistribution3[0]) {
        for (let index = 0; index < Object.entries(featureDistribution2).length; index++) {
          trainDistributionData.push([Object.entries(featureDistribution2)[index][0], featureDistribution3[0].distribution])
        }
      }
      let trainDistribution = { "name": "train distribution", "data": trainDistributionData }

      data.push(runtimeThreshold, runtimeDistribution, trainDistribution)
    }
    else {
      const thresholdData = []
      if (featureDistribution1 && featureDistribution1[0]) {
        for (let index = 0; index < featureDistribution3.length; index++) {
          thresholdData.push([featureDistribution3[index].timestamp, featureDistribution1[index].threshold])
        }
      }
      let runtimeThreshold = { "name": "threshold", "data": thresholdData }

      const trainDistributionData = []
      if (featureDistribution3 && featureDistribution3[0]) {
        for (let index = 0; index < featureDistribution3.length; index++) {
          trainDistributionData.push([featureDistribution3[index].timestamp, featureDistribution3[0].distribution])
        }
      }
      let trainDistribution = { "name": "train distribution", "data": trainDistributionData, "color": "green" }

      data.push(runtimeDistribution, runtimeThreshold, trainDistribution)
    }

    return (
      <div className="runtime-content">
        <div className="data-section">
          <div className="row">
            <div className="col-md-6">
              <div className="data-sec-head">
                <h4>Bias</h4>
              </div>
              <div className="data-section-body">
                <div className="action-right-col right-select-actions">
                  <div className="right-col-content">
                    <table className="table data-table no-border modeldetails-table">
                      <thead>
                        <tr>
                          <td>Feature</td>
                          <td>Value</td>
                          <td>Output</td>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                            <select className="form-control select-field" onChange={this.getFeatureDistributionOnEvent}>
                              {featureOptions}
                            </select>
                          </td>
                          <td>{featureDistribution1[0] && featureDistribution1[0].value.replace("|", " to ")}</td>
                          <td>{featureDistribution1[0] && featureDistribution1[0].output.replace("|", " to ")}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div>
                  <FeatureDistributionChart data={data} />
                </div>

              </div>
            </div>

            <div className="col-md-6">
              <div className="data-sec-head">
                <h4>Bias Insights</h4>
              </div>
              <div className="data-section-body">
                <div className="fixed-height-table fixed-content-box">
                  <table className="table table-bordered data-table insights-table">
                    <thead>
                      <tr>
                        <td>Date</td>
                        <td>Reasons</td>
                      </tr>
                    </thead>
                    <tbody>
                      {biasData &&
                        biasData.map((item, index) => {
                          return (
                            <tr key={index}>
                              <td>{moment(item.timestamp).format('llll')}</td>
                              <td>{item.reason}</td>
                            </tr>
                          );
                        })}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div>
            <hr />
            <Settings modelDetails={this.props.modelDetails}></Settings>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token
  };
};

export default connect(mapStateToProps)(Bias);
