import React, { Component } from 'react';
import ReactTooltip from 'react-tooltip'
import Avatar from '@material-ui/core/Avatar';


function getInitialsEmail(email="") {
  email = email || "";
  const first = email.replace(/^(\w).*/,'$1');
  const second = email.replace(/^\w.*?\.([A-z]).*?@.*?$/,'$1');
  const str = second.length > 1 ? first : first+second;
  return str.toUpperCase() || '?';
}

function User(props) {
  return (
    <span data-tip='' data-for={props.user} className="no-border pd-0">
      <Avatar className="avatar">{getInitialsEmail(props.user)}</Avatar>
      {props.isMenu ? <i className="fa fa-angle-down ml-sm" /> : ''}
      <ReactTooltip id={props.user} place="bottom" type="dark" getContent={() => { return props.user }}/>
    </span>
  )
}

export default User;
