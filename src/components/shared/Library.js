import React from 'react';
import _ from 'lodash';
import azure from '../../images/library/azurelogo.png'
import sklearn from '../../images/library/sklearnlogo.png'
import R from '../../images/library/Rlogo.png'
import TeansorFlow from '../../images/library/tensorflowlogo.jpg'
import PyTorch from '../../images/library/pytorchlogo.png'
import Python from '../../images/library/pythonlogo.png'
import Keras from '../../images/library/keraslogo.png'
import H2O from '../../images/library/h2ologo.png'
import ReactTooltip from 'react-tooltip'

const Library = (props) => {
  let image = sklearn
  let library = (_.isEmpty(props.library)) ? "" : props.library.toUpperCase();
  let count = props.count;
  switch (library) {
    case "SKLEARN":
      image = sklearn
      break;
    case "AZURE":
      image = azure
      break;
    case "KERAS":
      image = Keras
      break;
    case "R":
      image = R
      break;
    case "TENSORFLOW":
      image = TeansorFlow
      break;
    case "PYTORCH":
      image = PyTorch
      break;
    case "PYTHON":
    case "PYTHON2":
    case "PYTHON3":
      image = Python
      break;
    case "H2O":
      image = H2O
      break;
    default:
      image = sklearn
  }

  if (!(/base64/g).test(image)) {
    var re = RegExp('\^' + process.env.PUBLIC_URL, 'g');
    image = process.env.REACT_APP_BASE_URL.replace(/\/$/g, '') + image.replace(re, '');
  }

  return (
    <li data-tip='' data-for={image}><img src={image} alt={image} />
      <ReactTooltip id={image} place="bottom" type="dark" getContent={() => { return library.toLowerCase() + " " + (count || "") }} />
    </li>
  )
}

export default Library;
