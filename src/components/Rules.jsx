import React, { Component } from 'react';
import { connect } from "react-redux";
import Group from './Group';
import { Link } from "react-router-dom";
import {
    getBiasByModelId,
    getAllJobs
  } from "../api";

class Rules extends Component {
    _isMounted = false;
    state = {
        groups: [],
        togglerAnd: true,
        togglerOr: false,
        bias: null,
        actionType: 1,
        actionInp: "",
        inpHolder: "URL",
        inpType: "Webhook URL",
        jobs: []
    }
    constructor(props) {
        super(props);
        this.deleteGrp = this.deleteGrp.bind(this);
        this.addGrp = this.addGrp.bind(this);
        this.handleToggle = this.handleToggle.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.getBias = this.getBias.bind(this);
        this.getJobs = this.getJobs.bind(this);
    }
    
    componentDidMount() {
        this._isMounted = true;
        this.getBias();
        this.getJobs();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }
    
    getBias() {
        getBiasByModelId(this.props.token, this.props.modelDetails.id)
        .then(res => {
            const biases = res.data.filter(bias => bias.active === 'Yes');
            if (biases && biases.length > 0 && this._isMounted) {
                const bias = biases[0];
                this.setState({ bias: bias });
            } else {
                console.log('No active biases found');
            }
        })
        .catch(err => {
            console.log('Error fetching Bias Config:', err);
        });
    }

    getJobs() {
        getAllJobs(this.props.token, {}, `?model_name.equals=${this.props.modelDetails.name}&project_name.equals=${this.props.modelDetails.project_name}`)
        .then(res => {
            let data = res.data;
            let arr = []
            data.map((e,i) => {
                arr.push(e.name);
            });
            if(this._isMounted)
                this.setState({
                    jobs: arr
                })
        })
        .catch(err => {
            console.log('Error fetching Bias Config:', err);
        });
    }

    deleteGrp(num) {
        let groups = [...this.state.groups];
        groups[num] = '';
        let arr = groups.filter((e)=>{return e});
        if(!arr.length) {
            groups = [];
        }
        this.setState({
            groups: groups
        })
    }

    addGrp(name="") {
        const len = this.state.groups.length;
        let groups = [...this.state.groups];
        groups.push(
            <Group key={len} index={len} name={name} deleteMe={this.deleteGrp} />
        )
        this.setState({
            groups: groups
        })
    }
    
    showChild(e) {
        if(!e.target.className === "parent-node")
            return 0
        const target = e.target;
        let nodes = target.querySelectorAll(".child-node");
        nodes.forEach((elem)=>{
            if(elem.style.display === "block") {   
                elem.style.display = "none"
                target.querySelector(".plusIcon").querySelector("i").className = "fa fa-plus-square";
            } else {   
                elem.style.display = "block"
                target.querySelector(".plusIcon").querySelector("i").className = "fa fa-minus-square";
            }
        });
    }

    handleToggle(e) {
        let name = e.target.name;
        let val = this.state[name];
        
        this.setState({
            [name]: !val
        },()=>{
            if(name === "togglerAnd" && this.state[name]) {
                this.setState({
                    togglerOr: false
                })
            } else if (name === "togglerOr" && this.state[name]) {
                this.setState({
                    togglerAnd: false
                })
            }
        })
    }

    handleChange(e) {
        const target = e.target;
        const name = target.name;
        const value = target.value;
        this.setState({
            [name]: value
        },()=>{
            if(name === 'actionType') {
                switch(value) {
                    case '1': 
                        this.setState({
                            inpHolder: "URL",
                            inpType: "Webhook URL",
                        });
                    break;
                    case '2': 
                        this.setState({
                            inpHolder: "Emails seperated by ','",
                            inpType: "Email Address",
                        });
                    break;
                    case '3': 
                        this.setState({
                            inpHolder: "Job ID",
                            inpType: "Enter job ID",
                        });
                    break;
                    case '4': 
                        this.setState({
                            inpHolder: "URL",
                            inpType: "Webhook URL",
                        });
                    break;
                    default:
                    this.setState({
                        inpHolder: "",
                        inpType: "",
                    })
                }
            }
        })
    }

    render() {
        return(
            <div>
                <div className="container">
                    <div className="row">
                        <div className="col-md-3">
                            <h5>Properties</h5>
                            <div className="block h-500 of-auto">
                                <div className="parent-node" onClick={this.showChild}>
                                    <span className="plusIcon">
                                        <i className='fa fa-plus-square'></i>
                                    </span> Model
                                    <div className="child-node" onClick={()=>{this.addGrp("failing")}}>failing</div>
                                    <div className="child-node" onClick={()=>{this.addGrp("health_score")}}>health_score</div>
                                    <div className="child-node" onClick={()=>{this.addGrp("data_drift")}}>data_drift</div>
                                    <div className="child-node" onClick={()=>{this.addGrp("no_of_predictions")}}>no_of_predictions</div>
                                </div>
                                <div className="parent-node" onClick={this.showChild}>
                                    <span className="plusIcon">
                                        <i className='fa fa-plus-square'></i>
                                    </span> Drift
                                    {this.props.modelDetails && this.props.modelDetails.feature_significance.map((e,i)=>{
                                        return (
                                            <div className="child-node" key={e.name+i} onClick={()=>{this.addGrp(`drift.${e.name}`)}}>{e.name}</div>
                                        );
                                    })}
                                </div>
                                <div className="parent-node" onClick={this.showChild}>
                                    <span className="plusIcon">
                                        <i className='fa fa-plus-square'></i>
                                    </span> Bias
                                    {this.state.bias && this.state.bias.features.map((e,i)=>{
                                        return(
                                            <div className="child-node" key={e.name+i} onClick={()=>{this.addGrp(`bias.${e.name}`)}}>{e.name}</div>
                                        )
                                    })}
                                </div>
                                <div className="parent-node" onClick={this.showChild}>
                                    <span className="plusIcon">
                                        <i className='fa fa-plus-square'></i>
                                    </span> Predictions
                                    <div className="child-node" onClick={()=>{this.addGrp("predictions.incorrect_%")}}>incorrect_%</div>
                                    <div className="child-node" onClick={()=>{this.addGrp("predictions.total_count")}}>total_count</div>
                                    <div className="child-node" onClick={()=>{this.addGrp("predictions.output")}}>output</div>
                                    <div className="child-node" onClick={()=>{this.addGrp("predictions.probability")}}>probability</div>
                                    <div className="child-node" onClick={()=>{this.addGrp("predictions.inputs")}}>inputs</div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-9">
                            <h5>Conditions</h5>
                            <div className="block h-500 of-auto">
                                <div className="action-bar of-auto pt-xs">
                                    <div className="float-left">
                                        <button name="togglerAnd" className={"btn btn-default btn-sm mr-sm "+(this.state.togglerAnd ? "activeBtn" : "")} onClick={this.handleToggle}>AND</button>
                                        <button name="togglerOr" className={"btn btn-default btn-sm "+(this.state.togglerOr ? "activeBtn" : "")} onClick={this.handleToggle}>OR</button>
                                    </div>
                                    <div className="float-right">
                                        <button className="btn btn-primary btn-sm mr-sm" onClick={()=>{this.addGrp()}}>Add Rule</button>
                                    </div>
                                </div>
                                <div className="group-section mt">
                                    {this.state.groups}
                                </div>
                            </div>
                        </div>
                        <div className="of-auto mt col-md-12">
                            <h5>Actions</h5>
                            <div className="block of-auto">
                                <div className="mlr">
                                <label htmlFor="#actionType" className="f13">Type :</label>
                                <select className="form-control wd250 h-auto f13 mt-sm" style={{padding: 7}} value={this.state.actionType} onChange={this.handleChange} id="actionType" name="actionType" >
                                    <option value="1">Slack Notification</option>
                                    <option value="2">Email Notification</option>
                                    <option value="3">Run Job</option>
                                    <option value="4">Webhook</option>
                                </select>
                                {this.state.actionType === '3' ?
                                    this.state.jobs.length ?
                                    <div>
                                        <label htmlFor="#actionInp" className="f13">{this.state.inpType}</label>
                                        <select className="form-control wd250 h-auto f13 mt-sm" style={{padding: 7}} value={this.state.actionInp} onChange={this.handleChange} id="actionInp" name="actionInp">
                                            {this.state.jobs.map((e,i)=>{
                                                return(<option value={e} key={e+i}>{e}</option>)
                                            })}
                                        </select>
                                    </div>
                                    : <div className="f13 mt-sm">No workflow found. Add Workflow <Link to="/workflows/new">here</Link></div>
                                : <label htmlFor="#actionInp" className="mt f13">
                                    {this.state.inpType}
                                    <input name="actionInp" className="form-control wd250 pd-sm h-auto f13 mt-sm" value={this.state.actionInp} placeholder={this.state.inpHolder} onChange={this.handleChange} />
                                </label>
                                }
                                </div>
                            </div>
                        </div>
                        <div className="of-auto mt col-md-12">
                            <button className="btn btn-primary btn-sm float-right">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token
    };
};

export default connect(mapStateToProps)(Rules);