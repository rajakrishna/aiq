import React, { Component } from "react";
import { connect } from "react-redux";
import { TextField } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import blue from "@material-ui/core/colors/blue";
import {
  withStyles,
  MuiThemeProvider,
  createMuiTheme,
} from "@material-ui/core/styles";

const theme = createMuiTheme({
  palette: {
    primary: blue,
  },
  typography: {
    useNextVariants: true,
  },
});

class ChooseSizeForm extends Component {
  state = {
    extraResources: "{}",
    memory: process.env.REACT_APP_NOTEBOOK_SIZE_NANO_MEMORY,
    cpu: process.env.REACT_APP_NOTEBOOK_SIZE_NANO_CPU,
    nanobuttoncolor: null,
    microbuttoncolor: null,
    smallbuttoncolor: null,
    mediumbuttoncolor: null,
    largebuttoncolor: null,
    xlargebuttoncolor: null,
    xxlargebuttoncolor: null,
    costumbuttoncolor: null,
    costumSize: false,
    gpu: 0,
  };
  constructor(props) {
    super();

    this.state.gpu = props.size.gpu;

    this.state.cpu = props.size.cpu;
    this.state.memory = props.size.memory;
  }
  handleChange = (e) => {
    const target = e.target;
    this.setState({
      [target.name]: target.value,
    });
    this.props.handleChange(e);
  };
  componentDidMount() {
    this.setButtonColor(this.props.size.size);
  }
  setButtonColor(size) {
    console.log("color == " + size);
    switch (size) {
      case "Nano":
        return this.setState({
          nanobuttoncolor: "primary",
          microbuttoncolor: null,
          smallbuttoncolor: null,
          mediumbuttoncolor: null,
          largebuttoncolor: null,
          xlargebuttoncolor: null,
          xxlargebuttoncolor: null,
          costumbuttoncolor: null,
        });
      case "Micro":
        return this.setState({
          nanobuttoncolor: null,
          microbuttoncolor: "primary",
          smallbuttoncolor: null,
          mediumbuttoncolor: null,
          largebuttoncolor: null,
          xlargebuttoncolor: null,
          xxlargebuttoncolor: null,
          costumbuttoncolor: null,
        });
      case "Small":
        return this.setState({
          nanobuttoncolor: null,
          microbuttoncolor: null,
          smallbuttoncolor: "primary",
          mediumbuttoncolor: null,
          largebuttoncolor: null,
          xlargebuttoncolor: null,
          xxlargebuttoncolor: null,
          costumbuttoncolor: null,
        });
      case "Medium":
        return this.setState({
          nanobuttoncolor: null,
          microbuttoncolor: null,
          smallbuttoncolor: null,
          mediumbuttoncolor: "primary",
          largebuttoncolor: null,
          xlargebuttoncolor: null,
          xxlargebuttoncolor: null,
          costumbuttoncolor: null,
        });
      case "Large":
        return this.setState({
          nanobuttoncolor: null,
          microbuttoncolor: null,
          smallbuttoncolor: null,
          mediumbuttoncolor: null,
          largebuttoncolor: "primary",
          xlargebuttoncolor: null,
          xxlargebuttoncolor: null,
          costumbuttoncolor: null,
        });
      case "Large":
        return this.setState({
          nanobuttoncolor: null,
          microbuttoncolor: null,
          smallbuttoncolor: null,
          mediumbuttoncolor: null,
          largebuttoncolor: "primary",
          xlargebuttoncolor: null,
          xxlargebuttoncolor: null,
          costumbuttoncolor: null,
        });
      case "XLarge":
        return this.setState({
          nanobuttoncolor: null,
          microbuttoncolor: null,
          smallbuttoncolor: null,
          mediumbuttoncolor: null,
          largebuttoncolor: null,
          xlargebuttoncolor: "primary",
          xxlargebuttoncolor: null,
          costumbuttoncolor: null,
        });
      case "2XLarge":
        return this.setState({
          nanobuttoncolor: null,
          microbuttoncolor: null,
          smallbuttoncolor: null,
          mediumbuttoncolor: null,
          largebuttoncolor: null,
          xlargebuttoncolor: null,
          xxlargebuttoncolor: "primary",
          costumbuttoncolor: null,
        });
      case "Costum":
        return this.setState({
          nanobuttoncolor: null,
          microbuttoncolor: null,
          smallbuttoncolor: null,
          mediumbuttoncolor: null,
          largebuttoncolor: null,
          xlargebuttoncolor: null,
          xxlargebuttoncolor: null,
          costumbuttoncolor: "primary",
        });
      default:
        return this.setState({
          nanobuttoncolor: "primary",
          microbuttoncolor: null,
          smallbuttoncolor: null,
          mediumbuttoncolor: null,
          largebuttoncolor: null,
          xlargebuttoncolor: null,
          xxlargebuttoncolor: null,
          costumbuttoncolor: null,
        });
    }
  }
  nanoSize = () => {
    this.setButtonColor("Nano");
    this.props.size.size = "Nano";
    this.props.size.extraResources = "{}";
    this.props.size.memory = process.env.REACT_APP_NOTEBOOK_SIZE_NANO_MEMORY;
    this.props.size.cpu =process.env.REACT_APP_NOTEBOOK_SIZE_NANO_CPU;
    this.setState({
      extraResources: "{}",
      memory: process.env.REACT_APP_NOTEBOOK_SIZE_NANO_MEMORY,
      cpu: "0.5",
      costumSize: process.env.REACT_APP_NOTEBOOK_SIZE_NANO_CPU,
    });
  };
  microSize = () => {
    this.setButtonColor("Micro");
    this.props.size.size = "Micro";
    this.props.size.extraResources = "{}";
    this.props.size.memory = process.env.REACT_APP_NOTEBOOK_SIZE_MICRO_MEMORY;
    this.props.size.cpu = process.env.REACT_APP_NOTEBOOK_SIZE_MICRO_CPU;

    this.setState({
      extraResources: "{}",
      memory: process.env.REACT_APP_NOTEBOOK_SIZE_MICRO_MEMORY,
      cpu: process.env.REACT_APP_NOTEBOOK_SIZE_MICRO_CPU,
      costumSize: "",
    });
  };
  smallSize = () => {
    this.setButtonColor("Small");
    this.props.size.size = "Small";
    this.props.size.extraResources = "{}";
    this.props.size.memory = process.env.REACT_APP_NOTEBOOK_SIZE_SMALL_MEMORY;
    this.props.size.cpu = process.env.REACT_APP_NOTEBOOK_SIZE_SMALL_CPU;
    this.setState({
      extraResources: "{}",
      memory: process.env.REACT_APP_NOTEBOOK_SIZE_SMALL_MEMORY,
      cpu: process.env.REACT_APP_NOTEBOOK_SIZE_SMALL_CPU,
      costumSize: "",
    });
  };
  mediumSize = () => {
    this.setButtonColor("Medium");
    this.props.size.size = "Medium";
    this.props.size.extraResources = "{}";
    this.props.size.memory = process.env.REACT_APP_NOTEBOOK_SIZE_MEDIUM_MEMORY;
    this.props.size.cpu =process.env.REACT_APP_NOTEBOOK_SIZE_MEDIUM_CPU;
    this.setState({
      extraResources: "{}",
      memory: process.env.REACT_APP_NOTEBOOK_SIZE_MEDIUM_MEMORY,
      cpu: process.env.REACT_APP_NOTEBOOK_SIZE_MEDIUM_CPU,
      costumSize: "",
    });
  };
  largeSize = () => {
    this.setButtonColor("Large");
    this.props.size.size = "Large";
    this.props.size.extraResources = "{}";
    this.props.size.memory = process.env.REACT_APP_NOTEBOOK_SIZE_LARGE_MEMORY;
    this.props.size.cpu = process.env.REACT_APP_NOTEBOOK_SIZE_LARGE_CPU;
    this.setState({
      extraResources: "{}",
      memory: process.env.REACT_APP_NOTEBOOK_SIZE_LARGE_MEMORY,
      cpu: process.env.REACT_APP_NOTEBOOK_SIZE_LARGE_CPU,
      costumSize: "",
    });
  };
  xlargeSize = () => {
    this.setButtonColor("XLarge");
    this.props.size.size = "XLarge";
    this.props.size.extraResources = "{}";
    this.props.size.memory = process.env.REACT_APP_NOTEBOOK_SIZE_XLARGE_MEMORY;
    this.props.size.cpu = process.env.REACT_APP_NOTEBOOK_SIZE_XLARGE_CPU;
    this.setState({
      extraResources: "{}",
      memory: process.env.REACT_APP_NOTEBOOK_SIZE_XLARGE_MEMORY,
      cpu: process.env.REACT_APP_NOTEBOOK_SIZE_XLARGE_CPU,
      costumSize: "",
    });
  };
  xxlargeSize = () => {
    this.setButtonColor("2XLarge");
    this.props.size.size = "2XLarge";
    this.props.size.extraResources = "{}";
    this.props.size.memory = process.env.REACT_APP_NOTEBOOK_SIZE_2XLARGE_MEMORY;
    this.props.size.cpu = process.env.REACT_APP_NOTEBOOK_SIZE_2XLARGE_CPU;
    this.setState({
      extraResources: "{}",
      memory: process.env.REACT_APP_NOTEBOOK_SIZE_2XLARGE_MEMORY,
      cpu: process.env.REACT_APP_NOTEBOOK_SIZE_2XLARGE_CPU,
      costumSize: "",
    });
  };
  costum = () => {
    this.setButtonColor("Costum");
    this.props.size.size = "Costum";
    this.setState({
      costumSize: "X",
    });
  };
  render() {
    return (
      <div className="package-content">
        <form>
          <div className="pd-lg pl-0">
            <div className="step-title">Step 2: Choose a Notebook Size</div>
            <div className="mt-lg step-description">
              A Notebook can have verying combination of CPU, memory and storage
              and give you the flexibility to choose the appropriate mix of
              resources for your application.
            </div>
            {this.props.userTier=='Standard Tier' && (
              <div className="row mt-24">
                  <div className="col-sm-12">
                      <div className="alert alert-info f-13">
                                  
                        <strong>   <i class="fa fa-info-circle"></i> </strong>
                                  Your current subscription supports Notebooks sizes upto Medium. Please contact <a href={`mailto:support@predera.com?`}>Support </a>
                                  if you need larger Notebooks
                      </div>
                            
                  </div>
               </div>
              )} 
            <div className="mt-24">
           
              <MuiThemeProvider theme={theme}>
                <Button
                 style={{ textTransform:'none'}}
                  variant="outlined"
                  color={this.state.nanobuttoncolor}
                  onClick={this.nanoSize}
                >
                  Nano
                </Button>
              </MuiThemeProvider>
              <MuiThemeProvider theme={theme}>
                <Button
                 style={{ textTransform:'none'}}
                  variant="outlined"
                  color={this.state.microbuttoncolor}
                  onClick={this.microSize}
                >
                  Micro
                </Button>
              </MuiThemeProvider>
              <MuiThemeProvider theme={theme}>
                <Button
                 style={{ textTransform:'none'}}
                  variant="outlined"
                  color={this.state.smallbuttoncolor}
                  onClick={this.smallSize}
                >
                  Small
                </Button>
              </MuiThemeProvider>
              <Button
               style={{ textTransform:'none'}}
                variant="outlined"
                color={this.state.mediumbuttoncolor}
                onClick={this.mediumSize}
              >
                Medium
              </Button>
              <MuiThemeProvider theme={theme}>
                <Button
                 style={{ textTransform:'none'}}
                  variant="outlined"
                  disabled={this.props.userTier=='Standard Tier'}
                  color={this.state.largebuttoncolor}
                  onClick={this.largeSize}
                >
                  Large
                </Button>
              </MuiThemeProvider>
              <MuiThemeProvider theme={theme}>
                <Button
                 style={{ textTransform:'none'}}
                  variant="outlined"
                  disabled={this.props.userTier=='Standard Tier'}
                  color={this.state.xlargebuttoncolor}
                  onClick={this.xlargeSize}
                >
                  X Large
                </Button>
              </MuiThemeProvider>
              <MuiThemeProvider theme={theme}>
                <Button
                 style={{ textTransform:'none'}}
                  variant="outlined"
                  disabled={this.props.userTier=='Standard Tier'}
                  color={this.state.xxlargebuttoncolor}
                  onClick={this.xxlargeSize}
                >
                  2X Large
                </Button>
              </MuiThemeProvider>
              <MuiThemeProvider theme={theme}>
                <Button
                 style={{ textTransform:'none'}}
                  variant="outlined"
                  disabled={this.props.userTier=='Standard Tier'}
                  color={this.state.costumbuttoncolor}
                  onClick={this.costum}
                >
                  Custom
                </Button>
              </MuiThemeProvider>
            </div>

            <div className="col-sm-6 pl-0  mt-30">
              <TextField
                name="cpu"
                id="cpu"
                label={`CPU`}
                value={this.state.cpu}
                onChange={this.handleChange}
                margin="normal"
                fullWidth
                InputProps={{
                  readOnly: this.state.costumSize !== "X",
                  disabled: this.state.costumSize !== "X",
                }}
                required
              />{" "}
              <div className="size-linput-label">
                Total amount of CPU reserved by your Notebook
              </div>
            </div>
            <div className="col-sm-6 pl-0  mt-30">
              <TextField
                className="mt-30"
                name="memory"
                id="memory"
                label={`Memory`}
                value={this.state.memory}
                onChange={this.handleChange}
                margin="normal"
                fullWidth
                InputProps={{
                  readOnly: this.state.costumSize !== "X",
                  disabled: this.state.costumSize !== "X",
                }}
                required
              />
              <div className="size-linput-label">
                Total amount of RAM reserved by your Notebook
              </div>
            </div>
            <div className="col-sm-6 pl-0  mt-30">
              <TextField
                type="number"
                name="gpu"
                id="gpu"
                label={`GPU`}
                value={this.props.size.gpu}
                onChange={this.handleChange}
                margin="normal"
                fullWidth
                required
                InputProps={{
                  readOnly: this.state.costumSize !== "X",
                  disabled: this.state.costumSize !== "X",
                }}
              />
              <div className="size-linput-label">
                Total number of GPUs reserved by your Notebook
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
  };
};

export default connect(mapStateToProps)(ChooseSizeForm);
