import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getAllModels, getDeploymentList } from "../api";
import ModelListItem from "./models/modelCard";
import Pagination from "./common/pagination";
import MicroModal from "micromodal";
import CardLoader from "./models/cardLoader";
import FilterModal from "./models/filterModal";
import { startCase } from "lodash";
import _ from "lodash";
import Mixpanel from "mixpanel-browser";
import { getErrorMessage } from "../utils/helper";
import { globalProjectSelection } from "../ducks/projects";

class Monitoring extends Component {
    _isMounted = false;
    state = {
        listType: "DEPLOYED",
        projectID: this.props.selectedProject && this.props.selectedProject.id,
        modelsList: [],
        card_loader: true,
        appliedFilters: {},
        deploymentsList: null,
        depFilters: {
            nameContains: ""
        },
        filters: {
            projectIDEquals: "",
            nameContains: "",
            healthScore: 0,
            dataDrift: 0,
            mlLibraryContains: "",
            ml_algorithm: null,
            type: null,
            failing: ""
        },
        currentPage: 0,
        currentDepPage: 0,
        totalModels: 0,
        emptyModel: false,
    };

    componentDidMount() {
        this._isMounted = true;
        Mixpanel.track("Model Monitoring Page");
        this.getModels({projectIDEquals:this.props.selectedProject && this.props.selectedProject.id});
        MicroModal.init({
            disableScroll: true,
            disableFocus: false,
            awaitCloseAnimation: false,
            debugMode: true
        });
    }

    componentDidUpdate(prevProps){
        if(this.props.selectedProject && prevProps.selectedProject.id !== this.props.selectedProject.id)
        {
            this.getModels(this.props.selectedProject.id);
            this.setState({
                projectID: this.props.selectedProject.id               
            });
        }
         
    }

    getColor(type) {
        const status = {
            'Creating': "info",
            'Failed': "failed",
            'Available': "success"
        }
        const color = status[type] ? status[type] : 'primary';
        return color;
    }

    componentWillUnmount() {
        this._isMounted = false
    }

    getDeployments = (params = {}) => {
        this.setState({ deploymentsList: [], card_loader: true });
        params = { ...params, projectID: this.props.projectID || params.projectID };
        getDeploymentList(this.props.token, params)
            .then(response => {
                if (response.status && this._isMounted) {
                    this.setState({
                        allDeployments: response.data,
                        deploymentsList: this.parseDepList(response.data, response.data.length),
                        card_loader: false,
                        totalDeployments: response.data.length
                    });
                }
            })
            .catch(error => {
                const errorMessage = getErrorMessage(error) || "An error occurred while fetching Deployments, please try again.";
                Mixpanel.track("Get Deployments Failed", { error: errorMessage });
            });
    };

    parseDepList = (data, size) => {
        const { currentDepPage } = this.state;
        const range = [20 * currentDepPage, (20 * (currentDepPage + 1)) - 1]
        range[1] = range[1] <= size ? range[1] : size;
        const parsedData = [];
        for (let i = range[0]; i <= range[1]; i++) {
            if (data[i]) {
                parsedData.push(data[i]);
            }
        }
        return parsedData;
    }

    // getModels fetches models based on different search criteria
    getModels = params => {
        if (!this._isMounted) {
            return 0;
        }
        this.setState({ modelsList: [], card_loader: true });
        const defaultParams = {
            statusEquals: this.state.listType,
            projectIDEquals: this.props.selectedProject && this.props.selectedProject.id 
        };
        let allParams = { ...defaultParams, ...params };

        let ml_algorithm = params && params.ml_algorithm
        if (ml_algorithm && ml_algorithm.value && ml_algorithm.value != "") {
            allParams[`mlAlgorithm${ml_algorithm.key}`] = ml_algorithm.value
        }
        let type = params && params.type
        if (type && type.value && type.value != "") {
            allParams[`type${type.key}`] = type.value
        }
        if (!allParams.sort) {
            allParams = { ...allParams, sort: "created_date,desc" };
        }
        let health_score = params && params.health_score
        if (health_score) {
            if (health_score.key === "isBetween") {
                allParams[`healthScoregreaterOrEqualThan`] = health_score.value1
                allParams[`healthScorelessOrEqualThan`] = health_score.value2
            } else {
                allParams[`healthScore${health_score.key}`] = health_score.value1
            }
        }
        let data_drift = params && params.data_drift
        if (data_drift) {
            if (data_drift.key === "isBetween") {
                allParams[`dataDriftgreaterOrEqualThan`] = data_drift.value1
                allParams[`dataDriftlessOrEqualThan`] = data_drift.value2
            } else {
                allParams[`dataDrift${data_drift.key}`] = data_drift.value1
            }
        }

        let currentPage = allParams.page ? allParams.page : 0;
        getAllModels(this.props.token, allParams)
            .then(response => {
                if (response.status && this._isMounted) {
                    this.setState({
                        modelsList: response.data,
                        emptyModel: response.data.length > 0 ? false : true,
                        card_loader: false,
                        totalModels: response.headers["x-total-count"],
                        currentPageSize: response.data.length,
                        currentPage: currentPage
                    });
                }
            })
            .catch(error => {
                const errorMessage = getErrorMessage(error) || "An error occurred while fetching Models, please try again.";
                Mixpanel.track("Get Models Failed", { error: errorMessage });
            });
    };

    handleInputChange = e => {
        let filters = { ...this.state.filters };
        filters[e.target.name] = e.target.value;
        this.applyFilters(filters);
    };

    handleInputSearchChange = e => {
        let filters = { ...this.state.depFilters };
        filters[e.target.name] = e.target.value;
        this.applyDepFilters(filters);
    };

    applyFilters = (filters) => {
        this.setState({ filters, currentPage: 1 });
        this.getModels(_.pickBy(filters, _.identity));
    };

    applyDepFilters = (depFilters) => {
        this.setState({ depFilters, currentPage: 1 });
        this.getDeployments(_.pickBy(depFilters, _.identity));
    };

    handleRemoveFilter = filter => {
        let updatedFilters = { ...this.state.filters };
        updatedFilters[filter] = null;
        this.setState({ filters: updatedFilters });
        this.applyFilters(updatedFilters);
    };

    handleDepRemoveFilter = filter => {
        let updatedFilters = { ...this.state.depFilters };
        updatedFilters[filter] = null;
        this.setState({ depFilters: updatedFilters });
        this.applyDepFilters(updatedFilters);
    };

    handlePageChange = pageAction => {
        let pageNumber = this.state.currentPage;
        pageAction === "nextPage" ? (pageNumber += 1) : (pageNumber -= 1);
        let params = { page: pageNumber, ...this.state.filters };
        this.getModels(params);
    };

    handleDepPageChange = pageAction => {
        let pageNumber = this.state.currentDepPage;
        const allDeployments = this.state.allDeployments;
        pageAction === "nextPage" ? (pageNumber += 1) : (pageNumber -= 1);
        this.setState({
            currentDepPage: pageNumber,
            deploymentsList: this.parseDepList(allDeployments, allDeployments.length)
        })
    };

    deleteDeployments = (id) => {
        this.props.deleteDeployment(id);
    }

    getFilterTab = (data) => {
        switch (typeof (data)) {
            case "object":
                if (data.value) {
                    return `${data.key}-${data.value}`
                }
                if (data.key == "isBetween") {
                    return `${data.key}-${data.value1}&${data.value2}`
                }
                if (data.value1) {
                    return `${data.key}-${data.value1}`
                }
                break;
            default:
                return data
        }
    }

    render() {
        const {
            modelsList,
            filters,
            totalModels,
            currentPage,
            currentPageSize,
        } = this.state;
        let appliedFilters = _.pickBy(filters, _.identity);
        return (
            <div className="main-container">
                <div className="main-body">
                    <div className="content-wraper-sm">
                        <div>
                            <div className="card">
                                <div>
                                    <div className="card-header header-with-search pl-4 mr-5 pt-3">
                                        <h4 className="bold-heading">Monitoring</h4>
                                        <div className="card-search">
                                            <input
                                                type="text"
                                                className="form-control"
                                                name="nameContains"
                                                value={filters.nameContains || ""}
                                                placeholder="SEARCH"
                                                onChange={(e) => {
                                                    filters.nameContains = e.target.value
                                                    this.setState({ filters })
                                                    //this.applyFilters(filters)
                                                }}
                                                onKeyPress={event => {
                                                    if (event.key === "Enter") {
                                                        this.handleInputChange(event)
                                                    }
                                                }}
                                            />
                                        </div>
                                        <div className="card-tools">
                                            <div className="model-pagination">
                                                {totalModels > 20 && (
                                                    <Pagination
                                                        itemsCount={totalModels}
                                                        currentPage={currentPage}
                                                        currentPageSize={currentPageSize}
                                                        pageSize={20}
                                                        onPageChange={this.handlePageChange}
                                                    />
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="model-filter-header pl-4 mr-5">
                                        <div className="filter-btn">
                                            <button
                                                className="btn btn-primary btn-sm btn-rounded"
                                                onClick={() => MicroModal.show('modal-1')}
                                            >
                                                Add Filter +
                                            </button>
                                        </div>
                                        <div className="applied-filters">
                                            <ul className="filter-items-list">
                                                {!_.isEmpty(appliedFilters) &&
                                                    Object.keys(appliedFilters).map(key => (
                                                        <li key={key} className="filter-tag-item">
                                                            {startCase(key)}-{this.getFilterTab(appliedFilters[key])}
                                                            <i className="fa fa-times" style={{ marginTop: 3 }} onClick={() => this.handleRemoveFilter(key)}>
                                                            </i>
                                                        </li>
                                                    ))}
                                            </ul>
                                        </div>
                                        <div className="filter-right-actions mr-3">
                                            <div className="row align-items-center justify-content-between">
                                                <div className="col">
                                                    <label><small>Sort By</small></label>
                                                    <select
                                                        name="sort"
                                                        onChange={this.handleInputChange}
                                                        id=""
                                                        className="form-control select-field"
                                                        value={appliedFilters.sort || ''}
                                                    >
                                                        <option value=''>Select</option>
                                                        <option value="type,asc">Type</option>
                                                        <option value="ml_library,asc">Library</option>
                                                        <option value="ml_algorithm,asc">Algorithm</option>
                                                        <option value="created_date,desc">Created</option>
                                                        <option value="health_score,asc">Health</option>
                                                        <option value="failing,desc">Failing</option>
                                                        <option value="data_drift,desc">Data Drift</option>
                                                    </select>
                                                </div>
                                                <div className="col">
                                                    <label><small>Created Date</small></label>
                                                    <select
                                                        name="created_date"
                                                        id=""
                                                        onChange={this.handleInputChange}
                                                        className="form-control select-field"
                                                        value={appliedFilters.created_date || ''}
                                                    >
                                                        <option value=''>Select</option>
                                                        <option value="now-1d">Last 1 day</option>
                                                        <option value="now-7d">Last 7 days</option>
                                                        <option value="now-30d">Last Month</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-body mr-5">
                                        {this.state.card_loader ? <CardLoader /> : ""}
                                        {modelsList.length !== 0 && (
                                            <ModelListItem
                                                history={this.props.history}
                                                modelsList={modelsList}
                                                scores={true} />
                                        )}
                                        {modelsList.length === 0 && !this.state.card_loader &&
                                            (<div className="empty-items-box">
                                                <h4>You don't have any Monitored Models!</h4>
                                            </div>)
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <FilterModal
                    filterType={"models"}
                    applyFilters={this.applyFilters}
                    filters={this.state.filters}
                    project={(/project/).test(this.props.match.path)}
                />
            </div>
        );
    }
}
const mapStateToProps = state => {
    return {
        token: state.auth.token,
        selectedProject: state.projects.globalProject
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            globalProjectSelection
        },
        dispatch
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(Monitoring);
