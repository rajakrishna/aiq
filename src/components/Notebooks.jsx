import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getNotebookList, deleteNotebook, getToken,updateNotebook, getCurrentUser } from "../api";
import NotebookTokenModal from "./modals/NotebookTokenModal";
import EditNotebookModal from "./modals/EditNotebookModal";
import CardLoader from "./models/cardLoader";
import NotebookForm from "./NotebookForm";
import Tooltip from "@material-ui/core/Tooltip";
import CircularProgress from "@material-ui/core/CircularProgress";
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MicroModal from "micromodal";
import { Copy } from "./Copy";
import { confirm } from "./Confirm";
import { getUserDetail } from "../ducks/users";
import { Link } from "react-router-dom";

const notebook = '';

class Notebooks extends Component {
  
  state = {
    notebooks: [],
    loading: false,
    showForm: false,
    tokenId:'',
    notebook:{},
    tokenError:false,
    loader:false,
    currentUser:[],
    cpuWarning:"",
    memWarning:""
  };

  componentDidMount() {
    this.props.getUserDetail();
    getCurrentUser(this.props.token)
    .then(response => {
      if (response.status) {
        
        this.setState({ currentUser: response.data });
      }
    })
    .catch(error => {
     
    });
    this.fetchNotebooks();

  }
  
  componentDidUpdate(prevProps) {
    if (this.props.selectedProject && prevProps.selectedProject.id !==  this.props.selectedProject.id) {
      this.fetchNotebooks();
    }
  }

  showTable = () => {
    this.setState(
      {
        showForm: false,
      },
      () => this.fetchNotebooks()
    );
  };

  fetchNotebooks = () => {
    this.setState({
      loading: true,
    });
    getNotebookList(this.props.token,this.props.selectedProject && this.props.selectedProject.id)
      .then((res) => {
        this.setState({
          notebooks: res.data.notebooks,
          loading: false,
        });
      })
      .catch((err) => {
        this.setState({
          loading: false,
        });
      });
  };

  getToken = (notebook) => {
    this.setState({ loader: true });
    MicroModal.show("notebook-token-modal");
    getToken(this.props.token, notebook.name)
      .then((res) => {
        this.setState({ tokenId: res.data.token });
        this.setState({ loader: false });
      })
      .catch((error) => {
        this.setState({ tokenError: true, loader: false });
      });
  };
 

  handleDelete = (name) => {
    deleteNotebook(this.props.token, name).then((res) => {
      this.fetchNotebooks();
    });
  };

  getStatus = (name, obj) => {
    if (name && name.length) {
      switch (name) {
        case "running":
          return (
            <Tooltip title="Running">
              <i
                className="fas fa-check-circle"
                style={{ fontSize: 24, color: "green" }}
              ></i>
            </Tooltip>
          );
        case "terminated":
          return (
            <Tooltip title={obj.status[name].reason}>
              <i
                className="fas fa-times"
                style={{ fontSize: 24, color: "red" }}
              ></i>
            </Tooltip>
          );
        default:
          return (
            <Tooltip title={obj.status[name].reason}>
              <i
                className="fas fa-exclamation-triangle"
                style={{ fontSize: 24, color: "orange" }}
              ></i>
            </Tooltip>
          );
      }
    } else {
      return (
        <Tooltip title={"Scheduling Pod"}>
          <CircularProgress size={30} />
        </Tooltip>
      );
    }
  };
  getNotebookStatus = (name, obj) => {
   // this.setState({notebook:obj})
    this.notebook = obj
    if (name && name.length) {
      switch (name) {
        case "running":
          return (
            <div className="d-inline">
              <NotebookTokenModal notebookObj = {obj}/>
            </div>
          );
      
        default:
          return (
            <Tooltip title="token">
            <i
              className="fa fa-key ml clickable disabled"
              ></i>
             </Tooltip> 
          );
      }
    }
  };

  handleEdit =(name) =>{
    const editNotebook = this.state.notebooks.find((item) => item.name === name );
    this.setState({notebook:editNotebook});
    this.setState({cpuWarning:""});
    this.setState({memWarning:""});
  }

  notebookTable = () => {
    return this.state.notebooks && this.state.notebooks.length > 0 ? (
      <table className="table notebook-table">
        <thead>
          <tr>
            <th>Status</th>
            <th>Name</th>
            <th>Created</th>
            <th>Package</th>
            <th>GPU</th>
            <th>CPU</th>
            <th>Memory</th>
            <th>Volumes</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {this.state.notebooks.map((e, i) => {
            return (
              <tr key={i}>
                <td className="text-center">
                  {this.getStatus(Object.keys(e.status)[0], e)}
                </td>
                <td>
                  <Link to={`/notebooks/${e.name}`}>{e.name}</Link>
                </td>
                <td>{e.uptime}</td>
                <td>{e.srt_image}</td>
                <td>{e.gpu}</td>
                <td>{e.cpu}</td>
                <td>{e.mem}</td>
                <td className="text-center">
                  <span
                    className="dropdown-toggle pd-xs content-none clickable"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <i className="fa fa-database"></i>
                  </span>
                  <div className="dropdown-menu">
                    {e.volumes.map((elem, i) => {
                      return (
                        <span key={elem.name} className="dropdown-item">
                          {elem.name}
                        </span>
                      );
                    })}
                  </div>
                </td>
                <td>
                  {e.status.running ? (
                    <a
                      className="mr"
                      href={`${process.env.REACT_APP_API_URL}/../notebook/kai/${e.name}`}
                      target="_blank"
                    >
                      CONNECT
                    </a>
                  ) : (
                    <span className="text-gray mr clickable disabled">
                      CONNECT
                    </span>
                  )}
                  {this.getNotebookStatus(Object.keys(e.status)[0], e)}
                  <EditNotebookModal notebookObj={this.notebook} />
                  <Tooltip title="Delete">
                  <i
                    className="fa fa-trash ml clickable"
                    onClick={() => {
                      confirm(
                        "This will delete the Notebook but not the Volumes. A new Notebook can be created later using the same Volumes to restore data.",
                        "Delete Notebook?",
                        "delete"
                      ).then(() => {
                        this.handleDelete(e.name);
                      });
                    }}
                   
                  ></i>
                  </Tooltip>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    ) : (
      <div className="empty-items-box">
          {this.state.currentUser.tier ==="Free Tier" ? (
            <div>
             <h4>Your current subscription does not include Notebooks.</h4>
             <h4> Please contact <a href={`mailto:support@predera.com?`}>Support </a> to upgrade your subscription</h4>
             </div>
           ):(
            <h4>You don't have any Notebooks!</h4>
           )}
        
       
      </div>
      
    );
  };

  handleChange =(e)=>{
    const target = e.target;
    let name = target.name;
    let value = (name === "mem") ? target.value+"Gi" : target.value;
    this.setState({
      ...this.state,notebook: {...this.state.notebook, [name]: value}
    })
  }

  handleBlurCpu =()=>{
    if((this.props.user.tier === 'Standard Tier') && this.state.notebook.cpu > '2'){
      this.setState({cpuWarning:"Max CPU size allowed for this subscription is 2."})
    }
  }

  handleBlurMem =()=>{
    if((this.props.user.tier === 'Standard Tier') && this.state.notebook.mem > '4Gi'){
      this.setState({memWarning:"Max Memory size allowed for this subscription is 4Gi."})
    }
  }

  updateNotebook =()=>{
    const gpuValue = (this.state.notebook.gpu == 0) ? {} : {"nvidia.com/gpu":this.state.notebook.gpu}
    const updateObj = {
      "cpu":this.state.notebook.cpu,
      "memory":this.state.notebook.mem,
      "extraResources": gpuValue
    }
    updateNotebook(this.props.token,this.props.selectedProject && this.props.selectedProject.id,
      this.state.notebook.name,updateObj).then((res)=>{
          console.log("res.status::",res.status,"res.data::",res.data)
          this.handleClose();
      })
      .catch((err)=>{
        console.log("error while notebook Update:",err.message)
      })

  }

  render() {
    const { showForm, loading,loader } = this.state;
    return (
      <div className="main-container">
        <div className="main-body">
          <div className="content-wraper-sm">
            <div className="card">
              {showForm ? (
                <div className="card-header">
                  <span
                    className="clickable"
                    onClick={() => {
                      this.setState({ showForm: false });
                    }}
                  >
                    <i className="fa fa-angle-left" aria-hidden="true"></i>{" "}
                    <span className="notebook-form-title">
                      {" "}
                      CREATE NOTEBOOK
                    </span>
                  </span>
                </div>
              ) : (
                <div className="card-header mt pl-4 mr-5 mt-2">
                  <h4>NOTEBOOKS</h4>
                  <div className="card-tools">
                    <div className="inline-flex">
                      {this.state.currentUser.tier && 
                      this.state.currentUser.tier !=='Free Tier' &&
                      <button
                        className="btn btn-sm btn-primary ml"
                        type="button"
                        onClick={() => {
                          this.props.history.push("/notebooks/new")
                        }}
                      >
                        Create Notebook
                      </button>}
                      {this.state.currentUser.tier &&
                        this.state.currentUser.tier !== 'Free Tier' &&
                        <button
                        className="btn btn-sm btn-default ml"
                        type="button"
                        onClick={() => this.fetchNotebooks()}
                      >
                        <i className="fa fa-sync-alt"></i>
                      </button>}
                    </div>
                  </div>
                </div>
              )}
              {loading ? (
                <CardLoader />
              ) : (
                <React.Fragment>
                  {showForm ? (
                    <div className="card-body">
                      <NotebookForm
                        project={this.props.project}
                        showTable={this.showTable}
                      />
                    </div>
                  ) : (
                    <div className="card-body h-min-370 mr-5">
                      {this.notebookTable()}
                    </div>
                  )}
                </React.Fragment>
              )}
            </div>
          </div>
        </div>
      <div
        className="modal micromodal-slide"
        id="notebook-token-modal"
        aria-hidden="true"
      >
        <div className="modal__overlay" tabIndex="-1" data-micromodal-close>
          <div
            className="modal__container"
            role="dialog"
            aria-modal="true"
            aria-labelledby="secret-modal-title"
          >
            <header className="modal__header">
              <h2 className="modal__title ml-15">Notebook Token</h2>
              <button
                type="button"
                className="modal__close"
                aria-label="Close modal"
                data-micromodal-close
              />
            </header>

            <main className="modal__content mt-0 mb-0 h120">
              {loader ? (
                <div className="modal-filter-form">
                  <div className="modal-body f-13 ml-15 mt-30 mr">
                    <CircularProgress size={20} disableShrink />
                  </div>
                </div>
              ) : (
                <div>
                  {this.state.tokenError ? (
                    <div className="modal-filter-form">
                      <div className="modal-body alert alert-danger f-13 ml-15 mt-30 mr">
                        <strong>Error: </strong>
                        An error occurred while fetching the Token, please try again.
                      </div>
                      <div className="ml-15 mt-30">
                        <button
                          onClick={() => {
                            this.getToken(this.notebook);
                          }}
                          type="button"
                          className="btn btn-primary"
                        >
                          Retry
                        </button>
                        <button
                          onClick={() => MicroModal.close("notebook-token-modal")}
                          type="button"
                          className="btn btn-secondary ml-15"
                        >
                          Close
                        </button>
                      </div>{" "}
                    </div>
                  ) : (
                    <div className="modal-filter-form">
                      <div className="modal-body f-13">
                        <Copy className="pd-0">{`${this.state.tokenId}`}</Copy>

                        <div className="text-gray mt-lg">
                          You will be asked to provide a token when connecting to a
                          Notebook. Please use the above token when prompted
                        </div>
                      </div>
                      <div className="ml-15 mt-10">
                        {loader && <CircularProgress size={20} disableShrink />}

                        <button
                          onClick={() => MicroModal.close("notebook-token-modal")}
                          type="button"
                          className="btn btn-primary"
                        >
                          Close
                        </button>
                      </div>
                    </div>
                  )}
                </div>
              )}
            </main>
          </div>
        </div>
      </div>
      {/* notebook update modal */}
      <div
        className="modal micromodal-slide"
        id="modify-notebook"
        aria-hidden="true"
      >
        <div className="modal__overlay" tabIndex="-1" data-micromodal-close>
          <div
            className="modal__container"
            role="dialog"
            aria-modal="true"
            aria-labelledby="project-modal-title"
          >
            <header className="modal__header">
              <h2 className="modal__title">{`Update Notebook : ${this.state.notebook.name}`}</h2>
              <button
                type="button"
                className="modal__close"
                aria-label="Close modal"
                data-micromodal-close
              />
            </header>
            <main className="modal__content">
              <h5 className="mb-0">Resources</h5>
             <form>
              <div className="modal-filter-form">
                <div className="modal-body">
                 <div className="mt-30">
                  <FormControl fullWidth error={(this.props.user.tier === 'Standard Tier') && this.state.cpuWarning}>
                    <InputLabel htmlFor="notebook_cpu">CPU</InputLabel>
                    <Input
                       id={"notebook_cpu"}
                       name={"cpu"}
                      value={this.state.notebook.cpu || ""}
                      onBlur={this.handleBlurCpu}
                      onFocus={()=>this.setState({cpuWarning:""})}
                      onChange ={this.handleChange}
                      inputProps={{ min: 0.1, step: 0.1 }}
                      type="number"
                      aria-describedby="component-cpu-error-text"
                    />
                     <FormHelperText id="component-cpu-error-text">
                      {
                        (this.props.user.tier === 'Standard Tier') && this.state.cpuWarning ? this.state.cpuWarning:"vCPU/Core"
                      }
                      </FormHelperText>
                  </FormControl>
                 </div>
                 <div className="mt-30">
                    <FormControl fullWidth error={(this.props.user.tier === 'Standard Tier') && this.state.memWarning }>
                      <InputLabel htmlFor="notebook_memory">Memory</InputLabel>
                        <Input
                          id={"notebook_memory"}
                          name={"mem"}
                          inputProps={{ min: 0.5, step: 0.5 }}
                          type="number"
                          value={this.state.notebook.mem && this.state.notebook.mem.replace("Gi","") || ""}
                          onChange = {this.handleChange}
                          onBlur={this.handleBlurMem}
                          onFocus={()=>this.setState({memWarning:""})}
                          aria-describedby="component-mem-error-text"
                        />
                      <FormHelperText id="component-mem-error-text">
                        {
                          (this.props.user.tier === 'Standard Tier') && this.state.memWarning ? this.state.memWarning : "Gi"
                        }
                      </FormHelperText>
                    </FormControl>
                    </div>
                    <div className="mt-30">
                    <FormControl   fullWidth  disabled = {this.props.user.tier === 'Standard Tier'}>
                      <InputLabel htmlFor="notebook_gpu">GPU</InputLabel>
                      <Input
                       id="notebook_gpu"
                       name={"gpu"} 
                       inputProps={{ min: 0, step: 1 }}
                       type="number"
                       value={this.state.notebook.gpu || 0}
                       onChange = {this.handleChange} 
                       />
                      <FormHelperText>
                        {
                          (this.props.user.tier === 'Standard Tier') ? "Note: for current subscription you are not allowed to change GPU" : ""
                        }
                      </FormHelperText>
                    </FormControl>
                   </div>   
                  </div>
                
                <div className="modal-footer">
                  {loader && <CircularProgress size={20} disableShrink />}
                  <button
                    onClick={() => MicroModal.close("modify-notebook")}
                    type="button"
                    className="btn btn-secondary"
                  >
                    Cancel
                  </button>
                  <button
                    type="button"
                    className={`btn btn-primary clickable ${(this.state.cpuWarning || this.state.memWarning) ? "disabled":""}`}
                    onClick={this.updateNotebook}
                    disabled={this.state.cpuWarning || this.state.memWarning}
                  >
                    Update 
                  </button>
                </div>
              </div>
            </form> 
            </main>
          </div>
        </div>
      </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    user: state.users.user,
    selectedProject: state.projects.globalProject,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    ...bindActionCreators(
      {
        getUserDetail,
      },
      dispatch
    ),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Notebooks);
