import React, { Component } from 'react';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import User from "./shared/User";
import moment from "moment";
import CardLoader from "./models/cardLoader";
import { alertMsg } from "../ducks/alertsReducer";
import { getRegisteredModels, deletingRegisteredModel } from '../ducks/models';
import MicroModal from "micromodal";
import Pagination from "./common/pagination";
import RegisteredModelVersionsPreview from "./RegisteredModelVersionsPreview";
import { startCase } from "lodash";
import _ from "lodash";
import FilterDeployments from "./deployments/FilterDeployments";
import { globalProjectSelection } from "../ducks/projects";
import ReactTooltip from "react-tooltip";

class ModelCatalog extends Component {

    state = {
        projectID: "",
        modelsList: [],
        appliedFilters: {},
        filters: {
            projectName: "",
        },
        currentPage: 0,
        totalModels: 0,
    }

    componentDidMount() {
        this.getModels();
    }

    componentDidUpdate = (prevProps) => {
        if (this.props.selectedProject && prevProps.selectedProject.id !== this.props.selectedProject.id) {
            this.getModels();
        }
    }

    getModels = (filters={...this.state.filters}) => {
        const { currentPage } = this.state;
        const projectID = this.props.selectedProject && this.props.selectedProject.id;
        if(projectID)
            filters = {...filters, projectID: projectID}
        if(!filters.sort) {
            filters.sort = "created_date,desc";
        }
        this.props.getRegisteredModels(
            filters,
            (data) => {
                this.setState({
                    totalModels: data.headers["x-total-count"],
                    currentPageSize: data.data.length,
                    currentPage: currentPage
                })
            },
            (err) => {
                this.props.alertMsg("Unable to fetch Model, please try again", "error")
            }
        );
    }

    handleDelete = (id) => {
        this.props.deletingRegisteredModel(id,
            () => {
                this.getModels();
            },
            (err) => {
                this.props.alertMsg("Unable to delete Model, please try again", "error")
            }
        )
    }

    handleInputChange = e => {
        let filters = { ...this.state.filters };
        filters[e.target.name] = e.target.value;
        this.applyFilters(filters)
    };
    
    applyFilters = filters => {
        this.setState({ filters, currentPage: 1 });
        this.getModels(_.pickBy(filters, _.identity));
    }; 

    handleRemoveFilter = filter => {
        let updatedFilters = { ...this.state.filters };
        updatedFilters[filter] = null;
        this.setState({ filters: updatedFilters });
        this.applyFilters(updatedFilters);
    };

    handlePageChange = pageAction => {
        let pageNumber = this.state.currentPage;
        pageAction === "nextPage" ? (pageNumber += 1) : (pageNumber -= 1);
        let params = { page: pageNumber, ...this.state.filters };
        this.getModels(params);
    };
    
    getFilterTab = (data) => {
        switch (typeof (data)) {
            case "object":
                if (data.value) {
                    return `${data.key}-${data.value}`
                }
                if (data.key == "isBetween") {
                    return `${data.key}-${data.value1}&${data.value2}`
                }
                if (data.value1) {
                    return `${data.key}-${data.value1}`
                }
                break;
            default:
                return data
        }
    }

    renderTable = (registeredModels, currentId) => {
        return (
            <div>
                {registeredModels && registeredModels.map((registeredModel, index) => {
                    return (
                        <div className="expandable-table" key={"catlogs"+index}>
                            <div className="clickable" data-active={currentId === registeredModel.id ? "true" : "false"} key={"registered" + index} onClick={()=>{
                                this.setState({
                                    currentId: currentId === registeredModel.id ? "" : registeredModel.id
                                });
                            }}>
                                <span data-type="name">                                    
                                    <span className="text-link" onClick={() => { 
                                        this.props.history.push("/models/"+registeredModel.id)
                                    }}>
                                        {registeredModel.name}
                                    </span>
                                </span>
                                <span data-type="details">
                                    {registeredModel.description}
                                </span>
                                <span data-type="name">
                                    {registeredModel.project_name}
                                </span>
                                <span data-type="icon">
                                    {registeredModel.created_by && <User user={registeredModel.created_by} />}
                                </span>
                                <span data-type="date">
                                    {registeredModel.created_date && moment(registeredModel.created_date).format("LL")}
                                </span>
                                <span data-type="icon">
                                    {registeredModel.last_modified_by && <User user={registeredModel.last_modified_by} />}
                                </span>
                                <span data-type="date">
                                    {registeredModel.last_modified_date && moment(registeredModel.last_modified_date).format("LL")}
                                </span>
                                <span className="p-0" data-type="icon">
                                    <button
                                        style={{ margin: 6 }}
                                        className="btn btn-sm btn-danger"
                                        onClick={() => {
                                            this.handleDelete(registeredModel.id);
                                        }}
                                    >   
                                        <i className="fa fa-trash" />
                                    </button>
                                </span>
                                {currentId === registeredModel.id &&
                                <div>
                                    <hr />
                                    <RegisteredModelVersionsPreview registeredModelId={currentId} />
                                </div>}
                            </div>
                        </div>
                    );
                })}
            </div>
        )
    }

    render() {
        const { registeredModels } = this.props;
        const { filters, totalModels, currentPage, currentPageSize, currentId } = this.state;
        let appliedFilters = _.pickBy(filters, _.identity);
        return (
            <div className="main-container">
                <div className="main-body">
                    <div className="content-wraper-sm">
                        <div>
                            <div className="card exp-list">
                                <div className="card-header header-with-search pl-4 mr-5 pt-3">
                                    <h4>MODEL CATALOG</h4>
                                    <div className="card-search">
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="nameContains"
                                            value={filters.nameContains || ""}
                                            placeholder="SEARCH"
                                            onChange={(e) => {
                                                filters.nameContains = e.target.value
                                                this.setState({ filters })
                                                //this.applyFilters(filters)
                                            }}
                                            onKeyPress={event => {
                                                if (event.key === "Enter") {
                                                    this.handleInputChange(event)
                                                }
                                            }}
                                        />
                                    </div>
                                    <div className="card-tools">
                                        <div className="model-pagination inline-flex">
                                            {totalModels > 20 && (
                                                <Pagination
                                                    itemsCount={totalModels}
                                                    currentPage={currentPage}
                                                    currentPageSize={currentPageSize}
                                                    pageSize={20}
                                                    onPageChange={this.handlePageChange}
                                                />
                                            )}
                                        </div>
                                        <div className="model-pagination inline-flex">
                                            <button 
                                             className={`btn btn-sm btn-primary ml clickable ${!this.props.selectedProject ? "disabled":""}`}
                                             onClick={() => { this.props.history.push("/models/new") }} 
                                             type="button" 
                                             disabled ={!this.props.selectedProject}
                                             >
                                            Create Model
                                            </button>
                                            <button 
                                            className="btn btn-sm btn-default ml" 
                                            onClick={() => { this.getModels(_.pickBy(this.state.filters, _.identity)); }} 
                                            type="button"
                                            >
                                            <i className="fa fa-sync-alt" />
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div className="model-filter-header mr-5 pl-4">
                                    {/* <div className="filter-btn">
                                        <button
                                            className="btn btn-primary btn-sm btn-rounded"
                                            onClick={() => MicroModal.show('modal-deploy')}
                                        >
                                            Add Filter +
                                        </button>
                                    </div> */}
                                    <div className="applied-filters">
                                        <ul className="filter-items-list">
                                            {!_.isEmpty(appliedFilters) &&
                                                Object.keys(appliedFilters).map(key => (
                                                    <li key={key} className="filter-tag-item">
                                                        {startCase(key)}-{this.getFilterTab(appliedFilters[key])}
                                                        <i className="fa fa-times" style={{ marginTop: 3 }} onClick={() => this.handleRemoveFilter(key)}>
                                                        </i>
                                                    </li>
                                                ))}
                                        </ul>
                                    </div>
                                    <div className="filter-right-actions">
                                        <div className="row align-items-center justify-content-between">
                                            <div className="col">
                                                <label><small>Sort By</small></label>
                                                <select
                                                    name="sort"
                                                    onChange={this.handleInputChange}
                                                    id=""
                                                    className="form-control select-field"
                                                    value={appliedFilters.sort || ''}
                                                >
                                                    <option value=''>Select</option>
                                                    <option value="name,asc">Name</option>
                                                    <option value="created_date,desc">Created</option>
                                                </select>
                                            </div>
                                            <div className="col">
                                                <label><small>Created Date</small></label>
                                                <select
                                                    name="created_date"
                                                    id=""
                                                    onChange={this.handleInputChange}
                                                    className="form-control select-field"
                                                    value={appliedFilters.created_date || ''}
                                                >
                                                    <option value=''>Select</option>
                                                    <option value="now-1d">Last 1 day</option>
                                                    <option value="now-7d">Last 7 days</option>
                                                    <option value="now-30d">Last Month</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {registeredModels ?
                                    <div className="card-body h-min-370 pl-3 mr-5">
                                        {registeredModels && registeredModels.length > 0 ? this.renderTable(registeredModels, currentId) :
                                    
                                            <div className="empty-items-box">
                                                <h4>You don't have any Models in the Catalog!</h4>
                                            </div>
                                        }
                                    </div> :

                                    <CardLoader />
                                }
                            </div>
                        </div>
                    </div>
                </div>          
                <FilterDeployments
                    filterType={"models"}
                    noModel={true}
                    applyFilters={this.applyFilters}
                    filters={this.state.filters}
                    project={(/project/).test(this.props.match.path)}
                />
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token,
        user: state.users.user,
        registeredModels: state.models.registeredModels,
        selectedProject: state.projects.globalProject
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            getRegisteredModels,
            deletingRegisteredModel,
            alertMsg,
            globalProjectSelection
        },
        dispatch
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(ModelCatalog);