import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import RecentInsights from "./home/recentInsights";
import RecentModelDeployment from "./home/recentModelDeployment";
import ExperimentsOverview from "./home/experimentsOverview";
import ModelsOverview from "./home/ModelsOverview";
import RecentJobs from "./home/recentJobs";
import TutorialModal from "./walkthrough/tutorialModal";
import MicroModal from "micromodal";
import { experimentCounter } from "../ducks/experiments";
import Mixpanel from "mixpanel-browser";

class Home extends Component {

  state = {
    tutorial: true,
    run: true
  }

  async componentDidMount() {
    Mixpanel.track("Home Page");
    if(this.state.tutorial) {
      this.props.experimentCounter((count)=>{if(count === 0) MicroModal.show('tutorial-modal')});
    }
  }

  static getDerivedStateFromProps(nextProps) {
    if(nextProps.history.location.state && !nextProps.history.location.state.tutorial) {
      let tutorial = nextProps.history.location.state.tutorial;
      return {
        tutorial
      }
    }
    return null;
  }

  render() {
    return (
      <div className="main-container">
        {/* <NavigationLinks path={this.props.match.path} /> */}
        {/* <Sidebar path={this.props.match.path}/> */}
        <div className="main-body">
          <div className="content-wraper-sm">
            {/* <Counters history={this.props.history} /> */}
            <ExperimentsOverview />
            <ModelsOverview />
            <div className="row">
              <div className="col-md-6">
                <RecentModelDeployment />
              </div>
              <div className="col-md-6">
                <RecentInsights />
              </div>
            </div>
            <RecentJobs />
          </div>
        </div>
        <TutorialModal history={this.props.history} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token,
    experimentCounts: state.experiments.counter
  };
};

const mapDispatchToProps = dispatch => {
  return {
    ...bindActionCreators({
      experimentCounter
    }, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
