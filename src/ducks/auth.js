import { login as userLogin, register } from "../api";
// import { Route, Redirect } from "react-router";
import Mixpanel from "mixpanel-browser";
import { loadToken, saveState } from "../utils/storage";
import {
  LOGIN,
  LOGOUT,
  SIGNUP,
  AUTH_ERROR_MESSAGE,
  REG_ERROR_MESSAGE,
  INITAL_PASSWORD_RESET,
  AUTH_RESET
} from "./actionType";
import { getErrorMessage, getResponseError } from "../utils/helper";

// default state
const defaultState = {
  token: loadToken(),
  newPasswordRequired: false,
  loading: true
};

// Reducers
export default function reducer(state = defaultState, action = {}) {
  switch (action.type) {
    case AUTH_RESET:
      return {...state, message: null, regMessage: null, loading: true,sucessMessage:null};
    case LOGIN:
      return { ...state, token: action.token, message: action.message };
    case SIGNUP:
      return {
        ...state,
        sucessMessage: action.sucessMessage,
        message: action.message
      };
    case LOGOUT:
      return { ...state, token: null, newPasswordRequired: false };
    case AUTH_ERROR_MESSAGE:
      return { ...state, message: action.message, loading:false };
    case REG_ERROR_MESSAGE:
      return { ...state, regMessage: action.message, loading:false };
    case INITAL_PASSWORD_RESET:
      return { ...state, newPasswordRequired: action.newPasswordRequired, loading:false };
    default:
      return state;
  }
}

// Action creators
export function authReset(data) {
  return (dispatch) => {
    dispatch({
      type: AUTH_RESET
    });
  }
}

export function verifyUserAndLogin(userData) {
  // use thunk to call the api
  // when we have the data call the reducer to set the access token
  // userLogin(email, password);
  return dispatch => {
    userLogin(userData)
      .then(response => {
        const data = response.data;
        if (response.status) {
          // success
          Mixpanel.identify(userData.userName);
          Mixpanel.people.set({
            $last_login: new Date()
          });
          Mixpanel.people.increment("Login Count");
          if (data.newPasswordRequired && data.newPasswordRequired === true) {
            Mixpanel.people.set_once("First Login", new Date());
            window.location.pathname = `/initial_password_change/${userData.userName}`;
            dispatch({
              type: INITAL_PASSWORD_RESET,
              newPasswordRequired: true
            });
          } else {
            saveState(data);
            dispatch(login({ token: data.token }));
          }
        } else {
          //failed
          const errorMessage =
            getResponseError(response) || "Incorrect username or password.";
          Mixpanel.track("Login Failed", {
            userName: userData.userName,
            error: errorMessage
          });
          dispatch(login({ message: "Incorrect username or password." }));
        }
      })
      .catch(error => {
        const errorMessage =
          getErrorMessage(error) || "Incorrect username or password.";
        Mixpanel.track("Login Failed", {
          userName: userData.userName,
          error: errorMessage
        });
        dispatch({
          type: AUTH_ERROR_MESSAGE,
          message: errorMessage
        });
      });
  };
}

export function LoginUser(userData) {
  // use thunk to call the api
  // when we have the data call the reducer to set the access token
  // userLogin(email, password);
  return dispatch => {
    userLogin(userData)
      .then(response => {
        const data = response.data;
        if (response.status) {
          // clear the error message
          dispatch({
            type: AUTH_ERROR_MESSAGE,
            message: null
          });
          // success
          Mixpanel.identify(userData.userName);
          Mixpanel.people.set({
            $last_login: new Date()
          });
          Mixpanel.people.increment("Login Count");
          if (data.newPasswordRequired && data.newPasswordRequired === true) {
            Mixpanel.people.set_once("First Login", new Date());
            dispatch({
              type: INITAL_PASSWORD_RESET,
              newPasswordRequired: true
            });
          } else {
            saveState(data);
            dispatch(login({ token: data.token }));
          }
        } else {
          //failed
          const errorMessage =
            getResponseError(response) || "Incorrect username or password.";
          Mixpanel.track("Login Failed", {
            userName: userData.userName,
            error: errorMessage
          });
          dispatch(login({ message: "Incorrect username or password." }));
        }
      })
      .catch(error => {
        const errorMessage =
          getErrorMessage(error) || "Incorrect username or password.";
        Mixpanel.track("Login Failed", {
          userName: userData.userName,
          error: errorMessage
        });
        dispatch({
          type: AUTH_ERROR_MESSAGE,
          message: "Incorrect username or password."
        });
      });
  };
}

export function registerTenant(registerDetails) {
  return dispatch => {
    register(registerDetails)
      .then(response => {
        if (response.status === 200 || response.status === 201) {
          Mixpanel.alias(registerDetails.userName);
          Mixpanel.people.set({
            $email: registerDetails.email,
            $first_name: registerDetails.firstName,
            $last_name: registerDetails.lastName,
            $name: `${registerDetails.firstName} ${registerDetails.lastName}`,
            $created: new Date(),
            Tier: registerDetails.tier,
            "Company Name": registerDetails.companyName,
            "Account Name": registerDetails.accountName
          });
          Mixpanel.track("SignUp Successfull", registerDetails);
          dispatch({
            type: SIGNUP,
            sucessMessage:
              "Registration Successful. Check your email for login details."
          });
        } else {
          //failed
          const errorMessage =
            getResponseError(response) ||
            "An error occurred during Registration, please try again.";
          Mixpanel.track("SignUp Failed", {
            ...registerDetails,
            error: errorMessage
          });
          dispatch({
            type: REG_ERROR_MESSAGE,
            message: errorMessage
          });
        }
      })
      .catch(error => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred during Registration, please try again.";
        Mixpanel.track("SignUp Failed", {
          ...registerDetails,
          error: errorMessage
        });
        dispatch({
          type: REG_ERROR_MESSAGE,
          message: errorMessage
        });
      });
  };
}

export function logout() {
  Mixpanel.track("Logout");
  return dispatch => {
    // remove the token from storage
    saveState("");
    dispatch({ type: LOGOUT });
  };
}

// private action creators
function login({ token, message }) {
  return {
    type: LOGIN,
    token,
    message
  };
}
