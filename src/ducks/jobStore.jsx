import { getAllSecrets } from "../api";
import { getErrorMessage } from "../utils/helper";
import {
  JOB_STORE,
  JOB_STORE_WEBHOOK,
  JOB_PARAMETERS,
  JOB_ERROR,
  JOB_STEPS,
  JOB_STEP_TYPE,
  JOB_ACTIVE_STEP,
  SECRETS_LIST,
  JOB_RESET
} from './actionType';

// default state
const defaultState = {
  jobStore: {spec:{}},
  jobStoreWebhook: {spec:{}},
  jobError: {},
  selectedSteps: [],
  currentStep: "",
  secretsList: [],
  stepTypes: {},
  status: ''
}

// Reducers
export default function reducer(state = defaultState, action={}) {
  switch (action.type) {
    case JOB_ACTIVE_STEP:
      return {...state, currentStep: action.payload}
    case JOB_STEPS:
        let currentStep = state.currentStep
      return {...state, selectedSteps: action.payload, currentStep}
    case JOB_STEP_TYPE:
      return {...state, stepTypes: action.payload};
    case JOB_STORE:
      return {...state, jobStore: action.payload };
    case JOB_STORE_WEBHOOK:
      return {...state, jobStoreWebhook: action.payload};
    case JOB_PARAMETERS:
      return {...state, parameters: action.payload};
    case JOB_ERROR:
        return {...state, jobError: action.payload };
    case SECRETS_LIST:
        return {...state, secretsList: action.payload, status: action.status };
    case JOB_RESET:
        return {
          jobStore: {},
          jobStoreWebhook: {},
          jobError: {},
          selectedSteps: [],
          currentStep: "",
          secretsList: [],
          stepTypes: {},
          status: ''
        }
    default:
      return state;
  }
}

// Action creators
export function toggleSteps(data) {
  return (dispatch) => {
    dispatch({
      type: JOB_STEPS,
      payload: data
    })
  }
}

export function setStep(data = "") {
  return (dispatch) => {
    dispatch({
      type: JOB_ACTIVE_STEP,
      payload: data
    })
  }
}

export function setStepType(data = "") {
  return (dispatch) => {
    dispatch({
      type: JOB_STEP_TYPE,
      payload: data
    })
  }
}

export function changeJobStore(data) {
  return (dispatch) => {
    dispatch({
      type: JOB_STORE,
      payload: data
    });
  }
}

export function changeJobStoreWebhook(data) {
  return (dispatch) => {
    dispatch({
      type: JOB_STORE_WEBHOOK,
      payload: data
    })
  }
}

export function changeJobParameters(data) {
  return (dispatch) => {
    dispatch({
      type: JOB_PARAMETERS,
      payload: data
    })
  }
}

export function errorStore(data) {
  return (dispatch) => {
    dispatch({
      type: JOB_ERROR,
      payload: data
    })
  }
}

export function fetchSecrets(data) {
  return (dispatch, getStore) => {
    dispatch({
      type: SECRETS_LIST,
      payload: [],
      status: 'loading'
    })
    getAllSecrets(getStore().auth.token, data)
      .then(response => {
        if (response.status) {
          dispatch({
            type: SECRETS_LIST,
            payload: response.data,
            status: 'loaded'
          });
        }
      })
      .catch(error => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while creating the Job, please try again.";
          dispatch({
            type: JOB_ERROR,
            payload: errorMessage
          });
      });
  };
}

export function resetJobStore() {
  return (dispatch) => {
    dispatch({
      type: JOB_RESET,
      payload: null
    })
  }
}
