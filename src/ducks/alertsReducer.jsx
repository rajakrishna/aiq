// import {  getAllJobs, createJob, updateJob, runJob } from '../api';
import {
  ALERT_CLOSE,
  ALERT_MSG
} from './actionType';

// default state
const defaultState = {
    alActive: false,
    msg: '',
    alType: ''
}

// Reducers
export default function reducer(state = defaultState, action={}) {
  switch (action.type) {
    case ALERT_MSG:
      return {...action};
    case ALERT_CLOSE:
      return {...action};
    default:
      return state;
  }
}

// Action creators
export function alertMsg(msg, alType, alActive=true) {
  return (dispatch) => {
    dispatch({
        type: ALERT_MSG,
        msg,
        alType,
        alActive
    });
    }
}

export function closeAlert() {
    return (dispatch) => {
        dispatch({
            type: ALERT_CLOSE,
            alActive: false
        });
    }
}