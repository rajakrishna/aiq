import { getAllJobs, createJob, updateJob, runJob, getJobsCounts } from "../api";
import Mixpanel from "mixpanel-browser";
import {
  JOBS_LIST,
  JOB_RUN,
  JOB_CREATE,
  JOB_UPDATE,
  JOB_RESET,
  JOBS_COUNTER,
  JOB_LOADING,
  ERROR_MESSAGE,
} from "./actionType";
import { getErrorMessage, getResponseError } from "../utils/helper";

// default state
const defaultState = {
  jobs: [],
  jobId: "",
  counter: 0,
  message: "",
  successMessage: "",
  loader: false,
  jobSubmission: false,
  submitStatus: null,
  updateStatus: null
};

// Reducers
export default function reducer(state = defaultState, action = {}) {
  switch (action.type) {
    case JOBS_LIST:
      return {
        ...state,
        jobs: action.payload,
        message: "",
        successMessage: "",
        loader: false,
      };
    case JOB_CREATE:
      return { ...state, jobId: action.data, submitStatus: action.status, message: "", loader: false};
    case JOB_UPDATE:
      return { ...state, updateStatus: action.status, message: "", loader: false};
    case JOB_RUN:
      return { ...state, successMessage: action.message, loader: false};
    case JOB_LOADING:
      return { ...state, loader: action.loader}
    case JOBS_COUNTER:
      return { ...state, counter: action.payload}
    case ERROR_MESSAGE:
      return { ...state, message: action.message, submitStatus: action.status, updateStatus: action.status, loader: false};
    case JOB_RESET:
        return { 
          jobs: [],
          message: "",
          counter: 0,
          successMessage: "",
          jobSubmission: false,
          submitStatus: null,
          updateStatus: null,
          loader: false,
        };
    default:
      return state;
  }
}

// Action creators
export function jobsListAction(data) {
  return (dispatch ,getStore) => {
    dispatch({
      type: JOB_LOADING,
      loader: true
    });
    getAllJobs(getStore().auth.token, data)
      .then(response => {
        if (response.status) {
          dispatch({
            type: JOBS_LIST,
            payload: response.data
          });
        } else {
          const errorMessage =
            getResponseError(response) ||
            "An error occurred while fetching Jobs, please try again.";
          Mixpanel.track("Get Jobs Failed", { error: errorMessage });
          dispatch({
            type: ERROR_MESSAGE,
            message: errorMessage
          });
        }
      })
      .catch(error => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while fetching Jobs, please try again.";
        Mixpanel.track("Get Jobs Failed", { error: errorMessage });
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage
        });
      });
  };
}

// Action creators
export function createJobAction(data, callback, errCallback) {
  return (dispatch, getStore) => {
    dispatch({
      type: JOB_CREATE,
      status: "run"
    });
    createJob(getStore().auth.token, data)
      .then(response => {
        if (response.status) {
          Mixpanel.track('Create Job Successful', { data: data });
          dispatch({
            type: JOB_CREATE,
            data: response.data.id,
            status: true
          });
          callback && callback(response)
        }
      })
      .catch(error => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while creating the Job, please try again.";
        Mixpanel.track("Create Job Failed", { data: data, error: errorMessage });
        errCallback && errCallback(errorMessage)
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage,
          status: null
        });
      });
  };
}

export function jobCounter(callback) {
  return (dispatch, getStore) => {
    getJobsCounts(getStore().auth.token)
      .then(response => {
        if(response.status) {
          dispatch({
            type: JOBS_COUNTER,
            payload: response.data.count
          })
          if(response.data.count && callback)
            callback(response.data.count);
        }
      })
      .catch(error => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while updating the Job Counter, please try again.";
        Mixpanel.track("Update Job Counter Failed", { error: errorMessage });
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage,
          status: null
        });
      });
  }
}

export function updateJobAction(data, callback, errCallback) {
  return (dispatch, getStore) => {
    dispatch({
      type: JOB_UPDATE,
      status: "run"
    });
    updateJob(getStore().auth.token, data)
      .then(response => {
        if (response.status) {
          Mixpanel.track('Update Job Successful', { data: data });
          dispatch({
            type: JOB_UPDATE,
            status: true
          });
          callback && callback(response)
        }
      })
      .catch(error => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while updating the Job, please try again.";
        Mixpanel.track("Update Job Failed", { data: data, error: errorMessage });
        errCallback && errCallback(errorMessage)
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage,
          status: null
        });
      });
  };
}

export function runJobAction(access_token, data, parameters=[]) {
  return dispatch => {
    dispatch({
      type: JOB_LOADING,
      loader: true
    });
    runJob(access_token, data, parameters)
      .then(response => {
        if (response.status) {
          Mixpanel.track('Submit Job Successful', { data: data });
          dispatch({
            type: JOB_RUN,
            message: "Job submitted successfully"
          });
        }
      })
      .catch(error => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while submitting the Job, please try again.";
        Mixpanel.track("Submit Job Failed", { data: data, error: errorMessage });
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage
        });
      });
  };
}

export function resetJobs() {
  return (dispatch) => {
    dispatch({
      type: JOB_RESET,
      payload: null
    })
  }
}
