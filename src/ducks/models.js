import {
  getAllProjectsNames,
  getAllModelsByProject,
  getAllModels,
  getAllRegisteredModels,
  createRegisteredModels,
  getAllRegisteredModelsDetail,
  getAllRegisteredModelVersion,
  getRegisteredModelCounts,
  deleteRegisteredModel,
  deleteRegisteredModelVersion,
  downloadModelFileById,
  deployRegisteredModel
  } from '../api';
import {
  PROJECTS_NAMES,
  MODELS_LIST,
  MODELS_BY_STATUS,
  ERROR_MESSAGE,
  MODELS_REGISTERED_LIST,
  MODELS_REGISTERED,
  MODELS_REGISTERED_COUNTER,
  MODELS_REGISTERED_VERSION_LIST,
  MODELS_REGISTERED_VERSION_DELETE,
  MODELS_REGISTERED_DELETE,
  MODELS_REGISTERED_RESET,
  MODELS_LOADING,
  MODELS_DEPLOY,
  MODELS_FILE
} from './actionType';
import Mixpanel from "mixpanel-browser";
import { getErrorMessage } from "../utils/helper";
import FileDownload from "js-file-download";

// default state
const defaultState = {
  projectsNames: [],
  models: [],
  registeredModels: null,
  registeredModelDetail: [],
  registeredModelVersion: [],
  counter: 0,
  registered_model_counter: 0,
  loader: false,
  message: '',
  deployedModel: {}
}

// Reducers
export default function reducer(state = defaultState, action={}) {
  switch (action.type) {
    case PROJECTS_NAMES:
      return {...state, projectsNames: action.payload, loader: false };
    case MODELS_LIST:
      return {...state, models: action.payload, loader: false };
    case MODELS_REGISTERED_LIST:
      return {...state, registeredModels: action.payload, loader: false };
    case MODELS_REGISTERED_VERSION_LIST:
      return {...state, registeredModelVersion: action.payload, loader: false };
    case MODELS_REGISTERED:
      return {...state, registeredModelDetail: action.payload, loader: false };
    case MODELS_BY_STATUS:
      return {...state, models: action.payload, loader: false }
    case MODELS_REGISTERED_COUNTER:
      return {...state, registered_model_counter: action.payload, loader: false }
    case MODELS_REGISTERED_RESET:
      return { ...state, registeredModelDetail: [], loader: false }
    case ERROR_MESSAGE:
      return { ...state, message: action.message, loader: false };
    case MODELS_DEPLOY:
      return { ...state, deployedModel: action.payload, loader: false};
    case MODELS_LOADING:
      return { ...state, loader: action.payload };
    default:
      return state;
  }
}

// Action creators

export function getAllProjectsNamesList() {
  return (dispatch, getStore) => {
    dispatch({
      type: MODELS_LOADING,
      payload: true
    });
    getAllProjectsNames(getStore().auth.token)
    .then((response) => {
      const data = response.data;
      if(response.status) {
        let dataOption = data.map(el => ({ label: el.name, value: el.name }))
        dispatch({
          type: PROJECTS_NAMES,
          payload: dataOption
        });
      } else {
        const errorMessage = "Project not found";
        Mixpanel.track("Fetching Project failed", { error: errorMessage });
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage
        });
      }
    })
    .catch((error) => {
      const errorMessage = getErrorMessage(error) || "Project not found";
      Mixpanel.track("Fetching Project failed", { error: errorMessage });
      dispatch({
        type: ERROR_MESSAGE,
        message: errorMessage
      })
    })
  }
}

export function getRegisteredModels(data, callback, errCallback) {
  return (dispatch, getStore) => {
    dispatch({
      type: MODELS_LOADING,
      payload: true
    });
    getAllRegisteredModels(getStore().auth.token, data)
    .then((response) => {
      const data = response.data;
      if(response.status) {
        dispatch({
          type: MODELS_REGISTERED_LIST,
          payload: data
        });
        if(callback) callback(response)
      } else {
        dispatch({
          type: ERROR_MESSAGE,
          message: 'Error Fetching Models'
        });
        if(errCallback) errCallback(response)
      }
    })
    .catch((error) => {
      const errorMessage = getErrorMessage(error) || "An error occured while fetching registered models, please try again!";
      Mixpanel.track("Fetching Registered Model failed", { error: errorMessage });
      dispatch({
        type: ERROR_MESSAGE,
        message: errorMessage
      })
      if(errCallback) errCallback(error)
    })
  }
}

export function getRegisteredModelsVersion(data) {
  return (dispatch, getStore) => {
    if(data.noLoader) {
      dispatch({
        type: MODELS_LOADING,
        payload: true
      });
    }
    getAllRegisteredModelVersion(getStore().auth.token, data)
    .then((response) => {
      const data = response.data;
      if(response.status) {
        dispatch({
          type: MODELS_REGISTERED_VERSION_LIST,
          payload: data
        });
      } else {
        dispatch({
          type: ERROR_MESSAGE,
          message: 'Error Fetching Models'
        });
      }
    })
    .catch((error) => {
      const errorMessage = getErrorMessage(error) || "An error occured while fetching registered model versions, please try again!";
      Mixpanel.track("Fetching Registered Model Versions failed", { error: errorMessage });
      dispatch({
        type: ERROR_MESSAGE,
        message: errorMessage
      })
    })
  }
}

export function getRegisteredModelDetail(data) {
  return (dispatch, getStore) => {
    dispatch({
      type: MODELS_LOADING,
      payload: true
    });
    getAllRegisteredModelsDetail(getStore().auth.token, data)
    .then((response) => {
      const data = response.data;
      if(response.status) {
        dispatch({
          type: MODELS_REGISTERED,
          payload: data
        });
      } else {
        dispatch({
          type: ERROR_MESSAGE,
          message: 'Error Fetching Models'
        });
      }
    })
    .catch((error) => {
      const errorMessage = getErrorMessage(error) || "An error occured while fetching registered model details, please try again!";
      Mixpanel.track("Fetching Registered Model Details failed", { error: errorMessage });
      dispatch({
        type: ERROR_MESSAGE,
        message: errorMessage
      })
    })
  }
}

export function submitModelCatalog(data, callback, errCallback) {
  return (dispatch, getStore) => {
    dispatch({
      type: MODELS_LOADING,
      payload: true
    });
    createRegisteredModels(getStore().auth.token, data)
    .then((response) => {
      dispatch({
        type: MODELS_LOADING,
        payload: false
      });
      if(response.status) {
        if(callback) callback();
      } else {
        if(callback) callback();
      }
    })
    .catch((error) => {
      const errorMessage = getErrorMessage(error) || "An error occured while submitting models catalog, please try again!";
      Mixpanel.track("Submitting Model Catalog failed", { error: errorMessage });
      dispatch({
        type: MODELS_LOADING,
        payload: false
      });
      if(errCallback) errCallback(error);
    })
  }
}

export function deletingRegisteredModel(data, callback, errCallback) {
  return (dispatch, getStore) => {
    dispatch({
      type: MODELS_LOADING,
      payload: true
    });
    deleteRegisteredModel(getStore().auth.token, data)
    .then((response) => {
      if(response.status === 200) {
        if(callback) callback();
        dispatch({
          type: MODELS_REGISTERED_DELETE,
          payload: true
        });
      } else {
        if(errCallback) errCallback();
        dispatch({
          type: MODELS_REGISTERED_DELETE,
          payload: false
        });
      }
    })
    .catch((error) => {
      const errorMessage = getErrorMessage(error) || "An error occured while deleting registered models, please try again!";
      Mixpanel.track("Deleting Registered Model failed", { error: errorMessage });
      if(errCallback) errCallback(error);
      dispatch({
        type: ERROR_MESSAGE,
        payload: errorMessage
      });
    })
  }
}

export function deletingRegisteredModelVersion(data, callback, errCallback) {
  return (dispatch, getStore) => {
    dispatch({
      type: MODELS_LOADING,
      payload: true
    });
    deleteRegisteredModelVersion(getStore().auth.token, data)
    .then((response) => {
      if(response.status === 200) {
        if(callback) callback();
        dispatch({
          type: MODELS_REGISTERED_VERSION_DELETE,
          payload: true
        });
      } else {
        if(errCallback) errCallback();
        dispatch({
          type: MODELS_REGISTERED_VERSION_DELETE,
          payload: false
        });
      }
    })
    .catch((error) => {
      const errorMessage = getErrorMessage(error) || "An error occured while deleting registered model version, please try again!";
      Mixpanel.track("Deleting Registered Model Version failed", { error: errorMessage });
      if(errCallback) errCallback(error);
      dispatch({
        type: ERROR_MESSAGE,
        payload: errorMessage
      });
    })
  }
}

export function getAllModelsByProjectNames(project_name) {
  return (dispatch, getStore) => {
    dispatch({
      type: MODELS_LOADING,
      payload: true
    });
    getAllModelsByProject(getStore().auth.token, project_name)
    .then((response) => {
      const data = response.data;
      if(response.status) {
        dispatch({
          type: MODELS_LIST,
          payload: data
        });
      } else {
        dispatch({
          type: ERROR_MESSAGE,
          message: 'Project not found'
        });
      }
    })
    .catch((error) => {
      const errorMessage = getErrorMessage(error) || "An error occurred while fetching the Model, please try again.";
      Mixpanel.track("Fetching model by project name failed", { error: errorMessage });
      dispatch({
        type: ERROR_MESSAGE,
        message: errorMessage
      })
    })
  }
}

export function getModelsByProjectStatus(data){
  return(dispatch, getStore) => {
    dispatch({
      type: MODELS_LOADING,
      payload: true
    });
    getAllModels(getStore().auth.token, data)
    .then((response) => {
      const data = response.data;
      if(response.status) {
        dispatch({
          type: MODELS_BY_STATUS,
          payload: data
        })
      }
    })
    .catch((error) => {
      const errorMessage = getErrorMessage(error) || "An error occurred while fetching the Model, please try again.";
      Mixpanel.track("Fetching model by project status failed", { error: errorMessage });
      dispatch({
        type: ERROR_MESSAGE,
        message: error.message
      })
    })
  }
}

export function modelCounter(callback) {
  return (dispatch, getStore) => {
    getRegisteredModelCounts(getStore().auth.token)
      .then(response => {
        if(response.status) {
          dispatch({
            type: MODELS_REGISTERED_COUNTER,
            payload: response.data.count
          })
          if(response.data.count && callback)
            callback(response.data.count);
        }
      })
      .catch(error => {
        const errorMessage = getErrorMessage(error) || "An error occurred while updating the Registered Model Counter, please try again.";
        Mixpanel.track("Update Registered Model Counter Failed", { error: errorMessage });
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage,
          status: null
        });
      });
  }
}

export function downloadAnyModelFile(modelId, callback, errCallback) {
  return (dispatch, getStore) => {
    downloadModelFileById(getStore().auth.token, modelId)
    .then((response) => {
      const data = response.data;
      FileDownload(data, modelId);
      if(response.status) {
        dispatch({
          type: MODELS_FILE,
          payload: data
        });
      } else {
        dispatch({
          type: ERROR_MESSAGE,
          message: 'Invalid Link'
        });
      }
      if(callback) callback();
    })
    .catch((error) => {
      const errorMessage = getErrorMessage(error) || "An error occurred while Downloading the Model File, please try again.";
      Mixpanel.track("Downloading Model file failed", { error: errorMessage });
      dispatch({
        type: ERROR_MESSAGE,
        message: errorMessage
      })
      if(errCallback) errCallback(errorMessage);
    })
  }
}

export function actionDeployRegisteredModel(data, callback, errCallback) {
  return (dispatch, getStore) => {
    deployRegisteredModel(getStore().auth.token, data)
    .then((response) => {
      const data = response.data;
      if(response.status) {
        dispatch({
          type: MODELS_DEPLOY,
          payload: data
        });
        if(callback) callback();
      } else {
        dispatch({
          type: ERROR_MESSAGE,
          message: 'Unable to Deploy Registered Model'
        });
        if(errCallback) errCallback();
      }
    })
    .catch((error) => {
      const errorMessage = getErrorMessage(error) || "An error occurred while Deploying the Registered Model, please try again.";
      Mixpanel.track("Deploying Registered Model failed", { error: errorMessage });
      dispatch({
        type: ERROR_MESSAGE,
        message: errorMessage
      })
      if(errCallback) errCallback();
    })
  }
}

export function clearRegisteredModelStore() {
  return (dispatch) => {
    dispatch({
      type: MODELS_REGISTERED_RESET
    })
  }
}