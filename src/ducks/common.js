import {
    EVENT_LISTENER,
} from './actionType';
// import Mixpanel from "mixpanel-browser";
// import { getErrorMessage } from "../utils/helper";

// default state
const defaultState = {
    events: {}
}

// Reducers
export default function reducer(state = defaultState, action = {}) {
    switch (action.type) {
        case EVENT_LISTENER:
            return { ...state, events: action.payload };
        default:
            return state;
    }
}

// Action creators

export function eventPosters(data) {
    return (dispatch) => {
        dispatch({
            type: EVENT_LISTENER,
            payload: data
        });   
    }
}