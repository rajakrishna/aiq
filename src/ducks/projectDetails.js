import {
  getProject
} from '../api';
import {
  PROJECTS_DETAILS,
  ERROR_MESSAGE
} from './actionType';

const defaultState = {
  projectDetails: [],
  message: ''
}

// Reducers
export default function reducer(state = defaultState, action={}) {
  switch (action.type) {
    case PROJECTS_DETAILS:
      return {...state, projectDetails: action.payload };
    case ERROR_MESSAGE:
      return { ...state, projectErr: action.message};
    default:
      return state;
  }
}

// Action creators

export function getProjectDetails(projectId) {
  return (dispatch,getState) => {
    getProject(getState().auth.token,projectId)
    .then((response) => {
      const data = response.data;
      if(response.status) {
        dispatch({
          type: PROJECTS_DETAILS,
          payload: data
        });
      } else {
        dispatch({
          type: ERROR_MESSAGE,
          message: 'Project Details not found'
        });
      }
    })
    .catch((error) => {
      dispatch({
        type: ERROR_MESSAGE,
        message: error.message
      })
    })
  }
}
