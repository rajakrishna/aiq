import {  getAllModels, createExperiment, getModelCounts, getAllMLFlow, getMLFlow, getMLFlowRunList } from '../api';
import Mixpanel from "mixpanel-browser";
import { getErrorMessage } from "../utils/helper";
import {
  EXPERIMENTS_LIST,
  EXPERIMENTS_MLFLOW_LIST,
  EXPERIMENTS_MLFLOW_DETAIL,
  EXPERIMENT_CREATE,
  ERROR_MESSAGE,
  EXPERIMENTS_LOADING,
  EXPERIMENTS_MLFLOW_LOADING,
  EXPERIMENTS_COUNTER
} from './actionType';

// default state
const defaultState = {
  experiments: [],
  mlFlowList: [],
  mlFlowDetail: {},
  message: '',
  loader: false,
  mlLoader: false,
  counter: null,
}

// Reducers
export default function reducer(state = defaultState, action={}) {
  switch (action.type) {
    case EXPERIMENTS_LIST:
      return {...state, experiments: action.payload, loader: false };
    case EXPERIMENTS_MLFLOW_LIST:
      return {...state, mlFlowList: action.payload, mlLoader: false };
    case EXPERIMENTS_MLFLOW_DETAIL:
      return {...state, mlFlowDetail: action.payload };
    case EXPERIMENTS_LOADING:
      return {...state, loader: action.loader};
    case EXPERIMENTS_MLFLOW_LOADING:
      return {...state, mlLoader: action.loader};
    case EXPERIMENT_CREATE:
      return {...state, successMessage: action.message, loader: false };
    case EXPERIMENTS_COUNTER:
      return {...state, counter: action.payload, loader: false};
    case ERROR_MESSAGE:
      return { ...state, message: action.message, loader: false, mlLoader: false};
    default:
      return state;
  }
}

// Action creators
export function getExperimentList(data) {
  return (dispatch, getStore) => {
    dispatch({
      type: EXPERIMENTS_LOADING,
      loader: true
    });

    getAllModels(getStore().auth.token, data)
    .then((response) => {
      const data = response.data;
      if(response.status) {
        dispatch({
          type: EXPERIMENTS_LIST,
          payload: data
        });
      } else {
        dispatch({
          type: ERROR_MESSAGE,
          message: 'Unable to list experiments'
        });
      }
    })
    .catch((error) => {
      const errorMessage = getErrorMessage(error) || "An error occurred while fetching the Experiment list, please try again.";
      Mixpanel.track("Fetching Experiment List Failed", { error: errorMessage });
      dispatch({
        type: ERROR_MESSAGE,
        message: errorMessage
      })
    })
  }
}

export function getMlFlowList(data={}, callback, errCallback) {
  return (dispatch, getStore) => {
    dispatch({
      type: EXPERIMENTS_MLFLOW_LOADING,
      loader: true
    });

    getAllMLFlow(getStore().auth.token, data)
    .then((response) => {
      const data = response.data;
      if(response.status) {
        dispatch({
          type: EXPERIMENTS_MLFLOW_LIST,
          payload: data.experiments
        });
      } else {
        dispatch({
          type: ERROR_MESSAGE,
          message: 'Unable to list ML Flow Experiments'
        });
      }
      callback && callback();
    })
    .catch((error) => {
      const errorMessage = (error.response && error.response.data && error.response.data.detail) || getErrorMessage(error) || "An error occurred while fetching the MLFlow Experiment list, please try again.";
      Mixpanel.track("Fetching MLFlow Experiment List Failed", { error: errorMessage });
      errCallback && errCallback(errorMessage);
      dispatch({
        type: ERROR_MESSAGE,
        message: errorMessage
      })
    })
  }
}

export function fetchMlFlowRunList(data={}, callback, errCallback) {
  return (dispatch, getStore) => {
    getMLFlowRunList(getStore().auth.token, data)
    .then((response) => {
      const data = response.data;
      if(response.status) {
        callback && callback(data);
      }
    })
    .catch((error) => {
      const errorMessage = getErrorMessage(error) || "An error occurred while fetching the MLFlow Run list, please try again.";
      Mixpanel.track("Fetching MLFlow Run List Failed", { error: errorMessage });
      dispatch({
        type: ERROR_MESSAGE,
        message: errorMessage
      })
      errCallback && errCallback(error);
    })
  }
}

export function fetchMlFlow(param, callback, errCallback) {
  return (dispatch, getStore) => {
    getMLFlow(getStore().auth.token, param)
    .then((response) => {
      const data = response.data;
      if(response.status) {
        dispatch({
          type: EXPERIMENTS_MLFLOW_DETAIL,
          payload: data
        });
        callback && callback(data)
      } else {
        dispatch({
          type: ERROR_MESSAGE,
          message: `Unable to fetch ML Flow Experiment ${param.id}`
        });
        errCallback && errCallback(data)
      }
    })
    .catch((error) => {
      const errorMessage = getErrorMessage(error) || "An error occurred while fetching the MLFlow Experiment, please try again.";
      Mixpanel.track("Fetching MLFlow Experiment Failed", { error: errorMessage });
      dispatch({
        type: ERROR_MESSAGE,
        message: errorMessage
      })
      errCallback && errCallback(errorMessage)
    })
  }
}

// Action creators
export function createExperimentAction(access_token, data) {
  return (dispatch) => {
    dispatch({
      type: EXPERIMENTS_LOADING,
      loader: true
    });
    createExperiment(access_token,data)
    .then((response) => {
      if(response.status) {
        dispatch({
          type: EXPERIMENT_CREATE,
          message: 'Experiment successfully created'
        });
      }
    })
    .catch((error) => {
      const errorMessage = getErrorMessage(error) || "An error occurred while creating the Experiment, please try again.";
      Mixpanel.track("Create Experiment Failed", { error: errorMessage });
      dispatch({
        type: ERROR_MESSAGE,
        message: errorMessage
      })
    })
  }
}

export function experimentCounter(callback) {
  return (dispatch, getStore) => {
    getModelCounts(getStore().auth.token, {statusEquals: "EXPERIMENT"})
      .then(response => {
        if(response.status) {
          dispatch({
            type: EXPERIMENTS_COUNTER,
            payload: response.data.count
          })
          if(callback)
            callback(response.data.count);
        }
      })
      .catch(error => {
        const errorMessage = getErrorMessage(error) || "An error occurred while updating the Experiment Counter, please try again.";
        Mixpanel.track("Update Experiment Counter Failed", { error: errorMessage });
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage,
          status: null
        });
      });
  }
}
