import {
    getAllNotifications,
    getNotificationCounts,
    updateNotificationAsRead
} from '../api';
import {
    NOTIFICATIONS,
    NOTIFICATIONS_CUSTOM,
    NOTIFICATIONS_COUNT,
    NOTIFICATIONS_UNREAD_COUNT,
    NOTIFICATIONS_READ,
    NOTIFICATIONS_ERROR,
    NOTIFICATIONS_LOAD
} from './actionType';
import Mixpanel from "mixpanel-browser";
import { getErrorMessage } from "../utils/helper";

// default state
const defaultState = {
    notifications: [],
    customNotification: [],
    notifications_count: 0,
    notifications_unread_count: 0,
    message: '',
    page: 0,
    loader: false
}

// Reducers
export default function reducer(state = defaultState, action = {}) {
    switch (action.type) {
        case NOTIFICATIONS:
            return { ...state, notifications: action.payload, page: action.page, loader: action.loading };
        case NOTIFICATIONS_CUSTOM:
            return { ...state, customNotification: action.payload };
        case NOTIFICATIONS_COUNT:
            return { ...state, notifications_count: action.payload };
        case NOTIFICATIONS_UNREAD_COUNT:
            return { ...state, notifications_unread_count: action.payload };
        case NOTIFICATIONS_ERROR:
            return { ...state, message: action.message };
        case NOTIFICATIONS_LOAD:
            return { ...state, loader: action.loading }
        default:
            return state;
    }
}

// Action creators

export function notificationsList(params = {}) {
    return (dispatch, getStore) => {
        dispatch({
            type: NOTIFICATIONS_LOAD,
            loading: true
        });
        getAllNotifications(getStore().auth.token, params)
            .then((response) => {
                const data = response.data;
                if (response.status) {
                    dispatch({
                        type: NOTIFICATIONS,
                        payload: data,
                        loading: false,
                        page: params.pageNumber
                    });
                } else {
                    dispatch({
                        type: NOTIFICATIONS_ERROR,
                        loading: false,
                        message: 'No notification found!'
                    });
                }
            })
            .catch((error) => {
                const errorMessage =
                getErrorMessage(error) ||
                "An error occurred while fetching Notifications, please try again.";
                Mixpanel.track("Get Notifications Failed", { error: errorMessage });
                dispatch({
                    type: NOTIFICATIONS_ERROR,
                    loading: false,
                    message: errorMessage
                })
            })
    }
}

export function customNotificationsList(params = {}, callback, errCallback) {
    return (dispatch, getStore) => {
        getAllNotifications(getStore().auth.token, params)
            .then((response) => {
                const data = response.data;
                if (response.status) {
                    dispatch({
                        type: NOTIFICATIONS_CUSTOM,
                        payload: data,
                    });
                }
                callback && callback();
            })
            .catch((error) => {
                const errorMessage =
                getErrorMessage(error) ||
                "An error occurred while fetching Notifications, please try again.";
                Mixpanel.track("Get Notifications Failed", { error: errorMessage });
                errCallback && errCallback();
            })

    }
}

export function notificationCounts(params = {}) {
    return (dispatch, getStore) => {
        getNotificationCounts(getStore().auth.token, params)
            .then((response) => {
                const data = response.data;
                if (response.status) {
                    dispatch({
                        type: NOTIFICATIONS_COUNT,
                        payload: data.count
                    });
                } else {
                    dispatch({
                        type: NOTIFICATIONS_ERROR,
                        message: 'Unable to fetch notification count'
                    });
                }
            })
            .catch((error) => {
                const errorMessage =
                getErrorMessage(error) ||
                "An error occurred while fetching Notifications Counter, please try again.";
                Mixpanel.track("Get Notifications Counter Failed", { error: errorMessage });
                dispatch({
                    type: NOTIFICATIONS_ERROR,
                    message: errorMessage
                })
            })
    }
}

export function notificationUnreadCounts(params = {read: false}) {
    return (dispatch, getStore) => {
        getNotificationCounts(getStore().auth.token, params)
        .then((response) => {
            const data = response.data;
            if (response.status) {
                dispatch({
                    type: NOTIFICATIONS_UNREAD_COUNT,
                    payload: data.count
                });
            } else {
                dispatch({
                    type: NOTIFICATIONS_ERROR,
                    message: 'Unable to fetch notification count'
                });
            }
        })
        .catch((error) => {
            const errorMessage = getErrorMessage(error) ||
                "An error occurred while fetching Unread Notifications Counter, please try again.";
                Mixpanel.track("Get Unread Notifications Counter Failed", { error: errorMessage });
            dispatch({
                type: NOTIFICATIONS_ERROR,
                message: errorMessage
            })
        })
    }
}

export function markAsRead(params) {
    return (dispatch, getStore) => {
        updateNotificationAsRead(getStore().auth.token, params)
            .then((response) => {
                const data = response.data;
                if (response.status) {
                    dispatch({
                        type: NOTIFICATIONS_READ,
                        payload: data
                    })
                }
            })
            .catch((error) => {
                const errorMessage = getErrorMessage(error) ||
                "An error occurred while marking notification read, please try again.";
                Mixpanel.track("Marking notification read Failed", { error: errorMessage });
                dispatch({
                    type: NOTIFICATIONS_ERROR,
                    message: errorMessage
                })
            })
    }
}
