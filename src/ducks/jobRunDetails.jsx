import {
    getJobRun,
    getLogs,
    getJobsRunById,
    getJobsRunList,
    delJobRun
  } from '../api';
import {
  JOB_RUN_LOADING,
  JOB_RUN_DETAILS,
  JOB_RUN_LIST,
  JOB_RUN_DETAILS_RESET,
  LOGS,
  ERROR_MESSAGE
} from './actionType';
import { getErrorMessage } from "../utils/helper";
import Mixpanel from "mixpanel-browser";
  
  const defaultState = {
    jobRunDetails: {},
    loader: false,
    logs: '',
    message: ''
  }
  
  // Reducers
  export default function reducer(state = defaultState, action={}) {
    switch (action.type) {
      case JOB_RUN_DETAILS:
        return {...state, jobRunDetails: action.payload, loader: action.loader };
      case JOB_RUN_DETAILS_RESET:
        return {...state, jobRunDetails: {}, loader: false };
      case JOB_RUN_LIST:
        return {...state, jobRunList: action.payload, loader: action.loader };
      case JOB_RUN_LOADING:
        return {...state, loader: action.loader };
      case LOGS:
        return {...state, logs: action.payload };
      case ERROR_MESSAGE:
        return { ...state, message: action.message, loader: action.loader};
      default:
        return state;
    }
  }
  
  // Action creators

  export function clearJobRunDetails() {
    return (dispatch) => {
      dispatch({
        type: JOB_RUN_DETAILS_RESET
      })
    }
  }
  
  export function getjobRunDetails(jobRunName, callback, errCallback) {
    return (dispatch, getStore) => {
      dispatch({
        type: JOB_RUN_LOADING,
        loader: true
      });
      getJobRun(getStore().auth.token, jobRunName)
      .then((response) => {
        const data = response.data;
        if(response.status) {
          dispatch({
            type: JOB_RUN_DETAILS,
            payload: data,
            loader: false
          });
        } else {
          dispatch({
            type: ERROR_MESSAGE,
            message: `An error occurred while fetching Job Run Details for ${jobRunName}, please try again.`,
            loader: false
          });
        }
        callback && callback(data);
      })
      .catch((error) => {
        const errorMessage = getErrorMessage(error) || `An error occurred while fetching Job Run Details for ${jobRunName}, please try again.`;
        Mixpanel.track("Get Job Run Details Failed", { error: errorMessage });
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage,
          loader: false
        })
        errCallback && errCallback(errorMessage)
      })
    }
  }

  export function workflowRunListById(id) {
    return (dispatch, getStore) => {
      dispatch({
        type: JOB_RUN_LOADING,
        loader: true
      });
      getJobsRunById(getStore().auth.token, id)
      .then((response) => {
        const data = response.data;
        if(response.status) {
          dispatch({
            type: JOB_RUN_LIST,
            payload: data,
            loader: false
          });
        } else {
          dispatch({
            type: ERROR_MESSAGE,
            message: `An error occurred while fetching Job Run for ${id}, please try again.`,
            loader: false
          });
        }
      })
      .catch((error) => {
        const errorMessage = getErrorMessage(error) || `An error occurred while fetching Job Run for ${id}, please try again.`;
        Mixpanel.track("Get Job Run Failed", { error: errorMessage });
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage,
          loader: false
        })
      })
    }
  }

  export function workflowRunList(params) {
    return (dispatch, getStore) => {
      dispatch({
        type: JOB_RUN_LOADING,
        loader: true
      });
      getJobsRunList(getStore().auth.token, params)
      .then((response) => {
        const data = response.data;
        if(response.status) {
          dispatch({
            type: JOB_RUN_LIST,
            payload: data,
            loader: false
          });
        } else {
          dispatch({
            type: ERROR_MESSAGE,
            message: "An error occurred while fetching Job Run, please try again.",
            loader: false
          });
        }
      })
      .catch((error) => {
        const errorMessage = getErrorMessage(error) || "An error occurred while fetching Job Run, please try again.";
        Mixpanel.track("Get Job Run Failed", { error: errorMessage });
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage,
          loader: false
        })
      })
    }
  }

  export function workflowRunDelete(name, callback) {
    return (dispatch, getStore) => {
      delJobRun(getStore().auth.token, name)
      .then((response) => {
        if(response.status) {
          callback();
        } else {
          Mixpanel.track("An error occurred while deleting Job Run, please try again.");
          dispatch({
            type: ERROR_MESSAGE,
            message: "An error occurred while deleting Job Run, please try again.",
            loader: false
          });
        }
      })
      .catch((error) => {
        const errorMessage = getErrorMessage(error) || "An error occurred while deleting Job Run, please try again.";
        Mixpanel.track("Delete Job Run Failed", { error: errorMessage });
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage,
          loader: false
        })
      })
    }
  }

  export function getjobRunLogs(jobRunName) {
    return (dispatch, getStore) => {
      getLogs(getStore().auth.token, jobRunName)
      .then((response) => {
        const data = response.data;
        if(response.status) {
          dispatch({
            type: LOGS,
            payload: data
          });
        } else {
          dispatch({
            type: ERROR_MESSAGE,
            message: 'No logs found',
            loader: false
          });
        }
      })
      .catch((error) => {
        const errorMessage = getErrorMessage(error) || "An error occurred while fetching Job Run logs, please try again.";
        Mixpanel.track("Get Job Run logs Failed", { error: errorMessage });
        dispatch({
            type: ERROR_MESSAGE,
            message: errorMessage,
            loading: false
        })
      })
    }
  }
  