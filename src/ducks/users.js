import { getAllUsers, getCurrentUser } from "../api";
import {
  USERS_LIST,
  ERROR_MESSAGE,
  USER_DETAILS,
  USER_ERROR
} from "./actionType";
import Mixpanel from "mixpanel-browser";
import { getErrorMessage, getResponseError } from "../utils/helper";

// default state
const defaultState = {
  users: [],
  user: {},
  message: ""
};

// Reducers
export default function reducer(state = defaultState, action = {}) {
  switch (action.type) {
    case USERS_LIST:
      return { ...state, users: action.payload, message: "" };
    case USER_DETAILS:
      return { ...state, user: action.payload, message: "" };
    case ERROR_MESSAGE:
      return { ...state, message: action.message };
    case USER_ERROR:
      return { ...state, message: action.message };
    default:
      return state;
  }
}

// Action creators
export function getUsersList() {
  return (dispatch, getStore) => {
    getAllUsers(getStore().auth.token)
      .then(response => {
        const data = response.data;
        if (response.status === 200) {
          dispatch({ type: USERS_LIST, payload: data });
        } else {
          const errorMessage =
            getResponseError(response) ||
            "An error occurred while fetching Users, please try again.";
          Mixpanel.track("Get Users List Failed", { error: errorMessage });
          dispatch({ type: ERROR_MESSAGE, message: errorMessage });
        }
      })
      .catch(error => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while fetching Users, please try again.";
        Mixpanel.track("Get Users List Failed", { error: errorMessage });
        dispatch({ type: ERROR_MESSAGE, message: errorMessage });
      });
  };
}

export function getUserDetail() {
  return (dispatch, getStore) => {
    getCurrentUser(getStore().auth.token)
      .then(response => {
        const data = response.data;
        if (response.status) {
          Mixpanel.people.set({
            $first_name: data.firstName,
            $last_name: data.lastName,
            $name: `${data.firstName} ${data.lastName}`,
            Enabled: data.enabled,
            Status: data.confirmedStatus,
            Tier: data.tier,
            Role: data.role
          });
          dispatch({ type: USER_DETAILS, payload: data });
        } else {
          const errorMessage =
            getResponseError(response) ||
            "An error occurred while fetching User details, please try again.";
          Mixpanel.track("Get User Details Failed", { error: errorMessage });
          dispatch({ type: USER_ERROR, message: errorMessage });
        }
      })
      .catch(error => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while fetching User details, please try again.";
        Mixpanel.track("Get User Details Failed", { error: errorMessage });
        dispatch({ type: USER_ERROR, message: errorMessage });
      });
  };
}
