import {
    getAllSecrets, 
    createSecrets, 
    updateSecrets, 
    deleteSecrets
} from '../api';
import {
    SECRETS_LIST,
    SECRETS_CREATE,
    SECRETS_UPDATE,
    SECRETS_DELETE,
    ERROR_MESSAGE
} from './actionType';

import Mixpanel from "mixpanel-browser";
import { getErrorMessage } from "../utils/helper";

// default state
const defaultState = {
    list: [],
    create: "",
    update: "",
    delete: "",
    filteredList: null,
    loader: false
}

// Reducers
export default function reducer(state = defaultState, action = {}) {
    switch (action.type) {
        case SECRETS_LIST:
            return { ...state, list: action.payload, loader: false }
        case SECRETS_CREATE:
            return { ...state, create: action.payload, loader: false }
        case SECRETS_UPDATE:
            return { ...state, update: action.payload, loader: false }
        case SECRETS_DELETE:
            return { ...state, delete: action.payload, loader: false }
        case ERROR_MESSAGE:
            return { ...state, errorMessage: action.error, loader: false }
        default:
            return state;
    }
}

// Action creators

export function getAllSecretAction() {
    return (dispatch, getStore) => {
        getAllSecrets(getStore().auth.token)
        .then((response) => {
            const data = response.data;
            if (response.status) {
                dispatch({
                    type: SECRETS_LIST,
                    payload: data
                });
            } else {
                dispatch({
                    type: ERROR_MESSAGE,
                    message: 'Error while fetching secrets'
                });
            }
        })
        .catch((error) => {
            const errorMessage = getErrorMessage(error) || "An error occurred while fetching the Secrets, please try again.";
            Mixpanel.track("Get Secrets Failed", { error: errorMessage });
            dispatch({
                type: ERROR_MESSAGE,
                message: errorMessage
            })
        })
    }
}

export function createSecretsAction(data = {}, callback, errCallback) {
    return (dispatch, getStore) => {
        createSecrets(getStore().auth.token, data)
        .then((response) => {
            const data = response.data;
            if (response.status) {
                dispatch({
                    type: SECRETS_CREATE,
                    payload: data
                });
                if(callback) callback();
            } else {
                dispatch({
                    type: ERROR_MESSAGE,
                    message: 'Error while fetching secrets'
                });
            }
        })
        .catch((error) => {
            const errorMessage = getErrorMessage(error) || "An error occurred while creating the Secret, please try again.";
            if(errCallback) errCallback(errorMessage);
            Mixpanel.track("Create Secrets Failed", { error: errorMessage });
            dispatch({
                type: ERROR_MESSAGE,
                message: errorMessage
            })
        })
    }
}

export function updateSecretsAction(data = {}) {
    return (dispatch, getStore) => {
        updateSecrets(getStore().auth.token, data)
        .then((response) => {
            const data = response.data;
            if (response.status) {
                dispatch({
                    type: SECRETS_UPDATE,
                    payload: data
                });
            } else {
                dispatch({
                    type: ERROR_MESSAGE,
                    message: 'Error while updating secrets'
                });
            }
        })
        .catch((error) => {
            const errorMessage = getErrorMessage(error) || "An error occurred while updating the Secret, please try again.";
            Mixpanel.track("Update Secrets Failed", { error: errorMessage });
            dispatch({
                type: ERROR_MESSAGE,
                message: errorMessage
            })
        })
    }
}

export function deleteSecretsAction(name, callback) {
    return (dispatch, getStore) => {
        deleteSecrets(getStore().auth.token, name)
        .then((response) => {
            const data = response.data;
            if (response.status) {
                dispatch({
                    type: SECRETS_DELETE,
                    payload: data
                });
                callback && callback();
            } else {
                dispatch({
                    type: ERROR_MESSAGE,
                    message: 'Error while deleting secrets'
                });
            }
        })
        .catch((error) => {
            const errorMessage = getErrorMessage(error) || "An error occurred while deleting the Secret, please try again.";
            Mixpanel.track("Delete Secrets Failed", { error: errorMessage });
            dispatch({
                type: ERROR_MESSAGE,
                message: errorMessage
            })
        })
    }
}