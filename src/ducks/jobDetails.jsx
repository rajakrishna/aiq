import {
    getJob,
  } from '../api';
  import {
    JOB_DETAILS,
    ERROR_MESSAGE
  } from './actionType';
  
  const defaultState = {
    jobDetails: {},
    message: ''
  }
  
  // Reducers
  export default function reducer(state = defaultState, action={}) {
    switch (action.type) {
      case JOB_DETAILS:
        return {...state, jobDetails: action.payload };
      case ERROR_MESSAGE:
        return { ...state, message: action.message};
      default:
        return state;
    }
  }
  
  // Action creators
  
  export function getjobDetails(jobId) {
    return (dispatch, getStore) => {
      getJob(getStore().auth.token, jobId)
      .then((response) => {
        const data = response.data;
        if(response.status) {
          dispatch({
            type: JOB_DETAILS,
            payload: data
          });
        } else {
          dispatch({
            type: ERROR_MESSAGE,
            message: 'Job Details not found'
          });
        }
      })
      .catch((error) => {
        dispatch({
          type: ERROR_MESSAGE,
          message: error.message
        })
      })
    }
  }
  