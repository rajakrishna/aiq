import {
  getAllProjects,
  createProject,
  getAllProjectsData,
  getProjectCounts,
} from "../api";
import Mixpanel from "mixpanel-browser";
import {
  PROJECTS_RESET,
  PROJECTS_LIST,
  PROJECTS_SUMMARY,
  PROJECTS_CREATE,
  PROJECTS_ERROR_MESSAGE,
  PROJECTS_COUNTER,
  PROJECTS_LOADING,
  GLOBAL_PROJECT_SET,
  GLOBAL_PROJECT_GET,
} from "./actionType";
import { getErrorMessage, getResponseError } from "../utils/helper";

// default state
const defaultState = {
  projects: [],
  summary: [],
  message: "",
  loading: false,
  counter: 0,
  globalProject: {},
};

// Reducers
export default function reducer(state = defaultState, action = {}) {
  switch (action.type) {
    case PROJECTS_LOADING:
      return { ...state, loading: true };
    case PROJECTS_RESET:
      return { ...state, loading: false };
    case PROJECTS_LIST:
      return {
        ...state,
        projects: action.payload,
        message: "",
        loading: false,
      };
    case PROJECTS_SUMMARY:
      return {
        ...state,
        summary: action.payload.data,
        projects_count: action.payload.headers["x-total-count"],
        message: "",
        loading: false,
        };
    case PROJECTS_CREATE:
      return {
        ...state,
        successMessage: action.message,
        message: "",
        loading: false,
      };
    case PROJECTS_ERROR_MESSAGE:
      return {
        ...state,
        message: action.message,
        loading: false,
      };
    case PROJECTS_COUNTER:
      return {
        ...state,
        counter: action.payload,
      };
    case GLOBAL_PROJECT_SET:
      return {
        ...state,
        globalProject: action.payload,
      };
    case GLOBAL_PROJECT_GET:
      return {
        ...state,
        globalProject: action.payload,
      };
    default:
      return state;
  }
}

// Action creators
export function projectReset(data) {
  return (dispatch) => {
    dispatch({
      type: PROJECTS_RESET,
    });
  };
}

export function getProjectsSummary(data) {
  return (dispatch, getStore) => {
    dispatch({
      type: PROJECTS_LOADING,
    });
    getAllProjectsData(getStore().auth.token, data)
      .then((response) => {
        //const data = response.data;
        if (response.status === 200) {
          console.log("Action type is PROJECTS_SUMMARY")
          dispatch({
            type: GLOBAL_PROJECT_SET,
            payload: response.data[0],
          });
          dispatch({
            type: PROJECTS_SUMMARY,
            payload: response,
          });
        
        } else {
          const errorMessage =
            getResponseError(response) ||
            "An error occurred while fetching Projects, please try again.";
          Mixpanel.track("Get Projects Summary Failed", {
            error: errorMessage,
          });
          dispatch({
            type: PROJECTS_ERROR_MESSAGE,
            message: errorMessage,
          });
        }
      })
      .catch((error) => {
        let errorMessage =
          getErrorMessage(error) ||
          "An error occurred while fetching Projects, please try again.";
        Mixpanel.track("Get Projects Summary Failed", { error: errorMessage });
        dispatch({
          type: PROJECTS_ERROR_MESSAGE,
          message: errorMessage,
        });
      });
  };
}

export function getProjectsList(params = {}) {
  return (dispatch, getStore) => {
    dispatch({
      type: PROJECTS_LOADING,
    });
    getAllProjects(getStore().auth.token, params)
      .then((response) => {
        const data = response.data;
        if (response.status) {
          dispatch({
            type: PROJECTS_LIST,
            payload: data,
          });
        } else {
          const errorMessage =
            getResponseError(response) ||
            "An error occurred while fetching Projects, please try again.";
          Mixpanel.track("Get Projects List Failed", {
            error: errorMessage,
          });
          dispatch({
            type: PROJECTS_ERROR_MESSAGE,
            message: errorMessage,
          });
        }
      })
      .catch((error) => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while fetching Projects, please try again.";
        Mixpanel.track("Get Projects List Failed", {
          error: errorMessage,
        });
        dispatch({
          type: PROJECTS_ERROR_MESSAGE,
          message: errorMessage,
        });
      });
  };
}

// Action creators
export function createProjectAction(data, callback, errCallback) {
  return (dispatch, getStore) => {
    dispatch({
      type: PROJECTS_LOADING,
    });
    createProject(getStore().auth.token, data)
      .then((response) => {
        if (response.status === 200 || response.status === 201) {
          Mixpanel.track("Create Project Successful", {
            "Project Name": data.name,
            "Project Description": data.description,
            "Project Users": data.users,
          });
          if (callback) {
            callback();
          }
          dispatch({
            type: PROJECTS_CREATE,
            message: "Project created successfully",
          });
        } else {
          const errorMessage =
            getResponseError(response) ||
            "An error occurred while creating the Project, please try again.";
          Mixpanel.track("Create Project Failed", {
            "Project Name": data.name,
            "Project Description": data.description,
            "Project Users": data.users,
            error: errorMessage,
          });
          if (errCallback) errCallback(errorMessage);
          dispatch({
            type: PROJECTS_ERROR_MESSAGE,
            message: errorMessage,
          });
        }
      })
      .catch((error) => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while creating the Project, please try again.";
        Mixpanel.track("Create Project Failed", {
          "Project Name": data.name,
          "Project Description": data.description,
          "Project Users": data.users,
          error: errorMessage,
        });
        if (errCallback) errCallback(errorMessage);
        dispatch({
          type: PROJECTS_ERROR_MESSAGE,
          message: errorMessage,
        });
      });
  };
}

export function projectCounter(data = {}, callback) {
  return (dispatch, getStore) => {
    getProjectCounts(getStore().auth.token, data)
      .then((response) => {
        if (response.status) {
          dispatch({
            type: PROJECTS_COUNTER,
            payload: response.data.count,
          });
          if (response.data.count && callback) callback(response.data.count);
        }
      })
      .catch((error) => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while updating the Project Counter, please try again.";
        Mixpanel.track("Update Project Counter Failed", {
          error: errorMessage,
        });
        dispatch({
          type: PROJECTS_ERROR_MESSAGE,
          message: errorMessage,
          status: null,
        });
      });
  };
}
export function globalProjectSelection(data, callback) {
  return (dispatch) => {
    dispatch({
      type: GLOBAL_PROJECT_SET,
      payload: data,
    });
    if ((data, callback)) {
      callback(data);
    }
  };
}
