import {
  getDeploymentList,
  getDeployment,
  getDeploymentLog,
  getDeploymentCpu,
  getDeploymentMemory,
  scalingDeployment,
  delDeployment,
  createDeployment,
  downloadPredictionFile,
  getBatchScoringRuns,
  deleteBatchScoreRun,
  startDeployments,
  stopDeployments,
  metricsRequestrate,
  metricsSuccess,
  metrics5xx,
  metrics4xx,
  metricsReqpersec,
  metricsLatency,
} from "../api";
import {
  DEPLOYMENTS_LIST,
  DEPLOYMENTS_DETAIL,
  DEPLOYMENTS_CPU,
  DEPLOYMENTS_LOG,
  DEPLOYMENTS_MEMORY,
  DEPLOYMENTS_LOADING,
  DEPLOYMENTS_SCALING,
  DEPLOYMENTS_COUNTER,
  ERROR_MESSAGE,
  CREATE_DEPLOYMENT,
  PREDICTION_FILE,
  CREATE_BATCHSCORERUN,
  BATCHSCORERUNS_LIST,
  DELETE_BATCHSCORERUN,
  DELETE_BATCHSCORERUN_ERROR,
  BATCHSCORERUNS_LIST_PER_PAGE,
  BATCHSCORERUNS_LIST_PER_PAGE_ERROR,
  STOP_DEPLOYMENT,
  START_DEPLOYMENT,
  DEPLOYMENTS_METRIX_REQ_RATE,
  DEPLOYMENTS_METRIX_SUCCESS_RATE,
  DEPLOYMENTS_METRIX_4XX_RATE,
  DEPLOYMENTS_METRIX_5XX_RATE,
  DEPLOYMENTS_METRIX_REQ_PERSEC,
  DEPLOYMENTS_METRIX_LATANCY
} from "./actionType";
import { getErrorMessage, getResponseError } from "../utils/helper";
import Mixpanel from "mixpanel-browser";
import { saveAs } from "file-saver";

// default state
const defaultState = {
  deployments: [],
  deployment: {},
  deployment_log: "",
  deployment_cpu: "",
  deployment_memory: "",
  counter: 0,
  message: "",
  loader: false,
  batchscoreruns: [],
  batchscorerunsPerPage: [],
  batchscorerun: {},
  metricsRequestrateData: {},
  metricsSuccessData: {},
  metrics5xxData: {},
  metrics4xxData: {},
  metricsReqpersecData: {},
  metricsLatency:[]
};

// Reducers
export default function reducer(state = defaultState, action = {}) {
  switch (action.type) {
    case DEPLOYMENTS_LOADING:
      return { ...state, loader: action.loading };
    case DEPLOYMENTS_LIST:
      return { ...state, deployments: action.payload, loader: action.loading };
    case DEPLOYMENTS_DETAIL:
      return { ...state, deployment: action.payload, loader: action.loading };
    case DEPLOYMENTS_LOG:
      return {
        ...state,
        deployment_log: action.payload,
        loader: action.loading,
      };
    case DEPLOYMENTS_CPU:
      return {
        ...state,
        deployment_cpu: action.payload,
        loader: action.loading,
      };
    case DEPLOYMENTS_MEMORY:
      return {
        ...state,
        deployment_memory: action.payload,
        loader: action.loading,
      };
    case DEPLOYMENTS_COUNTER:
      return { ...state, counter: action.payload };
    case CREATE_DEPLOYMENT:
      return { ...state, deployment: action.payload };
    case ERROR_MESSAGE:
      return { ...state, message: action.message, loader: action.loading };
    case PREDICTION_FILE:
      return { ...state, predictionFile: action.payload };
    case CREATE_BATCHSCORERUN:
      return { ...state, batchscorerun: action.payload };
    case BATCHSCORERUNS_LIST:
      return {
        ...state,
        batchscoreruns: action.payload,
        loader: action.loading,
      };
    case DELETE_BATCHSCORERUN:
      return {
        ...state,
        message: action.message,
      };
    case DELETE_BATCHSCORERUN_ERROR:
      return {
        ...state,
        message: action.message,
      };
    case BATCHSCORERUNS_LIST_PER_PAGE:
      return {
        ...state,
        batchscorerunsPerPage: action.payload,
      };
    case BATCHSCORERUNS_LIST_PER_PAGE_ERROR:
      return {
        ...state,
        message: action.message,
      };
    case DEPLOYMENTS_METRIX_REQ_RATE:
      return {
        ...state,
        metricsRequestrateData: action.payload,
      };

    case DEPLOYMENTS_METRIX_SUCCESS_RATE:
      return {
        ...state,
        metricsSuccessData: action.payload,
      };
    case DEPLOYMENTS_METRIX_5XX_RATE:
      return {
        ...state,
        metrics5xxData: action.payload,
      };
    case DEPLOYMENTS_METRIX_4XX_RATE:
      return {
        ...state,
        metrics4xxData: action.payload,
      };

    case DEPLOYMENTS_METRIX_REQ_PERSEC:
      return {
        ...state,
        metricsReqpersecData: action.payload,
      };
      case DEPLOYMENTS_METRIX_LATANCY:
        return {
          ...state,
          metricsLatency: action.payload,
        };
    default:
      return state;
  }
}

// Action creators

export function getDeployments(params = {}) {
  return (dispatch, getStore) => {
    dispatch({
      type: DEPLOYMENTS_LOADING,
      loading: true,
    });
    getDeploymentList(getStore().auth.token, params)
      .then((response) => {
        const data = response.data;
        if (response.status) {
          dispatch({
            type: DEPLOYMENTS_LIST,
            payload: data,
            loading: false,
          });
        } else {
          dispatch({
            type: ERROR_MESSAGE,
            message: "Deployments not found",
            loading: false,
          });
        }
      })
      .catch((error) => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while fetching Deployments, please try again.";
        Mixpanel.track("Get Deployments Failed", { error: errorMessage });
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage,
          loading: false,
        });
      });
  };
}

export function getDeploymentDetail(name, callback, errCallback) {
  return (dispatch, getStore) => {
    dispatch({
      type: DEPLOYMENTS_LOADING,
      loading: true,
    });
    getDeployment(getStore().auth.token, name)
      .then((response) => {
        const data = response.data;
        if (response.status) {
          dispatch({
            type: DEPLOYMENTS_DETAIL,
            payload: data,
            loading: false,
          });
          if (callback) callback();
        } else {
          dispatch({
            type: ERROR_MESSAGE,
            message: "Deployment not found",
            loading: false,
          });
          if (errCallback) errCallback();
        }
      })
      .catch((error) => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while fetching Deployment Details, please try again.";
        Mixpanel.track("Get Deployment Details Failed", {
          error: errorMessage,
        });
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage,
          loading: false,
        });
        if (errCallback) errCallback(errorMessage);
      });
  };
}

export function getDeploymentLogs(data, callback, errCallback) {
  return (dispatch, getStore) => {
    dispatch({
      type: DEPLOYMENTS_LOADING,
      loading: true,
    });
    getDeploymentLog(getStore().auth.token, data)
      .then((response) => {
        const data = response.data;
        if (response.status) {
          dispatch({
            type: DEPLOYMENTS_LOG,
            payload: data,
            loading: false,
          });
          if (callback) callback();
        } else {
          dispatch({
            type: ERROR_MESSAGE,
            message: "Unable to fetch Logs",
            loading: false,
          });
          if (errCallback) errCallback();
        }
      })
      .catch((error) => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while fetching Deployment Logs, please try again.";
        Mixpanel.track("Get Deployment Logs Failed", { error: errorMessage });
        dispatch({
          type: DEPLOYMENTS_LOG,
          payload: { error: errorMessage },
          loading: false,
        });
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage,
          loading: false,
        });
        if (errCallback) errCallback(errorMessage);
      });
  };
}

export function getDeploymentsCpu(data, callback, errCallback) {
  return (dispatch, getStore) => {
    dispatch({
      type: DEPLOYMENTS_LOADING,
      loading: true,
    });
    getDeploymentCpu(getStore().auth.token, data)
      .then((response) => {
        const data = response.data;
        if (response.status) {
          dispatch({
            type: DEPLOYMENTS_CPU,
            payload: data,
            loading: false,
          });
          if (callback) callback();
        } else {
          dispatch({
            type: ERROR_MESSAGE,
            message: "Unable to fetch CPU Usage",
            loading: false,
          });
          if (errCallback) errCallback();
        }
      })
      .catch((error) => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while fetching Deployment CPU usage, please try again.";
        Mixpanel.track("Get Deployment CPU Usage Failed", {
          error: errorMessage,
        });
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage,
          loading: false,
        });
        if (errCallback) errCallback(errorMessage);
      });
  };
}

export function getDeploymentsMemory(data, callback, errCallback) {
  return (dispatch, getStore) => {
    dispatch({
      type: DEPLOYMENTS_LOADING,
      loading: true,
    });
    getDeploymentMemory(getStore().auth.token, data)
      .then((response) => {
        const data = response.data;
        if (response.status) {
          dispatch({
            type: DEPLOYMENTS_MEMORY,
            payload: data,
            loading: false,
          });
          if (callback) callback();
        } else {
          dispatch({
            type: ERROR_MESSAGE,
            message: "Unable to fetch Memory Usage",
            loading: false,
          });
          if (errCallback) errCallback();
        }
      })
      .catch((error) => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while fetching Deployment Memory usage, please try again.";
        Mixpanel.track("Get Deployment Memory Usage Failed", {
          error: errorMessage,
        });
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage,
          loading: false,
        });
        if (errCallback) errCallback(errorMessage);
      });
  };
}

export function deleteDeployment(name, callback, errCallback) {
  return (dispatch, getStore) => {
    delDeployment(getStore().auth.token, name)
      .then(() => {
        callback && callback();
      })
      .catch((error) => {
        const errorMessage =
          getErrorMessage(error) ||
          `An error occurred while deleting Deployment "${name}", please try again.`;
        Mixpanel.track("Delete Deployment Failed", { error: errorMessage });
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage,
        });
        errCallback && errCallback();
      });
  };
}

export function scaleDeployment(details, callback, errCallback) {
  return (dispatch, getStore) => {
    // dispatch({
    //     type: DEPLOYMENTS_LOADING,
    //     loading: true
    // });
    scalingDeployment(getStore().auth.token, details)
      .then(() => {
        // dispatch({
        //     type: DEPLOYMENTS_SCALING,
        //     loading: false
        // });
        callback && callback();
      })
      .catch((error) => {
        const errorMessage =
          getErrorMessage(error) ||
          `An error occurred while Scaling Deployment "${details.name}", please try again.`;
        Mixpanel.track("Scaling Deployment Failed", { error: errorMessage });
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage,
        });
        errCallback && errCallback();
      });
  };
}

// Action creators
export function createDeploymentAction(data, callback, errCallback) {
  return (dispatch, getStore) => {
    dispatch({
      type: DEPLOYMENTS_LOADING,
    });
    createDeployment(getStore().auth.token, data)
      .then((response) => {
        if (response.status === 200 || response.status === 201) {
          Mixpanel.track("Create Deployment Successful", {
            "Deploymemt Name": data.name,
          });
          if (callback) {
            callback();
          }
          dispatch({
            type: CREATE_DEPLOYMENT,
            message: "Deployment created successfully",
          });
        } else {
          const errorMessage =
            getResponseError(response) ||
            "An error occurred while creating the Deployment, please try again.";
          Mixpanel.track("Create Deployment Failed", {
            "Deployment Name": data.name,

            error: errorMessage,
          });
          if (errCallback) errCallback(errorMessage);
          dispatch({
            type: ERROR_MESSAGE,
            message: errorMessage,
          });
        }
      })
      .catch((error) => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while creating the Deployment, please try again.";
        Mixpanel.track("Create Deployment Failed", {
          "Project Name": data.name,
          error: errorMessage,
        });
        if (errCallback) errCallback(errorMessage);
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage,
        });
      });
  };
}

export function downloadPrediction(
  name,
  predictionId,
  progress,
  cancellationToken
) {
  return (dispatch, getStore) => {
    downloadPredictionFile(
      getStore().auth.token,
      name,
      predictionId,
      progress,
      cancellationToken
    )
      .then((response) => {
        const data = response.data;

        saveAs(data, name);
        if (response.status) {
          dispatch({
            type: PREDICTION_FILE,
            payload: data,
          });
        } else {
          dispatch({
            type: ERROR_MESSAGE,
            message: "Invalid Link",
          });
        }
      })
      .catch((error) => {
        dispatch({
          type: ERROR_MESSAGE,
          message: error.message,
        });
        progress(error.message);
      });
  };
}

// get all batch score list
export function getBatchScoreRuns(params = {}) {
  return (dispatch, getStore) => {
    dispatch({
      type: DEPLOYMENTS_LOADING,
      loading: true,
    });
    getBatchScoringRuns(getStore().auth.token, params)
      .then((response) => {
        const data = response.data;
        if (response.status) {
          dispatch({
            type: BATCHSCORERUNS_LIST,
            payload: data,
            loading: false,
          });
        } else {
          dispatch({
            type: ERROR_MESSAGE,
            message: "Deployments not found",
            loading: false,
          });
        }
      })
      .catch((error) => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while fetching Batch score runs, please try again.";
        Mixpanel.track("Get Batchscores Failed", { error: errorMessage });
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage,
          loading: false,
        });
      });
  };
}

// get batch scores list per page
export function getBatchScoreRunsPerPage(params = {}) {
  return (dispatch, getStore) => {
    getBatchScoringRuns(getStore().auth.token, params)
      .then((response) => {
        if (response.status) {
          dispatch({
            type: BATCHSCORERUNS_LIST_PER_PAGE,
            payload: response.data,
          });
        } else {
          dispatch({
            type: BATCHSCORERUNS_LIST_PER_PAGE_ERROR,
            message: "batch score runs not found...",
          });
        }
      })
      .catch((error) => {
        const errorMessage =
          getErrorMessage(error) ||
          "Error occurred while fetching Batch Scoring Runs. Please try again or contact support.";
        dispatch({
          type: BATCHSCORERUNS_LIST_PER_PAGE_ERROR,
          message: errorMessage,
        });
      });
  };
}

// delete a batchScoreRunItem
export function deleteBatchScoreRunItem(deployment_name, item_id, callback) {
  return (dispatch, getStore) => {
    deleteBatchScoreRun(getStore().auth.token, deployment_name, item_id)
      .then((res) => {
        dispatch({
          type: DELETE_BATCHSCORERUN,
          message: "Batch Scoring Run deleted successfully",
        });
        if (callback) callback();
      })
      .catch((err) => {
        dispatch({
          type: DELETE_BATCHSCORERUN_ERROR,
          message: "Failed to delete the Batch Scoring Run",
        });
        if (callback) callback();
      });
  };
}
export function startDeployment(name, callback, errCallback) {
  return (dispatch, getStore) => {
    dispatch({
      type: DEPLOYMENTS_LOADING,
      loading: true,
    });
    startDeployments(getStore().auth.token, name)
      .then((response) => {
        if (response.status) {
          dispatch({
            type: DEPLOYMENTS_DETAIL,
            payload: response.data,
            loading: false,
          });
          if (callback) callback();
        } else {
          dispatch({
            type: ERROR_MESSAGE,
            message: "Failed to start deployment",
            loading: false,
          });
          if (errCallback) errCallback();
        }
      })
      .catch((error) => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while starting Deployment, please try again.";
        Mixpanel.track("Deployment Start Failed", { error: errorMessage });
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage,
          loading: false,
        });
      });
  };
}

export function stopDeployment(name, callback, errCallback) {
  return (dispatch, getStore) => {
    dispatch({
      type: DEPLOYMENTS_LOADING,
      loading: true,
    });
    stopDeployments(getStore().auth.token, name)
      .then((response) => {
        const data = response.data;
        if (response.status) {
          dispatch({
            type: DEPLOYMENTS_DETAIL,
            payload: response.data,
            loading: false,
          });
          if (callback) callback();
        } else {
          dispatch({
            type: ERROR_MESSAGE,
            message: "Failed to stop deployment",
            loading: false,
          });
          if (errCallback) errCallback();
        }
      })
      .catch((error) => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while Stopping the Deployment..., please try again.";
        Mixpanel.track("Unable to stop Deployment", {
          error: errorMessage,
        });
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage,
          loading: false,
        });
        if (errCallback) errCallback(errorMessage);
      });
  };
}
export function reStartDeployment(name) {
  return (dispatch, getStore) => {
    dispatch({
      type: DEPLOYMENTS_LOADING,
      loading: true,
    });
    stopDeployments(getStore().auth.token, name)
      .then((response) => {
        startDeployments(getStore().auth.token, name)
          .then((response) => {
            const data = response.data;
            dispatch({
              type: DEPLOYMENTS_DETAIL,
              payload: data,
              loading: false,
            });
          })
          .catch((error) => {
            const errorMessage =
              getErrorMessage(error) ||
              "An error occurred while starting Deployment, please try again.";
            Mixpanel.track("Deployment Start Failed", { error: errorMessage });
            dispatch({
              type: ERROR_MESSAGE,
              message: errorMessage,
              loading: false,
            });
          });
      })
      .catch((error) => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while stopping Deployment..., please try again.";
        Mixpanel.track("Deployment Start Failed", { error: errorMessage });
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage,
          loading: false,
        });
      });
  };
}

export function getMetricsRequestrate(data, callback, errCallback) {
  return (dispatch, getStore) => {
    
    metricsRequestrate(getStore().auth.token, data)
      .then((response) => {
        const data = response.data;
        if (response.status) {
          console.log("metricsRequestrate", data);
          dispatch({
            type: DEPLOYMENTS_METRIX_REQ_RATE,
            payload: data,
            loading: false,
          });
          if (callback) callback();
        } else {
          dispatch({
            type: ERROR_MESSAGE,
            message: "Unable to fetch CPU Usage",
            loading: false,
          });
          if (errCallback) errCallback();
        }
      })
      .catch((error) => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while fetching Deployment CPU usage, please try again.";
        Mixpanel.track("Get Deployment CPU Usage Failed", {
          error: errorMessage,
        });
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage,
          loading: false,
        });
        if (errCallback) errCallback(errorMessage);
      });
  
   
  };
}
export function getMetricsSuccess(data, callback, errCallback) {
  return (dispatch, getStore) => {
    // dispatch({
    //   type: DEPLOYMENTS_LOADING,
    //   loading: true,
    // });
   
    metricsSuccess(getStore().auth.token, data)
      .then((response) => {
        const data = response.data;
        if (response.status) {
          console.log("metricsSuccess", data);
          dispatch({
            type: DEPLOYMENTS_METRIX_SUCCESS_RATE,
            payload: data,
            loading: false,
          });
          if (callback) callback();
        } else {
          dispatch({
            type: ERROR_MESSAGE,
            message: "Unable to fetch CPU Usage",
            loading: false,
          });
          if (errCallback) errCallback();
        }
      })
      .catch((error) => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while fetching Deployment CPU usage, please try again.";
        Mixpanel.track("Get Deployment CPU Usage Failed", {
          error: errorMessage,
        });
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage,
          loading: false,
        });
        if (errCallback) errCallback(errorMessage);
      });
    }
}
export function getMetrics5xx(data, callback, errCallback) {
  return (dispatch, getStore) => {
    // dispatch({
    //   type: DEPLOYMENTS_LOADING,
    //   loading: true,
    // });
    
    metrics5xx(getStore().auth.token, data)
      .then((response) => {
        const data = response.data;
        if (response.status) {
          console.log("metrics5xx", data);
          dispatch({
            type: DEPLOYMENTS_METRIX_5XX_RATE,
            payload: data,
            loading: false,
          });
          if (callback) callback();
        } else {
          dispatch({
            type: ERROR_MESSAGE,
            message: "Unable to fetch CPU Usage",
            loading: false,
          });
          if (errCallback) errCallback();
        }
      })
      .catch((error) => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while fetching Deployment CPU usage, please try again.";
        Mixpanel.track("Get Deployment CPU Usage Failed", {
          error: errorMessage,
        });
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage,
          loading: false,
        });
        if (errCallback) errCallback(errorMessage);
      });
   
  };
}
export function getMetrics4xx(data, callback, errCallback) {
  return (dispatch, getStore) => {
    // dispatch({
    //   type: DEPLOYMENTS_LOADING,
    //   loading: true,
    // });
   
    metrics4xx(getStore().auth.token, data)
      .then((response) => {
        const data = response.data;
        if (response.status) {
          console.log("metrics4xx", data);
          dispatch({
            type: DEPLOYMENTS_METRIX_4XX_RATE,
            payload: data,
            loading: false,
          });
          if (callback) callback();
        } else {
          dispatch({
            type: ERROR_MESSAGE,
            message: "Unable to fetch CPU Usage",
            loading: false,
          });
          if (errCallback) errCallback();
        }
      })
      .catch((error) => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while fetching Deployment CPU usage, please try again.";
        Mixpanel.track("Get Deployment CPU Usage Failed", {
          error: errorMessage,
        });
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage,
          loading: false,
        });
        if (errCallback) errCallback(errorMessage);
      });
  
  };
}
export function getMetricsReqpersec(data, callback, errCallback) {
  return (dispatch, getStore) => {
    // dispatch({
    //   type: DEPLOYMENTS_LOADING,
    //   loading: true,
    // });
   
    metricsReqpersec(getStore().auth.token, data)
      .then((response) => {
        const data = response.data;
        if (response.status) {
          dispatch({
            type: DEPLOYMENTS_METRIX_REQ_PERSEC,
            payload: data,
            loading: false,
          });
          if (callback) callback();
        } else {
          dispatch({
            type: ERROR_MESSAGE,
            message: "Unable to fetch CPU Usage",
            loading: false,
          });
          if (errCallback) errCallback();
        }
      })
      .catch((error) => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while fetching Deployment CPU usage, please try again.";
        Mixpanel.track("Get Deployment CPU Usage Failed", {
          error: errorMessage,
        });
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage,
          loading: false,
        });
        if (errCallback) errCallback(errorMessage);
      });
  };
}

export function getMetricsLatency(data, callback, errCallback) {
  return (dispatch, getStore) => {
    // dispatch({
    //   type: DEPLOYMENTS_LOADING,
    //   loading: true,
    // });
   
    metricsLatency(getStore().auth.token, data)
      .then((response) => {
        const data = response.data;
        if (response.status) {
          dispatch({
            type: DEPLOYMENTS_METRIX_LATANCY,
            payload: data,
            loading: false,
          });
          if (callback) callback();
        } else {
          dispatch({
            type: ERROR_MESSAGE,
            message: "Unable to fetch CPU Usage",
            loading: false,
          });
          if (errCallback) errCallback();
        }
      })
      .catch((error) => {
        const errorMessage =
          getErrorMessage(error) ||
          "An error occurred while fetching Deployment CPU usage, please try again.";
        Mixpanel.track("Get Deployment CPU Usage Failed", {
          error: errorMessage,
        });
        dispatch({
          type: ERROR_MESSAGE,
          message: errorMessage,
          loading: false,
        });
        if (errCallback) errCallback(errorMessage);
      });
  };
}