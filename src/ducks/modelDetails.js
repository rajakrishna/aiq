import {
  getModel,
  getAllModelHealth,
  saveModelNotes,
  getModelHealth,
  downloadModel,
  saveModelReq
} from '../api';
import {
  MODELS_DETAILS,
  MODELS_HEALTH_DETAILS,
  MODELS_FILE,
  ERROR_MESSAGE
} from './actionType';
import FileDownload from "js-file-download";

const defaultState = {
  modelDetails: {},
  modelHealthDetails: {},
  modelFile: '',
  message: ''
}

// Reducers
export default function reducer(state = defaultState, action={}) {
  switch (action.type) {
    case MODELS_DETAILS:
      return {...state, modelDetails: action.payload };
    case MODELS_FILE:
      return {...state, modelFile: action.payload};
    case MODELS_HEALTH_DETAILS:
      return {...state, modelHealthDetails: action.payload };
    case ERROR_MESSAGE:
      return { ...state, message: action.message};
    default:
      return state;
  }
}

// Action creators

export function getModelDetails(modelId) {
  return (dispatch, getStore) => {
    getModel(getStore().auth.token, modelId)
    .then((response) => {
      const data = response.data;
      if(response.status) {
        dispatch({
          type: MODELS_DETAILS,
          payload: data
        });
      } else {
        dispatch({
          type: ERROR_MESSAGE,
          message: 'Model Details not found'
        });
      }
    })
    .catch((error) => {
      dispatch({
        type: ERROR_MESSAGE,
        message: error.message
      })
    })
  }
}

export function saveNotes(modelId, noteData, callback, errCallback) {
  return (dispatch, getStore) => {
    saveModelNotes(getStore().auth.token, modelId, noteData)
    .then((response) => {
      const data = response.data;
      if(response.status) {
        dispatch({
          type: MODELS_DETAILS,
          payload: data
        });
      } else {
        dispatch({
          type: ERROR_MESSAGE,
          message: 'Notes are not updated!'
        });
      }
      callback && callback();
    })
    .catch((error) => {
      dispatch({
        type: ERROR_MESSAGE,
        message: error.message
      })
      errCallback && errCallback();
    })
  }
}

export function saveRequirements(modelId, reqData, callback, errCallback) {
  return (dispatch, getStore) => {
    saveModelReq(getStore().auth.token, modelId, reqData)
    .then((response) => {
      const data = response.data;
      if(response.status) {
        dispatch({
          type: MODELS_DETAILS,
          payload: data
        });
      } else {
        dispatch({
          type: ERROR_MESSAGE,
          message: 'Requirements are not updated!'
        });
      }
      callback && callback();
    })
    .catch((error) => {
      dispatch({
        type: ERROR_MESSAGE,
        message: error.message
      })
      errCallback && errCallback();
    })
  }
}

export function downloadModelFile(modelId, progress, cancellationToken) {
  return (dispatch, getStore) => {
    downloadModel(getStore().auth.token, modelId, progress, cancellationToken)
    .then((response) => {
      const data = response.data;
      FileDownload(data, modelId);
      if(response.status) {
        dispatch({
          type: MODELS_FILE,
          payload: data
        });
      } else {
        dispatch({
          type: ERROR_MESSAGE,
          message: 'Invalid Link'
        });
      }
    })
    .catch((error) => {
      dispatch({
        type: ERROR_MESSAGE,
        message: error.message
      })
      progress(error.message);
    })
  }
}

export function getModelHealthDetails(model_id) {
  return (dispatch, getStore) => {
    getModelHealth(getStore().auth.token, { modelIdEquals: model_id})
    .then((response) => {
      const data = response.data;
      if(response.status) {
        dispatch({
          type: MODELS_HEALTH_DETAILS,
          payload: data
        });
      } else {
        dispatch({
          type: ERROR_MESSAGE,
          message: 'Model Health Details not found'
        });
      }
    })
    .catch((error) => {
      dispatch({
        type: ERROR_MESSAGE,
        message: error.message
      })
    })
  }
}


export function getAllModelHealthDetails(data) {
  return (dispatch, getStore) => {
    getAllModelHealth(getStore().auth.token, data)
    .then((response) => {
      const data = response.data;
      if(response.status) {
        dispatch({
          type: MODELS_HEALTH_DETAILS,
          payload: data
        });
      } else {
        dispatch({
          type: ERROR_MESSAGE,
          message: 'Model Health Details not found'
        });
      }
    })
    .catch((error) => {
      dispatch({
        type: ERROR_MESSAGE,
        message: error.message
      })
    })
  }
}
