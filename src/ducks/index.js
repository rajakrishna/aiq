/**
 * central place to combine all reducers
 */

import { combineReducers } from "redux";
import AuthReducer from "./auth";
import ProjectsReducer from "./projects";
import UsersReducer from "./users";
import ModelsReducer from "./models";
import DeploymentsReducer from "./deployments";
import ModelDetailsReducer from "./modelDetails";
import jobDetailsReducer from "./jobDetails";
import jobRunDetailsReducer from "./jobRunDetails";
import jobsReducer from "./jobs";
import jobStore from "./jobStore";
import alertsReducer from "./alertsReducer";
import ProjectDetailsReducer from "./projectDetails";
import ExperimentReducer from "./experiments";
import NotificationReducer from "./notifications";
import CommonReducer from "./common";

// import and add more reducers as we go on to the combine reducers to make them
// available in the view
const rootReducer = combineReducers({
  auth: AuthReducer,
  projects: ProjectsReducer,
  users: UsersReducer,
  models: ModelsReducer,
  deployments: DeploymentsReducer,
  modelDetails: ModelDetailsReducer,
  jobDetails: jobDetailsReducer,
  jobRunDetails: jobRunDetailsReducer,
  jobs: jobsReducer,
  jobStore: jobStore,
  alerts: alertsReducer,
  experiments: ExperimentReducer,
  projectDetails: ProjectDetailsReducer,
  notification: NotificationReducer,
  common: CommonReducer
});

export default rootReducer;
