import axios from "./base";

export function getAllArtifacts(access_token) {
  return axios.request({
    url: "/api/artifacts",
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`
    }
  });
}

export function downloadArtifact(access_token, artifact, filename) {
  return axios.request({
    url: `/api/artifacts/download/${artifact}/${filename}`,
    responseType: 'blob',
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`
    }
  })
}