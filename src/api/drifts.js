import axios from "./base";


export function getAllDrifts(access_token, data) {
  return axios.request({
    url: `/api/drifts`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`
    },
    params: {
      "feature.equals": data.featureEquals || null,
      "feature.contains": data.featureContains || null,
      "feature.in": data.featureIn || null,
      "feature.specified": data.featureSpecified || null,
      "id.contains": data.idContains || null,
      "id.equals": data.idEquals || null,
      "id.in": data.idIn || null,
      "id.specified": data.idSpecified || null,
      "model_id.contains": data.modelIdContains || null,
      "model_id.equals": data.modelIdEquals || null,
      "model_id.in": data.modelIdIn || null,
      "model_id.specified": data.modelIdSpecified || null,
      "model_name.contains": data.modelNameContains || null,
      "model_name.equals": data.modelNameEquals || null,
      "model_name.in": data.modelNameIn || null,
      "model_name.specified": data.modelNameSpecified || null,
      "project.contains": data.projectContains || null,
      "project.equals": data.projectEquals || null,
      "project.in": data.projectIn || null,
      "project.specified": data.projectSpecified || null,
      "sort.sorted": data.sortSorted || null,
      "sort.unsorted": data.sortUnsorted || null,
      "timestamp.equals": data.timestampEquals || null,
      "timestamp.greaterOrEqualThan": data.timestampGreaterOrEqualThan || null,
      "timestamp.greaterThan": data.timestampGreaterThan || null,
      "timestamp.in": data.timestampIn || null,
      "timestamp.lessOrEqualThan": data.timestampLessOrEqualThan || null,
      "timestamp.lessThan": data.timestampLessThan || null,
      "timestamp.specified": data.timestampSpecified || null,
      "offset": data.offset || null,
      "pageNumber": data.pageNumber || null,
      "pageSize": data.pageSize || null,
      "paged": data.paged || null,
      "unpaged": data.unpaged || null,
          },
  })
}