import axios from './base';

export function getAllExperiments(access_token) {
  return axios.request({
    url: '/modelmanager/api/experiments',
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${access_token}`
    }
  });
}

export function createExperiment(access_token, data) {
  return axios.request({
    url: '/modelmanager/api/experiments',
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${access_token}`,
      'Content-Type': 'application/json'
    },
    data: data,
  });
}
