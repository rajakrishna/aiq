import axios, { HOST_URL } from './base';
import SockJS from 'sockjs-client'
import Stomp from 'stompjs'

export function listenEvents(userName, callback) {
    var socket = new SockJS(HOST_URL + '/websocket');
    var stompClient = Stomp.over(socket);
    stompClient.debug = process.env.REACT_APP_WS_DEBUG ? stompClient.debug : null;
    //testing
    stompClient.connect({}, function (frame) {
      stompClient.subscribe('/user/' + userName + '/events', callback);
    });
}