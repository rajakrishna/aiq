import axios from "./base";

export function getAllProjectsNames(access_token) {
  return axios.request({
    url: "/api/projects",
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`
    }
  });
}

export function undeployModel(access_token, id) {
  return axios.request({
    url: `/api/models/${id}/undeploy`,
    method: "POST",
    headers: {
      Authorization: `Bearer ${access_token}`
    }
  })
}

export function downloadModel(access_token, id, progress, downloadCancelToken) {
  return axios.request({
    url: `/api/models/${id}/download`,
    method: "GET",
    responseType: 'blob',
    headers: {
      Authorization: `Bearer ${access_token}`
    },
    onDownloadProgress: progress,
    cancelToken: downloadCancelToken.token
  })
}

export function getAllRegisteredModels(access_token, data) {
  return axios.request({
    url: `/api/registered-models`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`
    },
    params: {
      sort: data.sort || null,
      size: data.size || null,
     "created_by.contains": data.createdByContains || null,
      "created_by.equals": data.createdByEquals || null,
      "created_by.in": data.createdByIn || null,
      "created_by.specified": data.createdBySpecified || null,
      "created_date.equals": data.createdDateEquals || null,
      "created_date.greaterOrEqualThan": data.created_date || null,
      "created_date.greaterThan": data.createdDateGreaterThan || null,
      "created_date.in": data.createdDateIn || null,
      "created_date.lessOrEqualThan": data.createdDateLessOrEqualThan || null,
      "created_date.lessThan": data.createdDateLessThan || null,
      "created_date.specified": data.createdDateSpecified || null,
      "id.contains": data.idContains || null,
      "id.equals": data.idEquals || null,
      "id.in": data.idIn || null,
      "id.specified": data.idSpecified || null,
      "name.contains": data.nameContains || null,
      "name.equals": data.nameEquals || null,
      "name.in": data.nameIn || null,
      "name.specified": data.nameSpecified || null,
      offset: data.offset || null,
      pageNumber: data.pageNumber || null,
      pageSize: data.pageSize || null,
      paged: data.paged || null,
      page: data.page || null,
      "project_name.contains": data.projectNameContains || null,
      "project_name.equals": data.projectNameEquals || null,
      "project_name.in": data.projectNameIn || null,
      "project_id.contains": data.projectIDContains || null,
      "project_id.equals": data.projectID || null,
      "project_name.specified": data.projectNameSpecified || null,
      "sort.sorted": data.sortSorted || null,
      "sort.unsorted": data.sortUnsorted || null,
      unpaged: data.unpaged || null
    }
  });
}

export function createRegisteredModels(access_token, data) {
  return axios.request({
    url: `/api/registered-models`,
    method: "POST",
    headers: {
      Authorization: `Bearer ${access_token}`
    },
    data
  });
}

export function getAllRegisteredModelsDetail(access_token, data) {
  return axios.request({
    url: `/api/registered-model-versions/${data.id}`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`
    }
  });
}

export function getAllRegisteredModelVersion(access_token, data) {
  return axios.request({
    url: `/api/registered-model-versions`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`
    },
    params: {
      size: data.size || null,
      "registered_model_id.equals": data.registered_model_id || null,
      "type.equals": data.type || null,
      "project_name.equals": data.projectNameContains || null,
      "ml_algorithm.equals": (data.ml_algorithm && data.ml_algorithm.value) || null,
      "ml_library.contains": data.mlLibraryContains || null
    }
  });
}

export function deleteRegisteredModel(access_token, id) {
  return axios.request({
    url: `/api/registered-models/${id}`,
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${access_token}`
    },
  });
}

export function downloadModelFileById(access_token, id) {
  return axios.request({
    url: `/api/model-files/${id}/download`,
    method: "GET",
    responseType: 'blob',
    headers: {
      Authorization: `Bearer ${access_token}`
    }
  })
}

export function deployRegisteredModel(access_token, data) {
  return axios.request({
    url: `/api/mlworkflow-runs`,
    method: "POST",
    headers: {
      Authorization: `Bearer ${access_token}`
    },
    data
  })
}

export function deleteRegisteredModelVersion(access_token, id) {
  return axios.request({
    url: `/api/registered-model-versions/${id}`,
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${access_token}`
    },
  });
}

export function getAllModels(access_token, data) {
  return axios.request({
    url: `/api/models`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`
    },
    params: {
      sort: data.sort || null,
      size: data.size || null,
      "failing.equals": data.failing || null,
      "created_by.contains": data.createdByContains || null,
      "created_by.equals": data.createdByEquals || null,
      "created_by.in": data.createdByIn || null,
      "created_by.specified": data.createdBySpecified || null,
      "created_date.equals": data.createdDateEquals || null,
      "created_date.greaterOrEqualThan": data.created_date || null,
      "created_date.greaterThan": data.createdDateGreaterThan || null,
      "created_date.in": data.createdDateIn || null,
      "created_date.lessOrEqualThan": data.createdDateLessOrEqualThan || null,
      "created_date.lessThan": data.createdDateLessThan || null,
      "created_date.specified": data.createdDateSpecified || null,
      "id.contains": data.idContains || null,
      "id.equals": data.idEquals || null,
      "id.in": data.idIn || null,
      "id.specified": data.idSpecified || null,
      "ml_algorithm.contains": data.mlAlgorithmcontains || null,
      "ml_algorithm.equals": data.mlAlgorithmequals || null,
      "ml_algorithm.in": data.mlAlgorithmin || null,
      "ml_algorithm.specified": data.mlAlgorithmSpecified || null,
      "ml_library.contains": data.mlLibraryContains || null,
      "ml_library.equals": data.mlLibraryEquals || null,
      "ml_library.in": data.mlLibraryIn || null,
      "ml_library.specified": data.mlLibrarySpecified || null,
      "name.contains": data.nameContains || null,
      "name.equals": data.nameEquals || null,
      "name.in": data.nameIn || null,
      "name.specified": data.nameSpecified || null,
      offset: data.offset || null,
      "owner.contains": data.ownerContains || null,
      "owner.equals": data.ownerEquals || null,
      "owner.in": data.ownerIn || null,
      "owner.specified": data.ownerSpecified || null,
      pageNumber: data.pageNumber || null,
      pageSize: data.pageSize || null,
      paged: data.paged || null,
      page: data.page || null,
      "project_name.contains": data.projectNameContains || null,
      "project_name.equals": data.projectNameEquals || null,
      "project_name.in": data.projectNameIn || null,
      "project_id.contains": data.projectIDContains || null,
      "project_id.equals": data.projectIDEquals || null,
      "project_name.specified": data.projectNameSpecified || null,
      "sort.sorted": data.sortSorted || null,
      "sort.unsorted": data.sortUnsorted || null,
      "status.contains": data.statusContains || null,
      "status.equals": data.statusEquals || null,
      "status.in": data.statusIn || null,
      "status.specified": data.statusSpecified || null,
      "type.contains": data.typecontains || null,
      "type.equals": data.typeequals || null,
      "type.in": data.typein || null,
      "type.specified": data.typeSpecified || null,
      "data_drift.equals":data.dataDriftequals,
      "data_drift.in":data.dataDriftin,
      "data_drift.lessThan":data.dataDriftlessThan,
      "data_drift.lessOrEqualThan":data.dataDriftlessOrEqualThan,
      "data_drift.greaterThan":data.dataDriftgreaterThan,
      "data_drift.greaterOrEqualThan":data.dataDriftgreaterOrEqualThan,
      "health_score.equals":data.healthScoreequals,
      "health_score.in":data.healthScorein,
      "health_score.lessThan":data.healthScorelessThan,
      "health_score.lessOrEqualThan":data.healthScorelessOrEqualThan,
      "health_score.greaterThan":data.healthScoregreaterThan,
      "health_score.greaterOrEqualThan":data.healthScoregreaterOrEqualThan,
      unpaged: data.unpaged || null
    }
  });
}

export function getAllMLFlow(access_token, data) {
  return axios.request({
    url: `/api/mlflow/experiments`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`
    },
    params: {
      host: data.hostname || null
    }
  });
}

export function getMLFlow(access_token, data) {
  return axios.request({
    url: `/api/mlflow/experiments/${data.id}`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`
    },
    params: {
      host: data.hostname || null
    }
  });
}

export function getMLFlowRunList(access_token, data) {
  return axios.request({
    url: `/api/mlflow/runs`,
    method: "POST",
    headers: {
      Authorization: `Bearer ${access_token}`
    },
    params: {
      host: data.hostname || null
    },
    data: {
        "experiment_ids": [
          data.id
        ],
        "run_view_type": "ACTIVE_ONLY"
      }
  });
}

export function getAllModelsByProject(access_token, project_name) {
  return axios.request({
    // url: `/modelmanager/api/projects/${project_name}`,
    url: `/api/models`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`
    },
    params: {
      "project_name.equals": project_name
    }
  });
}

export function getModelHealth(access_token, data) {
  return axios.request({
    url: `/api/model-health`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`
    },
    params: {
      "health_score.equals": data.healthScoreEquals || null,
      "health_score.greaterOrEqualThan":
        data.healthScoreGreaterOrEqualThan || null,
      "health_score.greaterThan": data.healthScoreGreaterThan || null,
      "health_score.in": data.healthScoreIn || null,
      "health_score.lessOrEqualThan": data.healthScoreLessOrEqualThan || null,
      "health_score.lessThan": data.healthScoreLessThan || null,
      "health_score.specified": data.healthScoreSpecified || null,
      "id.contains": data.idContains || null,
      "id.equals": data.idEquals || null,
      "id.in": data.idIn || null,
      "id.specified": data.idSpecified || null,
      "model_id.contains": data.modelIdContains || null,
      "model_id.equals": data.modelIdEquals || null,
      "model_id.in": data.modelIdIn || null,
      "model_id.specified": data.modelIdSpecified || null,
      "model_name.contains": data.modelNameContains || null,
      "model_name.equals": data.modelNameEquals || null,
      "model_name.in": data.modelNameIn || null,
      "model_name.specified": data.modelNameSpecified || null,
      "performance_loss.equals": data.performanceLossEquals || null,
      "performance_loss.greaterOrEqualThan":
        data.performanceLossGreaterOrEqualThan || null,
      "performance_loss.greaterThan": data.performanceLossGreaterThan || null,
      "performance_loss.in": data.performanceLossIn || null,
      "performance_loss.lessOrEqualThan":
        data.performanceLossLessOrEqualThan || null,
      "performance_loss.lessThan": data.performanceLossLessThan || null,
      "performance_loss.specified": data.performanceLossSpecified || null,
      "points_lost.equals": data.pointsLostEquals || null,
      "points_lost.greaterOrEqualThan":
        data.pointsLostGreaterOrEqualThan || null,
      "points_lost.greaterThan": data.pointsLostGreaterThan || null,
      "points_lost.in": data.pointsLostIn || null,
      "points_lost.lessOrEqualThan": data.pointsLostLessOrEqualThan || null,
      "points_lost.lessThan": data.pointsLostLessThan || null,
      "points_lost.specified": data.pointsLostSpecified || null,
      "project_name.contains": data.projectNameContains || null,
      "project_name.equals": data.projectNameEquals || null,
      "project_name.in": data.projectNameIn || null,
      "project_name.specified": data.projectNameSpecified || null,
      "sort.sorted": data.sortSorted || null,
      "sort.unsorted": data.sortUnsorted || null,
      "timestamp.equals": data.timestampEquals || null,
      "timestamp.greaterOrEqualThan": data.timestampGreaterOrEqualThan || null,
      "timestamp.greaterThan": data.timestampGreaterThan || null,
      "timestamp.in": data.timestampIn || null,
      "timestamp.lessOrEqualThan": data.timestampLessOrEqualThan || null,
      "timestamp.lessThan": data.timestampLessThan || null,
      "timestamp.specified": data.timestampSpecified || null,
      offset: data.offset || null,
      pageNumber: data.pageNumber || null,
      pageSize: data.pageSize || null,
      paged: data.paged || null,
      unpaged: data.unpaged || null
    }
  });
}

export function getAllPredictions(access_token, data) {
  return axios.request({
    url: `/api/predictions`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`
    },
    params: {
      sort: data.sort || null,
      "id.contains": data.idContains || null,
      "id.equals": data.idEquals || null,
      "id.in": data.idIn || null,
      "id.specified": data.idSpecified || null,
      "model_id.contains": data.modelIdContains || null,
      "model_id.equals": data.modelIdEquals || null,
      "model_id.in": data.modelIdIn || null,
      "model_id.specified": data.modelIdSpecified || null,
      "model_name.contains": data.modelNameContains || null,
      "model_name.equals": data.modelNameEquals || null,
      "model_name.in": data.modelNameIn || null,
      "model_name.specified": data.modelNameSpecified || null,
      "prediction_date.equals": data.predictionDateEquals || null,
      "prediction_date.greaterOrEqualThan":
        data.predictionDateGreaterOrEqualThan || null,
      "prediction_date.greaterThan": data.predictionDateGreaterThan || null,
      "prediction_date.in": data.predictionDateIn || null,
      "prediction_date.lessOrEqualThan":
        data.predictionDateLessOrEqualThan || null,
      "prediction_date.lessThan": data.predictionDateLessThan || null,
      "prediction_date.specified": data.predictionDateSpecified || null,
      "project_name.contains": data.projectNameContains || null,
      "project_name.equals": data.projectNameEquals || null,
      "project_name.in": data.projectNameIn || null,
      "project_name.specified": data.projectNameSpecified || null,
      "sort.sorted": data.sortSorted || null,
      "sort.unsorted": data.sortUnsorted || null,
      offset: data.offset || null,
      pageNumber: data.pageNumber || null,
      pageSize: data.pageSize || null,
      paged: data.paged || null,
      unpaged: data.unpaged || null
    }
  });
}

export function getModel(access_token, model_id) {
  return axios.request({
    url: `/api/models/${model_id}`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`
    }
  });
}

export function saveModelNotes(access_token, model_id, notesData) {
  return axios.request({
    url: `/api/models/${model_id}/notes`,
    method: "POST",
    headers: {
      Authorization: `Bearer ${access_token}`
    },
    data: {
      notes: notesData
    }
  });
}

export function saveModelReq(access_token, model_id, reqData) {
  return axios.request({
    url: `/api/models/${model_id}/requirements`,
    method: "POST",
    headers: {
      Authorization: `Bearer ${access_token}`
    },
    data: {
      requirements: reqData
    }
  });
}

export function getPredictionCount(access_token, data) {
  return axios.request({
    url: `/api/predictions/aggs/count`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`
    },
    params: {
      'field': data.field || null,
      'interval': data.interval || null,
      'model_id.equals': data.modelIdEquals || null,
      'prediction_date.greaterOrEqualThan': data.greaterOrEqualThan || null,
    }
  });
}

export function deleteModel(access_token, model_id) {
  return axios.request({
    url: `/api/models/${model_id}`,
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${access_token}`
    }
  });
}

export function getRegisteredModelCounts(access_token, data) {
  return axios.request({
    url: `/api/registered-models/aggs/count`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`
    },
    params: data || null
  });
}

export function getModelCounts(access_token, data) {
  return axios.request({
    url: `/api/models/aggs/count`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`
    },
    params: {
      field: data.field || null,
      "created_by.contains": data.createdByContains || null,
      "created_by.equals": data.createdByEquals || null,
      "created_by.in": data.createdByIn || null,
      "created_by.specified": data.createdBySpecified || null,
      "created_date.equals": data.createdDateEquals || null,
      "created_date.greaterOrEqualThan":
        data.createdDateGreaterOrEqualThan || null,
      "created_date.greaterThan": data.createdDateGreaterThan || null,
      "created_date.in": data.createdDateIn || null,
      "created_date.lessOrEqualThan": data.createdDateLessOrEqualThan || null,
      "created_date.lessThan": data.createdDateLessThan || null,
      "created_date.specified": data.createdDateSpecified || null,
      "id.contains": data.idContains || null,
      "id.equals": data.idEquals || null,
      "id.in": data.idIn || null,
      "id.specified": data.idSpecified || null,
      "ml_algorithm.contains": data.mlAlgorithmContains || null,
      "ml_algorithm.equals": data.mlAlgorithmEquals || null,
      "ml_algorithm.in": data.mlAlgorithmIn || null,
      "ml_algorithm.specified": data.mlAlgorithmSpecified || null,
      "ml_library.contains": data.mlLibraryContains || null,
      "ml_library.equals": data.mlLibraryEquals || null,
      "ml_library.in": data.mlLibraryIn || null,
      "ml_library.specified": data.mlLibrarySpecified || null,
      "name.contains": data.nameContains || null,
      "name.equals": data.nameEquals || null,
      "name.in": data.nameIn || null,
      "name.specified": data.nameSpecified || null,
      offset: data.offset || null,
      "owner.contains": data.ownerContains || null,
      "owner.equals": data.ownerEquals || null,
      "owner.in": data.ownerIn || null,
      "owner.specified": data.ownerSpecified || null,
      pageNumber: data.pageNumber || null,
      pageSize: data.pageSize || null,
      paged: data.paged || null,
      "project_name.contains": data.projectNameContains || null,
      "project_name.equals": data.projectNameEquals || null,
      "project_name.in": data.projectNameIn || null,
      "project_name.specified": data.projectNameSpecified || null,
      "sort.sorted": data.sortSorted || null,
      "sort.unsorted": data.sortUnsorted || null,
      "status.contains": data.statusContains || null,
      "status.equals": data.statusEquals || null,
      "status.in": data.statusIn || null,
      "status.specified": data.statusSpecified || null,
      "type.contains": data.typeContains || null,
      "type.equals": data.typeEquals || null,
      "type.in": data.typeIn || null,
      "type.specified": data.typeSpecified || null,
      unpaged: data.unpaged || null
    }
  });
}

export function getDataDriftsTrend(access_token, data) {
  return axios.request({
    url: `/api/models/${data.id}/data-drifts/trend`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`
    },
    params: {
      "timestamp.greaterOrEqualThan": data.timestampGreaterOrEqualThan || null,
      in: data.in || null,
      equals: data.equals || null,
      greaterOrEqualThan: data.greaterOrEqualThan || null,
      greaterThan: data.greaterThan || null,
      lessOrEqualThan: data.lessOrEqualThan || null,
      lessThan: data.lessThan || null,
      specified: data.specified || null
    }
  });
}

export function getTnRSignificance(access_token,ID){
  return axios.request({
    url: `/api/drifts/runtime-metrics/${ID}`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`
    }
  });
}

export function createModel(access_token, data) {
  return axios.request({
    url: `/api/models`,
    method: "POST",
    headers: {
      Authorization: `Bearer ${access_token}`
    },
    data: data
  })
}

export function uploadFile(access_token, data) {
  const formData = new FormData();
  formData.append('file', data.file);
  formData.append('model_file', data.model_file);
  return axios.request({
    url: `/api/model-files/upload`,
    method: "POST",
    headers: {
      Authorization: `Bearer ${access_token}`,
      'Content-Type': 'multipart/form-data',
    },
    data: formData
  })
}
