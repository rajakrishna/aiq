import axios from './base';

//=================================
//   Tenant Management
//================================

export function getTenant(access_token, id) {
  return axios.request({
    baseURL: process.env.REACT_APP_TENANT_URL,
    url: `/tenant/${id}`,
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${access_token}`
    },
  })
}

export function updateTenant(access_token, data) {
  return axios.request({
    baseURL: process.env.REACT_APP_TENANT_URL,
    url: '/tenant',
    method: 'PUT',
    headers: {
      'Authorization': `Bearer ${access_token}`
    },
    data: data
  })
}

export function deleteTenant(access_token, id) {
  return axios.request({
    baseURL: process.env.REACT_APP_TENANT_URL,
    url: `/tenant/${id}`,
    method: 'DELETE',
    headers: {
      'Authorization': `Bearer ${access_token}`
    },
  })
}
