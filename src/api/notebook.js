import axios from './base';

export function getNotebookList(access_token, projectId) {
  return axios.request({
    baseURL: process.env.REACT_APP_JUPYTER_URL,
    url: `api/projects/${projectId}/notebooks`,
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${access_token}`
    }
  });
}

export function updateNotebook(access_token,projectId,name,data){
  return axios.request({
    baseURL: process.env.REACT_APP_JUPYTER_URL,
    url:`/api/projects/${projectId}/notebooks/${name}`,
    method:'PATCH',
    headers:{
      'Authorization': `Bearer ${access_token}`,
      "Content-Type": "application/json"
    },
    data:data
  })
}

export function deleteNotebook(access_token, name){
  return axios.request({
    baseURL: process.env.REACT_APP_JUPYTER_URL,
    url: `/api/namespaces/kai/notebooks/${name}`,
    method: "DELETE",
    headers: {
      'Authorization': `Bearer ${access_token}`
    }
  })
}

export function spawnNotebook(access_token, data) {
  return axios.request({
    baseURL: process.env.REACT_APP_JUPYTER_URL,
    url: `/api/namespaces/kai/notebooks`,
    method: "POST",
    headers: {
      'Authorization': `Bearer ${access_token}`,
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    data: data
  })
}

export function getNotebooksUnusedVolumes(access_token, projectId){
  return axios.request({
    baseURL: process.env.REACT_APP_JUPYTER_URL,
    url: `/api/projects/${projectId}/volumes?unused=true`,
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${access_token}`
    }
  });
}

export function getToken(access_token, name){
  return axios.request({
    url: `/api/notebooks/${name}/token`,
    method: "POST",
    headers: {
      'Authorization': `Bearer ${access_token}`
    }
  })
}

export function getCpuUsageByNotebook(access_token, noteBookName, start, end, step){
  return axios.request({
    url: `/api/notebooks/${noteBookName}/metrics/cpu?start=${start}&end=${end}&step=${step}`,
    method: `GET`,
    headers: {  
      'Authorization': `Bearer ${access_token}`
    }
  })
}

export function getMemoryUsageByNotebook(access_token, noteBookName, start, end, step){
  return axios.request({
    url: `/api/notebooks/${noteBookName}/metrics/memory?start=${start}&end=${end}&step=${step}`,
    method: `GET`,
    headers: {
      'Authorization': `Bearer ${access_token}`
    }
  })
}