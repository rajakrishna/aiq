import axios, { HOST_URL } from './base';
import SockJS from 'sockjs-client'
import Stomp from 'stompjs'

export function getAllNotifications(access_token, data) {
  return axios.request({
    url: '/api/notifications',
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${access_token}`
    },
    params: {
      "created_date.equals": data.createdDateEquals || null,
      "created_date.greaterOrEqualThan": data.createdDateGreaterOrEqualThan || null,
      "created_date.greaterThan": data.createdDateGreaterThan || null,
      "created_date.in": data.createdDateIn || null,
      "created_date.lessOrEqualThan": data.createdDateLessOrEqualThan || null,
      "created_date.lessThan": data.createdDateLessThan || null,
      "created_date.specified": data.createdDateSpecified || null,
      "entity_id.equals": data.entityId || null,
      "read.equals": data.read,
      "read.in": data.readIn || null,
      "read.specified": data.readSpecified || null,
      "sort": "created_date,desc" || null,
      "sort.unsorted": data.sortUnsorted || null,
      "subject.contains": data.subjectContains || null,
      "subject.equals": data.subjectEquals || null,
      "subject.in": data.subjectIn || null,
      "subject.specified": data.subjectSpecified || null,
      "type.equals": data.type || null,
      "offset": data.offset || null,
      "page": data.pageNumber || null,
      "pageSize": data.pageSize || null,
      "paged": data.paged || null,
      "unpaged": data.unpaged || null,
      "size": data.size || null,
    }
  });
}

export function getNotificationCounts(access_token, data) {
  return axios.request({
    url: '/api/notifications/aggs/count',
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${access_token}`
    },
    params: {
      "created_date.equals": data.createdDateEquals || null,
      "created_date.greaterOrEqualThan": data.createdDateGreaterOrEqualThan || null,
      "created_date.greaterThan": data.createdDateGreaterThan || null,
      "created_date.in": data.createdDateIn || null,
      "created_date.lessOrEqualThan": data.createdDateLessOrEqualThan || null,
      "created_date.lessThan": data.createdDateLessThan || null,
      "created_date.specified": data.createdDateSpecified || null,
      "read.equals": data.read,
      "read.in": data.readIn || null,
      "read.specified": data.readSpecified || null,
      "sort.sorted": data.sortSorted || null,
      "sort.unsorted": data.sortUnsorted || null,
      "subject.contains": data.subjectContains || null,
      "subject.equals": data.subjectEquals || null,
      "subject.in": data.subjectIn || null,
      "subject.specified": data.subjectSpecified || null,
      "offset": data.offset || null,
      "pageNumber": data.pageNumber || null,
      "pageSize": data.pageSize || null,
      "paged": data.paged || null,
      "unpaged": data.unpaged || null,
    }
  });
}


export function listenNotifications(userName, cb) {
  var socket = new SockJS(HOST_URL + '/websocket');
  var stompClient = Stomp.over(socket);
  stompClient.debug = process.env.REACT_APP_WS_DEBUG ? stompClient.debug : null;
  stompClient.connect({}, function (frame) {
    stompClient.subscribe('/user/' + userName + '/notifications', cb);
  });
}

export function updateNotificationAsRead(access_token, notificationId) {
  return axios.request({
    url: `/api/notifications/read`,
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${access_token}`
    },
    data: notificationId
  })
}
