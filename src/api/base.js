import axios from "axios";
// NOTE: this should be derived from the env based on the environment
export const HOST_URL = process.env.REACT_APP_API_URL;
export const cancellationToken = axios.CancelToken;
// configure an axios instance
const instance = axios.create({
  baseURL: HOST_URL,
  headers: {
    "Content-Type": "application/json"
  }
});

//Auto Logout on Unauthoried(401) response
instance.interceptors.response.use(
  function(response) {
    return response;
  },
  function(error) {
    if (error.response && error.response.status === 401) {
      localStorage.clear();
      window.location.reload(true);
    }
    return Promise.reject(error);
  }
);

export default instance;
