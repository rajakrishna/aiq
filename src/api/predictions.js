import axios from "./base";

export function getAllPredictions(access_token, data) {
  return axios.request({
    url: `/api/predictions`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`
    },
    params: {
      "id.contains": data.idContains || null,
      "id.equals": data.idEquals || null,
      "id.in": data.idIn || null,
      "id.specified": data.idSpecified || null,
      "model_id.contains": data.modelIdContains || null,
      "model_id.equals": data.modelIdEquals || null,
      "model_id.in": data.modelIdIn || null,
      "model_id.specified": data.modelIdSpecified || null,
      "model_name.contains": data.modelNameContains || null,
      "model_name.equals": data.modelNameEquals || null,
      "model_name.in": data.modelNameIn || null,
      "model_name.specified": data.modelNameSpecified || null,
      "prediction_date.equals": data.predictionDateEquals || null,
      "prediction_date.greaterOrEqualThan": data.predictionDateGreaterOrEqualThan || null,
      "prediction_date.greaterThan": data.prediction_date.greaterThan || null,
      "prediction_date.in": data.predictionDateIn || null,
      "prediction_date.lessOrEqualThan": data.predictionDateLessOrEqualThan || null,
      "prediction_date.lessThan": data.predictionDateLessThan || null,
      "prediction_date.specified": data.predictionDateSpecified || null,
      "project_name.contains": data.projectNameContains || null,
      "project_name.equals": data.projectNameEquals || null,
      "project_name.in": data.projectNameIn || null,
      "project_name.specified": data.projectNameSpecified || null,
      "sort.sorted": data.sort.sorted || null,
      "sort.unsorted": data.sortUnsorted || null,
      "offset": data.offset || null,
      "pageNumber": data.pageNumber || null,
      "pageSize": data.pageSize || null,
      "paged": data.paged || null,
      "unpaged": data.unpaged || null,
    },
  })
}


export function getAllFilteredPredictions(access_token, data) {
  return axios.request({
    url: `/api/predictions?${data}`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`
    }
  })
}
