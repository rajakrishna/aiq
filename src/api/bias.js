import axios from './base';

//=================================
//   Bias 
//================================

export function getBiasByModelId(access_token, id) {
  return axios.request({
    url: `/api/bias-config?model_id.equals=${id}`,
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${access_token}`
    },
  })
}

export function updateBias(access_token, data) {
  return axios.request({
    url: '/api/bias-config',
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${access_token}`
    },
    data: data
  })
}