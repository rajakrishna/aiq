import axios from './base';

export function getAllSecrets(access_token, data = {}) {
  return axios.request({
    url: `/api/secrets`,
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${access_token}`
    },
    params: {
      name: data.name || null,
      project_id: data.project_id || null,
      project_name: data.project_name || null
    }
  });
}

export function createSecrets(access_token, data) {
  return axios.request({
    url: `/api/secrets`,
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${access_token}`
    },
    data: data
  });
}

export function updateSecrets(access_token, data) {
  return axios.request({
    url: `/api/secrets`,
    method: 'PUT',
    headers: {
      'Authorization': `Bearer ${access_token}`
    },
    data: data
  });
}

export function deleteSecrets(access_token, name) {
    return axios.request({
        url: `/api/secrets/${name}`,
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${access_token}`
        }
    })
}