import axios from './base';

export function getAllInsights(access_token, data) {
  return axios.request({
    url: '/api/insights',
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${access_token}`
    },
    params: {
      "id.contains": data.idContains || null,	
      "id.equals": data.idEquals || null,	
      "id.in": data.idIn || null,	
      "id.specified": data.idSpecified || null,	
      "model_id.contains": data.modelIdContains || null,	
      "model_id.equals": data.modelIdEquals || null,	
      "model_id.in": data.modelIdIn || null,	
      "model_id.specified": data.modelIdSpecified || null,	
      "model_name.contains": data.modelNameContains || null,	
      "model_name.equals": data.modelNameEquals || null,	
      "model_name.in": data.modelNameIn || null,	
      "model_name.specified": data.modelNameSpecified || null,	
      "points_category.contains": data.pointsCategoryContains || null,	
      "points_category.equals": data.pointsCategoryEquals || null,	
      "points_category.in": data.pointsCategoryIn || null,	
      "points_category.specified": data.pointsCategorySpecified || null,	
      "project_name.contains": data.projectNameContains || null,	
      "project_name.equals": data.projectNameEquals || null,	
      "project_name.in": data.projectNameIn || null,	
      "project_name.specified": data.projectNameSpecified || null,	
      "sort.sorted": data.sortSorted || null,	
      "sort.unsorted": data.sortSnsorted || null,	
      "timestamp.equals": data.timestampEquals || null,	
      "timestamp.greaterOrEqualThan": data.timestampGreaterOrEqualThan || null,	
      "timestamp.greaterThan": data.timestampGreaterThan || null,	
      "timestamp.in": data.timestampIn || null,	
      "timestamp.lessOrEqualThan": data.timestampLessOrEqualThan || null,	
      "timestamp.lessThan": data.timestampLessThan || null,	
      "timestamp.specified": data.timestampSpecified || null,	
      "pageNumber": data.pageNumber || null,	
      "pageSize": data.pageSize || null,	
      "paged": data.paged || null,	
      "offset": data.offset || null,	
      "unpaged": data.unpaged || null,
      "size": data.size || null,
      "sort": data.sort || null,
         }
  });
}

export function createInsight(access_token, data) {
  return axios.request({
    url: '/api/insights',
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${access_token}`
    },
    data: data
  })
}
