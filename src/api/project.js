import axios from "./base";

export function getAllProjectsData(access_token, data) {
  return axios.request({
    url: "/api/projects/summary",
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`,
      "Content-Type": "application/json"
    },
    params: {
      "created_by.contains": data.createdByContains || null,
      "created_by.equals": data.createdByEquals || null,
      "created_by.in": data.createdByIn || null,
      "created_by.specified": data.createdBySpecified || null,
      "created_date.equals": data.createdDateEquals || null,
      "created_date.greaterOrEqualThan":
        data.createdDateGreaterOrEqualThan || null,
      "created_date.greaterThan": data.createdDateGreaterThan || null,
      "created_date.in": data.createdDateIn || null,
      "created_date.lessOrEqualThan": data.createdDateLessOrEqualThan || null,
      "created_date.lessThan": data.createdDateLessThan || null,
      "created_date.specified": data.createdDateSpecified || null,
      "description.contains": data.descriptionContains || null,
      "description.equals": data.descriptionEquals || null,
      "description.in": data.descriptionIn || null,
      "description.specified": data.descriptionSpecified || null,
      "id.contains": data.idContains || null,
      "id.equals": data.idEquals || null,
      "id.in": data.idIn || null,
      "id.specified": data.idSpecified || null,
      "last_modified_by.contains": data.lastModifiedByContains || null,
      "last_modified_by.equals": data.lastModifiedByEquals || null,
      "last_modified_by.in": data.lastModifiedByIn || null,
      "last_modified_by.specified": data.lastModifiedBySpecified || null,
      "last_modified_date.equals": data.lastModifiedDateEquals || null,
      "last_modified_date.greaterOrEqualThan":
        data.lastModifiedDateGreaterOrEqualThan || null,
      "last_modified_date.greaterThan": data.lastModifiedDateGreaterThan || null,
      "last_modified_date.in": data.lastModifiedDateIn || null,
      "last_modified_date.lessOrEqualThan":
        data.lastModifiedDateLessOrEqualThan || null,
      "last_modified_date.lessThan": data.lastModifiedDateLessThan || null,
      "last_modified_date.specified": data.lastModifiedDateSpecified || null,
      "name.contains": data.nameContains || null,
      "name.equals": data.nameEquals || null,
      "name.in": data.nameIn || null,
      "name.specified": data.nameSpecified || null,
      offset: data.offset || null,
      "owner.contains": data.ownerContains || null,
      "owner.equals": data.ownerEquals || null,
      "owner.in": data.ownerIn || null,
      "owner.specified": data.ownerSpecified || null,
      page: data.pageNumber || null,
      pageSize: data.pageSize || null,
      paged: data.paged || null,
      "sort.sorted": data.sortSorted || null,
      "sort.unsorted": data.sortUnsorted || null,
      unpaged: data.unpaged || null,
      "users.contains": data.usersContains || null,
      "users.equals": data.usersEquals || null,
      "users.in": data.usersIn || null,
      "users.specified": data.usersSpecified || null
    }
  });
}

export function getAllProjects(access_token, data) {
  return axios.request({
    url: "/api/projects",
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`
    },
    params: {
      "created_by.contains": data.createdByContains || null,
      "created_by.equals": data.createdByEquals || null,
      "created_by.in": data.createdByIn || null,
      "created_by.specified": data.createdBySpecified || null,
      "created_date.equals": data.createdDateEquals || null,
      "created_date.greaterOrEqualThan":
        data.createdDateGreaterOrEqualThan || null,
      "created_date.greaterThan": data.createdDateGreaterThan || null,
      "created_date.in": data.createdDateIn || null,
      "created_date.lessOrEqualThan": data.createdDateLessOrEqualThan || null,
      "created_date.lessThan": data.createdDateLessThan || null,
      "created_date.specified": data.createdDateSpecified || null,
      "description.contains": data.descriptionContains || null,
      "description.equals": data.descriptionEquals || null,
      "description.in": data.descriptionIn || null,
      "description.specified": data.descriptionSpecified || null,
      "id.contains": data.idContains || null,
      "id.equals": data.idEquals || null,
      "id.in": data.idIn || null,
      "id.specified": data.idSpecified || null,
      "last_modified_by.contains": data.lastModifiedByContains || null,
      "last_modified_by.equals": data.lastModifiedByEquals || null,
      "last_modified_by.in": data.lastModifiedByIn || null,
      "last_modified_by.specified": data.lastModifiedBySpecified || null,
      "last_modified_date.equals": data.lastModifiedDateEquals || null,
      "last_modified_date.greaterOrEqualThan":
        data.lastModifiedDateGreaterOrEqualThan || null,
      "last_modified_date.greaterThan": data.lastModifiedDateGreaterThan || null,
      "last_modified_date.in": data.lastModifiedDateIn || null,
      "last_modified_date.lessOrEqualThan":
        data.lastModifiedDateLessOrEqualThan || null,
      "last_modified_date.lessThan": data.lastModifiedDateLessThan || null,
      "last_modified_date.specified": data.lastModifiedDateSpecified || null,
      "name.contains": data.nameContains || null,
      "name.equals": data.nameEquals || null,
      "name.in": data.nameIn || null,
      "name.specified": data.nameSpecified || null,
      offset: data.offset || null,
      "owner.contains": data.ownerContains || null,
      "owner.equals": data.ownerEquals || null,
      "owner.in": data.ownerIn || null,
      "owner.specified": data.ownerSpecified || null,
      pageNumber: data.pageNumber || null,
      pageSize: data.pageSize || null,
      paged: data.paged || null,
      size: data.size || null,
      "sort.sorted": data.sortSorted || null,
      "sort.unsorted": data.sortUnsorted || null,
      unpaged: data.unpaged || null,
      "users.contains": data.usersContains || null,
      "users.equals": data.usersEquals || null,
      "users.in": data.usersIn || null,
      "users.specified": data.usersSpecified || null
    }
  });
}

export function createProject(access_token, data) {
  return axios.request({
    url: "/api/projects",
    method: "POST",
    headers: {
      Authorization: `Bearer ${access_token}`,
      "Content-Type": "application/json"
    },
    data: data
  });
}

export function updateProject(access_token, data) {
  return axios.request({
    url: "/api/projects",
    method: "PUT",
    headers: {
      Authorization: `Bearer ${access_token}`,
      "Content-Type": "application/json"
    },
    data: data
  });
}

export function getProject(access_token, projectID) {
  return axios.request({
    url: `/api/projects/${projectID}`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`,
      "Content-Type": "application/json"
    }
  });
}

export function deleteProject(access_token, projectID) {
  return axios.request({
    url: `/api/projects/${projectID}`,
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${access_token}`,
      "Content-Type": "application/json"
    }
  });
}

export function getProjectCounts(access_token, data) {
  return axios.request({
    url: `/api/projects/aggs/count`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`
    },
    params: {
      field: data.field || null,
      "created_by.contains": data.createdByContains || null,
      "created_by.equals": data.createdByEquals || null,
      "created_by.in": data.createdByIn || null,
      "created_by.specified": data.createdBySpecified || null,
      "created_date.equals": data.createdDateEquals || null,
      "created_date.greaterOrEqualThan":
        data.createdDateGreaterOrEqualThan || null,
      "created_date.greaterThan": data.createdDateGreaterThan || null,
      "created_date.in": data.createdDateIn || null,
      "created_date.lessOrEqualThan": data.createdDateLessOrEqualThan || null,
      "created_date.lessThan": data.createdDateLessThan || null,
      "created_date.specified": data.createdDateSpecified || null,
      "id.contains": data.idContains || null,
      "id.equals": data.idEquals || null,
      "id.in": data.idIn || null,
      "id.specified": data.idSpecified || null,
      "ml_algorithm.contains": data.mlAlgorithmContains || null,
      "ml_algorithm.equals": data.mlAlgorithmEquals || null,
      "ml_algorithm.in": data.mlAlgorithmIn || null,
      "ml_algorithm.specified": data.mlAlgorithmSpecified || null,
      "ml_library.contains": data.mlLibraryContains || null,
      "ml_library.equals": data.mlLibraryEquals || null,
      "ml_library.in": data.mlLibraryIn || null,
      "ml_library.specified": data.mlLibrarySpecified || null,
      "name.contains": data.nameContains || null,
      "name.equals": data.nameEquals || null,
      "name.in": data.nameIn || null,
      "name.specified": data.nameSpecified || null,
      offset: data.offset || null,
      "owner.contains": data.ownerContains || null,
      "owner.equals": data.ownerEquals || null,
      "owner.in": data.ownerIn || null,
      "owner.specified": data.ownerSpecified || null,
      pageNumber: data.pageNumber || null,
      pageSize: data.pageSize || null,
      paged: data.paged || null,
      "project_name.contains": data.projectNameContains || null,
      "project_name.equals": data.projectNameEquals || null,
      "project_name.in": data.projectNameIn || null,
      "project_name.specified": data.projectNameSpecified || null,
      "sort.sorted": data.sortSorted || null,
      "sort.unsorted": data.sortUnsorted || null,
      "status.contains": data.statusContains || null,
      "status.equals": data.statusEquals || null,
      "status.in": data.statusIn || null,
      "status.specified": data.statusSpecified || null,
      "type.contains": data.typeContains || null,
      "type.equals": data.typeEquals || null,
      "type.in": data.typeIn || null,
      "type.specified": data.typeSpecified || null,
      unpaged: data.unpaged || null
    }
  });
}
