import axios from './base';

export function getAllUsers(access_token) {
  return axios.request({
    baseURL: process.env.REACT_APP_USER_URL,
    url: '/users',
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${access_token}`
    }
  });
}

export function addUser(access_token, data) {
  return axios.request({
    baseURL: process.env.REACT_APP_USER_URL,
    url: '/user',
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${access_token}`
    },
    data: data,
  });
}

export function updateUser(access_token, data) {
  return axios.request({
    baseURL: process.env.REACT_APP_USER_URL,
    url: '/user',
    method: 'PUT',
    headers: {
      'Authorization': `Bearer ${access_token}`
    },
    data: data,
  });
}

export function deleteUser(access_token, id) {
  return axios.request({
    baseURL: process.env.REACT_APP_USER_URL,
    url: `/user/${id}`,
    method: 'DELETE',
    headers: {
      'Authorization': `Bearer ${access_token}`
    }
  });
}

export function disableUser(access_token, data) {
  return axios.request({
    baseURL: process.env.REACT_APP_USER_URL,
    url: '/user/disable',
    method: 'PUT',
    headers: {
      'Authorization': `Bearer ${access_token}`
    },
    data: data,
  });
}

export function enableUser(access_token, data) {
  return axios.request({
    baseURL: process.env.REACT_APP_USER_URL,
    url: '/user/enable',
    method: 'PUT',
    headers: {
      'Authorization': `Bearer ${access_token}`
    },
    data: data,
  });
}


export function getCurrentUser(access_token) {
  return axios.request({
    baseURL: process.env.REACT_APP_USER_URL,
    url: '/account',
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${access_token}`
    }
  });
}

export function updateCurrentUser(access_token, userData) {
  return axios.request({
    baseURL: process.env.REACT_APP_USER_URL,
    url: '/account',
    method: 'PUT',
    headers: {
      'Authorization': `Bearer ${access_token}`
    },
    data: userData
  });
}

export function updatePassword(access_token, passData) {
  return axios.request({
    baseURL: process.env.REACT_APP_USER_URL,
    url: '/account/change-password',
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${access_token}`
    },
    data: passData
  });
}
