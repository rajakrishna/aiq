import axios from "./base";

export function getDeploymentList(access_token, data) {
  return axios.request({
    url: `/api/deployments`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
    params: {
      sort: data.sort || null,
      size: data.size || null,
      failing: data.failing || null,
      "created_by.contains": data.createdByContains || null,
      created_by: data.createdByEquals || null,
      "created_by.in": data.createdByIn || null,
      "created_by.specified": data.createdBySpecified || null,
      timestamp: data.createdDateEquals || null,
      "timestamp.greaterOrEqualThan": data.created_date || null,
      "timestamp.greaterThan": data.createdDateGreaterThan || null,
      "timestamp.in": data.createdDateIn || null,
      "timestamp.lessOrEqualThan": data.createdDateLessOrEqualThan || null,
      "timestamp.lessThan": data.createdDateLessThan || null,
      "timestamp.specified": data.createdDateSpecified || null,
      "id.contains": data.idContains || null,
      id: data.idEquals || null,
      "id.in": data.idIn || null,
      "id.specified": data.idSpecified || null,
      "ml_algorithm.contains": data.mlAlgorithmcontains || null,
      ml_algorithm: data.mlAlgorithmequals || null,
      "ml_algorithm.in": data.mlAlgorithmin || null,
      "ml_algorithm.specified": data.mlAlgorithmSpecified || null,
      "ml_library.contains": data.mlLibraryContains || null,
      ml_library: data.mlLibraryEquals || null,
      "ml_library.in": data.mlLibraryIn || null,
      "ml_library.specified": data.mlLibrarySpecified || null,
      model_id: data.modelID || null,
      model_name: data.modelNameEquals || null,
      "name.contains": data.nameContains || null,
      name: data.nameEquals || null,
      "name.in": data.nameIn || null,
      "name.specified": data.nameSpecified || null,
      offset: data.offset || null,
      "owner.contains": data.ownerContains || null,
      owner: data.ownerEquals || null,
      "owner.in": data.ownerIn || null,
      "owner.specified": data.ownerSpecified || null,
      pageNumber: data.pageNumber || null,
      pageSize: data.pageSize || null,
      paged: data.paged || null,
      page: data.page || null,
      "project_name.contains": data.projectNameContains || null,
      project_name: data.projectNameEquals || null,
      "project_name.in": data.projectNameIn || null,
      "project_id.contains": data.projectIDContains || null,
      project_id: data.projectID || null,
      "project_name.specified": data.projectNameSpecified || null,
      "sort.sorted": data.sortSorted || null,
      "sort.unsorted": data.sortUnsorted || null,
      "status.contains": data.statusContains || null,
      status: data.statusEquals || null,
      "status.in": data.statusIn || null,
      "status.specified": data.statusSpecified || null,
      "type.contains": data.typecontains || null,
      type: data.typeequals || null,
      "type.in": data.typein || null,
      "type.specified": data.typeSpecified || null,
      data_drift: data.dataDriftequals,
      "data_drift.in": data.dataDriftin,
      "data_drift.lessThan": data.dataDriftlessThan,
      "data_drift.lessOrEqualThan": data.dataDriftlessOrEqualThan,
      "data_drift.greaterThan": data.dataDriftgreaterThan,
      "data_drift.greaterOrEqualThan": data.dataDriftgreaterOrEqualThan,
      health_score: data.healthScoreequals,
      "health_score.in": data.healthScorein,
      "health_score.lessThan": data.healthScorelessThan,
      "health_score.lessOrEqualThan": data.healthScorelessOrEqualThan,
      "health_score.greaterThan": data.healthScoregreaterThan,
      "health_score.greaterOrEqualThan": data.healthScoregreaterOrEqualThan,
      unpaged: data.unpaged || null,
    },
  });
}

export function delDeployment(access_token, name) {
  return axios.request({
    url: `/api/deployments/${name}`,
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
  });
}

export function scalingDeployment(access_token, details) {
  return axios.request({
    url: `/api/deployments/${details.name}/scale/${details.replicas}`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
  });
}

export function getDeployment(access_token, name) {
  return axios.request({
    url: `/api/deployments/${name}`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
  });
}

export function getDeploymentLog(access_token, data) {
  return axios.request({
    url: `/api/deployments/${data.name}/logs`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
    params: {
      replica: data.replica || 0,
    },
  });
}

export function externalEndpoint(access_token, url, data) {
  return axios.request({
    url: url,
    method: "POST",
    headers: {
      Authorization: `Bearer ${access_token}`,
      "Content-Type": "application/json",
    },
    data: data,
  });
}

export function internalEndpoint(access_token, name, data) {
  return axios.request({
    url: `/api/deployments/${name}/predict`,
    method: "POST",
    headers: {
      Authorization: `Bearer ${access_token}`,
      "Content-Type": "application/json",
    },
    data: data,
  });
}

export function getDeploymentCpu(access_token, data) {
  
  return axios.request({
    url: `/api/deployments/${data.name}/metrics/cpu`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
    params: {
      step: data.step || "1h",
      start: data.end || "now-1d",
      end: data.start || "now-1d",
    },
  });
}

export function getDeploymentMemory(access_token, data) {
  return axios.request({
    url: `/api/deployments/${data.name}/metrics/memory`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
    params: {
      step: data.step || "1h",
      start: data.end || "now-1d",
      end: data.start || "now-1d",
    },
  });
}

export function createDeployment(access_token, data) {
  return axios.request({
    url: `/api/deployments`,
    method: "POST",
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
    data: data,
  });
}

export function submitBatchScore(access_token, deploymentName, data) {
  return axios.request({
    url: `/api/deployments/${deploymentName}/score`,
    method: "POST",
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
    data: data,
  });
}

export function uploadDatasets(access_token, data) {
  const formData = new FormData();
  formData.append("file", data.file);
  return axios.request({
    url: `/api/deployments/${data.deploymentName}/batch-scoring-runs`,
    method: "POST",
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
    data: formData,
  });
}

export function getBatchScoringRuns(access_token, data) {
  return axios.request({
    url: `/api/deployments/${data.name}/batch-scoring-runs`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
    params: {
      sort: data.sort || null,
      "requested_date.greaterOrEqualThan": data.startDate || null,
      "requested_date.lessOrEqualThan": data.endDate || null,
      "workflow_status.equals": data.status || null,
      pageNumber: data.pageNumber || null,
      pageSize: data.pageSize || null,
      page: data.pageNumber || null,
      size: data.pageSize || null,
    },
  });
}

export function downloadPredictionFile(
  access_token,
  id,
  deploymentName,
  progress,
  downloadCancelToken
) {
  return axios.request({
    url: `/api/deployments/${deploymentName}/batch-scoring-runs/${id}/output`,
    method: "GET",
    responseType: "blob",
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
    onDownloadProgress: progress,
    cancelToken: downloadCancelToken.token,
  });
}

export function deleteBatchScoreRun(
  access_token,
  deployment_name,
  workflow_id
) {
  return axios.request({
    url: `/api/deployments/${deployment_name}/batch-scoring-runs/${workflow_id}`,
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
  });
}

export function startDeployments(access_token, name) {
  return axios.request({
    url: `/api/deployments/${name}/start`,
    method: "POST",
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
  });
}
export function stopDeployments(access_token, name) {
  return axios.request({
    url: `/api/deployments/${name}/stop`,
    method: "POST",
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
  });
}
export function metricsRequestrate(access_token, data) {
  return axios.request({
    url: `/api/deployments/${data.name}/metrics/totalrequests`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
    params: {
      step: data.step || "30s",
      start: data.end || "now-1d",
      end: data.start || "now-1d",
    },
  });
}
export function metricsSuccess(access_token, data) {
  return axios.request({
    url: `/api/deployments/${data.name}/metrics/status/200`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
    params: {
      step: data.step || "30s",
      start: data.end || "now-1d",
      end: data.start || "now-1d",
    },
  });
}
export function metrics5xx(access_token, data) {
  return axios.request({
    url: `/api/deployments/${data.name}/metrics/status/500`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
    params: {
      step: data.step || "30s",
      start: data.end || "now-1d",
      end: data.start || "now-1d",
    },
  });
}
export function metrics4xx(access_token, data) {
  return axios.request({
    url: `/api/deployments/${data.name}/metrics/status/400`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
    params: {
      step: data.step || "30s",
      start: data.end || "now-1d",
      end: data.start || "now-1d",
    },
  });
}
export function metricsReqpersec(access_token, data) {
  return axios.request({
    url: `/api/deployments/${data.name}/metrics/reqpersec`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
    params: {
      step: data.step || "30s",
      start: data.end || "now-1d",
      end: data.start || "now-1d",
    },
  });
}
export function metricsLatency(access_token, data) {
  return axios.request({
    url: `/api/deployments/${data.name}/metrics/latency`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
    params: {
      step:data.step || "40s",
      start: data.end || "now-1d",
      end: data.start || "now-1d",
    },
  });
}
