import axios from './base';

export function getJobsRunList(access_token, data) {
  return axios.request({
    url: `/api/job-runs`,
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${access_token}`
    },
    params: {
      id: data.jobId || null,
      project_id: data.projectId || null,
      model_id: data.modelId || null
    }
  });
}

export function getJobsRunById(access_token,id) {
  return axios.request({
    url: `/api/jobs/${id}/runs`,
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${access_token}`
    }
  });
}

export function getLogs(access_token, name, displayName) {
  return axios.request({
    url: `/api/mlworkflow-runs/${name}/step/${displayName}/streamlogs`,
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${access_token}`,
    },
    responseType: "stream",
    timeout: 0
  });
}

export function getJobsCounts(access_token) {
  return axios.request({
    url: '/api/jobs/aggs/count',
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${access_token}`
    }
  })
}

export function getAllJobs(access_token, data) {
  return axios.request({
    url: '/api/jobs',
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${access_token}`
    },
    params: {
      sort: data.sort || null,
      size: data.size || null,
      "failing.equals": data.failing || null,
      "created_by.contains": data.createdByContains || null,
      "created_by.equals": data.createdByEquals || null,
      "created_by.in": data.createdByIn || null,
      "created_by.specified": data.createdBySpecified || null,
      "created_date.equals": data.createdDateEquals || null,
      "created_date.greaterOrEqualThan": data.created_date || null,
      "created_date.greaterThan": data.createdDateGreaterThan || null,
      "created_date.in": data.createdDateIn || null,
      "created_date.lessOrEqualThan": data.createdDateLessOrEqualThan || null,
      "created_date.lessThan": data.createdDateLessThan || null,
      "created_date.specified": data.createdDateSpecified || null,
      "id.contains": data.idContains || null,
      "id.equals": data.idEquals || null,
      "id.in": data.idIn || null,
      "id.specified": data.idSpecified || null,
      "ml_algorithm.contains": data.mlAlgorithmcontains || null,
      "ml_algorithm.equals": data.mlAlgorithmequals || null,
      "ml_algorithm.in": data.mlAlgorithmin || null,
      "ml_algorithm.specified": data.mlAlgorithmSpecified || null,
      "ml_library.contains": data.mlLibraryContains || null,
      "ml_library.equals": data.mlLibraryEquals || null,
      "ml_library.in": data.mlLibraryIn || null,
      "ml_library.specified": data.mlLibrarySpecified || null,
      "name.contains": data.nameContains || null,
      "name.equals": data.nameEquals || null,
      "name.in": data.nameIn || null,
      "name.specified": data.nameSpecified || null,
      offset: data.offset || null,
      "owner.contains": data.ownerContains || null,
      "owner.equals": data.ownerEquals || null,
      "owner.in": data.ownerIn || null,
      "owner.specified": data.ownerSpecified || null,
      "project_name.in": data.projectName || null,
      "project_id.contains": data.projectIDContains || null,
      pageNumber: data.pageNumber || null,
      pageSize: data.pageSize || null,
      paged: data.paged || null,
      page: data.page || null,
      "sort.sorted": data.sortSorted || null,
      "sort.unsorted": data.sortUnsorted || null,
      "status.contains": data.statusContains || null,
      "status.equals": data.statusEquals || null,
      "status.in": data.statusIn || null,
      "status.specified": data.statusSpecified || null,
      "type.contains": data.typecontains || null,
      "type.equals": data.typeequals || null,
      "type.in": data.typein || null,
      "type.specified": data.typeSpecified || null,
      unpaged: data.unpaged || null
    }
  });
}

export function delJob(access_token, id) {
  return axios.request({
    url: `/api/jobs/${id}`,
    method: 'DELETE',
    headers: {
      'Authorization': `Bearer ${access_token}`
    }
  });
}

export function delJobRun(access_token, name) {
  return axios.request({
    url: `/api/job-runs/${name}`,
    method: 'DELETE',
    headers: {
      'Authorization': `Bearer ${access_token}`
    }
  });
}

export function createJob(access_token, data) {
  return axios.request({
    url: '/api/jobs',
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${access_token}`,
      'Content-Type': 'application/json'
    },
    data: data,
  });
}

export async function runJob(access_token, data, parameters=[]) {
  return axios.request({
    url: `/api/jobs/${data}/submit`,
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${access_token}`
    },
    data: parameters
  });
}

export async function updateJob(access_token, data) {
  return axios.request({
    url: '/api/jobs',
    method: 'PUT',
    headers: {
      'Authorization': `Bearer ${access_token}`
    },
    data: data,
  });
}

export function getJob(access_token, jobID) {
  return axios.request({
      url: `/api/jobs/${jobID}`,
      method: 'GET',
      headers: {
        Authorization: `Bearer ${access_token}`
      }
  });
}

export function getJobRun(access_token, name) {
  return axios.request({
      url: `/api/job-runs/${name}/`,
      method: 'GET',
      headers: {
      'Authorization': `Bearer ${access_token}`
      }
  });
}

export function getJobCpuData(access_token,name,start,end,step) {
  return axios.request({
      url: `/api/workflows/${name}/metrics/cpu?start=${start}&end=${end}&step=${step}`,
      method: 'GET',
      headers: {
      'Authorization': `Bearer ${access_token}`
      }
  });
}

export function getJobMemoryData(access_token,name,start,end,step) {
  return axios.request({
      url: `/api/workflows/${name}/metrics/memory?start=${start}&end=${end}&step=${step}`,
      method: 'GET',
      headers: {
      'Authorization': `Bearer ${access_token}`
      }
  });
}
