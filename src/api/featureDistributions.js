import axios from './base';

//=================================
//   Bias 
//================================

export function getFeatureDistributionsByModelId(access_token, id, bias_config_id, feature) {
  return axios.request({
    url: `/api/feature-distributions?model_id.equals=${id}&bias_config_id.equals=${bias_config_id}&feature.equals=${feature}`,
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${access_token}`
    },
  })
}

export function getFeatureDistributionsByType(access_token, id, bias_config_id, feature, type) {
  return axios.request({
    url: `/api/feature-distributions?model_id.equals=${id}&bias_config_id.equals=${bias_config_id}&feature.equals=${feature}&type.equals=${type}`,
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${access_token}`
    },
  })
}

export function getAvgFeatureDistributionsByType(access_token, id, bias_config_id, feature, type) {
  return axios.request({
    url: `/api/feature-distributions/aggs/avg?interval=day&field=distribution&model_id.equals=${id}&bias_config_id.equals=${bias_config_id}&feature.equals=${feature}&type.equals=${type}`,
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${access_token}`
    },
  })
}