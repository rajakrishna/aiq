import retailImg from '../images/solutions/retail.jpg';
import salesImg from '../images/solutions/sales.jpg';
import healthcareImg from '../images/solutions/healthcare.jpg';

export const Solutions = [
  {
    id: 1,
    heading: "retail",
    subheading: "Optimize forecasting, customer experiences, inventory management, and more.",
    imgUrl: retailImg
  },
  {
    id: 2,
    heading: "sales",
    subheading: "Boost sales productivity and drive sales performance improvements.",
    imgUrl: salesImg
  },
  {
    id: 3,
    heading: "healthcare",
    subheading: "Improve health outcomes and drive significant cost savings.",
    imgUrl: healthcareImg
  }
]