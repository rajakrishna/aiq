import salesforceImg from '../images/datasource/salesforce.jpg';
import snowflakeImg from '../images/datasource/snowflake.png';
import allscriptsImg from '../images/datasource/allscripts.png';
import hl7Img from '../images/datasource/hl7.png';
import epicImg from '../images/datasource/epic.png';
import fhirImg from '../images/datasource/fhir.png';
import postgreImg from '../images/datasource/postgrey.png';
import exlcelImg from '../images/datasource/excel.svg';
import azurefilesImg from '../images/datasource/azure-files.svg';
import azuresqlImg from '../images/datasource/azure-sql-database.svg';
import cosmosdbImg from '../images/datasource/cosmos-db.svg';
import bigcommerceImg from '../images/datasource/Bigcommerce.png';
import shopifyImg from '../images/datasource/shopify.jpeg';
import dynamics365Img from '../images/datasource/dynamics-365.png';
import aiqforecastImg from '../images/datasource/predera-aiq.jpeg';


export const fieldsFromDatasourceApi = [
    {
        "name": "CHURN",
        "type": "CHURN",
        "defaultValue": "00Q5j00000EntH9EAJ"
    },
    {
        "name": "CUSTOMERID",
        "type": "CUSTOMERID",
        "defaultValue": "00Q5j00000EntH9EAJ"
    },
    {
        "name": "GENDER",
        "type": "GENDER",
        "defaultValue": false
    },
    {
        "name": "SENIORCITIZEN",
        "type": "SENIORCITIZEN",
        "defaultValue": null
    },
    {
        "name": "PARTNER",
        "type": "PARTNER",
        "defaultValue": "Becker"
    },
    {
        "name": "DEPENDENTS",
        "type": "DEPENDENTS",
        "defaultValue": "Glenn"
    },
    {
        "name": "TENURE",
        "type": "TENURE",
        "defaultValue": null
    },
    {
        "name": "PHONESERVICE",
        "type": "PHONESERVICE",
        "defaultValue": "Glenn Becker"
    },
    {
        "name": "MULTIPLELINES",
        "type": "MULTIPLELINES",
        "defaultValue": null
    },
    {
        "name": "INTERNETSERVICE",
        "type": "INTERNETSERVICE",
        "defaultValue": "Glass and Sons"
    },
    {
        "name": "ONLINESECURITY",
        "type": "ONLINESECURITY",
        "defaultValue": "33301 Vincent Manors"
    },
    {
        "name": "ONLINEBACKUP",
        "type": "ONLINEBACKUP",
        "defaultValue": "Samuelton"
    },
    {
        "name": "DEVICEPROTECTION",
        "type": "DEVICEPROTECTION",
        "defaultValue": "Tennessee"
    },
    {
        "name": "TECHSUPPORT",
        "type": "TECHSUPPORT",
        "defaultValue": "27329"
    },
    {
        "name": "STREAMINGTV",
        "type": "STREAMINGTV",
        "defaultValue": "Saint Pierre and Miquelon"
    },
    {
        "name": "STREAMINGMOVIES",
        "type": "STREAMINGMOVIES",
        "defaultValue": null
    },
    {
        "name": "CONTRACT",
        "type": "CONTRACT",
        "defaultValue": "/services/images/photo/00Q5j00000EntH9EAJ"
    },
    {
        "name": "PAPERLESSBILLING",
        "type": "PAPERLESSBILLING",
        "defaultValue": null
    },
    {
        "name": "PAYMENTMETHOD",
        "type": "PAYMENTMETHOD",
        "defaultValue": "Purchased List"
    },
    {
        "name": "MONTHLYCHARGES",
        "type": "MONTHLYCHARGES",
        "defaultValue": "Open - Not Contacted"
    },
    {
        "name": "TOTALCHARGES",
        "type": "TOTALCHARGES",
        "defaultValue": "Construction"
    }
]
export const salesForceFieldsArray = [
    {
        "fieldName": "ActionCadenceAssigneeId",
        "type": "reference",
        "value": "s29i9DD99D#!39i"
    },
    {
        "fieldName": "ActivityMetricId",
        "type": "reference",
        "value": "002989dee998992w"
    },
    {
        "fieldName": "Address",
        "type": "address",
        "value": "Ms Alice Smith Apartment 1c 213 Derrick Street Boston, MA 02130 USA"
    },
    {
        "fieldName": "AnualRevenue",
        "type": "currency",
        "value": "$USD"
    },
    {
        "fieldName": "City",
        "type": "string",
        "value": "London"
    },
    {
        "fieldName": "CleanStatus",
        "type": "picklist",
        "value": "[ready, updated, pending]"
    },
    {
        "fieldName": "Company",
        "type": "string",
        "value": "General Motors"
    },
    {
        "fieldName": "CompanyDunsNumber",
        "type": "string",
        "value": "909-323-466-737"
    },
    {
        "fieldName": "ConnectionReceivedId",
        "type": "reference",
        "value": "00Q5j000008plQ5EAI"
    },
    {
        "fieldName": "connectionSentId",
        "type": "reference",
        "value": "0055j000005yrWlAAI"
    },
    {
        "fieldName": "ConvertedAccountId",
        "type": "reference",
        "value": "0055j000005yrWlAAI"
    },
    {
        "fieldName": "ConvertedContactId",
        "type": "reference",
        "value": "0055j000005yrWlAAI"
    },
    {
        "fieldName": "ConvertedDate",
        "type": "date",
        "value": "25-04-2022"
    },
    {
        "fieldName": "ConvertedOpportunityId",
        "type": "reference",
        "value": "0055j000005yrWlAAI"
    },
    {
        "fieldName": "Country",
        "type": "string",
        "value": "France"
    },
    {
        "fieldName": "CountryCode",
        "type": "picklist",
        "value": "[93, 355, 213]"
    },
    {
        "fieldName": "CountryIsoCode",
        "type": "picklist",
        "value": "[004, 248, 008]"
    },
    {
        "fieldName": "Description",
        "type": "textarea",
        "value": "If true, lead has been assigned, but not yet viewed. See Unread Leads for more information"
    },
    {
        "fieldName": "Division",
        "type": "picklist",
        "value": "[open, closed, semi-open]"
    },
    {
        "fieldName": "Email",
        "type": "email",
        "value": "johnson@gmail.com"
    },
    {
        "fieldName": "EmailBouncedDate",
        "type": "dateTime",
        "value": "Sun Mar 25 02:31:30 GMT 2021"
    },
    {
        "fieldName": "EmailBouncedReason",
        "type": "string",
        "value": "Rejection"
    },
    {
        "fieldName": "Fax",
        "type": "phone",
        "value": "+91-898833677"
    },
    {
        "fieldName": "FirstCallDateTime",
        "type": "datetime",
        "value": "Sun Mar 12 02:31:30 GMT 2022"
    },
    {
        "fieldName": "FirstEmailDateTime",
        "type": "datetime",
        "value": "Sun Mar 20 02:31:30 GMT 2022"
    },
    {
        "fieldName": "FirstName",
        "type": "string",
        "value": "Johnson"
    },
    {
        "fieldName": "HasOptedOutOfEmail",
        "type": "boolean",
        "value": "false"
    },
    {
        "fieldName": "HasOptedOutOfFax",
        "type": "boolean",
        "value": "true"
    },
    {
        "fieldName": "GeocodeAccuracy",
        "type": "picklist",
        "value": "[Ind, USA, UK]"
    },
    {
        "fieldName": "IndividualId",
        "type": "reference",
        "value": "0055j000005yrWlAAI"
    },
    {
        "fieldName": "Industry",
        "type": "picklist",
        "value": "[shipping, auto, transport]"
    },
    {
        "fieldName": "IsConverted",
        "type": "boolean",
        "value": "true"
    },
    {
        "fieldName": "IsDeleted",
        "type": "boolean",
        "value": "false"
    },
    {
        "fieldName": "IsUnreadByOwner",
        "type": "boolean",
        "value": "true"
    }
]

export const enabled = [
    {
        "name": "Snowflake-01",
        "type": "SNOWFLAKE",
        "category": "sales",
        "status": "enabled",
        "imgUrl": snowflakeImg,
        "desc": "this data is from Snowflake",
        "ref_id": "f845d953e6eb415cbcba2b29697a8f2c",
        "created_at": "2022-07-20T07:27:48.847672",
        "updated_at": "2022-07-20T07:27:48.847676",
        "created_by": "forecast-aiq_dataset",
        "updated_by": "forecast-aiq_dataset",
        "params": [
            {
                "name": "user",
                "value": "vijay.reddy@predera.com",
                "ref_id": "674c6dabfd48495181e6d85410d33a81"
            },
            {
                "name": "password",
                "value": "********",
                "ref_id": "17fd6995d25c4ba793fe6b05126a9ce92"
            },
            {
                "name": "account",
                "value": "er42416.central-us.azure",
                "ref_id": "17fd6995d25c4ba793fe6b05126a9ce93"
            },
            {
                "name": "warehouse",
                "value": "Telco_customers",
                "ref_id": "17fd6995d25c4ba793fe6b05126a9ce94"
            },
            {
                "name": "database",
                "value": "Telco_customer",
                "ref_id": "17fd6995d25c4ba793fe6b05126a9ce95"
            },
            {
                "name": "schema",
                "value": "PUBLIC",
                "ref_id": "17fd6995d25c4ba793fe6b05126a9ce96"
            },
            {
                "name": "sandbox",
                "value": "False",
                "ref_id": "17fd6995d25c4ba793fe6b05126a9ce97"
            }
        ]
    },

    {
        "name": "salesforce_-01",
        "type": "SALESFORCE",
        "category": "sales",
        "status": "enabled",
        "imgUrl": salesforceImg,
        "desc": "this data is from Salesforce",
        "ref_id": "f845d953e6eb415cbcba2b29697a8f2c3",
        "created_at": "2022-07-20T07:27:48.847672",
        "updated_at": "2022-07-20T07:27:48.847676",
        "created_by": "forecast-aiq_dataset",
        "updated_by": "forecast-aiq_dataset",
        "params": [
            {
                "name": "username",
                "value": "bala.sivakumari@predera.com",
                "ref_id": "674c6dabfd48495181e6d85410d33a88"
            },
            {
                "name": "dataset_id",
                "value": "bc612a489ea04d8dae524b07db448cd1",
                "ref_id": "17fd6995d25c4ba793fe6b05126a9ce9"
            }
        ]
    },
    {
        "name": "forecast-aiq_dataset",
        "type": "AIQ_DATASET",
        "category": "retail",
        "status": "enabled",
        "imgUrl": aiqforecastImg,
        "desc": "this data is from aiq dataset",
        "ref_id": "f845d953e6eb415cbcba2b29697a8f2c9",
        "created_at": "2022-07-20T07:27:48.847672",
        "updated_at": "2022-07-20T07:27:48.847676",
        "created_by": "forecast-aiq_dataset",
        "updated_by": "forecast-aiq_dataset",
        "params": [
            {
                "name": "username",
                "value": "bala.sivakumari@predera.com",
                "ref_id": "674c6dabfd48495181e6d85410d33a88"
            },
            {
                "name": "dataset_id",
                "value": "bc612a489ea04d8dae524b07db448cd1",
                "ref_id": "17fd6995d25c4ba793fe6b05126a9ce9"
            }
        ]
    },
    {
        "name": "demo-D365-01",
        "type": "DYNAMICS365",
        "category": "retail",
        "status": "enabled",
        "imgUrl": dynamics365Img,
        "desc": "This data is from dynamic 365 database.",
        "ref_id": "7b7d157280b54229b22da35f43ace453",
        "created_at": "2022-07-20T08:42:22.427906",
        "updated_at": "2022-07-20T08:42:22.427910",
        "created_by": "demo-D365-01",
        "updated_by": "demo-D365-01",
        "params": [
            {
                "name": "username",
                "value": "d-nazeer@digipropel.com",
                "ref_id": "568c505c89d0407bace3c1895eb96248"
            },
            {
                "name": "password",
                "value": "DigiPred!",
                "ref_id": "c91e4c2e481643dbab240c81b34e7b04"
            },
            {
                "name": "URL",
                "value": "https://org64c3562c.crm8.dynamics.com",
                "ref_id": "ee487bfb3f3d45bd9637d70b7bce67e0"
            },
            {
                "name": "clientId",
                "value": "51f81489-12ee-4a9e-aaae-a2591f45987d",
                "ref_id": "d3e16119ee794081a046611101ac5668"
            },
            {
                "name": "tenantId",
                "value": "3cd6cc04-a0f3-4b1c-aff4-b8bb8c0394ec",
                "ref_id": "12663e3b07404d2ba1043ff3ab809cd8"
            }
        ]
    },
    {
        "name": "BC-datasource-01",
        "type": "BIGCOMMERCE",
        "category": "retail",
        "status": "enabled",
        "imgUrl": bigcommerceImg,
        "desc": "demo-datasource-01",
        "ref_id": "3d025d890cd14f9b93c83cb3e0b2479c",
        "created_at": "2022-07-20T08:44:55.681600",
        "updated_at": "2022-07-20T08:44:55.681603",
        "created_by": "BC-datasource-01",
        "updated_by": "BC-datasource-01",
        "params": [
            {
                "name": "username",
                "value": "srikanth.thirumala@predera.com",
                "ref_id": "347b121cc85541a29a96a0b3f96a9c8c"
            },
            {
                "name": "password",
                "value": "B@l@$!v@",
                "ref_id": "729c405784464301aba8d85edd2c18f1"
            }
        ]
    },
    {
        "name": "PostgreSQL",
        "type": "POSTGRES",
        "category": "healthcare",
        "status": "enabled",
        "imgUrl": postgreImg,
        "desc": "Connect to PostgreSQL database.",
        "ref_id": "f845d953e6eb415cbcba2b29697a8f2c2",
        "created_at": "2022-07-20T07:27:48.847672",
        "updated_at": "2022-07-20T07:27:48.847676",
        "created_by": "Postgres",
        "updated_by": "Postgres",
        "params": [
            {
                "name": "username",
                "value": "bala.sivakumari@predera.com",
                "ref_id": "674c6dabfd48495181e6d85410d33a88"
            },
            {
                "name": "dataset_id",
                "value": "bc612a489ea04d8dae524b07db448cd1",
                "ref_id": "17fd6995d25c4ba793fe6b05126a9ce9"
            }
        ]
    },
    {
        "name": "PostgreSQL-02",
        "type": "POSTGRES",
        "category": "healthcare",
        "status": "enabled",
        "imgUrl": postgreImg,
        "desc": "Connect to PostgreSQL database.",
        "ref_id": "f845d953e6eb415cbcba2b29697a8f2c2019",
        "created_at": "2022-07-20T07:27:48.847672",
        "updated_at": "2022-07-20T07:27:48.847676",
        "created_by": "Postgres",
        "updated_by": "Postgres",
        "params": [
            {
                "name": "username",
                "value": "bala.sivakumari@predera.com",
                "ref_id": "674c6dabfd48495181e6d85410d33a88"
            },
            {
                "name": "dataset_id",
                "value": "bc612a489ea04d8dae524b07db448cd1",
                "ref_id": "17fd6995d25c4ba793fe6b05126a9ce9"
            }
        ]
    },
]




export const available = [
    {
        "id": 1,
        "type": "SNOWFLAKE",
        "category": ["sales"],
        "status": "available",
        "heading": "Snowflake",
        "subheading": "Data Source",
        "imgUrl": snowflakeImg,
        "desc": "Connect to SNOWFLAKE database.",
        "params": [
            {
                "name": "username",
                "value": "",
                "ref_id": 1
            },
            {
                "name": "password",
                "value": "",
                "ref_id": 2
            },
            {
                "name": "account",
                "value": "",
                "ref_id": 3
            },
            {
                "name": "warehouse",
                "value": "",
                "ref_id": 4
            },
            {
                "name": "database",
                "value": "",
                "ref_id": 5
            },
            {
                "name": "schema",
                "value": "",
                "ref_id": 6
            },
            {
                "name": "sandbox",
                "value": "False",
                "ref_id": 7
            }
        ]
    },
    {
        "id": 2,
        "type": "SALESFORCE",
        "category": ["sales"],
        "status": "available",
        "heading": "Salesforce",
        "subheading": "Data Source",
        "imgUrl": salesforceImg,
        "desc": "Connect to SALESFORCE database.",
        "params": [
            {
                "name": "username",
                "value": "",
                "ref_id": 1
            },
            {
                "name": "password",
                "value": "",
                "ref_id": 2
            },
            {
                "name": "security_token",
                "value": "",
                "ref_id": 3
            },
            {
                "name": "sandbox",
                "value": "False",
                "ref_id": 4
            }
        ]
    },

    {
        "id": 3,
        "type": "ALLSCRIPTS",
        "category": ["healthcare"],
        "status": "available",
        "heading": "AllScripts-demo",
        "subheading": "Data Source",
        "imgUrl": allscriptsImg,
        "desc": "Connect to ALLSCRIPTS database.",
        "params": [
            {
                "name": "username",
                "value": "",
                "ref_id": 1
            },
            {
                "name": "dataset_id",
                "value": "",
                "ref_id": 2
            }
        ]
    },
    {
        "id": 4,
        "type": "EPIC",
        "category": ["healthcare"],
        "status": "available",
        "heading": "Epic-datasource-01",
        "subheading": "Data Source",
        "imgUrl": epicImg,
        "desc": "Connect to EPIC database.",
        "params": [
            {
                "name": "username",
                "value": "",
                "ref_id": 1
            },
            {
                "name": "dataset_id",
                "value": "",
                "ref_id": 2
            }
        ]
    },
    {
        "id": 5,
        "type": "HL7",
        "category": ["healthcare"],
        "status": "available",
        "heading": "Hl7",
        "subheading": "Data Source",
        "imgUrl": hl7Img,
        "desc": "Connect to HL7 database.",
        "params": [
            {
                "name": "username",
                "value": "",
                "ref_id": 1
            },
            {
                "name": "dataset_id",
                "value": "",
                "ref_id": 2
            }
        ]
    },
    {
        "id": 6,
        "type": "FHIR",
        "category": ["healthcare"],
        "status": "available",
        "heading": "Hl7fhir",
        "subheading": "Data Source",
        "imgUrl": fhirImg,
        "desc": "Connect to FHIR database.",
        "params": [
            {
                "name": "username",
                "value": "",
                "ref_id": 1
            },
            {
                "name": "dataset_id",
                "value": "",
                "ref_id": 2
            }
        ]
    },
    {
        "id": 7,
        "type": "AIQ_DATASET",
        "category": ["healthcare", "sales"],
        "status": "available",
        "heading": "Forecast-aiq_dataset",
        "subheading": "Data Source",
        "imgUrl": exlcelImg,
        "desc": "Connect to AIQ_DATASET database.",
        "params": [
            {
                "name": "username",
                "value": "",
                "ref_id": 1
            },
            {
                "name": "dataset_id",
                "value": "",
                "ref_id": 2
            }
        ]
    },
    {
        "id": 8,
        "type": "DYNAMICS365",
        "category": ["sales"],
        "status": "available",
        "heading": "Dynamics-365",
        "subheading": "Data Source",
        "imgUrl": dynamics365Img,
        "desc": "This data is from dynamic 365 database.",
        "params": [
            {
                "name": "username",
                "value": "",
                "ref_id": 1
            },
            {
                "name": "password",
                "value": "",
                "ref_id": 2
            },
            {
                "name": "url",
                "value": "",
                "ref_id": 3
            },
            {
                "name": "client_id",
                "value": "",
                "ref_id": 4
            },
            {
                "name": "tenant_id",
                "value": "",
                "ref_id": 5
            }
        ]

    },
    {
        "id": 9,
        "type": "POSTGRES",
        "category": ["healthcare", "sales", "retail"],
        "status": "available",
        "heading": "Postgres",
        "subheading": "Data Source",
        "imgUrl": postgreImg,
        "desc": "Connect to POSTGRES database.",
        "params": [
            {
                "name": "username",
                "value": "",
                "ref_id": 1
            },
            {
                "name": "dataset_id",
                "value": "",
                "ref_id": 2
            }
        ]

    },
    {
        "id": 10,
        "type": "SHOPIFY",
        "category": ["sales"],
        "status": "available",
        "heading": "Shopify",
        "subheading": "Data Source",
        "imgUrl": shopifyImg,
        "desc": "Connect to SHOPIFY database.",
        "params": [
            {
                "name": "username",
                "value": "",
                "ref_id": 1
            },
            {
                "name": "dataset_id",
                "value": "",
                "ref_id": 2
            }
        ]
    },
    {
        "id": 11,
        "type": "BIGCOMMERCE",
        "category": ["retail"],
        "status": "available",
        "heading": "BigCommerce",
        "subheading": "Data Source",
        "imgUrl": bigcommerceImg,
        "desc": "Connect to BIGCOMMERCE database.",
        "params": [
            {
                "name": "username",
                "value": "",
                "ref_id": 1
            },
            {
                "name": "dataset_id",
                "value": "",
                "ref_id": 2
            }
        ]
    },
    {
        "id": 12,
        "type": "AZURE COSMOS DB",
        "category": ["healthcare", "sales"],
        "status": "available",
        "heading": "Azure Cosmos DB",
        "subheading": "Data Source",
        "imgUrl": cosmosdbImg,
        "desc": "Connect to AZURE COSMOS DB database.",
        "params": [
            {
                "name": "username",
                "value": "",
                "ref_id": 1
            },
            {
                "name": "dataset_id",
                "value": "",
                "ref_id": 2
            }
        ]

    },
    {
        "id": 13,
        "type": "AZURE SQL DATABASE",
        "category": ["healthcare", "sales"],
        "status": "available",
        "heading": "Azure SQL Database",
        "subheading": "Data Source",
        "imgUrl": azuresqlImg,
        "desc": "Connect to AZURE SQL DATABASE database.",
        "params": [
            {
                "name": "username",
                "value": "",
                "ref_id": 1
            },
            {
                "name": "dataset_id",
                "value": "",
                "ref_id": 2
            }
        ]
    },
    {
        "id": 14,
        "type": "AZURE FILES",
        "category": ["healthcare", "sales", "retail"],
        "status": "available",
        "heading": "Azure File",
        "subheading": "Data Source",
        "imgUrl": azurefilesImg,
        "desc": "Connect to AZURE FILES database.",
        "params": [
            {
                "name": "username",
                "value": "",
                "ref_id": 1
            },
            {
                "name": "dataset_id",
                "value": "",
                "ref_id": 2
            }
        ]
    },
    {
        "id": 15,
        "type": "EXCEL",
        "category": ["healthcare", "sales"],
        "status": "available",
        "heading": "Excel File",
        "subheading": "Data Source",
        "imgUrl": exlcelImg,
        "desc": "Connect to EXCEL database.",
        "params": [
            {
                "name": "username",
                "value": "",
                "ref_id": 1
            },
            {
                "name": "dataset_id",
                "value": "",
                "ref_id": 2
            }
        ]
    },

    {
        "id": 16,
        "type": "CSV",
        "category": ["retail"],
        "status": "available",
        "heading": "CSV file",
        "subheading": "Data Source",
        "imgUrl": exlcelImg,
        "desc": "Connect to CSV database.",
        "params": [
            {
                "name": "username",
                "value": "",
                "ref_id": 1
            },
            {
                "name": "dataset_id",
                "value": "",
                "ref_id": 2
            }
        ]
    },
]