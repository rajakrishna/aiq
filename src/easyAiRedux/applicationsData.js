import optimize_operationsImg from '../images/applications/optimise_operations.svg';
import suply_chainImg from '../images/applications/suply_chain.svg';
import contexual_personalisationImg from '../images/applications/contexual_personalisation.svg';
import demand_forecastingImg from '../images/applications/demand_forecast.png';
import customer_aquisitionImg from '../images/applications/customer_aquisition.svg';
import churn_predictionImg from '../images/applications/churn_prediction.png';
import lead_scroingImg from '../images/applications/lead_scoring.png';
import product_fitmentImg from '../images/applications/product_figment.webp';
import revenue_forecastImg from '../images/applications/revenue_forecast.svg';
import opportunity_scoringImg from '../images/applications/opportunity_scoring.svg';
import agent_routingImg from '../images/applications/agent_routing.png';
import nurse_staffingImg from '../images/applications/nurse_staffing.svg';
import asthama_predictionsImg from '../images/applications/asthama_prediction.svg';
import lengthofstayImg from '../images/applications/lengthofstay.svg';
import sepsis_predictionImg from '../images/applications/sepsis_prediction.svg';
import readmissionsImg from '../images/applications/readmissions.svg';
import heart_failureImg from '../images/applications/heart_failure.svg';
import dynamic_pricingImg from '../images/applications/dynamic_pricing.webp';





export const AllAvailableApplications = [
    {
        "id": 1,
        "name": "Lead Scoring",
        "description": "Improve conversion and close rates by identifying most valuable leads.",
        "Type": "Lead Conversion",
        "category": "sales",
        "status": "Available",
        "primary_dashboard": "https://stage.sandbox.com/id",
        "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
        "additional_report_ids": "\"1125\", \"5294\"",
        "created_at": "2022-07-05T10:19:59.670676",
        "created_by": "srikanth.thirumala@predera.com",
        "updated_at": "2022-07-05T10:19:59.670676",
        "updated_by": "vijaykumar.guntreddy@predera.com",
        "app_image_url": lead_scroingImg,
        "app_id": "lead_scoring"
    },
    {
        "id": 19,
        "name": "Churn Prediction",
        "description": "Retain more customers and grow your company.",
        "Type": "Chrun Prediction",
        "category": "sales",
        "status": "Available",
        "primary_dashboard": "https://stage.sandbox.com/id",
        "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
        "additional_report_ids": "\"1125\", \"5294\"",
        "created_at": "2022-07-05T10:19:59.670676",
        "created_by": "srikanth.thirumala@predera.com",
        "updated_at": "2022-07-05T10:19:59.670676",
        "updated_by": "vijaykumar.guntreddy@predera.com",
        "app_image_url": churn_predictionImg,
        "app_id": "churn_prediction"
    },
    {
        "id": 2,
        "name": "Product Fitment",
        "description": "Find out products, lead is likely to purchase and get business solutions empowered by artificial intelligence and machine learning.",
        "Type": "Product Fitment",
        "category": "sales",
        "status": "Available",
        "primary_dashboard": "https://sandbox.predere.com/id",
        "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
        "additional_report_ids": "\"1125\", \"5294\"",
        "created_at": "2022-07-05T10:19:59.670676",
        "created_by": "srikanth.thirumala@predera.com",
        "updated_at": "2022-07-05T10:19:59.670676",
        "updated_by": "vijaykumar.guntreddy@predera.com",
        "app_image_url": product_fitmentImg,
        "app_id": "product_fitment"
    },
    {
        "id": 3,
        "name": "Revenue Forecasting",
        "description": "revenue forecasting description. Revenue forecasting is helpful to the businesses to grow and stabilize.",
        "Type": "Revenue Forecasting",
        "category": "sales",
        "status": "Available",
        "primary_dashboard": "https://stage.sandbox.com/id",
        "model_ref_id": "7f4d6f882ed3428ca026a5d870d167c3",
        "additional_report_ids": "\"8334\", \"3393\"",
        "created_at": "2022-07-05T10:16:20.202190",
        "created_by": "vijaykumar.guntreddy@predera.com",
        "updated_at": "2022-07-05T10:16:20.202190",
        "updated_by": "srikanth.thirumala@predera.com",
        "app_image_url": revenue_forecastImg,
        "app_id": "revenue_forecasting"
    },
    {
        "id": 4,
        "name": "Opportunity Scoring",
        "description": "Identify the likelihood that an opportunity will be won.",
        "Type": "Opportunity Scoring",
        "category": "sales",
        "status": "Available",
        "primary_dashboard": "https://stage.sandbox.com/id",
        "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
        "additional_report_ids": "\"1125\", \"5294\"",
        "created_at": "2022-07-05T10:19:59.670676",
        "created_by": "srikanth.thirumala@predera.com",
        "updated_at": "2022-07-05T10:19:59.670676",
        "updated_by": "vijaykumar.guntreddy@predera.com",
        "app_image_url": opportunity_scoringImg,
        "app_id": "opportunity_scoring"
    },
    {
        "id": 5,
        "name": "Agent Routing",
        "description": " Build direct relationships between your customers and agents.",
        "Type": "Agent Routing",
        "category": "sales",
        "status": "Available",
        "primary_dashboard": "https://stage.sandbox.com/id",
        "model_ref_id": "a736a2a68ae74018bca74482e081873c",
        "additional_report_ids": "\"1135\", \"5363\"",
        "created_at": "2022-07-05T10:29:07.771793",
        "created_by": "srikanth.thirumala@predera.com",
        "updated_at": "2022-07-05T10:29:07.771793",
        "updated_by": "vijaykumar.guntreddy@predera.com",
        "app_image_url": agent_routingImg,
        "app_id": "agent_routing"
    },
    {
        "id": 6,
        "name": "Optimize Operations",
        "description": "Enable AI-driven site selection, staff scheduling, and capacity planning.",
        "Type": "Optimize Operations",
        "category": "retail",
        "status": "Available",
        "primary_dashboard": "https://stage.sandbox.predera.com/id",
        "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
        "additional_report_ids": "\"1123\", \"3939\"",
        "created_at": "2022-07-05T10:19:59.670676",
        "created_by": "vijaykumar.guntreddy@predera.com",
        "updated_at": "2022-07-05T10:19:59.670676",
        "updated_by": "vijaykumar.guntreddy@predera.com",
        "app_image_url": optimize_operationsImg,
        "app_id": "optimize_operations"
    },
    {
        "id": 7,
        "name": "Supply Chain Management",
        "description": "Predict shipment arrival times to foresee and mitigate potential disruptions to your supply chain.",
        "Type": "Supply Chain Management",
        "category": "retail",
        "status": "Available",
        "primary_dashboard": "https://stage.sandbox.predera.com/id",
        "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
        "additional_report_ids": "\"1129\", \"1192\"",
        "created_at": "2022-07-05T10:20:22.515729",
        "created_by": "vijaykumar.guntreddy@predera.com",
        "updated_at": "2022-07-05T10:20:22.515729",
        "updated_by": "srikanth.thirumala@predera.com",
        "app_image_url": suply_chainImg,
        "app_id": "supply_chain_management"
    },
    {
        "id": 8,
        "name": "Contextual Personalization",
        "description": "Real-time, cross-channel personalization provides contextually relevant content.",
        "Type": "Contextual Personalization",
        "category": "retail",
        "status": "Available",
        "primary_dashboard": "https://stage.sandbox.predera.com/id",
        "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
        "additional_report_ids": "\"1128\", \"9939\"",
        "created_at": "2022-07-05T10:19:59.670676",
        "created_by": "vijaykumar.guntreddy@predera.com",
        "updated_at": "2022-07-05T10:19:59.670676",
        "updated_by": "srikanth.thirumala@predera.com",
        "app_image_url": contexual_personalisationImg,
        "app_id": "contextual_personalization"
    },
    {
        "id": 9,
        "name": "Dynamic Pricing",
        "description": "Optimize offers and pricing using advanced customer insights.",
        "Type": "Dynamic Pricing",
        "category": "retail",
        "status": "Available",
        "primary_dashboard": "https://stage.predera.com/",
        "model_ref_id": "f662401899a94b5f98253579b163a99d",
        "additional_report_ids": "\"2239\", \"1193\"",
        "created_at": "2022-07-05T10:23:51.229208",
        "created_by": "srikanth.thirumala@predera.com",
        "updated_at": "2022-07-05T10:23:51.229208",
        "updated_by": "srikanth.thirumala@predera.com",
        "app_image_url": dynamic_pricingImg,
        "app_id": "dynamic_pricing"
    },
    {
        "id": 10,
        "name": "Demand Forecasting",
        "description": "Estimate future customer demand over a certain time period.",
        "Type": "Demand Forecasting",
        "category": "retail",
        "status": "Available",
        "primary_dashboard": "https://stage.sandbox.predera.com/id",
        "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
        "additional_report_ids": "\"1123\", \"3494\"",
        "created_at": "2022-07-05T10:20:22.515729",
        "created_by": "vijaykumar.guntreddy@predera.com",
        "updated_at": "2022-07-05T10:20:22.515729",
        "updated_by": "srikanth.thirumala@predera.com",
        "app_image_url": demand_forecastingImg,
        "app_id": "demand_forecasting"
    },
    {
        "id": 12,
        "name": "Customer Acquisition",
        "description": "Identify and target high-value prospects  to drive profitable growth.",
        "Type": "Customer Acquisition",
        "category": "retail",
        "status": "Available",
        "primary_dashboard": "https://stage.sandbox.predera.com/id",
        "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
        "additional_report_ids": "\"1123\", \"3984\"",
        "created_at": "2022-07-05T10:19:59.670676",
        "created_by": "vijaykumar.guntreddy@predera.com",
        "updated_at": "2022-07-05T10:19:59.670676",
        "updated_by": "srikanth.thirumala@predera.com",
        "app_image_url": customer_aquisitionImg,
        "app_id": "customer_acquisition"
    },
    {
        "id": 13,
        "name": "Nurse Staffing",
        "description": "Optimize nurse staffing. This can help in optimizing cost and effective service delivery.",
        "Type": "Nurse Staffing",
        "category": "healthcare",
        "status": "Available",
        "primary_dashboard": "https://stage.sandbox.predera.com/id",
        "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
        "additional_report_ids": "\"1223\", \"0909\"",
        "created_at": "2022-07-05T10:20:22.515729",
        "created_by": "vijaykumar.guntreddy@predera.com",
        "updated_at": "2022-07-05T10:20:22.515729",
        "updated_by": "srikanth.thirumala@predera.com",
        "app_image_url": nurse_staffingImg,
        "app_id": "nurse_staffing"
    },
    {
        "id": 14,
        "name": "Asthma Prediction",
        "description": "Predict the development of severe asthma exacerbations.",
        "Type": "Asthma Prediction",
        "category": "healthcare",
        "status": "Available",
        "primary_dashboard": "https://stage.sandbox.predera.com/id",
        "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
        "additional_report_ids": "\"1223\", \"9394\"",
        "created_at": "2022-07-05T10:19:59.670676",
        "created_by": "vijaykumar.guntreddy@predera.com",
        "updated_at": "2022-07-05T10:19:59.670676",
        "updated_by": "vijaykumar.guntreddy@predera.com",
        "app_image_url": asthama_predictionsImg,
        "app_id": "asthma_prediction"
    },
    {
        "id": 15,
        "name": "Length of Stay",
        "description": "Predict the duration of hospitalization. A patient can stay at hospital as long as he is ill.",
        "Type": "Length of Stay",
        "category": "healthcare",
        "status": "Available",
        "primary_dashboard": "https://stage.sandbox.predera.com/id",
        "model_ref_id": "f662401899a94b5f98253579b163a99d",
        "additional_report_ids": "\"3399\", \"2933\"",
        "created_at": "2022-07-05T10:23:51.229208",
        "created_by": "srikanth.thirumala@predera.com",
        "updated_at": "2022-07-05T10:23:51.229208",
        "updated_by": "srikanth.thirumala@predera.com",
        "app_image_url": lengthofstayImg,
        "app_id": "length_of_stay"
    },
    {
        "id": 16,
        "name": "Sepsis Prediction",
        "description": "Reduce mortality by early prediction and diagnosis of Sepsis.",
        "Type": "Sepsis Prediction",
        "category": "healthcare",
        "status": "Available",
        "primary_dashboard": "https://stage.sandbox.predera.com/id",
        "model_ref_id": "f662401899a94b5f98253579b163a99d",
        "additional_report_ids": "\"1292\", \"9939\"",
        "created_at": "2022-07-05T10:24:17.528183",
        "created_by": "vijaykumar.guntreddy@predera.com",
        "updated_at": "2022-07-05T10:24:17.528183",
        "updated_by": "srikanth.thirumala@predera.com",
        "app_image_url": sepsis_predictionImg,
        "app_id": "sepsis_prediction"
    },
    {
        "id": 17,
        "name": "Readmissions",
        "description": "Readmission risk prediction model. You can predict the risk of readmission.",
        "Type": "Readmissions",
        "category": "healthcare",
        "status": "Available",
        "primary_dashboard": "https://stage.sandbox.predera.com/id",
        "model_ref_id": "a736a2a68ae74018bca74482e081873c",
        "additional_report_ids": "\"2239\", \"3949\"",
        "created_at": "2022-07-05T10:29:07.771793",
        "created_by": "srikanth.thirumala@predera.com",
        "updated_at": "2022-07-05T10:29:07.771793",
        "updated_by": "srikanth.thirumala@predera.com",
        "app_image_url": readmissionsImg,
        "app_id": "readmissions"
    },
    {
        "id": 18,
        "name": "Heart Failure",
        "description": "Predict mortality by heart failure using Artificial Intelligence.",
        "Type": "Heart Failure",
        "category": "healthcare",
        "status": "Available",
        "primary_dashboard": "https://stage.sandbox.predera.com/id",
        "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
        "additional_report_ids": "\"1221\", \"3829\"",
        "created_at": "2022-07-05T10:20:22.515729",
        "created_by": "vijaykumar.guntreddy@predera.com",
        "updated_at": "2022-07-05T10:20:22.515729",
        "updated_by": "srikanth.thirumala@predera.com",
        "app_image_url": heart_failureImg,
        "app_id": "heart_failure"
    }
]

export const EnabledApplications = [
    {
        "id": "lead_scoring",
        "name": "Lead Scoring",
        "description": "Improve conversion and close rates by identifying most valuable leads.",
        "Type": "Lead Conversion",
        "category": "sales",
        "status": "Enabled",
        "primary_dashboard": "https://stage.sandbox.com/id",
        "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
        "additional_report_ids": {"dashboard" : 5305, "performance" : 5306, "insights": 5307},
        "created_at": "2022-07-05T10:19:59.670676",
        "created_by": "srikanth.thirumala@predera.com",
        "updated_at": "2022-07-05T10:19:59.670676",
        "updated_by": "vijaykumar.guntreddy@predera.com",
        "app_image_url": lead_scroingImg,
        "app_url": 'https://sandbox.predera.com/sales-ai/',
        "app_id": "lead_scoring"
    },
    {
        "id": "demand_forecasting",
        "name": "Demand Forecasting",
        "description": "Estimate future customer demand over a certain time period.",
        "Type": "Demand Forecasting",
        "category": "retail",
        "status": "Enabled",
        "primary_dashboard": "https://stage.sandbox.predera.com/id",
        "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
        "additional_report_ids": {"dashboard" : 5305, "performance" : 5306, "insights": 5307},
        "created_at": "2022-07-05T10:20:22.515729",
        "created_by": "vijaykumar.guntreddy@predera.com",
        "updated_at": "2022-07-05T10:20:22.515729",
        "updated_by": "srikanth.thirumala@predera.com",
        "app_image_url": demand_forecastingImg,
        "app_url" : 'https://sandbox.predera.com/forecasting-app/',
        "app_id": "demand_forecasting"
    },
    {
        "id": 17,
        "name": "Readmissions",
        "description": "Readmission risk prediction model. You can predict the risk of readmission.",
        "Type": "Readmissions",
        "category": "healthcare",
        "status": "Enabled",
        "primary_dashboard": "https://stage.sandbox.predera.com/id",
        "model_ref_id": "a736a2a68ae74018bca74482e081873c",
        "additional_report_ids": "\"2239\", \"3949\"",
        "created_at": "2022-07-05T10:29:07.771793",
        "created_by": "srikanth.thirumala@predera.com",
        "updated_at": "2022-07-05T10:29:07.771793",
        "updated_by": "srikanth.thirumala@predera.com",
        "app_image_url": readmissionsImg,
        "app_id": "readmissions"
    },
]

export const tempState = {
    "id": 1,
    "name": "",
    "description": "",
    "Type": "Lead Scoring",
    "TargetColumn": "",
    "inputColumns": [],
    "datasourceRefId": "",
    "category": "sales",
    "status": "Enabled",
    "primary_dashboard": "https://stage.sandbox.com/id",
    "model_ref_id": "94da4f6c7c9649e5836c310766449bd1",
    "additional_report_ids": {"dashboard" : 5305, "performance" : 5306, "insights": 5307},
    "created_at": "2022-07-05T10:19:59.670676",
    "created_by": "srikanth.thirumala@predera.com",
    "updated_at": "2022-07-05T10:19:59.670676",
    "updated_by": "vijaykumar.guntreddy@predera.com",
    "app_image_url": lead_scroingImg,
    "app_id": "lead_scoring"
}

export const appConfigLiveModelMetrics = [
    {
        "name": "Accuracy",
        "value": "77.1"
    },
    {
        "name": "F1 Score",
        "value": "48.0"
    },
    {
      "name": "Precision",
      "value": "52.2"
    },
    {
      "name": "Recall",
      "value": "44.4"
    }
]

export const appConfigModelManagementMetrics = [
    {
      "id": 3,
      "version": "3",
      "created_by": "nazeer@predera.com",
      "created_at": "2022-06-08T17:00:00.000Z",
      "workflow": "Completed",
      "deployment": "Available",
      "active": "Yes",
      "action_1": "View",
      "action_2": "Restore",
      "action_3": "Rebuild"
    },
    {
        "id": 2,
        "version": "2",
        "created_by": "nazeer@predera.com",
        "created_at": "2022-06-07T17:00:00.000Z",
        "workflow": "Error",
        "deployment": "Not Available",
        "active": "No",
        "action_1": "View",
        "action_2": "Restore",
        "action_3": "Rebuild"
      },
      {
        "id": 1,
        "version": "1",
        "created_by": "nazeer@predera.com",
        "created_at": "2022-06-05T23:00:00.000Z",
        "workflow": "Completed",
        "deployment": "Available",
        "active": "No",
        "action_1": "View",
        "action_2": "Restore",
        "action_3": "Rebuild"
      }            
]

export const appConfigurationTeamMembersData = [
    {
      "id": 1,
      "name": "Nazeer Shaik",
      "email": "nazeer@predera.com",
      "access": "Admin",
      "active": "Yes",
      "action_1": "Edit",
      "action_2": "X",
    },
    {
        "id": 2,
        "name": "Vamshi Ambati",
        "email": "vamshi@predera.com",
        "access": "Admin",
        "active": "Yes",
        "action_1": "Edit",
        "action_2": "X",
      },
      {
        "id": 3,
        "name": "Srikanth Thirumala",
        "email": "srikanth.thirumala@predera.com",
        "access": "Basic",
        "active": "Yes",
        "action_1": "Edit",
        "action_2": "X",
      },
      {
        "id": 4,
        "name": "Preeti Karmakar",
        "email": "preeti.karmakar@predera.com",
        "access": "Power",
        "active": "Yes",
        "action_1": "Edit",
        "action_2": "X",
      },       
]