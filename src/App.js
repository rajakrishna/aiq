import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import "bootstrap/dist/css/bootstrap.css";
import "@fortawesome/fontawesome-free/css/all.css";
import "./fonts.css";
import "./App.css";
import "./micromodal.css";

import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Login from "./components/Login";
import Signup from "./components/Signup";
import Navigation from "./components/NavigationLinks";
import Home from "./components/Home";
import Projects from "./components/Projects";
import Notebooks from "./components/Notebooks";
import Help from "./components/Help";
import PasswordForgot from "./components/PasswordForgot";
import PasswordForgotVerification from "./components/PasswordForgotVerification";
import PasswordForgotConfirmation from "./components/PasswordForgotConfirmation";
import PasswordReset from "./components/PasswordReset";
import Profile from "./components/Profile";
import NotFound from "./components/NotFound";
import NetworkError from "./components/NetworkError";
import Company from "./components/Company";
import ModelDetails from "./components/modelDetails.jsx";
import AddJobs from "./components/AddJobForm.jsx";
import Redirection from "./components/Redirection.jsx";
// import JobDetails from "./components/jobDetails.jsx";
// import JobRunDetails from "./components/jobRunDetails.jsx";
import ProjectShow from "./components/ProjectShow";
import InitialPasswordChange from "./components/InitialPasswordChange";
import CustomizedSnackbars from "./components/Snackbar";
import RunTimeHub from "./components/Dashboards/RunTimeHub";
import DataInsight from "./components/Dashboards/DataInsights";
import HumanInLoop from "./components/Dashboards/HumanInLoop";
import BusinessAiq from "./components/Dashboards/BusinessAiq";
import { closeAlert } from "./ducks/alertsReducer";
import Monitoring from "./components/Monitoring";
import Deployment from "./components/modelDetails/Deployment";
import JobsList from "./components/JobsList";
import ExperimentsList from "./components/experimentsList";
import GettingStarted from "./components/GettingStarted";
import JobsRunList from "./components/JobsRunList";
import RegisteredModelVersionDetails from "./components/RegisteredModelVersionDetails";
import RegisteredModelVersions from "./components/RegisteredModelVersions";
import ImportModel from "./components/models/ImportModel";
import Notifications from "./components/Notifications";
import ModelCatalog from "./components/ModelCatalog";
import deploymentsList from "./components/deploymentsList";
import NotebookForm from "./components/NotebookForm";
import ProjectActivity from "./components/ProjectActivity";
import ManageTeam from "./components/ManageTeam";
import Applications from "./components/Applications";
import DeploymentForm from "./components/deployments/DeploymentForm";
import Datasets from "./components/Datasets";
import Registration from "./components/Registration";
import SidebarDrawer from "./components/layouts/SidebarDrawer";
import DataSourceDetails from "./components/datasourceDetails";
import DataSourceForm from "./components/datasourceForm";
import DataSourceCatalog from "./components/datasourceCatalog";
import notebookDetails from "./components/notebookDetails";

class App extends Component {
  render() {
    const token = this.props.auth.token;
    const { alActive, alType, msg } = this.props;

    return (
      <Router basename={process.env.REACT_APP_BASE_URL}>
        <div>
          {/* <FullLoader display={true} /> */}
          <CustomizedSnackbars
            snackbarState={alActive}
            closeSnackbar={() => {
              this.props.closeAlert();
            }}
            variant={alType}
            message={msg}
          />
          {token && (
            <div>
              <div className="content-wraper-sm top-content-wraper">
                <Navigation />
              </div>
              {/* {window.location.pathname !== '/projects' && 
                <div>
                  <Sidebar />
                </div>
              } */}
            </div>
          )}

          {/* routes */}
          <div
          //  style={token && {paddingLeft: '200px'}}
          >
            <Switch>
              <PrivateRoute exact path={"/home"} component={Home} token={token} />
              <PrivateRoute exact path={"/"} component={Projects} token={token} />

              <SidebarLayout
                exact
                path={"/dashboard"}
                component={Home}
                token={token}
              />
              <PrivateRoute
                path={"/Getting-Started"}
                component={GettingStarted}
                token={token}
              />
              <SidebarLayout
                exact
                path={"/dashboard/run_time_hub"}
                component={RunTimeHub}
                token={token}
              />
              <SidebarLayout
                exact
                path={"/dashboard/data_insight"}
                component={DataInsight}
                token={token}
              />
              <SidebarLayout
                exact
                path={"/dashboard/human_in_loop"}
                component={HumanInLoop}
                token={token}
              />
              <SidebarLayout
                exact
                path={"/dashboard/business_aiq"}
                component={BusinessAiq}
                token={token}
              />

              <PrivateRoute
                exact
                path={"/projects"}
                component={Projects}
                token={token}
              />
              {/* <SidebarLayout
                path={"/project_details"}
                component={ProjectActivity}
                token={token}
              /> */}
              <SidebarLayout
                path={"/project_details"}
                component={ProjectActivity}
                token={token}
              />
              <SidebarLayout
                exact
                path={"/datasets"}
                component={Datasets}
                token={token}
              />
              <SidebarLayout
                exact
                path={"/datasets/datasource_catalog"}
                component={DataSourceCatalog}
                token={token}
              />
              <SidebarLayout
                exact
                path={"/datasets/:name"}
                component={DataSourceDetails}
                token={token}
              />
              <SidebarLayout
                exact
                path={"/datasource_form"}
                component={DataSourceForm}
                token={token}
              />
              <SidebarLayout
                exact
                path={"/notebooks"}
                component={Notebooks}
                token={token}
              />
              <SidebarLayout
                exact
                path={"/notebooks/new"}
                component={NotebookForm}
                token={token}
              />

              {process.env.REACT_APP_COMPONENT_NOTEBOOKS != "DISABLED" && (
                <SidebarLayout
                  exact
                  path={"/notebooks"}
                  component={Notebooks}
                  token={token}
                />
                )}

              <SidebarLayout
                exact
                path={"/notebooks/:id"}
                component={notebookDetails}
                token={token}
              />
              
              <PrivateRoute exact path={"/help"} component={Help} token={token} />
              <SidebarLayout
                exact
                path={"/models/new"}
                component={ImportModel}
                token={token}
              />
              <SidebarLayout
                exact
                path={"/models"}
                key={"models"}
                component={ModelCatalog}
                token={token}
              />
              <SidebarLayout
                exact
                path={"/models/:id"}
                key={"registeredModelVersion"}
                component={RegisteredModelVersions}
                token={token}
              />
              <SidebarLayout
                exact
                path={"/registered-model-versions"}
                key={"registeredModelVersion"}
                component={RegisteredModelVersions}
                token={token}
              />
              <SidebarLayout
                exact
                path={"/registered-model-versions/:id"}
                component={RegisteredModelVersionDetails}
                token={token}
              />
              <SidebarLayout
                exact
                path={"/monitoring"}
                component={Monitoring}
                token={token}
              />
              <SidebarLayout
                exact
                path={"/monitoring/:id"}
                component={ModelDetails}
                token={token}
              />
              <SidebarLayout
                path={"/experiments/:id"}
                component={ModelDetails}
                token={token}
              />
              <SidebarLayout
                exact
                path={"/deployments"}
                key={"deployments"}
                component={deploymentsList}
                token={token}
              />
              <SidebarLayout
                exact
                path={"/deployments/new"}
                key={"deployments"}
                component={DeploymentForm}
                token={token}
              />
              <SidebarLayout
                exact
                path={"/deployments/:name"}
                component={Deployment}
                token={token}
              />
              <PrivateRoute
                exact
                path={"/applications"}
                component={Applications}
                token={token}
              />
              <SidebarLayout
                exact
                path={"/workflows"}
                component={JobsList}
                token={token}
              />
              <PrivateRoute
                exact
                path={"/workflows/new"}
                component={AddJobs}
                token={token}
              />
              <PrivateRoute
                exact
                path={"/workflows/:id"}
                component={AddJobs}
                token={token}
              />
              <PrivateRoute
                exact
                path={"/manage_team"}
                component={ManageTeam}
                token={token}
              />
              <PrivateRoute
                exact
                path={"/redirect/:id"}
                component={Redirection}
                token={token}
              />
              {/* <PrivateRoute
                path={"/workflows/:id"}
                key={(new Date()).getTime()}
                component={AddJobs}
                token={token}
              /> */}
              <PrivateRoute
                exact
                path={"/run-history/:id"}
                component={AddJobs}
                key={"run-history"}
                token={token}
              />
              <SidebarLayout
                exact
                path={"/run-history"}
                component={JobsRunList}
                token={token}
              />
              <SidebarLayout
                exact
                path={"/experiments"}
                component={ExperimentsList}
                token={token}
              />
              <PrivateRoute
                exact
                path={"/reset_password"}
                component={PasswordReset}
                token={token}
              />
              <PrivateRoute
                exact
                path={"/notifications"}
                component={Notifications}
                token={token}
              />
              <PrivateRoute
                exact
                path={"/profile"}
                component={Profile}
                token={token}
              />
              <PrivateRoute
                exact
                path={"/company"}
                component={Company}
                token={token}
              />
              <PrivateRoute
                exact
                path={"/network_error"}
                component={NetworkError}
                token={token}
              />
              <Route path="/sign_in" component={Login} />
              <Route path="/sign_up" component={Signup} />
              <Route path={"/registration"} component={Registration} />
              <Route
                path={"/initial_password_change/:username"}
                component={InitialPasswordChange}
              />
              <Route path={"/forgot-password"} component={PasswordForgot} />
              <Route
                path={"/forgot-password-verification"}
                component={PasswordForgotVerification}
              />
              <Route
                path={"/forgot-password-confirmation"}
                component={PasswordForgotConfirmation}
              />
              <Route component={NotFound} />
            </Switch>
          </div>

          <ToastContainer
            position="top-right"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnVisibilityChange
            draggable
            pauseOnHover
          />
          {/* Same as */}
          <ToastContainer />
        </div>
      </Router>
    );
  }
}

/**
 * A helper private route component
 * check if the autentication granted, then only allow to route to the component
 * refactor to another file if required at other palces
 */
const PrivateRoute = ({ component: Component, token, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      authenticated(token) ? (
        <div className="header-top-margin">
          <Component {...props} />
        </div>
      ) : (
        <Redirect
          to={{
            pathname: "/sign_in",
            state: { from: props.location },
          }}
        />
      )
    }
  />
);

const SidebarLayout = ({ component: Component, token, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      authenticated(token) ? (
        <SidebarDrawer><Component {...props} /></SidebarDrawer>
      ) : (
        <Redirect
          to={{
            pathname: "/sign_in",
            state: { from: props.location },
          }}
        />
      )
    }
  />
);
/**
 * Authenticated checks if an access token is available in the state
 */
const authenticated = (token) => {
  return !!token;
};

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    msg: state.alerts.msg,
    alType: state.alerts.alType,
    alActive: state.alerts.alActive,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      closeAlert
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

// export default App;
