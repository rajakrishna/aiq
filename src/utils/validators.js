// Most names and labels in AIQ need conform to DNS (RFC 1035) definition.
const dns1035LabelFmt = "[a-z]([-a-z0-9]*[a-z0-9])?";
const dns1035LabelErrMsg =
  "must consist of lower case alphanumeric characters or '-', and must start and end with an alphanumeric character";
const dns1035LabelMaxLength = 63;

var dns1035LabelRegexp = new RegExp("^" + dns1035LabelFmt + "$");

/**
 * Validates if a given value is a valid DNS 1035 label.
 *
 * @param {string} value - The value to be validated.
 * @returns {object} - An object containing the validation result and an error message.
 */
export function isDNS1035Label(value) {
  let validationResult = { isValid: true, error: "" };
  if (value.length > dns1035LabelMaxLength) {
    validationResult.isValid = false;
    validationResult.error = maxLenError(dns1035LabelMaxLength);
  }
  if (!dns1035LabelRegexp.test(value)) {
    validationResult.isValid = false;
    validationResult.error = dns1035LabelErrMsg;
  }
  return validationResult;
}

const secretKeyFmt = `[-._a-zA-Z0-9]+`;
const secretKeyErrMsg =
  "must consist of alphanumeric characters, '-', '_' or '.'";
const secretKeyMaxLength = 253;

var secretKeyRegexp = new RegExp("^" + secretKeyFmt + "$");

/**
 * Validates if a given value is a valid key for a ConfigMap or Secret
 *
 * @param {string} value - The value to be validated.
 * @returns {object} - An object containing the validation result and an error message.
 */
export function isSecretKey(value) {
  let validationResult = { isValid: true, error: "" };
  if (value.length > secretKeyMaxLength) {
    validationResult.isValid = false;
    validationResult.error = maxLenError(secretKeyMaxLength);
  }
  if (!secretKeyRegexp.test(value)) {
    validationResult.isValid = false;
    validationResult.error = secretKeyErrMsg;
  }
  return validationResult;
}

/**
 * Returns a string explanation of a "string too long" validation failure.
 *
 * @param {number} length - The maximum allowed length for the string.
 * @return {string} - An error message describing the validation failure.
 */
export function maxLenError(length) {
  return `must be no more than ${length} characters`;
}
