export function loadToken() {
  try {
    const token = localStorage.getItem('token');
    if(token === null) {
      return undefined;
    }
    return JSON.parse(token);
  } catch (err) {
    console.info('Session storage cannot be accessed!')
    return undefined;
  }
}

export function saveState(data) {
  try {
    const serailaizedToken = JSON.stringify(data.token);
    localStorage.setItem('token', serailaizedToken);
    const serailaizedAccess = JSON.stringify(data.access);
    localStorage.setItem('access', serailaizedAccess);
  } catch(err) {
    console.info('Saving state to session storage failed');
  }
}
