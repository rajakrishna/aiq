export const isEmpty = (obj) => {
  for (var key in obj)
    return false;

  return true
}

export const capitalizeVars = (str) => {
  str = str.toUpperCase();
  return str.replace(/_|\//g," ").replace(/(\w)(\w{2,})/g,(str,grp1,grp2) => {
      return(grp1+grp2.toLowerCase());
  });
}

export const actLikeClick = (event, func, attr) => {
  if([13, 32].includes(event.charCode)) {
    func(attr);
  }
}

export const reorderElems = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

export const findStatus = (props) => {
  const { type, index, spec, jobStore } = props;
  const steps = (jobStore.status && jobStore.status.steps) || [];
  const displayName = index >= 0 ? type + '-' + index + '-' + spec[index].name : type + '-' + spec.name;
  const filteredSteps = steps && Object.keys(steps).filter((e, i) => {
    return steps[e].displayName === displayName.replace(/_/g, "-");
  })
  const status = steps && steps[filteredSteps[0]];
  return status ? status.phase : "";
}

export const getStatusColorIcon = (type) => {
  const status = {
    'CREATED': ["#0cb8fd", "fa fa-info-circle"],
    'STARTED':["#0cb8fd", "fa fa-info-circle"],
    'RUNNING':["#0cb8fd", "fa fa-circle-notch rotate"],
    'FAILED': ["red", "fa fa-exclamation-circle"],
    'ERRORED': ["red", "fa fa-exclamation-circle"],
    'SUCCEEDED': ["#57bc80", "fa fa-check-circle"],
    'TERMINATED': ["red", "fa fa-exclamation-circle"]
  }
  const data = status[type] ? status[type] : ["grey", "fa fa-circle"];
  return data;
}

export const arrHasElem = (arr, elem) => {
  let flag = -1;
  arr.forEach((e,i) => {
    let pattern = new RegExp(elem+"\\d","g");
    if(typeof e === 'object' && e.some(val => (pattern).test(val))) {
      flag = i;
    } else if(e === elem) {
      flag = i;
    }
  })
  return flag;
}

export const getLength = (arr, elem) => {
  let len = 0;
  arr.forEach((e,i) => {
    let pattern = new RegExp(elem+"\\d","g");
    if(typeof e === 'object' && e.some(val => (pattern).test(val))) {
      len = e.length;
    } 
  });
  return len;
}

export const sortByArray = (arr1, arr2) => {
  let arr = arr2.map((e) => {
    let index = arrHasElem(arr1, e);
    if(index > -1) {
      return arr1[index];
    }
  })
  return arr.filter(e => {return e});
}

export const totalArrayLength = (arr) => {
  let len = 0;
  for(let i in arr){
    if(typeof arr[i] === "object") {
      len += totalArrayLength(arr[i]);
    }
    else len++;
  }
  return len;
}

export const delKeys = (app) => {
  for(var key in app){
    if(app[key] !== null && typeof(app[key]) === 'object' && app[key] !== ""){
      delKeys(app[key])

      if(isEmpty(app[key])) {
        delete app[key]
      }
    } 
    if(app[key] === null){
      delete app[key]
    }
  }
}

export const getDeployementType = (deployment) => {
  const graph = deployment.spec && deployment.spec.predictors && deployment.spec.predictors[0].graph;
  if(graph && graph.children && graph.children.length) {
    if(graph.children[0] && graph.children[0].type) {
      return graph.children[0].type;
    } else {
      return graph.children.implementation;
    }
  } else if(graph && graph.type) {
    return graph.type;
  } else {
    return "MODEL";
  }
}

export const parseDepList = (data, size, currentDepPage = 0, max = 20) => {
  const range = [max * currentDepPage, (max * (currentDepPage + 1)) - 1]
  range[1] = range[1] <= size ? range[1] : size;
  const parsedData = [];
  for (let i = range[0]; i <= range[1]; i++) {
      if (data[i]) {
          parsedData.push(data[i]);
      }
  }
  return parsedData;
}

export const getErrorMessage = (error) => {
  if (error.response && error.response.data && (error.response.data.detail || error.response.data.title || error.response.data.message)) {
    return error.response.data.detail  || error.response.data.title || error.response.data.message
  } else if (error.response && error.response.data && error.response.data instanceof String) {
    return error.response.data
  } else if (error.message) {
    return error.message
  } else {
    return undefined
  }
}

export const getResponseError = (response) => {
  if (response.data && ( response.data.detail || response.data.title || response.data.message)) {
    return response.data.detail || response.data.title || response.data.message
  } else if (response.data && response.data instanceof String) {
    return response.data
  } else {
    return undefined
  }
}

export const parseEnvSrcToArray = (env_source=[]) => {
  const parsed_env = env_source.map((e)=>{
      return e.name
  })
  return parsed_env;
}

export const parseArrayToEnvSrc = (data) => {
  return data.map((e)=>{
    return({
        type: "secretRef",
        name: e
    })
  })
}

export const parseSecretToArray = (data) => {
  return data.map((e)=>{
    return(e.name);
  })
}

export const parseArrayToSecret = (data) => {
  return data.map((e)=>{
    return({
        name: e
    })
  })
}

export const randomStrings = (len, str) => { 
  let val = ''; 
  for (let i = len; i > 0; i--) { 
      val += str[Math.floor(Math.random() * str.length)]; 
  } 
  return val; 
}

export const sortJobRunByCreatedDate = (jobsListData) => {
  if(typeof jobsListData === "object" && jobsListData.length) {
    jobsListData.sort((a,b) => (a.metadata.creationTimestamp < b.metadata.creationTimestamp) ? 1 : ((b.metadata.creationTimestamp < a.metadata.creationTimestamp) ? -1 : 0));
    return jobsListData;
  } else {
    return [];
  }
}


export const getObjValueFromArray = (obj, id, key) => {
  let arr = obj.filter(e => e.id === id);
  return arr[0][key]
}

export const getDataSize = (data) => {
  if(isNaN(data)) return;
  let val = data;
  let count = 0
  let unit = 'B'
  while (val > 999) {
    val = val/1024;
    count++;
  }
  switch(count) {
    case 0: 
      unit = 'B';
    break;
    case 1: 
      unit = 'KB';
    break;
    case 2: 
      unit = 'MB';
    break;
    case 3: 
      unit = 'GB';
    break;
  }
  return Math.floor(val)+unit
}

export const l = {
  'DEPLOY_SIMPLE': "Custom Code",
  'DEPLOY_ARTIFACT': "Deploy Artifact",
  'ENSEMBLE_OF_MODELS': "Ensemble of Model",
  'DEPLOY_TF_SERVING': "Tensorflow Model",
  'DEPLOY_AB_TEST': "A/B Test",
  'DEPLOY_TRANSFORMER': "Model with Transformer",
  'DEPLOY_GRAPH': "Graph",
  "ONE_HOT_ENCODING": "One Hot Encoding",
  "IMAGE_TO_TFRECORD": "Image to TFRecord",
  "SPARK": "Spark",
  "DASK": "Dask",
  "PANDAS": "Pandas",
  "SIMPLE": "Custom Code",
  "TRAIN_SIMPLE": "Custom Code",
  "TRAIN_DISTRIBUTED": "Tensorflow Distributed Training",
  "VALIDATE": "TRAIN_SIMPLE",
  "data_source": "Data Inputs",
  "outputs": "Data Outputs",
  "env": "Environment Variables",
  "env_a": "Environment Variables A",
  "env_b": "Environment Variables B",
  "model_env": "Model Environment Variables",
  "transformer_env": "Transformer Environment Variables",
  "global_env_variables": "Global Environment Variables",
  "PYTHON2": "Python 2",
  "PYTHON3": "Python 3",
  "JAVA_1_8": "Java 1.8",
  "JAVA11": "Java 11",
  "R_3_6": "R 3.6",
  "SPARK_PYTHON3": "Spark Python 3",
  "SPARK_2_4": "Spark 2.4",
  'SKLEARN': "SK Learn",
  'TENSORFLOW': "Tensorflow",
  'KERAS': "Keras",
  'PYTORCH': "PyTorch",
  'H2O': "H2O",
  'SPARKML': "Spark ML",
  'SAGEMAKER': "Sage Maker",
  'TPOT': "Tpot",
  'AUTO_SKLEARN': "Auto SK Learn",
  'H2O_AUTOML': "H2O Auto ML",
  "/home": "Home",
  "/projects": "Projects",
  "/experiments": "Experiments",
  "/models": "Models",
  "/monitoring": "Monitoring",
  "/deployments": "Deployments",
  "/workflows": "Workflows",
  "/run-history": "Run History",
  "/workflows/new": "New Workflow",
  "/notifications": "Notifications"
}

export const runtime = [
  "PYTHON2",
  "PYTHON3",
  "JAVA_1_8",
  "JAVA11",
  "R_3_6",
]

export const sparkRuntime = [
  "SPARK_PYTHON3",
]

export const sparkVersion = [
  "SPARK_2_4"
]

export const mlLibraries = [
  'SKLEARN',
  'TENSORFLOW',
  'KERAS',
  'PYTORCH',
  'H2O',
  'SPARKML',
  'SAGEMAKER',
  'TPOT',
  'AUTO_SKLEARN',
  'H2O_AUTOML'
]

export const modelTypes = [
  "CLASSIFICATION",
  "REGRESSION",
  "CLUSTERING",
]

// export const lr = {
//   "SKlearn Model": 'SKLEARN_MODEL',
//   "Keras Model": 'KERAS_MODEL',
//   "Ensemble of Model": 'ENSEMBLE_OF_MODELS',
//   "Tensorflow Model": 'DEPLOY_TF_SERVING',
//   "A/B Test": 'DEPLOY_AB_TEST',
//   "Model with Transformer": 'DEPLOY_TRANSFORMER',
//   "Graph": 'DEPLOY_GRAPH',
//   "One Hot Encoding": "ONE_HOT_ENCODING",
//   "Image to TFRecord": "IMAGE_TO_TFRECORD",
//   "Custom Code": "TRAIN_SIMPLE",
//   "Spark": "SPARK",
//   "Dask": "DASK",
//   "Pandas": "PANDAS",
//   "Custom Deploy": "DEPLOY_SIMPLE",
//   "Distributed Train": "TRAIN_DISTRIBUTED",
//   "Custom Validate": "VALIDATE"
// }